package com.navdy.client.app.ui.search;

import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Spannable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.UiUtils;
import com.navdy.client.app.ui.customviews.DestinationImageView;
import com.navdy.client.app.ui.customviews.UnitSystemTextView;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

class SearchViewHolder extends ViewHolder {
    public static final int DIVIDER_HEIGHT = UiUtils.convertDpToPx(1.0f);
    protected View bottomItemDivider;
    private TextView details;
    private UnitSystemTextView distance;
    private DestinationImageView image;
    protected View itemView;
    protected ImageButton navButton;
    private TextView price;
    protected View row;
    private TextView title;

    public enum PhotoType {
        RESOURCE_IMAGE,
        CONTACT
    }

    SearchViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        this.row = itemView.findViewById(R.id.search_row);
        if (this.row == null) {
            this.row = itemView.findViewById(R.id.header_container);
        }
        this.title = (TextView) itemView.findViewById(R.id.search_row_title);
        this.details = (TextView) itemView.findViewById(R.id.search_row_details);
        this.distance = (UnitSystemTextView) itemView.findViewById(R.id.search_row_distance);
        this.price = (TextView) itemView.findViewById(R.id.search_row_price);
        this.image = (DestinationImageView) itemView.findViewById(R.id.search_row_image);
        this.navButton = (ImageButton) itemView.findViewById(R.id.nav_button);
        this.bottomItemDivider = itemView.findViewById(R.id.bottom_item_divider);
    }

    public void setTitle(String text, int colorResource) {
        TextView textView = this.title;
        if (StringUtils.isEmptyAfterTrim(text)) {
            text = "";
        }
        textView.setText(text);
        this.title.setTextColor(ContextCompat.getColor(NavdyApplication.getAppContext(), colorResource));
    }

    public void setTitle(Spannable text) {
        this.title.setText(text);
    }

    public void setTitle(String text) {
        this.title.setText(text);
    }

    void setDetails(String text, int colorResource) {
        this.details.setVisibility(StringUtils.isEmptyAfterTrim(text) ? GONE : VISIBLE);
        this.details.setText(text);
        this.details.setTextColor(ContextCompat.getColor(NavdyApplication.getAppContext(), colorResource));
    }

    public void setDetails(String text) {
        this.details.setVisibility(StringUtils.isEmptyAfterTrim(text) ? GONE : VISIBLE);
        this.details.setText(text);
    }

    public void setDetails(Spannable text) {
        this.details.setVisibility(StringUtils.isEmptyAfterTrim(text) ? GONE : VISIBLE);
        this.details.setText(text);
    }

    void setDistance(double distanceInMeters) {
        this.distance.setVisibility(distanceInMeters <= 0.0d ? GONE : VISIBLE);
        this.distance.setDistance(distanceInMeters);
    }

    void setPrice(String priceString) {
        this.price.setVisibility(StringUtils.isEmptyAfterTrim(priceString) ? GONE : VISIBLE);
        this.price.setText(priceString);
    }

    public void setImage(SearchRecyclerAdapter.SearchItem searchItem, boolean isAutocomplete) {
        this.image.clearInitials();
        this.image.setImageResource(searchItem.getRes(isAutocomplete));
    }

    public void setImage(Bitmap bitmap) {
        this.image.clearInitials();
        this.image.setImage(bitmap);
    }

    public void setImage(String name) {
        this.image.setImage(name);
    }

    void showNavButton() {
        this.navButton.setVisibility(VISIBLE);
    }

    void hideNavButton() {
        this.navButton.setVisibility(INVISIBLE);
    }

    void showDeleteButton() {
        this.navButton.setVisibility(VISIBLE);
        this.navButton.setImageResource(R.drawable.icon_delete);
    }

    void hideBottomDivider() {
        if (this.bottomItemDivider != null) {
            this.bottomItemDivider.setVisibility(GONE);
        }
    }

    void showShortDivider() {
        changeDividerAlignment(true);
    }

    void showLongDivider() {
        changeDividerAlignment(false);
    }

    private void changeDividerAlignment(boolean showShort) {
        if (this.bottomItemDivider != null) {
            LayoutParams layoutParams = new LayoutParams(-1, DIVIDER_HEIGHT);
            if (showShort) {
                layoutParams.addRule(5, R.id.text_section);
                layoutParams.addRule(18, R.id.text_section);
            }
            layoutParams.addRule(12);
            this.bottomItemDivider.setLayoutParams(layoutParams);
            this.bottomItemDivider.setVisibility(VISIBLE);
        }
    }
}
