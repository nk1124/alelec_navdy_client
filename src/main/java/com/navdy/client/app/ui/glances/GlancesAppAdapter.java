package com.navdy.client.app.ui.glances;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;

import java.util.List;

class GlancesAppAdapter extends ArrayAdapter<ApplicationInfo> {
    private final List<ApplicationInfo> appsList;
    private boolean glancesAreEnabled = false;
    private final LayoutInflater layoutInflater;
    private final PackageManager packageManager;
    private SharedPreferences sharedPrefs;
    public static final Logger sLogger = new Logger(GlancesAppAdapter.class);

    GlancesAppAdapter(@NonNull Context context, @LayoutRes int textViewResourceId, List<ApplicationInfo> appsList) {
        super(context, textViewResourceId, appsList);
        this.appsList = appsList;
        this.sharedPrefs = SettingsUtils.getSharedPreferences();
        if (this.sharedPrefs != null) {
            this.glancesAreEnabled = this.sharedPrefs.getBoolean(SettingsConstants.GLANCES, SettingsConstants.GLANCES_ENABLED_DEFAULT);
        }
        this.packageManager = context.getPackageManager();
        this.layoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.appsList != null ? this.appsList.size() : 0;
    }

    public ApplicationInfo getItem(int position) {
        if (this.appsList == null || position == 0) {
            return null;
        }
        return (ApplicationInfo) this.appsList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null || Boolean.TRUE.equals(convertView.getTag())) {
            convertView = this.layoutInflater.inflate(R.layout.app_list_row, parent, false);
        }
        convertView.setTag(Boolean.FALSE);
        if (this.appsList != null) {
            final ApplicationInfo data = (ApplicationInfo) this.appsList.get(position);
            if (data != null) {
                final Switch appName = (Switch) convertView.findViewById(R.id.app_name);
                if (appName != null) {
                    appName.setText(data.loadLabel(this.packageManager));
                    appName.setEnabled(this.glancesAreEnabled);
                    appName.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            GlanceUtils.saveGlancesConfigurationChanges(data.packageName, appName.isChecked());
                            GlancesAppAdapter.this.setSwitchColor(appName, appName.isChecked());
                        }
                    });
                    boolean isChecked = this.sharedPrefs.getBoolean(data.packageName, false);
                    appName.setChecked(isChecked);
                    setSwitchColor(appName, isChecked);
                }
                ImageView iconView = (ImageView) convertView.findViewById(R.id.app_icon);
                if (iconView != null) {
                    try {
                        iconView.setImageDrawable(data.loadIcon(this.packageManager));
                    } catch (OutOfMemoryError ignored) {
                        sLogger.i("iconView failed due to lack of memory");
                    }
                    iconView.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            if (appName != null && appName.isEnabled()) {
                                appName.performClick();
                            }
                        }
                    });
                }
            }
        }
        return convertView;
    }

    public void setSwitchColor(@NonNull Switch appSwitch, boolean isChecked) {
        Context context = NavdyApplication.getAppContext();
        int trackColor = ContextCompat.getColor(context, isChecked ? R.color.blue_high : R.color.grey);
        int thumbColor = ContextCompat.getColor(context, isChecked ? R.color.blue : R.color.grey_3);
        Drawable trackDrawable = appSwitch.getTrackDrawable();
        Drawable thumbDrawable = appSwitch.getThumbDrawable();
        if (VERSION.SDK_INT < 23) {
            if (trackDrawable != null) {
                trackDrawable.setColorFilter(trackColor, Mode.MULTIPLY);
            }
            if (thumbDrawable != null) {
                thumbDrawable.setColorFilter(thumbColor, Mode.MULTIPLY);
                return;
            }
            return;
        }
        if (trackDrawable != null) {
            trackDrawable.setTint(trackColor);
        }
        if (thumbDrawable != null) {
            thumbDrawable.setTint(thumbColor);
        }
    }
}
