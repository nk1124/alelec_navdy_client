package com.navdy.client.app.ui.firstlaunch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.FlashlightManager;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch;
import com.navdy.client.app.ui.PhotoViewerActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.settings.ContactUsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class CarInfoActivity extends BaseToolbarActivity {
    private static final int[] obdLocations = new int[]{R.drawable.obd_port_location_1, R.drawable.obd_port_location_2, R.drawable.obd_port_location_3, R.drawable.obd_port_location_4, R.drawable.obd_port_location_5, R.drawable.obd_port_location_6, R.drawable.obd_port_location_7, R.drawable.obd_port_location_8, R.drawable.obd_port_location_9, R.drawable.img_obd_location_unknown};
    public static final int DEFAULT_OBD_LOCATION = (obdLocations.length - 1);
    private String nextStep;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fle_vehicle_obd_locator);
        TextView yearMakeModel = (TextView) findViewById(R.id.subtitle);
        ImageView obdLocation = (ImageView) findViewById(R.id.obd_location);
        ImageView obdCrimePhoto = (ImageView) findViewById(R.id.obd_crime_photo);
        TextView help = (TextView) findViewById(R.id.help);
        Button done = (Button) findViewById(R.id.done);
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        String makeString = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String yearString = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String modelString = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        int obdLocationNumber = customerPrefs.getInt(ProfilePreferences.CAR_OBD_LOCATION_NUM, DEFAULT_OBD_LOCATION);
        Spanned helpString = StringUtils.fromHtml((int) R.string.need_help_contact_support);
        this.nextStep = getIntent().getStringExtra("extra_next_step");
        if (StringUtils.isEmptyAfterTrim(yearString) || StringUtils.isEmptyAfterTrim(makeString) || StringUtils.isEmptyAfterTrim(modelString)) {
            startEditInfoActivity(null);
            return;
        }
        if (yearMakeModel != null) {
            yearMakeModel.setText(SettingsUtils.getCarYearMakeModelString());
        }
        if (obdLocation != null) {
            if (obdLocationNumber < 0 || obdLocationNumber >= obdLocations.length) {
                obdLocation.setImageResource(obdLocations[DEFAULT_OBD_LOCATION]);
            } else {
                obdLocation.setImageResource(obdLocations[obdLocationNumber]);
            }
        }
        Bitmap crimePhoto = Tracker.getObdImage();
        if (obdCrimePhoto != null) {
            if (crimePhoto != null) {
                obdCrimePhoto.setImageBitmap(crimePhoto);
            } else {
                obdCrimePhoto.setImageResource(R.drawable.image_generic_port);
            }
        }
        if (help != null) {
            help.setText(helpString);
        }
        new ToolbarBuilder().title((int) R.string.obd2_port_locator).build();
        if (done == null) {
            return;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(this.nextStep, "installation_flow") || StringUtils.equalsOrBothEmptyAfterTrim(this.nextStep, "app_setup")) {
            done.setText(R.string.next);
            done.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.icon_installaton_next, 0);
            return;
        }
        done.setText(R.string.done);
        done.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
    }

    private String getLocalizedLocationDescription(String accessNote, String note) {
        String obdLocationString;
        accessNote = CarMdUtils.getLocalizedAccessNote(accessNote);
        note = CarMdUtils.getLocalizedNote(note);
        Context context = NavdyApplication.getAppContext();
        if (StringUtils.isEmptyAfterTrim(accessNote)) {
            obdLocationString = note;
        } else {
            obdLocationString = context.getString(R.string.text_with_parentheses, new Object[]{note, accessNote});
        }
        if (StringUtils.isEmptyAfterTrim(obdLocationString)) {
            return context.getString(R.string.obd_location_unknown);
        }
        return obdLocationString;
    }

    protected void onResume() {
        super.onResume();
        hideSystemUiDependingOnOrientation();
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        String obdLocationString = getLocalizedLocationDescription(customerPrefs.getString(ProfilePreferences.CAR_OBD_LOCATION_ACCESS_NOTE, ""), customerPrefs.getString(ProfilePreferences.CAR_OBD_LOCATION_NOTE, ""));
        TextView obdLocationDesc = (TextView) findViewById(R.id.obd_locator_description);
        if (obdLocationDesc != null) {
            obdLocationDesc.setText(obdLocationString);
        }
        Tracker.tagScreen(FirstLaunch.CAR_INFO);
    }

    protected void onPause() {
        FlashlightManager.getInstance().turnFlashLightOff();
        super.onPause();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 2) {
            hideSystemUI();
        } else if (newConfig.orientation == 1) {
            showSystemUI();
        }
    }

    public void startEditInfoActivity(View view) {
        startActivity(new Intent(getApplicationContext(), EditCarInfoActivity.class));
        finish();
    }

    public void onFlashlightClick(View view) {
        FlashlightManager.getInstance().switchLight(this);
    }

    public void onHelpClick(View view) {
        Intent intent = new Intent(getApplicationContext(), ContactUsActivity.class);
        intent.putExtra(ContactUsActivity.EXTRA_DEFAULT_PROBLEM_TYPE, 1);
        startActivity(intent);
    }

    public void onBackClick(View view) {
        finish();
    }

    public void onDoneClick(View view) {
        if (StringUtils.equalsOrBothEmptyAfterTrim(this.nextStep, "installation_flow")) {
            Intent intent = new Intent(getApplicationContext(), InstallActivity.class);
            intent.putExtra("extra_step", R.layout.fle_install_tidying_up);
            startActivity(intent);
            return;
        }
        finish();
    }

    public void onCrimePhotoClick(View view) {
        Intent intent = new Intent(getApplicationContext(), PhotoViewerActivity.class);
        intent.putExtra(PhotoViewerActivity.EXTRA_FILE_PATH, ProfilePreferences.OBD_IMAGE_FILE_NAME);
        startActivity(intent);
    }

    public void onWatchVideoClick(View view) {
        InstallActivity.startVideoPlayerForThisStep(R.layout.fle_install_locate_obd, this);
    }
}
