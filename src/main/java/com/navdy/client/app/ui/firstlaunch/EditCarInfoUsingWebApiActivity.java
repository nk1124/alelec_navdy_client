package com.navdy.client.app.ui.firstlaunch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.LocalyticsManager;
import com.navdy.client.app.framework.util.CarMdCallBack;
import com.navdy.client.app.framework.util.CarMdClient;
import com.navdy.client.app.framework.util.CarMdClient.CarMdObdLocationResponse;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.task.TaskManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import org.droidparts.adapter.widget.StringSpinnerAdapter;

@Deprecated
public class EditCarInfoUsingWebApiActivity extends BaseToolbarActivity {
    static final String EXTRA_NEXT_STEP = "extra_next_step";
    static final String INSTALL = "installation_flow";
    private static final int OTHER_DIALOG_ID = 1;

    private class ObdInfoCallBack extends CarMdCallBack {
        private SharedPreferences customerPrefs;

        ObdInfoCallBack(SharedPreferences customerPrefs) {
            this.customerPrefs = customerPrefs;
        }

        public void processResponse(ResponseBody responseBody) {
            try {
                CarMdObdLocationResponse carMdObdLocationResponse = CarMdClient.getInstance().parseCarMdObdLocationResponse(responseBody);
                EditCarInfoUsingWebApiActivity.saveObdInfo(this.customerPrefs, carMdObdLocationResponse.note, carMdObdLocationResponse.accessNote, carMdObdLocationResponse.locationNumber);
                try {
                    if (StringUtils.isEmptyAfterTrim(carMdObdLocationResponse.accessImageURL)) {
                        goToNextStep();
                        return;
                    }
                    CarMdClient.getInstance().sendCarMdRequest(new CarMdCallBack() {
                        public void processResponse(ResponseBody response) {
                            try {
                                Tracker.saveObdImageToInternalStorage(BitmapFactory.decodeStream(response.byteStream()));
                            } catch (Exception e) {
                                EditCarInfoUsingWebApiActivity.this.logger.e("Something went wrong while trying to process the response for the OBD photo.", e);
                            } finally {
                                ObdInfoCallBack.this.goToNextStep();
                            }
                        }

                        public void processFailure(Throwable error) {
                            EditCarInfoUsingWebApiActivity.this.logger.e("Unable to parse car md obd location photo response.", error);
                            ObdInfoCallBack.this.goToNextStep();
                        }
                    }, CarMdClient.getInstance().buildCarMdRequest(HttpUrl.parse(carMdObdLocationResponse.accessImageURL), null));
                } catch (Exception e) {
                    EditCarInfoUsingWebApiActivity.this.logger.e("Something went wrong while trying to get the obd location photo.", e);
                    goToNextStep();
                }
            } catch (Exception e2) {
                processFailure(e2);
            }
        }

        public void processFailure(Throwable error) {
            EditCarInfoUsingWebApiActivity.this.logger.e("Unable to parse car md obd location response.", error);
            EditCarInfoUsingWebApiActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                    ObdInfoCallBack.this.goToNextStep();
                }
            });
        }

        private void goToNextStep() {
            final String nextStep = EditCarInfoUsingWebApiActivity.this.getIntent().getStringExtra(EditCarInfoUsingWebApiActivity.EXTRA_NEXT_STEP);
            EditCarInfoUsingWebApiActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                    if (StringUtils.equalsOrBothEmptyAfterTrim(nextStep, EditCarInfoUsingWebApiActivity.INSTALL)) {
                        EditCarInfoUsingWebApiActivity.this.startActivity(new Intent(EditCarInfoUsingWebApiActivity.this.getApplicationContext(), CheckMountActivity.class));
                        return;
                    }
                    EditCarInfoUsingWebApiActivity.this.startActivity(new Intent(EditCarInfoUsingWebApiActivity.this.getApplicationContext(), CarInfoActivity.class));
                    EditCarInfoUsingWebApiActivity.this.finish();
                }
            });
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_vehicle_car_md_info);
        initCarSelectorScreen();
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(FirstLaunch.EDIT_CAR_INFO);
    }

    public void showManualEntry(View view) {
        setContentView((int) R.layout.fle_vehicle_car_info);
        initManualEntryScreen();
    }

    public void showCarSelector(View view) {
        setContentView((int) R.layout.fle_vehicle_car_md_info);
        initCarSelectorScreen();
    }

    private void initCarSelectorScreen() {
        new ToolbarBuilder().title((int) R.string.select_your_car).build();
        final Spinner make = (Spinner) findViewById(R.id.pick_a_year);
        final Spinner year = (Spinner) findViewById(R.id.pick_a_make);
        final Spinner model = (Spinner) findViewById(R.id.pick_a_model);
        if (make == null || year == null || model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        Context context = NavdyApplication.getAppContext();
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        final String makeString = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        final String yearString = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String modelString = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        ArrayList<String> arrayList = new ArrayList<>(1);
        arrayList.add(context.getString(R.string.settings_profile_make_label));
        make.setAdapter(new StringSpinnerAdapter(make, arrayList));
        arrayList = new ArrayList<>(1);
        arrayList.add(context.getString(R.string.settings_profile_year_label));
        final StringSpinnerAdapter yearAdapter = new StringSpinnerAdapter(year, arrayList);
        year.setAdapter(yearAdapter);
        arrayList = new ArrayList<>(1);
        arrayList.add(context.getString(R.string.settings_profile_model_label));
        final StringSpinnerAdapter modelAdapter = new StringSpinnerAdapter(model, arrayList);
        model.setAdapter(modelAdapter);
        final CarMdClient carMdClient = CarMdClient.getInstance();
        showProgressDialog();
        carMdClient.getMakes(new CarMdCallBack() {
            public void processResponse(ResponseBody responseBody) {
                try {
                    String jsonString = responseBody.string();
                    EditCarInfoUsingWebApiActivity.this.logger.v("Car MD: response: " + jsonString);
                    final ArrayList<String> list = carMdClient.getListFromJsonResponse(jsonString);
                    if (list == null || list.isEmpty()) {
                        throw new Exception("Empty make list!");
                    }
                    EditCarInfoUsingWebApiActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                            Context context = NavdyApplication.getAppContext();
                            list.add(0, context.getString(R.string.settings_profile_make_label));
                            list.add(context.getString(R.string.other));
                            StringSpinnerAdapter adapter = new StringSpinnerAdapter(make, list);
                            EditCarInfoUsingWebApiActivity.this.logger.d("setting make list to: " + list);
                            make.setAdapter(adapter);
                            year.setAdapter(yearAdapter);
                            model.setAdapter(modelAdapter);
                            int selection = list.indexOf(makeString);
                            if (selection >= 0) {
                                make.setSelection(selection);
                            }
                        }
                    });
                } catch (Exception e) {
                    EditCarInfoUsingWebApiActivity.this.logger.v("Car MD: unable to get the list of makes out of the response body: " + responseBody);
                    processFailure(e);
                }
            }

            public void processFailure(final Throwable error) {
                EditCarInfoUsingWebApiActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                        BaseActivity.showLongToast(R.string.need_internet_for_this_feature, new Object[0]);
                        EditCarInfoUsingWebApiActivity.this.logger.v("Unable to lookup the list of makes from car md.", error);
                        EditCarInfoUsingWebApiActivity.this.showManualEntry(null);
                    }
                });
            }
        });
        make.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int selectedMakePosition, long id) {
                EditCarInfoUsingWebApiActivity.this.logger.d("onItemSelected selectedMakePosition: " + selectedMakePosition);
                String selectedMake = (String) make.getSelectedItem();
                if (selectedMakePosition <= 0) {
                    year.setAdapter(yearAdapter);
                    model.setAdapter(modelAdapter);
                    return;
                }
                if (selectedMakePosition >= make.getCount() - 1) {
                    EditCarInfoUsingWebApiActivity.this.showDialogAboutOther();
                }
                EditCarInfoUsingWebApiActivity.this.showProgressDialog();
                carMdClient.getYears(selectedMake, new CarMdCallBack() {
                    public void processResponse(ResponseBody responseBody) {
                        try {
                            final ArrayList<String> list = carMdClient.getListFromJsonResponse(responseBody.string());
                            if (list == null || list.isEmpty()) {
                                throw new Exception("Empty make list!");
                            }
                            EditCarInfoUsingWebApiActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                                    Context context = NavdyApplication.getAppContext();
                                    list.add(0, context.getString(R.string.settings_profile_year_label));
                                    list.add(context.getString(R.string.other));
                                    StringSpinnerAdapter adapter = new StringSpinnerAdapter(year, list);
                                    EditCarInfoUsingWebApiActivity.this.logger.d("setting year list to: " + list);
                                    year.setAdapter(adapter);
                                    model.setAdapter(modelAdapter);
                                    int selection = list.indexOf(yearString);
                                    if (selection >= 0) {
                                        year.setSelection(selection);
                                    }
                                }
                            });
                        } catch (Exception e) {
                            EditCarInfoUsingWebApiActivity.this.logger.v("Car MD: unable to get the list of years out of the response body: " + responseBody);
                            processFailure(e);
                        }
                    }

                    public void processFailure(final Throwable error) {
                        EditCarInfoUsingWebApiActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                                EditCarInfoUsingWebApiActivity.this.logger.v("Unable to lookup the list of years from car md.", error);
                                EditCarInfoUsingWebApiActivity.this.showManualEntry(null);
                            }
                        });
                    }
                });
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        final String str = modelString;
        year.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int selectedYearPosition, long id) {
                EditCarInfoUsingWebApiActivity.this.logger.d("onItemSelected selectedYearPosition: " + selectedYearPosition);
                String selectedMake = (String) make.getSelectedItem();
                int selectedMakePosition = make.getSelectedItemPosition();
                if (selectedMakePosition > 0 && selectedMakePosition < make.getCount() - 1) {
                    String selectedYear = (String) year.getSelectedItem();
                    if (selectedYearPosition <= 0) {
                        model.setAdapter(modelAdapter);
                        return;
                    }
                    if (selectedYearPosition >= year.getCount() - 1) {
                        EditCarInfoUsingWebApiActivity.this.showDialogAboutOther();
                    }
                    EditCarInfoUsingWebApiActivity.this.showProgressDialog();
                    carMdClient.getModels(selectedMake, selectedYear, new CarMdCallBack() {
                        public void processResponse(ResponseBody responseBody) {
                            try {
                                final ArrayList<String> list = carMdClient.getListFromJsonResponse(responseBody.string());
                                if (list == null || list.isEmpty()) {
                                    throw new Exception("Empty make list!");
                                }
                                EditCarInfoUsingWebApiActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                                        Context context = NavdyApplication.getAppContext();
                                        list.add(0, context.getString(R.string.settings_profile_model_label));
                                        list.add(context.getString(R.string.other));
                                        StringSpinnerAdapter adapter = new StringSpinnerAdapter(model, list);
                                        EditCarInfoUsingWebApiActivity.this.logger.d("setting model list to: " + list);
                                        model.setAdapter(adapter);
                                        int selection = list.indexOf(str);
                                        if (selection >= 0) {
                                            model.setSelection(selection);
                                        }
                                    }
                                });
                            } catch (Exception e) {
                                EditCarInfoUsingWebApiActivity.this.logger.v("Car MD: unable to get the list of makes out of the response body: " + responseBody);
                                processFailure(e);
                            }
                        }

                        public void processFailure(final Throwable error) {
                            EditCarInfoUsingWebApiActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    EditCarInfoUsingWebApiActivity.this.hideProgressDialog();
                                    EditCarInfoUsingWebApiActivity.this.logger.v("Unable to lookup the list of models from car md.", error);
                                    EditCarInfoUsingWebApiActivity.this.showManualEntry(null);
                                }
                            });
                        }
                    });
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        model.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int selectedModelPosition, long id) {
                if (selectedModelPosition > 0 && selectedModelPosition >= model.getCount() - 1) {
                    EditCarInfoUsingWebApiActivity.this.showDialogAboutOther();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    public void onSetCarMdInfoClick(View view) {
        Spinner make = (Spinner) findViewById(R.id.pick_a_year);
        Spinner year = (Spinner) findViewById(R.id.pick_a_make);
        Spinner model = (Spinner) findViewById(R.id.pick_a_model);
        if (make == null || year == null || model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        int makeInt = make.getSelectedItemPosition();
        int yearInt = year.getSelectedItemPosition();
        int modelInt = model.getSelectedItemPosition();
        String makeString = make.getSelectedItem().toString();
        String yearString = year.getSelectedItem().toString();
        String modelString = model.getSelectedItem().toString();
        if (makeInt == 0 || makeInt >= make.getCount() - 1 || yearInt == 0 || yearInt >= year.getCount() - 1 || modelInt == 0 || modelInt >= model.getCount() - 1 || StringUtils.isEmptyAfterTrim(yearString) || StringUtils.isEmptyAfterTrim(makeString) || StringUtils.isEmptyAfterTrim(modelString)) {
            BaseActivity.showLongToast(R.string.please_enter_car_info);
        } else {
            saveCarInfoAndDownloadObdInfo(customerPrefs, makeString, yearString, modelString, false);
        }
    }

    public void saveCarInfoAndDownloadObdInfo(SharedPreferences customerPrefs, String makeString, String yearString, String modelString, boolean comesFromManualEntry) {
        showProgressDialog();
        saveCarInfo(customerPrefs, makeString, yearString, modelString, comesFromManualEntry, StringUtils.equalsOrBothEmptyAfterTrim(getIntent().getStringExtra(EXTRA_NEXT_STEP), INSTALL));
        Tracker.resetPhotoAndLocation(customerPrefs);
        CarMdClient.getInstance().getObdLocation(new ObdInfoCallBack(customerPrefs), makeString, yearString, modelString);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (SettingsUtils.isUsingDefaultObdScanSetting()) {
                    SettingsUtils.setDefaultObdSettingDependingOnBlacklistAsync();
                }
            }
        }, 1);
    }

    public void onBackClick(View view) {
        finish();
    }

    private void showDialogAboutOther() {
        showSimpleDialog(1, getString(R.string.manual_entry), getString(R.string.manual_entry_dialog_description));
        showManualEntry(null);
    }

    private void initManualEntryScreen() {
        new ToolbarBuilder().title((int) R.string.select_your_car).build();
        EditText make = (EditText) findViewById(R.id.enter_make);
        EditText year = (EditText) findViewById(R.id.enter_year);
        EditText model = (EditText) findViewById(R.id.enter_model);
        if (make == null || year == null || model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        String makeString = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String yearString = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String modelString = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        make.setText(makeString);
        year.setText(yearString);
        model.setText(modelString);
    }

    public void onSetCarInfoClick(View view) {
        EditText make = (EditText) findViewById(R.id.enter_make);
        EditText year = (EditText) findViewById(R.id.enter_year);
        EditText model = (EditText) findViewById(R.id.enter_model);
        if (make == null || year == null || model == null) {
            this.logger.e("Missing layout element.");
            return;
        }
        String makeString = make.getText().toString();
        String yearString = year.getText().toString();
        String modelString = model.getText().toString();
        if (fieldsAreValid(makeString, yearString, modelString)) {
            saveCarInfoAndDownloadObdInfo(SettingsUtils.getCustomerPreferences(), makeString, yearString, modelString, true);
            return;
        }
        BaseActivity.showLongToast(R.string.please_enter_car_info);
    }

    public boolean fieldsAreValid(String makeString, String yearString, String modelString) {
        return (StringUtils.isEmptyAfterTrim(yearString) || !yearString.matches("[12][0-9]{3}") || StringUtils.isEmptyAfterTrim(makeString) || StringUtils.isEmptyAfterTrim(modelString)) ? false : true;
    }

    public static void saveCarInfo(@NonNull SharedPreferences customerPrefs, @NonNull String makeString, @NonNull String yearString, @NonNull String modelString, boolean comesFromManualEntry, boolean isInFle) {
        String oldMakeString = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String oldYearString = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String oldModelString = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        if (!(StringUtils.equalsOrBothEmptyAfterTrim(oldMakeString, makeString) && StringUtils.equalsOrBothEmptyAfterTrim(oldYearString, yearString) && StringUtils.equalsOrBothEmptyAfterTrim(oldModelString, modelString))) {
            sLogger.v("Resetting car md data.");
            Tracker.resetPhotoAndLocation(customerPrefs);
            HashMap<String, String> attributes = new HashMap<>(3);
            attributes.put(Attributes.CAR_YEAR, yearString);
            attributes.put(Attributes.CAR_MAKE, makeString);
            attributes.put(Attributes.CAR_MODEL, modelString);
            attributes.put(Attributes.DURING_FLE, isInFle ? "True" : "False");
            Tracker.tagEvent(Event.CAR_INFO_CHANGED, attributes);
        }
        long serial = customerPrefs.getLong(ProfilePreferences.SERIAL_NUM, ProfilePreferences.SERIAL_NUM_DEFAULT);
        makeString = makeString.trim();
        yearString = yearString.trim();
        modelString = modelString.trim();
        customerPrefs.edit().putString(ProfilePreferences.CAR_MAKE, makeString).putString(ProfilePreferences.CAR_YEAR, yearString).putString(ProfilePreferences.CAR_MODEL, modelString).putBoolean(ProfilePreferences.CAR_MANUAL_ENTRY, comesFromManualEntry).putLong(ProfilePreferences.SERIAL_NUM, 1 + serial).apply();
        LocalyticsManager.setProfileAttribute(Attributes.CAR_MAKE, makeString);
        LocalyticsManager.setProfileAttribute(Attributes.CAR_YEAR, yearString);
        LocalyticsManager.setProfileAttribute(Attributes.CAR_MODEL, modelString);
        LocalyticsManager.setProfileAttribute(Attributes.CAR_MANUAL_ENTRY, Boolean.toString(comesFromManualEntry));
    }

    public static void saveObdInfo(@NonNull SharedPreferences customerPrefs, @NonNull String note, @NonNull String accessNote, int locationNumber) {
        customerPrefs.edit().putString(ProfilePreferences.CAR_OBD_LOCATION_NOTE, note).putString(ProfilePreferences.CAR_OBD_LOCATION_ACCESS_NOTE, accessNote).putInt(ProfilePreferences.CAR_OBD_LOCATION_NUM, locationNumber).apply();
    }
}
