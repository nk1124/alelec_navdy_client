package com.navdy.client.app.ui.base;

import android.content.Context;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPolyline;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewRect;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapGesture.OnGestureListener;
import com.here.android.mpa.mapping.MapMarker;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.location.NavdyLocationManager.OnNavdyLocationChangedListener;
import com.navdy.client.app.framework.map.HereMapsManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.ui.settings.GeneralSettingsActivity.LimitCellDataEvent;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

public class BaseHereMapFragment extends MapFragment {
    private static final int CHECK_DIMENSIONS_INTERVAL = 100;
    private static final float DEFAULT_ORIENTATION = 0.0f;
    private static final float DEFAULT_TILT = 0.0f;
    private static final double DEFAULT_ZOOM = 14.0d;
    private static final String HERE_MAP_SCHEME = "normal.day";
    private static final String HERE_MAP_THREAD_TAG = "here-map-fragment-";
    private static final int POS_INDICATOR_Z_INDEX = NavdyApplication.getAppContext().getResources().getInteger(R.integer.position_indicator_z_index);
    private static final AtomicInteger hereMapFragmentThreadCounter = new AtomicInteger(1);
    private static final Logger logger = new Logger(BaseHereMapFragment.class);
    private static Image markerImg;
    private final Queue<OnHereMapFragmentAttached> attachCompleteListeners;
    private final Handler backgroundThread;
    private final BusProvider bus;
    private final Runnable checkDimensions = new Runnable() {
        public void run() {
            if (BaseHereMapFragment.this.hereMap == null) {
                BaseHereMapFragment.logger.e("checkDimensions, hereMap is null!");
            } else if (BaseHereMapFragment.this.hereMap.getWidth() == 0 && BaseHereMapFragment.this.hereMap.getHeight() == 0) {
                BaseHereMapFragment.this.backgroundThread.postDelayed(this, 100);
            } else {
                BaseHereMapFragment.this.currentState = State.READY;
                BaseHereMapFragment.logger.v("fragment ready, calling readyCompleteListeners");
                while (!BaseHereMapFragment.this.readyCompleteListeners.isEmpty()) {
                    ((OnHereMapFragmentReady) BaseHereMapFragment.this.readyCompleteListeners.poll()).onReady(BaseHereMapFragment.this.hereMap);
                }
            }
        }
    };
    private Error currentError;
    private MapMarker currentPositionMarker;
    private State currentState;
    @Nullable
    private PointF currentTransformCenter;
    @Nullable
    private ViewRect currentViewRect;
    private final OnEngineInitListener fragmentInitListener = new OnEngineInitListener() {
        public void onEngineInitializationCompleted(final com.here.android.mpa.common.OnEngineInitListener.Error error) {
            BaseHereMapFragment.this.backgroundThread.post(new Runnable() {
                public void run() {
                    if (error != com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
                        BaseHereMapFragment.logger.e("Could not initialize HereMapFragment: " + error.name());
                        BaseHereMapFragment.this.currentState = State.FAILED;
                        BaseHereMapFragment.this.currentError = BaseHereMapFragment.Error.MAP_ENGINE_ERROR;
                        BaseHereMapFragment.this.callOnErrors(BaseHereMapFragment.Error.MAP_ENGINE_ERROR);
                        return;
                    }
                    BaseHereMapFragment.this.hereMap = BaseHereMapFragment.this.getMap();
                    if (BaseHereMapFragment.this.hereMap == null) {
                        BaseHereMapFragment.logger.e("hereMap is null at fragment init");
                        BaseHereMapFragment.this.currentState = State.FAILED;
                        BaseHereMapFragment.this.currentError = BaseHereMapFragment.Error.MAP_ENGINE_ERROR;
                        BaseHereMapFragment.this.callOnErrors(BaseHereMapFragment.Error.NO_MAP_ERROR);
                        return;
                    }
                    BaseHereMapFragment.this.initMap();
                    BaseHereMapFragment.this.currentState = State.INITIALIZED;
                    BaseHereMapFragment.logger.v("fragment initialized, calling initCompleteListeners");
                    while (!BaseHereMapFragment.this.initCompleteListeners.isEmpty()) {
                        if (BaseHereMapFragment.this.hereMap == null) {
                            BaseHereMapFragment.logger.e("Calling initCompleteListeners.onInit with a null hereMap!");
                            break;
                        }
                        ((OnHereMapFragmentInitialized) BaseHereMapFragment.this.initCompleteListeners.poll()).onInit(BaseHereMapFragment.this.hereMap);
                    }
                    BaseHereMapFragment.this.backgroundThread.postDelayed(BaseHereMapFragment.this.checkDimensions, 100);
                }
            });
        }
    };
    private Map hereMap;
    private final Queue<OnHereMapFragmentInitialized> initCompleteListeners;
    private boolean isAttached;
    private final OnNavdyLocationChangedListener navdyLocationListener = new OnNavdyLocationChangedListener() {
        public void onPhoneLocationChanged(@NonNull final Coordinate phoneLocation) {
            BaseHereMapFragment.this.whenInitialized(new OnHereMapFragmentInitialized() {
                public void onInit(@NonNull Map hereMap) {
                    if (BaseHereMapFragment.this.currentPositionMarker == null) {
                        BaseHereMapFragment.this.currentPositionMarker = BaseHereMapFragment.this.initPositionMarker(phoneLocation);
                        if (BaseHereMapFragment.this.currentPositionMarker != null) {
                            hereMap.addMapObject(BaseHereMapFragment.this.currentPositionMarker);
                            return;
                        }
                        return;
                    }
                    BaseHereMapFragment.this.currentPositionMarker.setCoordinate(new GeoCoordinate(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue()));
                }

                public void onError(@NonNull Error error) {
                }
            });
        }

        public void onCarLocationChanged(@NonNull Coordinate carLocation) {
        }
    };
    private final NavdyLocationManager navdyLocationManager;
    private final Queue<OnHereMapFragmentReady> readyCompleteListeners;
    private final Handler uiThread = new Handler(Looper.getMainLooper());

    public interface OnHereMapFragmentReady {
        void onError(@NonNull Error error);

        void onReady(@NonNull Map map);
    }

    public interface OnHereMapFragmentInitialized {
        void onError(@NonNull Error error);

        void onInit(@NonNull Map map);
    }

    private interface OnHereMapFragmentAttached {
        void onAttached();
    }

    public enum Error {
        MAP_ENGINE_ERROR,
        NO_MAP_ERROR
    }

    private enum State {
        INITIALIZING,
        INITIALIZED,
        READY,
        FAILED
    }

    static {
        initMarkerImage();
    }

    private static void initMarkerImage() {
        HereMapsManager.getInstance().addOnInitializedListener(new OnEngineInitListener() {
            public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
                BaseHereMapFragment.markerImg = new Image();
                try {
                    BaseHereMapFragment.markerImg.setImageResource(R.drawable.icon_user_location);
                } catch (IOException e) {
                    BaseHereMapFragment.logger.e("could not initialize map marker img", e);
                }
            }
        });
    }

    public BaseHereMapFragment() {
        HandlerThread background = new HandlerThread(HERE_MAP_THREAD_TAG + hereMapFragmentThreadCounter.getAndIncrement());
        background.start();
        this.backgroundThread = new Handler(background.getLooper());
        this.navdyLocationManager = NavdyLocationManager.getInstance();
        this.bus = BusProvider.getInstance();
        this.attachCompleteListeners = new LinkedList();
        this.initCompleteListeners = new LinkedList();
        this.readyCompleteListeners = new LinkedList();
        this.currentState = State.INITIALIZING;
        this.currentError = null;
        this.isAttached = false;
    }

    public void whenInitialized(final OnHereMapFragmentInitialized initListener) {
        if (initListener == null) {
            logger.w("tried to add a null initListener");
        } else {
            this.backgroundThread.post(new Runnable() {
                public void run() {
                    switch (BaseHereMapFragment.this.currentState) {
                        case READY:
                        case INITIALIZED:
                            initListener.onInit(BaseHereMapFragment.this.hereMap);
                            return;
                        case FAILED:
                            initListener.onError(BaseHereMapFragment.this.currentError);
                            return;
                        default:
                            BaseHereMapFragment.this.initCompleteListeners.add(initListener);
                            return;
                    }
                }
            });
        }
    }

    public void whenReady(final OnHereMapFragmentReady readyListener) {
        if (readyListener == null) {
            logger.w("tried to add a null readyListener");
        } else {
            this.backgroundThread.post(new Runnable() {
                public void run() {
                    switch (BaseHereMapFragment.this.currentState) {
                        case READY:
                            BaseHereMapFragment.this.setupTransformCenter();
                            readyListener.onReady(BaseHereMapFragment.this.hereMap);
                            return;
                        case FAILED:
                            readyListener.onError(BaseHereMapFragment.this.currentError);
                            return;
                        default:
                            BaseHereMapFragment.this.readyCompleteListeners.add(readyListener);
                            return;
                    }
                }
            });
        }
    }

    public void show() {
        whenAttached(new OnHereMapFragmentAttached() {
            public void onAttached() {
                BaseHereMapFragment.logger.v("show");
                BaseHereMapFragment.this.getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).show(BaseHereMapFragment.this).commitAllowingStateLoss();
            }
        });
    }

    public void hide() {
        whenAttached(new OnHereMapFragmentAttached() {
            public void onAttached() {
                BaseHereMapFragment.logger.v("hide");
                BaseHereMapFragment.this.getFragmentManager().beginTransaction().hide(BaseHereMapFragment.this).commitAllowingStateLoss();
                BaseHereMapFragment.this.getFragmentManager().executePendingTransactions();
            }
        });
    }

    public void centerOnUserLocation() {
        centerOnUserLocation(null);
    }

    public void centerOnUserLocation(@Nullable final Animation animation) {
        whenInitialized(new OnHereMapFragmentInitialized() {
            public void onInit(@NonNull Map hereMap) {
                Coordinate phoneLocation = BaseHereMapFragment.this.navdyLocationManager.getPhoneCoordinates();
                Coordinate carLocation = BaseHereMapFragment.this.navdyLocationManager.getCarCoordinates();
                GeoCoordinate newCenter = null;
                if (phoneLocation != null) {
                    newCenter = new GeoCoordinate(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue());
                } else if (carLocation != null) {
                    newCenter = new GeoCoordinate(carLocation.latitude.doubleValue(), carLocation.longitude.doubleValue());
                }
                if (newCenter != null) {
                    BaseHereMapFragment.this.move(newCenter, animation);
                } else {
                    BaseHereMapFragment.logger.w("could not center on user location, no location");
                }
            }

            public void onError(@NonNull Error error) {
            }
        });
    }

    public void centerOnUserLocationAndDestination(@NonNull Destination destination) {
        centerOnUserLocationAndDestination(destination, null);
    }

    public void centerOnUserLocationAndDestination(@NonNull final Destination destination, @Nullable final Animation animation) {
        whenReady(new OnHereMapFragmentReady() {
            public void onReady(@NonNull Map hereMap) {
                GeoCoordinate destinationGeo;
                Coordinate location = BaseHereMapFragment.this.navdyLocationManager.getSmartStartCoordinates();
                if (MapUtils.isValidSetOfCoordinates(destination.displayLat, destination.displayLong)) {
                    destinationGeo = new GeoCoordinate(destination.displayLat, destination.displayLong);
                } else {
                    destinationGeo = new GeoCoordinate(destination.navigationLat, destination.navigationLong);
                }
                if (location != null) {
                    BaseHereMapFragment.this.centerOnTwoPoints(new GeoCoordinate(location.latitude.doubleValue(), location.longitude.doubleValue()), destinationGeo, animation);
                    return;
                }
                BaseHereMapFragment.this.move(destinationGeo, animation);
            }

            public void onError(@NonNull Error error) {
            }
        });
    }

    public void centerOnRoute(@NonNull GeoPolyline route) {
        centerOnRoute(route, null);
    }

    public void centerOnRoute(@NonNull final GeoPolyline route, @Nullable final Animation animation) {
        whenReady(new OnHereMapFragmentReady() {
            public void onReady(@NonNull Map hereMap) {
                GeoBoundingBox geoBoundingBox = route.getBoundingBox();
                Animation sanitizedAnimation = animation != null ? animation : Animation.NONE;
                if (hereMap.getHeight() > 0 && hereMap.getWidth() > 0) {
                    if (BaseHereMapFragment.this.currentViewRect != null) {
                        hereMap.zoomTo(geoBoundingBox, BaseHereMapFragment.this.currentViewRect, sanitizedAnimation, 0.0f);
                    } else {
                        hereMap.zoomTo(geoBoundingBox, sanitizedAnimation, 0.0f);
                    }
                }
            }

            public void onError(@NonNull Error error) {
            }
        });
    }

    public void setUsableArea(int paddingTop, int paddingRight, int paddingBottom, int paddingLeft) {
        final int i = paddingTop;
        final int i2 = paddingBottom;
        final int i3 = paddingLeft;
        final int i4 = paddingRight;
        whenReady(new OnHereMapFragmentReady() {
            public void onReady(@NonNull Map hereMap) {
                int width = hereMap.getWidth();
                int height = hereMap.getHeight();
                if (i + i2 >= height || i3 + i4 >= width) {
                    BaseHereMapFragment.logger.w("invalid paddings. width=" + width + " height=" + height + " paddingTop=" + i + " paddingBottom=" + i2 + " paddingLeft=" + i3 + " paddingRight=" + i4);
                    return;
                }
                PointF transformCenter = hereMap.getTransformCenter();
                PointF newTransformCenter = new PointF((transformCenter.x + ((float) ((int) (((double) i3) / 2.0d)))) - ((float) ((int) (((double) i4) / 2.0d))), (transformCenter.y + ((float) ((int) (((double) i) / 2.0d)))) - ((float) ((int) (((double) i2) / 2.0d))));
                hereMap.setTransformCenter(newTransformCenter);
                BaseHereMapFragment.this.currentViewRect = new ViewRect(i3, i, (width - i3) - i4, (height - i) - i2);
                BaseHereMapFragment.this.currentTransformCenter = newTransformCenter;
            }

            public void onError(@NonNull Error error) {
            }
        });
    }

    public void setMapGesture(final OnGestureListener listener) {
        whenInitialized(new OnHereMapFragmentInitialized() {
            public void onInit(@NonNull Map hereMap) {
                MapGesture mapGesture = BaseHereMapFragment.this.getMapGesture();
                if (mapGesture != null) {
                    // TODO the 0, false might be wrong
                    // https://developer.here.com/documentation/android-premium/api_reference_java/com/here/android/mpa/mapping/MapGesture.html
                    // https://github.com/heremaps/here-android-sdk-examples/search?q=addOnGestureListener&unscoped_q=addOnGestureListener
                    mapGesture.addOnGestureListener(listener, 0, false);
                } else {
                    BaseHereMapFragment.logger.w("could not add map gesture due MapGesture being null");
                }
            }

            public void onError(@NonNull Error error) {
                BaseHereMapFragment.logger.e("could not add map gesture due to error: " + error.name());
            }
        });
    }

    @Subscribe
    public void onLimitCellDataEvent(LimitCellDataEvent event) {
        toggleTraffic(!event.hasLimitedCellData);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        this.isAttached = true;
        this.bus.register(this);
        logger.v("onAttach, attachCompleteListeners executing");
        while (!this.attachCompleteListeners.isEmpty()) {
            ((OnHereMapFragmentAttached) this.attachCompleteListeners.poll()).onAttached();
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        HereMapsManager.getInstance().addOnInitializedListener(new OnEngineInitListener() {
            public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
                BaseHereMapFragment.this.backgroundThread.post(new Runnable() {
                    public void run() {
                        BaseHereMapFragment.this.init(BaseHereMapFragment.this.fragmentInitListener);
                    }
                });
            }
        });
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    public void onResume() {
        super.onResume();
        this.navdyLocationManager.addListener(this.navdyLocationListener);
    }

    public void onPause() {
        this.navdyLocationManager.removeListener(this.navdyLocationListener);
        super.onPause();
    }

    public void onDetach() {
        if (this.isAttached) {
            this.bus.unregister(this);
        }
        this.isAttached = false;
        super.onDetach();
    }

    private void whenAttached(final OnHereMapFragmentAttached attachListener) {
        if (attachListener == null) {
            logger.w("tried to add a null attachListener");
        } else {
            this.uiThread.post(new Runnable() {
                public void run() {
                    if (BaseHereMapFragment.this.isAdded()) {
                        attachListener.onAttached();
                    } else if (BaseHereMapFragment.this.isRemoving()) {
                        BaseHereMapFragment.logger.w("whenAttached, isRemoving, no-op");
                    } else {
                        BaseHereMapFragment.logger.v("queueing up a whenAttachListener");
                        BaseHereMapFragment.this.attachCompleteListeners.add(attachListener);
                    }
                }
            });
        }
    }

    private void centerOnTwoPoints(@NonNull GeoCoordinate startGeoCoordinates, @NonNull GeoCoordinate endGeoCoordinates, @Nullable Animation animation) {
        if (isValid(startGeoCoordinates) && isValid(endGeoCoordinates)) {
            double maxLat;
            double maxLng;
            double minLat;
            double minLng;
            double startLat = startGeoCoordinates.getLatitude();
            double startLng = startGeoCoordinates.getLongitude();
            double endLat = endGeoCoordinates.getLatitude();
            double endLng = endGeoCoordinates.getLongitude();
            if (startLat > endLat) {
                maxLat = startLat;
            } else {
                maxLat = endLat;
            }
            if (startLng > endLng) {
                maxLng = startLng;
            } else {
                maxLng = endLng;
            }
            if (startLat < endLat) {
                minLat = startLat;
            } else {
                minLat = endLat;
            }
            if (startLng < endLng) {
                minLng = startLng;
            } else {
                minLng = endLng;
            }
            GeoBoundingBox geoBoundingBox = new GeoBoundingBox(new GeoCoordinate(maxLat, minLng), new GeoCoordinate(minLat, maxLng));
            Animation appliedAnimation = animation != null ? animation : Animation.NONE;
            if (this.hereMap.getHeight() > 0 && this.hereMap.getWidth() > 0) {
                if (this.currentViewRect != null) {
                    this.hereMap.zoomTo(geoBoundingBox, this.currentViewRect, animation, 0.0f);
                    return;
                } else {
                    this.hereMap.zoomTo(geoBoundingBox, appliedAnimation, 0.0f);
                    return;
                }
            }
            return;
        }
        logger.w("called centerOnTwoPoints and coordinates are invalid, no-op. startGeoCoordinates=" + startGeoCoordinates.getLatitude() + "," + startGeoCoordinates.getLongitude() + "; endGeoCoordinates=" + endGeoCoordinates.getLatitude() + "," + endGeoCoordinates.getLongitude());
    }

    private void move(GeoCoordinate geo, @Nullable Animation animation) {
        if (animation == null) {
            animation = Animation.NONE;
        }
        this.hereMap.setCenter(geo, animation, (double) DEFAULT_ZOOM, 0.0f, 0.0f);
    }

    private void initMap() {
        this.hereMap.setMapScheme("normal.day");
        centerOnUserLocation();
        addPositionIndicator();
        toggleTraffic(!SettingsUtils.isLimitingCellularData());
    }

    private void callOnErrors(Error error) {
        while (!this.initCompleteListeners.isEmpty()) {
            ((OnHereMapFragmentInitialized) this.initCompleteListeners.poll()).onError(error);
        }
    }

    private void addPositionIndicator() {
        if (this.currentPositionMarker == null) {
            this.currentPositionMarker = initPositionMarker(this.navdyLocationManager.getPhoneCoordinates());
            if (this.currentPositionMarker != null) {
                this.hereMap.addMapObject(this.currentPositionMarker);
            }
        }
    }

    private boolean isValid(GeoCoordinate geoCoordinate) {
        return (geoCoordinate == null || (geoCoordinate.getLatitude() == 0.0d && geoCoordinate.getLongitude() == 0.0d)) ? false : true;
    }

    private void toggleTraffic(final boolean active) {
        whenInitialized(new OnHereMapFragmentInitialized() {
            public void onInit(@NonNull Map hereMap) {
                hereMap.setTrafficInfoVisible(active);
            }

            public void onError(@NonNull Error error) {
            }
        });
    }

    @Nullable
    private MapMarker initPositionMarker(@Nullable Coordinate phoneLocation) {
        if (phoneLocation == null) {
            return null;
        }
        if (markerImg == null) {
            logger.e("markerImg is null and it shouldn't be!");
            return null;
        }
        MapMarker positionMarker = new MapMarker(new GeoCoordinate(phoneLocation.latitude.doubleValue(), phoneLocation.longitude.doubleValue()), markerImg);
        positionMarker.setZIndex(POS_INDICATOR_Z_INDEX);
        return positionMarker;
    }

    private void setupTransformCenter() {
        if (this.hereMap != null && !this.hereMap.getTransformCenter().equals(this.currentTransformCenter)) {
            this.hereMap.setTransformCenter(this.currentTransformCenter);
        }
    }
}
