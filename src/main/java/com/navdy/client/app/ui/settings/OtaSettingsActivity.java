package com.navdy.client.app.ui.settings;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils.TruncateAt;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.debug.util.S3Constants.BuildSource;
import com.navdy.client.debug.util.S3Constants.BuildType;
import com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus;
import com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.client.ota.OTAUpdateService.State;
import com.navdy.client.ota.OTAUpdateServiceInterface;
import com.navdy.client.ota.OTAUpdateUIClient;
import com.navdy.client.ota.OTAUpdateUIClient.Error;
import com.navdy.client.ota.model.UpdateInfo;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class OtaSettingsActivity extends BaseEditActivity implements ServiceConnection, OTAUpdateUIClient {
    public static final String ARGUMENT_CONFIRMATION_MESSAGE = "message_confirmation";
    public static final int DIALOG_DOWNLOAD_UNSTABLE_CONFIRMATION = 2;
    public static final int DIALOG_DOWNLOAD_USING_MOBILE_DATA = 1;
    public static final int DIALOG_ERROR = 3;
    private static final int MAX_LINES_FOR_HIDDEN_DESCRIPTION = 2;
    private static final double SCREEN_PERCENTAGE_FOR_IMAGE = 0.35d;
    private static final int START_INDEX_OF_HEX_COLOR = 3;
    private TextView appVersion;
    private BuildType buildType;
    private Switch buildTypeUnstableSwitch;
    private Button button;
    private TextView description;
    private TextView downloadVersion;
    private TextView hardwareSupportTitle;
    private TextView hudVersion;
    private ImageView image;
    private AtomicBoolean isBound = new AtomicBoolean(false);
    private TextView moreOrLess;
    private OTAUpdateServiceInterface otaUpdateService;
    private ProgressBar progressBar;
    private ProgressBar progressBar2;
    private RelativeLayout progressBarContainer;
    private TextView progressData;
    private TextView progressPercentage;
    private SharedPreferences sharedPrefs;
    private boolean showingMoreInfo = false;
    private TextView subtitle;
    private TextView subtitle2;
    private TextView title;
    private boolean viewsInitialized;

//    /* renamed from: com.navdy.client.app.ui.settings.OtaSettingsActivity$9 */
//    static /* synthetic */ class AnonymousClass9 {
//        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType = new int[BuildType.values().length];
//        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus = new int[DownloadUpdateStatus.values().length];
//        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus = new int[UploadToHUDStatus.values().length];
//        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error = new int[Error.values().length];
//
//        static {
//            try {
//                $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType[BuildType.user.ordinal()] = 1;
//            } catch (NoSuchFieldError e) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$debug$util$S3Constants$BuildType[BuildType.eng.ordinal()] = 2;
//            } catch (NoSuchFieldError e2) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[UploadToHUDStatus.UPLOAD_FAILED.ordinal()] = 1;
//            } catch (NoSuchFieldError e3) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[UploadToHUDStatus.DEVICE_NOT_CONNECTED.ordinal()] = 2;
//            } catch (NoSuchFieldError e4) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[UploadToHUDStatus.UPLOADING.ordinal()] = 3;
//            } catch (NoSuchFieldError e5) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus[UploadToHUDStatus.COMPLETED.ordinal()] = 4;
//            } catch (NoSuchFieldError e6) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[DownloadUpdateStatus.DOWNLOAD_FAILED.ordinal()] = 1;
//            } catch (NoSuchFieldError e7) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[DownloadUpdateStatus.NO_CONNECTIVITY.ordinal()] = 2;
//            } catch (NoSuchFieldError e8) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[DownloadUpdateStatus.NOT_ENOUGH_SPACE.ordinal()] = 3;
//            } catch (NoSuchFieldError e9) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[DownloadUpdateStatus.DOWNLOADING.ordinal()] = 4;
//            } catch (NoSuchFieldError e10) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus[DownloadUpdateStatus.COMPLETED.ordinal()] = 5;
//            } catch (NoSuchFieldError e11) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error[Error.NO_CONNECTIVITY.ordinal()] = 1;
//            } catch (NoSuchFieldError e12) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateUIClient$Error[Error.SERVER_ERROR.ordinal()] = 2;
//            } catch (NoSuchFieldError e13) {
//            }
//            $SwitchMap$com$navdy$client$ota$OTAUpdateService$State = new int[State.values().length];
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[State.UP_TO_DATE.ordinal()] = 1;
//            } catch (NoSuchFieldError e14) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[State.UPDATE_AVAILABLE.ordinal()] = 2;
//            } catch (NoSuchFieldError e15) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[State.DOWNLOADING_UPDATE.ordinal()] = 3;
//            } catch (NoSuchFieldError e16) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[State.READY_TO_UPLOAD.ordinal()] = 4;
//            } catch (NoSuchFieldError e17) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[State.UPLOADING.ordinal()] = 5;
//            } catch (NoSuchFieldError e18) {
//            }
//            try {
//                $SwitchMap$com$navdy$client$ota$OTAUpdateService$State[State.READY_TO_INSTALL.ordinal()] = 6;
//            } catch (NoSuchFieldError e19) {
//            }
//        }
//    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_ota);
        new ToolbarBuilder().title((int) R.string.menu_navdy_display).build();
        this.title = (TextView) findViewById(R.id.title);
        this.subtitle = (TextView) findViewById(R.id.subtitle);
        this.subtitle2 = (TextView) findViewById(R.id.subtitle2);
        this.image = (ImageView) findViewById(R.id.image);
        this.progressBarContainer = (RelativeLayout) findViewById(R.id.progress_container);
        this.progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        this.progressBar2 = (ProgressBar) findViewById(R.id.progress_bar2);
        this.progressPercentage = (TextView) findViewById(R.id.progress_percentage);
        this.progressData = (TextView) findViewById(R.id.progress_data);
        this.button = (Button) findViewById(R.id.button);
        this.description = (TextView) findViewById(R.id.description);
        this.moreOrLess = (TextView) findViewById(R.id.more_or_less);
        this.hudVersion = (TextView) findViewById(R.id.hud_version);
        this.appVersion = (TextView) findViewById(R.id.app_version);
        this.buildTypeUnstableSwitch = (Switch) findViewById(R.id.build_type);
        this.hardwareSupportTitle = (TextView) findViewById(R.id.release_notes_title);
        this.downloadVersion = (TextView) findViewById(R.id.download_version);
        if (this.title == null || this.subtitle == null || this.subtitle2 == null || this.image == null || this.downloadVersion == null || this.progressBarContainer == null || this.progressBar == null || this.progressBar2 == null || this.progressPercentage == null || this.progressData == null || this.button == null || this.description == null || this.moreOrLess == null || this.hudVersion == null || this.appVersion == null || this.buildTypeUnstableSwitch == null) {
            throw new NotFoundException("Layout element");
        }
        this.sharedPrefs = SettingsUtils.getSharedPreferences();
        setBuildSources();
        this.appVersion.setText(HomescreenActivity.getAppVersionString());
        getIntent().putExtra(OTAUpdateService.EXTRA_OTA_UPDATE_UI, false);
        this.viewsInitialized = true;
        if (this.otaUpdateService != null) {
            setupView();
        }
    }

    protected void onResume() {
        super.onResume();
        connectToService();
        Tracker.tagScreen(Settings.UPDATE);
        adjustImageContainerHeight();
    }

    public void onPause() {
        super.onPause();
        if (this.otaUpdateService != null) {
            this.otaUpdateService.unregisterUIClient();
            disconnectService();
        }
    }

    public void onButtonClick(View view) {
        switch (getState()) {
            case UP_TO_DATE:
                this.otaUpdateService.checkForUpdate();
                return;
            case UPDATE_AVAILABLE:
                startDownload();
                return;
            case DOWNLOADING_UPDATE:
                this.otaUpdateService.cancelDownload();
                this.otaUpdateService.toggleAutoDownload(false);
                this.otaUpdateService.checkForUpdate();
                return;
            case READY_TO_UPLOAD:
            case UPLOADING:
                this.otaUpdateService.cancelUpload();
                this.otaUpdateService.toggleAutoDownload(false);
                this.otaUpdateService.resetUpdate();
                this.otaUpdateService.checkForUpdate();
                return;
            case READY_TO_INSTALL:
                this.otaUpdateService.resetUpdate();
                this.otaUpdateService.checkForUpdate();
                return;
            default:
                this.logger.e("Unsupported state for a button click: " + getState());
                return;
        }
    }

    public void onMoreInfoClick(View view) {
        this.showingMoreInfo = !this.showingMoreInfo;
        showReleaseNotes(this.showingMoreInfo);
    }

    private void connectToService() {
        Context context = getApplicationContext();
        Intent serviceIntent = OTAUpdateService.getServiceIntent(context);
        if (context != null) {
            this.isBound.set(context.bindService(serviceIntent, this, 1));
        }
    }

    private void disconnectService() {
        Context context = getApplicationContext();
        if (context != null && this.isBound.get()) {
            try {
                context.unbindService(this);
            } catch (Exception e) {
                this.logger.e("Unable to unbind from OTA Service.", e);
            }
        }
    }

    public void onErrorCheckingForUpdate(final Error error) {
        this.handler.post(new Runnable() {
            public void run() {
                if (OtaSettingsActivity.this.isInForeground()) {
                    OtaSettingsActivity.this.setUIState(false, OtaSettingsActivity.this.getState(), OtaSettingsActivity.this.getUpdateInfo());
                    String message = null;
                    String title = OtaSettingsActivity.this.getString(R.string.error);
                    switch (error) {
                        case NO_CONNECTIVITY:
                            if (!SystemUtils.isAirplaneModeOn(NavdyApplication.getAppContext())) {
                                message = OtaSettingsActivity.this.getString(R.string.no_connectivity);
                                break;
                            } else {
                                message = OtaSettingsActivity.this.getString(R.string.air_plane_mode);
                                break;
                            }
                        case SERVER_ERROR:
                            message = OtaSettingsActivity.this.getString(R.string.server_error);
                            break;
                    }
                    OtaSettingsActivity.this.showSimpleDialog(3, title, message);
                }
            }
        });
    }

    public void onStateChanged(final State state, final UpdateInfo updateInfo) {
        this.handler.post(new Runnable() {
            public void run() {
                if (OtaSettingsActivity.this.isInForeground()) {
                    OtaSettingsActivity.this.setUIState(false, state, updateInfo);
                }
            }
        });
    }

    public void onDownloadProgress(DownloadUpdateStatus progress, long totalDownloaded, byte percent) {
        final byte b = percent;
        final DownloadUpdateStatus downloadUpdateStatus = progress;
        final long j = totalDownloaded;
        this.handler.post(new Runnable() {
            public void run() {
                if (OtaSettingsActivity.this.isInForeground()) {
                    String title = OtaSettingsActivity.this.getString(R.string.error);
                    switch (downloadUpdateStatus) {
                        case DOWNLOAD_FAILED:
                            OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.download_failed));
                            return;
                        case NO_CONNECTIVITY:
                            if (SystemUtils.isAirplaneModeOn(NavdyApplication.getAppContext())) {
                                OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.air_plane_mode));
                                return;
                            }
                            OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.no_connectivity));
                            return;
                        case NOT_ENOUGH_SPACE:
                            OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.not_enough_space));
                            return;
                        case DOWNLOADING:
                            OtaSettingsActivity.this.setDownloadProgressPercentage(b, j);
                            return;
                        case COMPLETED:
                            OtaSettingsActivity.this.setDownloadProgressPercentage(100, j);
                            return;
                        default:
                            OtaSettingsActivity.this.logger.e("Unsupported progress: " + downloadUpdateStatus);
                            return;
                    }
                }
            }
        });
    }

    public void onUploadProgress(UploadToHUDStatus progress, long totalUploaded, byte percent) {
        final UploadToHUDStatus uploadToHUDStatus = progress;
        final byte b = percent;
        final long j = totalUploaded;
        this.handler.post(new Runnable() {
            public void run() {
                if (OtaSettingsActivity.this.isInForeground()) {
                    String title = OtaSettingsActivity.this.getString(R.string.error);
                    switch (uploadToHUDStatus) {
                        case UPLOAD_FAILED:
                            OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.upload_to_device_failed));
                            return;
                        case DEVICE_NOT_CONNECTED:
                            OtaSettingsActivity.this.showSimpleDialog(3, title, OtaSettingsActivity.this.getString(R.string.not_connected_to_device));
                            return;
                        case UPLOADING:
                            OtaSettingsActivity.this.setUploadProgressPercentage(b, j);
                            return;
                        case COMPLETED:
                            OtaSettingsActivity.this.setUploadProgressPercentage(100, j);
                            return;
                        default:
                            return;
                    }
                }
            }
        });
    }

    private void setDownloadProgressPercentage(int percentage, long totalDownloaded) {
        setThisProgressPercentage(this.progressBar, percentage, totalDownloaded);
        this.progressBar2.setProgress(1);
    }

    private void setUploadProgressPercentage(int percentage, long totalUploaded) {
        if (percentage < 1) {
            percentage = 1;
        }
        setThisProgressPercentage(this.progressBar2, percentage, totalUploaded);
        this.progressBar.setProgress(100);
    }

    private void setThisProgressPercentage(ProgressBar progressBar, int percentage, long doneSoFar) {
        progressBar.setProgress(percentage);
        this.progressPercentage.setText(String.format(Locale.getDefault(), "%1$d%%", percentage));
        UpdateInfo updateInfo = getUpdateInfo();
        if (updateInfo != null) {
            Context applicationContext = getApplicationContext();
            this.progressData.setText(String.format("%1$s / %2$s", Formatter.formatShortFileSize(applicationContext, doneSoFar), Formatter.formatShortFileSize(applicationContext, updateInfo.size)));
        }
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        if (service != null) {
            this.otaUpdateService = (OTAUpdateServiceInterface) service;
            this.otaUpdateService.registerUIClient(this);
            if (!this.otaUpdateService.isCheckingForUpdate()) {
                State state = this.otaUpdateService.getOTAUpdateState();
                if (!(state == State.UPLOADING || state == State.DOWNLOADING_UPDATE)) {
                    this.otaUpdateService.checkForUpdate();
                }
            }
            if (this.viewsInitialized) {
                this.handler.post(new Runnable() {
                    public void run() {
                        OtaSettingsActivity.this.setupView();
                    }
                });
            }
        }
    }

    public void onServiceDisconnected(ComponentName name) {
        this.otaUpdateService = null;
    }

    protected void saveChanges() {
        BaseActivity.showShortToast(R.string.settings_ota_succeeded, new Object[0]);
        Tracker.tagEvent(Event.Settings.OTA_SETTINGS_CHANGED);
    }

    private void setupView() {
        setUIState(isCheckingForUpdate(), getState(), getUpdateInfo(), getLastKnownUploadSize());
    }

    private void setUIState(boolean checkingForUpdate, State state, UpdateInfo updateInfo) {
        setUIState(checkingForUpdate, state, updateInfo, 0);
    }

    private void setUIState(boolean checkingForUpdate, State state, UpdateInfo updateInfo, long lastKnownUpdateSize) {
        sLogger.v("setUIState; checkingForUpdate=" + checkingForUpdate + "; state=" + state + "; updateInfo.versionName=" + (updateInfo != null ? updateInfo.versionName : "updateInfo is null!!!") + "; lastKnownUpdateSize=" + lastKnownUpdateSize);
        Context context = getApplicationContext();
        if (context != null) {
            this.hudVersion.setText(StringUtils.fromHtml(getHUDVersionText()));
            this.subtitle2.setVisibility(GONE);
            this.progressBarContainer.setVisibility(GONE);
            this.downloadVersion.setVisibility(GONE);
            this.hardwareSupportTitle.setVisibility(GONE);
            this.description.setVisibility(GONE);
            this.button.setEnabled(true);
            this.button.setVisibility(VISIBLE);
            setBuildSources();
            showReleaseNotes(this.showingMoreInfo);
            if (checkingForUpdate) {
                this.title.setText(R.string.checking_for_updates);
                this.subtitle.setText(R.string.contacting_navdy_servers);
                ImageUtils.loadImage(this.image, R.drawable.image_navdy_checking, null);
                this.button.setText(R.string.checking_for_update);
                this.button.setEnabled(false);
                return;
            }
            switch (state) {
                case UP_TO_DATE:
                    this.title.setText(R.string.up_to_date);
                    this.subtitle.setText(R.string.navdy_display_up_to_date);
                    this.subtitle2.setVisibility(VISIBLE);
                    this.subtitle2.setText(getCurrentHUDVersionText());
                    ImageUtils.loadImage(this.image, R.drawable.image_navdy_up_to_date, null);
                    this.button.setText(R.string.check_for_update);
                    return;
                case UPDATE_AVAILABLE:
                    this.title.setText(R.string.update_available);
                    if (updateInfo != null) {
                        String shortFileSize = Formatter.formatShortFileSize(context, updateInfo.size);
                        this.subtitle.setText(String.format(getText(R.string.update_version_text).toString(), new Object[]{UpdateInfo.getFormattedVersionName(updateInfo), shortFileSize}));
                    } else {
                        this.subtitle.setText(R.string.no_update_info);
                    }
                    this.subtitle2.setVisibility(VISIBLE);
                    this.subtitle2.setText(getCurrentHUDVersionText());
                    ImageUtils.loadImage(this.image, R.drawable.image_navdy_update_available, null);
                    this.button.setText(R.string.download_update);
                    return;
                case DOWNLOADING_UPDATE:
                    this.title.setText(R.string.downloading_update);
                    this.subtitle.setText(R.string.we_are_downloading_update);
                    this.subtitle2.setVisibility(GONE);
                    ImageUtils.loadImage(this.image, R.drawable.image_phone_downloading, null);
                    this.button.setText(R.string.cancel_download);
                    makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    this.downloadVersion.setVisibility(VISIBLE);
                    setDownloadProgressPercentage(1, 0);
                    return;
                case READY_TO_UPLOAD:
                    this.title.setText(R.string.ready_to_transfer_update);
                    this.subtitle.setText(R.string.ready_to_transfer_update_desc);
                    this.subtitle2.setVisibility(GONE);
                    ImageUtils.loadImage(this.image, R.drawable.image_ready_to_transfer, null);
                    makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    setUploadProgressPercentage(0, 0);
                    this.progressData.setText(R.string.waiting_for_navdy);
                    this.progressPercentage.setText("");
                    this.button.setText(R.string.cancel_download);
                    return;
                case UPLOADING:
                    long total = updateInfo != null ? updateInfo.size : 0;
                    setUploadProgressPercentage(total > 0 ? (int) ((((float) lastKnownUpdateSize) / ((float) total)) * 100.0f) : 0, lastKnownUpdateSize);
                    this.title.setText(R.string.transferring_update);
                    this.subtitle.setText(R.string.transferring_update_desc);
                    this.subtitle2.setVisibility(GONE);
                    ImageUtils.loadImage(this.image, R.drawable.image_transferring, null);
                    makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    setUploadProgressPercentage(0, 0);
                    this.button.setText(R.string.cancel_download);
                    return;
                case READY_TO_INSTALL:
                    this.title.setText(R.string.ready_to_install);
                    this.subtitle.setText(R.string.ready_to_install_desc);
                    this.subtitle2.setVisibility(GONE);
                    ImageUtils.loadImage(this.image, R.drawable.image_ready, null);
                    if (updateInfo != null) {
                        setUploadProgressPercentage(100, updateInfo.size);
                        makeProgressBarVisibleAndSetInfoVersion(updateInfo);
                    }
                    this.button.setText(R.string.cancel_download);
                    return;
                default:
                    return;
            }
        }
    }

    public void makeProgressBarVisibleAndSetInfoVersion(UpdateInfo updateInfo) {
        this.logger.d("makeProgressBarVisibleAndSetInfoVersion: " + updateInfo);
        if (updateInfo != null) {
            this.progressBarContainer.setVisibility(VISIBLE);
            if (VERSION.SDK_INT <= 21) {
                LayerDrawable progressBar1LayerDrawable = (LayerDrawable) this.progressBar.getProgressDrawable();
                Drawable background = progressBar1LayerDrawable.findDrawableByLayerId(R.id.progress_bar_background_1);
                Drawable progress = progressBar1LayerDrawable.findDrawableByLayerId(R.id.progress_bar_progress_1);
                LayerDrawable progressBar2LayerDrawable = (LayerDrawable) this.progressBar2.getProgressDrawable();
                Drawable background2 = progressBar2LayerDrawable.findDrawableByLayerId(R.id.progress_bar_background_2);
                Drawable progress2 = progressBar2LayerDrawable.findDrawableByLayerId(R.id.progress_bar_progress_2);
                background.setColorFilter(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.grey_2), Mode.SRC_IN);
                progress.setColorFilter(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.purple_warm), Mode.SRC_IN);
                background2.setColorFilter(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.grey_2), Mode.SRC_IN);
                progress2.setColorFilter(ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.blue), Mode.SRC_IN);
            }
            this.downloadVersion.setVisibility(VISIBLE);
            this.downloadVersion.setText(String.format("v%1$s", new Object[]{UpdateInfo.getFormattedVersionName(updateInfo)}));
        }
    }

    private State getState() {
        if (this.otaUpdateService != null) {
            return this.otaUpdateService.getOTAUpdateState();
        }
        return State.UP_TO_DATE;
    }

    @Nullable
    private UpdateInfo getUpdateInfo() {
        UpdateInfo updateInfo = null;
        if (this.otaUpdateService != null) {
            updateInfo = this.otaUpdateService.getUpdateInfo();
        }
        if (updateInfo == null) {
            updateInfo = OTAUpdateService.bReadUpdateInfo();
        }
        this.logger.v("updateInfo status: " + updateInfo);
        return updateInfo;
    }

    private long getLastKnownUploadSize() {
        if (this.otaUpdateService == null || getState() != State.UPLOADING) {
            return 0;
        }
        return this.otaUpdateService.lastKnownUploadSize();
    }

    private boolean isCheckingForUpdate() {
        return this.otaUpdateService != null && this.otaUpdateService.isCheckingForUpdate();
    }

    private String getCurrentHUDVersionText() {
        if (this.otaUpdateService == null) {
            return "";
        }
        String resourceText = getText(R.string.current_hud_version_is).toString();
        if (StringUtils.isEmptyAfterTrim(this.otaUpdateService.getHUDBuildVersionText())) {
            return getString(R.string.current_hud_version_unknown);
        }
        return String.format(resourceText, new Object[]{this.otaUpdateService.getHUDBuildVersionText()});
    }

    private String getHUDVersionText() {
        if (this.otaUpdateService == null) {
            return null;
        }
        String resourceText = getText(R.string.current_hud_version).toString();
        if (StringUtils.isEmptyAfterTrim(this.otaUpdateService.getHUDBuildVersionText())) {
            return getString(R.string.unknown);
        }
        return String.format(resourceText, new Object[]{this.otaUpdateService.getHUDBuildVersionText()});
    }

    private void startDownload() {
        Context appContext = getApplicationContext();
        if (this.otaUpdateService == null) {
            return;
        }
        if (SystemUtils.isAirplaneModeOn(appContext)) {
            showSimpleDialog(3, getString(R.string.error), getString(R.string.air_plane_mode));
        } else if (!com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(appContext)) {
            showSimpleDialog(3, getString(R.string.error), getString(R.string.no_connectivity));
        } else if (com.navdy.service.library.util.SystemUtils.isConnectedToWifi(appContext)) {
            setUIState(true, null, null);
            this.otaUpdateService.downloadOTAUpdate();
        } else {
            showSimpleDialog(1, getString(R.string.large_file_over_network), getString(R.string.large_file_over_network_desc));
        }
    }

    public Dialog createDialog(int id, Bundle arguments) {
        Dialog dialog = super.createDialog(id, arguments);
        AlertDialog alert = (AlertDialog) dialog;
        if (dialog != null) {
            switch (id) {
                case 1:
                    alert.setButton(-1, getString(R.string.ok), new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            OtaSettingsActivity.this.logger.d("Download on LTE approved");
                            OtaSettingsActivity.this.otaUpdateService.setNetworkDownloadApproval(true);
                            OtaSettingsActivity.this.otaUpdateService.downloadOTAUpdate();
                        }
                    });
                    alert.setButton(-2, getString(R.string.cancel), new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            OtaSettingsActivity.this.logger.d("Download on LTE cancelled");
                        }
                    });
                    return alert;
            }
        }
        return dialog;
    }

    private void setBuildSources() {
        boolean z = false;
        if (this.buildType == null || this.buildType != OTAUpdateService.getBuildType()) {
            sLogger.d("Setting build source view");
            BuildSource buildSource = OTAUpdateService.getBuildSource();
            sLogger.d("Build source :" + buildSource.name());
            BuildSource[] sources = OTAUpdateService.getBuildSources();
            this.buildType = OTAUpdateService.getBuildType();
            sLogger.d("Build Type :" + this.buildType.name());
            int i = 0;
            int index = 0;
            for (BuildSource source : sources) {
                if (buildSource == source) {
                    index = i;
                }
                i++;
            }
            Switch switchR = this.buildTypeUnstableSwitch;
            if (index == 0) {
                z = true;
            }
            switchR.setChecked(z);
            this.buildTypeUnstableSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    OtaSettingsActivity.this.somethingChanged = true;
                    final State state = OtaSettingsActivity.this.getState();
                    OtaSettingsActivity.sLogger.d("State of the update process :" + state.name());
                    if (isChecked) {
                        int confirmationMessage = R.string.beta_disclaimer;
                        switch (OtaSettingsActivity.this.buildType) {
                            case user:
                                confirmationMessage = R.string.beta_disclaimer;
                                break;
                            case eng:
                                confirmationMessage = R.string.unstable_disclaimer;
                                break;
                        }
                        OtaSettingsActivity.this.showQuestionDialog(R.string.warning, confirmationMessage, R.string.ok, R.string.cancel, new OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int isPositive) {
                                if (isPositive == -1) {
                                    OtaSettingsActivity.this.setUnstableSource(true);
                                    SettingsUtils.setFeatureMode(FeatureMode.FEATURE_MODE_BETA);
                                    OtaSettingsActivity.this.cancelAndRecheckForUpdate(state);
                                    return;
                                }
                                OtaSettingsActivity.this.setUnstableSource(false);
                            }
                        }, null);
                        return;
                    }
                    OtaSettingsActivity.this.setUnstableSource(false);
                    SettingsUtils.setFeatureMode(FeatureMode.FEATURE_MODE_RELEASE);
                    OtaSettingsActivity.this.cancelAndRecheckForUpdate(state);
                }
            });
        }
    }

    @SuppressLint({"CommitPrefEdits"})
    private void setUnstableSource(boolean unstable) {
        int i;
        BuildSource[] sources = OTAUpdateService.getBuildSources();
        if (unstable) {
            i = 0;
        } else {
            i = 1;
        }
        BuildSource source = sources[i];
        OTAUpdateService.persistUserPreferredBuildSource(source);
        sLogger.d("Set to unstable source ? :" + unstable);
        sLogger.d("Setting to the source  :" + source.name());
        this.sharedPrefs.edit().putString(SettingsConstants.OTA_STATUS, SettingsConstants.OTA_STATUS_DEFAULT).putInt(SettingsConstants.OTA_VERSION, -1).putString(SettingsConstants.OTA_URL, "").putString(SettingsConstants.OTA_DESCRIPTION, "").putLong(SettingsConstants.OTA_SIZE, 0).putBoolean(SettingsConstants.OTA_IS_INCREMENTAL, false).putInt(SettingsConstants.OTA_FROM_VERSION, 0).commit();
        setUIState(true, State.UP_TO_DATE, null);
        setupView();
    }

    private void cancelAndRecheckForUpdate(State state) {
        switch (state) {
            case DOWNLOADING_UPDATE:
                this.otaUpdateService.cancelDownload();
                this.otaUpdateService.checkForUpdate();
                return;
            case UPLOADING:
                this.otaUpdateService.cancelUpload();
                this.otaUpdateService.resetUpdate();
                this.otaUpdateService.checkForUpdate();
                return;
            default:
                return;
        }
    }

    private void showReleaseNotes(boolean more) {
        sLogger.d("Show release notes, more : " + more);
        try {
            String releaseNotesText = UpdateInfo.extractReleaseNotes(getUpdateInfo());
            if (!StringUtils.isEmptyAfterTrim(releaseNotesText)) {
                this.description.setText(releaseNotesText);
                this.moreOrLess.setVisibility(VISIBLE);
                this.hardwareSupportTitle.setVisibility(VISIBLE);
                this.description.setVisibility(VISIBLE);
                if (more) {
                    expandAnimationForTextView(this.description);
                    this.description.setMaxLines(Integer.MAX_VALUE);
                    this.moreOrLess.setText(R.string.less_info);
                    return;
                }
                collapseAnimationForTextView(this.description);
                this.description.setMaxLines(2);
                this.description.setEllipsize(TruncateAt.END);
                this.moreOrLess.setText(R.string.more_info);
            }
        } catch (Exception e) {
            this.logger.e("exception found");
            sLogger.d("No release notes found");
            this.moreOrLess.setVisibility(GONE);
            this.description.setVisibility(GONE);
        }
    }

    public void expandAnimationForTextView(TextView textView) {
        textView.setMaxLines(Integer.MAX_VALUE);
    }

    public void collapseAnimationForTextView(TextView textView) {
        textView.setMaxLines(2);
    }

    public void adjustImageContainerHeight() {
        try {
            DisplayMetrics displayMetrics = NavdyApplication.getAppContext().getResources().getDisplayMetrics();
            if (this.image != null) {
                LayoutParams layoutParams = (LayoutParams) this.image.getLayoutParams();
                layoutParams.height = Double.valueOf(((double) displayMetrics.heightPixels) * SCREEN_PERCENTAGE_FOR_IMAGE).intValue();
                this.image.setLayoutParams(layoutParams);
            }
        } catch (Exception e) {
            this.logger.e("There was a problem changing the layout height of the ota container: " + e);
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    public void onUnstableVersionClick(View view) {
        if (this.buildTypeUnstableSwitch != null && this.buildTypeUnstableSwitch.isEnabled()) {
            this.buildTypeUnstableSwitch.performClick();
        }
    }
}
