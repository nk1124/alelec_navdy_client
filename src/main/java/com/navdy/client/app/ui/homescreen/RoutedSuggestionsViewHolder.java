package com.navdy.client.app.ui.homescreen;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.navigation.HereRouteManager;
import com.navdy.client.app.framework.navigation.HereRouteManager.Error;
import com.navdy.client.app.framework.navigation.HereRouteManager.Listener;
import com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.debug.util.FormatUtils;
import com.navdy.service.library.log.Logger;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static android.view.View.VISIBLE;

class RoutedSuggestionsViewHolder extends SuggestionsViewHolder {
    private static final long ETA_RECOMPUTE_RATE = TimeUnit.MINUTES.toMillis(15);
    private static final long ETA_REFRESH_RATE = TimeUnit.SECONDS.toMillis(30);
    private static final int UNKNOWN_DURATION = -1;
    static Handler handler = new Handler(Looper.getMainLooper());
    private static final Logger logger = new Logger(RoutedSuggestionsViewHolder.class);
    private Runnable etaRefresher = new Runnable() {
        public void run() {
            RoutedSuggestionsViewHolder.this.updateCalendarEta();
            long now = new Date().getTime();
            if (RoutedSuggestionsViewHolder.this.isCalendarEvent() && RoutedSuggestionsViewHolder.this.getTimeWhenWeGetThere() > RoutedSuggestionsViewHolder.this.suggestion.event.endTimestamp) {
                SuggestionManager.onCalendarChanged(true);
            } else if (now > RoutedSuggestionsViewHolder.this.lastRouteCalculation + RoutedSuggestionsViewHolder.ETA_RECOMPUTE_RATE) {
                RoutedSuggestionsViewHolder.this.calculateEtas(RoutedSuggestionsViewHolder.this.suggestion);
            } else if (RoutedSuggestionsViewHolder.this.isCalendarEvent()) {
                RoutedSuggestionsViewHolder.handler.postDelayed(RoutedSuggestionsViewHolder.this.etaRefresher, RoutedSuggestionsViewHolder.ETA_REFRESH_RATE);
            }
        }
    };
    private String etaString;
    private long lastRouteCalculation = 0;
    private int routeDurationWithTraffic = -1;
    private int routeDurationWithoutTraffic = -1;
    private int routeLength;
    private Suggestion suggestion = null;

    RoutedSuggestionsViewHolder(View itemView) {
        super(itemView);
    }

    void onBindViewHolder(Suggestion suggestion) {
        this.suggestion = suggestion;
        if (this.etaString == null && this.routeLength == 0) {
            calculateEtas(suggestion);
            return;
        }
        this.secondLine.setText(this.etaString);
        this.subIllustrationText.setDistance((double) this.routeLength);
        restartRefresher();
    }

    private void calculateEtas(final Suggestion suggestion) {
        this.lastRouteCalculation = new Date().getTime();
        if (suggestion != null && suggestion.destination != null && !SettingsUtils.isLimitingCellularData()) {
            double latitude;
            double longitude;
            if (suggestion.destination.hasValidNavCoordinates()) {
                latitude = suggestion.destination.navigationLat;
                longitude = suggestion.destination.navigationLong;
            } else if (suggestion.destination.hasValidDisplayCoordinates()) {
                latitude = suggestion.destination.displayLat;
                longitude = suggestion.destination.displayLong;
            } else {
                logger.e("No location for calculateEtaForFirstDestination");
                return;
            }
            HereRouteManager.getInstance().calculateRoute(latitude, longitude, new Listener() {
                public void onPreCalculation(@NonNull RouteHandle routeHandle) {
                }

                public void onRouteCalculated(@NonNull Error error, Route route) {
                    RoutedSuggestionsViewHolder.this.handleRouteCalculated(route, suggestion);
                }
            });
        }
    }

    private void handleRouteCalculated(Route route, Suggestion suggestion) {
        if (route == null) {
            logger.e("Attempting to handleRouteCalculated on a null route !");
        } else if (suggestion == null) {
            logger.e("Attempting to handleRouteCalculated on a null suggestion !");
        } else {
            this.routeDurationWithTraffic = route.getTta(TrafficPenaltyMode.OPTIMAL, Route.WHOLE_ROUTE).getDuration();
            this.routeDurationWithoutTraffic = route.getTta(TrafficPenaltyMode.DISABLED, Route.WHOLE_ROUTE).getDuration();
            this.routeLength = route.getLength();
            updateCalendarEta();
            this.subIllustrationText.setVisibility(VISIBLE);
            this.subIllustrationText.setDistance((double) this.routeLength);
            restartRefresher();
        }
    }

    private boolean isCalendarEvent() {
        return this.suggestion.event != null;
    }

    private void restartRefresher() {
        handler.removeCallbacks(this.etaRefresher);
        handler.postDelayed(this.etaRefresher, ETA_REFRESH_RATE);
    }

    private void updateCalendarEta() {
        if (this.suggestion == null) {
            logger.w("Can't update calendar ETA on a null suggestion.");
        } else if (isCalendarEvent()) {
            Context context = NavdyApplication.getAppContext();
            if (this.routeDurationWithTraffic != -1) {
                int etaResString;
                long timeLeft = this.suggestion.event.startTimestamp - getTimeWhenWeGetThere();
                if (timeLeft > 0) {
                    etaResString = R.string.depart_within;
                    this.secondLine.setTextColor(ContextCompat.getColor(context, R.color.grey_text));
                } else {
                    etaResString = R.string.youll_arrive_late;
                    this.secondLine.setTextColor(ContextCompat.getColor(context, R.color.red));
                }
                String deltaText = FormatUtils.formatDurationFromSecondsToSecondsMinutesHours(Math.abs((int) (timeLeft / 1000)));
                if (timeLeft == 0) {
                    this.etaString = context.getString(R.string.youll_arrive_right_when_it_starts);
                } else {
                    this.etaString = context.getString(etaResString, new Object[]{deltaText});
                }
                if (((double) this.routeDurationWithTraffic) / ((double) this.routeDurationWithoutTraffic) >= 1.25d) {
                    this.secondLine.setText(context.getString(R.string.heavy_traffic, new Object[]{this.etaString}));
                    return;
                }
                this.secondLine.setText(this.etaString);
            }
        }
    }

    private long getTimeWhenWeGetThere() {
        return ((long) (this.routeDurationWithTraffic * 1000)) + new Date().getTime();
    }
}
