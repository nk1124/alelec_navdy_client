package com.navdy.client.app.ui.firstlaunch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class InstallMountFragment extends InstallCardFragment {
    public InstallMountFragment() {
        setLayoutId(R.layout.fle_install_mounts);
    }

    public void setMountType(MountType type) {
        this.mountType = type;
        updateMountShown();
    }

    public void updateMountShown() {
        if (this.rootView != null) {
            View shortMount = this.rootView.findViewById(R.id.short_mount_card);
            View tallOrMediumMount = this.rootView.findViewById(R.id.tall_or_medium_mount_card);
            if (shortMount == null || tallOrMediumMount == null) {
                this.logger.e("Missing layout elements !");
            } else if (this.mountType != null) {
                switch (this.mountType) {
                    case MEDIUM:
                    case TALL:
                        shortMount.setVisibility(GONE);
                        tallOrMediumMount.setVisibility(VISIBLE);
                        return;
                    default:
                        shortMount.setVisibility(VISIBLE);
                        tallOrMediumMount.setVisibility(GONE);
                        return;
                }
            }
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        boolean showNewShortMountAssets = false;
        this.rootView = inflater.inflate(R.layout.fle_install_mounts, container, false);
        ImageCache imageCache = null;
        InstallActivity activity = (InstallActivity) getActivity();
        if (activity != null) {
            imageCache = activity.imageCache;
        }
        if (!StringUtils.equalsOrBothEmptyAfterTrim(SettingsUtils.getSharedPreferences().getString(SettingsConstants.BOX, "Old_Box"), "Old_Box")) {
            showNewShortMountAssets = true;
        }
        ImageUtils.loadImage((ImageView) this.rootView.findViewById(R.id.illustration2), R.drawable.asset_install_medium_installed, imageCache);
        ImageView imageView = (ImageView) this.rootView.findViewById(R.id.illustration);
        int asset = R.drawable.asset_install_2016_short_installed;
        if (showNewShortMountAssets) {
            asset = R.drawable.asset_install_2017_short_installed;
        }
        ImageUtils.loadImage(imageView, asset, imageCache);
        updateMountShown();
        return this.rootView;
    }

    public String getScreen() {
        if (this.mountType == MountType.SHORT) {
            return Install.SHORT_MOUNT;
        }
        return Install.MEDIUM_TALL_MOUNT;
    }
}
