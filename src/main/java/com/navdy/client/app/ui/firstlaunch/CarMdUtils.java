package com.navdy.client.app.ui.firstlaunch;

import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.client.ota.impl.OTAUpdateManagerImpl;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

class CarMdUtils {
    private static final long CACHE_TIMEOUT = TimeUnit.MINUTES.toMillis(5);
    private static final String CRIME_PHOTO_BUCKET = "navdy-prod-release";
    private static final String CRIME_PHOTO_KEY_BASE = "obd-locations/";
    private static final String FILE_EXTRENSION = ".jpg";
    private static final int NB_CELLS_PER_LINE = 6;
    private static volatile HashMap<String, HashMap<String, HashMap<String, ObdLocation>>> cachedYearMap;
    private static Handler handler = new Handler(Looper.getMainLooper());
    private static Logger logger = new Logger(CarMdUtils.class);

    static class ObdLocation {
        String accessNote = "";
        public int location = CarInfoActivity.DEFAULT_OBD_LOCATION;
        public String note = "";

        ObdLocation(int location, String accessNote, String note) {
            this.location = location;
            this.accessNote = accessNote;
            this.note = note;
        }
    }

    CarMdUtils() {
    }

    private static String getCrimePhotoKey(String year, String make, String model) {
        return CRIME_PHOTO_KEY_BASE + getVehicleName(year, make, model) + FILE_EXTRENSION;
    }

    private static String getVehicleName(String year, String make, String model) {
        return (make.trim() + "-" + model.trim() + "-" + year.trim()).replace("\\s", "-");
    }

    static void downloadObdCrimePhoto(String year, String make, String model, final Runnable callback) {
//        final String key = getCrimePhotoKey(year, make, model);
//        new TransferUtility(OTAUpdateManagerImpl.createS3Client(), NavdyApplication.getAppContext()).download("navdy-prod-release", key, new File(NavdyApplication.getAppContext().getFilesDir() + S3Constants.S3_FILE_DELIMITER + ProfilePreferences.OBD_IMAGE_FILE_NAME)).setTransferListener(new TransferListener() {
//            public void onStateChanged(int id, TransferState state) {
//                CarMdUtils.logger.d("Trying to download OBD image. State: " + state.name());
//                if (callback == null) {
//                    return;
//                }
//                if (state == TransferState.COMPLETED || state == TransferState.FAILED || state == TransferState.PAUSED || state == TransferState.WAITING_FOR_NETWORK) {
//                    callback.run();
//                }
//            }
//
//            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//            }
//
//            public void onError(int id, Exception ex) {
//                CarMdUtils.logger.e("Error while trying to download OBD image: " + key, ex);
//            }
//        });
        if (callback != null) {
            callback.run();
        }
    }

    @WorkerThread
    static synchronized HashMap<String, HashMap<String, HashMap<String, ObdLocation>>> buildCarList() {
        HashMap<String, HashMap<String, HashMap<String, ObdLocation>>> yearMap;
        Throwable th;
        Scanner scanner = null;
        synchronized (CarMdUtils.class) {
            SystemUtils.ensureNotOnMainThread();
            if (cachedYearMap != null) {
                resetClearingTimer();
                yearMap = cachedYearMap;
            } else {
                InputStream inputStream = null;
                yearMap = new HashMap<>();
                try {
                    inputStream = NavdyApplication.getAppContext().getResources().openRawResource(R.raw.vehicles);
                    scanner = new Scanner(inputStream, "UTF-8");
                    int location;
                    if (scanner.hasNextLine()) {
                        scanner.nextLine();
                    }
                    while (scanner.hasNextLine()) {
                        String line = scanner.nextLine();
                        try {
                            if (line.contains("\"\"\"Driver Side - Left of Steering Wheel, above Hood Release\"\"\"")) {
                                line = line.replace("\"\"\"Driver Side - Left of Steering Wheel, above Hood Release\"\"\"", "Driver Side - Left of Steering Wheel above Hood Release");
                            }
                            String[] cells = line.split(",", -1);
                            if (cells.length < 6) {
                                throw new RuntimeException("Unable to parse the obd car data! Not enough cells in this line:" + line);
                            }
                            String make = cells[0];
                            String model = cells[1];
                            String year = cells[2];
                            String position = cells[3];
                            String accessNote = cells[4];
                            String note = cells[5];
                            try {
                                location = Integer.parseInt(position) - 1;
                            } catch (NumberFormatException ex) {
                                location = CarInfoActivity.DEFAULT_OBD_LOCATION;
                            }
                            ObdLocation obdLocation = new ObdLocation(location, accessNote, note);
                            HashMap<String, ObdLocation> modelMap = new HashMap<>();
                            HashMap<String, HashMap<String, ObdLocation>> makeMap = yearMap.get(year);
                            if (makeMap == null) {
                                makeMap = new HashMap<>();
                            } else {
                                modelMap = makeMap.get(make);
                                if (modelMap == null) {
                                    modelMap = new HashMap<>();
                                } else if ((modelMap.get(model)) != null) {
                                    throw new RuntimeException("Duplicates in the list of obd locations for: year[" + year + "]" + " make[" + make + "]" + " model[" + model + "]" + " location[" + location + "]" + " accessNote[" + accessNote + "]" + " note[" + note + "]");
                                }
                            }
                            modelMap.put(model, obdLocation);
                            makeMap.put(make, modelMap);
                            yearMap.put(year, makeMap);
                            IOException ioException = scanner.ioException();
                            if (ioException != null) {
                                throw new RuntimeException("Unable to parse the obd car data!", ioException);
                            }
                        } catch(Throwable th2) {
                            th2.printStackTrace();
                        }
                    }
                    IOUtils.closeStream(inputStream);
                    IOUtils.closeStream(scanner);
                    cachedYearMap = yearMap;
                    resetClearingTimer();

                } catch (Throwable th3) {
                    th = th3;
                    IOUtils.closeStream(inputStream);
                    IOUtils.closeStream(scanner);
                    th.printStackTrace();
                }
            }
        }
        return yearMap;
    }

    static String getLocalizedAccessNote(String original) {
        if (StringUtils.isEmptyAfterTrim(original)) {
            return original;
        }
        Resources res = NavdyApplication.getAppContext().getResources();
        switch (original) {
            case "covered":
                return res.getString(R.string.covered);
            case "uncovered":
                return res.getString(R.string.uncovered);
            default:
                return original;
        }
    }

    static String getLocalizedNote(String original) {
        if (StringUtils.isEmptyAfterTrim(original)) {
            return original;
        }
        Resources res = NavdyApplication.getAppContext().getResources();
        switch (original) {
            case "Driver Side - Left of Steering Wheel above Hood Release":
                return res.getString(R.string.driver_side__left_of_steering_wheel_above_hood_release);
            case "Center Compartment - Next to Hand Brake":
                return res.getString(R.string.center_compartment__next_to_hand_brake);
            case "Center Compartment - Under Armrest":
                return res.getString(R.string.center_compartment__under_armrest);
            case "Center Console  -  Below Radio & A/C Controls":
                return res.getString(R.string.center_console____below_radio__ac_controls);
            case "Center Console - Above Climate Control":
                return res.getString(R.string.center_console__above_climate_control);
            case "Center Console - Behind Ashtray":
                return res.getString(R.string.center_console__behind_ashtray);
            case "Center Console - Behind Coin Tray":
                return res.getString(R.string.center_console__behind_coin_tray);
            case "Center Console - Behind Fuse Box Cover":
                return res.getString(R.string.center_console__behind_fuse_box_cover);
            case "Center Console - Right Side Of Console":
                return res.getString(R.string.center_console__right_side_of_console);
            case "Center Console - Right Side of Radio":
                return res.getString(R.string.center_console__right_side_of_radio);
            case "Driver Side - Kick Panel Behind Fuse Box Cover":
                return res.getString(R.string.driver_side__kick_panel_behind_fuse_box_cover);
            case "Driver Side - Left Side of Center Console":
                return res.getString(R.string.driver_side__left_side_of_center_console);
            case "Driver Side - Right Side of Steering Wheel":
                return res.getString(R.string.driver_side__right_side_of_steering_wheel);
            case "Driver Side - Under Lower Left Side of Dashboard":
                return res.getString(R.string.driver_side__under_lower_left_side_of_dashboard);
            case "Driver Side - Under Lower Right Side of Dashboard":
                return res.getString(R.string.driver_side__under_lower_right_side_of_dashboard);
            case "Driver Side - Under Steering Wheel Column":
                return res.getString(R.string.driver_side__under_steering_wheel_column);
            case "Passenger Side - Under Lower Left Side of Glove Compartment":
                return res.getString(R.string.passenger_side__under_lower_left_side_of_glove_compartment);
            case "Rear Center Console -  Left of Ashtray":
                return res.getString(R.string.rear_center_console___left_of_ashtray);
            default:
                throw new RuntimeException("Unknown location: " + original);
        }
    }

    private static synchronized void resetClearingTimer() {
        synchronized (CarMdUtils.class) {
            handler.removeCallbacksAndMessages(null);
            handler.postDelayed(new Runnable() {
                public void run() {
                    CarMdUtils.clearCache();
                    System.gc();
                }
            }, CACHE_TIMEOUT);
        }
    }

    private static synchronized void clearCache() {
        synchronized (CarMdUtils.class) {
            cachedYearMap = null;
        }
    }

    static synchronized boolean hasCachedCarList() {
        boolean z;
        synchronized (CarMdUtils.class) {
            z = cachedYearMap != null;
        }
        return z;
    }

    @Nullable
    static ObdLocation getObdLocation(@Nullable String year, @Nullable String make, @Nullable String model, @Nullable HashMap<String, HashMap<String, HashMap<String, ObdLocation>>> yearMap) {
        if (make == null || year == null || model == null || yearMap == null || yearMap.size() == 0) {
            return null;
        }
        HashMap<String, HashMap<String, ObdLocation>> makeMap = (HashMap) yearMap.get(year);
        if (makeMap == null) {
            return null;
        }
        HashMap<String, ObdLocation> modelMap = (HashMap) makeMap.get(make);
        if (modelMap != null) {
            return (ObdLocation) modelMap.get(model);
        }
        return null;
    }
}
