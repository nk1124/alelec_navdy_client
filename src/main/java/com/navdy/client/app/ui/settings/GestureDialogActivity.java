package com.navdy.client.app.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.firstlaunch.VideoPlayerActivity;
import com.navdy.service.library.events.ui.Screen;
//import com.navdy.service.library.events.ui.ShowScreen.Builder;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static  com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.events.ui.ShowScreen;

public class GestureDialogActivity extends BaseActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dialog_gestures);
    }

    protected void onResume() {
        super.onResume();
        Button primaryButton = (Button) findViewById(R.id.primary_button);
        Button secondaryButton = (Button) findViewById(R.id.secondary_button);
        if (DeviceConnection.isConnected()) {
            if (primaryButton != null) {
                primaryButton.setText(R.string.try_gestures);
            }
            if (secondaryButton != null) {
                secondaryButton.setVisibility(VISIBLE);
                return;
            }
            return;
        }
        if (primaryButton != null) {
            primaryButton.setText(R.string.watch_video);
        }
        if (secondaryButton != null) {
            secondaryButton.setVisibility(GONE);
        }
    }

    public void onCloseClick(View view) {
        onBackPressed();
    }

    public void onPrimaryButtonClick(View view) {
        if (DeviceConnection.isConnected()) {
            doTryGestureClick(view);
        } else {
            doWatchVideoClick(view);
        }
    }

    public void doWatchVideoClick(View view) {
        String videoUrl = getString(R.string.gestures_video_url);
        Intent intent = new Intent(getApplicationContext(), VideoPlayerActivity.class);
        intent.putExtra(VideoPlayerActivity.EXTRA_VIDEO_URL, videoUrl);
        startActivity(intent);
    }

    public void doTryGestureClick(View view) {
        if (DeviceConnection.isConnected()) {
            DeviceConnection.postEvent( new com.navdy.service.library.events.ui.ShowScreen.Builder().screen(Screen.SCREEN_GESTURE_LEARNING).build());
            BaseActivity.showLongToast(R.string.please_look_at_the_hud);
            return;
        }
        BaseActivity.showLongToast(R.string.navdy_not_connected);
    }
}
