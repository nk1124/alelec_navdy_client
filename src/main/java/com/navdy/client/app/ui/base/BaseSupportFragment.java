package com.navdy.client.app.ui.base;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider;
import com.navdy.service.library.log.Logger;
import org.jetbrains.annotations.Nullable;

public class BaseSupportFragment extends Fragment implements DialogProvider {
    protected BaseActivity baseActivity;
    protected final Handler handler = new Handler();
    protected final Logger logger = new Logger(getClass());
    private volatile boolean mIsInForeground = false;
    private ProgressDialog progressDialog = null;

    public void onCreate(Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        super.onCreate(savedInstanceState);
    }

    public boolean requiresBus() {
        return true;
    }

    public void onPause() {
        this.logger.v("::onPause");
        super.onPause();
        this.mIsInForeground = false;
        hideProgressDialog();
        if (requiresBus()) {
            BusProvider.getInstance().unregister(this);
        }
    }

    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
        this.mIsInForeground = true;
        if (requiresBus()) {
            BusProvider.getInstance().register(this);
        }
    }

    public void onAttach(Activity activity) {
        this.logger.v("::onAttach");
        super.onAttach(activity);
        if (activity instanceof BaseActivity) {
            this.baseActivity = (BaseActivity) activity;
        }
    }

    public void onDetach() {
        this.logger.v("::onDetach");
        this.baseActivity = null;
        super.onDetach();
    }

    public void onDestroy() {
        this.logger.v("::onDestroy");
        dismissProgressDialog();
        super.onDestroy();
    }

    public boolean isInForeground() {
        return this.mIsInForeground;
    }

    public boolean isAlive() {
        return (this.baseActivity == null || this.baseActivity.isActivityDestroyed()) ? false : true;
    }

    @NonNull
    protected Point getScreenSize() {
        return BaseActivity.getScreenSize(getActivity());
    }

    public Dialog createDialog(int id, Bundle arguments) {
        return null;
    }

    public void showProgressDialog() {
        this.progressDialog = BaseActivity.showProgressDialog(getActivity(), this.progressDialog);
    }

    public void hideProgressDialog() {
        BaseActivity.hideProgressDialog(getActivity(), this.progressDialog);
    }

    public void dismissProgressDialog() {
        BaseActivity.dismissProgressDialog(getActivity(), this.progressDialog);
    }

    public void showThisFragment(@Nullable android.app.Fragment fragment) {
        showThisFragment(fragment, getActivity());
    }

    public void hideThisFragment(@Nullable android.app.Fragment fragment) {
        hideThisFragment(fragment, getActivity());
    }

    public void hideThisFragmentImmediately(@Nullable android.app.Fragment fragment) {
        hideThisFragmentImmediately(fragment, getActivity());
    }

    public static void showThisFragment(@Nullable android.app.Fragment fragment, @Nullable FragmentActivity fragmentActivity) {
        if (!BaseActivity.isEnding(fragmentActivity) && !isEnding(fragment) && !fragment.isVisible()) {
            fragmentActivity.getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).show(fragment).commit();
        }
    }

    public static void hideThisFragment(@Nullable android.app.Fragment fragment, @Nullable FragmentActivity fragmentActivity) {
        if (!BaseActivity.isEnding(fragmentActivity) && !isEnding(fragment) && !fragment.isHidden()) {
            fragmentActivity.getFragmentManager().beginTransaction().setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out).hide(fragment).commitAllowingStateLoss();
        }
    }

    private void hideThisFragmentImmediately(android.app.Fragment fragment, FragmentActivity fragmentActivity) {
        if (!BaseActivity.isEnding(fragmentActivity) && !isEnding(fragment) && !fragment.isHidden()) {
            fragmentActivity.getFragmentManager().beginTransaction().hide(fragment).commitAllowingStateLoss();
        }
    }

    public static boolean isEnding(@Nullable android.app.Fragment fragment) {
        return fragment == null || fragment.isRemoving() || fragment.isDetached();
    }

    public static boolean isEnding(@Nullable Fragment fragment) {
        return fragment == null || fragment.isRemoving() || fragment.isDetached();
    }
}
