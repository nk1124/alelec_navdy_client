package com.navdy.client.app.ui.favorites;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.util.ImageCache;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.tracking.SetDestinationTracker;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.FAVORITE_TYPE_VALUES;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.SOURCE_VALUES;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseSupportFragment;
import com.navdy.client.app.ui.favorites.FavoritesFragmentRecyclerAdapter.ItemTouchHelperCallback;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FavoritesFragment extends BaseSupportFragment {
    public FavoritesFragmentRecyclerAdapter favoritesAdapter;
    private ImageCache imageCache = new ImageCache();
    private final NavdyRouteHandler navdyRouteHandler = NavdyRouteHandler.getInstance();
    public RecyclerView recyclerView;
    public RelativeLayout splash;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.hs_fragment_favorites, container, false);
        this.recyclerView = (RecyclerView) rootView.findViewById(R.id.favorites_fragment_recycler_view);
        Context context = getContext();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(1);
        this.recyclerView.setLayoutManager(layoutManager);
        this.favoritesAdapter = new FavoritesFragmentRecyclerAdapter(context);
        this.recyclerView.setAdapter(this.favoritesAdapter);
        this.favoritesAdapter.setClickListener(new OnClickListener() {
            public void onClick(View v) {
                int position = ((Integer) v.getTag()).intValue();
                final FragmentActivity activity = FavoritesFragment.this.getActivity();
                if (v.getId() != R.id.edit) {
                    FavoritesFragment.this.logger.v("position clicked: " + position);
                    Tracker.tagEvent(Event.NAVIGATE_USING_FAVORITES);
                    if (FavoritesFragment.this.favoritesAdapter == null) {
                        FavoritesFragment.this.logger.e("Can't handle click at position " + position + " without a valid adapter");
                        return;
                    }
                    final Destination destination = FavoritesFragment.this.favoritesAdapter.getItem(position);
                    final SetDestinationTracker setDestinationTracker = SetDestinationTracker.getInstance();
                    setDestinationTracker.setSourceValue(SOURCE_VALUES.FAVORITES_LIST);
                    setDestinationTracker.setDestinationType(TYPE_VALUES.FAVORITE);
                    setDestinationTracker.setFavoriteType(FAVORITE_TYPE_VALUES.getFavoriteTypeValue(destination));
                    ((BaseActivity) activity).showRequestNewRouteDialog(new Runnable() {
                        public void run() {
                            setDestinationTracker.tagSetDestinationEvent(destination);
                            FavoritesFragment.this.navdyRouteHandler.requestNewRoute(destination);
                            if (!AppInstance.getInstance().isDeviceConnected() && (activity instanceof HomescreenActivity)) {
                                activity.onBackPressed();
                            }
                        }
                    });
                } else if (FavoritesFragment.this.favoritesAdapter != null) {
                    FavoritesEditActivity.startFavoriteEditActivity(activity, FavoritesFragment.this.favoritesAdapter.getItem(position));
                }
            }
        });
        new ItemTouchHelper(new ItemTouchHelperCallback(this.favoritesAdapter)).attachToRecyclerView(this.recyclerView);
        this.splash = (RelativeLayout) rootView.findViewById(R.id.splash);
        ImageView illustration = (ImageView) rootView.findViewById(R.id.illustration);
        if (illustration != null) {
            ImageUtils.loadImage(illustration, R.drawable.image_favorites, this.imageCache);
        }
        return rootView;
    }

    public void onResume() {
        super.onResume();
        showOrHideSplashScreen();
        if (this.favoritesAdapter != null) {
            this.favoritesAdapter.setupDynamicShortcuts();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.favoritesAdapter != null) {
            this.favoritesAdapter.close();
        }
        this.imageCache.clearCache();
    }

    public void showOrHideSplashScreen() {
        if (!BaseSupportFragment.isEnding((Fragment) this)) {
            int itemCount = 0;
            if (this.favoritesAdapter != null) {
                itemCount = this.favoritesAdapter.getItemCount();
            }
            if (this.recyclerView != null && this.splash != null) {
                if (itemCount > 0) {
                    this.recyclerView.setVisibility(VISIBLE);
                    this.splash.setVisibility(GONE);
                    return;
                }
                this.recyclerView.setVisibility(GONE);
                this.splash.setVisibility(VISIBLE);
            }
        }
    }

    public boolean requiresBus() {
        return false;
    }
}
