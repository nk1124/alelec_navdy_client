package com.navdy.client.app.ui.homescreen;

import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import com.amazonaws.services.s3.internal.Constants;
import com.alelec.navdyclient.R;
import com.navdy.service.library.log.Logger;

class ContentLoadingViewHolder extends ViewHolder {
    private Logger logger = new Logger(ContentLoadingViewHolder.class);
    private AnimationDrawable loopAnimation = null;

    ContentLoadingViewHolder(View itemView) {
        super(itemView);
        ImageView image = (ImageView) itemView.findViewById(R.id.illustration);
        if (image != null) {
            this.loopAnimation = (AnimationDrawable) image.getDrawable();
        }
    }

    void startAnimation() {
        this.logger.d("Starting animation - loopAnimation: " + this.loopAnimation + " isRunning: " + (this.loopAnimation != null ? Boolean.valueOf(this.loopAnimation.isRunning()) : Constants.NULL_VERSION_ID));
        if (this.loopAnimation != null && !this.loopAnimation.isRunning()) {
            this.loopAnimation.start();
        }
    }

    void stopAnimation() {
        this.logger.d("Stopping animation - loopAnimation: " + this.loopAnimation + " isRunning: " + (this.loopAnimation != null ? Boolean.valueOf(this.loopAnimation.isRunning()) : Constants.NULL_VERSION_ID));
        if (this.loopAnimation != null && this.loopAnimation.isRunning()) {
            this.loopAnimation.stop();
        }
    }
}
