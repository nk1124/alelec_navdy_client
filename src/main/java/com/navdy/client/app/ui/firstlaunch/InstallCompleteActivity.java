package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import java.util.HashMap;

import static android.view.View.GONE;

public class InstallCompleteActivity extends BaseToolbarActivity {
    public static final String EXTRA_COMING_FROM_SETTINGS = "comming_from_settings";
    private boolean comingFromSettings = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_install_complete);
        new ToolbarBuilder().title((int) R.string.install_complete).build();
        this.comingFromSettings = getIntent().getBooleanExtra(EXTRA_COMING_FROM_SETTINGS, false);
        if (this.comingFromSettings) {
            RelativeLayout bottomBar = (RelativeLayout) findViewById(R.id.bottom_bar);
            if (bottomBar != null) {
                bottomBar.setVisibility(GONE);
            }
            View bottomBarDivider = findViewById(R.id.bottom_bar_divider);
            if (bottomBarDivider != null) {
                bottomBarDivider.setVisibility(GONE);
            }
        }
        loadImage(R.id.img1, R.drawable.image_2017_short_overview_fw);
        loadImage(R.id.img2, R.drawable.image_install_2017_short_fw);
        loadImage(R.id.img2b, R.drawable.image_install_med_fw);
        loadImage(R.id.img3, R.drawable.image_install_2017_short_success_fw);
        loadImage(R.id.img4, R.drawable.image_install_2017_secure_fw);
        loadImage(R.id.img5, R.drawable.image_install_cords_fw);
        loadImage(R.id.img7, R.drawable.image_install_tidying_fw);
        loadImage(R.id.img8, R.drawable.image_install_2017_power_fw);
        loadImage(R.id.img9, R.drawable.image_install_dial_fw);
    }

    protected void onResume() {
        super.onResume();
        if (TextUtils.equals(SettingsUtils.getSharedPreferences().getString(SettingsConstants.BOX, "Old_Box"), "New_Box")) {
            View card = findViewById(R.id.medium_tall_mount);
            if (card != null) {
                card.setVisibility(GONE);
            }
        }
        Tracker.tagScreen(Install.FINISHED);
    }

    public void onOverviewClick(View view) {
        startInstallActivityAtThisStep(R.layout.fle_install_overview);
    }

    public void onInstallShortMountClick(View view) {
        startInstallActivityAtThisStep(R.layout.fle_install_short_mount);
    }

    public void onInstallMediumOrTallMountClick(View view) {
        startInstallActivityAtThisStep(R.layout.fle_install_medium_or_tall_mount);
    }

    public void onLensCheckClick(View view) {
        startInstallActivityAtThisStep(R.layout.fle_install_lens_check);
    }

    public void onSecuringMountClick(View view) {
        startInstallActivityAtThisStep(R.layout.fle_install_secure_mount);
    }

    public void onPickCableClick(View view) {
        startActivity(new Intent(getApplicationContext(), CablePickerActivity.class));
    }

    public void onTidyingUpClick(View view) {
        startInstallActivityAtThisStep(R.layout.fle_install_tidying_up);
    }

    public void onTurnOnClick(View view) {
        startInstallActivityAtThisStep(R.layout.fle_install_turn_on);
    }

    public void onInstallingDialClick(View view) {
        startInstallActivityAtThisStep(R.layout.fle_install_dial);
    }

    private void startInstallActivityAtThisStep(int step) {
        HashMap<String, String> attributes = new HashMap(1);
        attributes.put(InstallAttributes.VIDEO_URL, InstallActivity.getVideoUrlForStep(step));
        Tracker.tagEvent(Event.Install.VIDEO_TAPPED_ON_INSTALL_COMPLETE, attributes);
        Intent intent = new Intent(getApplicationContext(), InstallActivity.class);
        intent.putExtra("extra_step", step);
        if (this.comingFromSettings) {
            intent.putExtra(EXTRA_COMING_FROM_SETTINGS, true);
        }
        startActivity(intent);
        finish();
    }

    public void onNextClick(View view) {
        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.FINISHED_INSTALL, true).apply();
        if (this.comingFromSettings) {
            finish();
        } else if (Tracker.weHaveCarInfo()) {
            AppSetupActivity.goToAppSetup(this);
        } else {
            Intent intent = new Intent(getApplicationContext(), EditCarInfoActivity.class);
            intent.putExtra("extra_next_step", "app_setup");
            startActivity(intent);
        }
    }

    public void onLookDownClick(View view) {
        ScrollView sv = (ScrollView) findViewById(R.id.scroll_view);
        if (sv != null) {
            sv.smoothScrollBy(0, (int) view.getY());
        }
    }
}
