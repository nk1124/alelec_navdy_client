package com.navdy.client.app.ui.firstlaunch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event.Install;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.ContactUsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class InstallActivity extends BaseActivity {
    static final String EXTRA_MOUNT = "extra_mount";
    static final String EXTRA_STEP = "extra_step";
    private boolean comingFromSettings = false;
    private InstallPagerAdapter installPagerAdapter = new InstallPagerAdapter(getSupportFragmentManager());
    private boolean isFirstTimeInPager = true;
    private int startingStep = R.layout.fle_install_overview;
    private ViewPager viewPager;
    private boolean weHadCarInfo = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_install);
        this.comingFromSettings = getIntent().getBooleanExtra(InstallCompleteActivity.EXTRA_COMING_FROM_SETTINGS, false);
        this.viewPager = (ViewPager) findViewById(R.id.view_pager);
        if (this.viewPager == null) {
            this.logger.e("Missing UI element: ViewPager !!!");
            return;
        }
        this.viewPager.setOffscreenPageLimit(1);
        this.viewPager.addOnPageChangeListener(new OnPageChangeListener() {
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                InstallActivity.this.handler.removeCallbacksAndMessages(null);
                String screen = InstallActivity.this.installPagerAdapter.getScreenAtPosition(position);
                if (!StringUtils.isEmptyAfterTrim(screen)) {
                    Tracker.tagScreen(screen);
                }
                InstallActivity.sLogger.v("trigger system gc");
                System.gc();
            }

            public void onPageScrollStateChanged(int state) {
            }
        });
        String screen = this.installPagerAdapter.getScreenAtPosition(0);
        if (!StringUtils.isEmptyAfterTrim(screen)) {
            Tracker.tagScreen(screen);
        }
        this.viewPager.setAdapter(this.installPagerAdapter);
        Intent in = getIntent();
        this.startingStep = in.getIntExtra(EXTRA_STEP, R.layout.fle_install_overview);
        String mount = in.getStringExtra(EXTRA_MOUNT);
        if (!StringUtils.isEmptyAfterTrim(mount)) {
            MountType type = MountType.getMountTypeForValue(mount);
            this.installPagerAdapter.setCurrentMountType(type);
            if (type != MountType.SHORT) {
                SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
            }
        }
        if (this.startingStep == R.layout.fle_install_short_mount || this.startingStep == R.layout.fle_install_secure_mount) {
            this.installPagerAdapter.setCurrentMountType(MountType.SHORT);
        }
        if (this.startingStep == R.layout.fle_install_medium_or_tall_mount && this.installPagerAdapter.getCurrentMountType() == MountType.SHORT) {
            this.installPagerAdapter.setCurrentMountType(MountType.TALL);
        }
        if (this.startingStep >= this.installPagerAdapter.getCount()) {
            this.installPagerAdapter.setHasPickedMount(true);
        }
        this.viewPager.setCurrentItem(InstallPagerAdapter.getPositionForStep(this.startingStep, InstallPagerAdapter.getInstallFlow(this.installPagerAdapter.getCurrentMountType())));
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        if (this.weHadCarInfo != Tracker.weHaveCarInfo()) {
            this.weHadCarInfo = Tracker.weHaveCarInfo();
            this.installPagerAdapter.notifyDataSetChanged();
        }
        boolean weAreNotOnTheLastPage;
        if (this.viewPager.getCurrentItem() < this.installPagerAdapter.getCount() - 1) {
            weAreNotOnTheLastPage = true;
        } else {
            weAreNotOnTheLastPage = false;
        }
        if (this.isFirstTimeInPager && weAreNotOnTheLastPage) {
            this.isFirstTimeInPager = false;
        }
    }

    public void onBackPressed() {
        View dialog = findViewById(R.id.custom_dialog);
        if (dialog == null || dialog.getVisibility() != VISIBLE) {
            if (this.viewPager != null) {
                int currentItem = this.viewPager.getCurrentItem();
                if (this.installPagerAdapter != null && (this.installPagerAdapter.isOnStepAfterCablePicker(currentItem) || this.installPagerAdapter.isOnStepAfterLocatingObd(currentItem))) {
                    super.onBackPressed();
                    return;
                } else if (currentItem - 1 >= 0) {
                    this.viewPager.setCurrentItem(currentItem - 1);
                    return;
                }
            }
            super.onBackPressed();
            return;
        }
        dialog.setVisibility(GONE);
    }

    public void onBackClick(View view) {
        onBackPressed();
    }

    public void onWatchClick(View view) {
        startVideoPlayerForCurrentStep();
    }

    public void onNextClick(View view) {
        if (this.viewPager != null) {
            int currentItem = this.viewPager.getCurrentItem();
            if (this.installPagerAdapter.isOnStepBeforeCablePicker(currentItem)) {
                startActivity(new Intent(getApplicationContext(), CablePickerActivity.class));
            } else if (currentItem + 1 < this.installPagerAdapter.getCount()) {
                this.viewPager.setCurrentItem(currentItem + 1);
            } else if (this.comingFromSettings) {
                finish();
            } else {
                startActivity(new Intent(getApplicationContext(), InstallCompleteActivity.class));
            }
        }
    }

    public void onLocateObdClick(View view) {
        Intent intent = new Intent(getApplicationContext(), CarInfoActivity.class);
        intent.putExtra("extra_next_step", "installation_flow");
        startActivity(intent);
    }

    public void startVideoPlayerForCurrentStep() {
        startVideoPlayerForThisStep(InstallPagerAdapter.getStepForCurrentPosition(this.viewPager.getCurrentItem(), this.installPagerAdapter.getCurrentMountType()), this);
    }

    public static void startVideoPlayerForThisStep(int step, Activity activity) {
        String videoUrl = getVideoUrlForStep(step);
        if (StringUtils.isEmptyAfterTrim(videoUrl)) {
            sLogger.e("Unable to start video player. the video url is null !");
            return;
        }
        Intent intent = new Intent(NavdyApplication.getAppContext(), VideoPlayerActivity.class);
        intent.putExtra(VideoPlayerActivity.EXTRA_VIDEO_URL, videoUrl);
        activity.startActivity(intent);
    }

    @Nullable
    public static String getVideoUrlForStep(@LayoutRes int step) {
        int videoUrlIndex;
        switch (step) {
            case R.layout.fle_install_dial /*2130903150*/:
                videoUrlIndex = 7;
                break;
            case R.layout.fle_install_locate_obd /*2130903153*/:
                videoUrlIndex = 4;
                break;
            case R.layout.fle_install_medium_or_tall_mount /*2130903154*/:
                videoUrlIndex = 2;
                break;
            case R.layout.fle_install_overview /*2130903156*/:
                videoUrlIndex = 0;
                break;
            case R.layout.fle_install_secure_mount /*2130903157*/:
                videoUrlIndex = 3;
                break;
            case R.layout.fle_install_short_mount /*2130903158*/:
                videoUrlIndex = 1;
                break;
            case R.layout.fle_install_tidying_up /*2130903159*/:
                videoUrlIndex = 5;
                break;
            case R.layout.fle_install_turn_on /*2130903160*/:
                videoUrlIndex = 6;
                break;
            default:
                videoUrlIndex = -1;
                break;
        }
        Context context = NavdyApplication.getAppContext();
        if (context == null) {
            sLogger.e("Unable to start video player. context is null !");
            return null;
        }
        Resources resources = context.getResources();
        if (resources == null) {
            sLogger.e("Unable to start video player. resources are null !");
            return null;
        }
        String[] videoUrls = resources.getStringArray(R.array.video_urls);
        if (!StringUtils.equalsOrBothEmptyAfterTrim(SettingsUtils.getSharedPreferences().getString(SettingsConstants.BOX, "Old_Box"), "Old_Box")) {
            videoUrls = resources.getStringArray(R.array.video_urls_2017);
        }
        if (videoUrls.length > 0 && videoUrlIndex >= 0 && videoUrlIndex <= videoUrls.length) {
            return videoUrls[videoUrlIndex];
        }
        sLogger.e("Unable to start video player. the video url array is null !");
        return null;
    }

    public void onMyLensIsTooHighClick(View view) {
        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
        switch (this.installPagerAdapter.getCurrentMountType()) {
            case SHORT:
                showDialogForTooHighWithLowMount();
                return;
            case MEDIUM:
            case TALL:
                showDialogForTooHighWithMediumTallMount();
                return;
            default:
                return;
        }
    }

    public void onMyLensIsOkClick(View view) {
        this.installPagerAdapter.setHasPickedMount(true);
        if (SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, false)) {
            showDialogForWhatEndedUpWorking();
            return;
        }
        switch (this.installPagerAdapter.getCurrentMountType()) {
            case SHORT:
                onShortWorkedClick(view);
                return;
            case MEDIUM:
                onMediumWorkedClick(view);
                return;
            case TALL:
                onTallWorkedClick(view);
                return;
            default:
                return;
        }
    }

    public void onMyLensIsTooLowClick(View view) {
        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
        switch (this.installPagerAdapter.getCurrentMountType()) {
            case SHORT:
                showDialogForTooLowWithLowMount();
                return;
            case MEDIUM:
            case TALL:
                showDialogForTooLowWithMediumTallMount();
                return;
            default:
                return;
        }
    }

    public void hideCustomDialog() {
        View dialog = findViewById(R.id.custom_dialog);
        if (dialog != null) {
            dialog.setVisibility(GONE);
        }
    }

    public void showDialogForTooHighWithLowMount() {
        showThisDialog(R.string.is_the_lens_too_high, R.string.try_raising_seat, true, false, false);
    }

    public void showDialogForTooLowWithLowMount() {
        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
        Intent intent = new Intent(getApplicationContext(), MountPickerActivity.class);
        intent.putExtra(MountPickerActivity.EXTRA_USE_CASE, 1);
        startActivity(intent);
    }

    public void showDialogForTooHighWithMediumTallMount() {
        SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.SHOULD_ASK_WHAT_MOUNT_WORKED, true).apply();
        Intent intent = new Intent(getApplicationContext(), MountPickerActivity.class);
        intent.putExtra(MountPickerActivity.EXTRA_USE_CASE, 2);
        startActivity(intent);
    }

    public void showDialogForTooLowWithMediumTallMount() {
        showThisDialog(R.string.is_the_lens_too_low, R.string.if_medium_try_tall_if_tall_contact_us, true, true, false);
    }

    public void showDialogForWhatEndedUpWorking() {
        showThisDialog(R.string.what_ended_up_working, R.string.what_ended_up_working_desc, false, false, false, true, true, true, false);
    }

    public void showThisDialog(int titleResId, int descResId, boolean showContactUs, boolean showMediumTallMount, boolean showShortMount) {
        showThisDialog(titleResId, descResId, showContactUs, showMediumTallMount, showShortMount, false, false, false, true);
    }

    public void showThisDialog(int titleResId, int descResId, boolean showContactUs, boolean showMediumTallMount, boolean showShortMount, boolean showShortWorked, boolean showMediumWorked, boolean showTallWorked, boolean showCancel) {
        View dialog = findViewById(R.id.custom_dialog);
        if (dialog != null) {
            dialog.setVisibility(VISIBLE);
        }
        TextView title = (TextView) findViewById(R.id.custom_dialog_title);
        if (title != null) {
            title.setVisibility(VISIBLE);
            title.setText(titleResId);
        }
        TextView desc = (TextView) findViewById(R.id.custom_dialog_desc);
        if (desc != null) {
            desc.setVisibility(VISIBLE);
            desc.setText(descResId);
        }
        updateThisButton(R.id.short_mount, showShortMount);
        updateThisButton(R.id.medium_tall_mount, showMediumTallMount);
        updateThisButton(R.id.contact_support, showContactUs);
        updateThisButton(R.id.cancel, showCancel);
        updateThisButton(R.id.short_worked, showShortWorked);
        updateThisButton(R.id.medium_worked, showMediumWorked);
        updateThisButton(R.id.tall_worked, showTallWorked);
    }

    public void updateThisButton(int buttonResId, boolean show) {
        Button contactSupport = (Button) findViewById(buttonResId);
        if (contactSupport != null) {
            contactSupport.setVisibility(show ? VISIBLE : GONE);
        }
    }

    public void onContactSupportClick(View view) {
        hideCustomDialog();
        Intent intent = new Intent(getApplicationContext(), ContactUsActivity.class);
        intent.putExtra(ContactUsActivity.EXTRA_DEFAULT_PROBLEM_TYPE, 1);
        startActivity(intent);
    }

    public void onMediumTallMountClick(View view) {
        onMountClick(MountType.TALL);
    }

    public void onShortMountClick(View view) {
        onMountClick(MountType.SHORT);
    }

    private void onMountClick(MountType mountType) {
        setMountTypeAndForceRefreshAdapterOnLensCheck(mountType);
        onBackClick(null);
    }

    public void onCancelClick(View view) {
        hideCustomDialog();
    }

    public void onShortWorkedClick(View view) {
        Tracker.tagScreen(Install.USER_PICKED_SHORT_MOUNT);
        onWorkedClick(MountType.SHORT);
    }

    public void onMediumWorkedClick(View view) {
        Tracker.tagScreen(Install.USER_PICKED_MEDIUM_MOUNT);
        onWorkedClick(MountType.MEDIUM);
    }

    public void onTallWorkedClick(View view) {
        Tracker.tagScreen(Install.USER_PICKED_TALL_MOUNT);
        onWorkedClick(MountType.TALL);
    }

    private void onWorkedClick(MountType mountType) {
        setMountTypeAndForceRefreshAdapterOnLensCheck(mountType);
        onNextClick(null);
    }

    private void setMountTypeAndForceRefreshAdapterOnLensCheck(MountType mountType) {
        hideCustomDialog();
        this.installPagerAdapter = new InstallPagerAdapter(getSupportFragmentManager());
        this.installPagerAdapter.setCurrentMountType(mountType);
        this.viewPager.setAdapter(this.installPagerAdapter);
        this.viewPager.setCurrentItem(2);
    }

    public static boolean userHasFinishedInstall() {
        return SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.FINISHED_INSTALL, false);
    }
}
