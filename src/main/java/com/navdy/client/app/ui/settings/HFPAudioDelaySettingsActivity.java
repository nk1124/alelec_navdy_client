package com.navdy.client.app.ui.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.customviews.MultipleChoiceLayout;
import com.navdy.client.app.ui.customviews.MultipleChoiceLayout.ChoiceListener;
import javax.inject.Inject;

public class HFPAudioDelaySettingsActivity extends BaseEditActivity implements OnClickListener {
    @InjectView(R.id.play_sequence)
    Button btnPlaySequence;
    @InjectView(R.id.sequence_delay_choice_layout)
    MultipleChoiceLayout multipleChoiceLayout;
    private String playingSequenceStatus;
    @Inject
    SharedPreferences sharedPreferences;
    @Inject
    TTSAudioRouter ttsAudioRouter;
    private int userPreferredDelayLevel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.inject(NavdyApplication.getAppContext(), this);
        this.playingSequenceStatus = getString(R.string.playing_sequence);
        setContentView((int) R.layout.hfp_delay_settings_layout);
        new ToolbarBuilder().title((int) R.string.speech_delay).build();
        ButterKnife.inject((Object) this, (Activity) this);
        this.btnPlaySequence.setOnClickListener(this);
        this.userPreferredDelayLevel = this.sharedPreferences.getInt(SettingsConstants.AUDIO_HFP_DELAY_LEVEL, 1);
        this.multipleChoiceLayout.setSelectedIndex(this.userPreferredDelayLevel);
        this.multipleChoiceLayout.setChoiceListener(new ChoiceListener() {
            public void onChoiceSelected(String text, int index) {
                HFPAudioDelaySettingsActivity.sLogger.d("User selected delay level " + index);
                HFPAudioDelaySettingsActivity.this.userPreferredDelayLevel = index;
                HFPAudioDelaySettingsActivity.this.ttsAudioRouter.setPreferredHFPDelayLevel(HFPAudioDelaySettingsActivity.this.userPreferredDelayLevel);
                HFPAudioDelaySettingsActivity.this.somethingChanged = true;
            }
        });
    }

    public void onClick(View view) {
        if (view == this.btnPlaySequence) {
            this.ttsAudioRouter.playSpeechDelaySequence();
            AudioDialogActivity.startAudioStatusActivity(this, this.playingSequenceStatus);
        }
    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, HFPAudioDelaySettingsActivity.class));
    }

    protected void saveChanges() {
        this.sharedPreferences.edit().putInt(SettingsConstants.AUDIO_HFP_DELAY_LEVEL, this.userPreferredDelayLevel).apply();
    }
}
