package com.navdy.client.app.ui.trips;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Trip;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.ui.base.BaseSupportFragment;
import java.util.Date;
import java.util.Locale;

public class TripsFragment extends BaseSupportFragment {
    private CursorAdapter cursorAdaptor;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.hs_fragment_trips, container, false);
        this.cursorAdaptor = new CursorAdapter(getContext(), NavdyContentProvider.getTripsCursor(), true) {
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                return LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_2, parent, false);
            }

            public void bindView(View view, Context context, Cursor cursor) {
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);
                text1.setTypeface(null, Typeface.BOLD);
                Trip trip = NavdyContentProvider.getTripsItemAt(cursor, cursor.getPosition());
                Destination destination = NavdyContentProvider.getThisDestination(trip.destinationId);
                if (destination != null) {
                    String str = "%s%n(%s)";
                    Object[] objArr = new Object[2];
                    objArr[0] = destination.isFavoriteDestination() ? destination.getFavoriteLabel() : destination.name;
                    objArr[1] = destination.getAddressForDisplay();
                    text1.setText(String.format(str, objArr));
                } else {
                    text1.setText(String.format("Unable to find destination: %s", new Object[]{Integer.valueOf(trip.destinationId)}));
                }
                long startTime = trip.startTime;
                long endTime = trip.endTime;
                text2.setText(String.format(Locale.getDefault(), "Start: %s (odo: %d)%n         %d + %d%n          lat: %f long: %f%nend: %s (odo: %d)%n          lat: %f long: %f%narrived: %d | trip number: %d | id: %d", new Object[]{TripsFragment.this.getDate(startTime), Integer.valueOf(trip.startOdometer), Long.valueOf(startTime), Integer.valueOf(trip.offset), Double.valueOf(trip.startLat), Double.valueOf(trip.startLong), TripsFragment.this.getDate(endTime), Integer.valueOf(trip.endOdometer), Double.valueOf(trip.endLat), Double.valueOf(trip.endLong), Long.valueOf(trip.arrivedAtDestination), Long.valueOf(trip.tripNumber), Integer.valueOf(trip.id)}));
            }
        };
        ((ListView) rootView.findViewById(R.id.list)).setAdapter(this.cursorAdaptor);
        return rootView;
    }

    public String getDate(long milliseconds) {
        if (milliseconds > 0) {
            return new Date(milliseconds).toString();
        }
        return "never";
    }

    public void onDestroy() {
        this.cursorAdaptor.changeCursor(null);
        super.onDestroy();
    }

    public boolean requiresBus() {
        return false;
    }
}
