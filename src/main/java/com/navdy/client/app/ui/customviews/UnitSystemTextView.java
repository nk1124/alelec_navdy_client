package com.navdy.client.app.ui.customviews;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.i18n.I18nManager;
import com.navdy.client.app.framework.i18n.I18nManager.Listener;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem;
import com.navdy.service.library.log.Logger;

public class UnitSystemTextView extends AppCompatTextView implements Listener {
    private static final Logger logger = new Logger(UnitSystemTextView.class);
    private final Context context;
    private UnitSystem currentUnitSystem;
    private double currentValue;
    private final I18nManager i18nManager;

    public UnitSystemTextView(Context context) {
        this(context, null, 0);
    }

    public UnitSystemTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UnitSystemTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.i18nManager = I18nManager.getInstance();
        this.currentUnitSystem = this.i18nManager.getUnitSystem();
        this.i18nManager.addListener(this);
    }

    public void setDistance(double meters) {
        String convertedValue;
        if (this.currentUnitSystem == UnitSystem.UNIT_SYSTEM_METRIC) {
            convertedValue = Double.toString(round(0.001d * meters));
            setText(this.context.getString(R.string.kilometers_abbrev, new Object[]{convertedValue}));
        } else {
            convertedValue = Double.toString(round(6.21371E-4d * meters));
            setText(this.context.getString(R.string.miles_abbrev, new Object[]{convertedValue}));
        }
        this.currentValue = meters;
    }

    public void onUnitSystemChanged(UnitSystem unitSystem) {
        logger.v("unit system changed to " + unitSystem.name() + ", setting new value");
        this.currentUnitSystem = unitSystem;
        setDistance(this.currentValue);
    }

    protected void onDetachedFromWindow() {
        this.i18nManager.removeListener(this);
        super.onDetachedFromWindow();
    }

    private double round(double value) {
        return ((double) Math.round(value * 10.0d)) / 10.0d;
    }

    public boolean isInEditMode() {
        return false;
    }
}
