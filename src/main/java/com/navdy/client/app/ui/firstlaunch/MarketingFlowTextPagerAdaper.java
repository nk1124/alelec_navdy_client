package com.navdy.client.app.ui.firstlaunch;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alelec.navdyclient.R;

public class MarketingFlowTextPagerAdaper extends PagerAdapter {
    private int[] descs = new int[0];
    private LayoutInflater mLayoutInflater;
    private OnClickListener onClickListener;
    private int[] titles = new int[0];

    public MarketingFlowTextPagerAdaper(Context context, int[] titles, int[] descs, OnClickListener onClickListener) {
        this.titles = (int[]) titles.clone();
        this.descs = (int[]) descs.clone();
        this.onClickListener = onClickListener;
        this.mLayoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.titles.length;
    }

    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = this.mLayoutInflater.inflate(R.layout.fle_marketing_flow_bottom_text, container, false);
        if (itemView == null) {
            return null;
        }
        itemView.setOnClickListener(this.onClickListener);
        TextView title = (TextView) itemView.findViewById(R.id.title);
        if (title != null) {
            title.setText(this.titles[position]);
        }
        TextView desc = (TextView) itemView.findViewById(R.id.desc);
        if (desc != null) {
            desc.setText(this.descs[position]);
        }
        container.addView(itemView);
        return itemView;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
