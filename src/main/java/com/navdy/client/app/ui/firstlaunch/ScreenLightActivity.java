package com.navdy.client.app.ui.firstlaunch;

import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.FlashlightManager;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.service.library.log.Logger;

public class ScreenLightActivity extends BaseActivity {
    private static final String WAKE_LOCK_TAG = "TORCH_WAKE_LOCK";
    private Logger logger = new Logger(ScreenLightActivity.class);
    private WakeLock wakeLock;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.screen_light);
    }

    protected void onResume() {
        super.onResume();
        startWakeLock();
        FlashlightManager.getInstance().setScreenToFullBrightness(getWindow());
        Tracker.tagScreen(FirstLaunch.SCREEN_LIGHT);
    }

    protected void onPause() {
        FlashlightManager.getInstance().setScreenBackToNormalBrightness(getWindow());
        stopWakeLock();
        super.onPause();
    }

    private void startWakeLock() {
        if (this.wakeLock == null) {
            this.logger.d("Getting a new WakeLock");
            PowerManager pm = (PowerManager) getSystemService("power");
            this.logger.d("PowerManager acquired");
            this.wakeLock = pm.newWakeLock(1, WAKE_LOCK_TAG);
            this.logger.d("WakeLock set");
        }
        this.wakeLock.acquire();
        this.logger.d("WakeLock acquired");
    }

    private void stopWakeLock() {
        if (this.wakeLock != null) {
            this.wakeLock.release();
            this.logger.d("WakeLock released");
        }
    }

    public void onScreenTap(View view) {
        finish();
    }
}
