package com.navdy.client.app.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

public class CustomSpinner extends Spinner {
    boolean spinnerHasBeenOpened;
    OnSpinnerStateChangedListener spinnerStateListener;

    public interface OnSpinnerStateChangedListener {
        void spinnerClosed();

        void spinnerOpened();
    }

    public CustomSpinner(Context context) {
        super(context);
    }

    public CustomSpinner(Context context, int mode) {
        super(context, mode);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean performClick() {
        this.spinnerHasBeenOpened = true;
        if (this.spinnerStateListener != null) {
            this.spinnerStateListener.spinnerOpened();
        }
        return super.performClick();
    }

    public void spinnerClosed() {
        this.spinnerHasBeenOpened = false;
        if (this.spinnerStateListener != null) {
            this.spinnerStateListener.spinnerClosed();
        }
    }

    public boolean hasBeenOpened() {
        return this.spinnerHasBeenOpened;
    }

    public void setListener(OnSpinnerStateChangedListener spinnerStateListener) {
        this.spinnerStateListener = spinnerStateListener;
    }
}
