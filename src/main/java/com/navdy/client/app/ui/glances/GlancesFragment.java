package com.navdy.client.app.ui.glances;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AlphaAnimation;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseSupportFragment;
import com.navdy.client.app.ui.homescreen.GlanceDialogActivity;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class GlancesFragment extends BaseSupportFragment {
    private static final float DISABLED_ALPHA_LEVEL = 0.5f;
    public static final boolean VERBOSE = false;
    private Switch allowGlances;
    private OnClickListener enableGlancesLearnMoreOnClickListener = new OnClickListener() {
        public void onClick(View view) {
            GlancesFragment.this.startActivity(new Intent(GlancesFragment.this.getContext(), GlanceDialogActivity.class));
        }
    };
    private boolean glancesAreEnabled = false;
    private boolean glancesAreReadAloud = true;
    private OnCheckedChangeListener glancesOnCheckedChangeListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                GlancesFragment.this.showEnableGlancesDialog();
            } else {
                GlancesFragment.this.setGlances(Boolean.valueOf(false));
            }
        }
    };
    private boolean glancesShowContent = false;
    private RadioButton readAloud;
    private RadioButton readAloudAndShowContent;
    private LinearLayout readAloudAndShowContentLayout;
    private OnClickListener readAloudAndShowContentOnClickListener = new OnClickListener() {
        public void onClick(View v) {
            boolean z;
            boolean z2 = true;
            switch (v.getId()) {
                case R.id.show_content_layout /*2131755714*/:
                case R.id.show_content /*2131755716*/:
                    GlancesFragment.this.glancesAreReadAloud = false;
                    GlancesFragment.this.glancesShowContent = true;
                    break;
                case R.id.read_aloud_and_show_content_layout /*2131755717*/:
                case R.id.read_aloud_and_show_content /*2131755719*/:
                    GlancesFragment.this.glancesAreReadAloud = true;
                    GlancesFragment.this.glancesShowContent = true;
                    break;
                default:
                    GlancesFragment.this.glancesAreReadAloud = true;
                    GlancesFragment.this.glancesShowContent = false;
                    break;
            }
            RadioButton access$500 = GlancesFragment.this.readAloud;
            if (!GlancesFragment.this.glancesAreReadAloud || GlancesFragment.this.glancesShowContent) {
                z = false;
            } else {
                z = true;
            }
            access$500.setChecked(z);
            access$500 = GlancesFragment.this.showContent;
            if (GlancesFragment.this.glancesAreReadAloud || !GlancesFragment.this.glancesShowContent) {
                z = false;
            } else {
                z = true;
            }
            access$500.setChecked(z);
            RadioButton access$700 = GlancesFragment.this.readAloudAndShowContent;
            if (!(GlancesFragment.this.glancesAreReadAloud && GlancesFragment.this.glancesShowContent)) {
                z2 = false;
            }
            access$700.setChecked(z2);
            new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... params) {
                    GlanceUtils.saveGlancesConfigurationChanges(SettingsConstants.GLANCES_READ_ALOUD, GlancesFragment.this.glancesAreReadAloud);
                    GlanceUtils.saveGlancesConfigurationChanges(SettingsConstants.GLANCES_SHOW_CONTENT, GlancesFragment.this.glancesShowContent);
                    GlancesFragment.this.increaseSerialAndSendAllGlancesPreferencesToHud();
                    return null;
                }
            }.execute(new Void[0]);
        }
    };
    private LinearLayout readAloudLayout;
    private View rootView;
    private RadioButton showContent;
    private LinearLayout showContentLayout;
    private OnClickListener testGlanceButtonOnClickListener = new OnClickListener() {
        public void onClick(View view) {
            GlanceUtils.sendTestGlance((BaseActivity) GlancesFragment.this.getActivity(), GlancesFragment.this.logger);
        }
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = inflater.inflate(R.layout.hs_fragment_glances, container, false);
        SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
        if (sharedPrefs != null) {
            this.glancesAreEnabled = sharedPrefs.getBoolean(SettingsConstants.GLANCES, SettingsConstants.GLANCES_ENABLED_DEFAULT);
            this.glancesAreReadAloud = sharedPrefs.getBoolean(SettingsConstants.GLANCES_READ_ALOUD, SettingsConstants.GLANCES_READ_ALOUD_DEFAULT);
            this.glancesShowContent = sharedPrefs.getBoolean(SettingsConstants.GLANCES_SHOW_CONTENT, SettingsConstants.GLANCES_SHOW_CONTENT_DEFAULT);
        }
        this.allowGlances = (Switch) this.rootView.findViewById(R.id.allow_glances_switch);
        this.allowGlances.setChecked(this.glancesAreEnabled);
        this.allowGlances.setOnCheckedChangeListener(this.glancesOnCheckedChangeListener);
        this.readAloudLayout = (LinearLayout) this.rootView.findViewById(R.id.read_aloud_layout);
        this.readAloud = (RadioButton) this.rootView.findViewById(R.id.read_aloud);
        this.readAloudLayout.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.readAloud.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.showContentLayout = (LinearLayout) this.rootView.findViewById(R.id.show_content_layout);
        this.showContent = (RadioButton) this.rootView.findViewById(R.id.show_content);
        this.showContentLayout.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.showContent.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.readAloudAndShowContentLayout = (LinearLayout) this.rootView.findViewById(R.id.read_aloud_and_show_content_layout);
        this.readAloudAndShowContent = (RadioButton) this.rootView.findViewById(R.id.read_aloud_and_show_content);
        this.readAloudAndShowContentLayout.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        this.readAloudAndShowContent.setOnClickListener(this.readAloudAndShowContentOnClickListener);
        TextView glancesSwitchDescription = (TextView) this.rootView.findViewById(R.id.glances_switch_desc);
        glancesSwitchDescription.setText(StringUtils.fromHtml((int) R.string.allow_glances_description));
        glancesSwitchDescription.setOnClickListener(this.enableGlancesLearnMoreOnClickListener);
        ((TextView) this.rootView.findViewById(R.id.test_glance)).setOnClickListener(this.testGlanceButtonOnClickListener);
        initAllSwitches();
        RelativeLayout notificationGlances = (RelativeLayout) this.rootView.findViewById(R.id.notification_glances);
        if (notificationGlances != null) {
            notificationGlances.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (GlancesFragment.this.glancesAreEnabled) {
                        GlancesFragment.this.startActivity(new Intent(GlancesFragment.this.getContext(), AppNotificationsActivity.class));
                    }
                }
            });
        }
        return this.rootView;
    }

    public void highlightGlanceSwitch() {
        if (this.rootView != null && this.baseActivity != null) {
            this.rootView.scrollTo(0, 0);
            startHighlightAnimation(this.rootView.findViewById(R.id.allow_glances_switch));
        }
    }

    private void startHighlightAnimation(View glances) {
        if (glances != null) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
            alphaAnimation.setDuration(250);
            alphaAnimation.setRepeatCount(6);
            alphaAnimation.setRepeatMode(2);
            glances.startAnimation(alphaAnimation);
        }
    }

    private void initAllSwitches() {
        boolean z;
        boolean z2 = true;
        float f = 1.0f;
        RadioButton radioButton = this.readAloud;
        if (!this.glancesAreReadAloud || this.glancesShowContent) {
            z = false;
        } else {
            z = true;
        }
        radioButton.setChecked(z);
        this.readAloud.setEnabled(this.glancesAreEnabled);
        this.readAloudLayout.setAlpha(this.glancesAreEnabled ? 1.0f : DISABLED_ALPHA_LEVEL);
        radioButton = this.showContent;
        if (this.glancesAreReadAloud || !this.glancesShowContent) {
            z = false;
        } else {
            z = true;
        }
        radioButton.setChecked(z);
        this.showContent.setEnabled(this.glancesAreEnabled);
        this.showContentLayout.setAlpha(this.glancesAreEnabled ? 1.0f : DISABLED_ALPHA_LEVEL);
        RadioButton radioButton2 = this.readAloudAndShowContent;
        if (!(this.glancesAreReadAloud && this.glancesShowContent)) {
            z2 = false;
        }
        radioButton2.setChecked(z2);
        this.readAloudAndShowContent.setEnabled(this.glancesAreEnabled);
        this.readAloudAndShowContentLayout.setAlpha(this.glancesAreEnabled ? 1.0f : DISABLED_ALPHA_LEVEL);
        initSwitch(R.id.allow_calls_switch, GlanceConstants.PHONE_PACKAGE);
        initSwitch(R.id.allow_sms_switch, GlanceConstants.SMS_PACKAGE);
        initSwitch(R.id.allow_fuel_switch, GlanceConstants.FUEL_PACKAGE);
        initSwitch(R.id.allow_music_switch, GlanceConstants.MUSIC_PACKAGE);
        initSwitch(R.id.allow_traffic_switch, GlanceConstants.TRAFFIC_PACKAGE);
        initSwitch(R.id.allow_gmail_switch, GlanceConstants.GOOGLE_MAIL);
        initSwitch(R.id.allow_google_calendar_switch, GlanceConstants.GOOGLE_CALENDAR);
        initSwitch(R.id.allow_hangouts_switch, GlanceConstants.GOOGLE_HANGOUTS);
        initSwitch(R.id.allow_slack_switch, GlanceConstants.SLACK);
        initSwitch(R.id.allow_whatsapp_switch, GlanceConstants.WHATS_APP);
        initSwitch(R.id.allow_facebook_messenger_switch, GlanceConstants.FACEBOOK_MESSENGER);
        initSwitch(R.id.allow_facebook_switch, GlanceConstants.FACEBOOK);
        initSwitch(R.id.allow_twitter_switch, GlanceConstants.TWITTER);
        RelativeLayout notificationGlances = (RelativeLayout) this.rootView.findViewById(R.id.notification_glances);
        if (notificationGlances != null) {
            notificationGlances.setAlpha(this.glancesAreEnabled ? 1.0f : DISABLED_ALPHA_LEVEL);
        }
        TextView otherGlances = (TextView) this.rootView.findViewById(R.id.other_glances);
        if (otherGlances != null) {
            if (!this.glancesAreEnabled) {
                f = DISABLED_ALPHA_LEVEL;
            }
            otherGlances.setAlpha(f);
        }
    }

    private void initSwitch(int switchId, String appPackage) {
        Switch aSwitch = (Switch) this.rootView.findViewById(switchId);
        aSwitch.setChecked(GlanceUtils.isThisGlanceEnabledInItself(appPackage));
        aSwitch.setEnabled(this.glancesAreEnabled);
        ViewParent parent = aSwitch.getParent();
        if (parent != null && (parent instanceof View)) {
            ((View) parent).setAlpha(this.glancesAreEnabled ? 1.0f : DISABLED_ALPHA_LEVEL);
        }
        aSwitch.setOnClickListener(getGlanceSwitchClickListener(appPackage));
    }

    private OnClickListener getGlanceSwitchClickListener(final String pkg) {
        return new OnClickListener() {
            public void onClick(View v) {
                if (v != null && (v instanceof Switch)) {
                    GlanceUtils.saveGlancesConfigurationChanges(pkg, ((Switch) v).isChecked());
                    if (GlanceUtils.isDrivingGlance(pkg)) {
                        GlancesFragment.this.increaseSerialAndSendAllGlancesPreferencesToHud();
                    }
                }
            }
        };
    }

    private void increaseSerialAndSendAllGlancesPreferencesToHud() {
        SettingsUtils.sendGlancesSettingsToTheHud(GlanceUtils.buildGlancesPreferences(SettingsUtils.incrementSerialNumber(SettingsConstants.GLANCES_SERIAL_NUM), this.glancesAreEnabled, this.glancesAreReadAloud, this.glancesShowContent));
    }

    private void showEnableGlancesDialog() {
        HomescreenActivity homescreenActivity = (HomescreenActivity) getActivity();
        if (homescreenActivity != null) {
            homescreenActivity.showQuestionDialog(R.string.warning, R.string.glances_warning, R.string.enable_glances, R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (i == -1) {
                        GlancesFragment.this.setGlances(Boolean.valueOf(true));
                    } else if (GlancesFragment.this.allowGlances != null) {
                        GlancesFragment.this.setGlances(Boolean.valueOf(false));
                        GlancesFragment.this.allowGlances.setChecked(false);
                    }
                }
            }, new OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    GlancesFragment.this.logger.v("onCancel");
                    GlancesFragment.this.setGlances(Boolean.valueOf(false));
                    GlancesFragment.this.allowGlances.setChecked(false);
                }
            });
        }
    }

    public void setGlances(final Boolean glancesAreEnabled) {
        this.glancesAreEnabled = glancesAreEnabled.booleanValue();
        if (glancesAreEnabled.booleanValue()) {
            SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
            if (!sharedPreferences.getBoolean(SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, false)) {
                sharedPreferences.edit().putBoolean(SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, true).apply();
                FragmentActivity activity = getActivity();
                if (activity instanceof HomescreenActivity) {
                    ((HomescreenActivity) activity).rebuildSuggestions();
                }
            }
        }
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                GlanceUtils.saveGlancesConfigurationChanges(SettingsConstants.GLANCES, glancesAreEnabled.booleanValue());
                GlancesFragment.this.increaseSerialAndSendAllGlancesPreferencesToHud();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                GlancesFragment.this.initAllSwitches();
            }
        }.execute();
    }

    public boolean requiresBus() {
        return false;
    }
}
