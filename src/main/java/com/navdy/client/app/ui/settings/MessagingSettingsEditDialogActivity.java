package com.navdy.client.app.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.base.BaseActivity;

public class MessagingSettingsEditDialogActivity extends BaseActivity {
    public static final int ADD_CODE = 4241;
    public static final int EDIT_CODE = 4242;
    public static final String EXTRA_NEW_MESSAGE = "extra_new_message";
    public static final String EXTRA_OLD_MESSAGE = "extra_old_message";
    private Button done;
    private EditText input;
    private String oldMessage;

    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(262144, 262144);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dialog_settings_messaging_add_edit);
        this.input = (EditText) findViewById(R.id.input);
        this.done = (Button) findViewById(R.id.done_button);
        if (this.input != null && this.done != null) {
            this.input.addTextChangedListener(new TextWatcher() {
                final Animation animShake = AnimationUtils.loadAnimation(MessagingSettingsEditDialogActivity.this, R.anim.shake);

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                public void afterTextChanged(Editable s) {
                    boolean messageIsValid;
                    if (StringUtils.isEmptyAfterTrim(s)) {
                        messageIsValid = false;
                    } else {
                        messageIsValid = true;
                    }
                    MessagingSettingsEditDialogActivity.this.done.setEnabled(messageIsValid);
                    int maxChars = MessagingSettingsEditDialogActivity.this.getResources().getInteger(R.integer.reply_text_max_chars);
                    if (s != null && s.length() >= maxChars) {
                        ((Vibrator) MessagingSettingsEditDialogActivity.this.getSystemService("vibrator")).vibrate(100);
                        MessagingSettingsEditDialogActivity.this.input.startAnimation(this.animShake);
                        int selectionEnd = MessagingSettingsEditDialogActivity.this.input.getSelectionEnd();
                        MessagingSettingsEditDialogActivity.this.input.setText(s.subSequence(0, s.length() - 1));
                        EditText access$100 = MessagingSettingsEditDialogActivity.this.input;
                        if (selectionEnd > MessagingSettingsEditDialogActivity.this.input.length()) {
                            selectionEnd = MessagingSettingsEditDialogActivity.this.input.length();
                        }
                        access$100.setSelection(selectionEnd);
                    }
                }
            });
            Intent intent = getIntent();
            if (intent != null) {
                this.oldMessage = intent.getStringExtra(EXTRA_OLD_MESSAGE);
                if (StringUtils.isEmptyAfterTrim(this.oldMessage)) {
                    TextView title = (TextView) findViewById(R.id.dialog_title);
                    if (title != null) {
                        title.setText(R.string.add_message);
                    }
                    this.done.setEnabled(false);
                    return;
                }
                this.input.setText(this.oldMessage);
                this.input.setSelection(this.input.length());
            }
        }
    }

    protected void onResume() {
        super.onResume();
        this.input.clearFocus();
        this.input.requestFocus();
        this.input.setSelection(this.input.length());
        getWindow().setSoftInputMode(4);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    public void onCancelClick(View view) {
        finish();
    }

    public void onDoneClick(View view) {
        String message = String.valueOf(this.input.getText());
        int code = ADD_CODE;
        if (!StringUtils.isEmptyAfterTrim(this.oldMessage)) {
            code = EDIT_CODE;
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRA_NEW_MESSAGE, message);
        intent.putExtra(EXTRA_OLD_MESSAGE, this.oldMessage);
        setResult(code, intent);
        finish();
    }
}
