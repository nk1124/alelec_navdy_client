package com.navdy.client.app.ui.homescreen;

import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Instances;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.models.Calendar;
import com.navdy.client.app.framework.models.Calendar.CalendarListItemType;
import com.navdy.client.app.framework.models.CalendarEvent;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.calendars.CalendarEventUpdates;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.droidparts.contract.SQL;

public class CalendarUtils {
    public static final long EVENTS_STALENESS_LIMIT = 300000;
    private static final int MAX_CALENDAR_EVENTS = 20;
    private static final long TIME_LIMIT_AFTER_NOW = 172800000;
    private static final long TIME_LIMIT_BEFORE_NOW = 1800000;
    private static List<CalendarEvent> cachedEvents = new ArrayList();
    private static long lastReadTime = 0;
    private static Logger logger = new Logger(CalendarUtils.class);

    public interface CalendarEventReader {
        void onCalendarEventsRead(List<CalendarEvent> list);
    }

    private static synchronized void setCachedEvents(List<CalendarEvent> cachedEvents) {
        synchronized (CalendarUtils.class) {
            lastReadTime = System.currentTimeMillis();
            cachedEvents = cachedEvents;
        }
    }

    public static void sendCalendarsToHud() {
        sendCalendarsToHud(AppInstance.getInstance().getRemoteDevice());
    }

    public static void sendCalendarsToHud(final RemoteDevice remoteDevice) {
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            logger.i("Not sending calendar events because not connected to HUD");
        } else {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    CalendarUtils.logger.d("sendCalendarsToHud");
                    CalendarUtils.forceCalendarRefresh();
                    CalendarUtils.readCalendarEvent(new CalendarEventReader() {
                        public void onCalendarEventsRead(List<CalendarEvent> calendarEvents) {
                            List calendarEventProtoObjects = new ArrayList();
                            if (!calendarEvents.isEmpty()) {
                                int i = 0;
                                while (i < calendarEvents.size() && i < 20) {
                                    CalendarEvent calendarEvent = (CalendarEvent) calendarEvents.get(i);
                                    Destination destination = NavdyContentProvider.getThisDestination(new Destination(calendarEvent.displayName, calendarEvent.location));
                                    if (destination != null) {
                                        calendarEvent.setDestination(destination.toProtobufDestinationObject());
                                    }
                                    calendarEventProtoObjects.add(calendarEvent.toProtobufObject());
                                    i++;
                                }
                                remoteDevice.postEvent(new CalendarEventUpdates(calendarEventProtoObjects));
                            }
                        }
                    }, false);
                }
            }, 1);
        }
    }

    private static String getSelectionForCalendarInstances(boolean mustHaveLocation) {
        long now = System.currentTimeMillis();
        String selection = "";
        if (mustHaveLocation) {
            selection = "eventLocation <> '' AND allDay = 0 AND ";
        }
        return selection + "begin > " + (now - 1800000) + SQL.AND + "begin" + " < " + (TIME_LIMIT_AFTER_NOW + now) + SQL.AND + "end" + " > " + now + SQL.AND + "selfAttendeeStatus" + " <> " + 2;
    }

    public static synchronized void forceCalendarRefresh() {
        synchronized (CalendarUtils.class) {
            lastReadTime = 0;
        }
    }

    /* JADX WARNING: Missing block: B:34:0x01dd, code:
            if (r42 == false) goto L_0x0220;
     */
    /* JADX WARNING: Missing block: B:35:0x01df, code:
            r0 = r41;
            r2 = new com.navdy.client.app.framework.map.HereNavigableCoordinateWorker(r30, new com.navdy.client.app.ui.homescreen.CalendarUtils.AnonymousClass2());
     */
    /* JADX WARNING: Missing block: B:46:?, code:
            r41.onCalendarEventsRead(r30);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void readCalendarEvent(CalendarEventReader callback, boolean buildForSuggestions) {
        synchronized (CalendarUtils.class) {
            Context context = NavdyApplication.getAppContext();
            SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
            if (callback == null) {
                logger.e("Called readCalendarEvent with a null callback! This should never happen.");
            } else {
                ArrayList<CalendarEvent> events = new ArrayList();
                if (ContextCompat.checkSelfPermission(context, "android.permission.READ_CALENDAR") != 0) {
                    logger.v("Calendar Access not permitted.");
                    callback.onCalendarEventsRead(events);
                } else {
                    long now = System.currentTimeMillis();
                    if (now - lastReadTime < EVENTS_STALENESS_LIMIT) {
                        logger.v("It hasn't been long enough. Ignoring request");
                        callback.onCalendarEventsRead(cachedEvents);
                    } else {
                        Cursor cursor = null;
                        try {
                            Builder builder = Instances.CONTENT_URI.buildUpon();
                            ContentUris.appendId(builder, now - 1800000);
                            ContentUris.appendId(builder, TIME_LIMIT_AFTER_NOW + now);
                            Uri contentUri = builder.build();
                            Context appContext = NavdyApplication.getAppContext();
                            String selection = getSelectionForCalendarInstances(buildForSuggestions);
                            logger.i("buildForSuggestions: " + buildForSuggestions + ", selection: " + selection);
                            cursor = appContext.getContentResolver().query(contentUri, new String[]{"title", "begin", "end", "allDay", "calendar_color", "eventLocation", "visible", "calendar_id"}, selection, null, "begin ASC");
                            if (cursor != null && cursor.moveToFirst()) {
                                logger.d("Found " + cursor.getCount() + " Calendar Event(s) in the DB that matches the timebox criteria");
                                while (true) {
                                    int displayNameColumnIndex = cursor.getColumnIndex("title");
                                    int startTimeColumnIndex = cursor.getColumnIndex("begin");
                                    int endTimeColumnIndex = cursor.getColumnIndex("end");
                                    int allDayColumnIndex = cursor.getColumnIndex("allDay");
                                    int colorColumnIndex = cursor.getColumnIndex("calendar_color");
                                    int locationColumnIndex = cursor.getColumnIndex("eventLocation");
                                    int visibleColumnIndex = cursor.getColumnIndex("visible");
                                    int calendarIdColumnIndex = cursor.getColumnIndex("calendar_id");
                                    String displayNameString = cursor.getString(displayNameColumnIndex);
                                    Long startDateAndTime = parseLongSafely(cursor.getString(startTimeColumnIndex));
                                    Long endDateAndTime = parseLongSafely(cursor.getString(endTimeColumnIndex));
                                    boolean allDay = cursor.getInt(allDayColumnIndex) > 0;
                                    int colorARGB = cursor.getInt(colorColumnIndex);
                                    String locationString = cursor.getString(locationColumnIndex);
                                    boolean visible = cursor.getInt(visibleColumnIndex) > 0;
                                    long calendarId = cursor.getLong(calendarIdColumnIndex);
                                    CalendarEvent calendarEvent = new CalendarEvent(displayNameString, calendarId, startDateAndTime.longValue(), endDateAndTime.longValue(), locationString, (long) colorARGB, allDay);
                                    if (sharedPrefs.getBoolean(SettingsConstants.CALENDAR_PREFIX + calendarId, visible)) {
                                        logger.d("Adding calendar event: " + calendarEvent.toString());
                                        events.add(calendarEvent);
                                    } else {
                                        logger.d("Ignoring calendar event: " + calendarEvent.toString());
                                    }
                                    if (!cursor.moveToNext()) {
                                        break;
                                    }
                                }
                            }
                            callback.onCalendarEventsRead(events);
                            setCachedEvents(events);
                        } finally {
                            IOUtils.closeStream(cursor);
                        }
                    }
                }
            }
        }
    }

    public static ArrayList<Calendar> listCalendars(Context context) {
        if (ContextCompat.checkSelfPermission(context, "android.permission.READ_CALENDAR") != 0) {
            return null;
        }
        Cursor cur = null;
        try {
            cur = context.getContentResolver().query(Calendars.CONTENT_URI, new String[]{"_id", "calendar_displayName", "calendar_color", "account_name", "account_type", "ownerAccount", "visible"}, null, null, "account_type ASC, visible DESC");
            ArrayList<Calendar> list;
            if (cur == null) {
                list = null;
                return list;
            }
            list = new ArrayList();
            list.add(new Calendar(CalendarListItemType.GLOBAL_SWITCH));
            String lastAccountType = null;
            while (cur.moveToNext()) {
                boolean visible;
                long id = cur.getLong(cur.getColumnIndex("_id"));
                String calendarDisplayName = cur.getString(cur.getColumnIndex("calendar_displayName"));
                int calendarColor = cur.getInt(cur.getColumnIndex("calendar_color"));
                String accountName = cur.getString(cur.getColumnIndex("account_name"));
                String accountType = cur.getString(cur.getColumnIndex("account_type"));
                String ownerAccount = cur.getString(cur.getColumnIndex("ownerAccount"));
                if (cur.getInt(cur.getColumnIndex("visible")) == 1) {
                    visible = true;
                } else {
                    visible = false;
                }
                if (!StringUtils.equalsOrBothEmptyAfterTrim(accountType, lastAccountType)) {
                    if ("com.google".equals(accountType)) {
                        list.add(new Calendar(CalendarListItemType.TITLE, context.getString(R.string.google_calendar)));
                    } else {
                        list.add(new Calendar(CalendarListItemType.TITLE, accountType));
                    }
                    lastAccountType = accountType;
                }
                list.add(new Calendar(CalendarListItemType.CALENDAR, id, calendarDisplayName, calendarColor, accountName, accountType, ownerAccount, visible));
            }
            IOUtils.closeStream(cur);
            return list;
        } finally {
            IOUtils.closeStream(cur);
        }
    }

    private static Long parseLongSafely(String n) {
        Long ret = Long.valueOf(0);
        if (n == null) {
            return ret;
        }
        try {
            return Long.valueOf(Long.parseLong(n));
        } catch (Exception e) {
            logger.e("Can't parse a long out of this string: " + n);
            return ret;
        }
    }

    public static String getDateAndTime(long milliSeconds) {
        Context context = NavdyApplication.getAppContext();
        if (milliSeconds == 0) {
            return context.getString(R.string.unknown);
        }
        return SimpleDateFormat.getDateTimeInstance().format(new Date(milliSeconds));
    }

    public static String getTime(long milliSeconds) {
        return DateFormat.getTimeFormat(NavdyApplication.getAppContext()).format(new Date(milliSeconds));
    }

    public static void convertAllClendarNameSharedPrefsToCalendarIds() {
        Context context = NavdyApplication.getAppContext();
        if (ContextCompat.checkSelfPermission(context, "android.permission.READ_CALENDAR") != 0) {
            removeAllSelectedCalendarsFromSharedPrefs();
            return;
        }
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        Set<String> keySet = sharedPreferences.getAll().keySet();
        Editor editor = sharedPreferences.edit();
        for (String key : keySet) {
            if (key.startsWith(SettingsConstants.CALENDAR_PREFIX)) {
                boolean value = sharedPreferences.getBoolean(key, true);
                String calendarName = key.substring(SettingsConstants.CALENDAR_PREFIX.length());
                try {
                    Cursor cur = context.getContentResolver().query(Calendars.CONTENT_URI, new String[]{"_id"}, "name=?", new String[]{calendarName}, null);
                    while (cur != null && cur.moveToNext()) {
                        long calendarId = cur.getLong(cur.getColumnIndex("_id"));
                        if (calendarId > 0) {
                            editor.putBoolean(SettingsConstants.CALENDAR_PREFIX + calendarId, value);
                        }
                    }
                    IOUtils.closeStream(cur);
                    editor.remove(key);
                } catch (Throwable th) {
                    IOUtils.closeStream(null);
                }
            }
        }
        editor.apply();
    }

    private static void removeAllSelectedCalendarsFromSharedPrefs() {
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        Set<String> keySet = sharedPreferences.getAll().keySet();
        Editor editor = sharedPreferences.edit();
        for (String key : keySet) {
            if (key.startsWith(SettingsConstants.CALENDAR_PREFIX)) {
                editor.remove(key);
            }
        }
        editor.apply();
    }
}
