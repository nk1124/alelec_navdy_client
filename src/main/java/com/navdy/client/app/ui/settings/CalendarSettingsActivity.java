package com.navdy.client.app.ui.settings;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CalendarSettingsActivity extends BaseToolbarActivity {
    private CalendarAdapter adapter;
    private View permissionScreen;
    private RecyclerView recyclerView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_calendar);
        new ToolbarBuilder().title((int) R.string.menu_calendar).build();
        this.permissionScreen = findViewById(R.id.permission_screen);
        this.recyclerView = (RecyclerView) findViewById(R.id.list);
        if (this.recyclerView != null) {
            this.adapter = new CalendarAdapter();
            this.recyclerView.setAdapter(this.adapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            layoutManager.setOrientation(1);
            this.recyclerView.setLayoutManager(layoutManager);
        }
        TextView desc = (TextView) findViewById(R.id.desc);
        if (desc != null) {
            desc.setText(StringUtils.fromHtml((int) R.string.fle_app_setup_calendar_desc_fail));
        }
    }

    protected void onResume() {
        super.onResume();
        if (this.adapter.getItemCount() <= 0) {
            if (this.permissionScreen != null) {
                this.permissionScreen.setVisibility(VISIBLE);
            }
            if (this.recyclerView != null) {
                this.recyclerView.setVisibility(GONE);
                return;
            }
            return;
        }
        if (this.permissionScreen != null) {
            this.permissionScreen.setVisibility(GONE);
        }
        if (this.recyclerView != null) {
            this.recyclerView.setVisibility(VISIBLE);
        }
    }

    protected void onPause() {
        SuggestionManager.forceSuggestionFullRefresh();
        super.onPause();
    }

    public void onDescriptionClick(View view) {
        SystemUtils.goToSystemSettingsAppInfoForOurApp(this);
    }
}
