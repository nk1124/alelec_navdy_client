package com.navdy.client.app.ui.settings;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event.Settings;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.firstlaunch.CarInfoActivity;
import com.navdy.client.app.ui.firstlaunch.EditCarInfoActivity;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting;
import com.navdy.service.library.task.TaskManager;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class CarSettingsActivity extends BaseEditActivity {
    private Switch autoOn;
    private RadioButton compact;
    private LinearLayout compactUiLinearLayout;
    private SharedPreferences customerPrefs;
    private RadioButton normal;
    private String obdEnabledString = "";
    private Switch obdSwitch;
    private OnCheckedChangeListener onObdSwitchChangedListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton compoundButton, boolean isEnabled) {
            CarSettingsActivity.this.obdSwitch.setEnabled(false);
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    CarSettingsActivity.this.somethingChanged = true;
                    if (CarSettingsActivity.this.obdSwitch != null) {
                        if (!CarSettingsActivity.this.obdSwitch.isChecked()) {
                            CarSettingsActivity.this.obdEnabledString = ObdScanSetting.SCAN_OFF.toString();
                        } else if (SettingsUtils.carIsBlacklistedForObdData()) {
                            CarSettingsActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    CarSettingsActivity.this.showQuestionDialog(R.string.settings_obd_warning_title, R.string.settings_obd_warning_description, R.string.ok, R.string.cancel, new OnClickListener() {
                                        public void onClick(DialogInterface dialogInterface, int whichButton) {
                                            if (whichButton == -1) {
                                                SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.HUD_BLACKLIST_OVERRIDDEN, true).apply();
                                                CarSettingsActivity.this.obdEnabledString = ObdScanSetting.SCAN_ON.toString();
                                                return;
                                            }
                                            CarSettingsActivity.this.obdSwitch.setChecked(false);
                                        }
                                    }, new OnCancelListener() {
                                        public void onCancel(DialogInterface dialogInterface) {
                                            CarSettingsActivity.this.obdSwitch.setChecked(false);
                                        }
                                    });
                                }
                            });
                        } else {
                            CarSettingsActivity.this.obdEnabledString = ObdScanSetting.SCAN_ON.toString();
                        }
                    }
                    CarSettingsActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            CarSettingsActivity.this.obdSwitch.setEnabled(true);
                        }
                    });
                }
            }, 1);
            CarSettingsActivity.this.obdSwitch.setEnabled(true);
        }
    };
    private SharedPreferences sharedPrefs;
    private TextView vehicleInfo;

    public void onNormalClick(View view) {
        this.somethingChanged = true;
        updateUiScalingSetting(false);
    }

    public void onCompactClick(View view) {
        this.somethingChanged = true;
        updateUiScalingSetting(true);
    }

    private void updateUiScalingSetting(boolean uiScalingIsOn) {
        this.normal.setChecked(!uiScalingIsOn);
        this.compact.setChecked(uiScalingIsOn);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_car);
        new ToolbarBuilder().title((int) R.string.menu_car).build();
        this.vehicleInfo = (TextView) findViewById(R.id.car_info);
        this.autoOn = (Switch) findViewById(R.id.auto_on);
        this.obdSwitch = (Switch) findViewById(R.id.obd_on);
        this.normal = (RadioButton) findViewById(R.id.normal);
        this.compact = (RadioButton) findViewById(R.id.compact);
        this.compactUiLinearLayout = (LinearLayout) findViewById(R.id.compact_ui_linear_layout);
    }

    protected void onResume() {
        super.onResume();
        this.customerPrefs = SettingsUtils.getCustomerPreferences();
        this.sharedPrefs = SettingsUtils.getSharedPreferences();
        boolean autoOnEnabled = this.sharedPrefs.getBoolean(SettingsConstants.HUD_AUTO_ON, SettingsConstants.HUD_AUTO_ON_DEFAULT);
        this.obdEnabledString = this.sharedPrefs.getString(SettingsConstants.HUD_OBD_ON, SettingsConstants.HUD_OBD_ON_DEFAULT);
        if (this.vehicleInfo != null) {
            String carInfo = SettingsUtils.getCarYearMakeModelString();
            if (StringUtils.isEmptyAfterTrim(carInfo)) {
                carInfo = getString(R.string.tap_to_set_car_info);
            }
            this.vehicleInfo.setText(carInfo);
        }
        if (this.obdEnabledString.equals(ObdScanSetting.SCAN_DEFAULT_ON.toString()) || this.obdEnabledString.equals(ObdScanSetting.SCAN_ON.toString())) {
            initCompoundButton(this.obdSwitch, true, this.onObdSwitchChangedListener);
        } else {
            initCompoundButton(this.obdSwitch, false, this.onObdSwitchChangedListener);
        }
        initCompoundButton(this.autoOn, autoOnEnabled);
        if (this.sharedPrefs.getBoolean(SettingsConstants.HUD_COMPACT_CAPABLE, SettingsConstants.HUD_COMPACT_CAPABLE_DEFAULT)) {
            this.compactUiLinearLayout.setVisibility(VISIBLE);
            updateUiScalingSetting(this.sharedPrefs.getBoolean(SettingsConstants.HUD_UI_SCALING_ON, SettingsConstants.HUD_UI_SCALING_ON_DEFAULT));
            return;
        }
        this.compactUiLinearLayout.setVisibility(INVISIBLE);
    }

    public void onPause() {
        this.obdSwitch.setOnCheckedChangeListener(null);
        super.onPause();
    }

    public void startEditInfoActivity(View view) {
        startActivity(new Intent(getApplicationContext(), EditCarInfoActivity.class));
    }

    public void startObdLocatorActivity(View view) {
        startActivity(new Intent(getApplicationContext(), CarInfoActivity.class));
    }

    protected void saveChanges() {
        final boolean uiScalingIsOn = this.compact.isChecked();
        final boolean autoOnIsEnabled = this.autoOn.isChecked();
        this.logger.d("current obd setting: " + this.obdEnabledString);
        if (this.sharedPrefs != null) {
            new AsyncTask<Object, Boolean, Boolean>() {
                protected void onPreExecute() {
                    super.onPreExecute();
                    CarSettingsActivity.this.showProgressDialog();
                }

                protected Boolean doInBackground(Object... params) {
                    CarSettingsActivity.this.sharedPrefs.edit().putBoolean(SettingsConstants.HUD_UI_SCALING_ON, uiScalingIsOn).putBoolean(SettingsConstants.HUD_AUTO_ON, autoOnIsEnabled).putString(SettingsConstants.HUD_OBD_ON, CarSettingsActivity.this.obdEnabledString).apply();
                    Tracker.tagEvent(Settings.PROFILE_SETTINGS_CHANGED);
                    SettingsUtils.incrementDriverProfileSerial();
                    return Boolean.valueOf(SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue());
                }

                protected void onPostExecute(Boolean success) {
                    if (success.booleanValue()) {
                        BaseActivity.showShortToast(R.string.settings_car_edit_succeeded, new Object[0]);
                    } else {
                        BaseActivity.showShortToast(R.string.settings_need_to_be_connected_to_hud, new Object[0]);
                    }
                    CarSettingsActivity.this.hideProgressDialog();
                    super.onPostExecute(success);
                }
            }.execute(new Object[0]);
        } else {
            BaseActivity.showShortToast(R.string.settings_car_edit_failed, new Object[0]);
        }
    }

    public void onAutoOnClick(View view) {
        if (this.autoOn != null && this.autoOn.isEnabled()) {
            this.autoOn.performClick();
        }
    }

    public void onObdSettingChangeClick(View view) {
        if (this.obdSwitch != null && this.obdSwitch.isEnabled()) {
            this.obdSwitch.performClick();
        }
    }
}
