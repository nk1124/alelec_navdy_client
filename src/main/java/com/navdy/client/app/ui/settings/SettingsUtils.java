package com.navdy.client.app.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaRouter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.LocaleList;
import android.provider.Telephony.Sms;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.JsonToken;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.i18n.I18nManager;
import com.navdy.client.app.framework.models.MountInfo;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.servicehandler.PhotoServiceHandler;
import com.navdy.client.app.framework.util.CrashlyticsAppender;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event.Settings;
import com.navdy.client.app.tracking.TrackerConstants.Screen;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.Capabilities;
import com.navdy.service.library.events.LegacyCapability;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.glances.CannedMessagesUpdate;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferences;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate;
import com.navdy.service.library.events.preferences.DriverProfilePreferences;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.Builder;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DisplayFormat;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.FeatureMode;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.ObdScanSetting;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem;
import com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate;
import com.navdy.service.library.events.preferences.InputPreferences;
import com.navdy.service.library.events.preferences.InputPreferences.DialSensitivity;
import com.navdy.service.library.events.preferences.InputPreferencesUpdate;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic;
import com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType;
import com.navdy.service.library.events.preferences.NavigationPreferencesUpdate;
import com.navdy.service.library.events.preferences.NotificationPreferences;
import com.navdy.service.library.events.preferences.NotificationPreferencesUpdate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.droidparts.contract.SQL.DDL;

public class SettingsUtils {
    private static final String FLAT_SUPPORTED = "flatsupported";
    private static final String MAKE = "make";
    private static final String MEDIUM_SUPPORTED = "mediumsupported";
    private static final String MODEL = "model";
    private static final String RECOMMENDED = "recommended";
    private static final String TALL_SUPPORTED = "tallsupported";
    private static final String YEAR = "year";
    private static String lastMake = "";
    private static String lastModel = "";
    private static MountInfo lastMountInfo = new MountInfo();
    private static String lastYear = "";
    private static final Logger logger = new Logger(SettingsUtils.class);
    private static boolean updateSettingsUponNextConnection = false;

    static class SettingsLogger {
        private SharedPreferences customerPreferences = SettingsUtils.getCustomerPreferences();
        private SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();

        SettingsLogger() {
        }

        void logBoolean(SharedPreferences preferences, String key, boolean defaultValue) {
            SettingsUtils.logger.i(key + CrashlyticsAppender.SEPARATOR + preferences.getBoolean(key, defaultValue));
        }

        void logString(SharedPreferences preferences, String key, String defaultValue) {
            SettingsUtils.logger.i(key + CrashlyticsAppender.SEPARATOR + preferences.getString(key, defaultValue));
        }

        void logLong(SharedPreferences preferences, String key, long defaultValue) {
            SettingsUtils.logger.i(key + CrashlyticsAppender.SEPARATOR + preferences.getLong(key, defaultValue));
        }

        void logInt(SharedPreferences preferences, String key, int defaultValue) {
            SettingsUtils.logger.i(key + CrashlyticsAppender.SEPARATOR + preferences.getInt(key, defaultValue));
        }

        void logFloat(SharedPreferences preferences, String key, float defaultValue) {
            SettingsUtils.logger.i(key + CrashlyticsAppender.SEPARATOR + preferences.getFloat(key, defaultValue));
        }

        boolean profileSetup() {
            return Tracker.isUserRegistered();
        }

        boolean glancesSetup() {
            return BaseActivity.weHaveNotificationPermission();
        }

        boolean displaySetup() {
            return DeviceConnection.isConnected();
        }

        boolean carSetup() {
            return !StringUtils.isEmptyAfterTrim(this.customerPreferences.getString(ProfilePreferences.CAR_YEAR, "")) && !StringUtils.isEmptyAfterTrim(this.customerPreferences.getString(ProfilePreferences.CAR_MAKE, "")) && !StringUtils.isEmptyAfterTrim(this.customerPreferences.getString(ProfilePreferences.CAR_MODEL, ""));
        }

        CharSequence audioOutput() {
            return ((MediaRouter) NavdyApplication.getAppContext().getSystemService(Context.MEDIA_ROUTER_SERVICE)).getSelectedRoute(1).getName();
        }

        void logConfiguration() {
            SettingsUtils.logger.i("USER CONFIGURATION ==========");
            SettingsUtils.logger.i("--- Client Version ---");
            logString(this.sharedPreferences, SettingsConstants.LAST_BUILD_VERSION, "1.0");
            SettingsUtils.logger.i("App Version: " + HomescreenActivity.getAppVersionString());
            SettingsUtils.logger.i("--- Navigation preferences ---");
            logBoolean(this.sharedPreferences, SettingsConstants.NAVIGATION_ROUTE_CALCULATION, SettingsConstants.NAVIGATION_ROUTE_CALCULATION_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.NAVIGATION_AUTO_RECALC, SettingsConstants.NAVIGATION_AUTO_RECALC_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.NAVIGATION_HIGHWAYS, SettingsConstants.NAVIGATION_HIGHWAYS_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.NAVIGATION_TOLL_ROADS, SettingsConstants.NAVIGATION_TOLL_ROADS_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.NAVIGATION_FERRIES, SettingsConstants.NAVIGATION_FERRIES_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.NAVIGATION_TUNNELS, SettingsConstants.NAVIGATION_TUNNELS_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.NAVIGATION_UNPAVED_ROADS, SettingsConstants.NAVIGATION_UNPAVED_ROADS_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.NAVIGATION_AUTO_TRAINS, SettingsConstants.NAVIGATION_AUTO_TRAINS_DEFAULT);
            SettingsUtils.logger.i("--- Driver preferences ---");
            logLong(this.customerPreferences, ProfilePreferences.SERIAL_NUM, ProfilePreferences.SERIAL_NUM_DEFAULT);
            logString(this.customerPreferences, ProfilePreferences.FULL_NAME, "");
            SettingsUtils.logger.i("--- Audio preferences ---");
            logBoolean(this.sharedPreferences, SettingsConstants.AUDIO_ALL_AUDIO, SettingsConstants.AUDIO_ALL_AUDIO_DEFAULT);
            logInt(this.sharedPreferences, SettingsConstants.AUDIO_HUD_VOLUME, 100);
            logBoolean(this.sharedPreferences, SettingsConstants.AUDIO_MENU_FEEDBACK, SettingsConstants.AUDIO_MENU_FEEDBACK_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.AUDIO_NOTIFICATIONS, SettingsConstants.AUDIO_NOTIFICATIONS_DEFAULT);
            logInt(this.sharedPreferences, SettingsConstants.AUDIO_PHONE_VOLUME, 75);
            logFloat(this.sharedPreferences, SettingsConstants.AUDIO_TTS_VOLUME, 0.6f);
            logBoolean(this.sharedPreferences, SettingsConstants.AUDIO_PLAY_WELCOME_MESSAGE, SettingsConstants.AUDIO_PLAY_WELCOME_MESSAGE_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS_DEFAULT);
            logString(this.sharedPreferences, SettingsConstants.AUDIO_VOICE, SettingsConstants.AUDIO_VOICE_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.AUDIO_SPEED_WARNINGS, SettingsConstants.AUDIO_SPEED_WARNINGS_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.AUDIO_CAMERA_WARNINGS, SettingsConstants.AUDIO_CAMERA_WARNINGS_DEFAULT);
            SettingsUtils.logger.i("--- Current audio routing ---");
            SettingsUtils.logger.i("output: " + audioOutput());
            SettingsUtils.logger.i("HFP connected: " + TTSAudioRouter.isHFPSpeakerConnected());
            SettingsUtils.logger.i("A2DP connected: " + TTSAudioRouter.isA2DPSpeakerConnected());
            SettingsUtils.logger.i("--- Software update ---");
            logInt(this.sharedPreferences, SettingsConstants.OTA_VERSION, -1);
            logString(this.sharedPreferences, SettingsConstants.OTA_URL, "");
            logString(this.sharedPreferences, SettingsConstants.OTA_DESCRIPTION, "");
            logLong(this.sharedPreferences, SettingsConstants.OTA_SIZE, 0);
            logString(this.sharedPreferences, SettingsConstants.OTA_STATUS, SettingsConstants.OTA_STATUS_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.OTA_IS_INCREMENTAL, SettingsConstants.OTA_IS_INCREMENTAL_DEFAULT);
            logInt(this.sharedPreferences, SettingsConstants.OTA_FROM_VERSION, 0);
            logBoolean(this.sharedPreferences, SettingsConstants.OTA_FORCE_FULL_UPDATE, SettingsConstants.OTA_FORCE_FULL_UPDATE_DEFAULT);
            logString(this.sharedPreferences, SettingsConstants.OTA_META_DATA, "");
            SettingsUtils.logger.i("--- Glances ---");
            logBoolean(this.sharedPreferences, SettingsConstants.GLANCES, SettingsConstants.GLANCES_ENABLED_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.GLANCES_READ_ALOUD, SettingsConstants.GLANCES_READ_ALOUD_DEFAULT);
            logBoolean(this.sharedPreferences, SettingsConstants.GLANCES_SHOW_CONTENT, SettingsConstants.GLANCES_SHOW_CONTENT_DEFAULT);
            SettingsUtils.logger.i("--- Onboarding completion ---");
            SettingsUtils.logger.i("profile setup: " + profileSetup());
            SettingsUtils.logger.i("display setup: " + displaySetup());
            SettingsUtils.logger.i("car setup: " + carSetup());
            SettingsUtils.logger.i("glances setup: " + glancesSetup());
            SettingsUtils.logger.i("========== END USER CONFIGURATION");
        }
    }

    public static void onSharedPrefsDowngrade(int oldVersion, int newVersion) {
        setSharedPrefsVersionToCurrent();
    }

    public static void onSharedPrefsUpgrade(int oldVersion, int newVersion) {
        if (oldVersion <= 0) {
            CalendarUtils.convertAllClendarNameSharedPrefsToCalendarIds();
        }
        setSharedPrefsVersionToCurrent();
    }

    public static void setSharedPrefsVersionToCurrent() {
        Editor editor = getSharedPreferences().edit();
        editor.putInt(SettingsConstants.SHARED_PREF_VERSION, 1);
        editor.apply();
    }

    private static synchronized void setUpdateSettingsUponNextConnection(boolean updateNextTime) {
        synchronized (SettingsUtils.class) {
            updateSettingsUponNextConnection = updateNextTime;
        }
    }

    private static synchronized boolean getUpdateSettingsUponNextConnection() {
        boolean z;
        synchronized (SettingsUtils.class) {
            z = updateSettingsUponNextConnection;
        }
        return z;
    }

    public static void updateAllSettingsIfNecessary() {
        if (getUpdateSettingsUponNextConnection()) {
            new AsyncTask<Void, Void, Boolean>() {
                protected Boolean doInBackground(Void... voids) {
                    return SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue() &&
                            SettingsUtils.sendHudSettingsBasedOnSharedPrefValue() &&
                            SettingsUtils.sendDialSettingsToTheHudBasedOnSharedPrefValue() &&
                            SettingsUtils.sendNavSettingsToTheHudBasedOnSharedPrefValue() &&
                            SettingsUtils.sendMessagingSettingsToTheHudBasedOnSharedPrefValue() &&
                            GlanceUtils.sendGlancesSettingsToTheHudBasedOnSharedPrefValue();
                }

                protected void onPostExecute(Boolean success) {
                    if (success) {
                        SettingsUtils.setUpdateSettingsUponNextConnection(false);
                    } else {
                        BaseActivity.showLongToast(R.string.settings_all_updated_failure);
                    }
                }
            }.execute();
        }
    }

    public static void incrementDriverProfileSerial() {
        SharedPreferences customerPrefs = getCustomerPreferences();
        customerPrefs.edit().putLong(ProfilePreferences.SERIAL_NUM, 1 + customerPrefs.getLong(ProfilePreferences.SERIAL_NUM, ProfilePreferences.SERIAL_NUM_DEFAULT.longValue())).apply();
    }

    @WorkerThread
    public static boolean sendDriverProfileToTheHudBasedOnSharedPrefValue() {
        DialLongPressAction dialLongPressAction;
        SystemUtils.ensureNotOnMainThread();
        SharedPreferences customerPrefs = getCustomerPreferences();
        long serial = customerPrefs.getLong(ProfilePreferences.SERIAL_NUM, ProfilePreferences.SERIAL_NUM_DEFAULT.longValue());
        logger.d("serial: " + serial);
        String fullName = customerPrefs.getString(ProfilePreferences.FULL_NAME, "");
        String email = customerPrefs.getString("email", "");
        String carYear = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String carMake = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String carModel = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        SharedPreferences settingsPrefs = getSharedPreferences();
        boolean autoOnEnabled = settingsPrefs.getBoolean(SettingsConstants.HUD_AUTO_ON, true);
        boolean uiScalingEnabled = settingsPrefs.getBoolean(SettingsConstants.HUD_UI_SCALING_ON, false);
        FeatureMode featureMode = FeatureMode.valueOf(settingsPrefs.getString(SettingsConstants.FEATURE_MODE, SettingsConstants.FEATURE_MODE_DEFAULT));
        logger.i("featureMode: " + featureMode);
        DisplayFormat displayFormat = uiScalingEnabled ? DisplayFormat.DISPLAY_FORMAT_COMPACT : DisplayFormat.DISPLAY_FORMAT_NORMAL;
        String phoneName = Build.MODEL;
        String photoChecksum = PhotoServiceHandler.buildPhotoChecksum(Tracker.getUserProfilePhoto());
        String locale = Locale.getDefault().toString();
        UnitSystem unitSystem = I18nManager.getInstance().getUnitSystem();
        ObdScanSetting obdScanSetting = ObdScanSetting.valueOf(settingsPrefs.getString(SettingsConstants.HUD_OBD_ON, SettingsConstants.HUD_OBD_ON_DEFAULT));
        long obdBlacklistLastModified = getObdBlacklistLastModifiedDate();
        boolean limitCellularData = isLimitingCellularData();
        ArrayList<String> additionalLocales = new ArrayList();
        Context appContext = NavdyApplication.getAppContext();
        if (VERSION.SDK_INT >= 24) {
            LocaleList locales = appContext.getResources().getConfiguration().getLocales();
            for (int i = 0; i < locales.size(); i++) {
                Locale availableLocale = locales.get(i);
                if (availableLocale != null) {
                    String language = availableLocale.toString();
                    if (!StringUtils.equalsOrBothEmptyAfterTrim(locale, language)) {
                        additionalLocales.add(language);
                    }
                }
            }
        }
        if (settingsPrefs.getInt(SettingsConstants.DIAL_LONG_PRESS_ACTION, SettingsConstants.DIAL_LONG_PRESS_ACTION_DEFAULT) == DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH.getValue()) {
            dialLongPressAction = DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH;
        } else {
            dialLongPressAction = DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT;
        }
        return sendDriverProfileSettingsToTheHud(new Builder().serial_number(Long.valueOf(serial)).driver_name(fullName).device_name(phoneName).profile_is_public(Boolean.valueOf(false)).photo_checksum(photoChecksum).driver_email(email).car_make(carMake).car_model(carModel).car_year(carYear).auto_on_enabled(Boolean.valueOf(autoOnEnabled)).display_format(displayFormat).locale(locale).additionalLocales(additionalLocales).unit_system(unitSystem).feature_mode(featureMode).obdScanSetting(obdScanSetting).limit_bandwidth(Boolean.valueOf(limitCellularData)).obdBlacklistLastModified(Long.valueOf(obdBlacklistLastModified)).dial_long_press_action(dialLongPressAction).build());
    }

    @Nullable
    public static UnitSystem getUnitSystem() {
        String unitSystemPreference = getCustomerPreferences().getString(ProfilePreferences.UNIT_SYSTEM, "");
        if (StringUtils.equalsOrBothEmptyAfterTrim(unitSystemPreference, UnitSystem.UNIT_SYSTEM_IMPERIAL.name())) {
            return UnitSystem.UNIT_SYSTEM_IMPERIAL;
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(unitSystemPreference, UnitSystem.UNIT_SYSTEM_METRIC.name())) {
            return UnitSystem.UNIT_SYSTEM_METRIC;
        }
        return null;
    }

    public static void setUnitSystem(final UnitSystem unitSystem) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                SettingsUtils.getCustomerPreferences().edit().putString(ProfilePreferences.UNIT_SYSTEM, unitSystem.name()).apply();
                SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            }
        }, 1);
    }

    public static boolean isLimitingCellularData() {
        return getSharedPreferences().getBoolean(SettingsConstants.LIMIT_CELLULAR_DATA, false);
    }

    static void setLimitCellularData(final boolean isLimited) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                SettingsUtils.getSharedPreferences().edit().putBoolean(SettingsConstants.LIMIT_CELLULAR_DATA, isLimited).apply();
                SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            }
        }, 1);
    }

    public static void setFeatureMode(final FeatureMode featureMode) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                SettingsUtils.incrementDriverProfileSerial();
                SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.FEATURE_MODE, featureMode.toString()).apply();
                SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            }
        }, 1);
    }

    public static FeatureMode getFeatureMode() {
        return FeatureMode.valueOf(getSharedPreferences().getString(SettingsConstants.FEATURE_MODE, SettingsConstants.FEATURE_MODE_DEFAULT));
    }

    public static boolean isBetaUser() {
        return getFeatureMode() == FeatureMode.FEATURE_MODE_BETA;
    }

    private static boolean sendDriverProfileSettingsToTheHud(DriverProfilePreferences driverProfilePrefs) {
        if (driverProfilePrefs == null) {
            return false;
        }
        boolean deviceIsConnected;
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (deviceIsConnected) {
            DriverProfilePreferencesUpdate.Builder updateBldr = new DriverProfilePreferencesUpdate.Builder();
            updateBldr.status = RequestStatus.REQUEST_SUCCESS;
            updateBldr.serial_number = driverProfilePrefs.serial_number;
            updateBldr.preferences = driverProfilePrefs;
            return remoteDevice.postEvent(updateBldr.build());
        }
        setUpdateSettingsUponNextConnection(true);
        return false;
    }

    static boolean sendHudSettings(boolean gestureIsEnabled) {
        return sendInputSettingsToTheHud(buildInputPreferences(getSharedPreferences().getLong(SettingsConstants.HUD_SERIAL_NUM, 0), gestureIsEnabled));
    }

    private static boolean sendHudSettingsBasedOnSharedPrefValue() {
        return sendHudSettings(getSharedPreferences().getBoolean(SettingsConstants.HUD_GESTURE, SettingsConstants.HUD_GESTURE_DEFAULT));
    }

    public static boolean sendDialSettingsToTheHudBasedOnSharedPrefValue() {
        SharedPreferences sharedPrefs = getSharedPreferences();
        return sendInputSettingsToTheHud(buildInputPreferences(sharedPrefs.getLong(SettingsConstants.HUD_SERIAL_NUM, 0), sharedPrefs.getBoolean(SettingsConstants.HUD_GESTURE, SettingsConstants.HUD_GESTURE_DEFAULT)));
    }

    @NonNull
    private static InputPreferences buildInputPreferences(long serialNumber, boolean useGesture) {
        return new InputPreferences(serialNumber, useGesture, DialSensitivity.DIAL_SENSITIVITY_STANDARD);
    }

    private static boolean sendInputSettingsToTheHud(InputPreferences inputPrefs) {
        if (inputPrefs == null) {
            return false;
        }
        boolean deviceIsConnected;
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (deviceIsConnected) {
            InputPreferencesUpdate.Builder updateBldr = new InputPreferencesUpdate.Builder();
            updateBldr.status = RequestStatus.REQUEST_SUCCESS;
            updateBldr.serial_number = inputPrefs.serial_number;
            updateBldr.preferences = inputPrefs;
            return remoteDevice.postEvent(updateBldr.build());
        }
        setUpdateSettingsUponNextConnection(true);
        return false;
    }

    public static boolean sendSpeakerSettingsToTheHudBasedOnSharedPrefValue() {
        SharedPreferences sharedPrefs = getSharedPreferences();
        return sendDisplaySpeakerSettingsToTheHud(buildDisplaySpeakerPreferences(sharedPrefs.getLong(SettingsConstants.AUDIO_SERIAL_NUM, 0), sharedPrefs.getBoolean(SettingsConstants.AUDIO_ALL_AUDIO, true), sharedPrefs.getInt(SettingsConstants.AUDIO_HUD_VOLUME, 100), sharedPrefs.getBoolean(SettingsConstants.AUDIO_MENU_FEEDBACK, true), sharedPrefs.getBoolean(SettingsConstants.AUDIO_NOTIFICATIONS, true)));
    }

    @NonNull
    private static DisplaySpeakerPreferences buildDisplaySpeakerPreferences(long serialNumber, boolean masterSound, int volumeLevel, boolean feedbackSound, boolean notificationSound) {
        return new DisplaySpeakerPreferences(serialNumber, masterSound, volumeLevel, feedbackSound, notificationSound);
    }

    private static boolean sendDisplaySpeakerSettingsToTheHud(DisplaySpeakerPreferences speakerPrefs) {
        boolean deviceIsConnected;
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (deviceIsConnected) {
            DisplaySpeakerPreferencesUpdate.Builder updateBldr = new DisplaySpeakerPreferencesUpdate.Builder();
            updateBldr.status = RequestStatus.REQUEST_SUCCESS;
            updateBldr.serial_number = speakerPrefs.serial_number;
            updateBldr.preferences = speakerPrefs;
            return remoteDevice.postEvent(updateBldr.build());
        }
        setUpdateSettingsUponNextConnection(true);
        return false;
    }

    public static boolean sendNavSettingsToTheHudBasedOnSharedPrefValue() {
        SharedPreferences sharedPrefs = getSharedPreferences();
        return sendNavSettingsToTheHud(buildNavigationPreferences(sharedPrefs.getLong("nav_serial_number", 0), sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_ROUTE_CALCULATION, SettingsConstants.NAVIGATION_ROUTE_CALCULATION_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_AUTO_RECALC, SettingsConstants.NAVIGATION_AUTO_RECALC_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_HIGHWAYS, SettingsConstants.NAVIGATION_HIGHWAYS_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_TOLL_ROADS, SettingsConstants.NAVIGATION_TOLL_ROADS_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_FERRIES, SettingsConstants.NAVIGATION_FERRIES_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_TUNNELS, SettingsConstants.NAVIGATION_TUNNELS_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_UNPAVED_ROADS, SettingsConstants.NAVIGATION_UNPAVED_ROADS_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.NAVIGATION_AUTO_TRAINS, SettingsConstants.NAVIGATION_AUTO_TRAINS_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.AUDIO_SPEED_WARNINGS, SettingsConstants.AUDIO_SPEED_WARNINGS_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.AUDIO_CAMERA_WARNINGS, SettingsConstants.AUDIO_CAMERA_WARNINGS_DEFAULT), sharedPrefs.getBoolean(SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS_DEFAULT)));
    }

    static boolean sendNavSettingsToTheHud(NavigationPreferences navPrefs) {
        if (navPrefs == null) {
            return false;
        }
        boolean deviceIsConnected;
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (deviceIsConnected) {
            NavigationPreferencesUpdate.Builder updateBldr = new NavigationPreferencesUpdate.Builder();
            updateBldr.status = RequestStatus.REQUEST_SUCCESS;
            updateBldr.serial_number = navPrefs.serial_number;
            updateBldr.preferences = navPrefs;
            return remoteDevice.postEvent(updateBldr.build());
        }
        setUpdateSettingsUponNextConnection(true);
        return false;
    }

    @NonNull
    static NavigationPreferences buildNavigationPreferences(long serialNumber, boolean calculateShortestRouteIsOn, boolean autoRecalcIsOn, boolean highwaysIsOn, boolean tollRoadsIsOn, boolean ferriesIsOn, boolean tunnelsIsOn, boolean unpavedRoadsIsOn, boolean autoTrainsIsOn, boolean spokenSpeedLimitWarningsAreOn, boolean spokenCameraWarningsAreOn, boolean spokenTurnByTurnIsOn) {
        NavigationPreferences.Builder navPrefBldr = new NavigationPreferences.Builder();
        navPrefBldr.serial_number = serialNumber;
        navPrefBldr.routingType = calculateShortestRouteIsOn ? RoutingType.ROUTING_SHORTEST : RoutingType.ROUTING_FASTEST;
        navPrefBldr.rerouteForTraffic = autoRecalcIsOn ? RerouteForTraffic.REROUTE_AUTOMATIC : RerouteForTraffic.REROUTE_CONFIRM;
        navPrefBldr.allowHighways = highwaysIsOn;
        navPrefBldr.allowTollRoads = tollRoadsIsOn;
        navPrefBldr.allowFerries = ferriesIsOn;
        navPrefBldr.allowTunnels = tunnelsIsOn;
        navPrefBldr.allowUnpavedRoads = unpavedRoadsIsOn;
        navPrefBldr.allowAutoTrains = autoTrainsIsOn;
        navPrefBldr.spokenSpeedLimitWarnings = spokenSpeedLimitWarningsAreOn;
        navPrefBldr.spokenCameraWarnings = spokenCameraWarningsAreOn;
        navPrefBldr.spokenTurnByTurn = spokenTurnByTurnIsOn;
        return navPrefBldr.build();
    }

    public static boolean sendMessagingSettingsToTheHudBasedOnSharedPrefValue() {
        return sendMessagingSettingsToTheHud(MessagingSettingsActivity.getRepliesFromSharedPrefs(), getSharedPreferences().getLong("nav_serial_number", 0));
    }

    static boolean sendMessagingSettingsToTheHud(ArrayList<String> cannedReplies, Long serialNumber) {
        if (cannedReplies == null) {
            return false;
        }
        boolean deviceIsConnected;
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice == null || !remoteDevice.isConnected()) {
            deviceIsConnected = false;
        } else {
            deviceIsConnected = true;
        }
        if (deviceIsConnected) {
            CannedMessagesUpdate.Builder cannedResponses = new CannedMessagesUpdate.Builder();
            cannedResponses.status = RequestStatus.REQUEST_SUCCESS;
            cannedResponses.serial_number = serialNumber;
            cannedResponses.cannedMessage = cannedReplies;
            return remoteDevice.postEvent(cannedResponses.build());
        }
        setUpdateSettingsUponNextConnection(true);
        return false;
    }

    public static SharedPreferences getSharedPreferences() {
        return NavdyApplication.getAppContext().getSharedPreferences(SettingsConstants.SETTINGS, 0);
    }

    public static SharedPreferences getCustomerPreferences() {
        return NavdyApplication.getAppContext().getSharedPreferences(ProfilePreferences.CUSTOMER_PREF_FILE, 0);
    }

    public static SharedPreferences getPermissionsSharedPreferences() {
        return NavdyApplication.getAppContext().getSharedPreferences(SettingsConstants.PERMISSIONS, 0);
    }

    public static long incrementSerialNumber(String serialNumKey) {
        SharedPreferences sharedPrefs = getSharedPreferences();
        long serialNumber = sharedPrefs.getLong(serialNumKey, 0) + 1;
        sharedPrefs.edit().putLong(serialNumKey, serialNumber).apply();
        return serialNumber;
    }

    public static String getDialerPackage(PackageManager packageManager) {
        Intent i = new Intent("android.intent.action.CALL");
        i.setData(Uri.parse("tel:123456789"));
        ResolveInfo resolveInfo = packageManager.resolveActivity(i, 0);
        if (resolveInfo != null) {
            ActivityInfo activityInfo = resolveInfo.activityInfo;
            if (activityInfo != null) {
                return activityInfo.packageName;
            }
        }
        return "com.android.dialer";
    }

    public static String getSmsPackage() {
        return Sms.getDefaultSmsPackage(NavdyApplication.getAppContext());
    }

    public static boolean sendGlancesSettingsToTheHud(NotificationPreferences notifPrefs) {
        if (notifPrefs == null) {
            return false;
        }
        boolean deviceIsConnected;
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        deviceIsConnected = remoteDevice != null && remoteDevice.isConnected();
        if (deviceIsConnected) {
            NotificationPreferencesUpdate.Builder updateBldr = new NotificationPreferencesUpdate.Builder();
            updateBldr.status = RequestStatus.REQUEST_SUCCESS;
            updateBldr.serial_number = notifPrefs.serial_number;
            updateBldr.preferences = notifPrefs;
            return remoteDevice.postEvent(updateBldr.build());
        }
        setUpdateSettingsUponNextConnection(true);
        return false;
    }

    public static String getCarYearMakeModelString() {
        SharedPreferences customerPrefs = getCustomerPreferences();
        String carYear = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String carMake = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String carModel = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        return String.format("%s %s %s", carYear, carMake, carModel).trim();
    }

    public static boolean carIsBlacklistedForObdData() {
        Exception e;
        Throwable th;
        SharedPreferences customerPrefs = getCustomerPreferences();
        String makeFromSharedPref = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String yearFromSharedPref = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String modelFromSharedPref = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        boolean modelMatches = false;
        boolean makeMatches = false;
        boolean yearMatches = false;
        if (StringUtils.isEmptyAfterTrim(makeFromSharedPref) || StringUtils.isEmptyAfterTrim(yearFromSharedPref) || StringUtils.isEmptyAfterTrim(modelFromSharedPref)) {
            logger.d("No car model was found");
            return false;
        }
        makeFromSharedPref = makeFromSharedPref.trim();
        yearFromSharedPref = yearFromSharedPref.trim();
        modelFromSharedPref = modelFromSharedPref.trim();
        JsonReader reader = null;
        try {
            JsonReader reader2 = new JsonReader(new InputStreamReader(NavdyApplication.getAppContext().getResources().openRawResource(R.raw.blacklist)));
            while (reader2.hasNext()) {
                try {
                    if (reader2.peek() == JsonToken.BEGIN_ARRAY) {
                        reader2.beginArray();
                    }
                    if (reader2.peek() == JsonToken.BEGIN_OBJECT) {
                        reader2.beginObject();
                    }
                    while (reader2.hasNext()) {
                        String name = reader2.nextName();
                        switch (name) {
                            case MODEL:
                                String model = reader2.nextString();
                                if (!StringUtils.isEmptyAfterTrim(model)) {
                                    modelMatches = model.equalsIgnoreCase(modelFromSharedPref);
                                    break;
                                }
                                break;
                            case MAKE:
                                String make = reader2.nextString();
                                if (!StringUtils.isEmptyAfterTrim(make)) {
                                    makeMatches = make.equalsIgnoreCase(makeFromSharedPref);
                                    break;
                                }
                                break;
                            case YEAR:
                                String year = reader2.nextString();
                                if (!StringUtils.isEmptyAfterTrim(year)) {
                                    yearMatches = year.equalsIgnoreCase(yearFromSharedPref);
                                    break;
                                }
                                break;
                            default:
                                reader2.skipValue();
                                break;
                        }
                    }
                    reader2.endObject();
                    if (makeMatches && yearMatches && modelMatches) {
                        logger.d("car is blacklisted: " + makeFromSharedPref + DDL.SEPARATOR + modelFromSharedPref + DDL.SEPARATOR + yearFromSharedPref);
                        IOUtils.closeStream(reader2);
                        return true;
                    }
                } catch (Exception e2) {
                    e = e2;
                    reader = reader2;
                    try {
                        logger.e("Exception found: " + e);
                        IOUtils.closeStream(reader);
                        logger.d("car is not blacklisted: " + makeFromSharedPref + DDL.SEPARATOR + modelFromSharedPref + DDL.SEPARATOR + yearFromSharedPref);
                        return false;
                    } catch (Throwable th2) {
                        th = th2;
                        IOUtils.closeStream(reader);
                        logger.e("Exception found: " + th);
                    }
                } catch (Throwable th3) {
                    th = th3;
                    reader = reader2;
                    IOUtils.closeStream(reader);
                    logger.e("Exception found: " + th);
                }
            }
            reader2.endArray();
            IOUtils.closeStream(reader2);
            reader = reader2;
        } catch (Exception e3) {
            e = e3;
            logger.e("Exception found: " + e);
            IOUtils.closeStream(reader);
            logger.d("car is not blacklisted: " + makeFromSharedPref + DDL.SEPARATOR + modelFromSharedPref + DDL.SEPARATOR + yearFromSharedPref);
            return false;
        }
        logger.d("car is not blacklisted: " + makeFromSharedPref + DDL.SEPARATOR + modelFromSharedPref + DDL.SEPARATOR + yearFromSharedPref);
        return false;
    }

    @WorkerThread
    @NonNull
    public static MountInfo getMountInfo() {
        Exception e;
        Throwable th;
        SharedPreferences customerPrefs = getCustomerPreferences();
        String yearFromSharedPref = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String makeFromSharedPref = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String modelFromSharedPref = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        if (TextUtils.equals(lastYear, yearFromSharedPref) && TextUtils.equals(lastMake, makeFromSharedPref) && TextUtils.equals(lastModel, modelFromSharedPref)) {
            return lastMountInfo;
        }
        boolean modelMatches = false;
        boolean makeMatches = false;
        boolean yearMatches = false;
        if (StringUtils.isEmptyAfterTrim(makeFromSharedPref) || StringUtils.isEmptyAfterTrim(yearFromSharedPref) || StringUtils.isEmptyAfterTrim(modelFromSharedPref)) {
            logger.d("No car model was found");
            lastMountInfo = new MountInfo();
            return lastMountInfo;
        }
        makeFromSharedPref = makeFromSharedPref.trim();
        yearFromSharedPref = yearFromSharedPref.trim();
        modelFromSharedPref = modelFromSharedPref.trim();
        JsonReader reader = null;
        try {
            JsonReader reader2 = new JsonReader(new InputStreamReader(NavdyApplication.getAppContext().getResources().openRawResource(R.raw.mount_compatibility)));
            while (reader2.hasNext()) {
                try {
                    if (reader2.peek() == JsonToken.BEGIN_ARRAY) {
                        reader2.beginArray();
                    }
                    if (reader2.peek() == JsonToken.BEGIN_OBJECT) {
                        reader2.beginObject();
                    }
                    MountInfo mountInfo = new MountInfo();
                    while (reader2.hasNext()) {
                        String name = reader2.nextName();
                        switch (name) {
                            case MODEL:
                                String model = reader2.nextString();
                                if (!StringUtils.isEmptyAfterTrim(model)) {
                                    modelMatches = model.equalsIgnoreCase(modelFromSharedPref);
                                    break;
                                }
                                break;
                            case MAKE:
                                String make = reader2.nextString();
                                if (!StringUtils.isEmptyAfterTrim(make)) {
                                    makeMatches = make.equalsIgnoreCase(makeFromSharedPref);
                                    break;
                                }
                                break;
                            case YEAR:
                                String year = reader2.nextString();
                                if (!StringUtils.isEmptyAfterTrim(year)) {
                                    yearMatches = year.equalsIgnoreCase(yearFromSharedPref);
                                    break;
                                }
                                break;
                            case FLAT_SUPPORTED:
                                mountInfo.shortSupported = reader2.nextBoolean();
                                break;
                            case MEDIUM_SUPPORTED:
                                mountInfo.mediumSupported = reader2.nextBoolean();
                                break;
                            case TALL_SUPPORTED:
                                mountInfo.tallSupported = reader2.nextBoolean();
                                break;
                            case RECOMMENDED:
                                mountInfo.recommendedMount = MountType.getMountTypeForName(reader2.nextString());
                                break;
                            default:
                                reader2.skipValue();
                                break;
                        }
                    }
                    reader2.endObject();
                    if (makeMatches && yearMatches && modelMatches) {
                        lastMountInfo = mountInfo;
                        IOUtils.closeStream(reader2);
                        return mountInfo;
                    }
                } catch (Exception e2) {
                    e = e2;
                    reader = reader2;
                    try {
                        logger.e("Exception found: " + e);
                        IOUtils.closeStream(reader);
                        logger.d("car is not blacklisted: " + makeFromSharedPref + DDL.SEPARATOR + modelFromSharedPref + DDL.SEPARATOR + yearFromSharedPref);
                        lastMountInfo = new MountInfo();
                        return lastMountInfo;
                    } catch (Throwable th2) {
                        th = th2;
                        IOUtils.closeStream(reader);
                        logger.e("Exception found: " + th);
                    }
                } catch (Throwable th3) {
                    th = th3;
                    reader = reader2;
                    IOUtils.closeStream(reader);
                    logger.e("Exception found: " + th);
                }
            }
            reader2.endArray();
            IOUtils.closeStream(reader2);
            reader = reader2;
        } catch (Exception e3) {
            e = e3;
            logger.e("Exception found: " + e);
            IOUtils.closeStream(reader);
            logger.d("car is not blacklisted: " + makeFromSharedPref + DDL.SEPARATOR + modelFromSharedPref + DDL.SEPARATOR + yearFromSharedPref);
            lastMountInfo = new MountInfo();
            return lastMountInfo;
        }
        logger.d("car is not blacklisted: " + makeFromSharedPref + DDL.SEPARATOR + modelFromSharedPref + DDL.SEPARATOR + yearFromSharedPref);
        lastMountInfo = new MountInfo();
        return lastMountInfo;
    }

    public static boolean isUsingDefaultObdScanSetting() {
        ObdScanSetting obdScanSetting = ObdScanSetting.valueOf(getSharedPreferences().getString(SettingsConstants.HUD_OBD_ON, SettingsConstants.HUD_OBD_ON_DEFAULT));
        return obdScanSetting.equals(ObdScanSetting.SCAN_DEFAULT_OFF) || obdScanSetting.equals(ObdScanSetting.SCAN_DEFAULT_ON);
    }

    public static boolean obdScanIsOn() {
        return ObdScanSetting.valueOf(getSharedPreferences().getString(SettingsConstants.HUD_OBD_ON, SettingsConstants.HUD_OBD_ON_DEFAULT)).equals(ObdScanSetting.SCAN_ON);
    }

    public static void setDefaultObdSettingDependingOnBlacklistAsync() {
        logger.d("setDefaultObdSettingDependingOnBlacklistAsync");
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
                String obdScanSettingString = ObdScanSetting.SCAN_DEFAULT_OFF.toString();
                if (!SettingsUtils.carIsBlacklistedForObdData()) {
                    obdScanSettingString = ObdScanSetting.SCAN_DEFAULT_ON.toString();
                }
                SettingsUtils.logger.d("OBD Setting changed: " + obdScanSettingString);
                sharedPrefs.edit().putString(SettingsConstants.HUD_OBD_ON, obdScanSettingString).apply();
                Tracker.tagEvent(Settings.PROFILE_SETTINGS_CHANGED);
                SettingsUtils.incrementDriverProfileSerial();
                SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            }
        }, 1);
    }

    public static void checkObdSettingIfBuildVersionChanged() {
        SharedPreferences sharedPrefs = getSharedPreferences();
        if (!StringUtils.equalsOrBothEmptyAfterTrim(sharedPrefs.getString(SettingsConstants.LAST_BUILD_VERSION, ""), "1.0")) {
            logger.d("build version has changed. Checking obd setting");
            if (isUsingDefaultObdScanSetting()) {
                setDefaultObdSettingDependingOnBlacklistAsync();
                return;
            }
            Editor editor = sharedPrefs.edit();
            boolean overrodeBlacklist = sharedPrefs.getBoolean(SettingsConstants.HUD_BLACKLIST_OVERRIDDEN, SettingsConstants.HUD_BLACKLIST_OVERRIDDEN_DEFAULT);
            logger.d("overrodeBlacklist: " + overrodeBlacklist);
            if (carIsBlacklistedForObdData() && !overrodeBlacklist) {
                editor.putString(SettingsConstants.HUD_OBD_ON, ObdScanSetting.SCAN_DEFAULT_OFF.toString());
            }
            editor.putString(SettingsConstants.LAST_BUILD_VERSION, "1.0");
            editor.apply();
        }
    }

    private static long getObdBlacklistLastModifiedDate() {
        Exception e;
        Throwable th;
        String modifiedDate = "0";
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = NavdyApplication.getAppContext().getResources().openRawResource(R.raw.modified);
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(inputStream));
            try {
                StringBuilder stringBuilder = new StringBuilder();
                while (true) {
                    String newLine = bufferedReader2.readLine();
                    if (newLine == null) {
                        break;
                    }
                    stringBuilder.append(newLine);
                }
                modifiedDate = stringBuilder.toString();
                IOUtils.closeStream(inputStream);
                IOUtils.closeStream(bufferedReader2);
                bufferedReader = bufferedReader2;
            } catch (Exception e2) {
                e = e2;
                bufferedReader = bufferedReader2;
                try {
                    logger.e("Exception found: " + e);
                    IOUtils.closeStream(inputStream);
                    IOUtils.closeStream(bufferedReader);
                    logger.d("the last modified date: " + modifiedDate);
                    return Long.parseLong(modifiedDate);
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeStream(inputStream);
                    IOUtils.closeStream(bufferedReader);
                    logger.e("Exception found: " + th);
                }
            } catch (Throwable th3) {
                th = th3;
                bufferedReader = bufferedReader2;
                IOUtils.closeStream(inputStream);
                IOUtils.closeStream(bufferedReader);
                logger.e("Exception found: " + th);
            }
        } catch (Exception e3) {
            e = e3;
            logger.e("Exception found: " + e);
            IOUtils.closeStream(inputStream);
            IOUtils.closeStream(bufferedReader);
            logger.d("the last modified date: " + modifiedDate);
            return Long.parseLong(modifiedDate);
        }
        logger.d("the last modified date: " + modifiedDate);
        return Long.parseLong(modifiedDate);
    }

    public static void checkLegacyCapabilityList(List<LegacyCapability> legacyCapabilityList) {
        if (legacyCapabilityList == null || legacyCapabilityList.isEmpty()) {
            logger.d("capability list is empty");
            return;
        }
        SharedPreferences sharedPrefs = getSharedPreferences();
        boolean compactCapable = SettingsConstants.HUD_COMPACT_CAPABLE_DEFAULT;
        boolean voiceSearchCapable = SettingsConstants.HUD_VOICE_SEARCH_CAPABLE_DEFAULT;
        boolean musicCapable = SettingsConstants.HUD_LOCAL_MUSIC_BROWSER_CAPABLE_DEFAULT;
        for (LegacyCapability legacyCapability : legacyCapabilityList) {
            if (legacyCapability.equals(LegacyCapability.CAPABILITY_COMPACT_UI)) {
                compactCapable = true;
            } else if (legacyCapability.equals(LegacyCapability.CAPABILITY_VOICE_SEARCH)) {
                voiceSearchCapable = true;
            } else if (legacyCapability.equals(LegacyCapability.CAPABILITY_LOCAL_MUSIC_BROWSER)) {
                musicCapable = true;
            }
        }
        processCapabilities(sharedPrefs, compactCapable, voiceSearchCapable, musicCapable, SettingsConstants.HUD_SEARCH_RESULT_LIST_CAPABLE_DEFAULT, SettingsConstants.HUD_CANNED_RESPONSE_CAPABLE_DEFAULT, SettingsConstants.HUD_DIAL_LONG_PRESS_CAPABLE_DEFAULT);
    }

    public static void checkCapabilities(Capabilities capabilities) {
        if (capabilities == null) {
            logger.d("device info has no capabilities.");
        } else {
            processCapabilities(getSharedPreferences(), Boolean.TRUE.equals(capabilities.compactUi), Boolean.TRUE.equals(capabilities.voiceSearch), Boolean.TRUE.equals(capabilities.localMusicBrowser), Boolean.TRUE.equals(capabilities.searchResultList), Boolean.TRUE.equals(capabilities.cannedResponseToSms), Boolean.TRUE.equals(capabilities.customDialLongPress));
        }
    }

    private static void processCapabilities(SharedPreferences sharedPrefs, boolean compactCapable, boolean voiceSearchCapable, boolean musicCapable, boolean searchResultListCapable, boolean cannedResponseToSms, boolean customDialLongPress) {
        logger.i("device capabilities updated.");
        sharedPrefs.edit().putBoolean(SettingsConstants.HUD_COMPACT_CAPABLE, compactCapable).putBoolean(SettingsConstants.HUD_VOICE_SEARCH_CAPABLE, voiceSearchCapable).putBoolean(SettingsConstants.HUD_LOCAL_MUSIC_BROWSER_CAPABLE, musicCapable).putBoolean(SettingsConstants.HUD_SEARCH_RESULT_LIST_CAPABLE, searchResultListCapable).putBoolean(SettingsConstants.HUD_CANNED_RESPONSE_CAPABLE, cannedResponseToSms).putBoolean(SettingsConstants.HUD_DIAL_LONG_PRESS_CAPABLE, customDialLongPress).apply();
        if (Boolean.TRUE.equals(cannedResponseToSms)) {
            getSharedPreferences().edit().putBoolean(SettingsConstants.MESSAGING_HAS_SEEN_CAPABLE_HUD, SettingsConstants.MESSAGING_HAS_SEEN_CAPABLE_HUD_DEFAULT).apply();
        }
    }
}
