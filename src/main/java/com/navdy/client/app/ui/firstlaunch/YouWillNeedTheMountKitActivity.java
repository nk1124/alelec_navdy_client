package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.models.MountInfo;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class YouWillNeedTheMountKitActivity extends BaseActivity {
    public static final String MOUNT_UPSELL_URL = "https://shop.navdy.com/products/mount-kit?utm_source=navdy-app&utm_medium=upsell";
    private MountInfo mountInfo;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_you_will_need_the_mount_kit);
        String modelString = SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.CAR_MODEL, "");
        TextView description = (TextView) findViewById(R.id.you_will_need_the_mount_kit_description);
        if (description != null) {
            description.setText(StringUtils.fromHtml(R.string.you_will_need_the_mount_kit_description, modelString));
        }
        loadImage(R.id.illustration, R.drawable.asset_install_mount_kit);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        Tracker.tagScreen(Install.MOUNT_KIT_NEEDED);
        this.mountInfo = SettingsUtils.getMountInfo();
    }

    public void onDescriptionClick(View view) {
        Intent intent;
        if (this.mountInfo.shortSupported) {
            Tracker.tagEvent(Event.Install.CONTINUE_WITH_UNRECOMMENDED_SHORT_MOUNT);
            intent = new Intent(getApplicationContext(), InstallActivity.class);
            intent.putExtra("extra_mount", MountType.SHORT.getValue());
        } else {
            Tracker.tagEvent(Event.Install.CONTINUE_WITH_UNSUPPORTED_SHORT_MOUNT);
            intent = new Intent(getApplicationContext(), MountNotRecommendedActivity.class);
            intent.putExtra("extra_mount", MountType.SHORT.getValue());
        }
        startActivity(intent);
    }

    public void onUserHasMediumMountClick(View view) {
        Tracker.tagEvent(Event.Install.ACTUALLY_I_ALREADY_HAVE_MED_TALL_MOUNT);
        SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.BOX, "New_Box_Plus_Mounts").apply();
        Intent intent = new Intent(getApplicationContext(), InstallActivity.class);
        intent.putExtra("extra_mount", MountType.TALL.getValue());
        startActivity(intent);
    }

    public void onPurchaseClick(View view) {
        Tracker.tagScreen(Install.MOUNT_KIT_PURCHASE);
        Tracker.tagEvent(Event.Install.PURCHASE_MOUNT_KIT);
        openBrowserFor(Uri.parse(MOUNT_UPSELL_URL));
    }

    public void onBackClick(View view) {
        onBackPressed();
    }
}
