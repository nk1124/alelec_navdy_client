package com.navdy.client.app.framework.servicehandler;

import android.content.ContentResolver;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.here.odnp.config.OdnpConfigStatic;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.MusicDbUtils;
import com.navdy.client.app.framework.util.MusicUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.framework.util.ThrottlingLogger;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.MusicAttributes;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.audio.MusicArtworkRequest;
import com.navdy.service.library.events.audio.MusicArtworkResponse;
import com.navdy.service.library.events.audio.MusicCapabilitiesRequest;
import com.navdy.service.library.events.audio.MusicCapabilitiesResponse;
import com.navdy.service.library.events.audio.MusicCapability;
import com.navdy.service.library.events.audio.MusicCapability.MusicAuthorizationStatus;
import com.navdy.service.library.events.audio.MusicCharacterMap;
import com.navdy.service.library.events.audio.MusicCollectionInfo;
import com.navdy.service.library.events.audio.MusicCollectionRequest;
import com.navdy.service.library.events.audio.MusicCollectionResponse;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicEvent;
import com.navdy.service.library.events.audio.MusicEvent.Action;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicTrackInfo.Builder;
import com.navdy.service.library.events.audio.MusicTrackInfoRequest;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.events.photo.PhotoUpdate;
import com.navdy.service.library.events.photo.PhotoUpdatesRequest;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.MusicDataUtils;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities.ScalingLogic;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okio.ByteString;
import org.droidparts.contract.SQL.DDL;

public class MusicServiceHandler {
    private static final Logger sLogger = new Logger(MusicServiceHandler.class);
    public static final MusicTrackInfo EMPTY_TRACK_INFO = new Builder().dataSource(MusicDataSource.MUSIC_SOURCE_NONE).playbackState(MusicPlaybackState.PLAYBACK_NONE).build();
    public static final int INDEX_PLAY_LIST_RETRY_INTERVAL = 10000;
    private static final int MAX_RETRIES_TO_INDEX_PLAY_LISTS = 5;
    public static final int PLAY_LIST_INDEXING_DELAY_MILLIS = 5000;
    private static final int THROTTLING_LOGGER_WAIT = 5000;
    private static MusicServiceHandler singleton = null;
    private static final ThrottlingLogger throttlingLogger = new ThrottlingLogger(sLogger, OdnpConfigStatic.MIN_ALARM_TIMER_INTERVAL);
    private String currentArtworkHash;
    private Bitmap currentMediaArtwork = null;
    @NonNull
    private MusicTrackInfo currentMediaTrackInfo = EMPTY_TRACK_INFO;
    private volatile int indexingRetries = 0;
    private Handler musicObserverHandler;
    private Looper musicObserverLooper;
    private MusicSeekHelper musicSeekHelper = null;
    private boolean postPhotoUpdates = false;
    private Runnable retryIndexingPlayListsRunnable = new Runnable() {
        public void run() {
            MusicServiceHandler.this.indexingRetries = MusicServiceHandler.this.indexingRetries + 1;
            MusicServiceHandler.sLogger.e("Retrying initialization of the playlist data base");
            MusicServiceHandler.this.indexAllPlaylists(true);
        }
    };
    private boolean shouldPerformFullPlaylistIndex = true;

    public interface MusicSeekHelper {
        void executeMusicSeekAction(Action action);
    }

    private class GpmPlaylistObserver extends ContentObserver {
        Pattern pattern = Pattern.compile("/playlists/(\\d+)/?.*");
        GpmObserverRunnable runnable = new GpmObserverRunnable();

        private class GpmObserverRunnable implements Runnable {
            Integer playlistId;

            private GpmObserverRunnable() {
            }

//            /* synthetic */ GpmObserverRunnable(GpmPlaylistObserver x0, AnonymousClass1 x1) {
//                this();
//            }

            public void run() {
                if (this.playlistId != null) {
                    MusicServiceHandler.this.indexPlaylist(this.playlistId.intValue());
                } else {
                    MusicServiceHandler.this.indexAllPlaylists(true);
                }
            }

            void setPlaylistId(Integer playlistId) {
                this.playlistId = playlistId;
            }
        }

        GpmPlaylistObserver() {
            super(MusicServiceHandler.this.musicObserverHandler);
        }

        public void onChange(boolean selfChange, Uri uri) {
            String path = uri.getPath();
            MusicServiceHandler.sLogger.d("onChange: " + path + DDL.OPENING_BRACE + uri + ")");
            if (StringUtils.isEmptyAfterTrim(path)) {
                MusicServiceHandler.this.musicObserverHandler.removeCallbacks(this.runnable);
                this.runnable.setPlaylistId(null);
                MusicServiceHandler.this.musicObserverHandler.postDelayed(this.runnable, 200);
                return;
            }
            Matcher matcher = this.pattern.matcher(path);
            if (matcher.matches()) {
                MusicServiceHandler.sLogger.d("Match: " + matcher.group());
                String playlistIdStr = matcher.group(1);
                try {
                    int playlistId = Integer.parseInt(playlistIdStr);
                    MusicServiceHandler.this.musicObserverHandler.removeCallbacks(this.runnable);
                    this.runnable.setPlaylistId(Integer.valueOf(playlistId));
                    MusicServiceHandler.this.musicObserverHandler.postDelayed(this.runnable, 200);
                } catch (NumberFormatException e) {
                    MusicServiceHandler.sLogger.e("Can't convert to integer: " + playlistIdStr);
                }
            }
        }
    }

    public MusicSeekHelper getMusicSeekHelper() {
        return this.musicSeekHelper;
    }

    public void setMusicSeekHelper(MusicSeekHelper musicSeekHelper) {
        this.musicSeekHelper = musicSeekHelper;
    }

    private MusicServiceHandler() {
        sLogger.d("MusicServiceHandler ctor");
        try {
            GpmPlaylistObserver gpmPlaylistObserver = new GpmPlaylistObserver();
            NavdyApplication.getAppContext().getContentResolver().registerContentObserver(MusicDbUtils.getGpmPlaylistsUri(), true, gpmPlaylistObserver);
        } catch ( java.lang.SecurityException ex) {
            MusicServiceHandler.sLogger.e("Error connecting to Google Play Music: " + ex);
        }

        BusProvider.getInstance().register(this);
        HandlerThread thread = new HandlerThread("MusicObserver");
        thread.start();
        this.musicObserverLooper = thread.getLooper();
        this.musicObserverHandler = new Handler(this.musicObserverLooper);
        this.musicObserverHandler.postDelayed(new Runnable() {
            public void run() {
                    MusicServiceHandler.this.indexAllPlaylists(true);
                    }
            }, OdnpConfigStatic.MIN_ALARM_TIMER_INTERVAL);
    }

    protected void finalize() throws Throwable {
        super.finalize();
        this.musicObserverLooper.quit();
    }

    public static synchronized MusicServiceHandler getInstance() {
        MusicServiceHandler musicServiceHandler;
        synchronized (MusicServiceHandler.class) {
            if (singleton == null) {
                singleton = new MusicServiceHandler();
            }
            musicServiceHandler = singleton;
        }
        return musicServiceHandler;
    }

    @Subscribe
    public void onHUDDisconnected(DeviceDisconnectedEvent event) {
        sLogger.v("HUD is disconnected - reset internal state");
        this.postPhotoUpdates = false;
        MusicUtils.executeLongPressedKeyUp();
        MusicUtils.stopInternalMusicPlayer();
    }

    @Subscribe
    public void onHUDConnected(DeviceConnectedEvent event) {
        sLogger.v("HUD is connected - doing some initial stuff for music");
        this.postPhotoUpdates = true;
    }

    @Subscribe
    public void onMusicCollectionRequest(final MusicCollectionRequest request) {
        TaskManager.getInstance().execute(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:156:0x05b9  */
            /* JADX WARNING: Removed duplicated region for block: B:204:0x0707  */
            /* JADX WARNING: Removed duplicated region for block: B:240:0x0819  */
            /* JADX WARNING: Removed duplicated region for block: B:240:0x0819  */
            /* JADX WARNING: Removed duplicated region for block: B:204:0x0707  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                Throwable t;
                Throwable th;
                MusicServiceHandler.sLogger.d("onMusicCollectionRequest " + request);
                int offset = request.offset != null ? request.offset.intValue() : 0;
                int limit = request.limit != null ? request.limit.intValue() : 0;
                int count = 0;
                boolean canShuffle = true;
                List<MusicCollectionInfo> collectionList = null;
                List<MusicTrackInfo> trackList = null;
                List<MusicCharacterMap> characterMap = null;
                if (MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL.equals(request.collectionSource)) {
                    Cursor membersCursor;
                    boolean groupByAlbums;
                    Cursor albumsCursor;
                    List<MusicCollectionInfo> collectionList2;
                    if (request.collectionId != null) {
                        List<MusicTrackInfo> arrayList;
                        if (MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(request.collectionType)) {
                            membersCursor = MusicDbUtils.getPlaylistMembersCursor(request.collectionId);
                            if (membersCursor != null) {
                                try {
                                    if (membersCursor.moveToPosition(offset)) {
                                        count = membersCursor.getCount();
                                        if (limit == 0) {
                                            limit = count;
                                        }
                                        arrayList = new ArrayList(limit);
                                        do {
                                            try {
                                                arrayList.add(new Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS).collectionId(request.collectionId).playbackState(MusicPlaybackState.PLAYBACK_NONE).name(membersCursor.getString(membersCursor.getColumnIndex("title"))).author(membersCursor.getString(membersCursor.getColumnIndex("artist"))).album(membersCursor.getString(membersCursor.getColumnIndex("album"))).trackId(String.valueOf(membersCursor.getInt(membersCursor.getColumnIndex("SourceId")))).build());
                                                if (!membersCursor.moveToNext()) {
                                                    break;
                                                }
                                            } catch (Throwable th2) {
                                                th = th2;
                                                trackList = arrayList;
                                                IOUtils.closeObject(membersCursor);
                                                throw th;
                                            }
                                        } while (arrayList.size() < limit);
                                        trackList = arrayList;
                                    }
                                } catch (Throwable th3) {
                                    t = th3;
                                    MusicServiceHandler.sLogger.e("Error querying GPM database: " + t);
                                    IOUtils.closeObject(membersCursor);
                                    DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(request.collectionSource).collectionType(request.collectionType).collectionId(request.collectionId).size(Integer.valueOf(count)).canShuffle(Boolean.valueOf(canShuffle)).build()).musicTracks(trackList).musicCollections(collectionList).characterMap(characterMap).build());
                                }
                            }
                            IOUtils.closeObject(membersCursor);
                        } else if (MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(request.collectionType)) {
                            groupByAlbums = Wire.get(request.groupBy, MusicCollectionType.COLLECTION_TYPE_UNKNOWN) == MusicCollectionType.COLLECTION_TYPE_ALBUMS;
                            albumsCursor = MusicDbUtils.getArtistsAlbums(request.collectionId);
                            Cursor artistTracksCursor = null;
                            if (groupByAlbums) {
                                if (albumsCursor != null) {
                                    try {
                                        if (albumsCursor.moveToPosition(offset)) {
                                            count = albumsCursor.getCount();
                                            if (limit == 0) {
                                                limit = count;
                                            }
                                            collectionList2 = new ArrayList(limit);
                                            do {
                                                try {
                                                    collectionList2.add(new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ALBUMS).collectionId(String.valueOf(albumsCursor.getString(albumsCursor.getColumnIndex("_id")))).name(albumsCursor.getString(albumsCursor.getColumnIndex("album"))).size(Integer.valueOf(albumsCursor.getInt(albumsCursor.getColumnIndex("numsongs")))).build());
                                                    if (!albumsCursor.moveToNext()) {
                                                        break;
                                                    }
                                                } catch (Throwable th4) {
                                                    th = th4;
                                                    collectionList = collectionList2;
                                                    IOUtils.closeObject(albumsCursor);
                                                    IOUtils.closeObject(artistTracksCursor);
                                                    throw th;
                                                }
                                            } while (collectionList2.size() < limit);
                                            collectionList = collectionList2;
                                        }
                                    } catch (Throwable th5) {
                                        t = th5;
                                        MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
                                        IOUtils.closeObject(albumsCursor);
                                        IOUtils.closeObject(artistTracksCursor);
                                        DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(request.collectionSource).collectionType(request.collectionType).collectionId(request.collectionId).size(Integer.valueOf(count)).canShuffle(Boolean.valueOf(canShuffle)).build()).musicTracks(trackList).musicCollections(collectionList).characterMap(characterMap).build());
                                    }
                                }
                                canShuffle = MusicDbUtils.getArtistSongsCount(request.collectionId) > 1;
                            } else {
                                artistTracksCursor = MusicDbUtils.getArtistMembersCursor(request.collectionId);
                                if (artistTracksCursor != null && artistTracksCursor.moveToPosition(offset)) {
                                    count = artistTracksCursor.getCount();
                                    if (limit == 0) {
                                        limit = count;
                                    }
                                    arrayList = new ArrayList(limit);
                                    do {
                                        try {
                                            arrayList.add(new Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ARTISTS).collectionId(request.collectionId).playbackState(MusicPlaybackState.PLAYBACK_NONE).name(artistTracksCursor.getString(artistTracksCursor.getColumnIndex("title"))).author(artistTracksCursor.getString(artistTracksCursor.getColumnIndex("artist"))).album(artistTracksCursor.getString(artistTracksCursor.getColumnIndex("album"))).trackId(String.valueOf(artistTracksCursor.getInt(artistTracksCursor.getColumnIndex("_id")))).build());
                                            if (!artistTracksCursor.moveToNext()) {
                                                break;
                                            }
                                        } catch (Throwable th6) {
                                            th = th6;
                                            trackList = arrayList;
                                            IOUtils.closeObject(albumsCursor);
                                            IOUtils.closeObject(artistTracksCursor);
                                            th.printStackTrace();
                                        }
                                    } while (arrayList.size() < limit);
                                    trackList = arrayList;
                                }
                                if (artistTracksCursor != null) {
                                    if (artistTracksCursor.moveToFirst() && artistTracksCursor.getCount() > 1) {
                                        canShuffle = true;
                                    }
                                }
                                canShuffle = false;
                            }
                            IOUtils.closeObject(albumsCursor);
                            IOUtils.closeObject(artistTracksCursor);
                        } else if (MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(request.collectionType)) {
                            membersCursor = MusicDbUtils.getAlbumMembersCursor(request.collectionId);
                            if (membersCursor != null) {
                                try {
                                    if (membersCursor.moveToPosition(offset)) {
                                        count = membersCursor.getCount();
                                        if (limit == 0) {
                                            limit = count;
                                        }
                                        arrayList = new ArrayList(limit);
                                        do {
                                            try {
                                                arrayList.add(new Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ALBUMS).collectionId(request.collectionId).playbackState(MusicPlaybackState.PLAYBACK_NONE).name(membersCursor.getString(membersCursor.getColumnIndex("title"))).author(membersCursor.getString(membersCursor.getColumnIndex("artist"))).album(membersCursor.getString(membersCursor.getColumnIndex("album"))).trackId(String.valueOf(membersCursor.getInt(membersCursor.getColumnIndex("_id")))).build());
                                                if (!membersCursor.moveToNext()) {
                                                    break;
                                                }
                                            } catch (Throwable th7) {
                                                th = th7;
                                                trackList = arrayList;
                                                IOUtils.closeObject(membersCursor);
                                                throw th;
                                            }
                                        } while (arrayList.size() < limit);
                                        trackList = arrayList;
                                    }
                                } catch (Throwable th8) {
                                    t = th8;
                                    MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
                                    IOUtils.closeObject(membersCursor);
                                    DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(request.collectionSource).collectionType(request.collectionType).collectionId(request.collectionId).size(Integer.valueOf(count)).canShuffle(Boolean.valueOf(canShuffle)).build()).musicTracks(trackList).musicCollections(collectionList).characterMap(characterMap).build());
                                }
                            }
                            IOUtils.closeObject(membersCursor);
                        }
                    } else if (MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(request.collectionType)) {
                        Cursor playlistsCursor = MusicDbUtils.getPlaylistsCursor();
                        if (playlistsCursor != null) {
                            try {
                                if (playlistsCursor.moveToPosition(offset)) {
                                    count = playlistsCursor.getCount();
                                    if (limit == 0) {
                                        limit = count;
                                    }
                                    collectionList2 = new ArrayList(limit);
                                    do {
                                        try {
                                            int playlistId = playlistsCursor.getInt(playlistsCursor.getColumnIndex("playlist_id"));
                                            String playlistName = playlistsCursor.getString(playlistsCursor.getColumnIndex("playlist_name"));
                                            membersCursor = MusicDbUtils.getPlaylistMembersCursor(playlistId);
                                            if (membersCursor != null) {
                                                if (membersCursor.moveToFirst()) {
                                                    collectionList2.add(new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS).collectionId(String.valueOf(playlistId)).name(playlistName).size(Integer.valueOf(membersCursor.getCount())).build());
                                                }
                                            }
                                            IOUtils.closeObject(membersCursor);
                                            if (!playlistsCursor.moveToNext()) {
                                                break;
                                            }
                                        } catch (Throwable th9) {
                                            th = th9;
                                            collectionList = collectionList2;
                                        }
                                    } while (collectionList2.size() < limit);
                                    collectionList = collectionList2;
                                }
                            } catch (Throwable th10) {
                                t = th10;
                                try {
                                    MusicServiceHandler.sLogger.e("Error querying GPM database: " + t);
                                    IOUtils.closeObject(playlistsCursor);
                                    if (((Boolean) Wire.get(request.includeCharacterMap, Boolean.valueOf(false))).booleanValue()) {
                                    }
                                    DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(request.collectionSource).collectionType(request.collectionType).collectionId(request.collectionId).size(Integer.valueOf(count)).canShuffle(Boolean.valueOf(canShuffle)).build()).musicTracks(trackList).musicCollections(collectionList).characterMap(characterMap).build());
                                } catch (Throwable th11) {
                                    th = th11;
                                    IOUtils.closeObject(playlistsCursor);
                                    th.printStackTrace();
                                }
                            }
                        }
                        IOUtils.closeObject(playlistsCursor);
                        if (((Boolean) Wire.get(request.includeCharacterMap, Boolean.valueOf(false))).booleanValue()) {
                            characterMap = MusicServiceHandler.this.createCharacterMap(MusicDbUtils.getPlaylistsCharacterMapCursor());
                        }
                    } else if (MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(request.collectionType)) {
                        Cursor artistsCursor = MusicDbUtils.getArtistsCursor();
                        groupByAlbums = Wire.get(request.groupBy, MusicCollectionType.COLLECTION_TYPE_UNKNOWN) == MusicCollectionType.COLLECTION_TYPE_ALBUMS;
                        if (artistsCursor != null) {
                            try {
                                if (artistsCursor.moveToPosition(offset)) {
                                    count = artistsCursor.getCount();
                                    if (limit == 0) {
                                        limit = count;
                                    }
                                    collectionList2 = new ArrayList(limit);
                                    do {
                                        try {
                                            String artistId = artistsCursor.getString(artistsCursor.getColumnIndex("_id"));
                                            String artistName = artistsCursor.getString(artistsCursor.getColumnIndex("artist"));
                                            membersCursor = groupByAlbums ? MusicDbUtils.getArtistsAlbums(artistId) : MusicDbUtils.getArtistMembersCursor(artistId);
                                            if (membersCursor != null) {
                                                if (membersCursor.moveToFirst()) {
                                                    int albumsCount = membersCursor.getCount();
                                                    MusicCollectionInfo.Builder infoBuilder = new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ARTISTS).collectionId(String.valueOf(artistId)).name(artistName).size(Integer.valueOf(membersCursor.getCount()));
                                                    if (groupByAlbums) {
                                                        infoBuilder.subtitle(NavdyApplication.getAppContext().getResources().getQuantityString(R.plurals.album_count, albumsCount, new Object[]{Integer.valueOf(albumsCount)}));
                                                    }
                                                    collectionList2.add(infoBuilder.build());
                                                }
                                            }
                                            IOUtils.closeObject(membersCursor);
                                            if (!artistsCursor.moveToNext()) {
                                                break;
                                            }
                                        } catch (Throwable th12) {
                                            th = th12;
                                            collectionList = collectionList2;
                                            IOUtils.closeObject(artistsCursor);
                                            throw th;
                                        }
                                    } while (collectionList2.size() < limit);
                                    collectionList = collectionList2;
                                }
                            } catch (Throwable th13) {
                                t = th13;
                                MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
                                IOUtils.closeObject(artistsCursor);
                                if (((Boolean) Wire.get(request.includeCharacterMap, Boolean.valueOf(false))).booleanValue()) {
                                }
                                DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(request.collectionSource).collectionType(request.collectionType).collectionId(request.collectionId).size(Integer.valueOf(count)).canShuffle(Boolean.valueOf(canShuffle)).build()).musicTracks(trackList).musicCollections(collectionList).characterMap(characterMap).build());
                            }
                        }
                        IOUtils.closeObject(artistsCursor);
                        if (((Boolean) Wire.get(request.includeCharacterMap, Boolean.valueOf(false))).booleanValue()) {
                            characterMap = MusicServiceHandler.this.createCharacterMap(MusicDbUtils.getArtistsCharacterMapCursor());
                        }
                    } else if (MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(request.collectionType)) {
                        albumsCursor = MusicDbUtils.getAlbumsCursor();
                        if (albumsCursor != null) {
                            try {
                                if (albumsCursor.moveToPosition(offset)) {
                                    count = albumsCursor.getCount();
                                    if (limit == 0) {
                                        limit = count;
                                    }
                                    collectionList2 = new ArrayList(limit);
                                    do {
                                        try {
                                            collectionList2.add(new MusicCollectionInfo.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionType(MusicCollectionType.COLLECTION_TYPE_ALBUMS).collectionId(String.valueOf(albumsCursor.getString(albumsCursor.getColumnIndex("_id")))).name(albumsCursor.getString(albumsCursor.getColumnIndex("album"))).size(Integer.valueOf(albumsCursor.getInt(albumsCursor.getColumnIndex("numsongs")))).subtitle(albumsCursor.getString(albumsCursor.getColumnIndex("artist"))).build());
                                            if (!albumsCursor.moveToNext()) {
                                                break;
                                            }
                                        } catch (Throwable th14) {
                                            th = th14;
                                            collectionList = collectionList2;
                                            IOUtils.closeObject(albumsCursor);
                                            throw th;
                                        }
                                    } while (collectionList2.size() < limit);
                                    collectionList = collectionList2;
                                }
                            } catch (Throwable th15) {
                                t = th15;
                                MusicServiceHandler.sLogger.e("Error querying MediaStore database: " + t);
                                IOUtils.closeObject(albumsCursor);
                                if (((Boolean) Wire.get(request.includeCharacterMap, Boolean.valueOf(false))).booleanValue()) {
                                }
                                DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(request.collectionSource).collectionType(request.collectionType).collectionId(request.collectionId).size(Integer.valueOf(count)).canShuffle(Boolean.valueOf(canShuffle)).build()).musicTracks(trackList).musicCollections(collectionList).characterMap(characterMap).build());
                            }
                        }
                        IOUtils.closeObject(albumsCursor);
                        if (((Boolean) Wire.get(request.includeCharacterMap, Boolean.valueOf(false))).booleanValue()) {
                            characterMap = MusicServiceHandler.this.createCharacterMap(MusicDbUtils.getAlbumsCharacterMapCursor());
                        }
                    }
                }
                DeviceConnection.postEvent(new MusicCollectionResponse.Builder().collectionInfo(new MusicCollectionInfo.Builder().collectionSource(request.collectionSource).collectionType(request.collectionType).collectionId(request.collectionId).size(Integer.valueOf(count)).canShuffle(Boolean.valueOf(canShuffle)).build()).musicTracks(trackList).musicCollections(collectionList).characterMap(characterMap).build());
            }
        }, 1);
    }

    private List<MusicCharacterMap> createCharacterMap(Cursor cursor) {
        Throwable t;
        Throwable th;
        List<MusicCharacterMap> characterMap = null;
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    List<MusicCharacterMap> characterMap2 = new ArrayList();
                    int characterOffset = 0;
                    boolean indexedNumbers = false;
                    boolean indexedSymbols = false;
                    do {
                        boolean shouldAdd = false;
                        try {
                            String c = cursor.getString(cursor.getColumnIndex(MusicDbUtils.LETTER_COLUMN));
                            int count = cursor.getInt(cursor.getColumnIndex(MusicDbUtils.COUNT_COLUMN));
                            if (StringUtils.isEmptyAfterTrim(c) || c.length() > 1) {
                                sLogger.e("Bad value in character map cursor: " + c);
                            } else {
                                if (c.matches("[A-Z]")) {
                                    shouldAdd = true;
                                } else if (c.matches("\\d")) {
                                    if (!indexedNumbers) {
                                        c = "#";
                                        indexedNumbers = true;
                                        shouldAdd = true;
                                    }
                                } else if (!indexedSymbols) {
                                    c = "%";
                                    indexedSymbols = true;
                                    shouldAdd = true;
                                }
                                if (shouldAdd) {
                                    characterMap2.add(new MusicCharacterMap.Builder().character(String.valueOf(c)).offset(Integer.valueOf(characterOffset)).build());
                                }
                                characterOffset += count;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            characterMap = characterMap2;
                            IOUtils.closeStream(cursor);
                            throw th;
                        }
                    } while (cursor.moveToNext());
                    characterMap = characterMap2;
                }
            } catch (Throwable th3) {
                t = th3;
                sLogger.e("Couldn't generate character map: " + t);
                IOUtils.closeStream(cursor);
                return characterMap;
            }
        }
        IOUtils.closeStream(cursor);
        return characterMap;
    }

    @Subscribe
    public void onMusicCapabilitiesRequest(MusicCapabilitiesRequest musicCapabilitiesRequest) {
        sLogger.d("onMusicCapabilitiesRequest");
        DeviceConnection.postEvent(getMusicCapabilities());
    }

    public static MusicCapabilitiesResponse getMusicCapabilities() {
        MusicAuthorizationStatus authorizationStatus;
        if (BaseActivity.weHaveStoragePermission()) {
            authorizationStatus = MusicAuthorizationStatus.MUSIC_AUTHORIZATION_AUTHORIZED;
        } else {
            authorizationStatus = MusicAuthorizationStatus.MUSIC_AUTHORIZATION_DENIED;
        }
        List<MusicCollectionType> collectionTypes = new ArrayList();
        collectionTypes.add(MusicCollectionType.COLLECTION_TYPE_PLAYLISTS);
        collectionTypes.add(MusicCollectionType.COLLECTION_TYPE_ARTISTS);
        collectionTypes.add(MusicCollectionType.COLLECTION_TYPE_ALBUMS);
        List<MusicCapability> capabilitiesList = new ArrayList();
        capabilitiesList.add(new MusicCapability.Builder().collectionSource(MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL).collectionTypes(collectionTypes).authorizationStatus(authorizationStatus).build());
        return new MusicCapabilitiesResponse.Builder().capabilities(capabilitiesList).build();
    }

    @Subscribe
    public void onMusicArtworkRequest(final MusicArtworkRequest request) {
        sLogger.d("onMusicArtworkRequest " + request);
        TaskManager.getInstance().execute(new Runnable() {
            /* JADX WARNING: Failed to extract finally block: empty outs */
            /* JADX WARNING: Failed to extract finally block: empty outs */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                if (StringUtils.isEmptyAfterTrim(request.collectionId)) {
                    MusicServiceHandler.sLogger.e("This artwork request is only for collections");
                    return;
                }
                try {
                    long collectionId = (long) Integer.parseInt(request.collectionId);
                    Bitmap bitmap = null;
                    ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
                    if (MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(request.collectionType)) {
                        bitmap = MusicUtils.getArtworkForAlbum(contentResolver, collectionId);
                    } else if (MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(request.collectionType)) {
                        bitmap = MusicUtils.getArtworkForArtist(contentResolver, collectionId);
                    } else if (MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(request.collectionType)) {
                        bitmap = MusicUtils.getArtworkForPlaylist(contentResolver, collectionId);
                    }
                    if (bitmap == null || bitmap.getByteCount() == 0) {
                        MusicServiceHandler.sLogger.i("Received photo has null or empty byte array");
                    }
                    Bitmap artwork = null;
                    ByteString photo = null;
                    if (bitmap != null) {
                        try {
                            artwork = ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(bitmap, request.size.intValue(), request.size.intValue(), ScalingLogic.FIT);
                            byte[] data = IOUtils.bitmap2ByteBuffer(artwork);
                            if (data == null) {
                                MusicServiceHandler.sLogger.e("Error artworking");
                                if (artwork != null) {
                                    artwork.recycle();
                                    return;
                                }
                                return;
                            }
                            photo = ByteString.of(data);
                        } catch (Exception e) {
                            MusicServiceHandler.sLogger.e("Error updating the artwork", e);
                            if (artwork != null) {
                                artwork.recycle();
                                return;
                            }
                            return;
                        } catch (Throwable th) {
                            if (artwork != null) {
                                artwork.recycle();
                            }
                            throw th;
                        }
                    }
                    DeviceConnection.postEvent(new MusicArtworkResponse.Builder().collectionSource(request.collectionSource).collectionType(request.collectionType).collectionId(request.collectionId).photo(photo).build());
                    if (artwork != null) {
                        artwork.recycle();
                    }
                } catch (Throwable e2) {
                    MusicServiceHandler.sLogger.e("Couldn't parse " + request.collectionId, e2);
                }
            }
        }, 1);
    }

    @Subscribe
    public void onMusicTrackInfoRequest(MusicTrackInfoRequest request) {
        sLogger.d("Request for track info received.");
        sendCurrentTrackInfo();
    }

    @Subscribe
    public void onMusicEvent(MusicEvent event) {
        sLogger.d("Request for performing some action to the music received.");
        try {
            MusicUtils.executeMusicAction(event);
        } catch (IOException e) {
            sLogger.e("Cannot load music", e);
        } catch (NameNotFoundException e2) {
            sLogger.e("Cannot start music player", e2);
        } catch (UnsupportedOperationException e3) {
            sLogger.e("Cannot execute action", e3);
        }
    }

    @Subscribe
    public void onPhotoUpdatesRequest(PhotoUpdatesRequest request) {
        sLogger.d("Request for changing photo updating status received with value " + String.valueOf(request.start) + " for imageResourceIds of type " + String.valueOf(request.photoType));
        if (PhotoType.PHOTO_ALBUM_ART.equals(request.photoType)) {
            this.postPhotoUpdates = request.start.booleanValue();
            sendCurrentMediaArtworkAsUpdate();
        }
    }

    private void sendRemoteMessage(Message message) {
        sLogger.v("Sending message to the HUD: " + String.valueOf(message));
        DeviceConnection.postEvent(message);
    }

    private void sendCurrentTrackInfo() {
        sendRemoteMessage(getCurrentMediaTrackInfo());
    }

    @NonNull
    public synchronized MusicTrackInfo getCurrentMediaTrackInfo() {
        return this.currentMediaTrackInfo;
    }

    public synchronized void setCurrentMediaTrackInfo(@Nullable MusicTrackInfo currentMediaTrackInfo) {
        sLogger.v("setCurrentMediaTrackInfo " + currentMediaTrackInfo);
        if (currentMediaTrackInfo == this.currentMediaTrackInfo) {
            sLogger.d("Got same track info object - disregarded");
        } else if (currentMediaTrackInfo == null) {
            sLogger.i("Got null track info object - cleaning");
            this.currentMediaTrackInfo = EMPTY_TRACK_INFO;
            setCurrentMediaArtwork(null, null);
            sendRemoteMessage(this.currentMediaTrackInfo);
        } else if (currentMediaTrackInfo.equals(this.currentMediaTrackInfo)) {
            throttlingLogger.i(1, "Got no new data in the update - disregarded");
        } else {
            this.currentMediaTrackInfo = currentMediaTrackInfo;
            sendRemoteMessage(this.currentMediaTrackInfo);
        }
    }

    private boolean isTheSameAsCurrentSong(MusicTrackInfo theOtherSong) {
        return StringUtils.equalsOrBothEmptyAfterTrim(MusicDataUtils.songIdentifierFromTrackInfo(theOtherSong), MusicDataUtils.songIdentifierFromTrackInfo(getCurrentMediaTrackInfo()));
    }

    public void checkAndSetCurrentMediaArtwork(MusicTrackInfo trackInfo, Bitmap artwork) {
        if (isTheSameAsCurrentSong(trackInfo)) {
            if (artwork == null) {
                sLogger.i("null set as current media artwork (artwork reset)");
            }
            String artworkHash = IOUtils.hashForBitmap(artwork);
            sLogger.v("Got album artwork with hash " + artworkHash);
            if (MusicUtils.isDefaultArtwork(artworkHash)) {
                sLogger.i("default album art found - ignored");
                return;
            } else {
                setCurrentMediaArtwork(artwork, artworkHash);
                return;
            }
        }
        sLogger.w("Tried to set artwork from the wrong song - ignored");
    }

    private synchronized void setCurrentMediaArtwork(Bitmap artwork, String hash) {
        if (StringUtils.equalsOrBothEmptyAfterTrim(this.currentArtworkHash, hash)) {
            sLogger.i("Album art is identical to current, ignoring");
        } else {
            this.currentMediaArtwork = artwork;
            this.currentArtworkHash = hash;
            sendCurrentMediaArtworkAsUpdate();
        }
    }

    public synchronized Bitmap getCurrentMediaArtwork() {
        return this.currentMediaArtwork;
    }

    private void sendCurrentMediaArtworkAsUpdate() {
        sLogger.i("sendCurrentMediaArtworkAsUpdate, postPhotoUpdates: " + this.postPhotoUpdates);
        if (this.postPhotoUpdates) {
            sLogger.v("Sending music artwork photo update");
            PhotoUpdate.Builder builder = new PhotoUpdate.Builder(null);
            builder.photoType(PhotoType.PHOTO_ALBUM_ART);
            builder.identifier(MusicDataUtils.photoIdentifierFromTrackInfo(getCurrentMediaTrackInfo()));
            Bitmap art = getCurrentMediaArtwork();
            sLogger.d("State of currentMediaArtwork: " + art);
            if (art != null) {
                builder = builder.photo(ByteString.of(IOUtils.bitmap2ByteBuffer(art)));
            }
            if (builder.photo == null) {
                sLogger.e("Photo within the photo update is null");
            }
            sendRemoteMessage(builder.build());
        }
    }

    public void setLastMusicApp(String packageName) {
        sLogger.d("setLastMusicApp " + packageName);
        SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.LAST_MEDIA_APP, packageName).apply();
    }

    @Nullable
    public String getLastMusicApp() {
        String packageName = SettingsUtils.getSharedPreferences().getString(SettingsConstants.LAST_MEDIA_APP, null);
        sLogger.d("getLastMusicApp " + packageName);
        return packageName;
    }

    public boolean isMusicPlayerActive() {
        return !EMPTY_TRACK_INFO.equals(this.currentMediaTrackInfo);
    }

    public void indexPlaylists() {
        this.musicObserverHandler.post(new Runnable() {
            public void run() {
                MusicServiceHandler.this.indexAllPlaylists(false);
            }
        });
    }

    @WorkerThread
    private void indexAllPlaylists(boolean initializing) {
        sLogger.d("indexAllPlaylists (" + initializing + ")");
        SystemUtils.ensureNotOnMainThread();
        Cursor playlistsCursor = null;
        try {
            NavdyContentProvider.deleteAllPlaylists();
            int analyticsPlaylistCount = 0;
            int analyticsTrackCount = 0;
            playlistsCursor = MusicDbUtils.getGpmPlaylistsCursor();
            if (playlistsCursor != null && playlistsCursor.moveToFirst()) {
                this.indexingRetries = 0;
                this.shouldPerformFullPlaylistIndex = false;
                do {
                    analyticsTrackCount += indexPlaylistMembers(playlistsCursor.getInt(playlistsCursor.getColumnIndex("_id")), playlistsCursor.getString(playlistsCursor.getColumnIndex("playlist_name")));
                    analyticsPlaylistCount++;
                } while (playlistsCursor.moveToNext());
                Map<String, String> attributes = new HashMap(2);
                attributes.put(MusicAttributes.NUM_GPM_PLAYLISTS_INDEXED, String.valueOf(analyticsPlaylistCount));
                attributes.put(MusicAttributes.NUM_GPM_TRACKS_INDEXED, String.valueOf(analyticsTrackCount));
                Tracker.tagEvent(Event.MUSIC_PLAYLIST_COMPLETE_REINDEX, attributes);
            } else if (playlistsCursor == null) {
                sLogger.e("Cursor returned is null, we will retry, Retries : " + this.indexingRetries);
                if (!initializing || this.indexingRetries > 5) {
                    sLogger.e("Not retrying as the maximum retries has reached");
                } else {
                    this.musicObserverHandler.removeCallbacks(this.retryIndexingPlayListsRunnable);
                    this.musicObserverHandler.postDelayed(this.retryIndexingPlayListsRunnable, 10000);
                }
            }
            IOUtils.closeStream(playlistsCursor);
        } catch (Throwable th) {
            IOUtils.closeStream(playlistsCursor);
            throw th;
        }
    }

    @WorkerThread
    private void indexPlaylist(int playlistId) {
        sLogger.d("indexPlaylist: " + playlistId);
        SystemUtils.ensureNotOnMainThread();
        Cursor cursor = null;
        try {
            NavdyContentProvider.deletePlaylist(playlistId);
            cursor = MusicDbUtils.getGpmPlaylistCursor(playlistId);
            if (cursor != null && cursor.moveToFirst()) {
                String playlistName = cursor.getString(cursor.getColumnIndex("playlist_name"));
                if (playlistName != null) {
                    indexPlaylistMembers(playlistId, playlistName);
                }
                return;
            }
            IOUtils.closeStream(cursor);
        } catch (Exception e) {
            sLogger.e("Exception while deleting playlist ", e);
        } finally {
            IOUtils.closeStream(cursor);
        }
    }

    @WorkerThread
    private int indexPlaylistMembers(int playlistId, String playlistName) {
        SystemUtils.ensureNotOnMainThread();
        Cursor membersCursor = MusicDbUtils.getGpmPlaylistMembersCursor(playlistId);
        int analyticsTrackCount = 0;
        if (membersCursor != null) {
            try {
                if (membersCursor.moveToFirst()) {
                    int count = 0;
                    do {
                        String sourceId = membersCursor.getString(membersCursor.getColumnIndex("SourceId"));
                        if (sourceId.matches("\\d+")) {
                            if (count == 0) {
                                sLogger.v("Playlist ID: " + playlistId + ", Name: " + playlistName);
                                NavdyContentProvider.addPlaylistToDb(playlistId, playlistName);
                            }
                            String artist = membersCursor.getString(membersCursor.getColumnIndex("artist"));
                            String album = membersCursor.getString(membersCursor.getColumnIndex("album"));
                            String title = membersCursor.getString(membersCursor.getColumnIndex("title"));
                            sLogger.v("- Artist: " + artist + ", Album: " + album + ", Title: " + title + ", ID: " + sourceId);
                            NavdyContentProvider.addPlaylistMemberToDb(playlistId, sourceId, artist, album, title);
                            count++;
                        }
                        analyticsTrackCount++;
                    } while (membersCursor.moveToNext());
                }
            } catch (Throwable th) {
                if (membersCursor != null) {
                    membersCursor.close();
                }
            }
        }
        if (membersCursor != null) {
            membersCursor.close();
        }
        return analyticsTrackCount;
    }

    private void logInternalPlaylistTables() {
        TaskManager.getInstance().execute(new Runnable() {
            /*  JADX ERROR: ConcurrentModificationException in pass: EliminatePhiNodes
                java.util.ConcurrentModificationException
                	at java.util.ArrayList$Itr.checkForComodification(Unknown Source)
                	at java.util.ArrayList$Itr.next(Unknown Source)
                	at jadx.core.dex.visitors.ssa.EliminatePhiNodes.replaceMerge(EliminatePhiNodes.java:114)
                	at jadx.core.dex.visitors.ssa.EliminatePhiNodes.replaceMergeInstructions(EliminatePhiNodes.java:68)
                	at jadx.core.dex.visitors.ssa.EliminatePhiNodes.visit(EliminatePhiNodes.java:31)
                	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
                	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
                	at java.util.ArrayList.forEach(Unknown Source)
                	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
                	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
                	at java.util.ArrayList.forEach(Unknown Source)
                	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
                	at jadx.core.ProcessClass.process(ProcessClass.java:32)
                	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:292)
                	at jadx.api.JavaClass.decompile(JavaClass.java:62)
                	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
                */
            public void run() {
                /*
                r12 = this;
                r6 = com.navdy.client.app.providers.NavdyContentProvider.getPlaylistsCursor();
                if (r6 == 0) goto L_0x00cc;
            L_0x0006:
                r9 = r6.moveToFirst();	 Catch:{ all -> 0x00d5 }
                if (r9 == 0) goto L_0x00cc;	 Catch:{ all -> 0x00d5 }
            L_0x000c:
                r9 = "playlist_id";	 Catch:{ all -> 0x00d5 }
                r9 = r6.getColumnIndex(r9);	 Catch:{ all -> 0x00d5 }
                r4 = r6.getInt(r9);	 Catch:{ all -> 0x00d5 }
                r9 = "playlist_name";	 Catch:{ all -> 0x00d5 }
                r9 = r6.getColumnIndex(r9);	 Catch:{ all -> 0x00d5 }
                r5 = r6.getString(r9);	 Catch:{ all -> 0x00d5 }
                r3 = com.navdy.client.app.providers.NavdyContentProvider.getPlaylistMembersCursor(r4);	 Catch:{ all -> 0x00d5 }
                if (r3 == 0) goto L_0x00c3;
            L_0x0026:
                r9 = r3.moveToFirst();	 Catch:{ all -> 0x00d0 }
                if (r9 == 0) goto L_0x00c3;	 Catch:{ all -> 0x00d0 }
            L_0x002c:
                r2 = 0;	 Catch:{ all -> 0x00d0 }
            L_0x002d:
                r9 = "artist";	 Catch:{ all -> 0x00d0 }
                r9 = r3.getColumnIndex(r9);	 Catch:{ all -> 0x00d0 }
                r1 = r3.getString(r9);	 Catch:{ all -> 0x00d0 }
                r9 = "album";	 Catch:{ all -> 0x00d0 }
                r9 = r3.getColumnIndex(r9);	 Catch:{ all -> 0x00d0 }
                r0 = r3.getString(r9);	 Catch:{ all -> 0x00d0 }
                r9 = "title";	 Catch:{ all -> 0x00d0 }
                r9 = r3.getColumnIndex(r9);	 Catch:{ all -> 0x00d0 }
                r8 = r3.getString(r9);	 Catch:{ all -> 0x00d0 }
                r9 = "SourceId";	 Catch:{ all -> 0x00d0 }
                r9 = r3.getColumnIndex(r9);	 Catch:{ all -> 0x00d0 }
                r7 = r3.getString(r9);	 Catch:{ all -> 0x00d0 }
                r9 = "\\d+";	 Catch:{ all -> 0x00d0 }
                r9 = r7.matches(r9);	 Catch:{ all -> 0x00d0 }
                if (r9 == 0) goto L_0x00bd;	 Catch:{ all -> 0x00d0 }
            L_0x005d:
                if (r2 != 0) goto L_0x0083;	 Catch:{ all -> 0x00d0 }
            L_0x005f:
                r9 = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger;	 Catch:{ all -> 0x00d0 }
                r10 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00d0 }
                r10.<init>();	 Catch:{ all -> 0x00d0 }
                r11 = "Playlist ID: ";	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r11);	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r4);	 Catch:{ all -> 0x00d0 }
                r11 = ", Name: ";	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r11);	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r5);	 Catch:{ all -> 0x00d0 }
                r10 = r10.toString();	 Catch:{ all -> 0x00d0 }
                r9.d(r10);	 Catch:{ all -> 0x00d0 }
            L_0x0083:
                r9 = com.navdy.client.app.framework.servicehandler.MusicServiceHandler.sLogger;	 Catch:{ all -> 0x00d0 }
                r10 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00d0 }
                r10.<init>();	 Catch:{ all -> 0x00d0 }
                r11 = "- Artist: ";	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r11);	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r1);	 Catch:{ all -> 0x00d0 }
                r11 = ", Album: ";	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r11);	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r0);	 Catch:{ all -> 0x00d0 }
                r11 = ", Title: ";	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r11);	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r8);	 Catch:{ all -> 0x00d0 }
                r11 = ", ID: ";	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r11);	 Catch:{ all -> 0x00d0 }
                r10 = r10.append(r7);	 Catch:{ all -> 0x00d0 }
                r10 = r10.toString();	 Catch:{ all -> 0x00d0 }
                r9.d(r10);	 Catch:{ all -> 0x00d0 }
                r2 = r2 + 1;	 Catch:{ all -> 0x00d0 }
            L_0x00bd:
                r9 = r3.moveToNext();	 Catch:{ all -> 0x00d0 }
                if (r9 != 0) goto L_0x002d;
            L_0x00c3:
                com.navdy.service.library.util.IOUtils.closeStream(r3);	 Catch:{ all -> 0x00d5 }
                r9 = r6.moveToNext();	 Catch:{ all -> 0x00d5 }
                if (r9 != 0) goto L_0x000c;
            L_0x00cc:
                com.navdy.service.library.util.IOUtils.closeStream(r6);
                return;
            L_0x00d0:
                r9 = move-exception;
                com.navdy.service.library.util.IOUtils.closeStream(r3);	 Catch:{ all -> 0x00d5 }
                throw r9;	 Catch:{ all -> 0x00d5 }
            L_0x00d5:
                r9 = move-exception;
                com.navdy.service.library.util.IOUtils.closeStream(r6);
                throw r9;
                */
                throw new UnsupportedOperationException("Method not decompiled: com.navdy.client.app.framework.servicehandler.MusicServiceHandler.6.run():void");
            }
        }, 1);
    }

    public boolean shouldPerformFullPlaylistIndex() {
        return this.shouldPerformFullPlaylistIndex;
    }
}
