package com.navdy.client.app.framework.util;

import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
//import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.navdy.service.library.log.Logger;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Fabric.Builder;
import java.lang.Thread.UncaughtExceptionHandler;

public final class CrashReporter {
    private static final Logger sLogger = new Logger(CrashReporter.class);
    private static volatile boolean stopCrashReporting;
    private boolean installed;
    private static final CrashReporter sInstance = new CrashReporter();

    public static CrashReporter getInstance() {
        return sInstance;
    }

    private CrashReporter() {
    }

    public synchronized void installCrashHandler(Context context, String userId) {
        if (!this.installed) {
            UncaughtExceptionHandler defaultUncaughtHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (defaultUncaughtHandler != null) {
                sLogger.v("default uncaught handler:" + defaultUncaughtHandler.getClass().getName());
            } else {
                sLogger.v("default uncaught handler is null");
            }
//            Fabric.with(new Builder(context).kits(new Crashlytics(), new CrashlyticsNdk()).build());
            Fabric.with(new Builder(context).kits(new Crashlytics()).build());
            if (userId != null) {
                Crashlytics.setUserIdentifier(userId);
            }
            Crashlytics.setString("Serial", Build.SERIAL);
            sLogger.i("crashlytics installed with user:" + userId);
            final UncaughtExceptionHandler crashlyticsHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (crashlyticsHandler != null) {
                sLogger.v("crashlytics uncaught handler:" + crashlyticsHandler.getClass().getName());
            } else {
                sLogger.v("crashlytics uncaught handler is null");
            }
            Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable exception) {
                    CrashReporter.this.handleUncaughtException(thread, exception, crashlyticsHandler);
                }
            });
            Logger.addAppender(new CrashlyticsAppender());
            this.installed = true;
        }
    }

    public synchronized void setUser(String userId) {
        if (userId != null) {
            Crashlytics.setUserIdentifier(userId);
        }
    }

    public void reportNonFatalException(Throwable throwable) {
        if (this.installed) {
            try {
                Crashlytics.logException(throwable);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void log(String msg) {
        if (this.installed) {
            try {
                Crashlytics.log(msg);
            } catch (Throwable t) {
                sLogger.e(t);
            }
        }
    }

    private void handleUncaughtException(Thread thread, Throwable exception, UncaughtExceptionHandler crashlyticsHandler) {
        if (stopCrashReporting) {
            sLogger.i("FATAL-reporting-turned-off", exception);
            return;
        }
        stopCrashReporting = true;
        String tag = "FATAL-CRASH";
        String msg = tag + " Uncaught exception - " + thread.getName() + ":" + thread.getId() + " ui thread id:" + Looper.getMainLooper().getThread().getId();
        Log.e(tag, msg, exception);
        sLogger.e(msg, exception);
        sLogger.i("closing logger");
        Logger.close();
        if (crashlyticsHandler != null) {
            crashlyticsHandler.uncaughtException(thread, exception);
        }
    }
}
