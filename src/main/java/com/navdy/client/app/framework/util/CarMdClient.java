package com.navdy.client.app.framework.util;

import android.util.Pair;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.Injector;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.http.IHttpManager;
import com.zendesk.service.HttpConstants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.HttpUrl.Builder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Deprecated
public class CarMdClient {
    public static final String ACCESS_IMAGE_URL = "accessImageURL";
    public static final String ACCESS_NOTES = "accessNotes";
    private static final HttpUrl CAR_MD_API_URL = HttpUrl.parse("https://api2.carmd.com/v2.0/decode");
    private static final HttpUrl CAR_MD_OBD_API_URL = HttpUrl.parse("http://api2.carmd.com/v2.0/articles/dlclocationbyymm");
    private static final int CONNECTION_TIMEOUT_IN_SECONDS = 30;
    public static final String DATA = "data";
    public static final String LOCATION_IMAGE_URL = "locationImageURL";
    public static final String NOTES = "notes";
    private static CarMdClient instance = null;
    private static Logger logger = new Logger(CarMdClient.class);
    private final String CAR_MD_AUTH;
    private final String CAR_MD_TOKEN;
    private OkHttpClient httpClient = null;
    @Inject
    IHttpManager mHttpManager = null;

    public static class CarMdObdLocationResponse {
        public String accessImageURL;
        public String accessNote;
        public int locationNumber;
        public String note;
    }

    private CarMdClient() {
        Injector.inject(NavdyApplication.getAppContext(), this);
        this.httpClient = this.mHttpManager.getClientCopy().retryOnConnectionFailure(true).readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS).build();
        this.CAR_MD_AUTH = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_car_md_auth));
        this.CAR_MD_TOKEN = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_car_md_token));
    }

    public static CarMdClient getInstance() {
        if (instance == null) {
            instance = new CarMdClient();
        }
        return instance;
    }

    public void getMakes(CarMdCallBack callback) {
        sendCarMdRequest(callback, buildCarMdRequest(CAR_MD_API_URL, null));
    }

    public void getYears(String make, CarMdCallBack callback) {
        ArrayList<Pair<String, String>> params = new ArrayList(1);
        params.add(new Pair("make", make));
        sendCarMdRequest(callback, buildCarMdRequest(CAR_MD_API_URL, params));
    }

    public void getModels(String make, String year, CarMdCallBack callback) {
        ArrayList<Pair<String, String>> params = new ArrayList(2);
        params.add(new Pair("make", make));
        params.add(new Pair("year", year));
        sendCarMdRequest(callback, buildCarMdRequest(CAR_MD_API_URL, params));
    }

    public ArrayList<String> getListFromJsonResponse(String jsonString) throws JSONException {
        if (!StringUtils.isEmptyAfterTrim(jsonString)) {
            JSONArray results = null;
            try {
                results = new JSONObject(jsonString).getJSONArray(DATA);
            } catch (Exception e) {
                logger.e("Unable to parse car info json response: " + jsonString, e);
            }
            if (results != null) {
                ArrayList<String> arrayList = new ArrayList(results.length());
                for (int i = 0; i < results.length(); i++) {
                    String make = results.optString(i);
                    if (!StringUtils.isEmptyAfterTrim(make)) {
                        arrayList.add(make);
                    }
                }
                return arrayList;
            }
        }
        return null;
    }

    public void getObdLocation(CarMdCallBack callback, String make, String year, String model) {
        if (callback == null || StringUtils.isEmptyAfterTrim(make) || StringUtils.isEmptyAfterTrim(year) || StringUtils.isEmptyAfterTrim(model)) {
            logger.e(String.format("getObdLocation: One of the parameters is null! callback = %s, make = %s year = %s model = %s", new Object[]{callback, make, year, model}));
            return;
        }
        ArrayList<Pair<String, String>> params = new ArrayList(3);
        params.add(new Pair("make", make));
        params.add(new Pair("year", year));
        params.add(new Pair("model", model));
        sendCarMdRequest(callback, buildCarMdRequest(CAR_MD_OBD_API_URL, params));
    }

    public void sendCarMdRequest(final CarMdCallBack callback, Request request) {
        if (callback != null) {
            logger.v("Calling car MD: " + request);
            try {
                this.httpClient.newCall(request).enqueue(new Callback() {
                    public void onResponse(Call call, Response response) throws IOException {
                        try {
                            int responseCode = response.code();
                            if (responseCode == 200 || responseCode == HttpConstants.HTTP_CREATED) {
                                ResponseBody responseBody = response.body();
                                if (responseBody.contentLength() > 0) {
                                    callback.processResponse(responseBody);
                                    return;
                                }
                                return;
                            }
                            callback.processFailure(new IOException("Car MD request failed with " + responseCode));
                        } catch (Throwable t) {
                            callback.processFailure(t);
                        }
                    }

                    public void onFailure(Call call, IOException e) {
                        callback.processFailure(e);
                    }
                });
            } catch (Exception e) {
                callback.processFailure(e);
            }
        }
    }

    public Request buildCarMdRequest(HttpUrl requestUrl, ArrayList<Pair<String, String>> queryParams) {
        if (queryParams != null) {
            Builder builder = requestUrl.newBuilder();
            Iterator it = queryParams.iterator();
            while (it.hasNext()) {
                Pair<String, String> param = (Pair) it.next();
                if (!(param == null || StringUtils.isEmptyAfterTrim((CharSequence) param.first) || StringUtils.isEmptyAfterTrim((CharSequence) param.second))) {
                    builder.addQueryParameter((String) param.first, (String) param.second);
                }
            }
            requestUrl = builder.build();
        }
        Request.Builder requestBuilder = new Request.Builder().url(requestUrl);
        if (StringUtils.isEmptyAfterTrim(this.CAR_MD_AUTH)) {
            logger.e("Missing car md auth credential !");
        } else {
            requestBuilder.addHeader("authorization", "Basic " + this.CAR_MD_AUTH);
        }
        if (StringUtils.isEmptyAfterTrim(this.CAR_MD_TOKEN)) {
            logger.e("Missing car md token credential !");
        } else {
            requestBuilder.addHeader("partner-token", this.CAR_MD_TOKEN);
        }
        return requestBuilder.build();
    }

    public CarMdObdLocationResponse parseCarMdObdLocationResponse(ResponseBody responseBody) throws Exception {
        CarMdObdLocationResponse response = new CarMdObdLocationResponse();
        String jsonString = responseBody.string();
        logger.v("Car MD: response: " + jsonString);
        JSONObject data = new JSONObject(jsonString).getJSONObject(DATA);
        response.accessImageURL = data.getString(ACCESS_IMAGE_URL);
        String locationImageURL = data.getString(LOCATION_IMAGE_URL);
        String notes = data.getString(NOTES);
        String accessNotes = data.getString(ACCESS_NOTES);
        response.note = notes;
        response.accessNote = accessNotes;
        response.locationNumber = -1;
        if (locationImageURL.contains("dlc/position/1.jpg")) {
            response.locationNumber = 0;
        } else if (locationImageURL.contains("dlc/position/2.jpg")) {
            response.locationNumber = 1;
        } else if (locationImageURL.contains("dlc/position/3.jpg")) {
            response.locationNumber = 2;
        } else if (locationImageURL.contains("dlc/position/4.jpg")) {
            response.locationNumber = 3;
        } else if (locationImageURL.contains("dlc/position/5.jpg")) {
            response.locationNumber = 4;
        } else if (locationImageURL.contains("dlc/position/6.jpg")) {
            response.locationNumber = 5;
        } else if (locationImageURL.contains("dlc/position/7.jpg")) {
            response.locationNumber = 6;
        } else if (locationImageURL.contains("dlc/position/8.jpg")) {
            response.locationNumber = 7;
        } else if (locationImageURL.contains("dlc/position/9.jpg")) {
            response.locationNumber = 8;
        }
        if (response.locationNumber >= 0 && response.locationNumber <= 8) {
            return response;
        }
        throw new Exception("Missing Obd Location. locationImageURL = " + locationImageURL);
    }
}
