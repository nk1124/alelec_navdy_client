package com.navdy.client.app.framework.util;

import android.os.Handler;
import android.os.Looper;

import com.crashlytics.android.BuildConfig;
import com.crashlytics.android.Crashlytics;
import com.squareup.otto.Bus;
import com.squareup.otto.EventHandler;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;


public final class BusProvider extends Bus {
    private static final BusProvider singleton = new BusProvider();
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private BusProvider() {
    }

    public static BusProvider getInstance() {
        return singleton;
    }

    public void post(final Object event) {
        if (event != null) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    BusProvider.super.post(event);
                }
            });
        }
    }

    public void register(final Object object) {
        if (object != null) {
            Map<Class<?>, Set<EventHandler>> foundHandlersMap = handlerFinder.findAllSubscribers(object);
            try {
                if (foundHandlersMap.size() == 0) {
                    throw new RuntimeException(object.getClass().getName() + " registered with no subscribers");
                }
            } catch (RuntimeException ex){
                Crashlytics.logException(ex);
            }

            if (Looper.getMainLooper() == Looper.myLooper()) {
                super.register(object);
            } else {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        BusProvider.super.register(object);
                    }
                });
            }
        }
    }

    public void unregister(final Object object) {
        if (object != null) {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                super.unregister(object);
            } else {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        BusProvider.super.unregister(object);
                    }
                });
            }
        }
    }
}
