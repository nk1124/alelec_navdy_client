package com.navdy.client.app.framework.map;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;

import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.MapSettings;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.OnEngineInitListener.Error;
import com.here.android.mpa.common.Version;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.LinkedList;
import java.util.Queue;

public class HereMapsManager {
    private static final String HERE_MAPS_INTERNAL_FOLDER = "/.here-maps";
    private static final String MAP_ENGINE_PROCESS_NAME = "Navdy.Mapservice";
    private static final String MAP_SERVICE_INTENT = "com.navdy.mapservice.MapService";
    private static final Object initListenersLock = new Object();
    private static final HereMapsManager instance = new HereMapsManager();
    private static final Logger logger = new Logger(HereMapsManager.class);
    private Error initError;
    private final Queue<OnEngineInitListener> initListeners = new LinkedList<>();
    private final Handler mainThread = new Handler(Looper.getMainLooper());
    private final OnEngineInitListener onEngineInitListener = new OnEngineInitListener() {
        public void onEngineInitializationCompleted(final Error error) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    HereMapsManager.this.initError = error;
                    if (error == Error.NONE) {
                        HereMapsManager.logger.i("MapEngine has initialized correctly");
                        HereMapsManager.logger.i("HERE SDK version " + Version.getSdkVersion());
                        HereMapsConfigurator.getInstance().updateMapsConfig();
                        HereMapsManager.this.state = State.INITIALIZED;
                        HereMapsManager.this.mainThread.post(new Runnable() {
                            public void run() {
                                synchronized (HereMapsManager.initListenersLock) {
                                    while (!HereMapsManager.this.initListeners.isEmpty()) {
                                        HereMapsManager.this.initListeners.poll().onEngineInitializationCompleted(error);
                                    }
                                }
                            }
                        });
                        return;
                    }
                    HereMapsManager.this.state = State.FAILED;
                    HereMapsManager.logger.e("Map failed to initialize: " + error);
                    HereMapsManager.logger.e("ERROR ON MAP ENGINE INIT, RESTARTING");
                    HereMapsManager.this.killSelfAndMapEngine();
                }
            }, 11);
        }
    };
    private final MapEngine mapEngine = MapEngine.getInstance();
    private volatile State state = State.IDLE;

    private enum State {
        IDLE,
        INITIALIZING,
        INITIALIZED,
        FAILED
    }

    public static HereMapsManager getInstance() {
        return instance;
    }

    private HereMapsManager() {
    }

    public void addOnInitializedListener(final OnEngineInitListener listener) {
        if (this.state != State.FAILED) {
            if (this.state == State.INITIALIZED) {
                this.mainThread.post(new Runnable() {
                    public void run() {
                        listener.onEngineInitializationCompleted(HereMapsManager.this.initError);
                    }
                });
            } else {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        synchronized (HereMapsManager.initListenersLock) {
                            HereMapsManager.this.initListeners.add(listener);
                        }
                        if (HereMapsManager.this.state == State.IDLE) {
                            HereMapsManager.this.initializeIfPermitted();
                        }
                    }
                }, 11);
            }
        }
    }

    public boolean isInitialized() {
        return this.state == State.INITIALIZED;
    }

    public void killSelfAndMapEngine() {
        for (RunningAppProcessInfo processInfo : ((ActivityManager) NavdyApplication.getAppContext().getSystemService("activity")).getRunningAppProcesses()) {
            if (processInfo.processName.equals(MAP_ENGINE_PROCESS_NAME)) {
                Process.killProcess(processInfo.pid);
                break;
            }
        }
        NavdyApplication.kill();
    }

    private void initializeIfPermitted() {
        Context context = NavdyApplication.getAppContext();
        boolean readExternalStoragePermission = BaseActivity.weHaveThisPermission(context, "android.permission.READ_EXTERNAL_STORAGE");
        boolean writeExternalStoragePermission = BaseActivity.weHaveThisPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        boolean accessFineLocationPermission = BaseActivity.weHaveThisPermission(context, "android.permission.ACCESS_FINE_LOCATION");
        if (readExternalStoragePermission && writeExternalStoragePermission && accessFineLocationPermission) {
            logger.v("initialization is permitted, initializing...");
            initialize();
            return;
        }
        logger.d("initialization is not permitted yet, permission granted for READ_EXTERNAL_STORAGE=" + readExternalStoragePermission + "; writeExternalStoragePermission=" + writeExternalStoragePermission + "; accessFineLocationPermission=" + accessFineLocationPermission);
    }

    private void initialize() {
        logger.v("initializing here map engine");
        this.state = State.INITIALIZING;
        Context context = NavdyApplication.getAppContext();
        MapSettings.setIsolatedDiskCacheRootPath(context.getFilesDir() + HERE_MAPS_INTERNAL_FOLDER, MAP_SERVICE_INTENT);
        this.mapEngine.init(new ApplicationContext(context), this.onEngineInitListener);
    }
}
