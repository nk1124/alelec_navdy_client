package com.navdy.client.app.framework.servicehandler;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.OnEngineInitListener.Error;
import com.navdy.client.app.framework.map.HereMapsManager;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.navigation.NavigationRouteCancelRequest;
import com.navdy.service.library.events.navigation.NavigationRouteRequest;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationRouteStatus;
import com.navdy.service.library.events.navigation.NavigationSessionDeferRequest;
import com.navdy.service.library.events.navigation.NavigationSessionResponse;
import com.navdy.service.library.events.navigation.NavigationSessionRouteChange;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import java.util.List;

public class DisplayNavigationServiceHandler {
    private static final boolean VERBOSE = false;
    private static final Logger logger = new Logger(DisplayNavigationServiceHandler.class);
    @SuppressLint({"StaticFieldLeak"})
    private static final DisplayNavigationServiceHandler singleton = new DisplayNavigationServiceHandler();
    private final Bus bus = BusProvider.getInstance();

    public static class ArrivedTripEvent {
    }

    private static class DeferStartActiveTripEvent {
        public final int delay;
        final long expireTimestamp;
        final String requestId;
        final String routeId;

        DeferStartActiveTripEvent(String requestId, String routeId, int delay, long expireTimestamp) {
            this.requestId = requestId;
            this.routeId = routeId;
            this.delay = delay;
            this.expireTimestamp = expireTimestamp;
        }
    }

    public static class DisplayInitiatedRequestEvent {
        public final Destination destination;
        public final String requestId;

        DisplayInitiatedRequestEvent(String requestId, Destination destination) {
            this.requestId = requestId;
            this.destination = destination;
        }
    }

    public static class HUDReadyEvent {
    }

    public static class RerouteActiveTripEvent {
        @Nullable
        public final Destination destination;
        @NonNull
        public final NavigationRouteResult route;
        @NonNull
        final String routeId;

        RerouteActiveTripEvent(@Nullable Destination destination, @NonNull NavigationRouteResult route, @NonNull String routeId) {
            this.destination = destination;
            this.route = route;
            this.routeId = routeId;
        }
    }

    public static class RouteCalculationProgress {
        public final String handle;
        public final int progress;
        public final String requestId;

        RouteCalculationProgress(String requestId, String handle, int progress) {
            this.requestId = requestId;
            this.handle = handle;
            this.progress = progress;
        }
    }

    public static class RoutesCalculatedEvent {
        public final String requestId;
        public final List<NavigationRouteResult> routes;

        RoutesCalculatedEvent(List<NavigationRouteResult> routes, String requestId) {
            this.routes = routes;
            this.requestId = requestId;
        }
    }

    public static class RoutesCalculationErrorEvent {
        @Nullable
        final String cancelHandle;
        public final RequestStatus status;

        RoutesCalculationErrorEvent(RequestStatus status, @Nullable String cancelHandle) {
            this.status = status;
            this.cancelHandle = cancelHandle;
        }
    }

    public static class StartActiveTripEvent {
        public final Destination destination;
        public final NavigationRouteResult route;
        public final String routeId;

        StartActiveTripEvent(Destination destination, NavigationRouteResult route, String routeId) {
            this.destination = destination;
            this.route = route;
            this.routeId = routeId;
        }
    }

    public static class StopActiveTripEvent {
        @Nullable
        public final String handle;
        @Nullable
        public final String requestId;
        @Nullable
        public final String routeId;

        StopActiveTripEvent(@Nullable String requestId, @Nullable String routeId, @Nullable String handle) {
            this.requestId = requestId;
            this.routeId = routeId;
            this.handle = handle;
        }
    }

    public static synchronized DisplayNavigationServiceHandler getInstance() {
        DisplayNavigationServiceHandler displayNavigationServiceHandler;
        synchronized (DisplayNavigationServiceHandler.class) {
            displayNavigationServiceHandler = singleton;
        }
        return displayNavigationServiceHandler;
    }

    private DisplayNavigationServiceHandler() {
        this.bus.register(this);
    }

    @Subscribe
    public void onNavigationRouteRequest(final NavigationRouteRequest event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DisplayNavigationServiceHandler.this.handleNavigationRouteRequest(event);
            }
        }, 1);
    }

    @Subscribe
    public void onNavigationSessionRouteChange(final NavigationSessionRouteChange event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DisplayNavigationServiceHandler.this.handleNavigationSessionRouteChange(event);
            }
        }, 1);
    }

    @Subscribe
    public void onNavigationSessionStatusEvent(final NavigationSessionStatusEvent event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DisplayNavigationServiceHandler.this.handleNavigationSessionStatusEvent(event);
            }
        }, 1);
    }

    @Subscribe
    public void onNavigationRouteStatus(final NavigationRouteStatus event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DisplayNavigationServiceHandler.this.handleNavigationRouteStatus(event);
            }
        }, 1);
    }

    @Subscribe
    public void onNavigationRouteResponse(final NavigationRouteResponse event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DisplayNavigationServiceHandler.this.handleNavigationRouteResponse(event);
            }
        }, 1);
    }

    @Subscribe
    public void onNavigationSessionDeferRequest(final NavigationSessionDeferRequest event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DisplayNavigationServiceHandler.this.handleNavigationSessionDeferRequest(event);
            }
        }, 1);
    }

    @Subscribe
    public void onNavigationRouteCancelRequest(final NavigationRouteCancelRequest event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DisplayNavigationServiceHandler.this.handleNavigationRouteCancelRequest(event);
            }
        }, 1);
    }

    @Subscribe
    public void onNavigationSessionResponse(final NavigationSessionResponse event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DisplayNavigationServiceHandler.this.handleNavigationSessionResponse(event);
            }
        }, 1);
    }

    private void handleNavigationRouteRequest(NavigationRouteRequest navigationRouteRequest) {
        if (navigationRouteRequest != null) {
            Destination destination = null;
            String destinationId = navigationRouteRequest.destination_identifier;
            if (!StringUtils.isEmptyAfterTrim(destinationId)) {
                try {
                    destination = NavdyContentProvider.getThisDestination(Integer.parseInt(destinationId));
                } catch (Exception e) {
                    logger.e("Unable to look up the destination for id " + destinationId, e);
                }
            }
            if (destination == null && navigationRouteRequest.requestDestination != null) {
                destination = new Destination(navigationRouteRequest.requestDestination);
            }
            if (destination == null) {
                logger.w("handleNavigationRouteRequest, destination is null, no-op");
            } else {
                postWhenReady(new DisplayInitiatedRequestEvent(navigationRouteRequest.requestId, destination));
            }
        }
    }

    private void handleNavigationSessionRouteChange(NavigationSessionRouteChange navigationSessionRouteChange) {
        if (navigationSessionRouteChange == null || navigationSessionRouteChange.newRoute == null) {
            logger.e("handleNavigationSessionRouteChange called with a null navigationSessionRouteChange or the newRoute is null.");
        } else {
            postWhenReady(new StartActiveTripEvent(getDestination(navigationSessionRouteChange.newRoute, null), navigationSessionRouteChange.newRoute, navigationSessionRouteChange.newRoute.routeId));
        }
    }

    private void handleNavigationSessionStatusEvent(NavigationSessionStatusEvent navigationSessionStatusEvent) {
        switch (navigationSessionStatusEvent.sessionState) {
            case NAV_SESSION_STARTED:
                postWhenReady(new StartActiveTripEvent(getDestination(navigationSessionStatusEvent.route, navigationSessionStatusEvent.destination_identifier), navigationSessionStatusEvent.route, navigationSessionStatusEvent.routeId));
                return;
            case NAV_SESSION_REROUTED:
                postWhenReady(new RerouteActiveTripEvent(getDestination(navigationSessionStatusEvent.route, navigationSessionStatusEvent.destination_identifier), navigationSessionStatusEvent.route, navigationSessionStatusEvent.routeId));
                return;
            case NAV_SESSION_ARRIVED:
                postWhenReady(new ArrivedTripEvent());
                return;
            case NAV_SESSION_STOPPED:
                postWhenReady(new StopActiveTripEvent(null, navigationSessionStatusEvent.routeId, null));
                return;
            case NAV_SESSION_ENGINE_READY:
                postWhenReady(new HUDReadyEvent());
                return;
            default:
                return;
        }
    }

    private void postWhenReady(final Object event) {
        HereMapsManager.getInstance().addOnInitializedListener(new OnEngineInitListener() {
            public void onEngineInitializationCompleted(Error error) {
                if (error != Error.NONE) {
                    DisplayNavigationServiceHandler.logger.e("can't post navigation events, error " + error.name());
                } else {
                    DisplayNavigationServiceHandler.this.bus.post(event);
                }
            }
        });
    }

    private void handleNavigationRouteStatus(NavigationRouteStatus navigationRouteStatus) {
        postWhenReady(new RouteCalculationProgress(navigationRouteStatus.requestId, navigationRouteStatus.handle, navigationRouteStatus.progress.intValue()));
    }

    private void handleNavigationRouteResponse(NavigationRouteResponse navigationRouteResponse) {
        switch (navigationRouteResponse.status) {
            case REQUEST_SUCCESS:
                postWhenReady(new RoutesCalculatedEvent(navigationRouteResponse.results, navigationRouteResponse.requestId));
                return;
            case REQUEST_NOT_READY:
            case REQUEST_NO_LOCATION_SERVICE:
            case REQUEST_SERVICE_ERROR:
            case REQUEST_UNKNOWN_ERROR:
            case REQUEST_NOT_AVAILABLE:
            case REQUEST_INVALID_REQUEST:
                String cancelHandle = null;
                if (navigationRouteResponse.status == RequestStatus.REQUEST_ALREADY_IN_PROGRESS) {
                    cancelHandle = navigationRouteResponse.statusDetail;
                }
                postWhenReady(new RoutesCalculationErrorEvent(navigationRouteResponse.status, cancelHandle));
                return;
            case REQUEST_CANCELLED:
                postWhenReady(new StopActiveTripEvent(navigationRouteResponse.requestId, null, null));
                return;
            default:
                return;
        }
    }

    private void handleNavigationSessionDeferRequest(NavigationSessionDeferRequest navigationSessionDeferRequest) {
        postWhenReady(new DeferStartActiveTripEvent(navigationSessionDeferRequest.requestId, navigationSessionDeferRequest.routeId, navigationSessionDeferRequest.delay.intValue(), navigationSessionDeferRequest.expireTimestamp.longValue()));
    }

    private void handleNavigationRouteCancelRequest(NavigationRouteCancelRequest navigationRouteCancelRequest) {
        postWhenReady(new StopActiveTripEvent(null, null, navigationRouteCancelRequest.handle));
    }

    private void handleNavigationSessionResponse(NavigationSessionResponse navigationSessionResponse) {
        logger.d("navigationSessionResponse:\n\t" + navigationSessionResponse);
        if (navigationSessionResponse.pendingSessionState == NavigationSessionState.NAV_SESSION_STOPPED) {
            postWhenReady(new StopActiveTripEvent(null, navigationSessionResponse.routeId, null));
        }
    }

    @Nullable
    private Destination getDestination(NavigationRouteResult navigationRouteResult, String destinationId) {
        Destination destination = null;
        if (destinationId != null) {
            destination = NavdyContentProvider.getThisDestination(destinationId);
        }
        if (destination != null || navigationRouteResult == null) {
            return destination;
        }
        destination = new Destination(navigationRouteResult);
        if (!StringUtils.isEmptyAfterTrim(destinationId)) {
            return destination;
        }
        Destination destinationFromNavRouteResult = NavdyContentProvider.getThisDestination(destination);
        if (destinationFromNavRouteResult != null) {
            return destinationFromNavRouteResult;
        }
        return destination;
    }
}
