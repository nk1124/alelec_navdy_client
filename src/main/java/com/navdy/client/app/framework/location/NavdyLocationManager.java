package com.navdy.client.app.framework.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import com.google.android.gms.maps.model.LatLng;
import com.here.odnp.config.OdnpConfigStatic;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.i18n.AddressUtils;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Trip;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.TimeUnit;

public final class NavdyLocationManager {
    private static final long COUNTRY_CODE_REFRESH_RATE = TimeUnit.DAYS.toMillis(1);
    private static final String FAKE_CAR_LOCATION_ACTION = "com.navdy.client.debug.INJECT_FAKE_CAR_LOCATION";
    private static final String FAKE_CAR_LOCATION_ACTION_CANCEL = "com.navdy.client.debug.INJECT_FAKE_CAR_LOCATION_CANCEL";
    private static final String FAKE_PHONE_LOCATION_ACTION = "com.navdy.client.debug.INJECT_FAKE_PHONE_LOCATION";
    private static final String FAKE_PHONE_LOCATION_ACTION_CANCEL = "com.navdy.client.debug.INJECT_FAKE_PHONE_LOCATION_CANCEL";
    private static final String LATITUDE_PREFERENCE = "latitude";
    private static final String LOCATION_ACCURACY_PREFERENCE = "accuracy";
    private static final String LOCATION_PROVIDER_PREFERENCE = "provider";
    private static final String LOCATION_TIME_PREFERENCE = "time";
    private static final String LONGITUDE_PREFERENCE = "longitude";
//    private static final long MAX_AGE_LOCATION_FIX = 30000;
    private static final long MAX_AGE_LOCATION_FIX = 3000;
//    private static final float MIN_DISTANCE_LOCATION_CHANGE = 25.0f;
    private static final float MIN_DISTANCE_LOCATION_CHANGE = 0;
    private static final int MIN_TIME_LOCATION_CHANGE = 5000;
    private static final String NAVDY_LOCATION_MANAGER_PREFS = "navdy_location_manager";
    private static final int SMART_START_LOCATION_LIMIT = 500;
    private static NavdyLocationManager instance;
    private static String lastKnownCountryCode = AddressUtils.DEFAULT_COUNTRY_CODE;
    private static final Logger logger = new Logger(NavdyLocationManager.class);
    private static final Object navdyLocationManagerLock = new Object();
    private static long nextCountryCodeUpdate = 0;
    private List<String> bestProviders;
    private Location currentCarLocation;
    private Location currentPhoneLocation;
    private boolean didProperlyInitialize;
    private final BroadcastReceiver fakeLocationReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            NavdyLocationManager.logger.w("non-debug build, sorry!");
        }
    };
    private boolean isInjectingFakeCarLocations;
    private boolean isInjectingFakePhoneLocations;
    private final List<WeakReference<OnNavdyLocationChangedListener>> listeners;
    private boolean listening;
    private final LocationListener locationListener = new LocationListener() {
        /* JADX WARNING: Missing block: B:27:?, code:
            return;
     */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onLocationChanged(Location location) {
            synchronized (NavdyLocationManager.navdyLocationManagerLock) {
                if (!NavdyLocationManager.this.isInjectingFakePhoneLocations) {
                    if (NavdyLocationManager.this.isRecentAndAccurateEnough(location)) {
                        NavdyLocationManager.logger.v("update new location from locationListener: " + location);
                        synchronized (NavdyLocationManager.navdyLocationManagerLock) {
                            NavdyLocationManager.this.currentPhoneLocation = location;
                            NavdyLocationManager.updateLastKnownCountryCode(NavdyLocationManager.this.currentPhoneLocation);
                        }
                        NavdyLocationManager.this.saveLocationToPreferences(location);
                        NavdyLocationManager.this.callListenersForPhoneLocation(location);
                    }
                }
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };
    private LocationManager locationManager;
    private final SharedPreferences preferences;

    public static class CarLocationChangedEvent {
        public final Coordinate coordinate;

        public CarLocationChangedEvent(Coordinate coordinate) {
            this.coordinate = coordinate;
        }
    }

    public interface OnNavdyLocationChangedListener {
        void onCarLocationChanged(@NonNull Coordinate coordinate);

        void onPhoneLocationChanged(@NonNull Coordinate coordinate);
    }

    public static NavdyLocationManager getInstance() {
        NavdyLocationManager navdyLocationManager;
        synchronized (navdyLocationManagerLock) {
            if (instance == null) {
                instance = new NavdyLocationManager();
            }
            navdyLocationManager = instance;
        }
        return navdyLocationManager;
    }

    private NavdyLocationManager() {
        Context context = NavdyApplication.getAppContext();
        this.listeners = new ArrayList();
        this.preferences = context.getSharedPreferences(NAVDY_LOCATION_MANAGER_PREFS, 0);
        this.didProperlyInitialize = false;
        this.isInjectingFakePhoneLocations = false;
        this.isInjectingFakeCarLocations = false;
        getLastTripUpdateCoordinates();
        initLocationServices();
        BusProvider.getInstance().register(this);
        registerFakeLocationReceiver(context);
    }

    public void initLocationServices() {
        Context context = NavdyApplication.getAppContext();
        boolean hasPermissions = ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0;
        synchronized (navdyLocationManagerLock) {
            if (!this.didProperlyInitialize && hasPermissions) {
                this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                this.bestProviders = getBestAvailableLocationProviders();
                List<Location> candidateLocations = new ArrayList<>(3);
                if (this.locationManager != null) {
                    for (String provider : this.bestProviders) {
                        Location lastKnownLocation = this.locationManager.getLastKnownLocation(provider);
                        if (lastKnownLocation != null) {
                            candidateLocations.add(lastKnownLocation);
                        }
                    }
                }
                String provider2 = this.preferences.getString(LOCATION_PROVIDER_PREFERENCE, "");
                float lat = this.preferences.getFloat(LATITUDE_PREFERENCE, 0.0f);
                float lng = this.preferences.getFloat(LONGITUDE_PREFERENCE, 0.0f);
                long time = this.preferences.getLong(LOCATION_TIME_PREFERENCE, 0);
                float accuracy = this.preferences.getFloat(LOCATION_ACCURACY_PREFERENCE, Float.MAX_VALUE);
                Location savedLocation = new Location(provider2);
                savedLocation.setLatitude((double) lat);
                savedLocation.setLongitude((double) lng);
                savedLocation.setTime(time);
                savedLocation.setAccuracy(accuracy);
                logger.v("initLocationServices, taken from prefs: " + savedLocation);
                candidateLocations.add(savedLocation);
                Collections.sort(candidateLocations, new Comparator<Location>() {
                    public int compare(Location lhs, Location rhs) {
                        return (int) (NavdyLocationManager.this.getLocationScore(lhs) - NavdyLocationManager.this.getLocationScore(rhs));
                    }
                });
                this.currentPhoneLocation = candidateLocations.get(0);
                saveLocationToPreferences(this.currentPhoneLocation);
                this.didProperlyInitialize = true;
                logger.v("Properly initialized, currentPhoneLocation=" + this.currentPhoneLocation);
            } else if (!hasPermissions) {
                logger.w("Error with location permissions!");
            }
        }
    }

    public void addListener(@NonNull OnNavdyLocationChangedListener navdyLocationListener) {
        synchronized (navdyLocationManagerLock) {
            this.listeners.add(new WeakReference<>(navdyLocationListener));
            if (!this.listening) {
                try {
                    if (ContextCompat.checkSelfPermission(NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0 && this.locationManager != null) {
                        for (String provider : this.bestProviders) {
                            this.locationManager.requestLocationUpdates(provider, MAX_AGE_LOCATION_FIX, MIN_DISTANCE_LOCATION_CHANGE,
                                    this.locationListener, Looper.getMainLooper());
                        }
                        this.listening = true;
                        logger.v("location listener installed[" + this.bestProviders + "]" + System.identityHashCode(this.locationListener));
                    }
                } catch (Throwable t) {
                    logger.e(t);
                }
            }
        }
    }

    public void removeListener(OnNavdyLocationChangedListener navdyLocationListener) {
        synchronized (navdyLocationManagerLock) {
            ListIterator<WeakReference<OnNavdyLocationChangedListener>> iterator = this.listeners.listIterator();
            while (iterator.hasNext()) {
                OnNavdyLocationChangedListener listener = (OnNavdyLocationChangedListener) ((WeakReference) iterator.next()).get();
                if (listener == null || listener.equals(navdyLocationListener)) {
                    iterator.remove();
                }
            }
            if (this.listening && this.listeners.size() == 0) {
                logger.v("stopListeningForLocationChange");
                try {
                    if (ContextCompat.checkSelfPermission(NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) {
                        this.locationManager.removeUpdates(this.locationListener);
                        logger.v("location listener removed:" + System.identityHashCode(this.locationListener));
                    }
                } catch (Throwable t) {
                    logger.e(t);
                }
                this.listening = false;
            }
        }
    }

    @Nullable
    public Location getSmartStartLocation() {
        Location carLocation;
        Location phoneLocation;
        synchronized (navdyLocationManagerLock) {
            carLocation = this.currentCarLocation;
            phoneLocation = this.currentPhoneLocation;
        }
        if (carLocation == null && phoneLocation == null) {
            logger.v("Both car and phone location are null :'(");
            return null;
        } else if (carLocation == null) {
            logger.v("Using phone location because car location is null");
            return phoneLocation;
        } else if (phoneLocation == null) {
            logger.v("Using car location because phone location is null");
            return carLocation;
        } else if (MapUtils.distanceBetween(phoneLocation, carLocation) <= SMART_START_LOCATION_LIMIT) {
            logger.v("Using car location because the car and the phone are close to each other.");
            return carLocation;
        } else if (phoneLocation.getTime() > carLocation.getTime()) {
            logger.v("Using phone location because car and phone are more than 500m apart and phone location is the most recent");
            return phoneLocation;
        } else {
            logger.v("Using car location because car and phone are more than 500m apart and car location is the most recent");
            return carLocation;
        }
    }

    @Nullable
    private Location getPhoneLocation() {
        Location location;
        synchronized (navdyLocationManagerLock) {
            location = this.currentPhoneLocation;
        }
        return location;
    }

    @Nullable
    private Location getCarLocation() {
        Location location;
        synchronized (navdyLocationManagerLock) {
            location = this.currentCarLocation;
        }
        return location;
    }

    @Nullable
    public Coordinate getSmartStartCoordinates() {
        return getCoordsFromLocation(getSmartStartLocation());
    }

    @Nullable
    public Coordinate getPhoneCoordinates() {
        return getCoordsFromLocation(getPhoneLocation());
    }

    @Nullable
    public Coordinate getCarCoordinates() {
        return getCoordsFromLocation(getCarLocation());
    }

    /* JADX WARNING: Missing block: B:7:0x000a, code:
            r1 = r6.coordinate;
            r0 = new android.location.Location("gps");
            r0.setLatitude(r1.latitude.doubleValue());
            r0.setLongitude(r1.longitude.doubleValue());
            r0.setTime(java.lang.System.currentTimeMillis());
     */
    /* JADX WARNING: Missing block: B:8:0x0030, code:
            if (isValidLocation(r0) == false) goto L_?;
     */
    /* JADX WARNING: Missing block: B:9:0x0032, code:
            logger.v("new car location: " + r0);
            r3 = navdyLocationManagerLock;
     */
    /* JADX WARNING: Missing block: B:10:0x004c, code:
            monitor-enter(r3);
     */
    /* JADX WARNING: Missing block: B:12:?, code:
            r5.currentCarLocation = r0;
            updateLastKnownCountryCode(r5.currentCarLocation);
     */
    /* JADX WARNING: Missing block: B:13:0x0054, code:
            monitor-exit(r3);
     */
    /* JADX WARNING: Missing block: B:14:0x0055, code:
            callListenersForCarLocation(r0);
     */
    /* JADX WARNING: Missing block: B:30:?, code:
            return;
     */
    /* JADX WARNING: Missing block: B:31:?, code:
            return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    @Subscribe
    public void onCarLocationChanged(CarLocationChangedEvent event) {
        synchronized (navdyLocationManagerLock) {
            if (this.isInjectingFakeCarLocations) {
            }
        }
    }

    private void getLastTripUpdateCoordinates() {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                double navdyLatitude = 0.0d;
                double navdyLongitude = 0.0d;
                long time = 0;
                Cursor tripsCursor = null;
                try {
                    tripsCursor = NavdyContentProvider.getTripsCursor();
                    if (tripsCursor != null && tripsCursor.moveToFirst()) {
                        Trip trip = NavdyContentProvider.getTripsItemAt(tripsCursor, tripsCursor.getPosition());
                        if (trip != null) {
                            if (trip.endLat == 0.0d && trip.endLong == 0.0d) {
                                navdyLatitude = trip.startLat;
                                navdyLongitude = trip.startLong;
                            } else {
                                navdyLatitude = trip.endLat;
                                navdyLongitude = trip.endLong;
                            }
                            time = trip.endTime;
                        }
                    }
                    IOUtils.closeStream(tripsCursor);
                    Location carLocation = new Location("");
                    carLocation.setLatitude(navdyLatitude);
                    carLocation.setLongitude(navdyLongitude);
                    carLocation.setTime(time);
                    if (NavdyLocationManager.this.isValidLocation(carLocation)) {
                        synchronized (NavdyLocationManager.navdyLocationManagerLock) {
                            NavdyLocationManager.this.currentCarLocation = carLocation;
                        }
                    }
                } catch (Throwable th) {
                    IOUtils.closeStream(tripsCursor);
                }
            }
        }, 1);
    }

    private boolean isValidLocation(Location location) {
        return location != null && (!(location.getLatitude() == 0.0d) || !(location.getLongitude() == 0.0d));
    }

    @NonNull
    private List<String> getBestAvailableLocationProviders() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(2);
        criteria.setPowerRequirement(1);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setSpeedRequired(false);
        LocationManager locationManager = (LocationManager) NavdyApplication.getAppContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = null;
        if (locationManager != null) {
            providers = locationManager.getProviders(criteria, true);
        }
        if (providers == null) {
            providers = new ArrayList();
        }
        if (providers.size() == 0) {
            providers.add("passive");
        }
        return providers;
    }

    @Nullable
    private Coordinate getCoordsFromLocation(@Nullable Location location) {
        if (location == null) {
            return null;
        }
        return MapUtils.buildNewCoordinate(location.getLatitude(), location.getLongitude());
    }

    private boolean isRecentAndAccurateEnough(Location location) {
        if (!MapUtils.isValidSetOfCoordinates(location)) {
            return false;
        }
        long now = System.currentTimeMillis();
        boolean newLocationIsRecent = now - location.getTime() < 30000;
        boolean newLocationIsAccurate = location.getAccuracy() < 500.0f;
        if (!newLocationIsRecent || !newLocationIsAccurate) {
            logger.v("phone location not recent/accurate enough");
            return false;
        }
        if (this.currentPhoneLocation == null) {
            return true;
        }

        boolean isMoreAccurateThanCurrentLocation = this.currentPhoneLocation.getAccuracy() > location.getAccuracy();
        boolean currentLocationIsTooOld = now - this.currentPhoneLocation.getTime() > 30000;
        return isMoreAccurateThanCurrentLocation || currentLocationIsTooOld;
    }

    private void callListenersForPhoneLocation(Location phoneLocation) {
        logger.v("phone location has changed to" + phoneLocation + ", calling listeners");
        Coordinate phoneCoords = getCoordsFromLocation(phoneLocation);
        if (phoneCoords != null) {
            for (WeakReference<OnNavdyLocationChangedListener> listenerWeakReference : this.listeners) {
                OnNavdyLocationChangedListener listener = listenerWeakReference.get();
                if (listener != null) {
                    listener.onPhoneLocationChanged(phoneCoords);
                }
            }
        }
    }

    private void callListenersForCarLocation(Location carLocation) {
        Coordinate carCoords = getCoordsFromLocation(carLocation);
        if (carCoords != null) {
            for (WeakReference<OnNavdyLocationChangedListener> listenerWeakReference : this.listeners) {
                OnNavdyLocationChangedListener listener = listenerWeakReference.get();
                if (listener != null) {
                    listener.onCarLocationChanged(carCoords);
                }
            }
        }
    }

    private void saveLocationToPreferences(Location location) {
        this.preferences.edit().putFloat(LATITUDE_PREFERENCE, (float) location.getLatitude()).putFloat(LONGITUDE_PREFERENCE, (float) location.getLongitude()).putLong(LOCATION_TIME_PREFERENCE, location.getTime()).putString(LOCATION_PROVIDER_PREFERENCE, location.getProvider()).putFloat(LOCATION_ACCURACY_PREFERENCE, location.getAccuracy()).apply();
    }

    private void registerFakeLocationReceiver(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(FAKE_PHONE_LOCATION_ACTION);
        intentFilter.addAction(FAKE_PHONE_LOCATION_ACTION_CANCEL);
        intentFilter.addAction(FAKE_CAR_LOCATION_ACTION);
        intentFilter.addAction(FAKE_CAR_LOCATION_ACTION_CANCEL);
        context.registerReceiver(this.fakeLocationReceiver, intentFilter);
    }

    private double getLocationScore(@NonNull Location location) {
        return ((double) location.getAccuracy()) - (((double) location.getTime()) / 1000.0d);
    }

    public static String getLastKnownCountryCode() {
        return lastKnownCountryCode;
    }

    public static void updateLastKnownCountryCode(Location currentLocation) {
        if (currentLocation != null) {
            final long now = new Date().getTime();
            if (now >= nextCountryCodeUpdate) {
                final Destination destination = new Destination();
                MapUtils.doReverseGeocodingFor(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), destination, new Runnable() {
                    public void run() {
                        String countryCode = destination.getCountryCode();
                        if (!StringUtils.isEmptyAfterTrim(countryCode)) {
                            NavdyLocationManager.lastKnownCountryCode = countryCode;
                            NavdyLocationManager.nextCountryCodeUpdate = now + NavdyLocationManager.COUNTRY_CODE_REFRESH_RATE;
                        }
                    }
                });
            }
        }
    }

    public static void forceLastKnownCountryCodeUpdate() {
        nextCountryCodeUpdate = 0;
    }
}
