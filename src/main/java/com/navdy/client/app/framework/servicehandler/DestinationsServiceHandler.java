package com.navdy.client.app.framework.servicehandler;

import android.database.Cursor;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.framework.util.VersioningUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.service.library.events.FavoriteDestinationsRequest;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.RecommendedDestinationsRequest;
import com.navdy.service.library.events.destination.RecommendedDestinationsUpdate;
import com.navdy.service.library.events.places.FavoriteDestinationsUpdate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import com.squareup.wire.Wire;
import java.util.ArrayList;
import java.util.List;

public class DestinationsServiceHandler {
    private static final boolean VERBOSE = false;
    public static final Logger logger = new Logger(DestinationsServiceHandler.class);
    private static DestinationsServiceHandler singleton = null;

    /* renamed from: com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler$2 */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$client$app$framework$servicehandler$DestinationsServiceHandler$DestinationType = new int[DestinationType.values().length];

        static {
            try {
                $SwitchMap$com$navdy$client$app$framework$servicehandler$DestinationsServiceHandler$DestinationType[DestinationType.FAVORITES.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$client$app$framework$servicehandler$DestinationsServiceHandler$DestinationType[DestinationType.SUGGESTIONS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    public enum DestinationType {
        SUGGESTIONS,
        FAVORITES
    }

    private DestinationsServiceHandler() {
        BusProvider.getInstance().register(this);
    }

    public static synchronized DestinationsServiceHandler getInstance() {
        DestinationsServiceHandler destinationsServiceHandler;
        synchronized (DestinationsServiceHandler.class) {
            if (singleton == null) {
                singleton = new DestinationsServiceHandler();
            }
            destinationsServiceHandler = singleton;
        }
        return destinationsServiceHandler;
    }

    @Subscribe
    public void onFavoriteDestinationsRequest(FavoriteDestinationsRequest favoriteDestinationsRequest) {
        logger.d("Request for Favorite Destinations received.");
        long favoritesVersion = VersioningUtils.getCurrentVersion(NavdyApplication.getAppContext(), DestinationType.FAVORITES);
        if (((Long) Wire.get(favoriteDestinationsRequest.serial_number, Long.valueOf(-1))).longValue() == favoritesVersion) {
            logger.v("Favorites Version up to date");
            sendRemoteMessage(new FavoriteDestinationsUpdate(RequestStatus.REQUEST_VERSION_IS_CURRENT, null, Long.valueOf(favoritesVersion), null));
            return;
        }
        sendDestinationsToDisplay(DestinationType.FAVORITES);
    }

    @Subscribe
    public void onRecommendedDestinationsRequest(RecommendedDestinationsRequest recommendedDestinationsRequest) {
        logger.d("Request for Recommended Destinations Received.");
        sendDestinationsToDisplay(DestinationType.SUGGESTIONS);
    }

    public static void sendDestinationsToDisplay(final DestinationType destinationType) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                switch (AnonymousClass2.$SwitchMap$com$navdy$client$app$framework$servicehandler$DestinationsServiceHandler$DestinationType[destinationType.ordinal()]) {
                    case 1:
                        DestinationsServiceHandler.sendFavoritesToDisplay();
                        return;
                    case 2:
                        DestinationsServiceHandler.sendSuggestionsToDisplay();
                        return;
                    default:
                        return;
                }
            }
        }, 1);
    }

    private static synchronized void sendFavoritesToDisplay() {
        synchronized (DestinationsServiceHandler.class) {
            Cursor cursor = null;
            try {
                long favoritesVersion = VersioningUtils.getCurrentVersion(NavdyApplication.getAppContext(), DestinationType.FAVORITES);
                cursor = NavdyContentProvider.getFavoritesCursor();
                if (cursor == null) {
                    sendRemoteMessage(new FavoriteDestinationsUpdate(RequestStatus.REQUEST_SUCCESS, null, Long.valueOf(favoritesVersion), new ArrayList()));
                    logger.v("closing cursor");
                    IOUtils.closeStream(cursor);
                } else {
                    int cursorCount = cursor.getCount();
                    ArrayList<Destination> protobufDestinations = new ArrayList(cursorCount);
                    for (int i = 0; i < cursorCount; i++) {
                        com.navdy.client.app.framework.models.Destination destination = NavdyContentProvider.getDestinationItemAt(cursor, i);
                        if (destination != null && destination.hasOneValidSetOfCoordinates()) {
                            protobufDestinations.add(destination.toProtobufDestinationObject());
                        }
                    }
                    if (protobufDestinations.size() > 0) {
                    }
                    sendRemoteMessage(new FavoriteDestinationsUpdate(RequestStatus.REQUEST_SUCCESS, null, Long.valueOf(favoritesVersion), protobufDestinations));
                    logger.v("closing cursor");
                    IOUtils.closeStream(cursor);
                }
            } catch (Throwable th) {
                logger.v("closing cursor");
                IOUtils.closeStream(cursor);
            }
        }
    }

    private static void sendSuggestionsToDisplay() {
        SuggestionManager.buildSuggestionList(true);
    }

    public static synchronized void sendSuggestionsToDisplay(long version, List<Suggestion> suggestions) {
        synchronized (DestinationsServiceHandler.class) {
            if (suggestions != null) {
                if (suggestions.size() > 0) {
                    logger.i("sendSuggestionsToDisplay sending list of size: " + suggestions.size());
                    ArrayList<Destination> protobufDestinations = new ArrayList(suggestions.size());
                    for (Suggestion suggestion : suggestions) {
                        if (!(suggestion == null || suggestion.destination == null || !suggestion.shouldSendToHud())) {
                            protobufDestinations.add(suggestion.toProtobufDestinationObject());
                        }
                    }
                    finalizeSuggestionsSendToNavdy(version, protobufDestinations);
                }
            }
            logger.i("sendSuggestionsToDisplay list is empty");
            sendRemoteMessage(new RecommendedDestinationsUpdate(RequestStatus.REQUEST_SUCCESS, null, Long.valueOf(version), new ArrayList()));
        }
    }

    private static void finalizeSuggestionsSendToNavdy(long version, List<Destination> protobufDestinations) {
        logger.i("finalizeSuggestionsSendToNavdy sending list of size: " + protobufDestinations.size());
        if (protobufDestinations.size() > 0) {
            sendRemoteMessage(new RecommendedDestinationsUpdate(RequestStatus.REQUEST_SUCCESS, null, Long.valueOf(version), protobufDestinations));
        } else {
            sendRemoteMessage(new RecommendedDestinationsUpdate(RequestStatus.REQUEST_SERVICE_ERROR, "No suggestions to send", Long.valueOf(0), null));
        }
    }

    private static void sendRemoteMessage(Message message) {
        if (!DeviceConnection.postEvent(message)) {
            logger.e("Unable to send message: " + message);
        }
    }
}
