package com.navdy.client.app.framework;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.ActivityRecognition;
import com.alelec.navdyclient.BuildConfig;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceInfoEvent;
import com.navdy.client.app.framework.service.ClientConnectionService;
import com.navdy.client.app.framework.service.ClientConnectionService.LocalBinder;
import com.navdy.client.app.framework.service.TaskRemovalService;
import com.navdy.client.app.framework.servicehandler.BatteryStatusManager;
import com.navdy.client.app.framework.servicehandler.CalendarHandler;
import com.navdy.client.app.framework.servicehandler.LocationTransmitter;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager;
import com.navdy.client.app.framework.suggestion.DestinationSuggestionService;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.SupportTicketService;
import com.navdy.client.app.service.ActivityRecognizedCallbackService;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.service.library.Version;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.discovery.BTRemoteDeviceScanner;
import com.navdy.service.library.events.Capabilities;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.location.TransmitLocation;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.service.library.events.settings.DateTimeConfiguration;
import com.navdy.service.library.events.settings.DateTimeConfiguration.Clock;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.Listenable;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

//import com.navdy.service.library.events.DeviceInfo.Platform;

public class AppInstance extends Listenable<AppInstance.Listener> implements com.navdy.client.app.framework.service.ClientConnectionService.DeviceChangeBroadcaster.Listener, ConnectionCallbacks, OnConnectionFailedListener {
    private static final long ACTIVITY_RECOGNITION_UPDATE_INTERVAL = TimeUnit.SECONDS.toMillis(10);
    private static final IntentFilter DATETIME_CHANGE_INTENT_FILTER = new IntentFilter("android.intent.action.DATE_CHANGED");
    private static final String EMPTY = "";
    private static final int REQUEST_CODE = 34567;
    private static final long SUGGESTION_KICK_OFF_DELAY_AFTER_CONNECTED = 15000;
    private static final long TRIP_DB_CHECK_KICK_OFF_DELAY_AFTER_CONNECTED = 60000;
    @SuppressLint({"StaticFieldLeak"})
    private static volatile AppInstance sInstance;
    public HomescreenActivity mHomescreenActivity;
    private static final Object sInstanceLock = new Object();
    public static final Logger sLogger = new Logger(AppInstance.class);
    private boolean busIsRegistered = false;
    private CalendarHandler calendarHandler = null;
    private final BroadcastReceiver datetimeUpdateReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            RemoteDevice remoteDevice = AppInstance.this.getRemoteDevice();
            if (remoteDevice != null) {
                AppInstance.this.sendCurrentTime(remoteDevice, DateFormat.is24HourFormat(context));
            }
        }
    };
    private volatile boolean deviceConnected = false;
    private boolean isHudMapEngineReady;
    private GoogleApiClient mApiClient;
    private boolean mAppInitialized = false;
    private BatteryStatusManager mBatteryStatus;
    private ClientConnectionService mConnectionService;
    private Context mContext = NavdyApplication.getAppContext();
    public LocationTransmitter mLocationTransmitter;
    private Handler mMainHandler = new Handler(this.mContext.getMainLooper());
    private NavigationSessionState mNavigationSessionState = NavigationSessionState.NAV_SESSION_ENGINE_NOT_READY;
    @Inject
    NetworkStatusManager mNetworkStatus;
    private RemoteDeviceRegistry mRemoteDeviceRegistry;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (service instanceof LocalBinder) {
                AppInstance.sLogger.i("Connected to ClientConnectionService");
                AppInstance.this.mConnectionService = ((LocalBinder) service).getService();
                AppInstance.this.mConnectionService.addListener(AppInstance.this);
                AppInstance.this.initializeDevice();
                return;
            }
            AppInstance.sLogger.v("service is not a LocalBinder, no-op");
        }

        public void onServiceDisconnected(ComponentName name) {
            AppInstance.sLogger.i("Disconnected from ClientConnectionService service");
            AppInstance.this.mConnectionService = null;
        }
    };

    interface EventDispatcher extends Listenable.EventDispatcher<AppInstance, Listener> {
    }

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onDeviceChanged(RemoteDevice remoteDevice);
    }

    static {
        DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIME_SET");
        DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIMEZONE_CHANGED");
    }

    private AppInstance() {
        Injector.inject(NavdyApplication.getAppContext(), this);
    }

    public static AppInstance getInstance() {
        if (sInstance == null) {
            synchronized (sInstanceLock) {
                if (sInstance == null) {
                    sInstance = new AppInstance();
                }
            }
        }
        return sInstance;
    }

    public synchronized void initializeApp() {
        if (!this.mAppInitialized) {
            this.mAppInitialized = true;
            initializeRegistry();
            initializeConnectionService();
            TaskRemovalService.startService(this.mContext);
            this.mApiClient = new Builder(NavdyApplication.getAppContext()).addApi(ActivityRecognition.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
            this.mApiClient.connect();
        }
    }

    public void initializeDevice() {
        ConnectionInfo connectionInfo = this.mRemoteDeviceRegistry.getDefaultConnectionInfo();
        if (connectionInfo != null && this.mConnectionService != null) {
            this.mConnectionService.connect(connectionInfo);
        }
    }

    private void initializeRegistry() {
        this.mRemoteDeviceRegistry = RemoteDeviceRegistry.getInstance(this.mContext);
        this.mRemoteDeviceRegistry.addRemoteDeviceScanner(new BTRemoteDeviceScanner(this.mContext, 12));
    }

    private void initializeConnectionService() {
        sLogger.v("init DeviceConnection");
        DeviceConnection.getInstance();
        sLogger.d("Binding to client connection service");
        Intent intent = new Intent(this.mContext, ClientConnectionService.class);
        intent.setAction(ClientConnectionService.class.getName());
        try {
            this.mContext.bindService(intent, this.serviceConnection, 1);
        } catch (Exception e) {
            e.printStackTrace();
            this.mAppInitialized = false;
        }
    }

    public void setLocationTransmitter(LocationTransmitter transmitter) {
        if (this.mLocationTransmitter != null) {
            this.mLocationTransmitter.stop();
        }
        this.mLocationTransmitter = transmitter;
        RemoteDevice device = this.mConnectionService.getRemoteDevice();
        if (device != null && device.isConnected() && this.mLocationTransmitter != null) {
            this.mLocationTransmitter.setHomescreenActivity(mHomescreenActivity);
            this.mLocationTransmitter.start();
        }
    }

    @Nullable
    public RemoteDevice getRemoteDevice() {
        if (this.mConnectionService != null) {
            return this.mConnectionService.getRemoteDevice();
        }
        return null;
    }

    public NavigationSessionState getNavigationSessionState() {
        return this.mNavigationSessionState;
    }

    public boolean isHudMapEngineReady() {
        return this.isHudMapEngineReady;
    }

    @Deprecated
    public void showToast(String toastText, boolean big) {
        showToast(toastText, big, 1);
    }

    @Deprecated
    public void showToast(final String toastText, final boolean big, final int toastDuration) {
        this.mMainHandler.post(new Runnable() {
            public void run() {
                Toast toast = Toast.makeText(AppInstance.this.mContext, toastText, toastDuration);
                View view = toast.getView();
                if (big) {
                    if (view instanceof LinearLayout) {
                        TextView messageTextView = (TextView) ((LinearLayout) view).getChildAt(0);
                        if (messageTextView != null) {
                            messageTextView.setTextSize(48.0f);
                        }
                    }
                    toast.setGravity(17, 0, 0);
                }
                toast.show();
            }
        });
    }

    void onDeviceConnectedFirstResponder(RemoteDevice device) {
        this.deviceConnected = true;
        if (!this.busIsRegistered) {
            this.busIsRegistered = true;
            BusProvider.getInstance().register(this);
        }
        BusProvider.getInstance().post(new DeviceConnectedEvent(device));
        requestActivityRecognitionUpdates();
        if (device != null) {
            sendPhoneInfo(device);
            sendCurrentTime(device, DateFormat.is24HourFormat(NavdyApplication.getAppContext()));
        }
        if (this.mLocationTransmitter == null) {
            this.mLocationTransmitter = new LocationTransmitter(this.mContext, device);
            this.mLocationTransmitter.setHomescreenActivity(mHomescreenActivity);
        }
        if (this.mBatteryStatus == null) {
            this.mBatteryStatus = new BatteryStatusManager(device);
        }
        this.mNetworkStatus.onRemoteDeviceConnected(device);
        this.mLocationTransmitter.start();
        this.mContext.registerReceiver(this.datetimeUpdateReceiver, DATETIME_CHANGE_INTENT_FILTER);
        this.mBatteryStatus.register();
        Tracker.tagEvent(Event.HUD_CONNECTION_ESTABLISHED);
        LocalyticsManager.setProfileAttribute(Attributes.MOBILE_APP_VERSION, BuildConfig.VERSION_NAME);
        if (SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.RECOMMENDATIONS_ENABLED, SettingsConstants.RECOMMENDATIONS_ENABLED_DEFAULT)) {
            this.mMainHandler.postDelayed(new Runnable() {
                public void run() {
                    DestinationSuggestionService.suggestDestination(AppInstance.this.mContext, false);
                }
            }, 15000);
        }
        if (this.calendarHandler == null) {
            this.calendarHandler = new CalendarHandler(this.mContext);
        }
        this.calendarHandler.registerObserver();
        CalendarUtils.sendCalendarsToHud(device);
        SupportTicketService.submitTicketsIfConditionsAreMet();
    }

    void onDeviceDisconnectedFirstResponder(RemoteDevice device, DisconnectCause cause) {
        String showName;
        this.mNavigationSessionState = NavigationSessionState.NAV_SESSION_STOPPED;
        this.deviceConnected = false;
        removeActivityRecognitionUpdates();
        this.isHudMapEngineReady = false;
        BusProvider.getInstance().post(new DeviceDisconnectedEvent(device, cause));
        if (this.mBatteryStatus != null) {
            this.mBatteryStatus.unregister();
            this.mBatteryStatus = null;
        }
        this.mNetworkStatus.onRemoteDeviceDisconnected();
        if (this.mLocationTransmitter != null) {
            this.mLocationTransmitter.stop();
            this.mLocationTransmitter = null;
        }
        try {
            showName = device.getDeviceId().getDisplayName();
        } catch (Exception e) {
            showName = "?";
        }
        showToast("Disconnected: " + showName, false);
        try {
            sLogger.v("unregistering datetimeUpdateReceiver");
            this.mContext.unregisterReceiver(this.datetimeUpdateReceiver);
        } catch (IllegalArgumentException e2) {
            sLogger.e("Cannot unregister datetime update receiver", e2);
        }
        Tracker.tagEvent(Event.HUD_CONNECTION_LOST);
        if (this.calendarHandler != null) {
            this.calendarHandler.unregisterObserver();
        }
        if (this.busIsRegistered) {
            this.busIsRegistered = false;
            BusProvider.getInstance().unregister(this);
        }
    }

    private synchronized void requestActivityRecognitionUpdates() {
        sLogger.d("GoogleApiClient : requesting activity updates");
        if (this.mApiClient.isConnected()) {
            sLogger.d("GoogleApiClient is already connected, requesting the updates");
            PendingIntent pendingIntent = PendingIntent.getService(NavdyApplication.getAppContext(), REQUEST_CODE, new Intent(NavdyApplication.getAppContext(), ActivityRecognizedCallbackService.class), 134217728);
            if (ActivityRecognition.ActivityRecognitionApi != null) {
                ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(this.mApiClient, ACTIVITY_RECOGNITION_UPDATE_INTERVAL, pendingIntent);
            }
        } else {
            sLogger.d("The GoogleApiClient is not connected yet.Waiting for it to be connected");
        }
    }

    private synchronized void removeActivityRecognitionUpdates() {
        sLogger.d("GoogleApiClient : removing activity updates");
        if (this.mApiClient != null && this.mApiClient.isConnected()) {
            PendingIntent pendingIntent = PendingIntent.getService(NavdyApplication.getAppContext(), REQUEST_CODE, new Intent(NavdyApplication.getAppContext(), ActivityRecognizedCallbackService.class), 134217728);
            if (!(ActivityRecognition.ActivityRecognitionApi == null || pendingIntent == null)) {
                try {
                    ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(this.mApiClient, pendingIntent);
                } catch (NullPointerException e) {
                    sLogger.e("Something went wrong while trying to remove activity recognition updates");
                }
            }
        }
        return;
    }

    public boolean isDeviceConnected() {
        return this.deviceConnected;
    }

    public boolean canReachInternet() {
        return this.mNetworkStatus.canReachInternet();
    }

    public void checkForNetwork() {
        this.mNetworkStatus.checkForNetwork();
    }

    @Subscribe
    public void onNavigationSessionStatusEvent(NavigationSessionStatusEvent status) {
        this.mNavigationSessionState = status.sessionState;
        if (status.sessionState == NavigationSessionState.NAV_SESSION_ENGINE_READY) {
            this.isHudMapEngineReady = true;
        }
    }

    @Subscribe
    public void onDeviceInfo(DeviceInfo deviceInfo) {
        RemoteDevice device = getRemoteDevice();
        if (device != null) {
            device.setDeviceInfo(deviceInfo);
        } else {
            sLogger.e("Unable to get remote device in order to update device info!");
        }
        sLogger.i("Display DeviceInfo: " + deviceInfo);
        if (deviceInfo.capabilities != null) {
            SettingsUtils.checkCapabilities(deviceInfo.capabilities);
        } else {
            SettingsUtils.checkLegacyCapabilityList(deviceInfo.legacyCapabilities);
        }
        LocalyticsManager.setProfileAttribute(Attributes.HUD_SERIAL_NUMBER, deviceInfo.deviceUuid);
        LocalyticsManager.setProfileAttribute(Attributes.HUD_VERSION_NUMBER, deviceInfo.systemVersion);
        OTAUpdateService.bPersistDeviceInfo(deviceInfo);
        OTAUpdateService.startService(this.mContext);
        BusProvider.getInstance().post(new DeviceInfoEvent(deviceInfo));
    }

    @Subscribe
    public void onTransmitLocation(TransmitLocation transmitLocation) {
        if (transmitLocation == null) {
            sLogger.e("location transmission event with a null transmit location extension !!! WTF !!!");
        } else if (Boolean.TRUE.equals(transmitLocation.sendLocation)) {
            sLogger.v("starting location transmission");
            this.mLocationTransmitter.start();
        } else {
            sLogger.v("stopping location transmission");
            this.mLocationTransmitter.stop();
        }
    }

    public void onDeviceChanged(final RemoteDevice newDevice) {
        sLogger.d("DeviceChanged: " + newDevice);
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(AppInstance source, Listener listener) {
                listener.onDeviceChanged(newDevice);
            }
        });
    }

    private void sendPhoneInfo(RemoteDevice device) {
        if (device == null) {
            sLogger.w("called setPhoneInfo with a null RemoteDevice!");
            return;
        }
        try {
            Capabilities.Builder capabilitiesBuilder = new Capabilities.Builder(null);
            capabilitiesBuilder.placeTypeSearch(Boolean.valueOf(true));
            capabilitiesBuilder.voiceSearch(Boolean.valueOf(true));
            capabilitiesBuilder.navCoordsLookup(Boolean.valueOf(true));
            capabilitiesBuilder.cannedResponseToSms(Boolean.valueOf(true));
            Capabilities capabilities = capabilitiesBuilder.build();
            NavdyDeviceId deviceId = NavdyDeviceId.getThisDevice(this.mContext);
            String deviceName = deviceId.getDeviceName();
            if (deviceName == null) {
                deviceName = "";
            }
            Message phoneInfo = new DeviceInfo.Builder()
                    .deviceId(deviceId.toString())
                    .clientVersion(BuildConfig.VERSION_NAME)
                    .protocolVersion(Version.PROTOCOL_VERSION.toString())
                    .deviceName(deviceName)
                    .systemVersion(getSystemVersion())
                    .model(Build.MODEL)
                    .deviceUuid("UUID")
                    .systemApiLevel(VERSION.SDK_INT)
                    .kernelVersion(System.getProperty("os.version"))
                    .platform(DeviceInfo.Platform.PLATFORM_Android)
                    .buildType(Build.TYPE)
                    .deviceMake(Build.MANUFACTURER)
                    .capabilities(capabilities)
                    .build();
            sLogger.v("sending device info:" + phoneInfo);
            device.postEvent(phoneInfo);
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    private String getSystemVersion() {
        if (String.valueOf(BuildConfig.VERSION_CODE).equals(VERSION.INCREMENTAL)) {
            return VERSION.INCREMENTAL;
        }
        return BuildConfig.VERSION_NAME;
    }

    private void sendCurrentTime(RemoteDevice remoteDevice, boolean is24HourFormat) {
        Calendar calendar = Calendar.getInstance();
        remoteDevice.postEvent(new DateTimeConfiguration(calendar.getTimeInMillis(), calendar.getTimeZone().getID(), is24HourFormat ? Clock.CLOCK_24_HOUR : Clock.CLOCK_12_HOUR));
    }

    public synchronized void onConnected(@Nullable Bundle bundle) {
        sLogger.d("GoogleApiClient : connected ");
        if (isDeviceConnected()) {
            sLogger.d("Remote device is already connected");
            requestActivityRecognitionUpdates();
        }
    }

    public void onConnectionSuspended(int i) {
        sLogger.d("GoogleApiClient : onConnectionSuspended , Connection :" + i);
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        sLogger.d("GoogleApiClient : onConnectionFailed , Connection result : " + connectionResult);
    }
}
