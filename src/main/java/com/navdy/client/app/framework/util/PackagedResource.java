package com.navdy.client.app.framework.util;

import android.content.Context;
import android.content.res.Resources;
import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class PackagedResource {
    private static final int BUFFER_SIZE = 16384;
    private static final String MD5_SUFFIX = ".md5";
    private static final Object lockObject = new Object();
    private static final Logger sLogger = new Logger(PackagedResource.class);
    private String basePath;
    private String destinationFilename;

    public PackagedResource(String destinationFilename, String basePath) {
        this.destinationFilename = destinationFilename;
        this.basePath = basePath;
    }

    public void updateFromResources(Context context, int resId, int hashResId) throws Throwable {
        updateFromResources(context, resId, hashResId, false);
    }

    public void updateFromResources(Context context, int resId, int hashResId, boolean unzip) throws Throwable {
        synchronized (lockObject) {
            String md5InApk;
            boolean copyFile = true;
            InputStream md5InputStream = null;
            Resources resources = context.getResources();
            File md5File = new File(this.basePath, this.destinationFilename + MD5_SUFFIX);
            File configFile = new File(this.basePath, this.destinationFilename);
            try {
                md5InputStream = resources.openRawResource(hashResId);
                md5InApk = IOUtils.convertInputStreamToString(md5InputStream, "UTF-8");
                IOUtils.closeStream(md5InputStream);
                md5InputStream = null;
                if (md5File.exists() && (unzip || configFile.exists())) {
                    String md5OnDevice = IOUtils.convertFileToString(md5File.getAbsolutePath());
                    if (StringUtils.equalsOrBothEmptyAfterTrim(md5OnDevice, md5InApk)) {
                        copyFile = false;
                        sLogger.d("no update required signature matches:" + md5OnDevice);
                    } else {
                        sLogger.d("update required, device:" + md5OnDevice + " apk:" + md5InApk);
                    }
                } else {
                    sLogger.d("update required, no file");
                }
                if (copyFile) {
                    IOUtils.copyFile(configFile.getAbsolutePath(), resources.openRawResource(resId));
                    if (unzip) {
                        unpackZip(this.basePath, configFile.getName());
                        IOUtils.deleteFile(context, configFile.getAbsolutePath());
                    }
                    IOUtils.copyFile(md5File.getAbsolutePath(), md5InApk.getBytes());
                }
            } catch (Throwable th) {
                IOUtils.closeStream(md5InputStream);
                sLogger.e("Error updateFromResources", th);
            }
        }
    }

    public static boolean unpackZipFromResource(String path, int resId) {
        boolean unpack;
        InputStream is = null;
        try {
            is = NavdyApplication.getAppContext().getResources().openRawResource(resId);
            unpack = unpack(path, is);
        } catch (Throwable e) {
            sLogger.e("Error reading resource", e);
            unpack = false;
        } finally {
            IOUtils.closeStream(is);
        }
        return unpack;
    }

    public static boolean unpackZip(String path, String zipname) {
        Throwable e;
        Throwable th;
        InputStream is = null;
        try {
            InputStream is2 = new FileInputStream(path + File.separator + zipname);
            try {
                boolean unpack = unpack(path, is2);
                IOUtils.closeStream(is2);
                is = is2;
                return unpack;
            } catch (Throwable th2) {
                th = th2;
                is = is2;
                IOUtils.closeStream(is);
                throw th;
            }
        } catch (Throwable th3) {
            e = th3;
            sLogger.e("Error reading resource", e);
            IOUtils.closeStream(is);
            return false;
        }
    }

    private static boolean unpack(String path, InputStream is) {
        File destDir = new File(path);
        byte[] buffer = new byte[16384];
        if (!(destDir.exists() || destDir.mkdirs())) {
            sLogger.e("unpack could not create destination dirs");
        }
        ZipInputStream zis = new ZipInputStream(new BufferedInputStream(is));
        while (true) {
            try {
                ZipEntry ze = zis.getNextEntry();
                if (ze != null) {
                    String filePath = path + File.separator + ze.getName();
                    if (!ze.isDirectory()) {
                        extractFile(zis, filePath, buffer);
                    } else if (!new File(filePath).mkdirs()) {
                        sLogger.e("unpack could not create destination dirs");
                    }
                    zis.closeEntry();
                } else {
                    IOUtils.closeStream(zis);
                    return true;
                }
            } catch (Throwable th) {
                IOUtils.closeStream(zis);
                th.printStackTrace();
            }
        }
    }

    private static void extractFile(ZipInputStream zis, String filePath, byte[] buffer) {
        Throwable t;
        Throwable th;
        FileOutputStream fout = null;
        try {
            FileOutputStream fout2 = new FileOutputStream(filePath);
            while (true) {
                try {
                    int count = zis.read(buffer);
                    if (count != -1) {
                        fout2.write(buffer, 0, count);
                    } else {
                        IOUtils.fileSync(fout2);
                        IOUtils.closeStream(fout2);
                        fout = fout2;
                        return;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fout = fout2;
                    IOUtils.closeStream(fout);
                    throw th;
                }
            }
        } catch (Throwable th3) {
            t = th3;
            sLogger.e("error extracting file", t);
            IOUtils.closeStream(fout);
        }
    }
}
