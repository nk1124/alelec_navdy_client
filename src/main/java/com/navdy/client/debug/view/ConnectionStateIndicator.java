package com.navdy.client.debug.view;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.alelec.navdyclient.R;
import com.navdy.service.library.device.connection.Connection.Status;
import com.navdy.service.library.device.connection.ConnectionType;

public class ConnectionStateIndicator extends ImageView {
    private static final int[] CONNECTED = new int[]{R.attr.connected};
    private static final int[] CONNECTING = new int[]{R.attr.connecting};
    private static final int[] DISCONNECTED = new int[]{R.attr.disconnected};
    private static final int[] USING_BLUETOOTH = new int[]{R.attr.using_bluetooth};
    private Status status;
    private boolean usingBluetooth;

    public ConnectionStateIndicator(Context context) {
        this(context, null);
    }

    public ConnectionStateIndicator(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ConnectionStateIndicator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.usingBluetooth = false;
        this.status = Status.DISCONNECTED;
    }

    public int[] onCreateDrawableState(int extraSpace) {
        int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
        if (this.usingBluetooth) {
            mergeDrawableStates(drawableState, USING_BLUETOOTH);
        }
        if (this.status == null) {
            mergeDrawableStates(drawableState, DISCONNECTED);
        } else {
            switch (this.status) {
                case DISCONNECTED:
                    mergeDrawableStates(drawableState, DISCONNECTED);
                    break;
                case CONNECTING:
                    mergeDrawableStates(drawableState, CONNECTING);
                    break;
                case CONNECTED:
                    mergeDrawableStates(drawableState, CONNECTED);
                    break;
            }
        }
        return drawableState;
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable drawable = getDrawable();
        if (drawable != null && (drawable.getCurrent() instanceof AnimationDrawable)) {
            ((AnimationDrawable) drawable.getCurrent()).start();
        }
    }

    public void setConnectionType(ConnectionType type) {
        boolean bluetooth = type == ConnectionType.BT_PROTOBUF;
        if (this.usingBluetooth != bluetooth) {
            this.usingBluetooth = bluetooth;
            refreshDrawableState();
        }
    }

    public void setConnectionStatus(Status status) {
        if (this.status != status) {
            this.status = status;
            refreshDrawableState();
        }
    }
}
