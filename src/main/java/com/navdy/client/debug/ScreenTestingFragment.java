package com.navdy.client.debug;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import butterknife.ButterKnife;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.notification.ShowCustomNotification;
import com.navdy.service.library.events.ui.Screen;
import com.navdy.service.library.events.ui.ShowScreen.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;
import java.util.ArrayList;
import java.util.Arrays;

public class ScreenTestingFragment extends ListFragment {
    private static final Logger sLogger = new Logger(ScreenTestingFragment.class);
    AppInstance mAppInstance = AppInstance.getInstance();
    protected ArrayList<ListItem> mListItems;

    enum ListItem {
        WELCOME("Welcome"),
        HYBRID_MAP(GlanceConstants.ACTION_MAP),
        DASH("SmartDash"),
        CONTEXT_MENU("Context menu"),
        MAIN_MENU("Main menu"),
        FAV_PLACES("Favorite Places"),
        RECOMMENDED_PLACES("Recommended Places"),
        FAV_CONTACT("Favorite ContactModel"),
        RECENT_CONTACT("Recent ContactModel"),
        SCREEN_BACK("Back"),
        SCREEN_OTA_CONFIRM("OTA Confirmation"),
        SCREEN_OPTIONS("Options"),
        SCREEN_NOTIFICATION("Show Brightness Notification"),
        SHUTDOWN("ShutDown"),
        DIAL_PAIRING("Dial Pairing");
        
        private final String mText;

        private ListItem(String item) {
            this.mText = item;
        }

        public String toString() {
            return this.mText;
        }
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        this.mListItems = new ArrayList(Arrays.asList(ListItem.values()));
        setListAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_activated_1, android.R.id.text1, ListItem.values()));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_test_screen, container, false);
        ButterKnife.inject((Object) this, rootView);
        return rootView;
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        switch ((ListItem) this.mListItems.get(position)) {
            case WELCOME:
                sendEvent(new Builder().screen(Screen.SCREEN_WELCOME).build());
                return;
            case HYBRID_MAP:
                sendEvent(new Builder().screen(Screen.SCREEN_HYBRID_MAP).build());
                return;
            case DASH:
                sendEvent(new Builder().screen(Screen.SCREEN_DASHBOARD).build());
                return;
            case CONTEXT_MENU:
                sendEvent(new Builder().screen(Screen.SCREEN_CONTEXT_MENU).build());
                return;
            case MAIN_MENU:
                sendEvent(new Builder().screen(Screen.SCREEN_MENU).build());
                return;
            case FAV_PLACES:
                sendEvent(new Builder().screen(Screen.SCREEN_FAVORITE_PLACES).build());
                return;
            case RECOMMENDED_PLACES:
                sendEvent(new Builder().screen(Screen.SCREEN_RECOMMENDED_PLACES).build());
                return;
            case FAV_CONTACT:
                sendEvent(new Builder().screen(Screen.SCREEN_FAVORITE_CONTACTS).build());
                return;
            case RECENT_CONTACT:
                sendEvent(new Builder().screen(Screen.SCREEN_RECENT_CALLS).build());
                return;
            case SCREEN_BACK:
                sendEvent(new Builder().screen(Screen.SCREEN_BACK).build());
                return;
            case SCREEN_OTA_CONFIRM:
                sendEvent(new Builder().screen(Screen.SCREEN_OTA_CONFIRMATION).build());
                return;
            case SCREEN_OPTIONS:
                sendEvent(new Builder().screen(Screen.SCREEN_OPTIONS).build());
                return;
            case SCREEN_NOTIFICATION:
                sendEvent(new ShowCustomNotification("BRIGHTNESS"));
                return;
            case SHUTDOWN:
                sendEvent(new Builder().screen(Screen.SCREEN_SHUTDOWN_CONFIRMATION).build());
                return;
            case DIAL_PAIRING:
                sendEvent(new Builder().screen(Screen.SCREEN_DIAL_PAIRING).build());
                return;
            default:
                sLogger.d("unhandled: " + id);
                return;
        }
    }

    private void sendEvent(Message event) {
        RemoteDevice remoteDevice = this.mAppInstance.getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent(event);
        }
    }
}
