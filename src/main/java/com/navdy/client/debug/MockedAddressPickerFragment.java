package com.navdy.client.debug;

import android.os.Bundle;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import com.navdy.service.library.events.places.PlacesSearchResponse;
import com.squareup.otto.Subscribe;

public class MockedAddressPickerFragment extends RemoteAddressPickerFragment {
    private AppInstance appInstance;

    static {
        fragmentTitle = R.string.title_mocked_position_search;
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        this.appInstance = AppInstance.getInstance();
    }

    protected void processSelectedLocation(Coordinate location, String locationLabel, String streetAddress) {
        this.appInstance.showToast("Current location mocked to " + locationLabel, false);
        getFragmentManager().popBackStack();
    }

    @Subscribe
    public void onPlacesSearchResponse(PlacesSearchResponse response) {
        super.onPlacesSearchResponse(response);
    }

    @Subscribe
    public void onNavigationRouteResponse(NavigationRouteResponse event) {
        super.onNavigationRouteResponse(event);
    }
}
