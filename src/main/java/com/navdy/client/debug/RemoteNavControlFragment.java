package com.navdy.client.debug;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.CrashlyticsAppender;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.events.navigation.NavigationSessionRequest;
import com.navdy.service.library.events.navigation.NavigationSessionResponse;
import com.navdy.service.library.events.navigation.NavigationSessionState;
import com.navdy.service.library.events.navigation.NavigationSessionStatusEvent;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import org.droidparts.contract.SQL.DDL;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class RemoteNavControlFragment extends Fragment {
    private static final String ARG_DESTINATION_LABEL = "destination";
    private static final String ARG_ROUTE_ID = "routeId";
    private static final String ARG_ROUTE_LABEL = "routeLabel";
    public static final String EXT_TAG = "RemoteNavControlFragment";
    public static final Logger sLogger = new Logger(RemoteNavControlFragment.class);
    private Button[] mAllButtons;
    private String mDestinationLabel;
    @InjectView(R.id.navigation_destination_label)
    protected TextView mDestinationLabelTextView;
    @InjectView(R.id.navigation_button_pause)
    protected Button mNavigationButtonPause;
    @InjectView(R.id.navigation_button_resume)
    protected Button mNavigationButtonResume;
    @InjectView(R.id.navigation_button_simulate)
    protected Button mNavigationButtonSimulate;
    @InjectView(R.id.navigation_button_start)
    protected Button mNavigationButtonStart;
    @InjectView(R.id.navigation_button_stop)
    protected Button mNavigationButtonStop;
    private NavigationSessionState mNavigationSessionState;
    private NavigationRouteResult mRouteResult;
    @InjectView(R.id.test_textview_current_sim_speed)
    protected TextView mTextViewCurrentSimSpeed;

    public static RemoteNavControlFragment newInstance(NavigationRouteResult routeResult, String destinationLabel) {
        RemoteNavControlFragment fragment = new RemoteNavControlFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ROUTE_ID, routeResult.routeId);
        args.putString(ARG_ROUTE_LABEL, routeResult.label);
        args.putString(ARG_DESTINATION_LABEL, destinationLabel);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mRouteResult = new NavigationRouteResult(getArguments().getString(ARG_ROUTE_ID), getArguments().getString(ARG_ROUTE_LABEL), null, Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), null, null, null);
            this.mDestinationLabel = getArguments().getString(ARG_DESTINATION_LABEL);
        }
        this.mNavigationSessionState = AppInstance.getInstance().getNavigationSessionState();
    }

    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_remote_nav_control, container, false);
        ButterKnife.inject((Object) this, rootView);
        this.mAllButtons = new Button[]{this.mNavigationButtonStart, this.mNavigationButtonSimulate, this.mNavigationButtonPause, this.mNavigationButtonResume, this.mNavigationButtonStop};
        if (!StringUtils.isEmptyAfterTrim(this.mDestinationLabel)) {
            this.mDestinationLabelTextView.setText(this.mDestinationLabel);
        }
        updateNavigationSessionState(this.mNavigationSessionState);
        this.mTextViewCurrentSimSpeed.setText("Current simulation speed: " + DebugActionsFragment.getSimulationSpeed() + " m/s " + DDL.SEPARATOR + ((int) (((float) DebugActionsFragment.getSimulationSpeed()) * 2.2369f)) + " MPH");
        return rootView;
    }

    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    private void updateNavigationSessionState(NavigationSessionState newState) {
        sLogger.d("RemoteNavControlFragment: : Received new state: " + newState);
        for (Button button : this.mAllButtons) {
            button.setEnabled(true);
            button.setVisibility(GONE);
        }
        switch (newState) {
            case NAV_SESSION_STOPPED:
                this.mNavigationButtonStart.setVisibility(VISIBLE);
                this.mNavigationButtonSimulate.setVisibility(VISIBLE);
                break;
            case NAV_SESSION_PAUSED:
                this.mNavigationButtonResume.setVisibility(VISIBLE);
                this.mNavigationButtonStop.setVisibility(VISIBLE);
                break;
            case NAV_SESSION_STARTED:
                this.mNavigationButtonPause.setVisibility(VISIBLE);
                break;
            default:
                this.mNavigationButtonStart.setVisibility(VISIBLE);
                this.mNavigationButtonSimulate.setVisibility(VISIBLE);
                this.mNavigationButtonStart.setEnabled(false);
                this.mNavigationButtonSimulate.setEnabled(false);
                break;
        }
        this.mNavigationSessionState = newState;
    }

    @OnClick({R.id.navigation_button_start})
    protected void onStartClicked() {
        sendStateChangeRequest(NavigationSessionState.NAV_SESSION_STARTED, 0);
    }

    @OnClick({R.id.navigation_button_simulate})
    protected void onSimulateClicked() {
        sendStateChangeRequest(NavigationSessionState.NAV_SESSION_STARTED, DebugActionsFragment.getSimulationSpeed());
    }

    @OnClick({R.id.navigation_button_pause})
    protected void onPauseClicked() {
        sendStateChangeRequest(NavigationSessionState.NAV_SESSION_PAUSED);
    }

    @OnClick({R.id.navigation_button_resume})
    protected void onResumeClicked() {
        sendStateChangeRequest(NavigationSessionState.NAV_SESSION_STARTED);
    }

    @OnClick({R.id.navigation_button_stop})
    protected void onStopClicked() {
        sendStateChangeRequest(NavigationSessionState.NAV_SESSION_STOPPED);
    }

    private void sendStateChangeRequest(NavigationSessionState newState) {
        sendStateChangeRequest(newState, 0);
    }

    private void sendStateChangeRequest(NavigationSessionState newState, int simulationSpeed) {
        sLogger.d("RemoteNavControlFragment: Attempting to change state: " + newState);
        if (DeviceConnection.isConnected()) {
            if (DeviceConnection.postEvent(new NavigationSessionRequest(newState, this.mDestinationLabel, this.mRouteResult.routeId, Integer.valueOf(simulationSpeed), Boolean.valueOf(false)))) {
                sLogger.e("RemoteNavControlFragment: State change request sent successfully.");
                return;
            } else {
                sLogger.e("RemoteNavControlFragment: State change request failed to send.");
                return;
            }
        }
        sLogger.d("RemoteNavControlFragment: : device is no longer connected");
    }

    @Subscribe
    public void onDeviceDisconnectedEvent(DeviceDisconnectedEvent event) {
        updateNavigationSessionState(NavigationSessionState.NAV_SESSION_STOPPED);
    }

    @Subscribe
    public void onNavigationSessionStatusEvent(NavigationSessionStatusEvent status) {
        updateNavigationSessionState(status.sessionState);
    }

    @Subscribe
    public void onNavigationSessionResponse(NavigationSessionResponse response) {
        sLogger.e("RemoteNavControlFragment: Session response for pendingState: " + response.pendingSessionState + CrashlyticsAppender.SEPARATOR + response.status + " - " + response.statusDetail);
    }
}
