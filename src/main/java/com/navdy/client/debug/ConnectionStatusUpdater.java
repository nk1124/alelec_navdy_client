package com.navdy.client.debug;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectingEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectionFailedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.debug.devicepicker.Device;
import com.navdy.client.debug.view.ConnectionStateIndicator;
import com.squareup.otto.Subscribe;

class ConnectionStatusUpdater {
    @InjectView(R.id.connectionStateIndicator)
    ConnectionStateIndicator mConnectionIndicator;
    @InjectView(R.id.current_device)
    TextView mTextViewCurrentDevice;

    ConnectionStatusUpdater(View parentView) {
        ButterKnife.inject((Object) this, parentView);
    }

    public void onResume() {
        BusProvider.getInstance().register(this);
        updateView();
    }

    public void onPause() {
        BusProvider.getInstance().unregister(this);
    }

    @SuppressLint({"SetTextI18n"})
    private void updateView() {
        DeviceConnection deviceConnection = DeviceConnection.getInstance();
        if (deviceConnection != null) {
            this.mConnectionIndicator.setConnectionType(deviceConnection.getConnectionType());
            this.mConnectionIndicator.setConnectionStatus(deviceConnection.getConnectionStatus());
            this.mTextViewCurrentDevice.setText("Current device: " + Device.prettyName(deviceConnection.getConnectionInfo()));
        }
    }

    @Subscribe
    public void onDeviceConnectingEvent(DeviceConnectingEvent event) {
        updateView();
    }

    @Subscribe
    public void onDeviceConnectFailureEvent(DeviceConnectionFailedEvent event) {
        updateView();
    }

    @Subscribe
    public void onDeviceConnectedEvent(DeviceConnectedEvent event) {
        updateView();
    }

    @Subscribe
    public void onDeviceDisconnectedEvent(DeviceDisconnectedEvent event) {
        updateView();
    }
}
