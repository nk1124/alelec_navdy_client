package com.navdy.client.debug;

import android.app.ListFragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import butterknife.ButterKnife;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.events.dial.DialBondRequest;
import com.navdy.service.library.events.dial.DialBondRequest.DialAction;
import com.navdy.service.library.events.dial.DialBondResponse;
import com.navdy.service.library.events.dial.DialStatusRequest;
import com.navdy.service.library.events.dial.DialStatusResponse;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NavdyDialFragment extends ListFragment {
    private static final Logger sLogger = new Logger(NavdyDialFragment.class);
    private AppInstance appInstance;
    private DialAction lastAction;
    private List<ListItem> listItems;

    private enum ListItem {
        GET_DIAL_STATUS(R.string.dial_get_status),
        BOND(R.string.dial_bond),
        REBOND(R.string.dial_rebond),
        CLEAR_BOND(R.string.dial_clear_bond);
        
        private final int mResId;
        private String mString;

        private ListItem(int item) {
            this.mResId = item;
        }

        void bindResource(Resources r) {
            this.mString = r.getString(this.mResId);
        }

        public String toString() {
            return this.mString;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appInstance = AppInstance.getInstance();
        this.listItems = new ArrayList(Arrays.asList(ListItem.values()));
        for (ListItem item : this.listItems) {
            item.bindResource(getResources());
        }
        setListAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_activated_1, android.R.id.text1, ListItem.values()));
    }

    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.inject((Object) this, rootView);
        return rootView;
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        ListItem clickedItem = (ListItem) this.listItems.get(position);
        if (DeviceConnection.isConnected()) {
            switch (clickedItem) {
                case GET_DIAL_STATUS:
                    DeviceConnection.postEvent(new DialStatusRequest());
                    sLogger.v("sent dial status request");
                    return;
                case BOND:
                    this.lastAction = DialAction.DIAL_BOND;
                    DeviceConnection.postEvent(new DialBondRequest(DialAction.DIAL_BOND));
                    sLogger.v("sent dial bonding request");
                    return;
                case REBOND:
                    this.lastAction = DialAction.DIAL_REBOND;
                    DeviceConnection.postEvent(new DialBondRequest(DialAction.DIAL_REBOND));
                    sLogger.v("sent dial repair bonding request");
                    return;
                case CLEAR_BOND:
                    this.lastAction = DialAction.DIAL_CLEAR_BOND;
                    DeviceConnection.postEvent(new DialBondRequest(DialAction.DIAL_CLEAR_BOND));
                    sLogger.v("sent dial clear bond request");
                    return;
                default:
                    return;
            }
        }
        this.appInstance.showToast("Not connected to Hud", false, 0);
    }

    @Subscribe
    public void onDialStatusResponse(DialStatusResponse status) {
        StringBuilder builder = new StringBuilder();
        if (Boolean.TRUE.equals(status.isPaired)) {
            builder.append("Dial '").append(status.name).append("' is Paired ");
            if (Boolean.TRUE.equals(status.isConnected)) {
                builder.append("and Connected");
            } else {
                builder.append("but Not Connected");
            }
        } else {
            builder.append("Dial is not paired");
        }
        this.appInstance.showToast(builder.toString(), true, 1);
    }

    @Subscribe
    public void onDialBondResponse(DialBondResponse response) {
        String msg = "";
        switch (response.status) {
            case DIAL_NOT_FOUND:
                msg = "No Dial Found";
                break;
            case DIAL_ALREADY_PAIRED:
                msg = "Already paired with " + response.name;
                break;
            case DIAL_PAIRED:
                msg = "Paired with " + response.name;
                break;
            case DIAL_NOT_PAIRED:
                if (this.lastAction == null) {
                    msg = "Cannot pair";
                    break;
                }
                if (this.lastAction == DialAction.DIAL_CLEAR_BOND || this.lastAction == DialAction.DIAL_REBOND) {
                    msg = "Pairing removed";
                } else {
                    msg = "Cannot pair";
                }
                this.lastAction = null;
                break;
            case DIAL_OPERATION_IN_PROGRESS:
                msg = "Pairing in progresss";
                break;
            case DIAL_ERROR:
                msg = "Error occurred";
                break;
            default:
                sLogger.e("Unknown dial response status: " + response.status);
                break;
        }
        this.appInstance.showToast(msg, true, 0);
    }
}
