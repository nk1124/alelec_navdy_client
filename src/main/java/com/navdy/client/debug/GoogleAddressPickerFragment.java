package com.navdy.client.debug;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.internal.view.SupportMenu;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.PendingResult.Callback;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.map.MapsForWorkUtils;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.CrashlyticsAppender;
import com.navdy.client.debug.DestinationAddressPickerFragment.DestinationListener;
import com.navdy.client.debug.adapter.RouteDescriptionData;
import com.navdy.client.debug.common.BaseDebugFragment;
import com.navdy.client.debug.navigation.HUDNavigationManager;
import com.navdy.client.debug.navigation.NavigationManager;
import com.navdy.client.debug.util.FormatUtils;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.navigation.NavigationRouteResponse;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.droidparts.contract.SQL.DDL;
import org.json.JSONException;
import org.json.JSONObject;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class GoogleAddressPickerFragment extends BaseDebugFragment implements OnConnectionFailedListener, ConnectionCallbacks, LocationListener {
    private static final String HERE_GEOCODER_ENDPOINT = "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=";
    private static final String HERE_GEOCODER_ENDPOINT2 = "&app_code=";
    private static final String HERE_GEOCODER_ENDPOINT3 = "&gen=9&searchtext=";
    private static String HERE_GEO_APP_ID = null;
    private static final String HERE_GEO_APP_ID_METADATA = "HERE_GEO_APP_ID";
    private static String HERE_GEO_APP_TOKEN = null;
    private static final String HERE_GEO_APP_TOKEN_METADATA = "HERE_GEO_APP_TOKEN";
    private static final String HERE_LATITUDE_STR = "Latitude";
    private static final String HERE_LOCATION_STR = "Location";
    private static final String HERE_LONGITUDE_STR = "Longitude";
    private static final String HERE_NAVIGATION_STR = "NavigationPosition";
    private static final String HERE_RESPONSE_STR = "Response";
    private static final String HERE_RESULT_STR = "Result";
    private static final String HERE_VIEW_STR = "View";
    private static final int ROUTE_REQUEST_TIMEOUT = 15000;
    private static final int SEARCH_RADIUS_METERS = 50000;
    private static final String SI_IS_ROUTE_REQUEST_PENDING = "1";
    private static final Logger sLogger = new Logger(GoogleAddressPickerFragment.class);
    private NavigationRouteResult chousedRoute;
    private int currentSelectedRouteIndex = -1;
    private NavigationRouteResponse event;
    private GeoApiContext geoApiContext;
    private Handler handler = new Handler();
    private Location lastLocation;
    @InjectView(R.id.listView)
    ListView listView;
    private PlaceAutocompleteAdapter mAdapter;
    private OnItemClickListener mAutocompleteClickListener = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            if (GoogleAddressPickerFragment.this.lastLocation == null) {
                Toast.makeText(NavdyApplication.getAppContext(), GoogleAddressPickerFragment.this.getString(R.string.no_location), Toast.LENGTH_LONG).show();
                return;
            }
            PlaceDebugAutocomplete item = GoogleAddressPickerFragment.this.mAdapter.getItem(position);
            String placeId = String.valueOf(item.placeId);
            GoogleAddressPickerFragment.sLogger.i("Autocomplete item selected: " + item.description);
            Places.GeoDataApi.getPlaceById(GoogleAddressPickerFragment.this.mGoogleApiClient, placeId).setResultCallback(GoogleAddressPickerFragment.this.mUpdatePlaceDetailsCallback);
            ((InputMethodManager) GoogleAddressPickerFragment.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(GoogleAddressPickerFragment.this.getActivity().getCurrentFocus().getWindowToken(), 0);
            GoogleAddressPickerFragment.this.mAutocompleteView.setText("");
            GoogleAddressPickerFragment.this.listView.setVisibility(GONE);
            GoogleAddressPickerFragment.sLogger.i("Called getPlaceById to get Place details for " + item.placeId);
            GoogleAddressPickerFragment.this.startProgress(GoogleAddressPickerFragment.this.getString(R.string.search_for_route));
        }
    };
    @InjectView(R.id.nav_autocomplete_places)
    AutoCompleteTextView mAutocompleteView;
    protected GoogleApiClient mGoogleApiClient;
    protected Place mLastSelectedPlace;
    private LocationRequest mLocationRequest;
    private GoogleMap mMap;
    protected NavigationManager mNavigationManager;
    protected LatLng mSearchLocation;
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                GoogleAddressPickerFragment.this.stopProgress();
                GoogleAddressPickerFragment.sLogger.e("Place query did not complete. Error: " + places.getStatus().toString());
                Toast.makeText(NavdyApplication.getAppContext(), places.getStatus().toString(), Toast.LENGTH_LONG).show();
            } else if (places.getCount() == 0) {
                GoogleAddressPickerFragment.this.stopProgress();
                places.release();
                GoogleAddressPickerFragment.sLogger.e("No places returned");
                Toast.makeText(NavdyApplication.getAppContext(), R.string.search_failed, Toast.LENGTH_LONG).show();
            } else {
                boolean b;
                Place place = null;
                boolean exitLoop = false;
                Iterator it = places.iterator();
                while (it.hasNext()) {
                    Place p = (Place) it.next();
                    for (Integer i : p.getPlaceTypes()) {
                        if (i == 1021) {
                            place = p;
                            exitLoop = true;
                            continue;
                        }
                    }
                    if (exitLoop) {
                        break;
                    }
                }
                if (place == null) {
                    GoogleAddressPickerFragment.sLogger.w("Street address type lookup failed: place is null");
                    place = places.get(0);
                    b = false;
                } else {
                    b = true;
                }
                final boolean isStreetAddress = b;
                GoogleAddressPickerFragment.sLogger.i("Place details received: " + place.getName());
                GoogleAddressPickerFragment.this.mLastSelectedPlace = place;
                GoogleAddressPickerFragment.this.updateMap(place);
                final Place destinationPlace = place;
                GoogleAddressPickerFragment.sLogger.v("google display coordinate=" + place.getLatLng());
                CharSequence name = destinationPlace.getName();
                CharSequence address = destinationPlace.getAddress();
                final PlaceBuffer placeBuffer = places;
                NavCoordsAddressProcessor.processDestination(new Destination(name != null ? name.toString() : "", address != null ? address.toString() : ""), new OnCompleteCallback() {
                    public void onSuccess(Destination destination) {
                        GoogleAddressPickerFragment.this.searchRoutesOnHud(destinationPlace, isStreetAddress, new LatLng(destination.navigationLat, destination.navigationLong));
                        placeBuffer.release();
                    }

                    public void onFailure(Destination destination, Error error) {
                        GoogleAddressPickerFragment.this.searchRoutesOnHud(destinationPlace, isStreetAddress, null);
                        placeBuffer.release();
                    }
                });
            }
        }
    };
    private ProgressDialog progressDialog;
    private boolean requestedLocationUpdates;
    private RouteDescriptionAdapter routeDescriptionAdapter;
    private boolean routeRequestSendToHud;
    private Runnable routeRequestTimeout = new Runnable() {
        public void run() {
            if (GoogleAddressPickerFragment.this.isAlive() && GoogleAddressPickerFragment.this.routeRequestSendToHud) {
                GoogleAddressPickerFragment.this.logger.v("HUD route request timed out");
                GoogleAddressPickerFragment.this.routeRequestSendToHud = false;
                GoogleAddressPickerFragment.this.stopProgress();
                Toast.makeText(NavdyApplication.getAppContext(), R.string.timed_out, Toast.LENGTH_LONG).show();
            }
        }
    };
    private List<Polyline> routes = new ArrayList();

    interface INavigationPositionCallback {
        void onFailure(Throwable th);

        void onSuccess(LatLng latLng);
    }

    private class RouteDescriptionAdapter extends BaseAdapter {
        private Context context;
        private List<RouteDescriptionData> data;

        class ViewHolder {
            TextView duration;
            TextView length;
            Button queueNavigation;
            TextView routeDiff;
            Button startNavigation;

            ViewHolder() {
            }
        }

        RouteDescriptionAdapter(List<RouteDescriptionData> data, Context context) {
            this.data = data;
            this.context = context;
        }

        public void setData(List<RouteDescriptionData> data) {
            this.data = data;
            notifyDataSetChanged();
        }

        public List<RouteDescriptionData> getData() {
            return this.data;
        }

        public int getCount() {
            return this.data.size();
        }

        public RouteDescriptionData getItem(int position) {
            return this.data.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            RouteDescriptionData item = getItem(position);
            if (convertView == null) {
                convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.route_description, parent, false);
                holder = new ViewHolder();
                holder.routeDiff = (TextView)convertView.findViewById(R.id.brightnessTextView);
                holder.duration = (TextView)convertView.findViewById(R.id.duration);
                holder.length = (TextView)convertView.findViewById(R.id.textView3);
                holder.startNavigation = (Button)convertView.findViewById(R.id.button);
                holder.queueNavigation = (Button)convertView.findViewById(R.id.button2);
                convertView.setTag(holder);
                holder.startNavigation.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        if (GoogleAddressPickerFragment.this.getActivity() instanceof RouteListener) {
                            ((RouteListener) GoogleAddressPickerFragment.this.getActivity()).setRoute(GoogleAddressPickerFragment.this.chousedRoute);
                            GoogleAddressPickerFragment.this.getFragmentManager().popBackStack();
                            return;
                        }
                        RemoteNavControlFragment navControlFragment = RemoteNavControlFragment.newInstance(GoogleAddressPickerFragment.this.chousedRoute, GoogleAddressPickerFragment.this.event.label);
                        FragmentManager fm = GoogleAddressPickerFragment.this.getFragmentManager();
                        if (fm != null) {
                            FragmentTransaction ft = fm.beginTransaction();
                            ft.replace(R.id.container, navControlFragment);
                            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                            ft.addToBackStack(null);
                            ft.commit();
                        }
                    }
                });
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.routeDiff.setText(FormatUtils.addPrefixForRouteDifference(item.diff));
            if (position == 1) {
                holder.duration.setTextColor(GoogleAddressPickerFragment.this.getResources().getColor(R.color.yellow));
            } else if (position == 2) {
                holder.duration.setTextColor(SupportMenu.CATEGORY_MASK);
            } else if (position == 0) {
                holder.duration.setTextColor(-16777216);
            }
            holder.duration.setText(FormatUtils.formatDurationFromSecondsToSecondsMinutesHours(item.duration));
            holder.length.setText(FormatUtils.formatLengthFromMetersToMiles(item.length));
            if (getCount() == 1) {
                holder.startNavigation.setVisibility(VISIBLE);
                holder.queueNavigation.setVisibility(VISIBLE);
            } else {
                holder.startNavigation.setVisibility(GONE);
                holder.queueNavigation.setVisibility(GONE);
            }
            return convertView;
        }
    }

    interface RouteListener {
        void setRoute(NavigationRouteResult navigationRouteResult);
    }

    static /* synthetic */ int access$1704(GoogleAddressPickerFragment x0) {
        int i = x0.currentSelectedRouteIndex + 1;
        x0.currentSelectedRouteIndex = i;
        return i;
    }

    static {
        try {
            Context context = NavdyApplication.getAppContext();
            ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (ai.metaData != null) {
                HERE_GEO_APP_ID = ai.metaData.getString(HERE_GEO_APP_ID_METADATA);
                HERE_GEO_APP_TOKEN = ai.metaData.getString(HERE_GEO_APP_TOKEN_METADATA);
            }
        } catch (Throwable t) {
            sLogger.e(t);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String apiKey = "";
        try {
            apiKey = NavdyApplication.getAppContext().getPackageManager().getApplicationInfo(NavdyApplication.getAppContext().getPackageName(), 128).metaData.getString("com.google.android.geo.API_KEY");
        } catch (NameNotFoundException e) {
            this.logger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e2) {
            this.logger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
        }
        this.geoApiContext = new GeoApiContext().setApiKey(apiKey);
        if (savedInstanceState != null) {
            this.routeRequestSendToHud = savedInstanceState.getBoolean("1", false);
        }
        this.mNavigationManager = new HUDNavigationManager();
    }

    @Subscribe
    public void onRoutingResponse(NavigationRouteResponse response) {
        if (this.routeRequestSendToHud) {
            this.handler.removeCallbacks(this.routeRequestTimeout);
            this.routeRequestSendToHud = false;
            handleRouteResponse(response);
            return;
        }
        sLogger.w("no current route request:" + response.label + " dest:" + response.destination);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_google_address_picker, container, false);
        ButterKnife.inject(this, rootView);
        if (getActivity().getActionBar() != null) {
            getActivity().getActionBar().hide();
        }
        Context context = inflater.getContext();
        this.mAutocompleteView.setOnItemClickListener(this.mAutocompleteClickListener);
        this.mAdapter = new PlaceAutocompleteAdapter(context, android.R.layout.simple_list_item_1, null, null);
        if (this.mGoogleApiClient != null && this.mGoogleApiClient.isConnected()) {
            this.mGoogleApiClient.disconnect();
        }
        rebuildGoogleApiClient();
        this.mAdapter.setGoogleApiClient(this.mGoogleApiClient);
        this.mAutocompleteView.setAdapter(this.mAdapter);
        MapFragment mMapFragment = MapFragment.newInstance();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.map_container, mMapFragment);
        transaction.commit();
        mMapFragment.getMapAsync(new OnMapReadyCallback() {
            public void onMapReady(GoogleMap googleMap) {
                GoogleAddressPickerFragment.this.mMap = googleMap;
                if (GoogleAddressPickerFragment.this.mSearchLocation != null) {
                    GoogleAddressPickerFragment.this.initMap();
                }
            }
        });
        this.listView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GoogleAddressPickerFragment.this.listView.setVisibility(INVISIBLE);
                GoogleAddressPickerFragment.this.showRouteDetails(i);
                GoogleAddressPickerFragment.this.resetRouteColor();
                Polyline p = GoogleAddressPickerFragment.this.routes.get(i);
                p.setColor(GoogleAddressPickerFragment.this.getResources().getColor(R.color.blue_warm));
                p.setZIndex(3.0f);
                p.setWidth(10.0f);
                GoogleAddressPickerFragment.sLogger.i("Number of routes : " + GoogleAddressPickerFragment.this.routes.size());
            }
        });
        return rootView;
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.routeRequestSendToHud) {
            outState.putBoolean("1", true);
        }
        super.onSaveInstanceState(outState);
    }

    public void onDestroy() {
        stopProgress();
        super.onDestroy();
    }

    private void resetRouteColor() {
        for (Polyline polyline : this.routes) {
            polyline.setColor(getResources().getColor(R.color.blue));
            polyline.setZIndex(0.0f);
            polyline.setWidth(7.0f);
        }
    }

    private List<Polyline> getSelectedRoute(LatLng latLng) {
        List<Polyline> result = new ArrayList();
        for (Polyline polyline : this.routes) {
            if (PolyUtil.isLocationOnPath(MapsForWorkUtils.m4bToGms(latLng), MapsForWorkUtils.m4bToGms(polyline.getPoints()), false, 100.0d)) {
                result.add(polyline);
            }
        }
        return result;
    }

    protected void showRouteDetails(int selectedRouteIndex) {
        this.chousedRoute = this.event.results.get(selectedRouteIndex);
        int i = 0;
        for (Polyline polyline : this.routes) {
            if (i != selectedRouteIndex) {
                polyline.setVisible(false);
            }
            i++;
        }
        this.listView.setVisibility(VISIBLE);
        RouteDescriptionData routeDescriptionData = this.routeDescriptionAdapter.getData().get(selectedRouteIndex);
        List<RouteDescriptionData> data = new ArrayList();
        data.add(routeDescriptionData);
        this.routeDescriptionAdapter.setData(data);
    }

    private void getGoogleNavigationCoordinate(Place place, final INavigationPositionCallback callback) {
        try {
            sLogger.v("using direction api to get nav position");
            LatLng latLng = place.getLatLng();
            DirectionsApi.newRequest(this.geoApiContext)
                    .destination(new com.google.maps.model.LatLng(latLng.latitude, latLng.longitude))
                    .origin(new com.google.maps.model.LatLng(this.lastLocation.getLatitude(), this.lastLocation.getLongitude()))
                    .setCallback(new Callback<DirectionsResult>() {
                public void onResult(DirectionsResult result) {
                    LatLng navLatLng = null;
                    DirectionsRoute[] routes = result.routes;
                    if (routes.length == 0) {
                        GoogleAddressPickerFragment.sLogger.e("no route found!");
                        callback.onSuccess(null);
                        return;
                    }
                    DirectionsRoute route = routes[0];
                    if (route.legs != null && route.legs.length > 0) {
                        DirectionsLeg leg = route.legs[route.legs.length - 1];
                        if (leg != null) {
                            GoogleAddressPickerFragment.sLogger.v("leg startAddress:" + leg.startAddress + " latlng:" + leg.startLocation);
                            GoogleAddressPickerFragment.sLogger.v("leg startAddress:" + leg.endAddress + " latlng:" + leg.endLocation);
                            if (leg.endLocation != null) {
                                navLatLng = new LatLng(leg.endLocation.lat, leg.endLocation.lng);
                            }
                        }
                    }
                    callback.onSuccess(navLatLng);
                }

                public void onFailure(Throwable e) {
                    callback.onFailure(e);
                }
            });
        } catch (Throwable t) {
            sLogger.e("getNavigationCoordinate", t);
            callback.onFailure(t);
        }
    }

    private void getHereNavigationCoordinate(final String streetAddress, final INavigationPositionCallback callback) {
        try {
            if (HERE_GEO_APP_ID == null || HERE_GEO_APP_TOKEN == null) {
                String str = "no here token in manifest";
                sLogger.v(str);
                callback.onFailure(new RuntimeException(str));
                return;
            }
            sLogger.v("using here geocoder to get nav position");
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    InputStream inputStream = null;
                    try {
                        HttpsURLConnection urlConnection = (HttpsURLConnection) new URL(GoogleAddressPickerFragment.HERE_GEOCODER_ENDPOINT + GoogleAddressPickerFragment.HERE_GEO_APP_ID + GoogleAddressPickerFragment.HERE_GEOCODER_ENDPOINT2 + GoogleAddressPickerFragment.HERE_GEO_APP_TOKEN + GoogleAddressPickerFragment.HERE_GEOCODER_ENDPOINT3 + URLEncoder.encode(streetAddress, "UTF-8")).openConnection();
                        int httpResponse = urlConnection.getResponseCode();
                        if (httpResponse < 200 || httpResponse >= 300) {
                            callback.onFailure(new RuntimeException("response code:" + httpResponse));
                        } else {
                            inputStream = urlConnection.getInputStream();
                            JSONObject navPos = ((JSONObject) new JSONObject(IOUtils.convertInputStreamToString(inputStream, "UTF-8")).getJSONObject(GoogleAddressPickerFragment.HERE_RESPONSE_STR).getJSONArray(GoogleAddressPickerFragment.HERE_VIEW_STR).get(0)).getJSONArray(GoogleAddressPickerFragment.HERE_RESULT_STR).getJSONObject(0).getJSONObject("Location").getJSONArray(GoogleAddressPickerFragment.HERE_NAVIGATION_STR).getJSONObject(0);
                            String latStr = navPos.getString(GoogleAddressPickerFragment.HERE_LATITUDE_STR);
                            String lonStr = navPos.getString(GoogleAddressPickerFragment.HERE_LONGITUDE_STR);
                            callback.onSuccess(new LatLng(Double.valueOf(latStr).doubleValue(), Double.valueOf(lonStr).doubleValue()));
                        }
                        IOUtils.closeStream(inputStream);
                    } catch (Throwable th) {
                        IOUtils.closeStream(null);
                        try {
                            throw th;
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, 3);
        } catch (Throwable t) {
            sLogger.e("getNavigationCoordinate", t);
            callback.onFailure(t);
        }
    }

    private void searchRoutesOnHud(Place place, boolean isStreetAddress, LatLng navigationCoordinate) {
        LatLng latLng;
        startProgress(getString(R.string.calculate_route));
        String label = place.getName().toString();
        String streetAddress = place.getAddress().toString();
        Coordinate display = null;
        if (navigationCoordinate != null) {
            latLng = navigationCoordinate;
            LatLng displayPos = place.getLatLng();
            display = new Coordinate(Double.valueOf(displayPos.latitude), Double.valueOf(displayPos.longitude), Float.valueOf(0.0f), Double.valueOf(0.0d), Float.valueOf(0.0f), Float.valueOf(0.0f), Long.valueOf(0), "Google");
        } else {
            latLng = MapsForWorkUtils.gmsToM4b(place.getLatLng());
        }
        Coordinate destination = new Coordinate(Double.valueOf(latLng.latitude), Double.valueOf(latLng.longitude), Float.valueOf(0.0f), Double.valueOf(0.0d), Float.valueOf(0.0f), Float.valueOf(0.0f), Long.valueOf(0), "Google");
        if (getActivity() instanceof DestinationListener) {
            ((DestinationListener) getActivity()).setDestination(label, destination);
        }
        sLogger.v("sending route to HUD:" + label + "dest:" + destination + " streetAddress:" + streetAddress);
        this.mNavigationManager.startRouteRequest(destination, label, null, streetAddress, display);
        this.handler.postDelayed(this.routeRequestTimeout, 15000);
        this.routeRequestSendToHud = true;
    }

    private void updateMap(Place place) {
        LatLng location = MapsForWorkUtils.gmsToM4b(place.getLatLng());
        this.mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 15.0f));
        this.mMap.clear();
        this.mMap.addMarker(new MarkerOptions().position(location).title((String) place.getName()));
    }

    private Polyline addPolyline(List<LatLng> points) {
        PolylineOptions opt = new PolylineOptions();
        opt.addAll(points);
        Polyline line = this.mMap.addPolyline(opt.width(6.0f).color(getResources().getColor(R.color.blue)));
        sLogger.i("ADD TO MAP:/....... " + this.mMap.toString());
        return line;
    }

    protected synchronized void rebuildGoogleApiClient() {
        this.mGoogleApiClient = new Builder(getActivity()).addConnectionCallbacks(this).addApi(Places.GEO_DATA_API).addApi(LocationServices.API).build();
        this.mGoogleApiClient.connect();
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        sLogger.e("onConnectionFailed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
        this.mAdapter.setGoogleApiClient(null);
    }

    public void onConnected(Bundle bundle) {
        this.mAdapter.setGoogleApiClient(this.mGoogleApiClient);
        sLogger.i("GoogleApiClient connected.");
        if (ContextCompat.checkSelfPermission(NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) {
            this.lastLocation = LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
        }
        if (this.lastLocation == null) {
            sLogger.e("lastLocation was null. Now requesting location update.");
            requestLocationUpdate();
            return;
        }
        setLocationBoundsForMap();
    }

    public void onConnectionSuspended(int i) {
        this.mAdapter.setGoogleApiClient(null);
        sLogger.e("GoogleApiClient connection suspended.");
    }

    public void createLocationRequest() {
        sLogger.i("creating LocationRequest Object");
        this.mLocationRequest = LocationRequest.create().setPriority(100).setInterval(10000).setFastestInterval(1000);
    }

    public void requestLocationUpdate() {
        createLocationRequest();
        sLogger.i("calling requestLocationUpdates from FusedLocationApi");
        if (ContextCompat.checkSelfPermission(NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) {
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, this);
        }
        this.requestedLocationUpdates = true;
    }

    protected void stopLocationUpdates() {
        if (this.requestedLocationUpdates) {
            LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, this);
            this.requestedLocationUpdates = false;
        }
    }

    public void onLocationChanged(Location location) {
        if (this.lastLocation == null) {
            sLogger.v("Location changed");
            this.lastLocation = location;
            setLocationBoundsForMap();
            stopLocationUpdates();
        }
    }

    public void onPause() {
        stopLocationUpdates();
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        if (this.mGoogleApiClient.isConnected()) {
            requestLocationUpdate();
        }
        BusProvider.getInstance().register(this);
    }

    public void setLocationBoundsForMap() {
        this.mSearchLocation = new LatLng(this.lastLocation.getLatitude(), this.lastLocation.getLongitude());
        sLogger.i("Setting search locus to " + this.lastLocation.getLatitude() + DDL.SEPARATOR + this.lastLocation.getLongitude());
        this.mAdapter.setBounds(searchAreaFor(this.mSearchLocation));
        if (this.mMap != null) {
            initMap();
        }
    }

    private void handleRouteResponse(NavigationRouteResponse event) {
        if (isAlive()) {
            sLogger.e(event.status + CrashlyticsAppender.SEPARATOR + event.statusDetail);
            stopProgress();
            this.event = event;
            if (event.status != RequestStatus.REQUEST_SUCCESS) {
                sLogger.e("Unable to get routes.");
                Toast.makeText(NavdyApplication.getAppContext(), getString(R.string.calculate_route_failure, event.statusDetail), 1).show();
                return;
            }
            List<NavigationRouteResult> results = event.results;
            if (results == null || results.size() == 0) {
                sLogger.e("No results.");
                Toast.makeText(NavdyApplication.getAppContext(), R.string.no_route, Toast.LENGTH_LONG).show();
                return;
            }
            List<RouteDescriptionData> data = new ArrayList<>();
            int i = 0;
            this.routes.clear();
            for (NavigationRouteResult res : results) {
                this.routes.add(addPolyline(MapUtils.getGmsLatLongsFromRouteResult(res)));
                int i2 = i + 1;
                data.add(new RouteDescriptionData(getRouteDifference(results, i).trim(), res.duration, res.length, res));
                i = i2;
            }
            this.routeDescriptionAdapter = new RouteDescriptionAdapter(data, getActivity());
            this.listView.setAdapter(this.routeDescriptionAdapter);
            this.listView.setVisibility(VISIBLE);
            this.chousedRoute = event.results.get(0);
        }
    }

    private String getRouteDifference(List<NavigationRouteResult> routeResults, int routeIndex) {
        if (routeResults == null || routeResults.size() == 0) {
            return "";
        }
        if (routeResults.size() <= 1) {
            return Arrays.asList(TextUtils.split(routeResults.get(0).label, ",")).get(0);
        }
        return findFirstDifference(routeResults.get(routeIndex).label, routeResults.get(routeIndex == routeResults.size() + -1 ? 0 : routeIndex + 1).label);
    }

    private String findFirstDifference(String label1, String label2) {
        List<String> firstLabelList = Arrays.asList(TextUtils.split(label1, ","));
        List<String> secondLabelList = Arrays.asList(TextUtils.split(label2, ","));
        int i = 0;
        for (String l : firstLabelList) {
            if (!l.equalsIgnoreCase(secondLabelList.get(i))) {
                return l;
            }
            i++;
        }
        return firstLabelList.get(0);
    }

    private LatLngBounds searchAreaFor(LatLng location) {
        return new LatLngBounds.Builder().include(SphericalUtil.computeOffset(MapsForWorkUtils.m4bToGms(location), 50000.0d, 0.0d)).include(SphericalUtil.computeOffset(MapsForWorkUtils.m4bToGms(location), 50000.0d, 90.0d)).include(SphericalUtil.computeOffset(MapsForWorkUtils.m4bToGms(location), 50000.0d, 180.0d)).include(SphericalUtil.computeOffset(MapsForWorkUtils.m4bToGms(location), 50000.0d, 270.0d)).build();
    }

    private void initMap() {
        this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(this.mSearchLocation, 15.0f));
        if (ContextCompat.checkSelfPermission(NavdyApplication.getAppContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) {
            this.mMap.setMyLocationEnabled(true);
        }
        this.mMap.setOnMapClickListener(new OnMapClickListener() {
            public void onMapClick(LatLng latLng) {
                List<Polyline> selectedPolylines = GoogleAddressPickerFragment.this.getSelectedRoute(latLng);
                if (selectedPolylines.size() > 0) {
                    GoogleAddressPickerFragment.this.resetRouteColor();
                    GoogleAddressPickerFragment.this.currentSelectedRouteIndex = GoogleAddressPickerFragment.this.currentSelectedRouteIndex < selectedPolylines.size() + -1 ? GoogleAddressPickerFragment.access$1704(GoogleAddressPickerFragment.this) : 0;
                    Polyline p = selectedPolylines.get(GoogleAddressPickerFragment.this.currentSelectedRouteIndex);
                    p.setWidth(10.0f);
                    p.setColor(GoogleAddressPickerFragment.this.getResources().getColor(R.color.blue_warm));
                }
            }
        });
    }

    private void startProgress(String str) {
        if (this.progressDialog == null) {
            this.progressDialog = new ProgressDialog(getActivity());
            this.progressDialog.setIndeterminate(true);
            this.progressDialog.setOnCancelListener(new OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    if (GoogleAddressPickerFragment.this.isAlive() && GoogleAddressPickerFragment.this.routeRequestSendToHud) {
                        GoogleAddressPickerFragment.this.routeRequestSendToHud = false;
                        GoogleAddressPickerFragment.this.handler.removeCallbacks(GoogleAddressPickerFragment.this.routeRequestTimeout);
                    }
                }
            });
        }
        this.progressDialog.setMessage(str);
        this.progressDialog.show();
    }

    private void stopProgress() {
        if (this.progressDialog != null) {
            this.progressDialog.hide();
        }
    }
}
