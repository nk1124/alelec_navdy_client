package com.navdy.client.debug.models;

import com.navdy.service.library.events.places.PlacesSearchResult;
import java.util.ArrayList;
import java.util.List;

public class RecentSearches {
    private static final int MAX_SIZE = 10;
    private ArrayList<PlacesSearchResult> results = new ArrayList();

    public List<PlacesSearchResult> getResults() {
        return this.results;
    }

    public void add(PlacesSearchResult result) {
        int position = findMatch(result);
        if (position != -1) {
            this.results.remove(position);
        }
        if (this.results.size() >= 10) {
            this.results.remove(9);
        }
        this.results.add(0, result);
    }

    public int findMatch(PlacesSearchResult result) {
        String label = result.label;
        for (int position = 0; position < this.results.size(); position++) {
            if (((PlacesSearchResult) this.results.get(position)).label.equals(label)) {
                return position;
            }
        }
        return -1;
    }
}
