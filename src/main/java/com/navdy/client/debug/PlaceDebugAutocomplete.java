package com.navdy.client.debug;

class PlaceDebugAutocomplete {
    public CharSequence description;
    public CharSequence placeId;

    PlaceDebugAutocomplete(CharSequence placeId, CharSequence description) {
        this.placeId = placeId;
        this.description = description;
    }

    public String toString() {
        return this.description.toString();
    }
}
