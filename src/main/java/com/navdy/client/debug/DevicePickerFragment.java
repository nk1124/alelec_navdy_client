package com.navdy.client.debug;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.debug.MainDebugActivity.MainFragment;
import com.navdy.client.debug.devicepicker.Device;
import com.navdy.client.debug.devicepicker.DeviceAdapter;
import com.navdy.service.library.device.NavdyDeviceId;
import com.navdy.service.library.device.RemoteDeviceRegistry;
import com.navdy.service.library.device.RemoteDeviceRegistry.DeviceListUpdatedListener;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.device.connection.TCPConnectionInfo;
import java.util.ArrayList;
import java.util.Set;

public class DevicePickerFragment extends ListFragment {
    protected AppInstance mAppInstance;
    @InjectView(R.id.device_picker_button_add_new_device)
    Button mButtonAddDevice;
    protected ConnectionInfo mCurrentDevice;
    protected DeviceListUpdatedListener mDeviceListUpdateListener;
    protected ArrayList<Device> mDevices;
    @InjectView(R.id.device_picker_edittext_add_new_device)
    EditText mEditTextNewDevice;
    protected RemoteDeviceRegistry mRemoteDeviceRegistry;
    @InjectView(R.id.device_picker_textview_current_device)
    TextView mTextViewCurrentDevice;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mRemoteDeviceRegistry = RemoteDeviceRegistry.getInstance(getActivity().getApplicationContext());
        this.mAppInstance = AppInstance.getInstance();
        this.mDevices = new ArrayList();
        addConnectionInfo(this.mRemoteDeviceRegistry.getKnownConnectionInfo());
        setListAdapter(new DeviceAdapter(getActivity(), this.mDevices));
        this.mDeviceListUpdateListener = new DeviceListUpdatedListener() {
            public void onDeviceListChanged(final Set<ConnectionInfo> deviceList) {
                DevicePickerFragment.this.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        DevicePickerFragment.this.addConnectionInfo(deviceList);
                        ((DeviceAdapter) DevicePickerFragment.this.getListAdapter()).notifyDataSetChanged();
                    }
                });
            }
        };
        this.mRemoteDeviceRegistry.addListener(this.mDeviceListUpdateListener);
    }

    protected void addConnectionInfo(ConnectionInfo connectionInfo, boolean updateIfFound) {
        Device newDevice = new Device(connectionInfo);
        int index = this.mDevices.indexOf(newDevice);
        if (index != -1) {
            ((Device) this.mDevices.get(index)).add(connectionInfo, updateIfFound);
        } else {
            this.mDevices.add(newDevice);
        }
    }

    protected void addConnectionInfo(Set<ConnectionInfo> connectionInfoSet) {
        for (ConnectionInfo connectionInfo : connectionInfoSet) {
            addConnectionInfo(connectionInfo, true);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_device_picker, container, false);
        ButterKnife.inject((Object) this, rootView);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        refreshCurrentDevice();
        this.mRemoteDeviceRegistry.startScanning();
    }

    public void onPause() {
        super.onPause();
        this.mRemoteDeviceRegistry.stopScanning();
    }

    public void onDestroy() {
        super.onDestroy();
        this.mRemoteDeviceRegistry.removeListener(this.mDeviceListUpdateListener);
    }

    public void refreshCurrentDevice() {
        this.mCurrentDevice = RemoteDeviceRegistry.getInstance(getActivity()).getDefaultConnectionInfo();
        if (this.mCurrentDevice != null) {
            this.mTextViewCurrentDevice.setText("Current device: " + Device.prettyName(this.mCurrentDevice));
            addConnectionInfo(this.mCurrentDevice, false);
        }
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        this.mRemoteDeviceRegistry.setDefaultConnectionInfo(((Device) this.mDevices.get(position)).getPreferredConnectionInfo());
        this.mAppInstance.initializeDevice();
        hideKeyboard();
        FragmentManager fragmentManager = getFragmentManager();
        Fragment splash = fragmentManager.findFragmentByTag(MainDebugActivity.SPLASH_TAG);
        fragmentManager.popBackStackImmediate();
        if (splash != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container, new MainFragment(), MainDebugActivity.MAIN_TAG);
            ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();
        }
    }

    @OnClick({R.id.device_picker_button_add_new_device})
    public void onAddClicked() {
        String newDeviceAddress = this.mEditTextNewDevice.getText().toString();
        if (!StringUtils.isEmptyAfterTrim(newDeviceAddress)) {
            this.mRemoteDeviceRegistry.addDiscoveredConnectionInfo(new TCPConnectionInfo(NavdyDeviceId.UNKNOWN_ID, newDeviceAddress));
            hideKeyboard();
            this.mEditTextNewDevice.setText("");
        }
    }

    protected void hideKeyboard() {
        ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.mEditTextNewDevice.getWindowToken(), 0);
    }

    private ActionBar getActionBar() {
        return getActivity().getActionBar();
    }
}
