package com.navdy.client.debug.navdebug;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.PositioningManager.LocationMethod;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.guidance.LaneInformation;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.guidance.NavigationManager.LaneInformationListener;
import com.here.android.mpa.guidance.NavigationManager.MapUpdateMode;
import com.here.android.mpa.guidance.NavigationManager.NavigationManagerEventListener;
import com.here.android.mpa.guidance.NavigationManager.NavigationMode;
import com.here.android.mpa.guidance.NavigationManager.NavigationState;
import com.here.android.mpa.guidance.NavigationManager.NewInstructionEventListener;
import com.here.android.mpa.guidance.NavigationManager.PositionListener;
import com.here.android.mpa.guidance.NavigationManager.SpeedWarningListener;
import com.here.android.mpa.guidance.VoiceCatalog;
import com.here.android.mpa.guidance.VoiceCatalog.OnDownloadDoneListener;
import com.here.android.mpa.guidance.VoicePackage;
import com.here.android.mpa.guidance.VoiceSkin;
import com.here.android.mpa.guidance.VoiceSkin.OutputType;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Maneuver.Turn;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.Router;
import com.here.android.mpa.routing.RoutingError;
import com.here.android.mpa.routing.CoreRouter.Listener;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RouteOptions.TransportMode;
import com.here.android.mpa.routing.RouteOptions.Type;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.debug.DebugActionsFragment;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.navigation.NavigationManeuverEvent;
import com.navdy.service.library.events.navigation.NavigationTurn;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Message;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.droidparts.contract.SQL.DDL;

import static android.view.View.VISIBLE;

public class NavigationDemoActivity extends Activity {
    private static final double[] DEFAULT_DESTINATION = new double[]{49.24831d, -122.980013d};
    private static final double[] DEFAULT_START_POINT = new double[]{49.260024d, -123.006984d};
    public static final String EXTRA_COORDS_END = "dest_coords";
    public static final String EXTRA_COORDS_START = "start_coords";
    public static final float FT_IN_METER = 3.28084f;
    public static final float METERS_IN_MI = 1609.34f;
    public static final float SMALL_UNITS_THRESHOLD = 0.1f;
    private static final String VOICE_MARC_CODE = "eng";
    public static final Logger sLogger = new Logger(NavigationDemoActivity.class);
    private TextView currentRoadTV;
    private TextView currentSpeedTV;
    private TextView distToNextManeuverTV;
    protected final SimpleDateFormat etaFormatter = new SimpleDateFormat("h:mm");
    private TextView etaTV;
    @InjectView(R.id.lane_info)
    protected TextView laneInfo;
    private LaneInformationListener laneInformationListener = new LaneInformationListener() {
        public void onLaneInformation(List<LaneInformation> list, RoadElement roadElement) {
            super.onLaneInformation(list, roadElement);
            NavigationDemoActivity.this.clearLaneInfo();
            NavigationDemoActivity.this.addLaneInfo(list);
        }
    };
    private String mCurrentRoad = "";
    private String mDistanceToPendingStreet = "";
    private GeoCoordinate mEndCoord;
    protected HashMap<Turn, NavigationTurn> mEngineTurnToNavigationTurnMap;
    private String mEtaText = "";
    private ArrayList<LaneInformation> mLaneInfoList;
    private Menu mMenu;
    private String mPendingStreet = "";
    private NavigationTurn mPendingTurn = NavigationTurn.NAV_TURN_UNKNOWN;
    private String mSpeedText = "";
    private GeoCoordinate mStartCoord;
    private ArrayList<Maneuver> maneuverDetailList;
    @InjectView(R.id.maneuver_detail_list)
    protected ListView maneuverDetailListView;
    private RelativeLayout maneuverInfo;
    private Map map = null;
    private MapFragment mapFragment = null;
    private MapRoute mapRoute = null;
    private RelativeLayout navigationInfo;
    private NavigationManager navigationManager;
    private NavigationManagerEventListener navigationManagerListener = new NavigationManagerEventListener() {
        public void onRunningStateChanged() {
            NavigationState navigationState = NavigationDemoActivity.this.navigationManager.getRunningState();
            NavigationDemoActivity.sLogger.e("Running state: " + navigationState);
            switch (AnonymousClass9.$SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState[navigationState.ordinal()]) {
                case 1:
                    NavigationDemoActivity.this.toggleButton.setText("||");
                    return;
                case 2:
                    NavigationDemoActivity.this.toggleButton.setText(">");
                    return;
                case 3:
                    NavigationDemoActivity.this.toggleButton.setText(" ");
                    return;
                default:
                    NavigationDemoActivity.sLogger.e("Unknown nav state: " + navigationState);
                    return;
            }
        }

        public void onRouteUpdated(Route route) {
            super.onRouteUpdated(route);
            NavigationDemoActivity.this.map.removeMapObject(NavigationDemoActivity.this.mapRoute);
            NavigationDemoActivity.this.mapRoute = new MapRoute(route);
            NavigationDemoActivity.this.map.addMapObject(NavigationDemoActivity.this.mapRoute);
        }

        public void onEnded(NavigationMode mode) {
            NavigationDemoActivity.this.resetUIOnNavigationEnds();
        }
    };
    private NewInstructionEventListener newInstructionEventListener = new NewInstructionEventListener() {
        public void onNewInstructionEvent() {
            super.onNewInstructionEvent();
            NavigationDemoActivity.this.updateManeuver();
        }
    };
    private TextView nextManeuverTurnTV;
    private TextView nextRoadTV;
    private PositionListener positionListener = new PositionListener() {
        public void onPositionUpdated(GeoPosition geoPosition) {
            super.onPositionUpdated(geoPosition);
            NavigationDemoActivity.this.updatePositionInfo(geoPosition);
        }
    };
    private PositioningManager positioningManager;
    private Route route = null;
    private Router.Listener CoreRouterListener = new Router.Listener<List<RouteResult>, RoutingError>() {
        @Override
        public void onCalculateRouteFinished(List<RouteResult> results,
                                             RoutingError error) {
            if (error != RoutingError.NONE || ((RouteResult) results.get(0)).getRoute() == null) {
                NavigationDemoActivity.sLogger.e("Route calculation failed:" + error.toString());
                return;
            }
            NavigationDemoActivity.this.route = ((RouteResult) results.get(0)).getRoute();
            NavigationDemoActivity.this.mapRoute = new MapRoute(NavigationDemoActivity.this.route);
            NavigationDemoActivity.this.map.addMapObject(NavigationDemoActivity.this.mapRoute);
            NavigationDemoActivity.this.map.zoomTo(NavigationDemoActivity.this.route.getBoundingBox(), Animation.NONE, -1.0f);
        }
        @Override
        public void onProgress(int percentage) {
            NavigationDemoActivity.sLogger.i("... " + percentage + "percent done ...");
        }
    };
    private TextView speedLimitTV;
    private SpeedWarningListener speedWarningListener = new SpeedWarningListener() {
        public void onSpeedExceeded(String roadName, float speedLimit) {
            super.onSpeedExceeded(roadName, speedLimit);
            NavigationDemoActivity.this.speedLimitTV.setText(String.format(Locale.getDefault(), "%d m/s", new Object[]{Integer.valueOf((int) speedLimit)}));
        }

        public void onSpeedExceededEnd(String roadName, float speedLimit) {
            super.onSpeedExceededEnd(roadName, speedLimit);
            NavigationDemoActivity.this.speedLimitTV.setText("");
        }
    };
    @InjectView(R.id.button_toggle_navigation)
    protected Button toggleButton;
    private VoiceCatalog voiceCatalog;
    private VoiceSkin voiceSkin = null;

    /* renamed from: com.navdy.client.debug.navdebug.NavigationDemoActivity$9 */
    static /* synthetic */ class AnonymousClass9 {
        static final /* synthetic */ int[] $SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState = new int[NavigationState.values().length];

        static {
            try {
                $SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState[NavigationState.RUNNING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState[NavigationState.PAUSED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$here$android$mpa$guidance$NavigationManager$NavigationState[NavigationState.IDLE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_demo);
        loadBundleArguments();
        this.mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment);
        if (this.mapFragment != null) {
            this.mapFragment.init(new OnEngineInitListener() {
                public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                    if (error == OnEngineInitListener.Error.NONE) {
                        NavigationDemoActivity.this.onMapEngineInitializationCompleted();
                        NavigationDemoActivity.this.createRoute(NavigationDemoActivity.this.mStartCoord, NavigationDemoActivity.this.mEndCoord);
                        return;
                    }
                    NavigationDemoActivity.sLogger.e("Cannot initialize map fragment:" + error.toString());
                }
            });
        }
        this.maneuverInfo = (RelativeLayout) findViewById(R.id.maneuver_info);
        this.navigationInfo = (RelativeLayout) findViewById(R.id.navigation_info);
        this.currentRoadTV = (TextView) findViewById(R.id.current_road);
        this.currentSpeedTV = (TextView) findViewById(R.id.curr_speed_nav);
        this.etaTV = (TextView) findViewById(R.id.estimated_time_arrival);
        this.nextManeuverTurnTV = (TextView) findViewById(R.id.next_maneuver_turn);
        this.nextRoadTV = (TextView) findViewById(R.id.next_road_name);
        this.distToNextManeuverTV = (TextView) findViewById(R.id.dist_to_next_maneuver);
        this.speedLimitTV = (TextView) findViewById(R.id.curr_speed_limit);
        ButterKnife.inject((Activity) this);
        this.maneuverDetailList = new ArrayList<>();
        this.maneuverDetailListView.setAdapter(new ManeuverArrayAdapter(this, this.maneuverDetailList));
        this.mLaneInfoList = new ArrayList<>();
    }

    protected void onDestroy() {
        super.onDestroy();
        sLogger.e("destroy!");
        this.navigationManager.removeNavigationManagerEventListener(this.navigationManagerListener);
    }

    public void loadBundleArguments() {
        double[] startLatLon = getIntent().getDoubleArrayExtra("start_coords");
        double[] endLatLon = getIntent().getDoubleArrayExtra("dest_coords");
        if (startLatLon == null || startLatLon.length != 2) {
            sLogger.e("Missing valid start coordinates. Falling back.");
            startLatLon = DEFAULT_START_POINT;
        }
        if (endLatLon == null || endLatLon.length != 2) {
            sLogger.e("Missing end coordinates. Falling back.");
            endLatLon = DEFAULT_DESTINATION;
        }
        this.mStartCoord = new GeoCoordinate(startLatLon[0], startLatLon[1]);
        this.mEndCoord = new GeoCoordinate(endLatLon[0], endLatLon[1]);
    }

    public static double[] latLonArrayFromGeoCoordinate(GeoCoordinate coordinate) {
        return new double[]{coordinate.getLatitude(), coordinate.getLongitude()};
    }

    public static Intent createIntentWithCoords(Context context, GeoCoordinate startCoord, GeoCoordinate endCoord) {
        if (startCoord == null || endCoord == null) {
            sLogger.e("createIntentWithCoords: bad coordinates");
            return null;
        }
        Intent i = new Intent(context, NavigationDemoActivity.class);
        i.putExtra("start_coords", latLonArrayFromGeoCoordinate(startCoord));
        i.putExtra("dest_coords", latLonArrayFromGeoCoordinate(endCoord));
        return i;
    }

    public void onResume() {
        super.onResume();
        if (this.navigationManager != null && this.navigationManager.getRunningState().equals(NavigationState.RUNNING)) {
            this.maneuverInfo.setVisibility(VISIBLE);
            this.navigationInfo.setVisibility(VISIBLE);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_navigation_demo, menu);
        this.mMenu = menu;
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_start /*2131756038*/:
                startNavigation(false);
                return true;
            case R.id.sim_start /*2131756039*/:
                startNavigation(true);
                return true;
            case R.id.navigation_stop /*2131756040*/:
                this.navigationManager.stop();
                resetUIOnNavigationEnds();
                return true;
            default:
                return false;
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (this.navigationManager != null && this.navigationManager.getRunningState().equals(NavigationState.RUNNING)) {
            updateMenuForNavigationActive(true);
        }
        return true;
    }

    private void onMapEngineInitializationCompleted() {
        if (this.mapFragment != null) {
            this.map = this.mapFragment.getMap();
        }
        this.positioningManager = PositioningManager.getInstance();
        if (!this.positioningManager.isActive()) {
            this.positioningManager.start(LocationMethod.GPS_NETWORK);
        }
        if (this.map != null) {
            this.map.getPositionIndicator().setVisible(true);
        }
        this.navigationManager = NavigationManager.getInstance();
        this.navigationManager.addNavigationManagerEventListener(new WeakReference(this.navigationManagerListener));
        this.navigationManager.addPositionListener(new WeakReference(this.positionListener));
        this.navigationManager.addLaneInformationListener(new WeakReference(this.laneInformationListener));
        this.navigationManager.addNewInstructionEventListener(new WeakReference(this.newInstructionEventListener));
        this.navigationManager.addSpeedWarningListener(new WeakReference(this.speedWarningListener));
        this.voiceCatalog = VoiceCatalog.getInstance();
        downloadTargetVoiceSkin();
    }

    private void createRoute(GeoCoordinate start, GeoCoordinate end) {
        CoreRouter CoreRouter = new CoreRouter();
        RoutePlan routePlan = new RoutePlan();
        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(TransportMode.CAR);
        routeOptions.setRouteType(Type.FASTEST);
        routePlan.setRouteOptions(routeOptions);
        routePlan.addWaypoint(new RouteWaypoint(start));
        routePlan.addWaypoint(new RouteWaypoint(end));
        CoreRouter.calculateRoute(routePlan, this.CoreRouterListener);
    }

    private void startNavigation(boolean simulated) {
        NavigationManager.Error error;
        updateMenuForNavigationActive(true);
        this.navigationManager.setMap(this.map);
        if (simulated) {
            error = this.navigationManager.simulate(this.route, (long) DebugActionsFragment.getSimulationSpeed());
        } else {
            error = this.navigationManager.startNavigation(this.route);
        }
        if (error == NavigationManager.Error.NONE) {
            this.navigationManager.setMapUpdateMode(MapUpdateMode.ROADVIEW);
            if (this.voiceSkin != null) {
                this.navigationManager.setVoiceSkin(this.voiceSkin);
            }
            this.maneuverInfo.setVisibility(VISIBLE);
            this.navigationInfo.setVisibility(VISIBLE);
            return;
        }
        sLogger.e("Start simulation failed:" + error.toString());
    }

    private void updateMenuForNavigationActive(boolean active) {
        boolean z;
        boolean z2 = true;
        MenuItem findItem = this.mMenu.findItem(R.id.navigation_start);
        if (active) {
            z = false;
        } else {
            z = true;
        }
        findItem.setEnabled(z);
        MenuItem findItem2 = this.mMenu.findItem(R.id.sim_start);
        if (active) {
            z2 = false;
        }
        findItem2.setEnabled(z2);
        this.mMenu.findItem(R.id.navigation_stop).setEnabled(active);
    }

    private void downloadTargetVoiceSkin() {
        for (VoiceSkin skin : this.voiceCatalog.getLocalVoiceSkins()) {
            if (skin.getMarcCode().compareToIgnoreCase(VOICE_MARC_CODE) == 0 && skin.getOutputType() == OutputType.TTS) {
                this.voiceSkin = skin;
                break;
            }
        }
        if (this.voiceSkin == null) {
            this.voiceCatalog.downloadCatalog(new OnDownloadDoneListener() {
                public void onDownloadDone(VoiceCatalog.Error error) {
                    if (error == VoiceCatalog.Error.NONE) {
                        for (final VoicePackage voicePackage : NavigationDemoActivity.this.voiceCatalog.getCatalogList()) {
                            if (voicePackage.getMarcCode().equalsIgnoreCase(NavigationDemoActivity.VOICE_MARC_CODE) && voicePackage.isTts()) {
                                NavigationDemoActivity.this.voiceCatalog.downloadVoice(voicePackage.getId(), new OnDownloadDoneListener() {
                                    public void onDownloadDone(VoiceCatalog.Error error) {
                                        if (error == VoiceCatalog.Error.NONE) {
                                            NavigationDemoActivity.this.voiceSkin = NavigationDemoActivity.this.voiceCatalog.getLocalVoiceSkin(voicePackage.getId());
                                        } else {
                                            NavigationDemoActivity.sLogger.e("Download voice package failed:" + error.toString());
                                        }
                                    }
                                });
                                return;
                            }
                        }
                        return;
                    }
                    NavigationDemoActivity.sLogger.e("Download voice catalog failed:" + error.toString());
                }
            });
        }
    }

    private void clearNavigationInfo() {
        this.nextRoadTV.setText("");
        this.nextManeuverTurnTV.setText("");
        this.distToNextManeuverTV.setText("");
        this.currentRoadTV.setText("");
        this.speedLimitTV.setText("");
        this.currentSpeedTV.setText("");
        this.etaTV.setText("");
        this.laneInfo.setText("");
    }

    private void resetUIOnNavigationEnds() {
        clearNavigationInfo();
        updateMenuForNavigationActive(false);
    }

    private void updatePositionInfo(GeoPosition loc) {
        int avgSpeed = (int) (loc.getSpeed() / 1609.3399658203125d);
        this.mSpeedText = String.format(Locale.getDefault(), "%d mph", new Object[]{Integer.valueOf(avgSpeed)});
        long remainingTimeMs = this.navigationManager.getEta(true, TrafficPenaltyMode.DISABLED).getTime() - new Date().getTime();
        Calendar eta = Calendar.getInstance();
        eta.add(13, (int) (remainingTimeMs / 1000));
        this.mEtaText = this.etaFormatter.format(eta.getTime());
        this.mDistanceToPendingStreet = imperialUSDistanceStringFromMeters(this.navigationManager.getNextManeuverDistance());
        this.distToNextManeuverTV.setText(this.mDistanceToPendingStreet);
        this.etaTV.setText(this.mEtaText);
        this.currentSpeedTV.setText(this.mSpeedText);
        Maneuver nextManeuver = this.navigationManager.getNextManeuver();
        if (nextManeuver != null) {
            this.mPendingStreet = nextManeuver.getNextRoadName();
            this.mCurrentRoad = nextManeuver.getRoadName();
            this.nextRoadTV.setText(this.mPendingStreet);
            this.currentRoadTV.setText(this.mCurrentRoad);
        }
        sendNavigationEvent();
    }

    @OnClick({R.id.button_toggle_navigation})
    protected void onToggleClicked() {
        if (this.navigationManager.getRunningState().equals(NavigationState.PAUSED)) {
            this.navigationManager.resume();
        } else if (this.navigationManager.getRunningState().equals(NavigationState.RUNNING)) {
            this.navigationManager.pause();
        }
    }

    private void updateManeuver() {
        Maneuver nextManeuver = this.navigationManager.getNextManeuver();
        if (nextManeuver != null) {
            this.mPendingStreet = nextManeuver.getNextRoadName();
            Turn engineNextTurn = nextManeuver.getTurn();
            if (this.mPendingTurn != null) {
                this.mPendingTurn = navigationTurnFromEngineTurn(engineNextTurn);
            }
            this.mCurrentRoad = nextManeuver.getRoadName();
            this.nextRoadTV.setText(this.mPendingStreet);
            this.nextManeuverTurnTV.setText(String.valueOf(this.mPendingTurn));
            this.currentRoadTV.setText(this.mCurrentRoad);
            this.maneuverDetailList.clear();
            this.maneuverDetailList.add(nextManeuver);
            ((ManeuverArrayAdapter) this.maneuverDetailListView.getAdapter()).notifyDataSetChanged();
            sendNavigationEvent();
            return;
        }
        sLogger.e("No next maneuver.");
    }

    public void addLaneInfo(List<LaneInformation> laneInfoList) {
        if (laneInfoList == null) {
            sLogger.e("empty lane info");
            return;
        }
        this.mLaneInfoList.clear();
        for (LaneInformation laneInfo : laneInfoList) {
            if (!this.mLaneInfoList.contains(laneInfo)) {
                this.mLaneInfoList.add(laneInfo);
            }
        }
        refreshLaneInfoDisplay();
    }

    public void clearLaneInfo() {
        this.mLaneInfoList.clear();
        refreshLaneInfoDisplay();
    }

    public void refreshLaneInfoDisplay() {
        ArrayList<String> laneTextList = new ArrayList();
        Iterator it = this.mLaneInfoList.iterator();
        while (it.hasNext()) {
            laneTextList.add("(" + TextUtils.join(DDL.SEPARATOR, ((LaneInformation) it.next()).getDirections()) + ")");
        }
        String laneText = TextUtils.join("; ", laneTextList);
        sLogger.e("Lanes: " + laneText);
        this.laneInfo.setText(laneText);
    }

    private void sendNavigationEvent() {
        Message navEvent = new NavigationManeuverEvent(this.mCurrentRoad, this.mPendingTurn, this.mDistanceToPendingStreet, this.mPendingStreet, this.mEtaText, this.mSpeedText, null, Long.valueOf(0));
        sLogger.d("Maneuver: " + navEvent.toString());
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null) {
            remoteDevice.postEvent(navEvent);
        }
    }

    public String imperialUSDistanceStringFromMeters(long meters) {
        float distanceValue;
        String distanceUnits;
        String distanceFormat;
        if (((float) meters) >= 160.934f) {
            distanceValue = ((float) meters) / 1609.34f;
            distanceUnits = "mi";
            distanceFormat = "%.1f %s";
        } else {
            distanceValue = ((float) meters) * 3.28084f;
            distanceUnits = "ft";
            distanceFormat = "%.0f %s";
        }
        return String.format(distanceFormat, new Object[]{Float.valueOf(distanceValue), distanceUnits});
    }

    private NavigationTurn navigationTurnFromEngineTurn(Turn turn) {
        if (turn == null) {
            sLogger.e("directionInfoFromTurn: passed null");
            return null;
        }
        if (this.mEngineTurnToNavigationTurnMap == null) {
            this.mEngineTurnToNavigationTurnMap = new HashMap();
            this.mEngineTurnToNavigationTurnMap.put(Turn.UNDEFINED, NavigationTurn.NAV_TURN_UNKNOWN);
            this.mEngineTurnToNavigationTurnMap.put(Turn.NO_TURN, NavigationTurn.NAV_TURN_UNKNOWN);
            this.mEngineTurnToNavigationTurnMap.put(Turn.KEEP_MIDDLE, NavigationTurn.NAV_TURN_STRAIGHT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.KEEP_RIGHT, NavigationTurn.NAV_TURN_KEEP_RIGHT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.LIGHT_RIGHT, NavigationTurn.NAV_TURN_EASY_RIGHT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.QUITE_RIGHT, NavigationTurn.NAV_TURN_RIGHT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.HEAVY_RIGHT, NavigationTurn.NAV_TURN_SHARP_RIGHT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.KEEP_LEFT, NavigationTurn.NAV_TURN_KEEP_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.LIGHT_LEFT, NavigationTurn.NAV_TURN_EASY_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.QUITE_LEFT, NavigationTurn.NAV_TURN_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.HEAVY_LEFT, NavigationTurn.NAV_TURN_SHARP_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.RETURN, NavigationTurn.NAV_TURN_UTURN_LEFT);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_1, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_2, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_3, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_4, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_5, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_6, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_7, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_8, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_9, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_10, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_11, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
            this.mEngineTurnToNavigationTurnMap.put(Turn.ROUNDABOUT_12, NavigationTurn.NAV_TURN_ROUNDABOUT_S);
        }
        NavigationTurn navigationTurn = (NavigationTurn) this.mEngineTurnToNavigationTurnMap.get(turn);
        if (navigationTurn != null) {
            return navigationTurn;
        }
        sLogger.e("navigationTurnFromEngineTurn: unknown & defaulting to straight for: " + turn);
        return NavigationTurn.NAV_TURN_STRAIGHT;
    }
}
