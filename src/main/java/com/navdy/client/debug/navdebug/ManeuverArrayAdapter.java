package com.navdy.client.debug.navdebug;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.here.android.mpa.routing.Maneuver;
import com.alelec.navdyclient.R;
import java.util.List;

public class ManeuverArrayAdapter extends ArrayAdapter<Maneuver> {
    private final Activity mActivity;
    private final List<Maneuver> mManeuvers;

    static class ViewHolder {
        ManeuverDetailPanel mManeuverDetailPanel;

        public ViewHolder(ManeuverDetailPanel maneuverDetailPanel) {
            this.mManeuverDetailPanel = maneuverDetailPanel;
        }
    }

    public ManeuverArrayAdapter(Activity activity, List<Maneuver> maneuvers) {
        super(activity, R.layout.maneuver_panel_children, maneuvers);
        this.mActivity = activity;
        this.mManeuvers = maneuvers;
    }

    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            LayoutInflater inflater = this.mActivity.getLayoutInflater();
            view = ManeuverDetailPanel.inflate(parent);
            holder = new ViewHolder((ManeuverDetailPanel) view);
            view.setTag(holder);
        }
        holder.mManeuverDetailPanel.setManeuver((Maneuver) this.mManeuvers.get(position));
        return view;
    }
}
