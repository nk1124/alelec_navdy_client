package com.navdy.client.debug.navdebug;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.OnEngineInitListener.Error;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.PositioningManager.LocationMethod;
import com.here.android.mpa.common.PositioningManager.LocationStatus;
import com.here.android.mpa.common.PositioningManager.OnPositionChangedListener;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.CoreRouter.Listener;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RouteOptions.TransportMode;
import com.here.android.mpa.routing.RouteOptions.Type;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.Router;
import com.here.android.mpa.routing.RoutingError;
import com.alelec.navdyclient.R;
import com.navdy.service.library.log.Logger;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NavRouteBrowserFragment extends ListFragment implements OnPositionChangedListener, OnEngineInitListener {
    private static final double[] DEFAULT_DESTINATION = new double[]{49.24831d, -122.980013d};
    private static final double[] DEFAULT_START_POINT = new double[]{49.260024d, -123.006984d};
    public static final String EXTRA_COORDS_END = "dest_coords";
    public static final String EXTRA_COORDS_START = "start_coords";
    public static final Logger sLogger = new Logger(NavRouteBrowserFragment.class);
    protected boolean mCalculatingRoute;
    protected GeoPosition mCurrentGeoPosition;
    private GeoCoordinate mEndCoord;
    protected boolean mEngineAvailable;
    protected boolean mEngineInitialized;
    protected ManeuverArrayAdapter mManeuverArrayAdapter;
    protected ArrayList<Maneuver> mManeuvers;
    private Route mRoute = null;
    private GeoCoordinate mStartCoord;

    public static NavRouteBrowserFragment newInstance(GeoCoordinate startCoord, GeoCoordinate endCoord) {
        NavRouteBrowserFragment fragment = new NavRouteBrowserFragment();
        Bundle args = new Bundle();
        if (startCoord == null || endCoord == null) {
            sLogger.e("createIntentWithCoords: bad coordinates");
            return null;
        }
        args.putDoubleArray("start_coords", latLonArrayFromGeoCoordinate(startCoord));
        args.putDoubleArray("dest_coords", latLonArrayFromGeoCoordinate(endCoord));
        fragment.setArguments(args);
        return fragment;
    }

    public void loadArguments() {
        double[] startLatLon = getArguments().getDoubleArray("start_coords");
        double[] endLatLon = getArguments().getDoubleArray("dest_coords");
        if (startLatLon == null || startLatLon.length != 2) {
            sLogger.e("Missing valid start coordinates. Falling back.");
            startLatLon = DEFAULT_START_POINT;
        }
        if (endLatLon == null || endLatLon.length != 2) {
            sLogger.e("Missing end coordinates. Falling back.");
            endLatLon = DEFAULT_DESTINATION;
        }
        this.mStartCoord = new GeoCoordinate(startLatLon[0], startLatLon[1]);
        this.mEndCoord = new GeoCoordinate(endLatLon[0], endLatLon[1]);
    }

    public static double[] latLonArrayFromGeoCoordinate(GeoCoordinate coordinate) {
        return new double[]{coordinate.getLatitude(), coordinate.getLongitude()};
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            loadArguments();
        }
        initEngine();
        this.mManeuvers = new ArrayList();
        setListAdapter(new ManeuverArrayAdapter(getActivity(), this.mManeuvers));
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nav_route_browser, container, false);
    }

    public void initEngine() {
        if (this.mEngineInitialized) {
            sLogger.e("Already inited.");
            return;
        }
        try {
            MapEngine.getInstance().init(new ApplicationContext(getActivity()), this);
        } catch (Exception e) {
            sLogger.e("Unable to init MapEngine");
            e.printStackTrace();
        }
    }

    public void onEngineInitializationCompleted(Error error) {
        if (error == Error.NONE) {
            sLogger.e("MapEngine initialized.");
            this.mEngineInitialized = true;
            initializePosition();
            return;
        }
        sLogger.e("MapEngine init completed with error: " + error.toString());
    }

    protected void initializePosition() {
        PositioningManager positioningManager = PositioningManager.getInstance();
        positioningManager.addListener(new WeakReference(this));
        if (!positioningManager.isActive()) {
            positioningManager.start(LocationMethod.GPS_NETWORK);
        }
    }

    public void onPositionUpdated(LocationMethod locationMethod, GeoPosition geoPosition, boolean isMapMatched) {
        sLogger.d("onPositionUpdated method: " + locationMethod + " position: " + geoPosition.getCoordinate().toString());
        this.mCurrentGeoPosition = geoPosition;
        if (!this.mEngineAvailable) {
            this.mEngineAvailable = true;
            onEngineAvailable();
        }
    }

    public void onEngineAvailable() {
        buildRoute();
    }

    public void onPositionFixChanged(LocationMethod locationMethod, LocationStatus locationStatus) {
        sLogger.d("onPositionFixChanged: method:" + locationMethod + " status: " + locationStatus);
    }

    public void buildRoute() {
        if (this.mRoute == null && !this.mCalculatingRoute && this.mEngineAvailable) {
            this.mCalculatingRoute = true;
            CoreRouter CoreRouter = new CoreRouter();
            RoutePlan routePlan = new RoutePlan();
            RouteOptions routeOptions = new RouteOptions();
            routeOptions.setTransportMode(TransportMode.CAR);
            routeOptions.setRouteType(Type.FASTEST);
            routePlan.setRouteOptions(routeOptions);
            routePlan.addWaypoint(new RouteWaypoint(this.mStartCoord));
            routePlan.addWaypoint(new RouteWaypoint(this.mEndCoord));
            CoreRouter.calculateRoute(routePlan, new Router.Listener<List<RouteResult>, RoutingError>() {
                @Override
                public void onCalculateRouteFinished(List<RouteResult> results, RoutingError error) {
                    NavRouteBrowserFragment.this.mCalculatingRoute = false;
                    if (error != RoutingError.NONE) {
                        NavRouteBrowserFragment.this.showError("Failed: " + error.toString());
                    } else if (results.size() > 0) {
                        NavRouteBrowserFragment.this.setRoute(((RouteResult) results.get(0)).getRoute());
                    } else {
                        NavRouteBrowserFragment.this.showError("No results.");
                    }
                }

                @Override
                public void onProgress(int percentage) {
                    NavRouteBrowserFragment.sLogger.i("... " + percentage + " percent done ...");
                }
            });
        }
    }

    protected void setRoute(Route route) {
        this.mRoute = route;
        if (route.getManeuvers() == null) {
            showError("No maneuvers in route.");
            return;
        }
        this.mManeuvers.clear();
        this.mManeuvers.addAll(route.getManeuvers());
        ((ManeuverArrayAdapter) getListAdapter()).notifyDataSetChanged();
    }

    protected void showError(String error) {
        sLogger.e(error);
        Toast.makeText(getActivity(), error, 0).show();
    }
}
