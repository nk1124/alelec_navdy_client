package com.navdy.client.debug;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.alelec.navdyclient.BuildConfig;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.DeviceInfo;
import com.navdy.service.library.events.obd.ObdStatusRequest;
import com.navdy.service.library.events.obd.ObdStatusResponse;
import com.squareup.otto.Subscribe;
import net.hockeyapp.android.UpdateManagerListener;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class AboutFragment extends Fragment {
    @InjectView(R.id.app_version)
    TextView appVersionLabel;
    @InjectView(R.id.display_info)
    TextView displayInfo;
    @InjectView(R.id.serial_label)
    TextView serialLabel;
    @InjectView(R.id.serial_no)
    TextView serialNumber;
    @InjectView(R.id.update_status)
    TextView updateStatusTextView;
    @InjectView(R.id.vin)
    TextView vin;
    @InjectView(R.id.vin_label)
    TextView vinLabel;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        BusProvider.getInstance().register(this);
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.inject((Object) this, rootView);
        this.appVersionLabel.setText(BuildConfig.VERSION_NAME);
        RemoteDevice device = AppInstance.getInstance().getRemoteDevice();
        int visibility = 8;
        if (device != null) {
            visibility = 0;
            DeviceInfo info = device.getDeviceInfo();
            this.serialNumber.setText(info != null ? info.deviceUuid : getResources().getString(R.string.unknown));
        }
        this.displayInfo.setVisibility(visibility);
        this.serialLabel.setVisibility(visibility);
        this.serialNumber.setVisibility(visibility);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        this.vinLabel.setVisibility(GONE);
        this.vin.setVisibility(GONE);
        DeviceConnection.postEvent(new ObdStatusRequest());
    }

    @OnClick({R.id.update_button})
    public void onCheckForUpdate(Button button) {
        this.updateStatusTextView.setVisibility(INVISIBLE);
        ((MainDebugActivity) getActivity()).checkForUpdates(new UpdateManagerListener() {
            public void onNoUpdateAvailable() {
                AboutFragment.this.updateStatusTextView.setVisibility(VISIBLE);
            }
        });
    }

    public void onDetach() {
        super.onDetach();
    }

    public void onPause() {
        BusProvider.getInstance().unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onObdStatusResponse(ObdStatusResponse response) {
        this.vinLabel.setVisibility(VISIBLE);
        this.vin.setVisibility(VISIBLE);
        this.vin.setText(response.vin != null ? response.vin : getResources().getString(R.string.unknown));
    }
}
