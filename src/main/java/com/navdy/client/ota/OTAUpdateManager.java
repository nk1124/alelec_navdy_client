package com.navdy.client.ota;

import com.navdy.client.ota.model.UpdateInfo;
import java.io.File;

public interface OTAUpdateManager {
    void abortDownload();

    void abortUpload();

    void checkForUpdate(int i, boolean z);

    void download(UpdateInfo updateInfo, File file, int i);

    void uploadToHUD(File file, long j, String str);
}
