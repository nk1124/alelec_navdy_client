package com.navdy.service.library.events.audio;

import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class VoiceSearchResponse extends Message {
    public static final Integer DEFAULT_CONFIDENCELEVEL = Integer.valueOf(0);
    public static final VoiceSearchError DEFAULT_ERROR = VoiceSearchError.NOT_AVAILABLE;
    public static final Boolean DEFAULT_LISTENINGOVERBLUETOOTH = Boolean.valueOf(false);
    public static final String DEFAULT_LOCALE = "";
    public static final String DEFAULT_PREFIX = "";
    public static final String DEFAULT_RECOGNIZEDWORDS = "";
    public static final List<Destination> DEFAULT_RESULTS = Collections.emptyList();
    public static final VoiceSearchState DEFAULT_STATE = VoiceSearchState.VOICE_SEARCH_ERROR;
    public static final Integer DEFAULT_VOLUMELEVEL = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 6, type = Datatype.INT32)
    public final Integer confidenceLevel;
    @ProtoField(tag = 5, type = Datatype.ENUM)
    public final VoiceSearchError error;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean listeningOverBluetooth;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String locale;
    @ProtoField(tag = 8, type = Datatype.STRING)
    public final String prefix;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String recognizedWords;
    @ProtoField(label = Label.REPEATED, messageType = Destination.class, tag = 7)
    public final List<Destination> results;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final VoiceSearchState state;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer volumeLevel;

    public static final class Builder extends com.squareup.wire.Message.Builder<VoiceSearchResponse> {
        public Integer confidenceLevel;
        public VoiceSearchError error;
        public Boolean listeningOverBluetooth;
        public String locale;
        public String prefix;
        public String recognizedWords;
        public List<Destination> results;
        public VoiceSearchState state;
        public Integer volumeLevel;

        public Builder() {}

        public Builder(VoiceSearchResponse message) {
            super(message);
            if (message != null) {
                this.state = message.state;
                this.recognizedWords = message.recognizedWords;
                this.volumeLevel = message.volumeLevel;
                this.listeningOverBluetooth = message.listeningOverBluetooth;
                this.error = message.error;
                this.confidenceLevel = message.confidenceLevel;
                this.results = Message.copyOf(message.results);
                this.prefix = message.prefix;
                this.locale = message.locale;
            }
        }

        public Builder state(VoiceSearchState state) {
            this.state = state;
            return this;
        }

        public Builder recognizedWords(String recognizedWords) {
            this.recognizedWords = recognizedWords;
            return this;
        }

        public Builder volumeLevel(Integer volumeLevel) {
            this.volumeLevel = volumeLevel;
            return this;
        }

        public Builder listeningOverBluetooth(Boolean listeningOverBluetooth) {
            this.listeningOverBluetooth = listeningOverBluetooth;
            return this;
        }

        public Builder error(VoiceSearchError error) {
            this.error = error;
            return this;
        }

        public Builder confidenceLevel(Integer confidenceLevel) {
            this.confidenceLevel = confidenceLevel;
            return this;
        }

        public Builder results(List<Destination> results) {
            this.results = com.squareup.wire.Message.Builder.checkForNulls(results);
            return this;
        }

        public Builder prefix(String prefix) {
            this.prefix = prefix;
            return this;
        }

        public Builder locale(String locale) {
            this.locale = locale;
            return this;
        }

        public VoiceSearchResponse build() {
            checkRequiredFields();
            return new VoiceSearchResponse(this);
        }
    }

    public enum VoiceSearchError implements ProtoEnum {
        NOT_AVAILABLE(0),
        NEED_PERMISSION(1),
        OFFLINE(2),
        FAILED_TO_START(3),
        NO_WORDS_RECOGNIZED(4),
        NO_RESULTS_FOUND(5),
        AMBIENT_NOISE(6),
        VOICE_SEARCH_ERROR_CARPLAY_BLACKLIST(7);
        
        private final int value;

        private VoiceSearchError(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum VoiceSearchState implements ProtoEnum {
        VOICE_SEARCH_ERROR(0),
        VOICE_SEARCH_STARTING(1),
        VOICE_SEARCH_LISTENING(2),
        VOICE_SEARCH_SEARCHING(3),
        VOICE_SEARCH_SUCCESS(4);
        
        private final int value;

        private VoiceSearchState(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public VoiceSearchResponse(VoiceSearchState state, String recognizedWords, Integer volumeLevel, Boolean listeningOverBluetooth, VoiceSearchError error, Integer confidenceLevel, List<Destination> results, String prefix, String locale) {
        this.state = state;
        this.recognizedWords = recognizedWords;
        this.volumeLevel = volumeLevel;
        this.listeningOverBluetooth = listeningOverBluetooth;
        this.error = error;
        this.confidenceLevel = confidenceLevel;
        this.results = Message.immutableCopyOf(results);
        this.prefix = prefix;
        this.locale = locale;
    }

    private VoiceSearchResponse(Builder builder) {
        this(builder.state, builder.recognizedWords, builder.volumeLevel, builder.listeningOverBluetooth, builder.error, builder.confidenceLevel, builder.results, builder.prefix, builder.locale);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof VoiceSearchResponse)) {
            return false;
        }
        VoiceSearchResponse o = (VoiceSearchResponse) other;
        if (equals((Object) this.state, (Object) o.state) && equals((Object) this.recognizedWords, (Object) o.recognizedWords) && equals((Object) this.volumeLevel, (Object) o.volumeLevel) && equals((Object) this.listeningOverBluetooth, (Object) o.listeningOverBluetooth) && equals((Object) this.error, (Object) o.error) && equals((Object) this.confidenceLevel, (Object) o.confidenceLevel) && equals(this.results, o.results) && equals((Object) this.prefix, (Object) o.prefix) && equals((Object) this.locale, (Object) o.locale)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.state != null ? this.state.hashCode() : 0) * 37;
        if (this.recognizedWords != null) {
            hashCode = this.recognizedWords.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.volumeLevel != null) {
            hashCode = this.volumeLevel.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.listeningOverBluetooth != null) {
            hashCode = this.listeningOverBluetooth.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.error != null) {
            hashCode = this.error.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.confidenceLevel != null) {
            hashCode = this.confidenceLevel.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (((hashCode2 + hashCode) * 37) + (this.results != null ? this.results.hashCode() : 1)) * 37;
        if (this.prefix != null) {
            hashCode = this.prefix.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.locale != null) {
            i = this.locale.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
