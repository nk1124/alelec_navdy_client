package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class SpeechRequestStatus extends Message {
    public static final String DEFAULT_ID = "";
    public static final SpeechRequestStatusType DEFAULT_STATUS = SpeechRequestStatusType.SPEECH_REQUEST_STARTING;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final SpeechRequestStatusType status;

    public static final class Builder extends com.squareup.wire.Message.Builder<SpeechRequestStatus> {
        public String id;
        public SpeechRequestStatusType status;

        public Builder() {}

        public Builder(SpeechRequestStatus message) {
            super(message);
            if (message != null) {
                this.id = message.id;
                this.status = message.status;
            }
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder status(SpeechRequestStatusType status) {
            this.status = status;
            return this;
        }

        public SpeechRequestStatus build() {
            return new SpeechRequestStatus(this);
        }
    }

    public enum SpeechRequestStatusType implements ProtoEnum {
        SPEECH_REQUEST_STARTING(1),
        SPEECH_REQUEST_STOPPED(2);
        
        private final int value;

        private SpeechRequestStatusType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public SpeechRequestStatus(String id, SpeechRequestStatusType status) {
        this.id = id;
        this.status = status;
    }

    private SpeechRequestStatus(Builder builder) {
        this(builder.id, builder.status);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof SpeechRequestStatus)) {
            return false;
        }
        SpeechRequestStatus o = (SpeechRequestStatus) other;
        if (equals((Object) this.id, (Object) o.id) && equals((Object) this.status, (Object) o.status)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.id != null) {
            result = this.id.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.status != null) {
            i = this.status.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
