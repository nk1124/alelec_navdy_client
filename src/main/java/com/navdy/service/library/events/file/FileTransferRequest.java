package com.navdy.service.library.events.file;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class FileTransferRequest extends Message {
    public static final String DEFAULT_DESTINATIONFILENAME = "";
    public static final String DEFAULT_FILEDATACHECKSUM = "";
    public static final Long DEFAULT_FILESIZE = Long.valueOf(0);
    public static final FileType DEFAULT_FILETYPE = FileType.FILE_TYPE_OTA;
    public static final Long DEFAULT_OFFSET = Long.valueOf(0);
    public static final Boolean DEFAULT_OVERRIDE = Boolean.valueOf(false);
    public static final Boolean DEFAULT_SUPPORTSACKS = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String destinationFileName;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String fileDataChecksum;
    @ProtoField(tag = 4, type = Datatype.INT64)
    public final Long fileSize;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final FileType fileType;
    @ProtoField(tag = 5, type = Datatype.INT64)
    public final Long offset;
    @ProtoField(tag = 7, type = Datatype.BOOL)
    public final Boolean override;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean supportsAcks;

    public static final class Builder extends com.squareup.wire.Message.Builder<FileTransferRequest> {
        public String destinationFileName;
        public String fileDataChecksum;
        public Long fileSize;
        public FileType fileType;
        public Long offset;
        public Boolean override;
        public Boolean supportsAcks;

        public Builder() {}

        public Builder(FileTransferRequest message) {
            super(message);
            if (message != null) {
                this.fileType = message.fileType;
                this.destinationFileName = message.destinationFileName;
                this.fileSize = message.fileSize;
                this.offset = message.offset;
                this.fileDataChecksum = message.fileDataChecksum;
                this.override = message.override;
                this.supportsAcks = message.supportsAcks;
            }
        }

        public Builder fileType(FileType fileType) {
            this.fileType = fileType;
            return this;
        }

        public Builder destinationFileName(String destinationFileName) {
            this.destinationFileName = destinationFileName;
            return this;
        }

        public Builder fileSize(Long fileSize) {
            this.fileSize = fileSize;
            return this;
        }

        public Builder offset(Long offset) {
            this.offset = offset;
            return this;
        }

        public Builder fileDataChecksum(String fileDataChecksum) {
            this.fileDataChecksum = fileDataChecksum;
            return this;
        }

        public Builder override(Boolean override) {
            this.override = override;
            return this;
        }

        public Builder supportsAcks(Boolean supportsAcks) {
            this.supportsAcks = supportsAcks;
            return this;
        }

        public FileTransferRequest build() {
            checkRequiredFields();
            return new FileTransferRequest(this);
        }
    }

    public FileTransferRequest(FileType fileType, String destinationFileName, Long fileSize, Long offset, String fileDataChecksum, Boolean override, Boolean supportsAcks) {
        this.fileType = fileType;
        this.destinationFileName = destinationFileName;
        this.fileSize = fileSize;
        this.offset = offset;
        this.fileDataChecksum = fileDataChecksum;
        this.override = override;
        this.supportsAcks = supportsAcks;
    }

    private FileTransferRequest(Builder builder) {
        this(builder.fileType, builder.destinationFileName, builder.fileSize, builder.offset, builder.fileDataChecksum, builder.override, builder.supportsAcks);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof FileTransferRequest)) {
            return false;
        }
        FileTransferRequest o = (FileTransferRequest) other;
        if (equals((Object) this.fileType, (Object) o.fileType) && equals((Object) this.destinationFileName, (Object) o.destinationFileName) && equals((Object) this.fileSize, (Object) o.fileSize) && equals((Object) this.offset, (Object) o.offset) && equals((Object) this.fileDataChecksum, (Object) o.fileDataChecksum) && equals((Object) this.override, (Object) o.override) && equals((Object) this.supportsAcks, (Object) o.supportsAcks)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.fileType != null ? this.fileType.hashCode() : 0) * 37;
        if (this.destinationFileName != null) {
            hashCode = this.destinationFileName.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.fileSize != null) {
            hashCode = this.fileSize.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.offset != null) {
            hashCode = this.offset.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.fileDataChecksum != null) {
            hashCode = this.fileDataChecksum.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.override != null) {
            hashCode = this.override.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.supportsAcks != null) {
            i = this.supportsAcks.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
