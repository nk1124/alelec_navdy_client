package com.navdy.service.library.events.contacts;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class Contact extends Message {
    public static final String DEFAULT_LABEL = "";
    public static final String DEFAULT_NAME = "";
    public static final String DEFAULT_NUMBER = "";
    public static final PhoneNumberType DEFAULT_NUMBERTYPE = PhoneNumberType.PHONE_NUMBER_HOME;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String label;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String name;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String number;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final PhoneNumberType numberType;

    public static final class Builder extends com.squareup.wire.Message.Builder<Contact> {
        public String label;
        public String name;
        public String number;
        public PhoneNumberType numberType;

        public Builder() {}

        public Builder(Contact message) {
            super(message);
            if (message != null) {
                this.name = message.name;
                this.number = message.number;
                this.numberType = message.numberType;
                this.label = message.label;
            }
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder number(String number) {
            this.number = number;
            return this;
        }

        public Builder numberType(PhoneNumberType numberType) {
            this.numberType = numberType;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Contact build() {
            return new Contact(this);
        }
    }

    public Contact(String name, String number, PhoneNumberType numberType, String label) {
        this.name = name;
        this.number = number;
        this.numberType = numberType;
        this.label = label;
    }

    private Contact(Builder builder) {
        this(builder.name, builder.number, builder.numberType, builder.label);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Contact)) {
            return false;
        }
        Contact o = (Contact) other;
        if (equals((Object) this.name, (Object) o.name) && equals((Object) this.number, (Object) o.number) && equals((Object) this.numberType, (Object) o.numberType) && equals((Object) this.label, (Object) o.label)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.name != null ? this.name.hashCode() : 0) * 37;
        if (this.number != null) {
            hashCode = this.number.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.numberType != null) {
            hashCode = this.numberType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.label != null) {
            i = this.label.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
