package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class MusicCollectionResponse extends Message {
    public static final List<MusicCharacterMap> DEFAULT_CHARACTERMAP = Collections.emptyList();
    public static final List<MusicCollectionInfo> DEFAULT_MUSICCOLLECTIONS = Collections.emptyList();
    public static final List<MusicTrackInfo> DEFAULT_MUSICTRACKS = Collections.emptyList();
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = MusicCharacterMap.class, tag = 4)
    public final List<MusicCharacterMap> characterMap;
    @ProtoField(tag = 1)
    public final MusicCollectionInfo collectionInfo;
    @ProtoField(label = Label.REPEATED, messageType = MusicCollectionInfo.class, tag = 2)
    public final List<MusicCollectionInfo> musicCollections;
    @ProtoField(label = Label.REPEATED, messageType = MusicTrackInfo.class, tag = 3)
    public final List<MusicTrackInfo> musicTracks;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicCollectionResponse> {
        public List<MusicCharacterMap> characterMap;
        public MusicCollectionInfo collectionInfo;
        public List<MusicCollectionInfo> musicCollections;
        public List<MusicTrackInfo> musicTracks;

        public Builder() {}

        public Builder(MusicCollectionResponse message) {
            super(message);
            if (message != null) {
                this.collectionInfo = message.collectionInfo;
                this.musicCollections = Message.copyOf(message.musicCollections);
                this.musicTracks = Message.copyOf(message.musicTracks);
                this.characterMap = Message.copyOf(message.characterMap);
            }
        }

        public Builder collectionInfo(MusicCollectionInfo collectionInfo) {
            this.collectionInfo = collectionInfo;
            return this;
        }

        public Builder musicCollections(List<MusicCollectionInfo> musicCollections) {
            this.musicCollections = com.squareup.wire.Message.Builder.checkForNulls(musicCollections);
            return this;
        }

        public Builder musicTracks(List<MusicTrackInfo> musicTracks) {
            this.musicTracks = com.squareup.wire.Message.Builder.checkForNulls(musicTracks);
            return this;
        }

        public Builder characterMap(List<MusicCharacterMap> characterMap) {
            this.characterMap = com.squareup.wire.Message.Builder.checkForNulls(characterMap);
            return this;
        }

        public MusicCollectionResponse build() {
            return new MusicCollectionResponse(this);
        }
    }

    public MusicCollectionResponse(MusicCollectionInfo collectionInfo, List<MusicCollectionInfo> musicCollections, List<MusicTrackInfo> musicTracks, List<MusicCharacterMap> characterMap) {
        this.collectionInfo = collectionInfo;
        this.musicCollections = Message.immutableCopyOf(musicCollections);
        this.musicTracks = Message.immutableCopyOf(musicTracks);
        this.characterMap = Message.immutableCopyOf(characterMap);
    }

    private MusicCollectionResponse(Builder builder) {
        this(builder.collectionInfo, builder.musicCollections, builder.musicTracks, builder.characterMap);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicCollectionResponse)) {
            return false;
        }
        MusicCollectionResponse o = (MusicCollectionResponse) other;
        if (equals((Object) this.collectionInfo, (Object) o.collectionInfo) && equals(this.musicCollections, o.musicCollections) && equals(this.musicTracks, o.musicTracks) && equals(this.characterMap, o.characterMap)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 1;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.collectionInfo != null ? this.collectionInfo.hashCode() : 0) * 37;
        if (this.musicCollections != null) {
            hashCode = this.musicCollections.hashCode();
        } else {
            hashCode = 1;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.musicTracks != null) {
            hashCode = this.musicTracks.hashCode();
        } else {
            hashCode = 1;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.characterMap != null) {
            i = this.characterMap.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
