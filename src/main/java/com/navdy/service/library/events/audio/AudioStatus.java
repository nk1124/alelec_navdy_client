package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class AudioStatus extends Message {
    public static final ConnectionType DEFAULT_CONNECTIONTYPE = ConnectionType.AUDIO_CONNECTION_NONE;
    public static final Boolean DEFAULT_HASSCOCONNECTION = Boolean.valueOf(false);
    public static final Boolean DEFAULT_ISCONNECTED = Boolean.valueOf(false);
    public static final Boolean DEFAULT_ISPLAYING = Boolean.valueOf(false);
    public static final ProfileType DEFAULT_PROFILETYPE = ProfileType.AUDIO_PROFILE_NONE;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final ConnectionType connectionType;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean hasSCOConnection;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean isConnected;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean isPlaying;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final ProfileType profileType;

    public static final class Builder extends com.squareup.wire.Message.Builder<AudioStatus> {
        public ConnectionType connectionType;
        public Boolean hasSCOConnection;
        public Boolean isConnected;
        public Boolean isPlaying;
        public ProfileType profileType;

        public Builder() {}

        public Builder(AudioStatus message) {
            super(message);
            if (message != null) {
                this.isConnected = message.isConnected;
                this.connectionType = message.connectionType;
                this.profileType = message.profileType;
                this.isPlaying = message.isPlaying;
                this.hasSCOConnection = message.hasSCOConnection;
            }
        }

        public Builder isConnected(Boolean isConnected) {
            this.isConnected = isConnected;
            return this;
        }

        public Builder connectionType(ConnectionType connectionType) {
            this.connectionType = connectionType;
            return this;
        }

        public Builder profileType(ProfileType profileType) {
            this.profileType = profileType;
            return this;
        }

        public Builder isPlaying(Boolean isPlaying) {
            this.isPlaying = isPlaying;
            return this;
        }

        public Builder hasSCOConnection(Boolean hasSCOConnection) {
            this.hasSCOConnection = hasSCOConnection;
            return this;
        }

        public AudioStatus build() {
            return new AudioStatus(this);
        }
    }

    public enum ConnectionType implements ProtoEnum {
        AUDIO_CONNECTION_NONE(1),
        AUDIO_CONNECTION_BLUETOOTH(2),
        AUDIO_CONNECTION_SPEAKER(3),
        AUDIO_CONNECTION_AUX(4);
        
        private final int value;

        private ConnectionType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum ProfileType implements ProtoEnum {
        AUDIO_PROFILE_NONE(1),
        AUDIO_PROFILE_A2DP(2),
        AUDIO_PROFILE_HFP(3);
        
        private final int value;

        private ProfileType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public AudioStatus(Boolean isConnected, ConnectionType connectionType, ProfileType profileType, Boolean isPlaying, Boolean hasSCOConnection) {
        this.isConnected = isConnected;
        this.connectionType = connectionType;
        this.profileType = profileType;
        this.isPlaying = isPlaying;
        this.hasSCOConnection = hasSCOConnection;
    }

    private AudioStatus(Builder builder) {
        this(builder.isConnected, builder.connectionType, builder.profileType, builder.isPlaying, builder.hasSCOConnection);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof AudioStatus)) {
            return false;
        }
        AudioStatus o = (AudioStatus) other;
        if (equals((Object) this.isConnected, (Object) o.isConnected) && equals((Object) this.connectionType, (Object) o.connectionType) && equals((Object) this.profileType, (Object) o.profileType) && equals((Object) this.isPlaying, (Object) o.isPlaying) && equals((Object) this.hasSCOConnection, (Object) o.hasSCOConnection)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.isConnected != null ? this.isConnected.hashCode() : 0) * 37;
        if (this.connectionType != null) {
            hashCode = this.connectionType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.profileType != null) {
            hashCode = this.profileType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.isPlaying != null) {
            hashCode = this.isPlaying.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.hasSCOConnection != null) {
            i = this.hasSCOConnection.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
