package com.navdy.service.library.events;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class Capabilities extends Message {
    public static final Boolean DEFAULT_CANNEDRESPONSETOSMS = Boolean.valueOf(false);
    public static final Boolean DEFAULT_COMPACTUI = Boolean.valueOf(false);
    public static final Boolean DEFAULT_CUSTOMDIALLONGPRESS = Boolean.valueOf(false);
    public static final Boolean DEFAULT_LOCALMUSICBROWSER = Boolean.valueOf(false);
    public static final Boolean DEFAULT_MUSICARTWORKCACHE = Boolean.valueOf(false);
    public static final Boolean DEFAULT_NAVCOORDSLOOKUP = Boolean.valueOf(false);
    public static final Boolean DEFAULT_PLACETYPESEARCH = Boolean.valueOf(false);
    public static final Boolean DEFAULT_SEARCHRESULTLIST = Boolean.valueOf(false);
    public static final Boolean DEFAULT_SUPPORTSPHONETICTTS = Boolean.valueOf(false);
    public static final Boolean DEFAULT_VOICESEARCH = Boolean.valueOf(false);
    public static final Boolean DEFAULT_VOICESEARCHNEWIOSPAUSEBEHAVIORS = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean cannedResponseToSms;
    @ProtoField(tag = 1, type = Datatype.BOOL)
    public final Boolean compactUi;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean customDialLongPress;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean localMusicBrowser;
    @ProtoField(tag = 7, type = Datatype.BOOL)
    public final Boolean musicArtworkCache;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean navCoordsLookup;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean placeTypeSearch;
    @ProtoField(tag = 6, type = Datatype.BOOL)
    public final Boolean searchResultList;
    @ProtoField(tag = 11, type = Datatype.BOOL)
    public final Boolean supportsPhoneticTts;
    @ProtoField(tag = 3, type = Datatype.BOOL)
    public final Boolean voiceSearch;
    @ProtoField(tag = 8, type = Datatype.BOOL)
    public final Boolean voiceSearchNewIOSPauseBehaviors;

    public static final class Builder extends com.squareup.wire.Message.Builder<Capabilities> {
        public Boolean cannedResponseToSms;
        public Boolean compactUi;
        public Boolean customDialLongPress;
        public Boolean localMusicBrowser;
        public Boolean musicArtworkCache;
        public Boolean navCoordsLookup;
        public Boolean placeTypeSearch;
        public Boolean searchResultList;
        public Boolean supportsPhoneticTts;
        public Boolean voiceSearch;
        public Boolean voiceSearchNewIOSPauseBehaviors;

        public Builder() {}

        public Builder(Capabilities message) {
            super(message);
            if (message != null) {
                this.compactUi = message.compactUi;
                this.placeTypeSearch = message.placeTypeSearch;
                this.voiceSearch = message.voiceSearch;
                this.localMusicBrowser = message.localMusicBrowser;
                this.navCoordsLookup = message.navCoordsLookup;
                this.searchResultList = message.searchResultList;
                this.musicArtworkCache = message.musicArtworkCache;
                this.voiceSearchNewIOSPauseBehaviors = message.voiceSearchNewIOSPauseBehaviors;
                this.cannedResponseToSms = message.cannedResponseToSms;
                this.customDialLongPress = message.customDialLongPress;
                this.supportsPhoneticTts = message.supportsPhoneticTts;
            }
        }

        public Builder compactUi(Boolean compactUi) {
            this.compactUi = compactUi;
            return this;
        }

        public Builder placeTypeSearch(Boolean placeTypeSearch) {
            this.placeTypeSearch = placeTypeSearch;
            return this;
        }

        public Builder voiceSearch(Boolean voiceSearch) {
            this.voiceSearch = voiceSearch;
            return this;
        }

        public Builder localMusicBrowser(Boolean localMusicBrowser) {
            this.localMusicBrowser = localMusicBrowser;
            return this;
        }

        public Builder navCoordsLookup(Boolean navCoordsLookup) {
            this.navCoordsLookup = navCoordsLookup;
            return this;
        }

        public Builder searchResultList(Boolean searchResultList) {
            this.searchResultList = searchResultList;
            return this;
        }

        public Builder musicArtworkCache(Boolean musicArtworkCache) {
            this.musicArtworkCache = musicArtworkCache;
            return this;
        }

        public Builder voiceSearchNewIOSPauseBehaviors(Boolean voiceSearchNewIOSPauseBehaviors) {
            this.voiceSearchNewIOSPauseBehaviors = voiceSearchNewIOSPauseBehaviors;
            return this;
        }

        public Builder cannedResponseToSms(Boolean cannedResponseToSms) {
            this.cannedResponseToSms = cannedResponseToSms;
            return this;
        }

        public Builder customDialLongPress(Boolean customDialLongPress) {
            this.customDialLongPress = customDialLongPress;
            return this;
        }

        public Builder supportsPhoneticTts(Boolean supportsPhoneticTts) {
            this.supportsPhoneticTts = supportsPhoneticTts;
            return this;
        }

        public Capabilities build() {
            return new Capabilities(this);
        }
    }

    public Capabilities(Boolean compactUi, Boolean placeTypeSearch, Boolean voiceSearch, Boolean localMusicBrowser, Boolean navCoordsLookup, Boolean searchResultList, Boolean musicArtworkCache, Boolean voiceSearchNewIOSPauseBehaviors, Boolean cannedResponseToSms, Boolean customDialLongPress, Boolean supportsPhoneticTts) {
        this.compactUi = compactUi;
        this.placeTypeSearch = placeTypeSearch;
        this.voiceSearch = voiceSearch;
        this.localMusicBrowser = localMusicBrowser;
        this.navCoordsLookup = navCoordsLookup;
        this.searchResultList = searchResultList;
        this.musicArtworkCache = musicArtworkCache;
        this.voiceSearchNewIOSPauseBehaviors = voiceSearchNewIOSPauseBehaviors;
        this.cannedResponseToSms = cannedResponseToSms;
        this.customDialLongPress = customDialLongPress;
        this.supportsPhoneticTts = supportsPhoneticTts;
    }

    private Capabilities(Builder builder) {
        this(builder.compactUi, builder.placeTypeSearch, builder.voiceSearch, builder.localMusicBrowser, builder.navCoordsLookup, builder.searchResultList, builder.musicArtworkCache, builder.voiceSearchNewIOSPauseBehaviors, builder.cannedResponseToSms, builder.customDialLongPress, builder.supportsPhoneticTts);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Capabilities)) {
            return false;
        }
        Capabilities o = (Capabilities) other;
        if (equals((Object) this.compactUi, (Object) o.compactUi) && equals((Object) this.placeTypeSearch, (Object) o.placeTypeSearch) && equals((Object) this.voiceSearch, (Object) o.voiceSearch) && equals((Object) this.localMusicBrowser, (Object) o.localMusicBrowser) && equals((Object) this.navCoordsLookup, (Object) o.navCoordsLookup) && equals((Object) this.searchResultList, (Object) o.searchResultList) && equals((Object) this.musicArtworkCache, (Object) o.musicArtworkCache) && equals((Object) this.voiceSearchNewIOSPauseBehaviors, (Object) o.voiceSearchNewIOSPauseBehaviors) && equals((Object) this.cannedResponseToSms, (Object) o.cannedResponseToSms) && equals((Object) this.customDialLongPress, (Object) o.customDialLongPress) && equals((Object) this.supportsPhoneticTts, (Object) o.supportsPhoneticTts)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.compactUi != null ? this.compactUi.hashCode() : 0) * 37;
        if (this.placeTypeSearch != null) {
            hashCode = this.placeTypeSearch.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.voiceSearch != null) {
            hashCode = this.voiceSearch.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.localMusicBrowser != null) {
            hashCode = this.localMusicBrowser.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.navCoordsLookup != null) {
            hashCode = this.navCoordsLookup.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.searchResultList != null) {
            hashCode = this.searchResultList.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.musicArtworkCache != null) {
            hashCode = this.musicArtworkCache.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.voiceSearchNewIOSPauseBehaviors != null) {
            hashCode = this.voiceSearchNewIOSPauseBehaviors.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.cannedResponseToSms != null) {
            hashCode = this.cannedResponseToSms.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.customDialLongPress != null) {
            hashCode = this.customDialLongPress.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.supportsPhoneticTts != null) {
            i = this.supportsPhoneticTts.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
