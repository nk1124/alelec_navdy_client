package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class MusicCharacterMap extends Message {
    public static final String DEFAULT_CHARACTER = "";
    public static final Integer DEFAULT_OFFSET = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String character;
    @ProtoField(tag = 2, type = Datatype.UINT32)
    public final Integer offset;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicCharacterMap> {
        public String character;
        public Integer offset;

        public Builder() {}

        public Builder(MusicCharacterMap message) {
            super(message);
            if (message != null) {
                this.character = message.character;
                this.offset = message.offset;
            }
        }

        public Builder character(String character) {
            this.character = character;
            return this;
        }

        public Builder offset(Integer offset) {
            this.offset = offset;
            return this;
        }

        public MusicCharacterMap build() {
            return new MusicCharacterMap(this);
        }
    }

    public MusicCharacterMap(String character, Integer offset) {
        this.character = character;
        this.offset = offset;
    }

    private MusicCharacterMap(Builder builder) {
        this(builder.character, builder.offset);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicCharacterMap)) {
            return false;
        }
        MusicCharacterMap o = (MusicCharacterMap) other;
        if (equals((Object) this.character, (Object) o.character) && equals((Object) this.offset, (Object) o.offset)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.character != null) {
            result = this.character.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.offset != null) {
            i = this.offset.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
