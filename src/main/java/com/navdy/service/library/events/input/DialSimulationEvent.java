package com.navdy.service.library.events.input;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class DialSimulationEvent extends Message {
    public static final DialInputAction DEFAULT_DIALACTION = DialInputAction.DIAL_CLICK;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final DialInputAction dialAction;

    public static final class Builder extends com.squareup.wire.Message.Builder<DialSimulationEvent> {
        public DialInputAction dialAction;

        public Builder() {}

        public Builder(DialSimulationEvent message) {
            super(message);
            if (message != null) {
                this.dialAction = message.dialAction;
            }
        }

        public Builder dialAction(DialInputAction dialAction) {
            this.dialAction = dialAction;
            return this;
        }

        public DialSimulationEvent build() {
            checkRequiredFields();
            return new DialSimulationEvent(this);
        }
    }

    public enum DialInputAction implements ProtoEnum {
        DIAL_CLICK(1),
        DIAL_LONG_CLICK(2),
        DIAL_LEFT_TURN(3),
        DIAL_RIGHT_TURN(4);
        
        private final int value;

        private DialInputAction(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DialSimulationEvent(DialInputAction dialAction) {
        this.dialAction = dialAction;
    }

    private DialSimulationEvent(Builder builder) {
        this(builder.dialAction);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof DialSimulationEvent) {
            return equals((Object) this.dialAction, (Object) ((DialSimulationEvent) other).dialAction);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.dialAction != null ? this.dialAction.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
