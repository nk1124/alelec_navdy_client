package com.navdy.service.library.events.file;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class FileTransferStatus extends Message {
    public static final Integer DEFAULT_CHUNKINDEX = Integer.valueOf(0);
    public static final FileTransferError DEFAULT_ERROR = FileTransferError.FILE_TRANSFER_NO_ERROR;
    public static final Boolean DEFAULT_SUCCESS = Boolean.valueOf(false);
    public static final Long DEFAULT_TOTALBYTESTRANSFERRED = Long.valueOf(0);
    public static final Boolean DEFAULT_TRANSFERCOMPLETE = Boolean.valueOf(false);
    public static final Integer DEFAULT_TRANSFERID = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer chunkIndex;
    @ProtoField(tag = 4, type = Datatype.ENUM)
    public final FileTransferError error;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean success;
    @ProtoField(tag = 3, type = Datatype.INT64)
    public final Long totalBytesTransferred;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.BOOL)
    public final Boolean transferComplete;
    @ProtoField(tag = 1, type = Datatype.INT32)
    public final Integer transferId;

    public static final class Builder extends com.squareup.wire.Message.Builder<FileTransferStatus> {
        public Integer chunkIndex;
        public FileTransferError error;
        public Boolean success;
        public Long totalBytesTransferred;
        public Boolean transferComplete;
        public Integer transferId;

        public Builder() {}

        public Builder(FileTransferStatus message) {
            super(message);
            if (message != null) {
                this.transferId = message.transferId;
                this.success = message.success;
                this.totalBytesTransferred = message.totalBytesTransferred;
                this.error = message.error;
                this.chunkIndex = message.chunkIndex;
                this.transferComplete = message.transferComplete;
            }
        }

        public Builder transferId(Integer transferId) {
            this.transferId = transferId;
            return this;
        }

        public Builder success(Boolean success) {
            this.success = success;
            return this;
        }

        public Builder totalBytesTransferred(Long totalBytesTransferred) {
            this.totalBytesTransferred = totalBytesTransferred;
            return this;
        }

        public Builder error(FileTransferError error) {
            this.error = error;
            return this;
        }

        public Builder chunkIndex(Integer chunkIndex) {
            this.chunkIndex = chunkIndex;
            return this;
        }

        public Builder transferComplete(Boolean transferComplete) {
            this.transferComplete = transferComplete;
            return this;
        }

        public FileTransferStatus build() {
            checkRequiredFields();
            return new FileTransferStatus(this);
        }
    }

    public FileTransferStatus(Integer transferId, Boolean success, Long totalBytesTransferred, FileTransferError error, Integer chunkIndex, Boolean transferComplete) {
        this.transferId = transferId;
        this.success = success;
        this.totalBytesTransferred = totalBytesTransferred;
        this.error = error;
        this.chunkIndex = chunkIndex;
        this.transferComplete = transferComplete;
    }

    private FileTransferStatus(Builder builder) {
        this(builder.transferId, builder.success, builder.totalBytesTransferred, builder.error, builder.chunkIndex, builder.transferComplete);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof FileTransferStatus)) {
            return false;
        }
        FileTransferStatus o = (FileTransferStatus) other;
        if (equals((Object) this.transferId, (Object) o.transferId) && equals((Object) this.success, (Object) o.success) && equals((Object) this.totalBytesTransferred, (Object) o.totalBytesTransferred) && equals((Object) this.error, (Object) o.error) && equals((Object) this.chunkIndex, (Object) o.chunkIndex) && equals((Object) this.transferComplete, (Object) o.transferComplete)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.transferId != null ? this.transferId.hashCode() : 0) * 37;
        if (this.success != null) {
            hashCode = this.success.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.totalBytesTransferred != null) {
            hashCode = this.totalBytesTransferred.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.error != null) {
            hashCode = this.error.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.chunkIndex != null) {
            hashCode = this.chunkIndex.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.transferComplete != null) {
            i = this.transferComplete.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
