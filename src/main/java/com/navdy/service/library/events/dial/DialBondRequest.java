package com.navdy.service.library.events.dial;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class DialBondRequest extends Message {
    public static final DialAction DEFAULT_ACTION = DialAction.DIAL_BOND;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final DialAction action;

    public static final class Builder extends com.squareup.wire.Message.Builder<DialBondRequest> {
        public DialAction action;

        public Builder() {}

        public Builder(DialBondRequest message) {
            super(message);
            if (message != null) {
                this.action = message.action;
            }
        }

        public Builder action(DialAction action) {
            this.action = action;
            return this;
        }

        public DialBondRequest build() {
            return new DialBondRequest(this);
        }
    }

    public enum DialAction implements ProtoEnum {
        DIAL_BOND(1),
        DIAL_CLEAR_BOND(2),
        DIAL_REBOND(3);
        
        private final int value;

        private DialAction(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DialBondRequest(DialAction action) {
        this.action = action;
    }

    private DialBondRequest(Builder builder) {
        this(builder.action);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof DialBondRequest) {
            return equals((Object) this.action, (Object) ((DialBondRequest) other).action);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.action != null ? this.action.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
