package com.navdy.client.ota;

import com.navdy.client.ota.OTAUpdateListener.DownloadUpdateStatus;
import com.navdy.client.ota.OTAUpdateListener.UploadToHUDStatus;
import com.navdy.client.ota.OTAUpdateService.State;
import com.navdy.client.ota.model.UpdateInfo;

public interface OTAUpdateUIClient {

    public enum Error {
        NO_CONNECTIVITY,
        SERVER_ERROR
    }

    boolean isInForeground();

    void onDownloadProgress(DownloadUpdateStatus downloadUpdateStatus, long j, byte b);

    void onErrorCheckingForUpdate(Error error);

    void onStateChanged(State state, UpdateInfo updateInfo);

    void onUploadProgress(UploadToHUDStatus uploadToHUDStatus, long j, byte b);
}
