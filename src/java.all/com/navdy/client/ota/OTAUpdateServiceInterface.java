package com.navdy.client.ota;

import android.os.IBinder;
import com.navdy.client.ota.OTAUpdateService.State;
import com.navdy.client.ota.model.UpdateInfo;

public interface OTAUpdateServiceInterface extends IBinder {
    void cancelDownload();

    void cancelUpload();

    boolean checkForUpdate();

    void downloadOTAUpdate();

    String getHUDBuildVersionText();

    State getOTAUpdateState();

    UpdateInfo getUpdateInfo();

    boolean isCheckingForUpdate();

    long lastKnownUploadSize();

    void registerUIClient(OTAUpdateUIClient oTAUpdateUIClient);

    void resetUpdate();

    void setNetworkDownloadApproval(boolean z);

    void toggleAutoDownload(boolean z);

    void unregisterUIClient();
}
