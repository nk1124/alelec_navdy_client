package com.navdy.client.debug;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.alelec.navdyclient.R;
import com.navdy.client.debug.util.FragmentHelper;

public class SplashFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash, container, false);
        ButterKnife.inject((Object) this, rootView);
        return rootView;
    }

    @OnClick({2131755654})
    void onConnectClick() {
        FragmentHelper.pushFullScreenFragment(getFragmentManager(), DevicePickerFragment.class);
    }
}
