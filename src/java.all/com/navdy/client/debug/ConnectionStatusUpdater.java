package com.navdy.client.debug;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectingEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectionFailedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.debug.devicepicker.Device;
import com.navdy.client.debug.view.ConnectionStateIndicator;
import com.squareup.otto.Subscribe;

class ConnectionStatusUpdater {
    @InjectView(2131755558)
    ConnectionStateIndicator mConnectionIndicator;
    @InjectView(2131755559)
    TextView mTextViewCurrentDevice;

    ConnectionStatusUpdater(View parentView) {
        ButterKnife.inject((Object) this, parentView);
    }

    public void onResume() {
        BusProvider.getInstance().register(this);
        updateView();
    }

    public void onPause() {
        BusProvider.getInstance().unregister(this);
    }

    @SuppressLint({"SetTextI18n"})
    private void updateView() {
        DeviceConnection deviceConnection = DeviceConnection.getInstance();
        if (deviceConnection != null) {
            this.mConnectionIndicator.setConnectionStatus(deviceConnection.getConnectionStatus());
            this.mConnectionIndicator.setConnectionType(deviceConnection.getConnectionType());
            this.mTextViewCurrentDevice.setText("Current device: " + Device.prettyName(deviceConnection.getConnectionInfo()));
        }
    }

    @Subscribe
    public void onDeviceConnectingEvent(DeviceConnectingEvent event) {
        updateView();
    }

    @Subscribe
    public void onDeviceConnectFailureEvent(DeviceConnectionFailedEvent event) {
        updateView();
    }

    @Subscribe
    public void onDeviceConnectedEvent(DeviceConnectedEvent event) {
        updateView();
    }

    @Subscribe
    public void onDeviceDisconnectedEvent(DeviceDisconnectedEvent event) {
        updateView();
    }
}
