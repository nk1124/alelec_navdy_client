package com.navdy.client.debug.navdebug;

import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArraySet;
import android.util.Pair;
import android.widget.TextView;
import android.widget.Toast;
import com.amazonaws.services.s3.internal.Constants;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.search.Address;
import com.here.android.mpa.search.ErrorCode;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.map.HereGeocoder;
import com.navdy.client.app.framework.map.HereGeocoder.OnGeocodeCompleteCallback;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.NavigationCoordinatesRetrievalCallback;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.Precision;
import com.navdy.client.app.framework.models.Destination.SearchType;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.search.GooglePlacesSearch;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Error;
import com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Query;
import com.navdy.client.app.framework.search.SearchResults;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.droidparts.contract.SQL.DDL;

public class NavCoordTestSuite {
    private static final int ON_SCREEN_LOG_MAX_LENGTH = 5000;
    private static NavCoordTestSuite instance;
    private final String GOOGLE_WEB_SERVICE_KEY;
    private final String OUTPUT_FILE;
    private HashMap<Step, Pair<Coordinate, Coordinate>> coords;
    private Destination currentDestination;
    private int currentQueryIndex;
    private Logger logger = new Logger(NavCoordTestSuite.class);
    private Handler mainThreadHandler;
    private OutputStreamWriter output;
    private final ArrayList<String> queries = new ArrayList();
    private boolean singleTest = false;
    private CharSequence text;
    private TextView textView;
    private Toast toast;

    enum Step {
        HERE_NATIVE_PLACES_SEARCH_WITH_NAME("white"),
        HERE_WEB_PLACES_SEARCH_WITH_NAME("brown"),
        HERE_NATIVE_PLACES_SEARCH_WITHOUT_NAME("green"),
        HERE_WEB_PLACES_SEARCH_WITHOUT_NAME("purple"),
        HERE_GEOCODER_WITH_NAME("yellow"),
        HERE_GEOCODER_WITHOUT_NAME("blue"),
        GOOGLE_DIRECTIONS("orange"),
        NAVDY_PROCESS("black");
        
        String markerColor;

        private Step(String markerColor) {
            this.markerColor = "red";
            this.markerColor = markerColor;
        }

        public String getTitle() {
            return name() + DDL.OPENING_BRACE + this.markerColor + ")";
        }

        public static Step firstStep() {
            return values()[0];
        }

        public Step nextStep() {
            if (isLastStep()) {
                return firstStep();
            }
            return values()[ordinal() + 1];
        }

        private boolean isLastStep() {
            return ordinal() + 1 >= values().length;
        }
    }

    private NavCoordTestSuite() {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), "navCoordTestSuite.txt");
            if (file.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(file));
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    this.queries.add(line);
                }
                br.close();
            } else {
                this.queries.add("SFO");
                this.queries.add("SFO Airport Terimanal 2 - Lower Level");
                this.queries.add("Palm Springs International Airport");
                this.queries.add("Fort Funston National Park");
                this.queries.add("575 7th St, SF, CA");
                this.queries.add("Kanishka Gasto pub, Walnut creek");
                this.queries.add("7030 Devon way, Berkeley");
                this.queries.add("265 Ygnacico valled rd, Walnut creek");
                this.queries.add("Apple Store, Walnut Creek");
                this.queries.add("wrangell st elias national park");
                this.queries.add("Menlo Park Caltrain station");
                this.queries.add("Yosemite");
                this.queries.add("Elektrotehnicka skola rade serbia");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.mainThreadHandler = null;
        this.textView = null;
        this.output = null;
        this.text = "";
        this.OUTPUT_FILE = "NavCoordTest.json";
        this.currentQueryIndex = 0;
        this.coords = new HashMap(Step.values().length);
        this.GOOGLE_WEB_SERVICE_KEY = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_google_web_service));
    }

    public static NavCoordTestSuite getInstance() {
        if (instance == null) {
            instance = new NavCoordTestSuite();
        }
        return instance;
    }

    public void onResume(Handler mainThreadHandler, TextView textView) {
        this.mainThreadHandler = mainThreadHandler;
        this.textView = textView;
        log("");
    }

    public void onPause() {
        this.textView = null;
    }

    public void showToast(String msg) {
        if (this.toast != null) {
            this.toast.cancel();
        }
        this.toast = Toast.makeText(NavdyApplication.getAppContext(), msg, 1);
        this.toast.show();
    }

    public void run() {
        run(null);
    }

    public void run(String query) {
        this.currentQueryIndex = 0;
        if (AppInstance.getInstance().canReachInternet()) {
            showToast("Starting test suite...");
            if (this.output == null) {
                try {
                    this.output = createOutputFileStreamWriter();
                } catch (IOException e) {
                    this.logger.e("Unable to create or open the output file (NavCoordTest.json) for nav coord test suite.", e);
                }
            }
            write("[");
            if (StringUtils.isEmptyAfterTrim(query)) {
                this.singleTest = false;
                runTest((String) this.queries.get(this.currentQueryIndex));
                return;
            }
            this.singleTest = true;
            runTest(query);
            return;
        }
        this.logger.e("Unable to test APIs when offline.");
        showToast("Unable to test APIs when offline.");
    }

    @NonNull
    private OutputStreamWriter createOutputFileStreamWriter() throws IOException {
        return new OutputStreamWriter(new FileOutputStream(Environment.getExternalStorageDirectory() + File.separator + "NavCoordTest.json", false));
    }

    private void runTest(String query) {
        write("{\n  \"search_terms\":\"" + query + "\",\n");
        log(" ");
        log(" @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        log(" @@ Testing: " + query);
        log(" ");
        log("\n===========\nGoogle Text Search\n===========");
        log("  search query:      " + query);
        new GooglePlacesSearch(new GoogleSearchListener() {
            public void onGoogleSearchResult(List<GoogleTextSearchDestinationResult> googleResults, Query queryType, Error error) {
                if (queryType == Query.TEXTSEARCH) {
                    NavCoordTestSuite.this.write("  \"google_text_search_results\":" + NavCoordTestSuite.this.googleSearchResultsToJsonArray(googleResults) + ",\n");
                    if (googleResults == null || googleResults.isEmpty()) {
                        NavCoordTestSuite.this.runNextTest();
                        return;
                    }
                    NavCoordTestSuite.this.log("  number of results: " + googleResults.size());
                    List<Destination> googleSearchDestinations = SearchResults.getDestinationListFromGoogleTextSearchResults(googleResults);
                    NavCoordTestSuite.this.write("  \"google_search_result_destinations\":" + NavCoordTestSuite.this.destinationsToJsonArray(googleSearchDestinations) + ",\n");
                    NavCoordTestSuite.this.currentDestination = null;
                    if (googleSearchDestinations != null && !googleSearchDestinations.isEmpty()) {
                        NavCoordTestSuite.this.currentDestination = (Destination) googleSearchDestinations.get(0);
                        NavCoordTestSuite.this.write("  \"selected_destination\":" + NavCoordTestSuite.this.currentDestination.toJsonString() + ",\n");
                        NavCoordTestSuite.this.log("  Selected currentDestination: name[" + NavCoordTestSuite.this.currentDestination.name + "]" + " address[" + NavCoordTestSuite.this.currentDestination.rawAddressNotForDisplay + "]");
                        NavCoordTestSuite.this.log("  Display coords:    " + NavCoordTestSuite.this.currentDestination.displayLat + DDL.SEPARATOR + NavCoordTestSuite.this.currentDestination.displayLong + "\tmap: https://www.google.com/maps?sourceid=chrome&q=" + NavCoordTestSuite.this.currentDestination.displayLat + "," + NavCoordTestSuite.this.currentDestination.displayLong);
                        GooglePlacesSearch googlePlacesSearch = new GooglePlacesSearch(this);
                        if (NavCoordTestSuite.this.currentDestination.placeId != null) {
                            NavCoordTestSuite.this.log("\n==========\nGoogle Place Details (red)\n==========");
                            NavCoordTestSuite.this.log("  placeId:         " + NavCoordTestSuite.this.currentDestination.placeId);
                            googlePlacesSearch.runDetailsSearchWebApi(NavCoordTestSuite.this.currentDestination.placeId);
                            return;
                        }
                        NavCoordTestSuite.this.doApiCalls(NavCoordTestSuite.this.currentDestination, Step.firstStep());
                    }
                } else if (queryType != Query.DETAILS) {
                } else {
                    if (NavCoordTestSuite.this.currentDestination == null) {
                        NavCoordTestSuite.this.logger.e("Received a google place details result for an unknown currentDestination. Ignoring results.");
                        return;
                    }
                    NavCoordTestSuite.this.write("  \"google_place_details_results\":" + NavCoordTestSuite.this.googleSearchResultsToJsonArray(googleResults) + ",\n");
                    if (!(googleResults == null || googleResults.isEmpty())) {
                        GoogleTextSearchDestinationResult destinationResult = (GoogleTextSearchDestinationResult) googleResults.listIterator().next();
                        Destination newDestination = new Destination();
                        destinationResult.toModelDestinationObject(SearchType.DETAILS, newDestination);
                        double distanceBetweenInOutCoords = MapUtils.distanceBetween(NavCoordTestSuite.this.currentDestination.getDisplayCoordinate(), newDestination.getNavigationCoordinate());
                        double distanceBetweenOutCoords = MapUtils.distanceBetween(newDestination.getDisplayCoordinate(), newDestination.getNavigationCoordinate());
                        double distanceThreshold = NavCoordsAddressProcessor.getDistanceThresholdFor(NavCoordTestSuite.this.currentDestination);
                        NavCoordTestSuite.this.write("  \"place_details_destination\":" + newDestination.toJsonString() + ",\n");
                        NavCoordTestSuite.this.write("  \"is_successful\":true,\n");
                        NavCoordTestSuite.this.write("  \"display_coords\":\"" + newDestination.displayLat + DDL.SEPARATOR + newDestination.displayLong + "\",\n");
                        NavCoordTestSuite.this.write("  \"nav_coords\":\"" + newDestination.navigationLat + DDL.SEPARATOR + newDestination.navigationLong + "\",\n");
                        NavCoordTestSuite.this.write("  \"distance_between_out_coords\":\"" + distanceBetweenOutCoords + "\",\n");
                        NavCoordTestSuite.this.write("  \"distance_between_in_n_out_coords\":\"" + distanceBetweenInOutCoords + "\",\n");
                        NavCoordTestSuite.this.write("  \"distance_in_n_out_threshold\":\"" + distanceThreshold + "\",\n");
                        NavCoordTestSuite.this.log("  display_coords:  " + newDestination.displayLat + DDL.SEPARATOR + newDestination.displayLong + "\tmap: https://www.google.com/maps?sourceid=chrome&q=" + newDestination.displayLat + "," + newDestination.displayLong);
                        NavCoordTestSuite.this.log("  Address:                          " + newDestination.rawAddressNotForDisplay);
                        NavCoordTestSuite.this.logCoordinate("  Display coords:                   ", newDestination.displayLat, newDestination.displayLong);
                        NavCoordTestSuite.this.logCoordinate("  Nav coords:                       ", newDestination.navigationLat, newDestination.navigationLong);
                        NavCoordTestSuite.this.log("  Distance between out coords:      " + distanceBetweenOutCoords);
                        NavCoordTestSuite.this.log("  Distance between in & out coords: " + distanceBetweenInOutCoords);
                        NavCoordTestSuite.this.log("  Distance threshold for in & out:  " + distanceThreshold);
                    }
                    NavCoordTestSuite.this.doApiCalls(NavCoordTestSuite.this.currentDestination, Step.firstStep());
                }
            }
        }, new Handler()).runTextSearchWebApi(query);
    }

    private void doApiCalls(@NonNull final Destination destination, final Step step) {
        Destination theDestination = new Destination(destination);
        if (step == Step.HERE_NATIVE_PLACES_SEARCH_WITH_NAME || step == Step.HERE_WEB_PLACES_SEARCH_WITH_NAME || step == Step.HERE_GEOCODER_WITH_NAME) {
            theDestination.rawAddressNotForDisplay = destination.name + DDL.SEPARATOR + destination.rawAddressNotForDisplay;
        }
        write("  \"" + step.name().toLowerCase() + "\":" + " {\n" + " \"currentDestination\":" + destination.toJsonString() + ",\n");
        log("\n===========\n" + step.getTitle() + "\n===========");
        final NavigationCoordinatesRetrievalCallback callback = new NavigationCoordinatesRetrievalCallback() {
            public void onSuccess(@NonNull Coordinate displayCoords, @NonNull Coordinate navCoords, @Nullable Address address, Precision precisionLevel) {
                double distanceBetweenInOutCoords = MapUtils.distanceBetween(destination.getDisplayCoordinate(), navCoords);
                double distanceBetweenOutCoords = MapUtils.distanceBetween(displayCoords, navCoords);
                double distanceThreshold = NavCoordsAddressProcessor.getDistanceThresholdFor(destination);
                NavCoordTestSuite.this.write("  \"is_successful\":true,\n");
                NavCoordTestSuite.this.write("  \"display_coords\":\"" + displayCoords.latitude + DDL.SEPARATOR + displayCoords.longitude + "\",\n");
                NavCoordTestSuite.this.write("  \"nav_coords\":\"" + navCoords.latitude + DDL.SEPARATOR + navCoords.longitude + "\",\n");
                NavCoordTestSuite.this.write("  \"distance_between_out_coords\":\"" + distanceBetweenOutCoords + "\",\n");
                NavCoordTestSuite.this.write("  \"distance_between_in_n_out_coords\":\"" + distanceBetweenInOutCoords + "\",\n");
                NavCoordTestSuite.this.write("  \"distance_in_n_out_threshold\":\"" + distanceThreshold + "\",\n");
                NavCoordTestSuite.this.coords.put(step, new Pair(displayCoords, navCoords));
                if (address != null) {
                    String addressString = address.toString().replaceAll("\n", DDL.SEPARATOR);
                    String streetNumber = address.getHouseNumber();
                    String streetName = address.getStreet();
                    String city = address.getCity();
                    String state = address.getState();
                    String zipCode = address.getPostalCode();
                    String country = address.getCountryName();
                    NavCoordTestSuite.this.write("  \"address\": {\n");
                    NavCoordTestSuite.this.write("  \"full\": \"" + addressString + "\",\n");
                    NavCoordTestSuite.this.write("  \"streetNumber\": \"" + streetNumber + "\",\n");
                    NavCoordTestSuite.this.write("  \"streetName\": \"" + streetName + "\",\n");
                    NavCoordTestSuite.this.write("  \"city\": \"" + city + "\",\n");
                    NavCoordTestSuite.this.write("  \"state\": \"" + state + "\",\n");
                    NavCoordTestSuite.this.write("  \"zipCode\": \"" + zipCode + "\",\n");
                    NavCoordTestSuite.this.write("  \"country\": \"" + country + "\"\n");
                    NavCoordTestSuite.this.write(" },\n");
                } else {
                    NavCoordTestSuite.this.write(" \"address\": {},\n");
                }
                NavCoordTestSuite.this.write(" \"precisionLevel\":\"" + precisionLevel.name() + "\"\n");
                NavCoordTestSuite.this.log("  Address:                          " + (address != null ? address.getText() : Constants.NULL_VERSION_ID));
                NavCoordTestSuite.this.logCoordinate("  Display coords:                   ", displayCoords.latitude.doubleValue(), displayCoords.longitude.doubleValue());
                NavCoordTestSuite.this.logCoordinate("  Nav coords:                       ", navCoords.latitude.doubleValue(), navCoords.longitude.doubleValue());
                NavCoordTestSuite.this.log("  Distance between out coords:      " + distanceBetweenOutCoords);
                NavCoordTestSuite.this.log("  Distance between in & out coords: " + distanceBetweenInOutCoords);
                NavCoordTestSuite.this.log("  Distance threshold for in & out:  " + distanceThreshold);
                goToNextStep();
            }

            public void onFailure(String error) {
                NavCoordTestSuite.this.write(" \"is_successful\":false,\n");
                NavCoordTestSuite.this.write(" \"throwable\":\"" + error + "\"\n");
                NavCoordTestSuite.this.log(" ### FAILURE ###");
                NavCoordTestSuite.this.log(" ### " + error);
                goToNextStep();
            }

            private void goToNextStep() {
                NavCoordTestSuite.this.logGoogleStaticMapForThisStep(step);
                if (step.isLastStep()) {
                    NavCoordTestSuite.this.write("}\n");
                    NavCoordTestSuite.this.logGoogleStaticMapForAllSteps();
                    NavCoordTestSuite.this.runNextTest();
                    return;
                }
                NavCoordTestSuite.this.write("},\n");
                NavCoordTestSuite.this.doApiCalls(destination, step.nextStep());
            }
        };
        OnGeocodeCompleteCallback hereCallback = new OnGeocodeCompleteCallback() {
            public void onComplete(@NonNull GeoCoordinate displayCoords, @Nullable GeoCoordinate navigationCoords, @Nullable Address address, Precision precision) {
                callback.onSuccess(new Coordinate(Double.valueOf(displayCoords.getLatitude()), Double.valueOf(displayCoords.getLongitude()), Float.valueOf(0.0f), Double.valueOf(0.0d), Float.valueOf(0.0f), Float.valueOf(0.0f), Long.valueOf(0), "HERE"), new Coordinate(Double.valueOf(navigationCoords != null ? navigationCoords.getLatitude() : 0.0d), Double.valueOf(navigationCoords != null ? navigationCoords.getLongitude() : 0.0d), Float.valueOf(0.0f), Double.valueOf(0.0d), Float.valueOf(0.0f), Float.valueOf(0.0f), Long.valueOf(0), "HERE"), address, precision);
            }

            public void onError(@NonNull ErrorCode error) {
                callback.onFailure(error.name());
            }
        };
        switch (step) {
            case HERE_NATIVE_PLACES_SEARCH_WITH_NAME:
            case HERE_NATIVE_PLACES_SEARCH_WITHOUT_NAME:
                log("Address: " + theDestination.rawAddressNotForDisplay);
                HereGeocoder.makeRequest(theDestination.rawAddressNotForDisplay, hereCallback);
                return;
            case HERE_WEB_PLACES_SEARCH_WITH_NAME:
            case HERE_WEB_PLACES_SEARCH_WITHOUT_NAME:
                log("Address: " + theDestination.rawAddressNotForDisplay);
                NavCoordsAddressProcessor.callWebHerePlacesSearchApi(theDestination.rawAddressNotForDisplay, hereCallback);
                return;
            case HERE_GEOCODER_WITH_NAME:
            case HERE_GEOCODER_WITHOUT_NAME:
                log("Address: " + theDestination.rawAddressNotForDisplay);
                NavCoordsAddressProcessor.callWebHereGeocoderApi(theDestination.rawAddressNotForDisplay, hereCallback);
                return;
            case GOOGLE_DIRECTIONS:
                log("PlaceId: " + theDestination.placeId + " Address: " + theDestination.rawAddressNotForDisplay);
                NavCoordsAddressProcessor.getCoordinatesFromGoogleDirectionsWebApi(theDestination, callback);
                return;
            default:
                log("PlaceId: " + theDestination.placeId + " Address: " + theDestination.rawAddressNotForDisplay);
                NavCoordsAddressProcessor.processDestination(theDestination, new OnCompleteCallback() {
                    public void onSuccess(Destination destination) {
                        callback.onSuccess(destination.getDisplayCoordinate(), destination.getNavigationCoordinate(), destination.getHereAddress(), destination.precisionLevel);
                    }

                    public void onFailure(Destination destination, NavCoordsAddressProcessor.Error error) {
                        callback.onFailure(error.name());
                    }
                });
                return;
        }
    }

    private void logGoogleStaticMapForThisStep(Step step) {
        Set<Step> keys = new ArraySet();
        keys.add(step);
        logGoogleStaticMapForAllSteps(keys);
    }

    private void logGoogleStaticMapForAllSteps() {
        logGoogleStaticMapForAllSteps(this.coords.keySet());
    }

    private void logGoogleStaticMapForAllSteps(Set<Step> keys) {
        log("Map Legend:");
        log("red: \t GOOGLE_PLACE_DETAILS");
        String staticMapUrl = addCoordMarker("https://maps.googleapis.com/maps/api/staticmap?size=1024x768&maptype=roadmap", "red", this.currentDestination.displayLat, this.currentDestination.displayLong, this.currentDestination.navigationLat, this.currentDestination.navigationLong);
        for (Step key : keys) {
            log(key.markerColor + ": \t " + key.name());
            Pair<Coordinate, Coordinate> coordinatePair = (Pair) this.coords.get(key);
            Coordinate disp = coordinatePair.first != null ? (Coordinate) coordinatePair.first : new Coordinate(Double.valueOf(0.0d), Double.valueOf(0.0d), Float.valueOf(0.0f), Double.valueOf(0.0d), Float.valueOf(0.0f), Float.valueOf(0.0f), Long.valueOf(0), "");
            Coordinate nav = coordinatePair.second != null ? (Coordinate) coordinatePair.second : new Coordinate(Double.valueOf(0.0d), Double.valueOf(0.0d), Float.valueOf(0.0f), Double.valueOf(0.0d), Float.valueOf(0.0f), Float.valueOf(0.0f), Long.valueOf(0), "");
            staticMapUrl = addCoordMarker(staticMapUrl, key.markerColor, disp.latitude.doubleValue(), disp.longitude.doubleValue(), nav.latitude.doubleValue(), nav.longitude.doubleValue());
        }
        log("Static map: " + (staticMapUrl + "&key=" + this.GOOGLE_WEB_SERVICE_KEY));
    }

    @NonNull
    private String addCoordMarker(String staticMapUrl, String color, double dispLatitude, double dispLongitude, double navLatitude, double navLongitude) {
        if (dispLatitude == navLatitude && dispLongitude == navLongitude) {
            return addMarker(staticMapUrl, color, "B", dispLatitude, dispLongitude);
        }
        String displayLabel = "D";
        String navLabel = "N";
        if (!MapUtils.isValidSetOfCoordinates(navLatitude, navLongitude)) {
            displayLabel = "C";
        }
        if (!MapUtils.isValidSetOfCoordinates(dispLatitude, dispLongitude)) {
            navLabel = "M";
        }
        return addMarker(addMarker(staticMapUrl, color, displayLabel, dispLatitude, dispLongitude), color, navLabel, navLatitude, navLongitude);
    }

    @NonNull
    private String addMarker(String staticMapUrl, String color, String label, double latitude, double longitude) {
        return !MapUtils.isValidSetOfCoordinates(latitude, longitude) ? staticMapUrl : staticMapUrl + "&markers=color:" + color + "%7Clabel:" + label + "%7C" + latitude + "," + longitude;
    }

    private void logCoordinate(String name, double lat, double lon) {
        String encodedName;
        try {
            encodedName = "(" + URLEncoder.encode(name.replaceAll("[: ]", ""), "UTF-8") + ")";
        } catch (UnsupportedEncodingException e) {
            encodedName = "";
        }
        String googleMapsUrl = "https://www.google.com/maps?sourceid=chrome&q=" + lat + "," + lon + encodedName;
        StringBuilder append = new StringBuilder().append(name).append(lat).append(DDL.SEPARATOR).append(lon);
        String str = (lat == 0.0d && lon == 0.0d) ? "" : "\tmap: " + googleMapsUrl;
        log(append.append(str).toString());
    }

    private void runNextTest() {
        write("}");
        this.currentQueryIndex++;
        if (this.singleTest || this.currentQueryIndex >= this.queries.size()) {
            write("]");
            try {
                this.output.close();
                this.output = null;
            } catch (IOException e) {
                this.logger.e("Unable to close the output file (NavCoordTest.json) for nav coord test suite.", e);
            }
            logEndOfTest();
            showToast("...Test suite complete");
            return;
        }
        write(",\n");
        runTest((String) this.queries.get(this.currentQueryIndex));
    }

    private void logEndOfTest() {
        log(" ");
        log(" @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        log(" @@@@@@     Test suite finished.     @@@@@@");
        log(" @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    }

    private String googleSearchResultsToJsonArray(List<GoogleTextSearchDestinationResult> googleResults) {
        StringBuilder jsonString = new StringBuilder("[ ");
        if (googleResults == null || googleResults.isEmpty()) {
            return "[]";
        }
        for (int i = 0; i < googleResults.size(); i++) {
            jsonString.append(((GoogleTextSearchDestinationResult) googleResults.get(i)).jsonString);
            if (i + 1 < googleResults.size()) {
                jsonString.append(",\n    ");
            }
        }
        jsonString.append(" ]");
        return jsonString.toString();
    }

    private String destinationsToJsonArray(List<Destination> googleSearchDestinations) {
        StringBuilder jsonString = new StringBuilder("[ ");
        if (googleSearchDestinations == null || googleSearchDestinations.isEmpty()) {
            return "[]";
        }
        for (int i = 0; i < googleSearchDestinations.size(); i++) {
            jsonString.append(((Destination) googleSearchDestinations.get(i)).toJsonString());
            if (i + 1 < googleSearchDestinations.size()) {
                jsonString.append(",\n    ");
            }
        }
        jsonString.append(" ]");
        return jsonString.toString();
    }

    private void write(String str) {
        try {
            if (this.output != null) {
                this.output.write(str);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void log(@NonNull final String message) {
        this.logger.d(message);
        this.mainThreadHandler.post(new Runnable() {
            public void run() {
                if (NavCoordTestSuite.this.textView != null) {
                    if (NavCoordTestSuite.this.text.length() + message.length() > 5000) {
                        NavCoordTestSuite.this.text.subSequence(500, NavCoordTestSuite.this.text.length());
                    }
                    NavCoordTestSuite.this.text = NavCoordTestSuite.this.text + "\n" + message;
                    NavCoordTestSuite.this.textView.setText(NavCoordTestSuite.this.text);
                }
            }
        });
    }
}
