package com.navdy.client.debug.navdebug;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnTextChanged;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.OnEngineInitListener.Error;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.PositioningManager.LocationMethod;
import com.here.android.mpa.common.PositioningManager.LocationStatus;
import com.here.android.mpa.common.PositioningManager.OnPositionChangedListener;
import com.here.android.mpa.search.Address;
import com.here.android.mpa.search.DiscoveryResultPage;
import com.here.android.mpa.search.ErrorCode;
import com.here.android.mpa.search.GeocodeRequest;
import com.here.android.mpa.search.Location;
import com.here.android.mpa.search.NavigationPosition;
import com.here.android.mpa.search.PlaceLink;
import com.here.android.mpa.search.ResultListener;
import com.here.android.mpa.search.SearchRequest;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.map.HereMapsManager;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.log.Logger;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import org.droidparts.widget.ClearableEditText;
import org.droidparts.widget.ClearableEditText.Listener;

public class NavAddressPickerFragment extends ListFragment implements OnEngineInitListener, OnPositionChangedListener {
    private static final double MI_TO_METERS = 1690.34d;
    private static final int SEARCH_RADIUS_METERS = 169034;
    public static final Logger sLogger = new Logger(NavAddressPickerFragment.class);
    protected AppInstance mAppInstance;
    protected GeoPosition mCurrentGeoPosition;
    protected ArrayList<GeoCoordinate> mDestinationCoordinates;
    protected ArrayList<String> mDestinationLabels;
    @InjectView(2131755628)
    ClearableEditText mEditTextDestinationQuery;
    protected GeocodeRequest mLastGeocodeRequest;
    protected SearchRequest mLastSearchRequest;
    protected Handler mMainHandler;
    private boolean mMapEngineAvailable = false;
    @InjectView(2131755625)
    RadioGroup mSearchTypeRadioGroup;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapEngine.getInstance().init(getActivity(), this);
        MapEngine.getInstance().init(getActivity().getApplicationContext(), this);
        this.mAppInstance = AppInstance.getInstance();
        this.mDestinationLabels = new ArrayList();
        this.mDestinationCoordinates = new ArrayList();
        setListAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, this.mDestinationLabels));
        this.mMainHandler = new Handler();
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.local_nav_actions, menu);
    }

    public void onEngineInitializationCompleted(Error error) {
        if (error == Error.NONE) {
            sLogger.e("MapEngine initialized.");
            initializePosition();
            return;
        }
        sLogger.e("MapEngine init completed with error: " + error.toString());
    }

    public void initializePosition() {
        PositioningManager positioningManager = PositioningManager.getInstance();
        positioningManager.addListener(new WeakReference(this));
        if (!positioningManager.isActive()) {
            positioningManager.start(LocationMethod.GPS_NETWORK);
        }
    }

    public void onPositionUpdated(LocationMethod locationMethod, GeoPosition geoPosition, boolean b) {
        sLogger.d("onPositionUpdated method: " + locationMethod + " position: " + geoPosition.getCoordinate().toString());
        this.mCurrentGeoPosition = geoPosition;
        this.mMapEngineAvailable = true;
    }

    public void onPositionFixChanged(LocationMethod locationMethod, LocationStatus locationStatus) {
        sLogger.d("onPositionFixChanged: method:" + locationMethod + " status: " + locationStatus);
    }

    protected void addDestinations(List<Location> destinationLocations) {
        this.mDestinationLabels.clear();
        this.mDestinationCoordinates.clear();
        for (Location destinationLocation : destinationLocations) {
            this.mDestinationLabels.add(getLabelForLocation(destinationLocation));
            this.mDestinationCoordinates.add(getCoordinateForLocation(destinationLocation));
        }
        ((ArrayAdapter) getListAdapter()).notifyDataSetChanged();
    }

    protected void addPlaces(List<PlaceLink> placeLocations) {
        this.mDestinationLabels.clear();
        this.mDestinationCoordinates.clear();
        for (PlaceLink placeLink : placeLocations) {
            this.mDestinationLabels.add(placeLink.getTitle());
            this.mDestinationCoordinates.add(placeLink.getPosition());
        }
        ((ArrayAdapter) getListAdapter()).notifyDataSetChanged();
    }

    protected String getLabelForLocation(Location location) {
        String label;
        if (location.getAddress() == null) {
            label = location.toString();
        } else {
            Address address = location.getAddress();
            String text = address.getText();
            if (!StringUtils.isEmptyAfterTrim(text)) {
                return text;
            }
            String number = address.getHouseNumber();
            String street = address.getStreet();
            String city = address.getCity();
            String state = address.getState();
            String streetAddress = String.format("%s %s", new Object[]{emptyIfNull(number), emptyIfNull(street)}).trim();
            String cityState = String.format("%s %s", new Object[]{emptyIfNull(city), emptyIfNull(state)}).trim();
            if (!StringUtils.isEmptyAfterTrim(streetAddress) && !StringUtils.isEmptyAfterTrim(cityState)) {
                label = String.format("%s, %s", new Object[]{streetAddress, cityState});
            } else if (!StringUtils.isEmptyAfterTrim(streetAddress)) {
                label = streetAddress;
            } else if (StringUtils.isEmptyAfterTrim(cityState)) {
                label = "(unknown address)";
            } else {
                label = cityState;
            }
        }
        return label;
    }

    protected String emptyIfNull(String s) {
        return s == null ? "" : s;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nav_address_picker_demo, container, false);
        ButterKnife.inject((Object) this, rootView);
        this.mEditTextDestinationQuery.setListener(new Listener() {
            public void didClearText() {
                NavAddressPickerFragment.this.showKeyboard();
            }
        });
        return rootView;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerForContextMenu(getListView());
        this.mSearchTypeRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                NavAddressPickerFragment.this.onQueryTypeCheckedChanged(radioGroup, i);
            }
        });
    }

    public void onResume() {
        super.onResume();
        refreshUI();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void refreshUI() {
        if (this.mDestinationLabels.size() == 0) {
            showKeyboard();
        }
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        l.showContextMenuForChild(v);
    }

    public boolean onContextItemSelected(MenuItem item) {
        GeoCoordinate destinationCoord = (GeoCoordinate) this.mDestinationCoordinates.get(((AdapterContextMenuInfo) item.getMenuInfo()).position);
        if (destinationCoord == null) {
            showError("Unable to find coordinate.");
            return true;
        }
        hideKeyboard();
        switch (item.getItemId()) {
            case R.id.local_nav_route_info /*2131756058*/:
                startRouteBrowser(this.mCurrentGeoPosition.getCoordinate(), destinationCoord);
                return true;
            case R.id.local_nav_navigate /*2131756059*/:
                startNavigation(this.mCurrentGeoPosition.getCoordinate(), destinationCoord);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    protected GeoCoordinate getCoordinateForLocation(Location destinationLocation) {
        GeoCoordinate destinationCoord = null;
        NavigationPosition navigationPosition = null;
        if (!(destinationLocation == null || destinationLocation.getAccessPoints() == null || destinationLocation.getAccessPoints().size() <= 0)) {
            navigationPosition = (NavigationPosition) destinationLocation.getAccessPoints().get(0);
        }
        if (navigationPosition != null) {
            destinationCoord = navigationPosition.getCoordinate();
        }
        if (destinationCoord == null) {
            destinationCoord = destinationLocation.getCoordinate();
        }
        if (destinationCoord != null) {
            sLogger.d("text: " + destinationLocation.getAddress().getText() + " pos: " + destinationCoord.toString());
        } else {
            sLogger.d("couldn't build coordinate");
        }
        return destinationCoord;
    }

    public void startNavigation(GeoCoordinate start, GeoCoordinate end) {
        startActivity(NavigationDemoActivity.createIntentWithCoords(getActivity(), start, end));
    }

    public void startRouteBrowser(GeoCoordinate start, GeoCoordinate end) {
        NavRouteBrowserFragment routeBrowserFragment = NavRouteBrowserFragment.newInstance(start, end);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.container, routeBrowserFragment);
        ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }

    @OnTextChanged({2131755628})
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        doSearch(s.toString());
    }

    public void onQueryTypeCheckedChanged(RadioGroup group, int checkedid) {
        doSearch(this.mEditTextDestinationQuery.getText().toString());
    }

    protected void doSearch(String queryText) {
        if (!StringUtils.isEmptyAfterTrim(queryText)) {
            if (!this.mMapEngineAvailable) {
                showError("Map engine not ready.");
            } else if (this.mCurrentGeoPosition == null) {
                showError("No current position.");
            } else if (this.mSearchTypeRadioGroup.getCheckedRadioButtonId() == R.id.radioaddresses) {
                doAddressSearch(queryText);
            } else {
                doPlaceSearch(queryText);
            }
        }
    }

    protected void doPlaceSearch(final String queryText) {
        HereMapsManager.getInstance().addOnInitializedListener(new OnEngineInitListener() {
            public void onEngineInitializationCompleted(Error error) {
                if (NavAddressPickerFragment.this.mLastSearchRequest != null) {
                    NavAddressPickerFragment.this.mLastSearchRequest.cancel();
                }
                NavAddressPickerFragment.this.mLastSearchRequest = new SearchRequest(queryText).setSearchCenter(NavAddressPickerFragment.this.mCurrentGeoPosition.getCoordinate());
                if (NavAddressPickerFragment.this.mLastSearchRequest == null) {
                    NavAddressPickerFragment.this.showError("Unable to search.");
                    return;
                }
                ErrorCode errorCode = NavAddressPickerFragment.this.mLastSearchRequest.execute(new ResultListener<DiscoveryResultPage>() {
                    public void onCompleted(final DiscoveryResultPage data, final ErrorCode errorCode) {
                        NavAddressPickerFragment.this.mMainHandler.post(new Runnable() {
                            public void run() {
                                if (errorCode != ErrorCode.NONE) {
                                    NavAddressPickerFragment.this.showError("Search error: " + errorCode.toString());
                                    return;
                                }
                                List<PlaceLink> places = data.getPlaceLinks();
                                if (places == null || places.size() <= 0) {
                                    String str = "No results";
                                    NavAddressPickerFragment.this.showError(str);
                                    NavAddressPickerFragment.sLogger.d(str);
                                    return;
                                }
                                NavAddressPickerFragment.sLogger.d("returned results: " + places.size());
                                NavAddressPickerFragment.this.addPlaces(places);
                            }
                        });
                    }
                });
                if (errorCode != ErrorCode.NONE) {
                    NavAddressPickerFragment.this.showError("Search error: " + errorCode.toString());
                }
            }
        });
    }

    protected void doAddressSearch(final String queryText) {
        HereMapsManager.getInstance().addOnInitializedListener(new OnEngineInitListener() {
            public void onEngineInitializationCompleted(Error error) {
                if (NavAddressPickerFragment.this.mLastGeocodeRequest != null) {
                    NavAddressPickerFragment.this.mLastGeocodeRequest.cancel();
                }
                NavAddressPickerFragment.this.mLastGeocodeRequest = new GeocodeRequest(queryText).setSearchArea(NavAddressPickerFragment.this.mCurrentGeoPosition.getCoordinate(), NavAddressPickerFragment.SEARCH_RADIUS_METERS);
                if (NavAddressPickerFragment.this.mLastGeocodeRequest == null) {
                    NavAddressPickerFragment.this.showError("Unable to search.");
                    return;
                }
                ErrorCode errorCode = NavAddressPickerFragment.this.mLastGeocodeRequest.execute(new ResultListener<List<Location>>() {
                    public void onCompleted(final List<Location> locations, final ErrorCode errorCode) {
                        NavAddressPickerFragment.this.mMainHandler.post(new Runnable() {
                            public void run() {
                                if (errorCode != ErrorCode.NONE) {
                                    NavAddressPickerFragment.this.showError("Search error: " + errorCode.toString());
                                } else if (locations == null || locations.size() <= 0) {
                                    String str = "No results";
                                    NavAddressPickerFragment.this.showError(str);
                                    NavAddressPickerFragment.sLogger.d(str);
                                } else {
                                    NavAddressPickerFragment.sLogger.d("returned results: " + locations.size());
                                    NavAddressPickerFragment.this.addDestinations(locations);
                                }
                            }
                        });
                    }
                });
                if (errorCode != ErrorCode.NONE) {
                    NavAddressPickerFragment.this.showError("Search error: " + errorCode.toString());
                }
            }
        });
    }

    protected void hideKeyboard() {
        ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.mEditTextDestinationQuery.getWindowToken(), 0);
    }

    protected void showKeyboard() {
        ((InputMethodManager) getActivity().getSystemService("input_method")).showSoftInput(this.mEditTextDestinationQuery, 1);
    }

    protected void showError(String error) {
        Toast.makeText(getActivity(), error, 0).show();
    }
}
