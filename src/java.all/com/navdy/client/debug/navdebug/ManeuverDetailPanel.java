package com.navdy.client.debug.navdebug;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.here.android.mpa.common.RoadElement;
import com.here.android.mpa.routing.Maneuver;
import com.here.android.mpa.routing.Signpost.LocalizedLabel;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import java.util.ArrayList;

public class ManeuverDetailPanel extends LinearLayout {
    public static final String NO_DATA_PLACEHOLDER = "(none)";
    @InjectView(2131755775)
    TextView mActionView;
    @InjectView(2131755778)
    TextView mAngleView;
    @InjectView(2131755786)
    TextView mCurStreetNameView;
    @InjectView(2131755785)
    TextView mCurStreetNumView;
    @InjectView(2131755782)
    TextView mDistFromPrevView;
    @InjectView(2131755781)
    TextView mDistFromStartView;
    @InjectView(2131755783)
    TextView mDistToNextView;
    @InjectView(2131755776)
    TextView mIconView;
    @InjectView(2131755784)
    TextView mInstructionView;
    protected Maneuver mManeuver;
    protected boolean mMergedChildren;
    @InjectView(2131755789)
    ImageView mNextStreetImageView;
    @InjectView(2131755788)
    TextView mNextStreetNameView;
    @InjectView(2131755787)
    TextView mNextStreetNumView;
    @InjectView(2131755794)
    TextView mRoadElementsView;
    @InjectView(2131755792)
    ImageView mSignpostIconView;
    @InjectView(2131755793)
    TextView mSignpostLabelDirectionView;
    @InjectView(2131755790)
    TextView mSignpostNumberView;
    @InjectView(2131755791)
    TextView mSignpostTextView;
    @InjectView(2131755795)
    TextView mStartTimeView;
    @InjectView(2131755779)
    TextView mTrafficDirectionView;
    @InjectView(2131755780)
    TextView mTransportModeView;
    @InjectView(2131755777)
    TextView mTurnView;

    public static ManeuverDetailPanel inflate(ViewGroup parent) {
        return (ManeuverDetailPanel) LayoutInflater.from(parent.getContext()).inflate(R.layout.maneuver_panel_root, parent, false);
    }

    public ManeuverDetailPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        mergeChildren(context);
    }

    public ManeuverDetailPanel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mergeChildren(context);
    }

    public Maneuver getManeuver() {
        return this.mManeuver;
    }

    protected void mergeChildren(Context context) {
        if (!this.mMergedChildren) {
            LayoutInflater.from(context).inflate(R.layout.maneuver_panel_children, this, true);
            this.mMergedChildren = true;
            ButterKnife.inject((Object) this, (View) this);
            updateViews();
        }
    }

    public void setManeuver(Maneuver maneuver) {
        this.mManeuver = maneuver;
        if (this.mMergedChildren) {
            updateViews();
        }
    }

    protected void updateViews() {
        String action = NO_DATA_PLACEHOLDER;
        String icon = NO_DATA_PLACEHOLDER;
        String turn = NO_DATA_PLACEHOLDER;
        String angle = NO_DATA_PLACEHOLDER;
        String trafficDirection = NO_DATA_PLACEHOLDER;
        String transportMode = NO_DATA_PLACEHOLDER;
        String distFromStart = NO_DATA_PLACEHOLDER;
        String distFromPrev = NO_DATA_PLACEHOLDER;
        String distToNext = NO_DATA_PLACEHOLDER;
        String instruction = NO_DATA_PLACEHOLDER;
        String curStreetNum = NO_DATA_PLACEHOLDER;
        String curStreetName = NO_DATA_PLACEHOLDER;
        String nextStreetNum = NO_DATA_PLACEHOLDER;
        String nextStreetName = NO_DATA_PLACEHOLDER;
        Bitmap nextStreetBitmap = null;
        String signpostNumber = NO_DATA_PLACEHOLDER;
        String signpostText = NO_DATA_PLACEHOLDER;
        int signpostFg = -16777216;
        int signpostBg = -1;
        Bitmap signpostIconBitmap = null;
        String signpostLabelDirection = NO_DATA_PLACEHOLDER;
        String roadElementsText = NO_DATA_PLACEHOLDER;
        String startTime = NO_DATA_PLACEHOLDER;
        if (this.mManeuver != null) {
            action = defaultIfNull(this.mManeuver.getAction());
            icon = defaultIfNull(this.mManeuver.getIcon());
            turn = defaultIfNull(this.mManeuver.getTurn());
            angle = "" + this.mManeuver.getAngle();
            trafficDirection = defaultIfNull(this.mManeuver.getTrafficDirection());
            transportMode = defaultIfNull(this.mManeuver.getTransportMode());
            distFromStart = "" + this.mManeuver.getDistanceFromStart();
            distFromPrev = "" + this.mManeuver.getDistanceFromPreviousManeuver();
            distToNext = "" + this.mManeuver.getDistanceToNextManeuver();
            curStreetNum = defaultIfEmpty(this.mManeuver.getRoadNumber());
            curStreetName = defaultIfEmpty(this.mManeuver.getRoadName());
            nextStreetNum = defaultIfEmpty(this.mManeuver.getNextRoadNumber());
            nextStreetName = defaultIfEmpty(this.mManeuver.getNextRoadName());
            if (this.mManeuver.getNextRoadImage() != null && this.mManeuver.getNextRoadImage().isValid()) {
                nextStreetBitmap = this.mManeuver.getNextRoadImage().getBitmap();
            }
            if (this.mManeuver.getSignpost() != null) {
                signpostNumber = defaultIfEmpty(this.mManeuver.getSignpost().getExitNumber());
                signpostText = defaultIfEmpty(this.mManeuver.getSignpost().getExitText());
                signpostFg = this.mManeuver.getSignpost().getForegroundColor();
                signpostBg = this.mManeuver.getSignpost().getBackgroundColor();
                if (this.mManeuver.getSignpost().getExitIcon() != null && this.mManeuver.getSignpost().getExitIcon().isValid()) {
                    signpostIconBitmap = this.mManeuver.getNextRoadImage().getBitmap();
                }
                signpostLabelDirection = NO_DATA_PLACEHOLDER;
                ArrayList<String> directionLabelList = new ArrayList();
                for (LocalizedLabel label : this.mManeuver.getSignpost().getExitDirections()) {
                    directionLabelList.add(String.format("dir: %s name: %s text: %s", new Object[]{defaultIfEmpty(label.getRouteDirection()), defaultIfEmpty(label.getRouteName()), defaultIfEmpty(label.getText())}));
                }
                signpostLabelDirection = TextUtils.join("\n", directionLabelList);
            }
            ArrayList<String> roadElements = new ArrayList();
            for (RoadElement roadElement : this.mManeuver.getRoadElements()) {
                String str = "roadName: %s routeName: %s%nattr: %s%navgSpeed: %f formOfWay: %s lanes: %s%nspeedLmt: %f.1 plural: %s ped: %s%nstartTime: %s";
                Object[] objArr = new Object[10];
                objArr[0] = defaultIfEmpty(roadElement.getRoadName());
                objArr[1] = defaultIfEmpty(roadElement.getRouteName());
                objArr[2] = TextUtils.join(" ", roadElement.getAttributes());
                objArr[3] = Float.valueOf(roadElement.getDefaultSpeed());
                objArr[4] = defaultIfNull(roadElement.getFormOfWay());
                objArr[5] = Integer.valueOf(roadElement.getNumberOfLanes());
                objArr[6] = Float.valueOf(roadElement.getSpeedLimit());
                objArr[7] = roadElement.isPlural() ? defaultIfNull(roadElement.getPluralType()) : "no";
                objArr[8] = Boolean.valueOf(roadElement.isPedestrian());
                objArr[9] = defaultIfNull(roadElement.getStartTime());
                roadElements.add(String.format(str, objArr));
            }
            roadElementsText = TextUtils.join("\n\n", roadElements);
            startTime = defaultIfNull(this.mManeuver.getStartTime());
        }
        this.mActionView.setText(action);
        this.mIconView.setText(icon);
        this.mTurnView.setText(turn);
        this.mAngleView.setText(angle);
        this.mTrafficDirectionView.setText(trafficDirection);
        this.mTransportModeView.setText(transportMode);
        this.mDistFromStartView.setText(distFromStart);
        this.mDistFromPrevView.setText(distFromPrev);
        this.mDistToNextView.setText(distToNext);
        this.mInstructionView.setText(instruction);
        this.mCurStreetNumView.setText(curStreetNum);
        this.mCurStreetNameView.setText(curStreetName);
        this.mNextStreetNumView.setText(nextStreetNum);
        this.mNextStreetNameView.setText(nextStreetName);
        if (nextStreetBitmap != null) {
            this.mNextStreetImageView.setVisibility(VISIBLE);
            this.mNextStreetImageView.setImageBitmap(nextStreetBitmap);
        } else {
            this.mNextStreetImageView.setVisibility(GONE);
        }
        this.mSignpostNumberView.setText(signpostNumber);
        this.mSignpostTextView.setText(signpostText);
        this.mSignpostNumberView.setTextColor(signpostFg);
        this.mSignpostTextView.setTextColor(signpostFg);
        this.mSignpostNumberView.setBackgroundColor(signpostBg);
        this.mSignpostTextView.setBackgroundColor(signpostBg);
        if (signpostIconBitmap != null) {
            this.mSignpostIconView.setVisibility(VISIBLE);
            this.mSignpostIconView.setImageBitmap(signpostIconBitmap);
        } else {
            this.mSignpostIconView.setVisibility(INVISIBLE);
        }
        this.mSignpostLabelDirectionView.setText(signpostLabelDirection);
        this.mRoadElementsView.setText(roadElementsText);
        this.mStartTimeView.setText(startTime);
    }

    protected String defaultIfNull(Object o) {
        if (o == null) {
            return NO_DATA_PLACEHOLDER;
        }
        return o.toString();
    }

    protected String defaultIfEmpty(String s) {
        if (StringUtils.isEmptyAfterTrim(s)) {
            return NO_DATA_PLACEHOLDER;
        }
        return s;
    }
}
