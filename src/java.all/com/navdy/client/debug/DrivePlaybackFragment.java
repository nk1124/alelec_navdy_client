package com.navdy.client.debug;

import android.app.AlertDialog.Builder;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import butterknife.ButterKnife;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.servicehandler.RecordingServiceManager;
import com.navdy.client.app.framework.servicehandler.RecordingServiceManager.Listener;
import com.navdy.service.library.log.Logger;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class DrivePlaybackFragment extends ListFragment {
    private static final Logger sLogger = new Logger(CustomNotificationFragment.class);
    private List<String> mPlaybackList;
    private ArrayAdapter<String> mPlaybackListAdapter;
    private Listener mRecordingListener = new Listener() {
        public void onDriveRecordingsResponse(List<String> recordings) {
            DrivePlaybackFragment.this.mPlaybackList.addAll(recordings);
            DrivePlaybackFragment.this.mPlaybackListAdapter.notifyDataSetChanged();
        }
    };
    private final RecordingServiceManager mRecordingServiceManager = RecordingServiceManager.getInstance();

    public DrivePlaybackFragment() {
        this.mRecordingServiceManager.addListener(new WeakReference(this.mRecordingListener));
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        this.mPlaybackList = new ArrayList();
        this.mPlaybackListAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_activated_1, android.R.id.text1, this.mPlaybackList);
        setListAdapter(this.mPlaybackListAdapter);
        this.mRecordingServiceManager.sendDriveRecordingsRequest();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_test_screen, container, false);
        ButterKnife.inject((Object) this, rootView);
        return rootView;
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        final String clickedItem = (String) this.mPlaybackList.get(position);
        Builder alertDialogBuilder = new Builder(getActivity());
        alertDialogBuilder.setTitle(clickedItem).setPositiveButton("Play", new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                DrivePlaybackFragment.this.mRecordingServiceManager.sendStartDrivePlaybackEvent(clickedItem);
            }
        }).setNegativeButton("Cancel", new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertDialogBuilder.create().show();
    }
}
