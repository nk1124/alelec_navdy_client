package com.navdy.client.debug.view;

import android.app.Fragment;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.mobileconnectors.s3.transfermanager.Download;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.client.debug.util.FormatUtils;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

public abstract class S3BrowserFragment extends Fragment {
    private static final int REPORT_ON_DOWNLOAD_STEP = 1000;
    private static final int S3_DOWNLOAD_TIMEOUT = 300000;
    private static final Logger sLogger = new Logger(S3BrowserFragment.class);
    private ObjectAdapter mAdapter;
    protected AppInstance mAppInstance;
    @InjectView(2131755396)
    Button mBackButton;
    protected AmazonS3Client mClient;
    protected Context mContext;
    @InjectView(2131755645)
    ListView mList;
    private String mPrefix = "";
    @InjectView(2131755646)
    Button mRefreshButton;

    private class ItemClickListener implements OnItemClickListener {
        private ItemClickListener() {
        }

//        /* synthetic */ ItemClickListener(S3BrowserFragment x0, AnonymousClass1 x1) {
//            this();
//        }

        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
            MyS3Object item = (MyS3Object) S3BrowserFragment.this.mAdapter.getItem(pos);
            if (item.isFolder()) {
                S3BrowserFragment.this.mPrefix = item.getKey();
                new RefreshTask(S3BrowserFragment.this, null).execute(new Void[0]);
                return;
            }
            S3BrowserFragment.this.onFileSelected(item.getKey());
        }
    }

    private static class MyS3Object {
        private final boolean isFolder;
        private final String key;
        private final long size;

        MyS3Object(String commonPrefix) {
            this.key = commonPrefix;
            this.isFolder = true;
            this.size = 0;
        }

        MyS3Object(S3ObjectSummary summary) {
            this.key = summary.getKey();
            this.size = summary.getSize();
            this.isFolder = false;
        }

        public String getKey() {
            return this.key;
        }

        public boolean isFolder() {
            return this.isFolder;
        }

        public long getSize() {
            return this.size;
        }
    }

    private class ObjectAdapter extends ArrayAdapter<MyS3Object> {

        private class ViewHolder {
            private final TextView key;
            private final TextView size;

//            /* synthetic */ ViewHolder(ObjectAdapter x0, View x1, AnonymousClass1 x2) {
//                this(x1);
//            }

            private ViewHolder(View view) {
                this.key = (TextView) view.findViewById(R.id.key);
                this.size = (TextView) view.findViewById(R.id.size);
            }
        }

        public ObjectAdapter(Context context) {
            super(context, R.layout.list_item_bucket);
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_bucket, null);
                holder = new ViewHolder(this, convertView, null);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            MyS3Object object = (MyS3Object) getItem(pos);
            holder.key.setText(object.getKey());
            if (object.isFolder()) {
                holder.size.setText("");
            } else {
                holder.size.setText(FormatUtils.readableFileSize(object.getSize()));
            }
            return convertView;
        }

        public void add(MyS3Object object) {
            if (!object.getKey().equals(S3BrowserFragment.this.mPrefix)) {
                super.add(object);
            }
        }

        public void addAll(Collection<? extends MyS3Object> collection) {
            for (MyS3Object obj : collection) {
                if (!obj.getKey().equals(S3BrowserFragment.this.mPrefix)) {
                    add(obj);
                }
            }
        }
    }

    private class RefreshTask extends AsyncTask<Void, Void, ObjectListing> {
        private RefreshTask() {
        }

//        /* synthetic */ RefreshTask(S3BrowserFragment x0, AnonymousClass1 x1) {
//            this();
//        }

        protected void onPreExecute() {
            S3BrowserFragment.this.mRefreshButton.setEnabled(false);
            S3BrowserFragment.this.mRefreshButton.setText(R.string.refreshing);
        }

        protected ObjectListing doInBackground(Void... params) {
            try {
                return S3BrowserFragment.this.mClient.listObjects(new ListObjectsRequest(S3BrowserFragment.this.getS3BucketName(), S3BrowserFragment.this.mPrefix, null, S3Constants.S3_FILE_DELIMITER, Integer.valueOf(1000)));
            } catch (AmazonClientException e) {
                S3BrowserFragment.sLogger.e("Exception while listing the files in the S3 Bucket :" + S3BrowserFragment.this.getS3BucketName() + ", Prefix :" + S3BrowserFragment.this.mPrefix, e);
                return null;
            }
        }

        protected void onPostExecute(ObjectListing objects) {
            if (objects == null || objects.getCommonPrefixes() == null || objects.getObjectSummaries() == null) {
                S3BrowserFragment.sLogger.e("onPostExecute objects are null !!!");
                return;
            }
            S3BrowserFragment.this.mAdapter.clear();
            for (String commonPrefix : objects.getCommonPrefixes()) {
                S3BrowserFragment.this.mAdapter.add(new MyS3Object(commonPrefix));
            }
            for (S3ObjectSummary summary : objects.getObjectSummaries()) {
                S3BrowserFragment.this.mAdapter.add(new MyS3Object(summary));
            }
            S3BrowserFragment.this.mRefreshButton.setEnabled(true);
            S3BrowserFragment.this.mRefreshButton.setText(R.string.refresh);
        }
    }

    protected abstract String getS3BucketName();

    protected abstract void onFileSelected(String str);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity().getApplicationContext();
        String AWS_ACCOUNT_ID = "";
        String AWS_SECRET = "";
        try {
            ApplicationInfo ai = this.mContext.getPackageManager().getApplicationInfo(NavdyApplication.getAppContext().getPackageName(), 128);
            AWS_ACCOUNT_ID = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_aws_account_id));
            AWS_SECRET = CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_aws_secret));
        } catch (NameNotFoundException e) {
            sLogger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e2) {
            sLogger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
        }
        this.mClient = new AmazonS3Client(new BasicAWSCredentials(AWS_ACCOUNT_ID, AWS_SECRET));
        this.mAppInstance = AppInstance.getInstance();
    }

    @OnClick({2131755396})
    public void onBack(View button) {
        back();
    }

    @OnClick({2131755646})
    public void onRefresh(View v) {
        new RefreshTask(this, null).execute(new Void[0]);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_s3_browser, container, false);
        ButterKnife.inject((Object) this, rootView);
        this.mAdapter = new ObjectAdapter(getActivity());
        this.mList.setOnItemClickListener(new ItemClickListener(this, null));
        this.mList.setAdapter(this.mAdapter);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        new RefreshTask(this, null).execute(new Void[0]);
    }

    private void back() {
        if (this.mPrefix.endsWith(S3Constants.S3_FILE_DELIMITER)) {
            this.mPrefix = this.mPrefix.substring(0, this.mPrefix.length() - 1);
        }
        int parentDir = this.mPrefix.lastIndexOf(47, this.mPrefix.length() - 1);
        if (parentDir != -1) {
            this.mPrefix = this.mPrefix.substring(0, parentDir + 1);
        } else {
            this.mPrefix = "";
        }
        new RefreshTask(this, null).execute(new Void[0]);
    }

    protected void storeS3ObjectInTemp(String s3Key, File output, ProgressListener downloadProgressListener) throws IOException {
        final TransferManager transferManager = new TransferManager(this.mClient);
        this.mBackButton.setEnabled(false);
        this.mRefreshButton.setEnabled(false);
        this.mRefreshButton.setText(R.string.downloading);
        this.mBackButton.invalidate();
        this.mRefreshButton.invalidate();
        final String str = s3Key;
        final File file = output;
        final ProgressListener progressListener = downloadProgressListener;
        new Thread(new Runnable() {
            public void run() {
                final Download download = transferManager.download(S3BrowserFragment.this.getS3BucketName(), str, file);
                final long startTime = SystemClock.elapsedRealtime();
                download.addProgressListener(progressListener);
                download.addProgressListener(new ProgressListener() {
                    private long downloadedSize = 0;
                    private int progressEventCounter = 0;

                    public void progressChanged(ProgressEvent progressEvent) {
                        this.downloadedSize += progressEvent.getBytesTransferred();
                        int eventCode = progressEvent.getEventCode();
                        if (eventCode == 4) {
                            S3BrowserFragment.sLogger.d("S3 file downloaded");
                        } else if (eventCode == 8) {
                            S3BrowserFragment.sLogger.d("Failed to download file from S3");
                        } else {
                            int i = this.progressEventCounter + 1;
                            this.progressEventCounter = i;
                            if (i > 1000) {
                                this.progressEventCounter = 0;
                                String message = String.format("%s downloaded", new Object[]{FormatUtils.readableFileSize(this.downloadedSize)});
                                S3BrowserFragment.sLogger.d(message);
                                if (SystemClock.elapsedRealtime() - startTime > CalendarUtils.EVENTS_STALENESS_LIMIT) {
                                    S3BrowserFragment.this.mAppInstance.showToast("S3 download timeout raised", false);
                                    try {
                                        download.abort();
                                    } catch (IOException e) {
                                        S3BrowserFragment.sLogger.e("Exception while aborting S3 download", e);
                                    } finally {
                                        transferManager.shutdownNow(false);
                                    }
                                } else {
                                    S3BrowserFragment.this.mAppInstance.showToast(message, false, 0);
                                }
                            }
                        }
                    }
                });
                try {
                    download.waitForCompletion();
                } catch (Throwable e) {
                    S3BrowserFragment.sLogger.d("Download interrupted ", e);
                    S3BrowserFragment.this.mAppInstance.showToast("S3 download process raised exception", false);
                } finally {
                    new Handler(S3BrowserFragment.this.mContext.getMainLooper()).post(new Runnable() {
                        public void run() {
                            S3BrowserFragment.this.mRefreshButton.setText(R.string.refresh);
                            S3BrowserFragment.this.mBackButton.setEnabled(true);
                            S3BrowserFragment.this.mRefreshButton.setEnabled(true);
                            S3BrowserFragment.this.mBackButton.invalidate();
                            S3BrowserFragment.this.mRefreshButton.invalidate();
                        }
                    });
                }
            }
        }).start();
    }
}
