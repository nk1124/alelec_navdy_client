package com.navdy.client.debug.videoplayer;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.mobileconnectors.s3.transfermanager.Download;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.servicehandler.LocationTransmitter;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.client.debug.videoplayer.MockLocationTransmitter.ITimeSource;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.net.URL;

public class VideoPlayerActivity extends Activity implements OnPreparedListener {
    public static final String BUCKET_NAME_EXTRA = "bucket_name_extra";
    public static final String DB_URL_EXTRA = "db_url_extra";
    public static final String SESSION_PATH_EXTRA = "session_path_extra";
    public static final String VIDEO_URL_EXTRA = "video_url_extra";
    private static final Logger sLogger = new Logger(VideoPlayerActivity.class);
    private String bucketName;
    private Download download;
    @InjectView(2131755226)
    TextView downloadStatus;
    private DownloadTask downloadTask;
    private File localDbFile;
    private MediaController mediaController;
    private String sessionPath;
    private ITimeSource timeSource = new ITimeSource() {
        public long getCurrentTime() {
            return (long) VideoPlayerActivity.this.videoView.getCurrentPosition();
        }
    };
    private TransferManager transferManager;
    private boolean videoPlayerIsInitialized = false;
    @InjectView(2131755225)
    VideoView videoView;

    private class DownloadTask extends AsyncTask<Void, Void, Void> {
        private DownloadTask() {
        }

//        /* synthetic */ DownloadTask(VideoPlayerActivity x0, AnonymousClass1 x1) {
//            this();
//        }

        protected void onPreExecute() {
            VideoPlayerActivity.this.downloadStatus.setText(String.format("Downloading %s%s", new Object[]{VideoPlayerActivity.this.sessionPath, S3Constants.DB_FILE}));
        }

        protected void onPostExecute(Void aVoid) {
            VideoPlayerActivity.this.downloadStatus.setVisibility(GONE);
        }

        protected Void doInBackground(Void... params) {
            String key = VideoPlayerActivity.this.sessionPath + S3Constants.DB_FILE;
            VideoPlayerActivity.this.localDbFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), key);
            VideoPlayerActivity.sLogger.d("Downloading s3 object (" + key + ") to " + VideoPlayerActivity.this.localDbFile.getAbsolutePath());
            if (!IOUtils.createDirectory(VideoPlayerActivity.this.localDbFile.getParentFile())) {
                VideoPlayerActivity.sLogger.e("Failed to create directory for db at " + VideoPlayerActivity.this.localDbFile.getAbsolutePath());
            } else if (VideoPlayerActivity.this.localDbFile.exists()) {
                VideoPlayerActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        VideoPlayerActivity.this.startMockLocationTransmitter();
                    }
                });
            } else {
                VideoPlayerActivity.this.download = VideoPlayerActivity.this.transferManager.download(VideoPlayerActivity.this.bucketName, key, VideoPlayerActivity.this.localDbFile);
                VideoPlayerActivity.this.download.addProgressListener(new ProgressListener() {
                    public void progressChanged(ProgressEvent progressEvent) {
                        VideoPlayerActivity.sLogger.d("Download event " + progressEvent);
                        int eventCode = progressEvent.getEventCode();
                        if (eventCode == 4) {
                            VideoPlayerActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    VideoPlayerActivity.this.startMockLocationTransmitter();
                                }
                            });
                        } else if (eventCode == 8) {
                            VideoPlayerActivity.sLogger.d("Failed to download db");
                        }
                    }
                });
                try {
                    VideoPlayerActivity.this.download.waitForCompletion();
                } catch (InterruptedException e) {
                    VideoPlayerActivity.sLogger.d("Download interrupted ", e);
                }
                VideoPlayerActivity.this.downloadTask = null;
            }
            return null;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView(R.layout.activity_video_player);
        this.sessionPath = getIntent().getStringExtra(SESSION_PATH_EXTRA);
        this.bucketName = getIntent().getStringExtra(BUCKET_NAME_EXTRA);
        ButterKnife.inject((Activity) this);
        String AWS_ACCOUNT_ID = "";
        String AWS_SECRET = "";
        try {
            Context context = NavdyApplication.getAppContext();
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            AWS_ACCOUNT_ID = bundle.getString("AWS_ACCOUNT_ID");
            AWS_SECRET = bundle.getString("AWS_SECRET");
        } catch (NameNotFoundException e) {
            sLogger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e2) {
            sLogger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
        }
        this.transferManager = new TransferManager(new BasicAWSCredentials(AWS_ACCOUNT_ID, AWS_SECRET));
    }

    private void initVideoPlayer() {
        if (!this.videoPlayerIsInitialized) {
            this.videoPlayerIsInitialized = true;
            this.mediaController = new MediaController(this);
            this.mediaController.setAnchorView(this.videoView);
            this.videoView.setOnPreparedListener(this);
            this.videoView.setMediaController(this.mediaController);
            URL videoUrl = (URL) getIntent().getSerializableExtra(VIDEO_URL_EXTRA);
            sLogger.d("Trying to play video at " + videoUrl);
            if (videoUrl != null) {
                this.videoView.setVideoURI(Uri.parse(videoUrl.toString()));
            }
        }
    }

    protected void onResume() {
        super.onResume();
        initVideoPlayer();
        if (this.videoView != null) {
            this.videoView.start();
        }
        this.downloadTask = new DownloadTask(this, null);
        this.downloadTask.execute(new Void[0]);
    }

    protected void onPause() {
        super.onPause();
        if (this.videoView != null) {
            this.videoView.stopPlayback();
        }
        if (this.downloadTask != null) {
            this.downloadTask.cancel(false);
            this.downloadTask = null;
        }
        startLocationTransmitter();
    }

    protected void onStop() {
        if (this.videoView != null) {
            this.videoView.cancelPendingInputEvents();
            this.videoView = null;
        }
        if (this.mediaController != null) {
            this.mediaController.hide();
            this.mediaController.removeAllViews();
            this.mediaController = null;
        }
        super.onStop();
    }

    private void startLocationTransmitter() {
        AppInstance instance = AppInstance.getInstance();
        RemoteDevice device = instance.getRemoteDevice();
        if (device != null) {
            instance.setLocationTransmitter(new LocationTransmitter(getApplicationContext(), device));
        }
    }

    private void startMockLocationTransmitter() {
        AppInstance instance = AppInstance.getInstance();
        RemoteDevice device = instance.getRemoteDevice();
        if (device != null) {
            sLogger.d("Starting location transmitter with db at " + this.localDbFile);
            MockLocationTransmitter transmitter = new MockLocationTransmitter(this, device);
            if (transmitter.setSource(this.localDbFile.getAbsolutePath(), this.timeSource)) {
                instance.setLocationTransmitter(transmitter);
            } else {
                Toast.makeText(this, "No location info with video", 0).show();
            }
        }
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        if (BaseActivity.isEnding(this)) {
            if (mediaPlayer != null) {
                mediaPlayer.stop();
            }
            if (this.videoView != null) {
                this.videoView.stopPlayback();
            }
        } else if (mediaPlayer != null) {
            mediaPlayer.setLooping(true);
        }
    }
}
