package com.navdy.client.debug.videoplayer;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Handler;
import com.navdy.client.app.framework.servicehandler.LocationTransmitter;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MockLocationTransmitter extends LocationTransmitter {
    private static final int DELAY_MS = 1000;
    private static final int NETWORK_UPDATE_INTERVAL = 10000;
    private static final Logger sLogger = new Logger(MockLocationTransmitter.class);
    private Comparator<Location> comparator = new Comparator<Location>() {
        public int compare(Location lhs, Location rhs) {
            long lh = lhs.getTime();
            long rh = rhs.getTime();
            if (lh < rh) {
                return -1;
            }
            return lh == rh ? 0 : 1;
        }
    };
    private ArrayList<Location> coordinates = new ArrayList();
    private Handler handler = new Handler();
    private long lastNetworkUpdate = 0;
    private Runnable locationUpdater = new Runnable() {
        public void run() {
            Location location = MockLocationTransmitter.this.getLocation();
            MockLocationTransmitter.this.transmitLocation(location);
            long now = System.currentTimeMillis();
            if (now - MockLocationTransmitter.this.lastNetworkUpdate > 10000) {
                MockLocationTransmitter.this.lastNetworkUpdate = now;
                location.setProvider("network");
                location.setAccuracy(30.0f);
                location.setAltitude(0.0d);
                location.setBearing(0.0f);
                location.setSpeed(0.0f);
                MockLocationTransmitter.this.transmitLocation(location);
            }
            if (MockLocationTransmitter.this.transmitting) {
                MockLocationTransmitter.this.handler.postDelayed(this, 1000);
            }
        }
    };
    private long recordingDuration;
    private long recordingEndTime;
    private long recordingStartTime;
    private ITimeSource timeSource;
    private boolean transmitting = false;

    public interface ITimeSource {
        long getCurrentTime();
    }

    public MockLocationTransmitter(Context context, RemoteDevice remoteDevice) {
        super(context, remoteDevice);
    }

    public boolean hasLocationServices() {
        return true;
    }

    protected void startLocationUpdates() {
        if (!this.transmitting) {
            this.transmitting = true;
            this.handler.postDelayed(this.locationUpdater, 1000);
        }
    }

    protected void stopLocationUpdates() {
        if (this.transmitting) {
            this.transmitting = false;
            this.handler.removeCallbacks(this.locationUpdater);
        }
    }

    public Location getLocation() {
        long mappedTime = (this.timeSource.getCurrentTime() % this.recordingDuration) + this.recordingStartTime;
        Location dummy = new Location("gps");
        dummy.setTime(mappedTime);
        int position = Collections.binarySearch(this.coordinates, dummy, this.comparator);
        if (position < 0) {
            position = (-position) - 1;
        }
        if (position >= this.coordinates.size()) {
            return dummy;
        }
        Location copy = new Location((Location) this.coordinates.get(position));
        copy.setTime(System.currentTimeMillis());
        return copy;
    }

    private boolean hasTable(SQLiteDatabase db, String tableName) {
        boolean z = false;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
            if (cursor != null && cursor.getCount() > 0) {
                z = true;
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (RuntimeException e) {
            sLogger.e("Failed to read table info", e);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00b7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean setSource(String path, ITimeSource timeSource) {
        this.coordinates.clear();
        this.timeSource = timeSource;
        sLogger.d("Reading locations from db");
        SQLiteDatabase db = SQLiteDatabase.openDatabase(path, null, 268435456);
        if (hasTable(db, "location")) {
            Cursor cursor = null;
            try {
                cursor = db.rawQuery("select * from location order by time", null);
                boolean first = true;
                while (cursor.moveToNext()) {
                    int col = 0 + 1;
                    Location location = new Location(cursor.getString(0));
                    int col2 = col + 1;
                    location.setAccuracy(cursor.getFloat(col));
                    col = col2 + 1;
                    location.setAltitude(cursor.getDouble(col2));
                    col2 = col + 1;
                    location.setBearing(cursor.getFloat(col));
                    col = col2 + 1;
                    location.setLatitude(cursor.getDouble(col2));
                    col2 = col + 1;
                    location.setLongitude(cursor.getDouble(col));
                    col = col2 + 1;
                    location.setSpeed(cursor.getFloat(col2));
                    col2 = col + 1;
                    long time = cursor.getLong(col);
                    location.setTime(time);
                    if (first) {
                        first = false;
                        this.recordingStartTime = time;
                    }
                    this.recordingEndTime = time;
                    this.coordinates.add(location);
                    cursor.moveToNext();
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e) {
                sLogger.e("Failed to load locations", e);
                db.close();
                if (this.coordinates.size() > 1) {
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            db.close();
            if (this.coordinates.size() > 1) {
                return false;
            }
            this.recordingDuration = this.recordingEndTime - this.recordingStartTime;
            sLogger.d("Read " + this.coordinates.size() + " locations from db - duration:" + (((double) this.recordingDuration) / 1000.0d) + " seconds");
            return true;
        }
        db.close();
        return false;
    }
}
