package com.navdy.client.debug;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import butterknife.ButterKnife;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.servicehandler.RecordingServiceManager;
import com.navdy.client.app.framework.suggestion.DestinationSuggestionService;
import com.navdy.client.app.framework.util.CredentialsUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Settings;
import com.navdy.client.debug.common.BaseDebugActivity;
import com.navdy.client.debug.gesture.GestureControlFragment;
import com.navdy.client.debug.music.MusicControlFragment;
import com.navdy.client.debug.util.FragmentHelper;
import com.navdy.client.ota.OTAUpdateService;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.hockeyapp.android.UpdateManager;
import net.hockeyapp.android.UpdateManagerListener;

public class MainDebugActivity extends BaseDebugActivity {
    public static final String EXTRA_OPEN_DIAL_SIM = "openDialSimulator";
    public static final String MAIN_TAG = "main";
    public static final String SPLASH_TAG = "splash";
    public static final Logger sLogger = new Logger(AppInstance.class);
    protected AppInstance mAppInstance;

    public static class MainFragment extends ListFragment {
        private static final IntentFilter DATETIME_CHANGE_INTENT_FILTER = new IntentFilter("android.intent.action.DATE_CHANGED");
        private ConnectionStatusUpdater mConnectionStatus;
        private List<ListItem> mListItems;

        enum ListItem {
            SEARCH(R.string.main_search, GoogleAddressPickerFragment.class),
            RECENTS(R.string.main_recents),
            SETTINGS(R.string.main_settings, SettingsFragment.class),
            GESTURE(R.string.main_gesture, GestureControlFragment.class),
            DEBUG(R.string.main_debug, DebugActionsFragment.class),
            TICKET(R.string.main_submit_ticket, SubmitTicketFragment.class),
            NAVDY_DIAL(R.string.main_navdy_dial, NavdyDialFragment.class),
            RECORD_ROUTE(R.string.main_record_drive),
            STOP_RECORD_ROUTE(R.string.main_stop_record_drive),
            UPLOAD_TRIP_DATA(R.string.upload_trip_data),
            MUSIC(R.string.main_music, MusicControlFragment.class);
            
            private final int mResId;
            private String mString;
            private Class<? extends Fragment> targetFragment;

            private ListItem(int item) {
                this(r2, r3, item, null);
            }

            private ListItem(int mResId, Class<? extends Fragment> targetFragment) {
                this.mResId = mResId;
                this.targetFragment = targetFragment;
            }

            public Class<? extends Fragment> getTargetFragment() {
                return this.targetFragment;
            }

            void bindResource(Resources r) {
                this.mString = r.getString(this.mResId);
            }

            public String toString() {
                return this.mString;
            }
        }

        static {
            DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIME_SET");
            DATETIME_CHANGE_INTENT_FILTER.addAction("android.intent.action.TIMEZONE_CHANGED");
        }

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            this.mListItems = new ArrayList(Arrays.asList(ListItem.values()));
            for (ListItem item : this.mListItems) {
                item.bindResource(getResources());
            }
            setListAdapter(new ArrayAdapter(getActivity(), R.layout.list_item_main, android.R.id.text1, ListItem.values()));
        }

        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            ButterKnife.inject((Object) this, rootView);
            this.mConnectionStatus = new ConnectionStatusUpdater(rootView);
            return rootView;
        }

        public void onPause() {
            this.mConnectionStatus.onPause();
            super.onPause();
        }

        public void onResume() {
            super.onResume();
            if (getActivity().getActionBar() != null) {
                getActivity().getActionBar().setTitle(R.string.title_activity_main);
            }
            this.mConnectionStatus.onResume();
            if (OTAUpdateService.isLaunchedByOtaUpdateService(getActivity().getIntent())) {
                FragmentHelper.pushFullScreenFragment(getFragmentManager(), SettingsFragment.class);
            }
        }

        public void onListItemClick(ListView l, View v, int position, long id) {
            super.onListItemClick(l, v, position, id);
            l.setItemChecked(position, false);
            ListItem clickedItem = (ListItem) this.mListItems.get(position);
            switch (clickedItem) {
                case SEARCH:
                    pushNavigationSearchFragment();
                    return;
                case RECORD_ROUTE:
                    recordRoute();
                    return;
                case STOP_RECORD_ROUTE:
                    stopRecordingRoute();
                    return;
                case UPLOAD_TRIP_DATA:
                    DestinationSuggestionService.uploadTripDatabase(getActivity(), false);
                    Toast.makeText(getActivity(), "Thanks :) You are awesome !!", 1).show();
                    return;
                default:
                    Class<? extends Fragment> nextFragment = clickedItem.getTargetFragment();
                    if (nextFragment != null) {
                        FragmentHelper.pushFullScreenFragment(getFragmentManager(), nextFragment);
                        return;
                    }
                    return;
            }
        }

        private void recordRoute() {
            View promptsView = LayoutInflater.from(getActivity()).inflate(R.layout.record_route_name_prompt, null);
            Builder alertDialogBuilder = new Builder(getActivity());
            alertDialogBuilder.setView(promptsView);
            final EditText userInput = (EditText) promptsView.findViewById(R.id.drive_name_input);
            alertDialogBuilder.setPositiveButton("Go", new OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    RecordingServiceManager.getInstance().sendStartDriveRecordingEvent(userInput.getText().toString());
                }
            }).setNegativeButton("Cancel", new OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            alertDialogBuilder.create().show();
        }

        private void stopRecordingRoute() {
            RecordingServiceManager.getInstance().sendStopDriveRecordingEvent();
        }

        private void pushNavigationSearchFragment() {
            FragmentHelper.pushFullScreenFragment(getFragmentManager(), GoogleAddressPickerFragment.class);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        boolean hasConnection;
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        setContentView(R.layout.activity_main);
        this.mAppInstance = AppInstance.getInstance();
        if (this.mAppInstance.getRemoteDevice() != null) {
            hasConnection = true;
        } else {
            hasConnection = false;
        }
        boolean launchedFromOtaNotification = OTAUpdateService.isLaunchedByOtaUpdateService(getIntent());
        if (hasConnection || launchedFromOtaNotification) {
            Fragment initialFragment = new MainFragment();
        } else {
            SplashFragment splashFragment = new SplashFragment();
        }
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(R.id.container, new MainFragment(), MAIN_TAG).commit();
            if (!NavdyApplication.isDeveloperBuild()) {
                checkForUpdates();
            }
        }
        if (getIntent().getBooleanExtra(EXTRA_OPEN_DIAL_SIM, false)) {
            FragmentHelper.pushFullScreenFragment(getFragmentManager(), GestureControlFragment.class);
        }
    }

    public void onResume() {
        super.onResume();
        Tracker.tagScreen(Settings.DEBUG);
    }

    public void checkForUpdates(UpdateManagerListener listener) {
        UpdateManager.register((Activity) this, CredentialsUtils.getCredentials(NavdyApplication.getAppContext().getString(R.string.metadata_hockey_app_credentials)), listener, true);
    }

    public void checkForUpdates() {
        checkForUpdates(null);
    }
}
