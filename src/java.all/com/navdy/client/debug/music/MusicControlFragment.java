package com.navdy.client.debug.music;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicEvent;
import com.navdy.service.library.events.audio.MusicEvent.Action;
import com.navdy.service.library.events.audio.MusicEvent.Builder;
import com.navdy.service.library.task.TaskManager;

public class MusicControlFragment extends Fragment {
    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_music_control, container, false);
        setUpButton((Button) rootView.findViewById(R.id.prev), Action.MUSIC_ACTION_PREVIOUS);
        setUpButton((Button) rootView.findViewById(R.id.play), Action.MUSIC_ACTION_PLAY);
        setUpButton((Button) rootView.findViewById(R.id.pause), Action.MUSIC_ACTION_PAUSE);
        setUpButton((Button) rootView.findViewById(R.id.next), Action.MUSIC_ACTION_NEXT);
        return rootView;
    }

    private void setUpButton(@Nullable Button pauseButton, final Action action) {
        if (pauseButton != null) {
            pauseButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    MusicControlFragment.this.performAction(action);
                }
            });
        }
    }

    private void performAction(Action action) {
        final MusicEvent event = new Builder().action(action).dataSource(MusicDataSource.MUSIC_SOURCE_NONE).build();
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                MusicServiceHandler.getInstance().onMusicEvent(event);
            }
        }, 1);
    }
}
