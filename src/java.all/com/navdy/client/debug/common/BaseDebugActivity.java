package com.navdy.client.debug.common;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider;
import com.navdy.client.app.ui.base.BaseFragment;
import com.navdy.client.debug.util.FragmentHelper;

public class BaseDebugActivity extends Activity implements DialogProvider {
    protected boolean destroyed;

    protected void onDestroy() {
        this.destroyed = true;
        super.onDestroy();
    }

    public boolean isActivityDestroyed() {
        return this.destroyed || isFinishing();
    }

    public Dialog createDialog(int id, Bundle arguments) {
        Fragment topFragment = FragmentHelper.getTopFragment(getFragmentManager());
        if (topFragment == null || !(topFragment instanceof BaseFragment)) {
            return null;
        }
        return ((BaseFragment) topFragment).createDialog(id, arguments);
    }
}
