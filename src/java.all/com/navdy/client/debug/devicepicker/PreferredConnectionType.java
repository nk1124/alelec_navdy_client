package com.navdy.client.debug.devicepicker;

import com.navdy.service.library.device.connection.ConnectionType;

public enum PreferredConnectionType {
    TCP(ConnectionType.TCP_PROTOBUF),
    BT(ConnectionType.BT_PROTOBUF),
    HTTP(ConnectionType.HTTP);
    
    private ConnectionType type;

    private PreferredConnectionType(ConnectionType type) {
        this.type = type;
    }

    public ConnectionType getType() {
        return this.type;
    }

    public int getPriority() {
        return ordinal();
    }

    static PreferredConnectionType fromConnectionType(ConnectionType type) {
        for (PreferredConnectionType preferredType : values()) {
            if (preferredType.getType() == type) {
                return preferredType;
            }
        }
        return null;
    }
}
