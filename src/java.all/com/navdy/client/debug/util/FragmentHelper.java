package com.navdy.client.debug.util;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import com.alelec.navdyclient.R;
import com.navdy.service.library.log.Logger;

public class FragmentHelper {
    private static final Logger sLogger = new Logger(FragmentHelper.class);

    public static void pushFullScreenFragment(FragmentManager fragmentManager, Class<? extends Fragment> fragmentClass) {
        pushFullScreenFragment(fragmentManager, fragmentClass, null);
    }

    public static void pushFullScreenFragment(FragmentManager fragmentManager, Class<? extends Fragment> fragmentClass, Bundle args) {
        try {
            Fragment newFragment = (Fragment) fragmentClass.newInstance();
            if (args != null) {
                newFragment.setArguments(args);
            }
            newFragment.setRetainInstance(true);
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container, newFragment);
            ft.setTransition(android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(null);
            ft.commit();
        } catch (InstantiationException e) {
            sLogger.d("Can't instantiate: " + fragmentClass.getCanonicalName(), e);
        } catch (IllegalAccessException e2) {
            sLogger.d("Can't access: " + fragmentClass.getCanonicalName(), e2);
        }
    }

    public static Fragment getTopFragment(FragmentManager manager) {
        return manager.findFragmentById(R.id.container);
    }
}
