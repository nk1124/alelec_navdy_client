package com.navdy.client.debug;

import com.navdy.service.library.events.location.Coordinate;

public class DestinationAddressPickerFragment extends RemoteAddressPickerFragment {

    public interface DestinationListener {
        void setDestination(String str, Coordinate coordinate);
    }

    protected void processSelectedLocation(Coordinate location, String locationLabel, String streetAddress) {
        if (getActivity() instanceof DestinationListener) {
            ((DestinationListener) getActivity()).setDestination(locationLabel, location);
        }
        getFragmentManager().popBackStack();
    }
}
