package com.navdy.client.debug;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.debug.file.FileTransferManager.FileTransferListener;
import com.navdy.client.debug.file.RemoteFileTransferManager;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.Locale;

public class FileTransferTestFragment extends Fragment implements OnClickListener {
    private static final Logger sLogger = new Logger(FileTransferTestFragment.class);
    private long downloadStartTime;
    private long downloadTotalBytes;
    private Handler handler = new Handler();
    @InjectView(2131755580)
    LinearLayout progressContainer;
    @InjectView(2131755575)
    RadioGroup sizeSelection;
    @InjectView(2131755579)
    Button transferButton;
    @InjectView(2131755582)
    TextView transferMessage;
    @InjectView(2131755581)
    ProgressBar transferProgress;

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sLogger.w("onCreate()");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sLogger.w("onCreateView()");
        View rootView = inflater.inflate(R.layout.fragment_file_transfer_test, container, false);
        ButterKnife.inject((Object) this, rootView);
        this.transferButton.setOnClickListener(this);
        return rootView;
    }

    private int getSelectedSize() {
        switch (this.sizeSelection.getCheckedRadioButtonId()) {
            case R.id.transfer_size1mb /*2131755576*/:
                return 1;
            case R.id.transfer_size10mb /*2131755577*/:
                return 10;
            case R.id.transfer_size100mb /*2131755578*/:
                return 100;
            default:
                sLogger.e(String.format("unexpected file transfer size selection %x", new Object[]{Integer.valueOf(sel)}));
                return 1;
        }
    }

    private void recordTransferComplete() {
        this.handler.post(new Runnable() {
            public void run() {
                FileTransferTestFragment.this.transferProgress.setProgress((int) FileTransferTestFragment.this.downloadTotalBytes);
                float rate = ((float) FileTransferTestFragment.this.downloadTotalBytes) / ((float) (System.currentTimeMillis() - FileTransferTestFragment.this.downloadStartTime));
                FileTransferTestFragment.this.transferMessage.setText(String.format(Locale.getDefault(), "transferred %d bytes in %d ms, %6.2f Kb/sec.", new Object[]{Long.valueOf(FileTransferTestFragment.this.downloadTotalBytes), Long.valueOf(elapsedMs), Float.valueOf(rate)}));
                FileTransferTestFragment.this.transferButton.setEnabled(true);
            }
        });
    }

    private void recordTransferFailed(final String error) {
        this.handler.post(new Runnable() {
            public void run() {
                FileTransferTestFragment.this.transferMessage.setText(FileTransferTestFragment.this.getString(R.string.transfer_failed, new Object[]{error}));
                FileTransferTestFragment.this.transferButton.setEnabled(true);
            }
        });
    }

    public void onClick(View v) {
        this.downloadTotalBytes = ((long) (getSelectedSize() * 1024)) * 1024;
        this.transferProgress.setMax((int) this.downloadTotalBytes);
        this.transferProgress.setProgress(0);
        this.transferMessage.setText(R.string.transfer_progress);
        this.progressContainer.setVisibility(VISIBLE);
        this.transferButton.setEnabled(false);
        this.downloadStartTime = System.currentTimeMillis();
        final RemoteFileTransferManager fileTransferManager = new RemoteFileTransferManager(NavdyApplication.getAppContext(), FileType.FILE_TYPE_PERF_TEST, ((long) (getSelectedSize() * 1024)) * 1024, new FileTransferListener() {
            public void onFileTransferResponse(FileTransferResponse response) {
                if (FileTransferTestFragment.this.isAdded() && !response.success.booleanValue()) {
                    FileTransferTestFragment.sLogger.e("error in file transfer: " + response);
                }
            }

            public void onFileTransferStatus(FileTransferStatus status) {
                if (!FileTransferTestFragment.this.isAdded()) {
                    return;
                }
                if (!status.success.booleanValue()) {
                    FileTransferTestFragment.this.recordTransferFailed(status.error.toString());
                } else if (status.transferComplete.booleanValue()) {
                    FileTransferTestFragment.this.recordTransferComplete();
                } else {
                    FileTransferTestFragment.this.transferProgress.setProgress(status.totalBytesTransferred.intValue());
                }
            }

            public void onError(FileTransferError errorCode, String error) {
                if (FileTransferTestFragment.this.isAdded()) {
                    FileTransferTestFragment.sLogger.e("Transfer Error :" + error);
                    FileTransferTestFragment.this.recordTransferFailed(error);
                }
            }
        });
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                fileTransferManager.sendFile();
            }
        }, 2);
    }
}
