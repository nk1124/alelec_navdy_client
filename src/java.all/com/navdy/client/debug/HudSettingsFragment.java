package com.navdy.client.debug;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.ui.base.BaseFragment;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.settings.ReadSettingsRequest;
import com.navdy.service.library.events.settings.ReadSettingsResponse;
import com.navdy.service.library.events.settings.Setting;
import com.navdy.service.library.events.settings.UpdateSettings;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class HudSettingsFragment extends BaseFragment {
    public static final String AUTO_BRIGHTNESS = "screen.auto_brightness";
    public static final String BRIGHTNESS = "screen.brightness";
    public static final String GESTURE_ENGINE = "gesture.engine";
    public static final String GESTURE_PREVIEW = "gesture.preview";
    public static final String LED_BRIGHTNESS = "screen.led_brightness";
    public static final String MAP_TILT = "map.tilt";
    public static final String MAP_ZOOM = "map.zoom";
    private static final double MAX_ZOOM_LEVEL = 20.0d;
    private static final double MIN_ZOOM_LEVEL = 2.0d;
    private static final double SCALE_FACTOR = 11.11111111111111d;
    public static final String START_VIDEO_ON_BOOT = "start_video_on_boot_preference";
    private static final int ZOOM_MAX_PROGRESS = 200;
    private static final double ZOOM_RANGE = 18.0d;
    @InjectView(2131755605)
    CheckBox autoBrightness;
    @InjectView(2131755607)
    SeekBar brightness;
    @InjectView(2131755606)
    TextView brightnessTextView;
    @InjectView(2131755611)
    CheckBox enableGesture;
    @InjectView(2131755612)
    CheckBox enablePreview;
    @InjectView(2131755603)
    CheckBox enableSpecialVoiceSearch;
    @InjectView(2131755613)
    CheckBox enableVideoLoop;
    private List<Setting> hudSettings;
    @InjectView(2131755610)
    SeekBar ledBrightness;
    @InjectView(2131755609)
    TextView ledBrightnessTextView;
    @InjectView(2131755617)
    SeekBar mapTilt;
    @InjectView(2131755615)
    SeekBar mapZoomLevel;
    private SharedPreferences sharedPreferences;
    private OnSeekBarChangeListener sliderListener = new OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            String setting;
            String value = String.valueOf(i);
            switch (seekBar.getId()) {
                case R.id.brightness /*2131755607*/:
                    setting = HudSettingsFragment.BRIGHTNESS;
                    HudSettingsFragment.this.brightnessTextView.setText(value);
                    break;
                case R.id.ledBrightness /*2131755610*/:
                    setting = HudSettingsFragment.LED_BRIGHTNESS;
                    HudSettingsFragment.this.ledBrightnessTextView.setText(value);
                    break;
                case R.id.mapZoomLevel /*2131755615*/:
                    setting = HudSettingsFragment.MAP_ZOOM;
                    value = String.format(Locale.US, "%.1f", new Object[]{Double.valueOf((((double) i) / HudSettingsFragment.SCALE_FACTOR) + HudSettingsFragment.MIN_ZOOM_LEVEL)});
                    HudSettingsFragment.this.zoomLevelTextView.setText(value);
                    break;
                case R.id.mapTilt /*2131755617*/:
                    setting = HudSettingsFragment.MAP_TILT;
                    HudSettingsFragment.this.tiltLevelTextView.setText(value);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown slider");
            }
            HudSettingsFragment.this.updateSetting(setting, value);
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };
    @InjectView(2131755616)
    TextView tiltLevelTextView;
    @InjectView(2131755602)
    TextView voiceSearchLabel;
    @InjectView(2131755614)
    TextView zoomLevelTextView;

    private class SettingSpinnerListener implements OnItemSelectedListener {
        private String setting;
        private String value;

        SettingSpinnerListener(String setting, String value) {
            this.setting = setting;
            this.value = value;
        }

        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String selectedValue = parent.getSelectedItem().toString();
            if (!selectedValue.equals(this.value)) {
                this.value = selectedValue;
                HudSettingsFragment.this.updateSetting(this.setting, this.value);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_hud_settings, container, false);
        ButterKnife.inject((Object) this, rootView);
        this.mapTilt.setOnSeekBarChangeListener(this.sliderListener);
        this.mapZoomLevel.setOnSeekBarChangeListener(this.sliderListener);
        this.brightness.setOnSeekBarChangeListener(this.sliderListener);
        this.ledBrightness.setOnSeekBarChangeListener(this.sliderListener);
        this.brightness.setEnabled(false);
        this.ledBrightness.setEnabled(false);
        this.sharedPreferences = SettingsUtils.getSharedPreferences();
        return rootView;
    }

    @OnClick({2131755605, 2131755612, 2131755611, 2131755613})
    public void onClick(CheckBox box) {
        String name;
        String checkedValue = String.valueOf(box.isChecked());
        switch (box.getId()) {
            case R.id.autoBrightness /*2131755605*/:
                name = AUTO_BRIGHTNESS;
                break;
            case R.id.enable_gesture /*2131755611*/:
                name = GESTURE_ENGINE;
                break;
            case R.id.enable_preview /*2131755612*/:
                name = GESTURE_PREVIEW;
                break;
            case R.id.enable_video_loop /*2131755613*/:
                name = START_VIDEO_ON_BOOT;
                break;
            default:
                throw new IllegalArgumentException("invalid checkbox");
        }
        updateSetting(name, checkedValue);
    }

    @OnClick({2131755618})
    public void onClick(View button) {
        List settings = new ArrayList();
        settings.add(new Setting(MAP_ZOOM, "-1"));
        settings.add(new Setting(MAP_TILT, "-1"));
        sendEvent(new UpdateSettings(null, settings));
        this.hudSettings = null;
        getSettings();
    }

    private void updateSetting(String name, String value) {
        List settings = new ArrayList();
        settings.add(new Setting(name, value));
        sendEvent(new UpdateSettings(null, settings));
    }

    public void onResume() {
        super.onResume();
        getSettings();
    }

    private void getSettings() {
        if (this.hudSettings == null) {
            List settings = new ArrayList();
            settings.add(AUTO_BRIGHTNESS);
            settings.add(BRIGHTNESS);
            settings.add(LED_BRIGHTNESS);
            settings.add(GESTURE_ENGINE);
            settings.add(GESTURE_PREVIEW);
            settings.add(MAP_TILT);
            settings.add(MAP_ZOOM);
            settings.add(START_VIDEO_ON_BOOT);
            sendEvent(new ReadSettingsRequest(settings));
        }
    }

    private void sendEvent(Message event) {
        DeviceConnection.postEvent(event);
    }

    @Subscribe
    public void onDeviceConnectedEvent(DeviceConnectedEvent event) {
        getSettings();
    }

    @Subscribe
    public void onDeviceDisconnectedEvent(DeviceDisconnectedEvent event) {
        this.hudSettings = null;
    }

    @Subscribe
    public void onReadSettingsResponse(ReadSettingsResponse response) {
        this.hudSettings = response.settings;
        for (Setting setting : this.hudSettings) {
            String str = setting.key;
            boolean z = true;
            switch (str.hashCode()) {
                case -1036816761:
                    if (str.equals(GESTURE_ENGINE)) {
                        z = true;
                        break;
                    }
                    break;
                case -790890461:
                    if (str.equals(GESTURE_PREVIEW)) {
                        z = true;
                        break;
                    }
                    break;
                case -408006893:
                    if (str.equals(BRIGHTNESS)) {
                        z = false;
                        break;
                    }
                    break;
                case 133816335:
                    if (str.equals(MAP_TILT)) {
                        z = true;
                        break;
                    }
                    break;
                case 134000933:
                    if (str.equals(MAP_ZOOM)) {
                        z = true;
                        break;
                    }
                    break;
                case 285256903:
                    if (str.equals(LED_BRIGHTNESS)) {
                        z = true;
                        break;
                    }
                    break;
                case 1278641513:
                    if (str.equals(START_VIDEO_ON_BOOT)) {
                        z = true;
                        break;
                    }
                    break;
                case 1839978335:
                    if (str.equals(AUTO_BRIGHTNESS)) {
                        z = true;
                        break;
                    }
                    break;
            }
            switch (z) {
                case false:
                    updateSeekBar(this.brightness, setting.value);
                    break;
                case true:
                    this.autoBrightness.setEnabled(true);
                    this.autoBrightness.setChecked(Boolean.parseBoolean(setting.value));
                    break;
                case true:
                    updateSeekBar(this.ledBrightness, setting.value);
                    break;
                case true:
                    this.enablePreview.setEnabled(true);
                    this.enablePreview.setChecked(Boolean.parseBoolean(setting.value));
                    break;
                case true:
                    this.enableGesture.setEnabled(true);
                    this.enableGesture.setChecked(Boolean.parseBoolean(setting.value));
                    break;
                case true:
                    updateSeekBar(this.mapZoomLevel, setting.value, MIN_ZOOM_LEVEL, SCALE_FACTOR);
                    break;
                case true:
                    updateSeekBar(this.mapTilt, setting.value);
                    break;
                case true:
                    this.enableVideoLoop.setEnabled(true);
                    this.enableVideoLoop.setChecked(Boolean.parseBoolean(setting.value));
                    break;
                default:
                    break;
            }
        }
    }

    private void setupSpinner(Spinner spinner, String setting, String value, int optionsArrayId) {
        spinner.setEnabled(true);
        List<String> optionsArray = Arrays.asList(getResources().getStringArray(optionsArrayId));
        spinner.setOnItemSelectedListener(new SettingSpinnerListener(setting, value));
        ArrayAdapter<String> adapter = new ArrayAdapter(getActivity(), 17367048, optionsArray);
        adapter.setDropDownViewResource(17367049);
        spinner.setAdapter(adapter);
        spinner.setSelection(optionsArray.indexOf(value));
    }

    private void updateSeekBar(SeekBar seekBar, String value) {
        updateSeekBar(seekBar, value, 0.0d, 1.0d);
    }

    private void updateSeekBar(SeekBar seekBar, String value, double offset, double scaleFactor) {
        seekBar.setEnabled(true);
        seekBar.setProgress((int) ((Double.valueOf(value).doubleValue() - offset) * scaleFactor));
    }
}
