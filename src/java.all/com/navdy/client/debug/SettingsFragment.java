package com.navdy.client.debug;

import android.app.ListFragment;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import butterknife.ButterKnife;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment;
import com.navdy.client.app.ui.settings.BluetoothPairActivity;
import com.navdy.client.debug.util.FragmentHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SettingsFragment extends ListFragment {
    private ConnectionStatusUpdater mConnectionStatus;
    private List<ListItem> mListItems;

    private enum ListItem {
        CONNECT(R.string.settings_connect),
        SCREEN_LAYOUT(R.string.settings_adjust_screen),
        HUD_SETTINGS(R.string.hud_settings),
        ABOUT(R.string.about);
        
        private final int mResId;
        private String mString;

        private ListItem(int item) {
            this.mResId = item;
        }

        void bindResource(Resources r) {
            this.mString = r.getString(this.mResId);
        }

        public String toString() {
            return this.mString;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mListItems = new ArrayList(Arrays.asList(ListItem.values()));
        for (ListItem item : this.mListItems) {
            item.bindResource(getResources());
        }
        setListAdapter(new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_activated_1, android.R.id.text1, ListItem.values()));
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.inject((Object) this, rootView);
        this.mConnectionStatus = new ConnectionStatusUpdater(rootView);
        return rootView;
    }

    public void onResume() {
        super.onResume();
        this.mConnectionStatus.onResume();
    }

    public void onPause() {
        this.mConnectionStatus.onPause();
        super.onPause();
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.setItemChecked(position, false);
        switch ((ListItem) this.mListItems.get(position)) {
            case CONNECT:
                Intent i = new Intent(getActivity(), BluetoothPairActivity.class);
                i.putExtra(BluetoothFramelayoutFragment.EXTRA_KILL_ACTIVITY_ON_CANCEL, true);
                startActivity(i);
                return;
            case HUD_SETTINGS:
                FragmentHelper.pushFullScreenFragment(getFragmentManager(), HudSettingsFragment.class);
                return;
            case SCREEN_LAYOUT:
                FragmentHelper.pushFullScreenFragment(getFragmentManager(), ScreenConfigFragment.class);
                return;
            case ABOUT:
                FragmentHelper.pushFullScreenFragment(getFragmentManager(), AboutFragment.class);
                return;
            default:
                return;
        }
    }
}
