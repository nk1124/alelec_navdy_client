package com.navdy.client.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Process;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import com.localytics.android.Localytics;
import com.localytics.android.LocalyticsActivityLifecycleCallbacks;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.LocalyticsManager;
import com.navdy.client.app.framework.ProdModule;
import com.navdy.client.app.framework.i18n.I18nManager;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.util.CrashReporter;
import com.navdy.client.app.framework.util.IMMLeaks;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SupportTicketService;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.service.DataCollectionService;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.LogAppender;
import com.navdy.service.library.log.LogcatAppender;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.log.MemoryMapFileAppender;
import com.navdy.service.library.network.http.IHttpManager;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.SystemUtils;
import com.navdy.service.library.util.TaskQueue;
import dagger.ObjectGraph;
import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import javax.inject.Inject;

public class NavdyApplication extends MultiDexApplication {
    private static final String HAS_KILLED = "crashed_mapengine_kill";
    private static final String LOG_FILENAME = "PhoneLogs";
    private static final int MAX_LOG_FILES = 12;
    private static final int MAX_LOG_SIZE = 262144;
    private static final String NASTY_HERE_ANALYTICS_CLASS = "com.here.sdk.analytics.internal.a$1";
    private static final String TAG = NavdyApplication.class.getName();
    private static MemoryMapFileAppender memoryMapFileAppender = null;
    private static Context sAppContext;
    private static final Logger sLogger = new Logger(TAG);
    @Inject
    TTSAudioRouter mAudioRouter;
    @Inject
    IHttpManager mHttpManager;
    private ObjectGraph mObjectGraph;

    public static class AppTaskQueue extends TaskQueue {
        public static final int CALL_SERIAL = 4;
        public static final int DESTINATIONS_PROCESSOR_SERIAL = 13;
        public static final int GENERAL_SERIAL = 12;
        public static final int GLANCE_SERIAL = 7;
        public static final int HERE_INIT_SERIAL = 11;
        public static final int IMAGE_LOADING = 6;
        public static final int MUSIC_EVENTS = 10;
        public static final int NETWORK = 3;
        public static final int NETWORK_REACHABILITY = 5;
        public static final int OTA = 2;
        public static final int ROUTE_CALCULATION = 8;
        public static final int ZENDESK_UPLOAD = 9;
    }

    public void onCreate() {
        super.onCreate();
        sAppContext = this;
        if (!StringUtils.equalsOrBothEmptyAfterTrim(SystemUtils.getProcessName(sAppContext, Process.myPid()), getString(R.string.map_service_process_name))) {
            initApp();
            sLogger.v("Starting app on " + Build.BRAND + "," + Build.HARDWARE + "," + Build.MODEL);
            try {
                registerActivityLifecycleCallbacks(new LocalyticsActivityLifecycleCallbacks(this));
            } catch (Exception e) {
                sLogger.e("Unable to register for LocalyticsActivityLifecycleCallbacks.", e);
            }
        }
    }

    public void onTrimMemory(int level) {
        if (level == 80) {
            DataCollectionService.getInstance().handleLowMemory();
        }
        super.onTrimMemory(level);
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        long l1 = SystemClock.elapsedRealtime();
        MultiDex.install(this);
        Log.v(TAG, "Time to install Multidex: " + (SystemClock.elapsedRealtime() - l1));
    }

    public void registerActivityLifecycleCallbacks(ActivityLifecycleCallbacks callback) {
        String className = "";
        if (callback != null) {
            className = callback.getClass().getName();
        }
        sLogger.d("Activity Callback " + className);
        if (NASTY_HERE_ANALYTICS_CLASS.equals(className)) {
            sLogger.d("Preventing " + className + " as an activity callback");
        } else {
            super.registerActivityLifecycleCallbacks(callback);
        }
    }

    public static Context getAppContext() {
        return sAppContext;
    }

    @SuppressLint({"CommitPrefEdits"})
    public static void kill() {
        SettingsUtils.getSharedPreferences().edit().putInt(HAS_KILLED, 1).commit();
        Process.killProcess(Process.myPid());
    }

    private static void initLogger() {
        File internalStorageFolder = getAppContext().getFilesDir();
        String internalStorageFolderPath = "";
        if (internalStorageFolder != null && internalStorageFolder.exists()) {
            internalStorageFolderPath = internalStorageFolder.getPath();
        }
        if (StringUtils.isEmptyAfterTrim(internalStorageFolderPath)) {
            Logger.init(new LogAppender[]{new LogcatAppender()});
            return;
        }
        memoryMapFileAppender = new MemoryMapFileAppender(sAppContext, internalStorageFolderPath, LOG_FILENAME, PlaybackStateCompat.ACTION_SET_REPEAT_MODE, 12, false);
        Logger.init(new LogAppender[]{new LogcatAppender(), memoryMapFileAppender});
    }

    @NonNull
    public static ArrayList<File> getLogFiles() {
        if (memoryMapFileAppender != null) {
            return memoryMapFileAppender.getLogFiles();
        }
        return new ArrayList<>(0);
    }

    private void installCrashHandler() {
        final UncaughtExceptionHandler defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            public void uncaughtException(Thread thread, Throwable exception) {
                NavdyApplication.sLogger.e("Uncaught exception - " + thread.getName() + ":" + thread.getId() + " ui thread id:" + Looper.getMainLooper().getThread().getId(), exception);
                NavdyApplication.sLogger.i("closing logger");
                Logger.close();
                defaultHandler.uncaughtException(thread, exception);
            }
        });
    }

    private void initTaskManager() {
        sLogger.v(":initializing taskMgr");
        TaskManager taskManager = TaskManager.getInstance();
        taskManager.addTaskQueue(1, 3);
        taskManager.addTaskQueue(2, 1);
        taskManager.addTaskQueue(3, 3);
        taskManager.addTaskQueue(4, 1);
        taskManager.addTaskQueue(5, 1);
        taskManager.addTaskQueue(6, 3);
        taskManager.addTaskQueue(7, 1);
        taskManager.addTaskQueue(8, 1);
        taskManager.addTaskQueue(9, 1);
        taskManager.addTaskQueue(10, 1);
        taskManager.addTaskQueue(11, 1);
        taskManager.addTaskQueue(12, 1);
        taskManager.addTaskQueue(13, 1);
        taskManager.init();
    }

    private void registerActivityLifecycleCallbacks() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            public void onActivityCreated(Activity activity, Bundle bundle) {
            }

            public void onActivityStarted(Activity activity) {
            }

            public void onActivityResumed(Activity activity) {
                LocalyticsManager.runWhenReady(new Runnable() {
                    public void run() {
                        try {
                            Localytics.openSession();
                            Localytics.upload();
                        } catch (Throwable t) {
                            NavdyApplication.sLogger.e(t);
                        }
                    }
                });
            }

            public void onActivityPaused(Activity activity) {
                LocalyticsManager.runWhenReady(new Runnable() {
                    public void run() {
                        try {
                            Localytics.closeSession();
                            Localytics.upload();
                        } catch (Throwable t) {
                            NavdyApplication.sLogger.e(t);
                        }
                    }
                });
            }

            public void onActivityStopped(Activity activity) {
            }

            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            }

            public void onActivityDestroyed(Activity activity) {
            }
        });
    }

    private void initApp() {
        IMMLeaks.fixFocusedViewLeak(this);
        this.mObjectGraph = ObjectGraph.create(new ProdModule(this));
        this.mObjectGraph.inject(this);
        Log.v(TAG, "Starting the HttpManager");
        Log.v(TAG, "install crash handler");
        installCrashHandler();
        Log.v(TAG, "init logger");
        initLogger();
        String userId = Tracker.getUserId();
        if (!isDeveloperBuild()) {
            sLogger.v("register crashlytics");
            CrashReporter.getInstance().installCrashHandler(getAppContext(), userId);
        }
        sLogger.v("init taskmgr");
        initTaskManager();
        registerActivityLifecycleCallbacks();
        long time = SystemClock.elapsedRealtime();
        sLogger.v("init AppInstance");
        AppInstance.getInstance().initializeApp();
        sLogger.d("It took " + (SystemClock.elapsedRealtime() - time) + "ms. to initialize App.");
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                NavdyApplication.sLogger.v("init the TTS audio router");
                NavdyApplication.this.mAudioRouter.init();
                NavdyApplication.sLogger.v("init localytics");
                LocalyticsManager.getInstance().initLocalytics();
                NavdyApplication.sLogger.v("Setting crashreporter user (again) now that the Localytics engine is initialized.");
                CrashReporter.getInstance().setUser(Tracker.getUserId());
                NavdyLocationManager.getInstance();
                I18nManager.getInstance();
                NavdyApplication.this.logIfKilled();
                SettingsUtils.checkObdSettingIfBuildVersionChanged();
                if (SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.PENDING_TICKETS_EXIST, false)) {
                    SupportTicketService.submitTicketsIfConditionsAreMet();
                }
                NavdyApplication.this.initSharedPrefs();
                DataCollectionService.getInstance().startService(NavdyApplication.sAppContext);
            }
        }, 4);
    }

    private void logIfKilled() {
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        if (sharedPreferences.getInt(HAS_KILLED, 0) > 0) {
            Tracker.tagEvent(Event.HAS_KILLED);
            sharedPreferences.edit().putInt(HAS_KILLED, 0).apply();
        }
    }

    private void initSharedPrefs() {
        int prefVersion = SettingsUtils.getSharedPreferences().getInt(SettingsConstants.SHARED_PREF_VERSION, 0);
        if (prefVersion > 1) {
            SettingsUtils.onSharedPrefsDowngrade(prefVersion, 1);
        } else if (prefVersion < 1) {
            SettingsUtils.onSharedPrefsUpgrade(prefVersion, 1);
        }
    }

    public Object getSystemService(@NonNull String name) {
        if (Injector.matchesService(name)) {
            return this.mObjectGraph;
        }
        return super.getSystemService(name);
    }

    public static boolean isDeveloperBuild() {
        return false;
    }
}
