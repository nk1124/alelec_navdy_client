package com.navdy.client.app.framework;

import android.content.SharedPreferences;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.service.library.network.http.IHttpManager;
import dagger.internal.BindingsGroup;
import dagger.internal.ModuleAdapter;
import dagger.internal.ProvidesBinding;
import javax.inject.Provider;

public final class ProdModule$$ModuleAdapter extends ModuleAdapter<ProdModule> {
    private static final Class<?>[] INCLUDES = new Class[0];
    private static final String[] INJECTS = new String[]{"members/com.navdy.client.app.NavdyApplication", "members/com.navdy.client.app.ui.settings.AudioSettingsActivity", "members/com.navdy.client.app.framework.servicehandler.SpeechServiceHandler", "members/com.navdy.client.app.framework.util.TTSAudioRouter", "members/com.navdy.client.app.framework.util.CarMdClient", "members/com.navdy.client.app.framework.servicehandler.NetworkStatusManager", "members/com.navdy.client.app.framework.AppInstance", "members/com.navdy.client.app.ui.search.SearchActivity", "members/com.navdy.client.app.ui.homescreen.SuggestionsFragment", "members/com.navdy.client.app.ui.routing.RoutingActivity", "members/com.navdy.client.app.ui.favorites.FavoritesEditActivity", "members/com.navdy.client.debug.SubmitTicketFragment", "members/com.navdy.client.app.framework.servicehandler.VoiceServiceHandler", "members/com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity", "members/com.navdy.client.app.service.ActivityRecognizedCallbackService"};
    private static final Class<?>[] STATIC_INJECTIONS = new Class[0];

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideHttpManagerProvidesAdapter extends ProvidesBinding<IHttpManager> implements Provider<IHttpManager> {
        private final ProdModule module;

        public ProvideHttpManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.service.library.network.http.IHttpManager", true, "com.navdy.client.app.framework.ProdModule", "provideHttpManager");
            this.module = module;
            setLibrary(true);
        }

        public IHttpManager get() {
            return this.module.provideHttpManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideNetworkStatusManagerProvidesAdapter extends ProvidesBinding<NetworkStatusManager> implements Provider<NetworkStatusManager> {
        private final ProdModule module;

        public ProvideNetworkStatusManagerProvidesAdapter(ProdModule module) {
            super("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", true, "com.navdy.client.app.framework.ProdModule", "provideNetworkStatusManager");
            this.module = module;
            setLibrary(true);
        }

        public NetworkStatusManager get() {
            return this.module.provideNetworkStatusManager();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideSharedPreferencesProvidesAdapter extends ProvidesBinding<SharedPreferences> implements Provider<SharedPreferences> {
        private final ProdModule module;

        public ProvideSharedPreferencesProvidesAdapter(ProdModule module) {
            super("android.content.SharedPreferences", true, "com.navdy.client.app.framework.ProdModule", "provideSharedPreferences");
            this.module = module;
            setLibrary(true);
        }

        public SharedPreferences get() {
            return this.module.provideSharedPreferences();
        }
    }

    /* compiled from: ProdModule$$ModuleAdapter */
    public static final class ProvideTTSAudioRouterProvidesAdapter extends ProvidesBinding<TTSAudioRouter> implements Provider<TTSAudioRouter> {
        private final ProdModule module;

        public ProvideTTSAudioRouterProvidesAdapter(ProdModule module) {
            super("com.navdy.client.app.framework.util.TTSAudioRouter", true, "com.navdy.client.app.framework.ProdModule", "provideTTSAudioRouter");
            this.module = module;
            setLibrary(true);
        }

        public TTSAudioRouter get() {
            return this.module.provideTTSAudioRouter();
        }
    }

    public ProdModule$$ModuleAdapter() {
        super(ProdModule.class, INJECTS, STATIC_INJECTIONS, false, INCLUDES, true, true);
    }

    public void getBindings(BindingsGroup bindings, ProdModule module) {
        bindings.contributeProvidesBinding("com.navdy.service.library.network.http.IHttpManager", new ProvideHttpManagerProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.client.app.framework.util.TTSAudioRouter", new ProvideTTSAudioRouterProvidesAdapter(module));
        bindings.contributeProvidesBinding("android.content.SharedPreferences", new ProvideSharedPreferencesProvidesAdapter(module));
        bindings.contributeProvidesBinding("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", new ProvideNetworkStatusManagerProvidesAdapter(module));
    }
}
