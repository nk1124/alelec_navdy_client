package com.navdy.client.app.framework.util;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.media.IAudioService;
import android.media.IAudioService.Stub;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.speech.tts.UtteranceProgressListener;
import android.speech.tts.Voice;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.DeviceConnection.DeviceConnectedEvent;
import com.navdy.client.app.framework.DeviceConnection.DeviceDisconnectedEvent;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.servicehandler.SpeechServiceHandler;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event.Audio;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.audio.SpeechRequestStatus;
import com.navdy.service.library.events.audio.SpeechRequestStatus.SpeechRequestStatusType;
import com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason;
import com.navdy.service.library.events.hudcontrol.AccelerateShutdown.Builder;
import com.navdy.service.library.log.Logger;
import com.squareup.otto.Bus;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Inject;

public class TTSAudioRouter extends BroadcastReceiver implements OnInitListener, ServiceListener {
    private static final int CHECK_TTS_FINISHED_DELAY = 100;
    public static final String EXTRA_VOLUME_STREAM_TYPE = "android.media.EXTRA_VOLUME_STREAM_TYPE";
    public static final String EXTRA_VOLUME_STREAM_VALUE = "android.media.EXTRA_VOLUME_STREAM_VALUE";
    public static final String GOOGLE_TTS_ENGINE = "com.google.android.tts";
    private static final long HFP_CONNECTION_MAX_WAIT_TIME = 5000;
    private static final long HFP_RETRY_AFTER_FAILED_ATTEMPT = TimeUnit.MINUTES.toMillis(5);
    public static final int MEASURED_DELAY_DEFAULT = 500;
    private static final int MESSAGE_CHECK_SCO = 3;
    private static final int MESSAGE_NEW_MESSAGE = 1;
    private static final int MESSAGE_SCO_CONNECTION_ATTEMPT_TIMEOUT = 4;
    private static final int MESSAGE_SCO_CONNECTION_FAILURE_RESET = 5;
    private static final int MESSAGE_SCO_STATE_CHANGED = 0;
    private static final int MESSAGE_START_VOICE_SEARCH_SESSION = 6;
    private static final int MESSAGE_STOP_VOICE_SEARCH_SESSION = 7;
    private static final int MESSAGE_TTS_FINISHED = 2;
    public static final int MINIMUM_HFP_DELAY = 500;
    public static final int SPEECH_DELAY_LEVELS = 6;
    public static final String SPEECH_DELAY_SEQUENCE_ID_PREFIX = "NavdyTTS_SPEECH_DELAY";
    public static final String SPEECH_DELAY_SEQUENCE_PREFIX = "SPEECH_DELAY";
    public static final int STREAM_BLUETOOTH_SCO = 6;
    public static final int STREAM_MUSIC = 3;
    public static final int STREAM_SPEAKER = 4;
    public static final int TTS_OVER_SPEAKER_DELAY = 1000;
    private static final String UTTERANCE_ID = "NavdyTTS_";
    public static final String VOLUME_CHANGED_ACTION = "android.media.VOLUME_CHANGED_ACTION";
    public static final Logger sLogger = new Logger(TTSAudioRouter.class);
    private BluetoothA2dp a2dpProxy;
    private TTSAudioStatus audioStatus = new TTSAudioStatus();
    Bus bus = BusProvider.getInstance();
    private BluetoothHeadset hfpProxy;
    private boolean isVoiceSearchSessionInitializing = false;
    private boolean isVoiceSearchSessionStarted = false;
    private boolean mA2dpTurnedOff = false;
    private AudioManager mAudioManager;
    private volatile TTSInfo mCurrent;
    private HandlerThread mHandlerThread = new HandlerThread("NavdyTTSThread");
    private long mLastHfpConnectionAttemptedTime;
    AtomicBoolean mPlayThroughBluetooth = new AtomicBoolean(false);
    AtomicBoolean mPlayThroughHFP = new AtomicBoolean(false);
    private MessageHandler mRequestHandler;
    private volatile boolean mScoConnected = false;
    boolean mSpeechAvailable = false;
    private ConcurrentHashMap<String, TTSInfo> mSpeechIds = new ConcurrentHashMap();
    private ConcurrentLinkedQueue<TTSInfo> mSpeechRequests = new ConcurrentLinkedQueue();
    protected TextToSpeech mTextToSpeech;
    private float mTtsVolume;
    private boolean mTurnOffSCOCalledAfterFailure = false;
    private AtomicBoolean mTurnedScoOn = new AtomicBoolean(false);
    private volatile boolean mWaitingForScoConnection;
    private int[] measuredTTSSequenceTimings;
    private int[] newMeasuredSequenceTimings;
    private boolean playingSpeechDelaySequence;
    @Inject
    SharedPreferences preferences;
    private int preferredHFPDelay;
    private boolean scoInitialized = false;
    private SpeechServiceHandler speechServiceHandler;
    private long utteranceStartTime = 0;
    private VoiceSearchSetupListener voiceSearchSetupListener;
    private HashMap<Integer, Integer> volumeLevels = new HashMap();

    public interface VoiceSearchSetupListener {
        void onAudioInputReady(boolean z, boolean z2);
    }

    public enum AudioOutput {
        BEST_AVAILABLE,
        BLUETOOTH_MEDIA,
        PHONE_SPEAKER
    }

    private static class MessageHandler extends Handler {
        WeakReference<TTSAudioRouter> mAudioRouterReference;

        MessageHandler(TTSAudioRouter router, Looper looper) {
            super(looper);
            this.mAudioRouterReference = new WeakReference(router);
        }

        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            TTSAudioRouter router = (TTSAudioRouter) this.mAudioRouterReference.get();
            if (router != null) {
                router.handleMessage(msg.what);
            } else {
                TTSAudioRouter.sLogger.e("TTS router messages received after router is GCed ");
            }
        }
    }

    private class NavdyUtteranceProgressListener extends UtteranceProgressListener {
        private NavdyUtteranceProgressListener() {
        }

        public void onStart(String utteranceId) {
            if (!StringUtils.isEmptyAfterTrim(utteranceId) && utteranceId.startsWith(TTSAudioRouter.UTTERANCE_ID)) {
                TTSAudioRouter.sLogger.v("onStart:" + utteranceId);
                if (utteranceId.startsWith(TTSAudioRouter.SPEECH_DELAY_SEQUENCE_ID_PREFIX)) {
                    TTSAudioRouter.this.utteranceStartTime = System.currentTimeMillis();
                }
                TTSInfo info = (TTSInfo) TTSAudioRouter.this.mSpeechIds.get(utteranceId);
                if (info != null && info.sendNotification) {
                    TTSAudioRouter.this.speechServiceHandler.sendSpeechNotification(new SpeechRequestStatus(info.originalId, SpeechRequestStatusType.SPEECH_REQUEST_STARTING));
                }
            }
        }

        public void onStop(String utteranceId, boolean interrupted) {
            super.onStop(utteranceId, interrupted);
            if (!StringUtils.isEmptyAfterTrim(utteranceId) && utteranceId.startsWith(TTSAudioRouter.UTTERANCE_ID)) {
                TTSAudioRouter.this.handleTTSFinished(false, utteranceId);
            }
        }

        public void onDone(String utteranceId) {
            if (!StringUtils.isEmptyAfterTrim(utteranceId) && utteranceId.startsWith(TTSAudioRouter.UTTERANCE_ID)) {
                TTSAudioRouter.this.handleTTSFinished(true, utteranceId);
            }
        }

        public void onError(String utteranceId) {
            if (utteranceId != null && utteranceId.startsWith(TTSAudioRouter.UTTERANCE_ID)) {
                TTSInfo info = (TTSInfo) TTSAudioRouter.this.mSpeechIds.remove(utteranceId);
                TTSAudioRouter.this.mCurrent = null;
                TTSAudioRouter.sLogger.v("onError removed:" + utteranceId + " :" + (info != null ? "true" : "false"));
            }
            TTSAudioRouter.this.mRequestHandler.sendEmptyMessage(2);
        }
    }

    public static class TTSAudioStatus {
        public int currentStreamMaxVolume;
        public int currentStreamVolume;
        public String outputDeviceName;
        public boolean playingTTS;
        public int streamType;
        public boolean throughBluetooth;
        public boolean throughHFp;
        public boolean waitingForHfp;

        public TTSAudioStatus copy() {
            TTSAudioStatus ttsAudioStatus = new TTSAudioStatus();
            ttsAudioStatus.playingTTS = this.playingTTS;
            ttsAudioStatus.currentStreamVolume = this.currentStreamVolume;
            ttsAudioStatus.currentStreamMaxVolume = this.currentStreamMaxVolume;
            ttsAudioStatus.throughBluetooth = this.throughBluetooth;
            ttsAudioStatus.throughHFp = this.throughHFp;
            ttsAudioStatus.outputDeviceName = this.outputDeviceName;
            ttsAudioStatus.waitingForHfp = this.waitingForHfp;
            ttsAudioStatus.streamType = this.streamType;
            return ttsAudioStatus;
        }

        public boolean reset() {
            boolean wasPlayingTTS = this.playingTTS;
            this.playingTTS = false;
            this.waitingForHfp = false;
            this.currentStreamVolume = 0;
            this.currentStreamMaxVolume = 0;
            this.throughBluetooth = false;
            this.throughHFp = false;
            this.outputDeviceName = "";
            this.streamType = -1;
            return wasPlayingTTS;
        }
    }

    private static class TTSInfo {
        public String id;
        String originalId;
        boolean sendNotification;
        int subTextCount = 1;
        public String text;

        TTSInfo(String text, String id, String originalId, boolean sendNotification) {
            this.text = text;
            this.id = id;
            this.originalId = originalId;
            this.sendNotification = sendNotification;
        }
    }

    private static class TTSVolumeChanged {
        private TTSVolumeChanged() {
        }
    }

    public void setTtsVolume(float ttsVolume) {
        this.mTtsVolume = ttsVolume;
    }

    private void setPlayThroughBluetooth(boolean playThroughBluetooth) {
        this.mPlayThroughBluetooth.set(playThroughBluetooth);
        this.mWaitingForScoConnection = false;
    }

    private void setPlayThroughHFP(boolean playThroughHFP) {
        this.mPlayThroughHFP.set(playThroughHFP);
        this.mWaitingForScoConnection = false;
    }

    public void setAudioOutput(AudioOutput audioOutput, boolean measureVolume) {
        sLogger.d("Set Audio output " + audioOutput);
        if (audioOutput != null) {
            switch (audioOutput) {
                case BEST_AVAILABLE:
                    setPlayThroughBluetooth(true);
                    setPlayThroughHFP(true);
                    break;
                case BLUETOOTH_MEDIA:
                    setPlayThroughBluetooth(true);
                    setPlayThroughHFP(false);
                    break;
                case PHONE_SPEAKER:
                    setPlayThroughBluetooth(false);
                    setPlayThroughHFP(false);
                    break;
            }
            if (measureVolume) {
                int stream = getStream();
                sLogger.d("Stream selected " + stream);
                if (stream == 6 || (stream == 4 && isBluetoothA2dpOn())) {
                    playSilenceToMeasureVolume();
                }
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Bundle extras = intent.getExtras();
        Object obj = -1;
        switch (action.hashCode()) {
            case -1940635523:
                if (action.equals(VOLUME_CHANGED_ACTION)) {
                    obj = 1;
                    break;
                }
                break;
            case -1692127708:
                if (action.equals("android.media.ACTION_SCO_AUDIO_STATE_UPDATED")) {
                    obj = null;
                    break;
                }
                break;
            case 545516589:
                if (action.equals("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED")) {
                    int obj2 = 2;
                    break;
                }
                break;
        }
        switch (obj2) {
            case null:
                handleScoAudioStateChangedIntent(intent);
                return;
            case 1:
                handleVolumeChangedIntent(intent);
                return;
            case 2:
                sLogger.d("BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED");
                if (extras != null) {
                    int previousState = extras.getInt("android.bluetooth.profile.extra.PREVIOUS_STATE");
                    int currentState = extras.getInt("android.bluetooth.profile.extra.STATE");
                    BluetoothDevice device = (BluetoothDevice) extras.getParcelable("android.bluetooth.device.extra.DEVICE");
                    sLogger.d("BluetoothHeadset State changed Previous state :" + previousState + " , Current State : " + currentState + " , Device : " + (device != null ? device.getName() : "NONE"));
                    if ((previousState == 2 || previousState == 3) && currentState == 0) {
                        sLogger.d("HFP device disconnected");
                        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
                        if (remoteDevice != null) {
                            remoteDevice.postEvent(new Builder().reason(AccelerateReason.ACCELERATE_REASON_HFP_DISCONNECT).build());
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void handleVolumeChangedIntent(Intent intent) {
        if (intent != null) {
            int streamType = intent.getIntExtra(EXTRA_VOLUME_STREAM_TYPE, -1);
            if (streamType == 6 || streamType == 3 || streamType == 0) {
                int streamVolume = intent.getIntExtra(EXTRA_VOLUME_STREAM_VALUE, -1);
                if (streamVolume >= 0) {
                    int localStreamType = streamType;
                    if (streamType != 3) {
                        this.volumeLevels.put(Integer.valueOf(streamType), Integer.valueOf(streamVolume));
                    } else if (isBluetoothA2dpOn()) {
                        sLogger.d("A2dp is on , A2dp Speaker connected " + isA2DPSpeakerConnected());
                        this.volumeLevels.put(Integer.valueOf(3), Integer.valueOf(streamVolume));
                        localStreamType = 3;
                    } else {
                        sLogger.d("A2dp is off , A2dp Speaker connected " + isA2DPSpeakerConnected());
                        this.volumeLevels.put(Integer.valueOf(4), Integer.valueOf(streamVolume));
                        localStreamType = 4;
                    }
                    if (this.audioStatus.streamType == localStreamType) {
                        this.audioStatus.currentStreamVolume = streamVolume;
                        publishAudioStatus();
                    }
                    this.bus.post(new TTSVolumeChanged());
                }
            }
        }
    }

    private void handleScoAudioStateChangedIntent(Intent intent) {
        boolean z = true;
        if (intent.hasExtra("android.media.extra.SCO_AUDIO_STATE")) {
            int state = intent.getIntExtra("android.media.extra.SCO_AUDIO_STATE", 0);
            sLogger.d("Audio SCO state " + state);
            if (state == 0) {
                this.mTurnedScoOn.set(false);
            }
            if (this.mRequestHandler != null) {
                this.mRequestHandler.sendEmptyMessage(0);
            }
            if (state != 1) {
                z = false;
            }
            this.mScoConnected = z;
        }
    }

    public void init() {
        Injector.inject(NavdyApplication.getAppContext(), this);
        this.mAudioManager = (AudioManager) NavdyApplication.getAppContext().getSystemService("audio");
        BluetoothAdapter.getDefaultAdapter().getProfileProxy(NavdyApplication.getAppContext(), this, 2);
        BluetoothAdapter.getDefaultAdapter().getProfileProxy(NavdyApplication.getAppContext(), this, 1);
        this.bus.register(this);
        this.mTextToSpeech = new TextToSpeech(NavdyApplication.getAppContext(), this, GOOGLE_TTS_ENGINE);
        this.mTextToSpeech.setOnUtteranceProgressListener(new NavdyUtteranceProgressListener());
        this.mHandlerThread.start();
        this.mRequestHandler = new MessageHandler(this, this.mHandlerThread.getLooper());
    }

    public void close() {
        if (this.mTextToSpeech != null) {
            this.mTextToSpeech.shutdown();
            this.mTextToSpeech = null;
            this.mSpeechAvailable = false;
        }
        this.mSpeechIds.clear();
        this.mSpeechRequests.clear();
        this.mA2dpTurnedOff = false;
        NavdyApplication.getAppContext().unregisterReceiver(this);
    }

    private void persistNewMeasuredSequenceTimings() {
        int i;
        for (i = 0; i < 6; i++) {
            sLogger.d(i + " Old timing " + this.measuredTTSSequenceTimings[i] + " , New timing " + this.newMeasuredSequenceTimings[i]);
            this.measuredTTSSequenceTimings[i] = this.newMeasuredSequenceTimings[i];
        }
        Editor edit = this.preferences.edit();
        for (i = 0; i < 6; i++) {
            edit.putInt(SettingsConstants.AUDIO_HFP_DELAY_MEASURED + i, this.newMeasuredSequenceTimings[i]);
        }
        edit.apply();
    }

    public void resetToUserPreference() {
        setAudioOutput(AudioOutput.values()[this.preferences.getInt(SettingsConstants.AUDIO_OUTPUT_PREFERENCE, SettingsConstants.AUDIO_OUTPUT_PREFERENCE_DEFAULT)], false);
        this.measuredTTSSequenceTimings = new int[6];
        for (int i = 0; i < 6; i++) {
            this.measuredTTSSequenceTimings[i] = this.preferences.getInt(SettingsConstants.AUDIO_HFP_DELAY_MEASURED + i, 500);
        }
        int preferredHFPDelayLevel = this.preferences.getInt(SettingsConstants.AUDIO_HFP_DELAY_LEVEL, 1);
        if (preferredHFPDelayLevel < 0 || preferredHFPDelayLevel > 6) {
            this.preferences.edit().putInt(SettingsConstants.AUDIO_HFP_DELAY_LEVEL, 1).apply();
        }
        setPreferredHFPDelayLevel(preferredHFPDelayLevel);
        this.mTtsVolume = this.preferences.getFloat(SettingsConstants.AUDIO_TTS_VOLUME, 0.6f);
        String savedVoice = this.preferences.getString(SettingsConstants.AUDIO_VOICE, null);
        sLogger.d("Saved Voice : " + savedVoice);
        if (VERSION.SDK_INT >= 21) {
            try {
                Voice selectedVoice = this.mTextToSpeech.getVoice();
                if (!StringUtils.isEmptyAfterTrim(savedVoice)) {
                    Set<Voice> voices = this.mTextToSpeech.getVoices();
                    if (voices != null) {
                        for (Voice temp : voices) {
                            if (temp.getName().equals(savedVoice)) {
                                selectedVoice = temp;
                                break;
                            }
                        }
                    }
                    sLogger.d("Voices not available");
                }
                this.mTextToSpeech.setVoice(selectedVoice);
            } catch (Exception e) {
                sLogger.e("Text To Speech exception found on: " + Build.MANUFACTURER + " " + Build.MODEL + ", error: " + e);
            }
        }
    }

    public void processTTSRequest(String text, String id, boolean sendNotification) {
        sLogger.d("New TTS request " + text + " " + this.mSpeechAvailable);
        if (!this.mSpeechAvailable) {
            return;
        }
        if (!this.playingSpeechDelaySequence || (!StringUtils.isEmptyAfterTrim(id) && id.startsWith(SPEECH_DELAY_SEQUENCE_PREFIX))) {
            this.mRequestHandler.removeMessages(2);
            String originalId = id;
            if (!StringUtils.isEmptyAfterTrim(id)) {
                id = UTTERANCE_ID + id;
            }
            TTSInfo info = new TTSInfo(text, id, originalId, sendNotification);
            if (!StringUtils.isEmptyAfterTrim(id)) {
                this.mSpeechIds.put(id, info);
                sLogger.v("added id [" + id + "]");
            }
            this.mSpeechRequests.add(info);
            this.mRequestHandler.sendEmptyMessage(1);
        }
    }

    public void cancelTTSRequest(String id) {
        sLogger.d("Cancel TTS request " + id + " " + this.mSpeechAvailable);
        if (id != null && this.mSpeechAvailable) {
            id = UTTERANCE_ID + id;
            TTSInfo info = (TTSInfo) this.mSpeechIds.remove(id);
            if (info != null) {
                boolean ret = this.mSpeechRequests.remove(info);
                if (ret || info != this.mCurrent) {
                    sLogger.v("id still exists [" + id + "], removed=" + ret);
                    return;
                }
                sLogger.v("stopping current text to speech");
                this.mTextToSpeech.stop();
                return;
            }
            sLogger.v("id does not exists [" + id + "]");
        }
    }

    public void cancelTtsAndClearQueue() {
        this.mTextToSpeech.stop();
        this.mSpeechIds.clear();
    }

    public boolean isTTSAvailable() {
        return this.mSpeechAvailable;
    }

    public void onInit(int status) {
        this.volumeLevels.put(Integer.valueOf(6), Integer.valueOf(this.mAudioManager.getStreamVolume(6)));
        this.volumeLevels.put(Integer.valueOf(3), Integer.valueOf(this.mAudioManager.getStreamVolume(3)));
        this.volumeLevels.put(Integer.valueOf(4), Integer.valueOf(this.mAudioManager.getStreamVolume(3)));
        resetToUserPreference();
        this.mSpeechAvailable = true;
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.media.ACTION_SCO_AUDIO_STATE_UPDATED");
        filter.addAction(VOLUME_CHANGED_ACTION);
        filter.addAction("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED");
        NavdyApplication.getAppContext().registerReceiver(this, filter);
    }

    private void handleTTSFinished(boolean completed, String utteranceId) {
        String method = completed ? "onDone" : "onStop";
        if (utteranceId.startsWith(SPEECH_DELAY_SEQUENCE_ID_PREFIX)) {
            long currentTime = System.currentTimeMillis();
            int timeTaken = (int) (currentTime - this.utteranceStartTime);
            sLogger.d("Time for Speech delay sequence utterance " + utteranceId + " " + (currentTime - this.utteranceStartTime));
            int index = Integer.parseInt(utteranceId.substring(SPEECH_DELAY_SEQUENCE_ID_PREFIX.length()));
            this.newMeasuredSequenceTimings[index] = timeTaken;
            if (index == 5) {
                sLogger.d("Finished playing the HFP delay sequence");
                this.playingSpeechDelaySequence = false;
                persistNewMeasuredSequenceTimings();
            }
        }
        TTSInfo info = (TTSInfo) this.mSpeechIds.get(utteranceId);
        if (info != null) {
            if (info.subTextCount == 1) {
                this.mSpeechIds.remove(utteranceId);
                this.mCurrent = null;
                if (info.sendNotification) {
                    this.speechServiceHandler.sendSpeechNotification(new SpeechRequestStatus(info.originalId, SpeechRequestStatusType.SPEECH_REQUEST_STOPPED));
                }
            } else {
                info.subTextCount--;
                sLogger.d(method + " called for multi part text , remaining pieces " + info.subTextCount);
            }
        }
        this.mRequestHandler.sendEmptyMessage(2);
    }

    public boolean isSpeaking() {
        return this.mTextToSpeech.isSpeaking();
    }

    public void handleMessage(int what) {
        switch (what) {
            case 0:
                sLogger.d("SCO state changed Bluetooth SCO on :" + this.mAudioManager.isBluetoothScoOn() + ", SCO connected :" + this.mScoConnected);
                if (!isScoConnected() || this.scoInitialized) {
                    if (!isScoConnected()) {
                        this.scoInitialized = false;
                        break;
                    }
                }
                waitForScoInitialization();
                break;
                break;
            case 1:
                sLogger.d("New message");
                break;
            case 2:
                sLogger.d("Finished speaking");
                break;
            case 3:
                sLogger.d("Checking if SCO connection is closed, Connected : " + this.mAudioManager.isBluetoothScoOn());
                this.mAudioManager.abandonAudioFocus(null);
                break;
            case 4:
                sLogger.d("SCO connection attempt time out");
                break;
            case 5:
                sLogger.d("SCO connection failure reset");
                break;
            case 6:
                sLogger.d("Start voice search session");
                this.isVoiceSearchSessionStarted = true;
                break;
            case 7:
                sLogger.d("Stop voice search session");
                this.isVoiceSearchSessionInitializing = false;
                this.isVoiceSearchSessionStarted = false;
                break;
            default:
                return;
        }
        boolean mHFPConnectionFailed = false;
        if (this.mWaitingForScoConnection && !isScoConnected()) {
            long currentTime = SystemClock.elapsedRealtime();
            mHFPConnectionFailed = currentTime - this.mLastHfpConnectionAttemptedTime > 5000;
            if (mHFPConnectionFailed) {
                sLogger.d("HFP connection has failed, HFP setting will be ignored if set");
                this.mRequestHandler.removeMessages(4);
                if (!this.mTurnOffSCOCalledAfterFailure) {
                    Tracker.tagEvent(Audio.TTS_HFP_FAILURE);
                    sLogger.d("Stopping HFP after connection failure");
                    this.mTurnOffSCOCalledAfterFailure = true;
                    this.mAudioManager.stopBluetoothSco();
                }
                if (currentTime - this.mLastHfpConnectionAttemptedTime > HFP_RETRY_AFTER_FAILED_ATTEMPT) {
                    sLogger.d("Resetting the HFP failure flag as its time to attempt to start the HFP again");
                    this.mTurnedScoOn.set(false);
                    this.mTurnOffSCOCalledAfterFailure = false;
                    this.mLastHfpConnectionAttemptedTime = 0;
                    mHFPConnectionFailed = false;
                    this.mWaitingForScoConnection = false;
                }
            }
        }
        boolean playThroughHFP = this.mPlayThroughHFP.get();
        boolean isHFPSpeakerConnected = isHFPSpeakerConnected();
        boolean playThroughBluetooth = this.mPlayThroughBluetooth.get();
        boolean isScoAvailableOffCall = this.mAudioManager.isBluetoothScoAvailableOffCall();
        if (what == 6 || this.isVoiceSearchSessionInitializing) {
            sLogger.d("Start Voice search session : playThroughHFP :" + playThroughHFP + ", isHFPSpeakerConnected :" + isHFPSpeakerConnected + ", isSCOAvailableOffCall:" + isScoAvailableOffCall + " , Waiting for SCO :" + this.mWaitingForScoConnection);
            if (playThroughBluetooth && isHFPSpeakerConnected && isScoAvailableOffCall) {
                sLogger.d("We can use SCO for voice search session");
                if (isScoConnected()) {
                    sLogger.d("SCO connected, starting voice search session over bluetooth");
                    if (!this.scoInitialized) {
                        sLogger.d("Waiting for SCO initialization");
                        waitForScoInitialization();
                    }
                    this.mRequestHandler.removeMessages(4);
                    this.mWaitingForScoConnection = false;
                    this.isVoiceSearchSessionInitializing = false;
                    this.voiceSearchSetupListener.onAudioInputReady(true, false);
                } else {
                    sLogger.d("SCO is not connected");
                    if (mHFPConnectionFailed) {
                        sLogger.d("Use phone mic as HFP connection has failed");
                        if (this.voiceSearchSetupListener != null) {
                            sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 3, 4) == 1 ? "Granted" : "Failed"));
                            this.voiceSearchSetupListener.onAudioInputReady(false, true);
                        }
                    } else if (startHFPIfNeeded()) {
                        this.isVoiceSearchSessionInitializing = true;
                    } else {
                        if (this.voiceSearchSetupListener != null) {
                            sLogger.d("Starting voice session over phone mic as we do not have to play audio through bluetooth");
                            sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 3, 4) == 1 ? "Granted" : "Failed"));
                            this.voiceSearchSetupListener.onAudioInputReady(false, false);
                        }
                        this.isVoiceSearchSessionInitializing = false;
                    }
                }
            } else {
                if (this.voiceSearchSetupListener != null) {
                    sLogger.d("Starting voice session over phone mic as we do not have to play audio through bluetooth");
                    sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 3, 4) == 1 ? "Granted" : "Failed"));
                    this.voiceSearchSetupListener.onAudioInputReady(false, false);
                }
                this.isVoiceSearchSessionInitializing = false;
            }
        }
        if (this.mTextToSpeech.isSpeaking()) {
            sLogger.d("Still speaking");
            if (what == 2) {
                sLogger.d("We received TTS finished message but TTS Engine thinks its still speaking");
                this.mRequestHandler.sendEmptyMessageDelayed(2, 100);
            }
        } else if (this.mSpeechRequests.isEmpty()) {
            sLogger.d("Queue is Empty");
            if (what == 2 && this.audioStatus.reset()) {
                publishAudioStatus();
            }
            sLogger.d("Bluetooth SCO On :" + this.mAudioManager.isBluetoothScoOn() + ", SCO connected :" + this.mScoConnected + ", Did we turn on the SCO :" + this.mTurnedScoOn.get() + ", Did we detect HFP connection failure :" + mHFPConnectionFailed);
            if (!mHFPConnectionFailed && this.mTurnedScoOn.get() && isScoConnected() && !this.isVoiceSearchSessionStarted) {
                sLogger.d("Stopping HFP");
                this.mAudioManager.stopBluetoothSco();
                this.mRequestHandler.sendEmptyMessageDelayed(3, 500);
            }
            if (this.mA2dpTurnedOff) {
                sLogger.d("Enabling A2dp again");
                setBluetoothA2dpOn(true);
            }
            if (!this.isVoiceSearchSessionStarted) {
                sLogger.d("Audio Focus abandoned : " + this.mAudioManager.abandonAudioFocus(null));
            }
        } else {
            sLogger.d("Processing speech request : playThroughHFP :" + playThroughHFP + ", isHFPSpeakerConnected :" + isHFPSpeakerConnected + ", isSCOAvailableOffCall:" + isScoAvailableOffCall + " , Waiting for SCO :" + this.mWaitingForScoConnection);
            sLogger.d("Is Music active :" + this.mAudioManager.isMusicActive());
            sLogger.d("Is Voice search session running " + this.isVoiceSearchSessionStarted);
            if (!playThroughBluetooth || ((this.mAudioManager.isMusicActive() && !this.playingSpeechDelaySequence && !this.isVoiceSearchSessionStarted) || ((!playThroughHFP && !this.isVoiceSearchSessionStarted) || !isHFPSpeakerConnected || !isScoAvailableOffCall || mHFPConnectionFailed))) {
                if (!(playThroughBluetooth && !this.mAudioManager.isMusicActive() && playThroughHFP && isHFPSpeakerConnected && isScoAvailableOffCall)) {
                    this.mWaitingForScoConnection = false;
                }
                routeAudioThroughNonScoChannel(playThroughBluetooth);
            } else if (isScoConnected()) {
                sLogger.d("SCO is connected");
                if (!this.scoInitialized) {
                    waitForScoInitialization();
                }
                this.mRequestHandler.removeMessages(4);
                this.mWaitingForScoConnection = false;
                sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 0, 3) == 1 ? "Granted" : "Failed"));
                TTSInfo ttsInfo = (TTSInfo) this.mSpeechRequests.poll();
                if (ttsInfo != null) {
                    this.mCurrent = ttsInfo;
                    routeTTSAudioToHFP(ttsInfo.text, ttsInfo.id);
                }
            } else if (!startHFPIfNeeded()) {
                routeAudioThroughNonScoChannel(true);
            }
        }
    }

    private void routeAudioThroughNonScoChannel(boolean playThroughBluetooth) {
        sLogger.d("Bluetooth SCO On :" + this.mAudioManager.isBluetoothScoOn() + ", SCO connected :" + this.mScoConnected + ", Did we turn on the SCO :" + this.mTurnedScoOn.get());
        if (this.mAudioManager.isBluetoothScoOn() && this.mTurnedScoOn.get() && !this.isVoiceSearchSessionStarted) {
            sLogger.d("Stopping HFP");
            this.mAudioManager.setBluetoothScoOn(false);
            this.mAudioManager.stopBluetoothSco();
            this.mRequestHandler.sendEmptyMessageDelayed(3, 500);
        }
        sLogger.d("Audio Focus :" + (this.mAudioManager.requestAudioFocus(null, 3, 3) == 1 ? "Granted" : "Failed"));
        TTSInfo ttsInfo = (TTSInfo) this.mSpeechRequests.poll();
        if (ttsInfo != null) {
            this.mCurrent = ttsInfo;
            if (playThroughBluetooth || !isA2DPSpeakerConnected()) {
                if (isA2DPSpeakerConnected() && !isBluetoothA2dpOn()) {
                    setBluetoothA2dpOn(true);
                }
                routeTTSAudioToDefaultOutput(ttsInfo.text, ttsInfo.id);
                return;
            }
            routeTTSAudioToSpeaker(ttsInfo.text, ttsInfo.id);
        }
    }

    private boolean startHFPIfNeeded() {
        try {
            if (!(this.mTurnedScoOn.get() || this.mWaitingForScoConnection)) {
                sLogger.d("Starting HFP");
                this.audioStatus.waitingForHfp = true;
                this.audioStatus.throughBluetooth = true;
                this.audioStatus.outputDeviceName = getHfpDeviceName();
                this.audioStatus.currentStreamVolume = ((Integer) this.volumeLevels.get(Integer.valueOf(6))).intValue();
                this.audioStatus.currentStreamMaxVolume = this.mAudioManager.getStreamMaxVolume(6);
                publishAudioStatus();
                this.mWaitingForScoConnection = true;
                this.mLastHfpConnectionAttemptedTime = SystemClock.elapsedRealtime();
                this.mTurnedScoOn.set(true);
                this.mTurnOffSCOCalledAfterFailure = false;
                this.mAudioManager.setBluetoothScoOn(true);
                this.mAudioManager.startBluetoothSco();
                this.mRequestHandler.sendEmptyMessageDelayed(4, 5000);
            }
            return true;
        } catch (Throwable t) {
            sLogger.e("Exception while staring HFP ", t);
            this.mTurnedScoOn.set(false);
            this.mTurnOffSCOCalledAfterFailure = false;
            this.mLastHfpConnectionAttemptedTime = 0;
            this.mWaitingForScoConnection = false;
            return false;
        }
    }

    private void waitForScoInitialization() {
        sLogger.d("SCO connected, waiting for initialization");
        this.scoInitialized = true;
        try {
            Thread.sleep(this.playingSpeechDelaySequence ? 500 : (long) this.preferredHFPDelay);
        } catch (InterruptedException e) {
            sLogger.e("TTS through HFP, Sleep interrupted ", e);
        }
    }

    private void routeTTSAudioToHFP(String text, String id) {
        int scoVolume = this.mAudioManager.getStreamVolume(6);
        this.volumeLevels.put(Integer.valueOf(6), Integer.valueOf(scoVolume));
        int maxVolume = this.mAudioManager.getStreamMaxVolume(6);
        sLogger.d("Playing text through HFP , SCO volume (" + scoVolume + S3Constants.S3_FILE_DELIMITER + maxVolume + ")");
        this.audioStatus.currentStreamMaxVolume = maxVolume;
        this.audioStatus.waitingForHfp = false;
        this.audioStatus.currentStreamVolume = scoVolume;
        this.audioStatus.playingTTS = true;
        this.audioStatus.outputDeviceName = getHfpDeviceName();
        this.audioStatus.throughBluetooth = true;
        this.audioStatus.throughHFp = true;
        this.audioStatus.streamType = 6;
        publishAudioStatus();
        playTTS(0, text, id);
    }

    private void playTTS(int stream, String text, String id) {
        if (id == null) {
            id = UTTERANCE_ID;
        }
        HashMap<String, String> params = new HashMap();
        params.put("streamType", String.valueOf(stream));
        params.put("utteranceId", id);
        params.put("volume", Float.toString(this.mTtsVolume));
        int maxInputLength = TextToSpeech.getMaxSpeechInputLength();
        if (text.length() < maxInputLength) {
            this.mTextToSpeech.speak(text, 0, params);
            return;
        }
        List<String> subTexts = splitLongSpeechText(text, maxInputLength);
        int count = subTexts.size();
        TTSInfo ttsInfo = (TTSInfo) this.mSpeechIds.get(id);
        if (ttsInfo != null) {
            ttsInfo.subTextCount = count;
        }
        sLogger.d("The text is broken into " + count + " pieces");
        boolean firstOne = true;
        for (String subText : subTexts) {
            this.mTextToSpeech.speak(subText, firstOne ? 0 : 1, params);
            if (firstOne) {
                firstOne = false;
            }
        }
    }

    private void routeTTSAudioToSpeaker(String text, String id) {
        setBluetoothA2dpOn(false);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int streamVolume = this.mAudioManager.getStreamVolume(3);
        this.volumeLevels.put(Integer.valueOf(4), Integer.valueOf(streamVolume));
        int streamMaxVolume = this.mAudioManager.getStreamMaxVolume(3);
        sLogger.d("Playing text through Speaker , Volume (" + streamVolume + S3Constants.S3_FILE_DELIMITER + streamMaxVolume + ")");
        this.audioStatus.currentStreamMaxVolume = streamMaxVolume;
        this.audioStatus.currentStreamVolume = streamVolume;
        this.audioStatus.waitingForHfp = false;
        this.audioStatus.playingTTS = true;
        this.audioStatus.outputDeviceName = NavdyApplication.getAppContext().getString(R.string.phone_speaker);
        this.audioStatus.throughBluetooth = false;
        this.audioStatus.streamType = 4;
        publishAudioStatus();
        playTTS(3, text, id);
    }

    private void routeTTSAudioToDefaultOutput(String text, String id) {
        playTTS(3, text, id);
        int streamVolume = this.mAudioManager.getStreamVolume(3);
        if (isBluetoothA2dpOn()) {
            this.volumeLevels.put(Integer.valueOf(3), Integer.valueOf(streamVolume));
            this.audioStatus.streamType = 3;
        } else {
            this.volumeLevels.put(Integer.valueOf(4), Integer.valueOf(streamVolume));
            this.audioStatus.streamType = 4;
        }
        int streamMaxVolume = this.mAudioManager.getStreamMaxVolume(3);
        sLogger.d("Playing text through default Music stream , Volume (" + streamVolume + S3Constants.S3_FILE_DELIMITER + streamMaxVolume + ")");
        this.audioStatus.currentStreamMaxVolume = streamMaxVolume;
        this.audioStatus.waitingForHfp = false;
        this.audioStatus.currentStreamVolume = streamVolume;
        this.audioStatus.playingTTS = true;
        this.audioStatus.outputDeviceName = isA2DPSpeakerConnected() ? getA2DPDevice() : NavdyApplication.getAppContext().getString(R.string.phone_speaker);
        this.audioStatus.throughBluetooth = isA2DPSpeakerConnected();
        publishAudioStatus();
    }

    public void setBluetoothA2dpOn(boolean on) {
        IAudioService service = getService();
        if (service != null) {
            try {
                service.setBluetoothA2dpOn(on);
                this.mA2dpTurnedOff = !on;
                return;
            } catch (RemoteException e) {
                sLogger.d("Exception while turning off a2dp", e);
                return;
            }
        }
        sLogger.e("Unable to get instance of audio service");
    }

    public boolean isBluetoothA2dpOn() {
        if (!isA2DPSpeakerConnected()) {
            return false;
        }
        IAudioService service = getService();
        if (service != null) {
            try {
                return service.isBluetoothA2dpOn();
            } catch (RemoteException e) {
                sLogger.d("Exception while getting a2dp", e);
            }
        } else {
            sLogger.e("Unable to get instance of audio service");
            return isA2DPSpeakerConnected();
        }
    }

    public static boolean isHFPSpeakerConnected() {
        return BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(1) == 2;
    }

    public static boolean isA2DPSpeakerConnected() {
        return BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(2) == 2;
    }

    public String getA2DPDevice() {
        if (isA2DPSpeakerConnected() && this.a2dpProxy != null) {
            List<BluetoothDevice> devices = this.a2dpProxy.getConnectedDevices();
            if (devices != null && devices.size() > 0) {
                return ((BluetoothDevice) devices.get(0)).getName();
            }
        }
        return "";
    }

    public String getHfpDeviceName() {
        BluetoothDevice device = getHfpDevice();
        if (device != null) {
            return device.getName();
        }
        return "";
    }

    public BluetoothDevice getHfpDevice() {
        if (!isHFPSpeakerConnected() || this.hfpProxy == null) {
            return null;
        }
        List<BluetoothDevice> devices = this.hfpProxy.getConnectedDevices();
        if (devices == null || devices.size() == 0) {
            return null;
        }
        for (BluetoothDevice device : devices) {
            if (this.hfpProxy.isAudioConnected(device)) {
                return device;
            }
        }
        return (BluetoothDevice) devices.get(0);
    }

    public boolean isScoConnected() {
        return this.mAudioManager.isBluetoothScoOn() && this.mScoConnected;
    }

    public TextToSpeech getTextToSpeech() {
        return this.mTextToSpeech;
    }

    private IAudioService getService() {
        IAudioService iAudioService = null;
        try {
            return Stub.asInterface((IBinder) Class.forName("android.os.ServiceManager").getMethod("getService", new Class[]{String.class}).invoke(null, new Object[]{"audio"}));
        } catch (Throwable t) {
            sLogger.d("Error getting service ", t);
            return iAudioService;
        }
    }

    public void setSpeechServiceHadler(SpeechServiceHandler speechServiceHandler) {
        this.speechServiceHandler = speechServiceHandler;
    }

    public static List<String> splitLongSpeechText(String speechText, int maxInputLength) {
        if (speechText == null || maxInputLength < 2) {
            return null;
        }
        int beginningIndex = 0;
        int textLength = speechText.length();
        List<String> subTexts = new ArrayList();
        do {
            int endIndex = Math.min((beginningIndex + maxInputLength) - 1, textLength - 1);
            if (endIndex < textLength - 1) {
                while (endIndex > beginningIndex + 1) {
                    if (Character.isLowSurrogate(speechText.charAt(endIndex))) {
                        endIndex--;
                    }
                    if (Character.isSpaceChar(Character.codePointAt(speechText, endIndex))) {
                        break;
                    }
                    endIndex--;
                }
                subTexts.add(speechText.substring(beginningIndex, endIndex + 1));
                beginningIndex = endIndex + 1;
            } else {
                subTexts.add(speechText.substring(beginningIndex, endIndex + 1));
                beginningIndex = endIndex + 1;
            }
        } while (beginningIndex < textLength - 1);
        return subTexts;
    }

    public synchronized void playSpeechDelaySequence() {
        if (!this.playingSpeechDelaySequence) {
            this.newMeasuredSequenceTimings = new int[6];
            this.playingSpeechDelaySequence = true;
            cancelTtsAndClearQueue();
            for (int i = 0; i < 6; i++) {
                processTTSRequest(Integer.toString(i + 1), SPEECH_DELAY_SEQUENCE_PREFIX + i, false);
            }
        }
    }

    public void onServiceConnected(int profile, BluetoothProfile bluetoothProfile) {
        switch (profile) {
            case 1:
                this.hfpProxy = (BluetoothHeadset) bluetoothProfile;
                return;
            case 2:
                this.a2dpProxy = (BluetoothA2dp) bluetoothProfile;
                return;
            default:
                return;
        }
    }

    public void onServiceDisconnected(int profile) {
        switch (profile) {
            case 1:
                this.hfpProxy = null;
                return;
            case 2:
                this.a2dpProxy = null;
                return;
            default:
                return;
        }
    }

    public int getDelayInMillisec(int level) {
        int delay = 0;
        if (isValidDelayLevel(level)) {
            for (int i = 0; i < level; i++) {
                delay += Math.min(this.measuredTTSSequenceTimings[i], 1000);
            }
        }
        return delay;
    }

    private boolean isValidDelayLevel(int level) {
        return level >= 0 && level < 6;
    }

    public void setPreferredHFPDelayLevel(int level) {
        if (isValidDelayLevel(level)) {
            this.preferredHFPDelay = getDelayInMillisec(level);
        }
    }

    private void playSilenceToMeasureVolume() {
        processTTSRequest(" ", null, false);
    }

    public int[] getStreamVolume() {
        int intValue;
        int localStreamType = getStream();
        int[] result = new int[2];
        if (this.volumeLevels.containsKey(Integer.valueOf(localStreamType))) {
            intValue = ((Integer) this.volumeLevels.get(Integer.valueOf(localStreamType))).intValue();
        } else {
            intValue = 0;
        }
        result[0] = intValue;
        AudioManager audioManager = this.mAudioManager;
        if (localStreamType == 4) {
            localStreamType = 3;
        }
        result[1] = audioManager.getStreamMaxVolume(localStreamType);
        return result;
    }

    public int getStream() {
        int localStreamType;
        boolean playThroughBluetooth = this.mPlayThroughBluetooth.get();
        boolean playThroughPhoneCall = this.mPlayThroughHFP.get();
        boolean isMusicPlaying = this.mAudioManager.isMusicActive();
        sLogger.d("Play through bluetooth : " + playThroughBluetooth + ", playAsPhoneCall : " + playThroughPhoneCall + ", isMusicPlaying :" + isMusicPlaying);
        if (playThroughBluetooth) {
            sLogger.d("Is HFP connected : " + isHFPSpeakerConnected());
            sLogger.d("Is A2dp connected : " + isA2DPSpeakerConnected());
            if (!isMusicPlaying && playThroughPhoneCall && isHFPSpeakerConnected()) {
                localStreamType = 6;
            } else if (isBluetoothA2dpOn()) {
                localStreamType = 3;
            } else {
                localStreamType = 4;
            }
        } else {
            localStreamType = 4;
        }
        sLogger.d("getStream : stream " + localStreamType);
        return localStreamType;
    }

    @Produce
    public TTSAudioStatus produceAudioStatus() {
        TTSAudioStatus audioStatusCopy = this.audioStatus.copy();
        int[] maxStreamVolume = getStreamVolume();
        audioStatusCopy.currentStreamVolume = maxStreamVolume[0];
        audioStatusCopy.currentStreamMaxVolume = maxStreamVolume[1];
        return audioStatusCopy;
    }

    @Subscribe
    public void onHUDDisconnected(DeviceDisconnectedEvent event) {
        stopVoiceSearchSession();
    }

    @Subscribe
    public void onHUDConnected(DeviceConnectedEvent event) {
        stopVoiceSearchSession();
    }

    public void publishAudioStatus() {
        this.bus.post(this.audioStatus.copy());
    }

    public void startVoiceSearchSession() {
        if (this.mRequestHandler != null) {
            this.mRequestHandler.sendEmptyMessage(6);
        }
    }

    public void stopVoiceSearchSession() {
        if (this.mRequestHandler != null) {
            this.mRequestHandler.sendEmptyMessage(7);
        }
    }

    public void setVoiceSearchSetupListener(VoiceSearchSetupListener voiceSearchSetupListener) {
        this.voiceSearchSetupListener = voiceSearchSetupListener;
    }
}
