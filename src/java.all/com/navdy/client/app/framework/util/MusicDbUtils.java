package com.navdy.client.app.framework.util;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore.Audio.Albums;
import android.provider.MediaStore.Audio.Artists;
import android.provider.MediaStore.Audio.Media;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.util.HashSet;
import java.util.Set;

public class MusicDbUtils {
    private static final String CHARACTER_MAP_SELECTION = "1=1) GROUP BY (letter";
    public static final String CHARACTER_MAP_SORT = "CASE\n  WHEN letter GLOB '[A-Z]' THEN 0\n  WHEN letter GLOB '[0-9]' THEN 1\n  ELSE 2\nEND ASC";
    private static final String COUNT = "count(*) AS count";
    public static final String COUNT_COLUMN = "count";
    private static final String FIRST_LETTER_FORMAT = "SUBSTR(UPPER(%s), 1, 1) AS letter";
    private static final String GPM_PLAYLISTS_BASE_URI = "content://com.google.android.music.MusicContent/playlists";
    public static final String GPM_PLAYLISTS_COLUMN_PLAYLIST_NAME = "playlist_name";
    private static final String GPM_PLAYLISTS_MEMBERS_PATH = "members";
    public static final String GPM_PLAYLIST_MEMBERS_COLUMN_ALBUM = "album";
    public static final String GPM_PLAYLIST_MEMBERS_COLUMN_ARTIST = "artist";
    public static final String GPM_PLAYLIST_MEMBERS_COLUMN_SOURCE_ID = "SourceId";
    public static final String GPM_PLAYLIST_MEMBERS_COLUMN_TITLE = "title";
    public static final String LETTER_COLUMN = "letter";
    public static final String ORDER_FORMAT = "CASE%n  WHEN UPPER(%1$s) GLOB '[A-Z]*' THEN 0%n  WHEN UPPER(%1$s) GLOB '[0-9]*' THEN 1%n  ELSE 2%nEND ASC, UPPER(%1$s);";
    private static Logger logger = new Logger(MusicDbUtils.class);

    public static Cursor getPlaylistsCursor() {
        return NavdyContentProvider.getPlaylistsCursor();
    }

    public static Cursor getPlaylistMembersCursor(String playlistIdStr) {
        try {
            return getPlaylistMembersCursor(Integer.parseInt(playlistIdStr));
        } catch (NumberFormatException e) {
            logger.e("Invalid collection ID: " + playlistIdStr);
            return null;
        }
    }

    public static Cursor getPlaylistMembersCursor(int playlistId) {
        return NavdyContentProvider.getPlaylistMembersCursor(playlistId);
    }

    public static Cursor getAlbumsCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(Albums.EXTERNAL_CONTENT_URI, new String[]{"_id", "album", "numsongs", "artist"}, null, null, getOrderString("album"));
    }

    public static Cursor getAlbumMembersCursor(String albumId) {
        return NavdyApplication.getAppContext().getContentResolver().query(Media.EXTERNAL_CONTENT_URI, new String[]{"title", "artist", "album", "_id"}, "album_id = ?", new String[]{albumId}, null);
    }

    public static Cursor getArtistsCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(Artists.EXTERNAL_CONTENT_URI, new String[]{"_id", "artist"}, null, null, getOrderString("artist"));
    }

    public static Cursor getArtistMembersCursor(String artistId) {
        return NavdyApplication.getAppContext().getContentResolver().query(Media.EXTERNAL_CONTENT_URI, new String[]{"title", "artist", "album", "_id"}, "artist_id = ?", new String[]{artistId}, null);
    }

    public static int getArtistSongsCount(String artistId) {
        int count = -1;
        try {
            Cursor cursor = NavdyApplication.getAppContext().getContentResolver().query(Media.EXTERNAL_CONTENT_URI, new String[]{COUNT}, "artist_id = ?", new String[]{artistId}, null);
            if (cursor != null && cursor.moveToFirst()) {
                count = cursor.getInt(cursor.getColumnIndex(COUNT_COLUMN));
            }
            IOUtils.closeObject(cursor);
            return count;
        } catch (Throwable th) {
            IOUtils.closeObject(null);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static Cursor getArtistsAlbums(String artistId) {
        ContentResolver contentResolver = NavdyApplication.getAppContext().getContentResolver();
        Uri membersUri = Media.EXTERNAL_CONTENT_URI;
        String[] membersColumns = new String[]{"DISTINCT album_id"};
        String query = "artist_id = ?";
        String[] args = new String[]{artistId};
        Cursor tracksCursor = null;
        Set<Integer> albumIds = new HashSet();
        try {
            tracksCursor = contentResolver.query(membersUri, membersColumns, query, args, null);
            StringBuilder queryBuilder;
            int count;
            int i;
            if (tracksCursor == null || !tracksCursor.moveToFirst()) {
                if (tracksCursor != null) {
                    tracksCursor.close();
                }
                queryBuilder = new StringBuilder();
                queryBuilder.append("_id IN (");
                count = albumIds.size();
                i = 0;
                args = new String[count];
                for (Integer num : albumIds) {
                    int i2 = i + 1;
                    args[i] = num.toString();
                    if (i2 <= count - 1) {
                        queryBuilder.append("?,");
                        i = i2;
                    } else if (i2 == count) {
                        queryBuilder.append("?)");
                        i = i2;
                    } else {
                        i = i2;
                    }
                }
                query = queryBuilder.toString();
                return contentResolver.query(Albums.EXTERNAL_CONTENT_URI, new String[]{"_id", "album", "numsongs", "artist"}, query, args, getOrderString("album"));
            }
            do {
                albumIds.add(Integer.valueOf(tracksCursor.getInt(tracksCursor.getColumnIndex("album_id"))));
            } while (tracksCursor.moveToNext());
            if (tracksCursor != null) {
            }
            queryBuilder = new StringBuilder();
            queryBuilder.append("_id IN (");
            count = albumIds.size();
            i = 0;
            args = new String[count];
            while (albumIdIterator.hasNext()) {
            }
            query = queryBuilder.toString();
            return contentResolver.query(Albums.EXTERNAL_CONTENT_URI, new String[]{"_id", "album", "numsongs", "artist"}, query, args, getOrderString("album"));
        } catch (Throwable th) {
            if (tracksCursor != null) {
                tracksCursor.close();
            }
        }
    }

    public static Cursor getPlaylistsCharacterMapCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(NavdyContentProviderConstants.PLAYLISTS_CONTENT_URI, getCharacterMapProjection("playlist_name"), CHARACTER_MAP_SELECTION, null, CHARACTER_MAP_SORT);
    }

    public static Cursor getAlbumsCharacterMapCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(Albums.EXTERNAL_CONTENT_URI, getCharacterMapProjection("album"), CHARACTER_MAP_SELECTION, null, CHARACTER_MAP_SORT);
    }

    public static Cursor getArtistsCharacterMapCursor() {
        return NavdyApplication.getAppContext().getContentResolver().query(Artists.EXTERNAL_CONTENT_URI, getCharacterMapProjection("artist"), CHARACTER_MAP_SELECTION, null, CHARACTER_MAP_SORT);
    }

    public static Uri getGpmPlaylistsUri() {
        return Uri.parse(GPM_PLAYLISTS_BASE_URI);
    }

    private static Uri getGpmPlaylistUri(int playlistId) {
        return Uri.parse("content://com.google.android.music.MusicContent/playlists/" + playlistId);
    }

    private static Uri getGpmPlaylistMembersUri(int playlistId) {
        return Uri.parse("content://com.google.android.music.MusicContent/playlists/" + playlistId + S3Constants.S3_FILE_DELIMITER + GPM_PLAYLISTS_MEMBERS_PATH);
    }

    public static Cursor getGpmPlaylistsCursor() {
        Cursor cursor = null;
        try {
            return NavdyApplication.getAppContext().getContentResolver().query(getGpmPlaylistsUri(), new String[]{"_id", "playlist_name"}, null, null, "playlist_name");
        } catch (Throwable t) {
            logger.e("Couldn't get GPM playlists cursor: " + t);
            return cursor;
        }
    }

    public static Cursor getGpmPlaylistCursor(int playlistId) {
        Cursor cursor = null;
        try {
            return NavdyApplication.getAppContext().getContentResolver().query(getGpmPlaylistUri(playlistId), new String[]{"playlist_name"}, null, null, null);
        } catch (Throwable t) {
            logger.e("Couldn't get GPM playlist cursor for playlist " + playlistId + CrashlyticsAppender.SEPARATOR + t);
            return cursor;
        }
    }

    public static Cursor getGpmPlaylistMembersCursor(int playlistId) {
        Cursor cursor = null;
        try {
            return NavdyApplication.getAppContext().getContentResolver().query(getGpmPlaylistMembersUri(playlistId), new String[]{"title", "artist", "album", "SourceId"}, null, null, null);
        } catch (Throwable t) {
            logger.e("Couldn't get GPM playlist members cursor for playlist " + playlistId + CrashlyticsAppender.SEPARATOR + t);
            return cursor;
        }
    }

    public static String getOrderString(String nameColumn) {
        return String.format(ORDER_FORMAT, new Object[]{nameColumn});
    }

    public static String[] getCharacterMapProjection(String nameColumn) {
        String[] strArr = new String[2];
        strArr[0] = COUNT;
        strArr[1] = String.format(FIRST_LETTER_FORMAT, new Object[]{nameColumn});
        return strArr;
    }
}
