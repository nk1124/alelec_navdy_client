package com.navdy.client.app.framework.util;

import android.database.ContentObserver;
import android.os.Handler;
import com.navdy.client.app.framework.servicehandler.ContactServiceHandler;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.service.library.events.contacts.FavoriteContactsRequest.Builder;
import com.navdy.service.library.log.Logger;
import java.util.concurrent.TimeUnit;

public class ContactObserver extends ContentObserver {
    private static final long CHECK_GAINED_CONTACTS_PERMISSION_INTERVAL = TimeUnit.MINUTES.toMillis(1);
    private static final long REFRESH_DELAY = TimeUnit.SECONDS.toMillis(15);
    private static final Logger logger = new Logger(ContactObserver.class);
    private ContactServiceHandler contactServiceHandler;
    private Handler handler;
    private Runnable refreshContacts = new Runnable() {
        public void run() {
            ContactObserver.logger.v("sending fresh contact info to Display");
            ContactObserver.this.contactServiceHandler.onFavoriteContactsRequest(new Builder().maxContacts(Integer.valueOf(30)).build());
        }
    };

    public ContactObserver(ContactServiceHandler contactServiceHandler, final Handler handler) {
        super(handler);
        this.handler = handler;
        this.contactServiceHandler = contactServiceHandler;
        boolean hasContactPermission = BaseActivity.weHaveContactsPermission();
        logger.v("init hasContactPermission=" + hasContactPermission);
        if (!hasContactPermission) {
            handler.post(new Runnable() {
                public void run() {
                    if (BaseActivity.weHaveContactsPermission()) {
                        ContactObserver.logger.i("has gained contacts permission");
                        handler.post(ContactObserver.this.refreshContacts);
                        return;
                    }
                    handler.postDelayed(this, ContactObserver.CHECK_GAINED_CONTACTS_PERMISSION_INTERVAL);
                }
            });
        }
    }

    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        logger.v("onChange to Contacts");
        refreshContactsWithDebounce();
    }

    private void refreshContactsWithDebounce() {
        this.handler.removeCallbacks(this.refreshContacts);
        this.handler.postDelayed(this.refreshContacts, REFRESH_DELAY);
    }
}
