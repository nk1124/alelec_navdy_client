package com.navdy.client.app.framework.util;

import android.net.Uri;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader.Builder;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import java.util.Date;
import java.util.UUID;

public class ZendeskJWT {
    public static final String EMAIL = "zendesk-sso@navdy.com";
    public static final String NAME = "Navdy";
    public static final String SSO_URL = "https://navdy.zendesk.com/access/jwt?jwt=";

    public static Uri getZendeskUri() {
        JWSObject jwsObject = new JWSObject(new Builder(JWSAlgorithm.HS256).contentType("text/plain").build(), new Payload(new JWTClaimsSet.Builder().issueTime(new Date()).jwtID(UUID.randomUUID().toString()).claim("name", "Navdy").claim("email", EMAIL).build().toJSONObject()));
        try {
            jwsObject.sign(new MACSigner(NavdyApplication.getAppContext().getString(R.string.zendesk_jwt_secret).getBytes()));
        } catch (JOSEException e) {
            e.printStackTrace();
        }
        return Uri.parse(SSO_URL + jwsObject.serialize());
    }
}
