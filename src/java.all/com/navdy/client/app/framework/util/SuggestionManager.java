package com.navdy.client.app.framework.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Pair;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.CalendarEvent;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.models.Suggestion.SuggestionType;
import com.navdy.client.app.framework.models.Trip;
import com.navdy.client.app.framework.navigation.HereRouteManager;
import com.navdy.client.app.framework.navigation.HereRouteManager.Error;
import com.navdy.client.app.framework.navigation.HereRouteManager.Listener;
import com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.suggestion.DestinationSuggestionService;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.client.app.ui.homescreen.CalendarUtils.CalendarEventReader;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.debug.util.FormatUtils;
import com.navdy.client.ota.OTAUpdateService.State;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class SuggestionManager {
    private static final int MAX_CALENDAR_SUGGESTIONS = 2;
    private static final long MAX_EARLY_FOR_CAL_SUGG = TimeUnit.HOURS.toMillis(1);
    private static final int MAX_RECENT_SUGGESTIONS = 23;
    private static final int MAX_RECOMMENDATIONS = 1;
    private static final float SUGGESTION_DISTANCE_LIMIT = 200.0f;
    private static ArrayList<Suggestion> cachedHistory = new ArrayList(20);
    private static ArrayList<Suggestion> cachedImportantTips = new ArrayList();
    private static ArrayList<Suggestion> cachedRecommendations = new ArrayList();
    private static ArrayList<Suggestion> cachedUnimportantTips = new ArrayList();
    private static ArrayList<Suggestion> history = new ArrayList(20);
    private static ArrayList<Suggestion> importantTips = new ArrayList();
    private static AtomicBoolean isBuildingSuggestionList = new AtomicBoolean(false);
    private static long lastListGeneration = 0;
    private static Location lastUserLocation = new Location("");
    private static ArrayList<WeakReference<SuggestionBuildListener>> listeners = new ArrayList();
    private static final Logger logger = new Logger(SuggestionManager.class);
    private static final NavdyLocationManager navdyLocationManager = NavdyLocationManager.getInstance();
    private static Suggestion ota = null;
    private static ArrayList<Suggestion> recommendations = new ArrayList();
    private static Suggestion trip = null;
    private static ArrayList<Suggestion> unimportantTips = new ArrayList();

    public interface SuggestionBuildListener {
        @WorkerThread
        void onSuggestionBuildComplete(ArrayList<Suggestion> arrayList);
    }

    public static synchronized void addListener(@NonNull WeakReference<SuggestionBuildListener> listener) {
        synchronized (SuggestionManager.class) {
            listeners.add(listener);
        }
    }

    public static synchronized void removeListener(@NonNull WeakReference<SuggestionBuildListener> listener) {
        synchronized (SuggestionManager.class) {
            listeners.remove(listener);
        }
    }

    private static synchronized void notifyListenersIfAny(ArrayList<Suggestion> suggestions) {
        synchronized (SuggestionManager.class) {
            if (listeners != null) {
                Iterator it = listeners.iterator();
                while (it.hasNext()) {
                    SuggestionBuildListener suggestionBuildListener = (SuggestionBuildListener) ((WeakReference) it.next()).get();
                    if (suggestionBuildListener != null) {
                        suggestionBuildListener.onSuggestionBuildComplete(suggestions);
                    }
                }
            }
        }
    }

    @MainThread
    public static synchronized void setTripAsync(@Nullable final Suggestion newTrip) {
        synchronized (SuggestionManager.class) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    SuggestionManager.setTrip(newTrip);
                }
            }, 1);
        }
    }

    @WorkerThread
    private static synchronized void setTrip(@Nullable Suggestion newTrip) {
        synchronized (SuggestionManager.class) {
            if ((trip != null && newTrip == null) || ((trip == null && newTrip != null) || !(trip == null || trip.equals(newTrip)))) {
                trip = newTrip;
                notifyListenersIfAny(getSuggestions());
            }
        }
    }

    public static Suggestion getTrip() {
        return trip;
    }

    private static synchronized void setRecommendations(ArrayList<Suggestion> recommendations) {
        synchronized (SuggestionManager.class) {
            recommendations = recommendations;
        }
    }

    public static synchronized void forceSuggestionFullRefresh() {
        synchronized (SuggestionManager.class) {
            lastListGeneration = 0;
            lastUserLocation = new Location("");
            CalendarUtils.forceCalendarRefresh();
        }
    }

    private static synchronized void insertOtaTip() {
        synchronized (SuggestionManager.class) {
            if (ota == null) {
                Destination destination = new Destination();
                Context context = NavdyApplication.getAppContext();
                destination.name = context.getString(R.string.sw_update_available);
                destination.rawAddressNotForDisplay = context.getString(R.string.tap_to_download);
                ota = new Suggestion(destination, SuggestionType.OTA);
            }
        }
    }

    private static synchronized void removeOtaTip() {
        synchronized (SuggestionManager.class) {
            ota = null;
        }
    }

    @WorkerThread
    public static synchronized void updateOtaStatus(State state) {
        synchronized (SuggestionManager.class) {
            SystemUtils.ensureNotOnMainThread();
            boolean somethingChanged = false;
            boolean otaWasHere = ota != null;
            if (state == State.UPDATE_AVAILABLE) {
                insertOtaTip();
                somethingChanged = true;
            } else if (otaWasHere) {
                removeOtaTip();
                somethingChanged = true;
            }
            if (somethingChanged) {
                notifyListenersIfAny(getSuggestions());
            }
        }
    }

    @NonNull
    public static synchronized ArrayList<Suggestion> getSuggestions() {
        ArrayList<Suggestion> suggestions;
        synchronized (SuggestionManager.class) {
            int successCount;
            Iterator it;
            suggestions = new ArrayList(23);
            if (trip != null) {
                suggestions.add(trip);
            }
            if (cachedImportantTips.size() > 0) {
                addThisHeader(R.string.tip, suggestions);
                suggestions.add(cachedImportantTips.get(0));
            }
            if (trip == null && cachedRecommendations.size() > 0) {
                Suggestion recommendationHeader = buildHeaderFor(R.string.suggested_places);
                suggestions.add(recommendationHeader);
                successCount = 0;
                it = cachedRecommendations.iterator();
                while (it.hasNext()) {
                    if (addToSuggestions((Suggestion) it.next(), suggestions)) {
                        successCount++;
                    }
                }
                if (successCount <= 0) {
                    suggestions.remove(recommendationHeader);
                }
            }
            if (cachedImportantTips.size() <= 0) {
                if (cachedUnimportantTips.size() > 0) {
                    addThisHeader(R.string.tip, suggestions);
                    suggestions.add(cachedUnimportantTips.get(0));
                } else if (ota != null) {
                    suggestions.add(ota);
                }
            }
            if (cachedHistory.size() > 0) {
                Suggestion historyHeader = buildHeaderFor(R.string.history);
                suggestions.add(historyHeader);
                successCount = 0;
                it = cachedHistory.iterator();
                while (it.hasNext()) {
                    Suggestion s = (Suggestion) it.next();
                    if (successCount >= 20) {
                        break;
                    } else if (addToSuggestionsOnlyCheckForDuplicates(s, suggestions)) {
                        successCount++;
                    }
                }
                if (successCount <= 0) {
                    suggestions.remove(historyHeader);
                }
            }
            if (suggestions.size() <= 0) {
                suggestions = getEmptySuggestionState();
            }
        }
        return suggestions;
    }

    @MainThread
    public static ArrayList<Suggestion> getEmptySuggestionState() {
        ArrayList<Suggestion> suggestions = new ArrayList();
        Suggestion trip = getTripSuggestion();
        if (trip != null) {
            suggestions.add(trip);
        } else {
            addThisHeader(R.string.space, suggestions);
        }
        suggestions.add(new Suggestion(null, SuggestionType.LOADING));
        return suggestions;
    }

    private static Suggestion getAddFavoriteSuggestion() {
        Destination destination = new Destination();
        Context context = NavdyApplication.getAppContext();
        destination.name = context.getString(R.string.add_favorite);
        destination.rawAddressNotForDisplay = context.getString(R.string.tap_to_add_new_favorite);
        return new Suggestion(destination, SuggestionType.ADD_FAVORITE);
    }

    public static void rebuildSuggestionListAndSendToHudAsync() {
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                SuggestionManager.buildSuggestionList(true);
                return null;
            }
        }.execute(new Void[0]);
    }

    @WorkerThread
    public static synchronized void buildSuggestionList(final boolean sendToHud) {
        synchronized (SuggestionManager.class) {
            SystemUtils.ensureNotOnMainThread();
            logger.v("Building suggestion list.");
            if (isBuildingSuggestionList.get()) {
                logger.v("Already in the process of building the suggestion list. Ignoring request.");
            } else {
                isBuildingSuggestionList.set(true);
                trip = null;
                importantTips = new ArrayList();
                unimportantTips = new ArrayList();
                history = new ArrayList(20);
                cachedImportantTips = new ArrayList();
                cachedRecommendations = new ArrayList();
                cachedUnimportantTips = new ArrayList();
                cachedHistory = new ArrayList(20);
                SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
                trip = getTripSuggestion();
                buildImportantTipsSuggestions(sharedPreferences);
                buildUnimportantTipsSuggestions(sharedPreferences);
                buildHistorySuggestions();
                buildCalendarAndMachineLearnedSuggestions(new SuggestionBuildListener() {
                    public void onSuggestionBuildComplete(ArrayList<Suggestion> recommendations) {
                        SuggestionManager.setRecommendations(recommendations);
                        SuggestionManager.finishSuggestionBuild(sendToHud);
                    }
                });
            }
        }
    }

    private static Suggestion getTripSuggestion() {
        NavdyRouteHandler navdyRouteHandler = NavdyRouteHandler.getInstance();
        if (navdyRouteHandler.isInOneOfTheActiveTripStates()) {
            return new Suggestion(navdyRouteHandler.getCurrentDestination(), SuggestionType.ACTIVE_TRIP);
        }
        if (navdyRouteHandler.isInOneOfThePendingTripStates()) {
            return new Suggestion(navdyRouteHandler.getCurrentDestination(), SuggestionType.PENDING_TRIP);
        }
        return null;
    }

    private static void buildImportantTipsSuggestions(SharedPreferences sharedPreferences) {
        Destination destination;
        Context context = NavdyApplication.getAppContext();
        if (!(sharedPreferences == null || sharedPreferences.getBoolean(SettingsConstants.USER_WATCHED_THE_DEMO, false))) {
            destination = new Destination();
            destination.name = context.getString(R.string.learn_how);
            destination.rawAddressNotForDisplay = context.getString(R.string.watch_feature_video);
            importantTips.add(new Suggestion(destination, SuggestionType.DEMO_VIDEO));
        }
        if (!(sharedPreferences == null || !sharedPreferences.getBoolean(SettingsConstants.HUD_GESTURE, true) || sharedPreferences.getBoolean(SettingsConstants.USER_TRIED_GESTURES_ONCE_BEFORE, false))) {
            destination = new Destination();
            destination.name = context.getString(R.string.learning_gestures);
            destination.rawAddressNotForDisplay = context.getString(R.string.learning_gestures_subtitle);
            importantTips.add(new Suggestion(destination, SuggestionType.TRY_GESTURES));
        }
        if (!(sharedPreferences == null || sharedPreferences.getBoolean(SettingsConstants.USER_ALREADY_SAW_MICROPHONE_TIP, false) || !sharedPreferences.getBoolean(SettingsConstants.HUD_VOICE_SEARCH_CAPABLE, false))) {
            if (BaseActivity.weHaveMicrophonePermission()) {
                destination = new Destination();
                destination.name = context.getString(R.string.voice_search);
                destination.rawAddressNotForDisplay = context.getString(R.string.voice_search_description);
                importantTips.add(new Suggestion(destination, SuggestionType.VOICE_SEARCH));
            } else {
                destination = new Destination();
                destination.name = context.getString(R.string.enable_microphone);
                destination.rawAddressNotForDisplay = context.getString(R.string.enable_microphone_description);
                importantTips.add(new Suggestion(destination, SuggestionType.ENABLE_MICROPHONE));
            }
        }
        if (!(sharedPreferences == null || sharedPreferences.getBoolean(SettingsConstants.USER_ALREADY_SAW_GOOGLE_NOW_TIP, false))) {
            destination = new Destination();
            destination.name = context.getString(R.string.google_now_tip);
            destination.rawAddressNotForDisplay = context.getString(R.string.google_now_tip_subtitle);
            importantTips.add(new Suggestion(destination, SuggestionType.GOOGLE_NOW));
        }
        if (!(sharedPreferences == null || sharedPreferences.getBoolean(SettingsConstants.USER_ALREADY_SAW_LOCAL_MUSIC_BROWSER_TIP, false) || !sharedPreferences.getBoolean(SettingsConstants.HUD_LOCAL_MUSIC_BROWSER_CAPABLE, false))) {
            destination = new Destination();
            destination.name = context.getString(R.string.hud_local_music_browser_tip);
            destination.rawAddressNotForDisplay = context.getString(R.string.hud_local_music_browser_tip_subtitle);
            importantTips.add(new Suggestion(destination, SuggestionType.HUD_LOCAL_MUSIC_BROWSER));
        }
        if (sharedPreferences != null && !sharedPreferences.getBoolean(SettingsConstants.GLANCES, false) && !sharedPreferences.getBoolean(SettingsConstants.USER_ENABLED_GLANCES_ONCE_BEFORE, false)) {
            destination = new Destination();
            destination.name = context.getString(R.string.enable_glances);
            destination.rawAddressNotForDisplay = context.getString(R.string.enable_glances_description);
            importantTips.add(new Suggestion(destination, SuggestionType.ENABLE_GLANCES));
        }
    }

    @WorkerThread
    private static void buildUnimportantTipsSuggestions(SharedPreferences sharedPreferences) {
        Context context = NavdyApplication.getAppContext();
        if (sharedPreferences != null) {
            Destination destination;
            if (!(sharedPreferences.getBoolean(SettingsConstants.HUD_GESTURE, true) || sharedPreferences.getBoolean(SettingsConstants.USER_ALREADY_SAW_GESTURE_TIP, false))) {
                destination = new Destination();
                destination.name = context.getString(R.string.enable_gestures);
                destination.rawAddressNotForDisplay = context.getString(R.string.enable_gestures_description);
                unimportantTips.add(new Suggestion(destination, SuggestionType.ENABLE_GESTURES));
            }
            if (NavdyContentProvider.getHome() == null && !sharedPreferences.getBoolean(SettingsConstants.USER_ALREADY_SAW_ADD_HOME_TIP, false)) {
                destination = new Destination();
                destination.name = context.getString(R.string.add_home);
                destination.rawAddressNotForDisplay = context.getString(R.string.tap_to_set_home_address);
                unimportantTips.add(new Suggestion(destination, SuggestionType.ADD_HOME));
            }
            if (NavdyContentProvider.getWork() == null && !sharedPreferences.getBoolean(SettingsConstants.USER_ALREADY_SAW_ADD_WORK_TIP, false)) {
                destination = new Destination();
                destination.name = context.getString(R.string.add_work);
                destination.rawAddressNotForDisplay = context.getString(R.string.tap_to_set_work_address);
                unimportantTips.add(new Suggestion(destination, SuggestionType.ADD_WORK));
            }
        }
        try {
            Cursor favoritesCursor = NavdyContentProvider.getFavoritesCursor(new Pair("is_special != ? AND is_special != ?", new String[]{String.valueOf(-3), String.valueOf(-2)}));
            if (favoritesCursor != null && favoritesCursor.getCount() <= 0) {
                unimportantTips.add(getAddFavoriteSuggestion());
            }
            IOUtils.closeStream(favoritesCursor);
        } catch (Throwable th) {
            IOUtils.closeStream(null);
        }
    }

    private static void buildHistorySuggestions() {
        assignDestinationIdsToTripsWithKnownDestinations();
        try {
            Cursor c = NavdyContentProvider.getRecentsCursor(new Pair("do_not_suggest != 1", null));
            if (c != null && c.moveToFirst()) {
                do {
                    history.add(makeSuggestion(NavdyContentProvider.getDestinationItemAt(c, c.getPosition()), SuggestionType.RECENT));
                    if (history.size() >= 23) {
                        break;
                    }
                } while (c.moveToNext());
            }
            IOUtils.closeStream(c);
        } catch (Throwable th) {
            IOUtils.closeStream(null);
        }
    }

    private static void assignDestinationIdsToTripsWithKnownDestinations() {
        ArrayList<Trip> trips = new ArrayList();
        try {
            Cursor c = NavdyContentProvider.getTripsCursor(new Pair("destination_id == 0 AND end_lat != 0 AND end_long != 0", null));
            if (c != null && c.moveToFirst()) {
                for (int i = 0; i < c.getCount(); i++) {
                    trips.add(NavdyContentProvider.getTripsItemAt(c, i));
                }
            }
            IOUtils.closeStream(c);
            Iterator it = trips.iterator();
            while (it.hasNext()) {
                Trip trip = (Trip) it.next();
                Cursor destinationCursor = null;
                try {
                    destinationCursor = NavdyContentProvider.getDestinationCursor(Destination.getCoordinateBoundingBoxSelectionClause(trip.endLat, trip.endLong));
                    if (destinationCursor != null && destinationCursor.moveToFirst()) {
                        Destination closestDestination = null;
                        float shortestDistance = -1.0f;
                        while (!destinationCursor.isAfterLast()) {
                            Destination d = NavdyContentProvider.getDestinationItemAt(destinationCursor, destinationCursor.getPosition());
                            Location loc1 = new Location("");
                            Location loc2 = new Location("");
                            float distance = -1.0f;
                            loc1.setLatitude(trip.endLat);
                            loc1.setLongitude(trip.endLong);
                            if (d.navigationLat != 0.0d || d.navigationLong != 0.0d) {
                                loc2.setLatitude(d.navigationLat);
                                loc2.setLongitude(d.navigationLong);
                                distance = loc1.distanceTo(loc2);
                            } else if (!(d.displayLat == 0.0d && d.displayLong == 0.0d)) {
                                loc2.setLatitude(d.displayLat);
                                loc2.setLongitude(d.displayLong);
                                distance = loc1.distanceTo(loc2);
                            }
                            if (distance < SUGGESTION_DISTANCE_LIMIT && (closestDestination == null || (distance != -1.0f && distance < shortestDistance))) {
                                closestDestination = d;
                                shortestDistance = distance;
                            }
                            destinationCursor.moveToNext();
                        }
                        if (closestDestination != null) {
                            trip.setDestinationIdAndSaveToDb(closestDestination.id);
                            if (closestDestination.lastRoutedDate > trip.startTime) {
                                closestDestination.lastRoutedDate = trip.startTime;
                                closestDestination.updateLastRoutedDateInDb();
                            }
                        }
                    }
                    IOUtils.closeStream(destinationCursor);
                } catch (Throwable th) {
                    IOUtils.closeStream(destinationCursor);
                }
            }
        } catch (Throwable th2) {
            IOUtils.closeStream(null);
        }
    }

    private static void buildCalendarAndMachineLearnedSuggestions(final SuggestionBuildListener callback) {
        long now = new Date().getTime();
        Location currentPhoneLocation = NavdyLocationManager.getInstance().getSmartStartLocation();
        if (now - lastListGeneration >= CalendarUtils.EVENTS_STALENESS_LIMIT || !isCloseToLastRecommendation(currentPhoneLocation)) {
            lastListGeneration = now;
            lastUserLocation = currentPhoneLocation;
            logger.d("Building new Calendar events and Machine learned recommendation");
            buildCalendarSuggestions(new SuggestionBuildListener() {
                public void onSuggestionBuildComplete(ArrayList<Suggestion> calendarSuggestions) {
                    SuggestionManager.buildMachineLearnedRecommendation(calendarSuggestions, new SuggestionBuildListener() {
                        public void onSuggestionBuildComplete(ArrayList<Suggestion> calendarEventsAndRecommendations) {
                            callback.onSuggestionBuildComplete(calendarEventsAndRecommendations);
                        }
                    });
                }
            });
            return;
        }
        logger.d("Using cached Calendar events and Machine learned recommendation");
        callback.onSuggestionBuildComplete(recommendations);
    }

    private static boolean isCloseToLastRecommendation(Location currentPhoneLocation) {
        if (currentPhoneLocation == null || lastUserLocation == null || MapUtils.distanceBetween(currentPhoneLocation, lastUserLocation) >= 100.0f) {
            return false;
        }
        return true;
    }

    public static void buildCalendarSuggestions(final SuggestionBuildListener callback) {
        final ArrayList<Suggestion> calendarSuggestions = new ArrayList(20);
        boolean calendarSuggestionsAreEnabled = SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.CALENDARS_ENABLED, true);
        if (BaseActivity.weHaveCalendarPermission() && calendarSuggestionsAreEnabled) {
            logger.d("============ Calendar Events ============");
            CalendarUtils.readCalendarEvent(new CalendarEventReader() {
                public void onCalendarEventsRead(List<CalendarEvent> events) {
                    if (events == null || events.size() <= 0) {
                        SuggestionManager.logger.d("No Calendar Event to process");
                        callback.onSuggestionBuildComplete(calendarSuggestions);
                        return;
                    }
                    final long now = System.currentTimeMillis();
                    Collections.sort(events, new Comparator<CalendarEvent>() {
                        public int compare(CalendarEvent lhs, CalendarEvent rhs) {
                            return (int) (Math.abs(now - lhs.startTimestamp) - Math.abs(now - rhs.startTimestamp));
                        }
                    });
                    processNextCalEvent(events, 0, 0, callback);
                }

                private void processNextCalEvent(List<CalendarEvent> events, int i, int addedCount, SuggestionBuildListener callback) {
                    if (addedCount >= 2 || i >= events.size()) {
                        callback.onSuggestionBuildComplete(calendarSuggestions);
                        return;
                    }
                    final CalendarEvent event = (CalendarEvent) events.get(i);
                    SuggestionManager.logger.d("Trying to add this calendar entry to the list of suggestions: " + event.toString());
                    Destination destination = new Destination();
                    destination.rawAddressNotForDisplay = event.location;
                    if (event.latLng != null) {
                        destination.navigationLat = event.latLng.latitude;
                        destination.navigationLong = event.latLng.longitude;
                    }
                    final Destination finalDestination = NavdyContentProvider.getThisDestination(destination);
                    finalDestination.setIsCalendarEvent(true);
                    final List<CalendarEvent> list = events;
                    final int i2 = i;
                    final int i3 = addedCount;
                    final SuggestionBuildListener suggestionBuildListener = callback;
                    HereRouteManager.getInstance().calculateRoute(destination.navigationLat, destination.navigationLong, new Listener() {
                        public void onPreCalculation(@NonNull RouteHandle routeHandle) {
                        }

                        public void onRouteCalculated(@NonNull Error error, @Nullable Route route) {
                            int i = 1;
                            if (willArriveOnTime(route, event)) {
                                Suggestion suggestion = new Suggestion(finalDestination, SuggestionType.CALENDAR, event);
                                suggestion.calculateSuggestedRoute = true;
                                boolean success = SuggestionManager.addToSuggestions(suggestion, calendarSuggestions);
                                int i2 = i3;
                                if (!success) {
                                    i = 0;
                                }
                                int newCount = i2 + i;
                                SuggestionManager.logger.d((success ? "Was" : "Was not") + " able to add it as a calendarSuggestions. addedCount = " + newCount);
                                AnonymousClass5.this.processNextCalEvent(list, i2 + 1, newCount, suggestionBuildListener);
                                return;
                            }
                            SuggestionManager.logger.d("We would not arrive on time for " + event.displayName + " so will not recommend");
                            AnonymousClass5.this.processNextCalEvent(list, i2 + 1, i3, suggestionBuildListener);
                        }

                        private boolean willArriveOnTime(Route route, CalendarEvent event) {
                            if (route == null) {
                                return false;
                            }
                            int durationWithTraffic = route.getTta(TrafficPenaltyMode.OPTIMAL, Route.WHOLE_ROUTE).getDuration() * 1000;
                            long now = new Date().getTime();
                            SuggestionManager.logger.d("It will take " + FormatUtils.formatDurationFromSecondsToSecondsMinutesHours(durationWithTraffic / 1000) + " to get to " + event.displayName);
                            SuggestionManager.logger.d("You would arrive on " + new Date(((long) durationWithTraffic) + now));
                            SuggestionManager.logger.d(event.displayName + " starts on " + new Date(event.startTimestamp) + " and ends on " + new Date(event.endTimestamp));
                            if (((long) durationWithTraffic) + now >= event.endTimestamp || ((long) durationWithTraffic) + now <= event.startTimestamp - SuggestionManager.MAX_EARLY_FOR_CAL_SUGG) {
                                return false;
                            }
                            return true;
                        }
                    });
                }
            }, true);
            return;
        }
        callback.onSuggestionBuildComplete(calendarSuggestions);
    }

    private static void buildMachineLearnedRecommendation(final ArrayList<Suggestion> calendarEventsAndRecommendations, final SuggestionBuildListener callback) {
        Context context = NavdyApplication.getAppContext();
        BusProvider.getInstance().register(new Object() {
            @Subscribe
            public void onDestinationSuggestion(Destination suggestedDestination) {
                if (suggestedDestination == null || suggestedDestination.id <= 0) {
                    SuggestionManager.logger.v("Machine learning did not return any destination recommendation.");
                } else {
                    SuggestionManager.logger.v("Machine learning returned a destination recommendation: " + suggestedDestination);
                    Suggestion recommendation = SuggestionManager.makeSuggestion(suggestedDestination, SuggestionType.RECOMMENDATION);
                    recommendation.calculateSuggestedRoute = true;
                    SuggestionManager.addToSuggestions(recommendation, calendarEventsAndRecommendations);
                }
                callback.onSuggestionBuildComplete(calendarEventsAndRecommendations);
                BusProvider.getInstance().unregister(this);
            }
        });
        logger.v("Calling the Machine learning algorithm for a destination recommendation.");
        DestinationSuggestionService.suggestDestination(context, true);
    }

    private static synchronized void finishSuggestionBuild(boolean sendToHud) {
        synchronized (SuggestionManager.class) {
            cachedImportantTips = importantTips;
            cachedRecommendations = recommendations;
            cachedUnimportantTips = unimportantTips;
            cachedHistory = history;
            ArrayList<Suggestion> suggestions = getSuggestions();
            if (sendToHud) {
                VersioningUtils.increaseVersionAndSendTheseSuggestionsToDisplayAsync(suggestions);
            }
            if (suggestionListIsEmpty()) {
                unimportantTips.add(getAddFavoriteSuggestion());
                suggestions = getSuggestions();
            }
            isBuildingSuggestionList.set(false);
            notifyListenersIfAny(suggestions);
        }
    }

    private static boolean suggestionListIsEmpty() {
        return trip == null && ota == null && importantTips.size() <= 0 && recommendations.size() <= 0 && unimportantTips.size() <= 0 && history.size() <= 0;
    }

    private static boolean addToSuggestions(Suggestion suggestion, ArrayList<Suggestion> suggestions) {
        return addToSuggestions(suggestion, suggestions, false);
    }

    private static boolean addToSuggestionsOnlyCheckForDuplicates(Suggestion suggestion, ArrayList<Suggestion> suggestions) {
        return addToSuggestions(suggestion, suggestions, true);
    }

    private static boolean addToSuggestions(Suggestion suggestion, ArrayList<Suggestion> suggestions, boolean onlyCheckForDuplicates) {
        if (suggestions == null) {
            logger.v("Can't add to suggestions list because the list is null!");
            return false;
        } else if (suggestion == null) {
            logger.v("Can't add to suggestions list because the suggestion is null!");
            return false;
        } else if (suggestion.destination == null) {
            logger.v("Can't add to suggestions list because the destination inside the suggestion is null!");
            return false;
        } else if (suggestion.destination.doNotSuggest) {
            logger.v("Can't add to suggestions list because the destination inside the suggestion is marked as doNotSuggest.");
            return false;
        } else if (isDuplicate(suggestion, suggestions)) {
            return false;
        } else {
            if (!onlyCheckForDuplicates) {
                if (suggestions.size() > 20) {
                    logger.v("Can't add to suggestions list because the list is full.");
                    return false;
                } else if (isTooCloseToCurrentLocation(suggestion.destination)) {
                    logger.v("Can't add to suggestions list because the destination is too close.");
                    return false;
                }
            }
            suggestions.add(suggestion);
            return true;
        }
    }

    private static boolean isTooCloseToCurrentLocation(@NonNull Destination destination) {
        Coordinate destinationCoord = MapUtils.buildNewCoordinate(destination.displayLat, destination.displayLong);
        if (!MapUtils.isValidSetOfCoordinates(destinationCoord)) {
            destinationCoord = MapUtils.buildNewCoordinate(destination.navigationLat, destination.navigationLong);
        }
        if (MapUtils.isValidSetOfCoordinates(destinationCoord)) {
            Coordinate smartStart = navdyLocationManager.getSmartStartCoordinates();
            if (smartStart == null) {
                logger.d("No smart start coordinates so assuming we are too close");
                return true;
            }
            double distanceBetween = MapUtils.distanceBetween(destinationCoord, smartStart);
            logger.d("There is " + distanceBetween + "m. between the user and " + destination.toShortString());
            if (distanceBetween >= 200.0d) {
                return false;
            }
            return true;
        }
        logger.d("The destination has no coordinates so assuming we are too close");
        return true;
    }

    private static void addThisHeader(int stringRes, @NonNull ArrayList<Suggestion> suggestions) {
        suggestions.add(buildHeaderFor(stringRes));
    }

    @NonNull
    private static Suggestion buildHeaderFor(int stringRes) {
        return new Suggestion(new Destination(NavdyApplication.getAppContext().getString(stringRes), ""), SuggestionType.SECTION_HEADER);
    }

    private static boolean isDuplicate(Suggestion suggestion, ArrayList<Suggestion> suggestions) {
        Iterator it = suggestions.iterator();
        while (it.hasNext()) {
            if (Destination.equals(((Suggestion) it.next()).destination, suggestion.destination)) {
                return true;
            }
        }
        return false;
    }

    @NonNull
    private static Suggestion makeSuggestion(Destination destination, SuggestionType suggestionType) {
        return new Suggestion(destination, suggestionType);
    }

    public static void onCalendarChanged(final boolean sendToHud) {
        buildCalendarSuggestions(new SuggestionBuildListener() {
            public void onSuggestionBuildComplete(ArrayList<Suggestion> calendarSuggestions) {
                Iterator it = SuggestionManager.recommendations.iterator();
                while (it.hasNext()) {
                    Suggestion recommendation = (Suggestion) it.next();
                    if (recommendation.getType() != SuggestionType.CALENDAR) {
                        calendarSuggestions.add(recommendation);
                    }
                }
                SuggestionManager.recommendations = calendarSuggestions;
                SuggestionManager.finishSuggestionBuild(sendToHud);
            }
        });
    }
}
