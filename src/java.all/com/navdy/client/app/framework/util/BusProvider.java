package com.navdy.client.app.framework.util;

import android.os.Handler;
import android.os.Looper;
import com.squareup.otto.Bus;

public final class BusProvider extends Bus {
    private static final BusProvider singleton = new BusProvider();
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private BusProvider() {
    }

    public static BusProvider getInstance() {
        return singleton;
    }

    public void post(final Object event) {
        if (event != null) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    super.post(event);
                }
            });
        }
    }

    public void register(final Object object) {
        if (object != null) {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                super.register(object);
            } else {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        super.register(object);
                    }
                });
            }
        }
    }

    public void unregister(final Object object) {
        if (object != null) {
            if (Looper.getMainLooper() == Looper.myLooper()) {
                super.unregister(object);
            } else {
                this.mHandler.post(new Runnable() {
                    public void run() {
                        super.unregister(object);
                    }
                });
            }
        }
    }
}
