package com.navdy.client.app.framework.util;

import okhttp3.ResponseBody;

public abstract class CarMdCallBack {
    public abstract void processFailure(Throwable th);

    public abstract void processResponse(ResponseBody responseBody);
}
