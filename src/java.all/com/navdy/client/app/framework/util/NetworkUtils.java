package com.navdy.client.app.framework.util;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.navdy.client.app.NavdyApplication;

public class NetworkUtils {
    public static boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) NavdyApplication.getAppContext().getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
