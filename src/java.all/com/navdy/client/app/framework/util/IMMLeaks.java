package com.navdy.client.app.framework.util;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.ui.homescreen.HomescreenActivity;
import com.navdy.service.library.log.Logger;
import java.lang.reflect.Method;

public class IMMLeaks {
    private static final Logger sLogger = new Logger(IMMLeaks.class);

    public static void fixFocusedViewLeak(Application application) {
        try {
            application.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
                public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                }

                public void onActivityStarted(Activity activity) {
                }

                public void onActivityResumed(Activity activity) {
                }

                public void onActivityPaused(Activity activity) {
                }

                public void onActivityStopped(Activity activity) {
                }

                public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                }

                public void onActivityDestroyed(Activity activity) {
                    try {
                        if (activity instanceof HomescreenActivity) {
                            InputMethodManager inputMethodManager = (InputMethodManager) NavdyApplication.getAppContext().getSystemService("input_method");
                            Method finishInputLockedMethod = InputMethodManager.class.getDeclaredMethod("finishInputLocked", new Class[0]);
                            finishInputLockedMethod.setAccessible(true);
                            IMMLeaks.sLogger.v("calling finishInputLocked");
                            finishInputLockedMethod.invoke(inputMethodManager, new Object[0]);
                            IMMLeaks.sLogger.v("called finishInputLocked");
                        }
                    } catch (Throwable t) {
                        IMMLeaks.sLogger.e(t);
                    }
                }
            });
            sLogger.v("IMM installed");
        } catch (Throwable t) {
            sLogger.e("IMMLeaks", t);
        }
    }
}
