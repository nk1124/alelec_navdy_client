package com.navdy.client.app.framework.util;

import com.crashlytics.android.Crashlytics;
import com.navdy.service.library.log.LogAppender;

public class CrashlyticsAppender implements LogAppender {
    public static final String SEPARATOR = ": ";

    public void v(String tag, String msg) {
        log(tag, msg);
    }

    public void v(String tag, String msg, Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void d(String tag, String msg) {
        log(tag, msg);
    }

    public void d(String tag, String msg, Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void i(String tag, String msg) {
        log(tag, msg);
    }

    public void i(String tag, String msg, Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void w(String tag, String msg) {
        log(tag, msg);
    }

    public void w(String tag, String msg, Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void e(String tag, String msg) {
        log(tag, msg);
    }

    public void e(String tag, String msg, Throwable tr) {
        logWithThrowable(tag, msg, tr);
    }

    public void flush() {
    }

    public void close() {
    }

    private void log(String tag, String msg) {
        try {
            Crashlytics.log(tag + SEPARATOR + msg);
        } catch (Throwable th) {
        }
    }

    private void logWithThrowable(String tag, String msg, Throwable tr) {
        try {
            Crashlytics.log(tag + SEPARATOR + msg);
            logThrowableToCrashlytics(tr);
        } catch (Throwable th) {
        }
    }

    private void logThrowableToCrashlytics(Throwable tr) {
        for (StackTraceElement stackTraceElement : tr.getStackTrace()) {
            Crashlytics.log(stackTraceElement.toString());
        }
    }
}
