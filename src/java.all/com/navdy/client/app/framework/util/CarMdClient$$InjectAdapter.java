package com.navdy.client.app.framework.util;

import com.navdy.service.library.network.http.IHttpManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

public final class CarMdClient$$InjectAdapter extends Binding<CarMdClient> implements MembersInjector<CarMdClient> {
    private Binding<IHttpManager> mHttpManager;

    public CarMdClient$$InjectAdapter() {
        super(null, "members/com.navdy.client.app.framework.util.CarMdClient", false, CarMdClient.class);
    }

    public void attach(Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", CarMdClient.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
    }

    public void injectMembers(CarMdClient object) {
        object.mHttpManager = (IHttpManager) this.mHttpManager.get();
    }
}
