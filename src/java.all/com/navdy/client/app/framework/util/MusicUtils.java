package com.navdy.client.app.framework.util;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.provider.MediaStore.Audio.Media;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.media.TransportMediator;
import android.view.KeyEvent;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.music.LocalMusicPlayer;
import com.navdy.client.app.framework.music.LocalMusicPlayer.Listener;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler;
import com.navdy.client.app.framework.servicehandler.MusicServiceHandler.MusicSeekHelper;
import com.navdy.service.library.events.audio.MusicCollectionSource;
import com.navdy.service.library.events.audio.MusicCollectionType;
import com.navdy.service.library.events.audio.MusicDataSource;
import com.navdy.service.library.events.audio.MusicEvent;
import com.navdy.service.library.events.audio.MusicEvent.Action;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicTrackInfo.Builder;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.ScalingUtilities;
import com.navdy.service.library.util.ScalingUtilities.ScalingLogic;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MusicUtils {
    private static final Uri ALBUM_ART_URI = Uri.parse("content://media/external/audio/albumart");
    public static final int ARTWORK_SIZE = 200;
    private static final Options BITMAP_OPTIONS = new Options();
    private static final String[] DEFAULT_ARTWORK_CHECKSUMS = new String[]{"a980062052ec31e96ae73029ec7e6772", "6bbaf7d1b3318d9414d51d8a71705a04", "3f89e08544651f0345b0ee39cb3ef3a4", "59d9cdaa3c6f52efd34ce5e8d282cf5e", "1cb6a67160592a08bf314e9742e0fed6", "4dba0b6fe14bf656c92aed8d50b56b8d"};
    private static final Map<Action, Integer> KEY_CODES_MAPPING = new HashMap<Action, Integer>() {
        {
            put(Action.MUSIC_ACTION_PLAY, KeyEvent.KEYCODE_MEDIA_PLAY);
            put(Action.MUSIC_ACTION_PAUSE, KeyEvent.KEYCODE_MEDIA_PAUSE);
            put(Action.MUSIC_ACTION_PREVIOUS, 88);
            put(Action.MUSIC_ACTION_NEXT, 87);
        }
    };
    private static final int MAX_PLAY_RETRIES = 10;
    private static final long PAUSE_BETWEEN_RETRIES = 500;
    private static final Map<Action, KeyEvent> SEEK_KEY_EVENTS_MAPPING = new HashMap<Action, KeyEvent>() {
        {
            put(Action.MUSIC_ACTION_REWIND_START, new KeyEvent(0, 89));
            put(Action.MUSIC_ACTION_FAST_FORWARD_START, new KeyEvent(0, 90));
            put(Action.MUSIC_ACTION_REWIND_STOP, new KeyEvent(1, 89));
            put(Action.MUSIC_ACTION_FAST_FORWARD_STOP, new KeyEvent(1, 90));
        }
    };
    private static final String SONG_ART_URI_POSTFIX = "/albumart";
    private static final String SONG_URI = "content://media/external/audio/media/";
    private static volatile AudioManager audioManager = null;
    private static LocalMusicPlayer localMusicPlayer = new LocalMusicPlayer();
    public static final Logger sLogger = new Logger(MusicUtils.class);
    private static KeyEvent startedContinuousKeyEvent = null;
    private static MusicDataSource startedEventDataSource = null;

    private static class MediaKeyEventRunnable implements Runnable {
        private MusicDataSource dataSource;
        private KeyEvent event;
        private boolean isDownUpEvent;

        public MediaKeyEventRunnable(MusicDataSource dataSource, KeyEvent event, boolean isDownUpEvent) {
            this.dataSource = dataSource;
            this.event = event;
            this.isDownUpEvent = isDownUpEvent;
        }

        private void executeKeyDownUp(KeyEvent eventToRun) {
            MusicUtils.audioManager.dispatchMediaKeyEvent(KeyEvent.changeAction(eventToRun, 0));
            MusicUtils.audioManager.dispatchMediaKeyEvent(KeyEvent.changeAction(eventToRun, 1));
        }

        public void run() {
            if (this.isDownUpEvent) {
                executeKeyDownUp(this.event);
            } else {
                executeSplitKeyEvent(this.dataSource, this.event);
            }
        }

        private static synchronized void executeSplitKeyEvent(MusicDataSource dataSource, KeyEvent event) {
            synchronized (MediaKeyEventRunnable.class) {
                int eventAction = event.getAction();
                if (eventAction == 0) {
                    if (MusicUtils.startedContinuousKeyEvent != null) {
                        MusicUtils.sLogger.w("Other key down event already started pressed event - ending it: " + String.valueOf(MusicUtils.startedContinuousKeyEvent));
                        MusicUtils.executeLongPressedKeyUp();
                    }
                    MusicUtils.dispatchMediaKeyEvent(event);
                    long uptime = SystemClock.uptimeMillis();
                    MusicUtils.startedContinuousKeyEvent = KeyEvent.changeTimeRepeat(event, uptime, 0);
                    MusicUtils.startedEventDataSource = dataSource;
                    MusicUtils.sLogger.v("Executing specific key event: " + String.valueOf(MusicUtils.startedContinuousKeyEvent) + " with after-event for long press.");
                    MusicUtils.dispatchMediaKeyEvent(MusicUtils.startedContinuousKeyEvent);
                    MusicUtils.dispatchMediaKeyEvent(KeyEvent.changeTimeRepeat(event, uptime, 1, event.getFlags() | 128));
                } else if (eventAction != 1) {
                    MusicUtils.dispatchMediaKeyEvent(event);
                } else if (MusicUtils.startedContinuousKeyEvent != null) {
                    if (event.getKeyCode() != MusicUtils.startedContinuousKeyEvent.getKeyCode()) {
                        MusicUtils.sLogger.w("Unmatched key up event - ending the current just in case: " + String.valueOf(MusicUtils.startedContinuousKeyEvent));
                    }
                    MusicUtils.executeLongPressedKeyUp();
                } else {
                    MusicUtils.sLogger.w("Unexpected key up event - doing nothing: " + String.valueOf(event));
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
        jadx.core.utils.exceptions.JadxRuntimeException: Unknown predecessor block by arg (r4_3 ?) in PHI: PHI: (r4_7 ?) = (r4_3 ?), (r4_3 ?), (r4_0 ?) binds: {(r4_0 ?)=B:20:0x00b8}
        	at jadx.core.dex.instructions.PhiInsn.replaceArg(PhiInsn.java:78)
        	at jadx.core.dex.visitors.ssa.SSATransform.inlinePhiInsn(SSATransform.java:392)
        	at jadx.core.dex.visitors.ssa.SSATransform.replacePhiWithMove(SSATransform.java:360)
        	at jadx.core.dex.visitors.ssa.SSATransform.fixPhiWithSameArgs(SSATransform.java:300)
        	at jadx.core.dex.visitors.ssa.SSATransform.fixUselessPhi(SSATransform.java:275)
        	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:61)
        	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:42)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.ProcessClass.process(ProcessClass.java:32)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:51)
        	at java.lang.Iterable.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:51)
        	at jadx.core.ProcessClass.process(ProcessClass.java:37)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:292)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
        */
    public static android.graphics.Bitmap getLocalMusicPhoto(java.lang.String r8) {
        /*
        r4 = 0;
        r5 = sLogger;
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Trying to fetch local music artwork for song with ID ";
        r6 = r6.append(r7);
        r6 = r6.append(r8);
        r6 = r6.toString();
        r5.d(r6);
        if (r8 != 0) goto L_0x0023;
    L_0x001b:
        r5 = sLogger;
        r6 = "Null sent as song ID";
        r5.e(r6);
    L_0x0022:
        return r4;
    L_0x0023:
        r2 = java.lang.Long.parseLong(r8);	 Catch:{ NumberFormatException -> 0x0034 }
        r5 = com.navdy.client.app.NavdyApplication.getAppContext();
        r0 = r5.getContentResolver();
        r4 = getArtworkForAlbumWithSong(r0, r2);	 Catch:{ FileNotFoundException -> 0x0054, IllegalArgumentException -> 0x00b4 }
        goto L_0x0022;
    L_0x0034:
        r1 = move-exception;
        r5 = sLogger;
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Cannot parse long from song's ID: ";
        r6 = r6.append(r7);
        r6 = r6.append(r8);
        r7 = " - not local song";
        r6 = r6.append(r7);
        r6 = r6.toString();
        r5.i(r6, r1);
        goto L_0x0022;
    L_0x0054:
        r1 = move-exception;
    L_0x0055:
        r5 = sLogger;
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Cannot get artwork for album for the song with ID ";
        r6 = r6.append(r7);
        r6 = r6.append(r8);
        r6 = r6.toString();
        r5.e(r6, r1);
        r5 = sLogger;
        r6 = "Couldn't get artwork for album, trying song";
        r5.d(r6);
        r4 = getArtworkForSong(r0, r8);	 Catch:{ NumberFormatException -> 0x0079, IllegalArgumentException -> 0x0099, FileNotFoundException -> 0x00b6, IllegalStateException -> 0x00b8 }
        goto L_0x0022;
    L_0x0079:
        r1 = move-exception;
        r5 = sLogger;
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Cannot parse received song ID ";
        r6 = r6.append(r7);
        r6 = r6.append(r8);
        r7 = " to long ";
        r6 = r6.append(r7);
        r6 = r6.toString();
        r5.e(r6, r1);
        goto L_0x0022;
    L_0x0099:
        r1 = move-exception;
    L_0x009a:
        r5 = sLogger;
        r6 = new java.lang.StringBuilder;
        r6.<init>();
        r7 = "Cannot get artwork for song with ID ";
        r6 = r6.append(r7);
        r6 = r6.append(r8);
        r6 = r6.toString();
        r5.e(r6, r1);
        goto L_0x0022;
    L_0x00b4:
        r1 = move-exception;
        goto L_0x0055;
    L_0x00b6:
        r1 = move-exception;
        goto L_0x009a;
    L_0x00b8:
        r1 = move-exception;
        goto L_0x009a;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.navdy.client.app.framework.util.MusicUtils.getLocalMusicPhoto(java.lang.String):android.graphics.Bitmap");
    }

    private static void populateAudioManagerFromContext() {
        if (audioManager == null) {
            audioManager = (AudioManager) NavdyApplication.getAppContext().getSystemService("audio");
        }
    }

    public static synchronized void executeMusicAction(final MusicEvent event) throws UnsupportedOperationException, NameNotFoundException, IOException {
        synchronized (MusicUtils.class) {
            sLogger.d("executeMusicAction " + event);
            populateAudioManagerFromContext();
            MusicServiceHandler musicServiceHandler = MusicServiceHandler.getInstance();
            Action action = event.action;
            MusicTrackInfo currentTrackInfo = musicServiceHandler.getCurrentMediaTrackInfo();
            if (MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL.equals(event.collectionSource) && Action.MUSIC_ACTION_PLAY.equals(action) && (event.index != null || event.shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS || (localMusicPlayer != null && localMusicPlayer.isActive()))) {
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        MusicUtils.handleLocalPlayEvent(event);
                    }
                }, 1);
            } else if (MusicCollectionSource.COLLECTION_SOURCE_ANDROID_LOCAL.equals(currentTrackInfo.collectionSource)) {
                if (Action.MUSIC_ACTION_PLAY.equals(action)) {
                    localMusicPlayer.play();
                } else if (Action.MUSIC_ACTION_PAUSE.equals(action)) {
                    localMusicPlayer.pause(true);
                } else if (Action.MUSIC_ACTION_NEXT.equals(action)) {
                    localMusicPlayer.next();
                } else if (Action.MUSIC_ACTION_PREVIOUS.equals(action)) {
                    localMusicPlayer.previous();
                } else if (SEEK_KEY_EVENTS_MAPPING.containsKey(action)) {
                    if (Action.MUSIC_ACTION_FAST_FORWARD_START.equals(action)) {
                        localMusicPlayer.startFastForward();
                    } else if (Action.MUSIC_ACTION_FAST_FORWARD_STOP.equals(action)) {
                        localMusicPlayer.stopFastForward();
                    } else if (Action.MUSIC_ACTION_REWIND_START.equals(action)) {
                        localMusicPlayer.startRewind();
                    } else if (Action.MUSIC_ACTION_REWIND_STOP.equals(action)) {
                        localMusicPlayer.stopRewind();
                    }
                } else if (Action.MUSIC_ACTION_MODE_CHANGE.equals(action)) {
                    localMusicPlayer.shuffle(event.shuffleMode);
                }
            } else if (musicServiceHandler.isMusicPlayerActive() || !Action.MUSIC_ACTION_PLAY.equals(action)) {
                broadcastMusicEvent(event);
            } else {
                startMediaPlayer(event);
            }
        }
    }

    private static void handleLocalPlayEvent(MusicEvent event) {
        if (isNewCollection(event)) {
            List<MusicTrackInfo> trackList = new ArrayList();
            Cursor membersCursor;
            if (MusicCollectionType.COLLECTION_TYPE_PLAYLISTS.equals(event.collectionType)) {
                membersCursor = MusicDbUtils.getPlaylistMembersCursor(event.collectionId);
                if (membersCursor != null) {
                    try {
                        if (membersCursor.moveToFirst()) {
                            while (true) {
                                trackList.add(new Builder().collectionSource(event.collectionSource).collectionType(event.collectionType).collectionId(event.collectionId).name(membersCursor.getString(membersCursor.getColumnIndex("title"))).author(membersCursor.getString(membersCursor.getColumnIndex("artist"))).album(membersCursor.getString(membersCursor.getColumnIndex("album"))).trackId(membersCursor.getString(membersCursor.getColumnIndex("SourceId"))).playbackState(MusicPlaybackState.PLAYBACK_PLAYING).isNextAllowed(Boolean.valueOf(true)).isPreviousAllowed(Boolean.valueOf(true)).build());
                                if (!membersCursor.moveToNext()) {
                                    break;
                                }
                            }
                            membersCursor.close();
                        }
                    } catch (Throwable t) {
                        sLogger.e("Error querying GPM database: " + t);
                    } finally {
                        IOUtils.closeObject(membersCursor);
                    }
                }
                IOUtils.closeObject(membersCursor);
            } else {
                if (MusicCollectionType.COLLECTION_TYPE_ALBUMS.equals(event.collectionType)) {
                    membersCursor = MusicDbUtils.getAlbumMembersCursor(event.collectionId);
                } else if (MusicCollectionType.COLLECTION_TYPE_ARTISTS.equals(event.collectionType)) {
                    membersCursor = MusicDbUtils.getArtistMembersCursor(event.collectionId);
                } else {
                    sLogger.e("Unknown type");
                    return;
                }
                if (membersCursor != null) {
                    try {
                        if (membersCursor.moveToFirst()) {
                            while (true) {
                                trackList.add(new Builder().collectionSource(event.collectionSource).collectionType(event.collectionType).collectionId(event.collectionId).name(membersCursor.getString(membersCursor.getColumnIndex("title"))).author(membersCursor.getString(membersCursor.getColumnIndex("artist"))).album(membersCursor.getString(membersCursor.getColumnIndex("album"))).trackId(String.valueOf(membersCursor.getInt(membersCursor.getColumnIndex("_id")))).playbackState(MusicPlaybackState.PLAYBACK_PLAYING).isNextAllowed(Boolean.valueOf(true)).isPreviousAllowed(Boolean.valueOf(true)).build());
                                if (!membersCursor.moveToNext()) {
                                    break;
                                }
                            }
                            membersCursor.close();
                        }
                    } catch (Throwable t2) {
                        sLogger.e("Error querying MediaStore database: " + t2);
                    } finally {
                        IOUtils.closeObject(membersCursor);
                    }
                }
                IOUtils.closeObject(membersCursor);
            }
            localMusicPlayer.initWithQueue(trackList);
            if (!(event.shuffleMode == null || event.shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_UNKNOWN)) {
                localMusicPlayer.shuffle(event.shuffleMode);
            }
            if (localMusicPlayer.getListener() == null) {
                localMusicPlayer.setListener(new Listener() {
                    private Bitmap albumArt = null;
                    @NonNull
                    private MusicTrackInfo musicTrackInfo = MusicServiceHandler.EMPTY_TRACK_INFO;

                    public void onMetadataUpdate(final MusicTrackInfo trackInfo) {
                        MusicUtils.sLogger.v("onMetadataUpdate " + trackInfo);
                        setMusicTrackInfo(trackInfo);
                        this.albumArt = null;
                        TaskManager.getInstance().execute(new Runnable() {
                            public void run() {
                                final Bitmap bitmap = MusicUtils.getLocalMusicPhoto(String.valueOf(trackInfo.trackId));
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    public void run() {
                                        if (bitmap == null || bitmap.getByteCount() == 0) {
                                            MusicUtils.sLogger.e("Received photo has null or empty byte array");
                                            AnonymousClass4.this.setMusicTrackInfo(trackInfo);
                                            AnonymousClass4.this.sendArtwork();
                                            return;
                                        }
                                        try {
                                            AnonymousClass4.this.albumArt = ScalingUtilities.createScaledBitmapAndRecycleOriginalIfScaled(bitmap, 200, 200, ScalingLogic.FIT);
                                            AnonymousClass4.this.setMusicTrackInfo(trackInfo);
                                            AnonymousClass4.this.sendArtwork();
                                        } catch (Exception e) {
                                            MusicUtils.sLogger.e("Error updating the artwork", e);
                                        }
                                    }
                                });
                            }
                        }, 1);
                    }

                    public void onPlaybackStateUpdate(MusicPlaybackState musicPlaybackState) {
                        MusicUtils.sLogger.v("onPlaybackStateUpdate " + musicPlaybackState);
                        setMusicTrackInfo(new Builder(this.musicTrackInfo).playbackState(musicPlaybackState).build());
                        sendArtwork();
                    }

                    public void onPositionUpdate(int position, int duration) {
                        MusicUtils.sLogger.v("onPositionUpdate");
                        setMusicTrackInfo(new Builder(this.musicTrackInfo).currentPosition(Integer.valueOf(position)).duration(Integer.valueOf(duration)).build());
                    }

                    private void setMusicTrackInfo(@NonNull MusicTrackInfo trackInfo) {
                        MusicUtils.sLogger.v("setMusicTrackInfo " + trackInfo);
                        this.musicTrackInfo = trackInfo;
                        MusicServiceHandler.getInstance().setCurrentMediaTrackInfo(this.musicTrackInfo);
                    }

                    private void sendArtwork() {
                        MusicUtils.sLogger.v("sendArtwork " + this.musicTrackInfo);
                        MusicServiceHandler.getInstance().checkAndSetCurrentMediaArtwork(this.musicTrackInfo, this.albumArt);
                    }
                });
            }
        }
        if (event.index != null) {
            localMusicPlayer.play(event.index.intValue());
        } else if (event.shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS) {
            localMusicPlayer.shuffle(event.shuffleMode);
            localMusicPlayer.play(0);
        } else {
            sLogger.e("No event index and not shuffling!!!");
        }
    }

    public static void stopInternalMusicPlayer() {
        if (localMusicPlayer != null) {
            localMusicPlayer.pause(true);
        }
    }

    private static boolean isNewCollection(MusicEvent event) {
        MusicTrackInfo currentTrackInfo = MusicServiceHandler.getInstance().getCurrentMediaTrackInfo();
        return (currentTrackInfo.collectionSource == event.collectionSource && currentTrackInfo.collectionType == event.collectionType && StringUtils.equalsOrBothEmptyAfterTrim(currentTrackInfo.collectionId, event.collectionId) && currentTrackInfo.shuffleMode == event.shuffleMode) ? false : true;
    }

    private static void broadcastMusicEvent(MusicEvent event) {
        sLogger.d("broadcastMusicEvent");
        MusicServiceHandler musicServiceHandler = MusicServiceHandler.getInstance();
        Action action = event.action;
        MediaKeyEventRunnable eventRunnable = null;
        if (KEY_CODES_MAPPING.containsKey(action)) {
            eventRunnable = new MediaKeyEventRunnable(event.dataSource, new KeyEvent(0, ((Integer) KEY_CODES_MAPPING.get(action)).intValue()), true);
        } else if (SEEK_KEY_EVENTS_MAPPING.containsKey(action)) {
            MusicSeekHelper ffAndRewindRunnable = musicServiceHandler.getMusicSeekHelper();
            if (ffAndRewindRunnable != null) {
                ffAndRewindRunnable.executeMusicSeekAction(action);
                return;
            }
            eventRunnable = new MediaKeyEventRunnable(event.dataSource, (KeyEvent) SEEK_KEY_EVENTS_MAPPING.get(action), false);
        }
        if (eventRunnable != null) {
            TaskManager.getInstance().execute(eventRunnable, 1);
        }
    }

    private static void startMediaPlayer(MusicEvent event) {
        Context context = NavdyApplication.getAppContext();
        Intent intent = resolveMediaPlayerIntent(context);
        if (intent != null) {
            String packageName = intent.getPackage();
            sLogger.i("startMediaPlayer " + packageName);
            intent.addFlags(268435456);
            context.startActivity(intent);
            sendPlaybackIntents(packageName, event);
            return;
        }
        sLogger.w("startMediaPlayer couldn't create an intent to start a media player");
    }

    private static void sendPlaybackIntents(final String packageName, final MusicEvent event) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                int retries = 0;
                while (retries < 10) {
                    if (MusicServiceHandler.getInstance().getCurrentMediaTrackInfo().playbackState != MusicPlaybackState.PLAYBACK_PLAYING) {
                        if (GlanceConstants.SPOTIFY.equals(packageName)) {
                            MusicUtils.broadcastMusicEvent(event);
                        } else {
                            broadcastPlayEventViaIntent(packageName);
                        }
                        try {
                            Thread.sleep(500);
                            retries++;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        MusicUtils.sLogger.i("Started music");
                        return;
                    }
                }
                MusicUtils.sLogger.w("Failed to start music");
            }

            private void broadcastPlayEventViaIntent(String packageName) {
                Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
                KeyEvent playButtonEvent = new KeyEvent(0, KeyEvent.KEYCODE_MEDIA_PLAY);
                intent.setPackage(packageName);
                intent.putExtra("android.intent.extra.KEY_EVENT", playButtonEvent);
                NavdyApplication.getAppContext().sendOrderedBroadcast(intent, null);
            }
        }, 10);
    }

    @Nullable
    private static Intent resolveMediaPlayerIntent(Context context) {
        String packageName = MusicServiceHandler.getInstance().getLastMusicApp();
        if (StringUtils.isEmptyAfterTrim(packageName) || !SystemUtils.isPackageInstalled(packageName)) {
            if (SystemUtils.isPackageInstalled(GlanceConstants.PANDORA)) {
                packageName = GlanceConstants.PANDORA;
            } else if (SystemUtils.isPackageInstalled(GlanceConstants.SPOTIFY)) {
                packageName = GlanceConstants.SPOTIFY;
            } else {
                packageName = GlanceConstants.GOOGLE_MUSIC;
            }
        }
        return context.getPackageManager().getLaunchIntentForPackage(packageName);
    }

    public static synchronized void executeLongPressedKeyUp() {
        synchronized (MusicUtils.class) {
            if (startedContinuousKeyEvent != null) {
                populateAudioManagerFromContext();
                KeyEvent event = KeyEvent.changeAction(startedContinuousKeyEvent, 1);
                sLogger.v("Executing specific key event: " + String.valueOf(event));
                dispatchMediaKeyEvent(event);
                startedContinuousKeyEvent = null;
            }
        }
    }

    private static void dispatchMediaKeyEvent(KeyEvent event) {
        sLogger.i("Dispatching key event: " + event);
        audioManager.dispatchMediaKeyEvent(event);
    }

    public static Bitmap getMusicPhoto(String songIdStr) {
        sLogger.d("Trying to fetch music artwork for song with ID " + songIdStr);
        Bitmap currentArtwork = MusicServiceHandler.getInstance().getCurrentMediaArtwork();
        return currentArtwork != null ? currentArtwork : getLocalMusicPhoto(songIdStr);
    }

    public static boolean isDefaultArtwork(String checksum) {
        return Arrays.asList(DEFAULT_ARTWORK_CHECKSUMS).contains(checksum);
    }

    protected static long getAlbumIdForSong(ContentResolver cr, long songId) throws IllegalArgumentException {
        Cursor song = null;
        try {
            ContentResolver contentResolver = cr;
            song = contentResolver.query(Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "album_id"}, "_id=?", new String[]{String.valueOf(songId)}, null);
            if (song == null || !song.moveToFirst()) {
                throw new IllegalArgumentException("Song with ID " + songId + " not found in media store");
            }
            long j = song.getLong(song.getColumnIndexOrThrow("album_id"));
            return j;
        } finally {
            IOUtils.closeStream(song);
        }
    }

    private static Bitmap getArtworkForAlbumWithSong(ContentResolver cr, long songId) throws FileNotFoundException, IllegalArgumentException {
        long albumId = getAlbumIdForSong(cr, songId);
        if (albumId < 0) {
            throw new IllegalArgumentException("albumId must be positive or 0");
        }
        Uri uri = ContentUris.withAppendedId(ALBUM_ART_URI, albumId);
        if (uri == null) {
            throw new FileNotFoundException("Cannot build URI for album art");
        }
        InputStream in = cr.openInputStream(uri);
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(in, null, BITMAP_OPTIONS);
            return decodeStream;
        } finally {
            IOUtils.closeStream(in);
        }
    }

    public static Bitmap getArtworkForAlbum(ContentResolver cr, long albumId) {
        Bitmap bitmap = null;
        Uri uri = ContentUris.withAppendedId(ALBUM_ART_URI, albumId);
        if (uri == null) {
            sLogger.e("Couldn't get URI for album: " + albumId);
        } else {
            InputStream in = null;
            try {
                in = NavdyApplication.getAppContext().getContentResolver().openInputStream(uri);
                bitmap = BitmapFactory.decodeStream(in, null, BITMAP_OPTIONS);
            } catch (Throwable e) {
                sLogger.e("Couldn't get album art", e);
            } finally {
                IOUtils.closeStream(in);
            }
        }
        return bitmap;
    }

    public static Bitmap getArtworkForArtist(ContentResolver cr, long artistId) {
        sLogger.d("getArtworkForArtist " + artistId);
        Cursor songsCursor = null;
        try {
            songsCursor = cr.query(Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, "artist_id = ?", new String[]{String.valueOf(artistId)}, null);
            if (songsCursor != null && songsCursor.moveToFirst()) {
                do {
                    Bitmap bitmap = getLocalMusicPhoto(String.valueOf(songsCursor.getInt(0)));
                    if (bitmap != null) {
                        songsCursor.close();
                        IOUtils.closeStream(songsCursor);
                        return bitmap;
                    }
                } while (songsCursor.moveToNext());
                songsCursor.close();
            }
            IOUtils.closeStream(songsCursor);
            return null;
        } catch (Throwable th) {
            IOUtils.closeStream(songsCursor);
        }
    }

    public static Bitmap getArtworkForPlaylist(ContentResolver cr, long playlistId) {
        Cursor membersCursor = MusicDbUtils.getPlaylistMembersCursor((int) playlistId);
        if (membersCursor != null) {
            try {
                if (membersCursor.moveToFirst()) {
                    Bitmap bitmap = getLocalMusicPhoto(membersCursor.getString(membersCursor.getColumnIndex("SourceId")));
                    if (bitmap != null) {
                        membersCursor.close();
                        return bitmap;
                    }
                    membersCursor.close();
                }
            } finally {
                IOUtils.closeStream(membersCursor);
            }
        }
        IOUtils.closeStream(membersCursor);
        return null;
    }

    private static Bitmap getArtworkForSong(ContentResolver cr, String songIdStr) throws IllegalArgumentException, FileNotFoundException, IllegalStateException {
        sLogger.d("getArtworkForSong " + songIdStr);
        if (Long.parseLong(songIdStr) >= 0) {
            return getBitmapFromUri(cr, Uri.parse(SONG_URI + songIdStr + SONG_ART_URI_POSTFIX));
        }
        throw new IllegalArgumentException("songId must be positive or 0");
    }

    private static Bitmap getBitmapFromUri(ContentResolver cr, Uri uri) throws FileNotFoundException {
        sLogger.d("getBitmapFromUri " + uri);
        ParcelFileDescriptor pfd = cr.openFileDescriptor(uri, "r");
        if (pfd == null) {
            throw new FileNotFoundException("Cannot open ParcelFileDescriptor for URI: " + uri);
        }
        try {
            Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(pfd.getFileDescriptor(), null, BITMAP_OPTIONS);
            return decodeFileDescriptor;
        } finally {
            IOUtils.closeStream(pfd);
        }
    }
}
