package com.navdy.client.app.framework.util;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Process;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;

public class GmsUtils {
    private static final int LATEST_VERSION_OF_GMS = 10084000;
    private static Logger sLogger = new Logger(GmsUtils.class);

    public enum GmsState {
        UP_TO_DATE,
        OUT_OF_DATE,
        MISSING
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x00b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static GmsState checkIfMissingOrOutOfDate() {
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        int gmsVersion = sharedPreferences.getInt(SettingsConstants.GMS_VERSION_NUMBER, -1);
        boolean gmsIsMissing = sharedPreferences.getBoolean(SettingsConstants.GMS_IS_MISSING, false);
        sLogger.i("From Shared Prefs: GMS version code is " + gmsVersion + " and gmsIsMissing is " + gmsIsMissing);
        if (!gmsIsMissing && gmsVersion != -1 && isUpToDateGmsVersion(gmsVersion)) {
            return GmsState.UP_TO_DATE;
        }
        PackageManager packageManager = NavdyApplication.getAppContext().getPackageManager();
        if (packageManager != null) {
            boolean found = false;
            for (PackageInfo packageInfo : packageManager.getInstalledPackages(128)) {
                if (StringUtils.equalsOrBothEmptyAfterTrim(packageInfo.packageName, "com.google.android.gms")) {
                    found = true;
                    sLogger.i("Found GMS. version code is " + packageInfo.versionCode + " and versionName is " + packageInfo.versionName);
                    Editor edit = sharedPreferences.edit();
                    edit.putInt(SettingsConstants.GMS_VERSION_NUMBER, packageInfo.versionCode);
                    edit.putBoolean(SettingsConstants.GMS_IS_MISSING, false);
                    edit.apply();
                    if (!isUpToDateGmsVersion(packageInfo.versionCode)) {
                        return GmsState.OUT_OF_DATE;
                    }
                    if (!found) {
                        sLogger.e("Could not find GMS !");
                        edit = sharedPreferences.edit();
                        edit.putBoolean(SettingsConstants.GMS_IS_MISSING, true);
                        edit.apply();
                        return GmsState.MISSING;
                    }
                }
            }
            if (found) {
            }
        }
        return GmsState.UP_TO_DATE;
    }

    public static boolean isUpToDateGmsVersion(int versionCode) {
        return versionCode == -1 || versionCode >= LATEST_VERSION_OF_GMS;
    }

    public static boolean finishIfGmsIsNotUpToDate(Activity activity) {
        GmsState gmsState = checkIfMissingOrOutOfDate();
        if (gmsState != GmsState.UP_TO_DATE) {
            if (gmsState == GmsState.OUT_OF_DATE) {
                sLogger.e("OUTDATED GOOGLE PLAY SERVICES!");
                BaseActivity.showLongToast(R.string.outdated_gms_title, new Object[0]);
            } else if (gmsState == GmsState.MISSING) {
                sLogger.e("NO GOOGLE PLAY SERVICES!");
                BaseActivity.showLongToast(R.string.no_google_play_services, new Object[0]);
            }
            if (activity != null) {
                sLogger.e("Killing activity, GMS error");
                activity.finish();
            } else {
                sLogger.e("KILLING APP, GMS ERROR");
                Process.killProcess(Process.myPid());
            }
        }
        if (gmsState != GmsState.UP_TO_DATE) {
            return true;
        }
        return false;
    }
}
