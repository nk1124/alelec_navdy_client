package com.navdy.client.app.framework.util;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings.System;
import android.support.annotation.NonNull;
import com.navdy.service.library.log.Logger;

public class AudioStreamVolumeObserver {
    private static Logger logger = new Logger(AudioStreamVolumeObserver.class);
    private AudioStreamVolumeContentObserver mAudioStreamVolumeContentObserver;
    private final Context mContext;

    private static class AudioStreamVolumeContentObserver extends ContentObserver {
        private final AudioManager mAudioManager;
        private final int mAudioStreamType;
        private int mLastVolume = this.mAudioManager.getStreamVolume(this.mAudioStreamType);
        private final OnAudioStreamVolumeChangedListener mListener;

        public AudioStreamVolumeContentObserver(@NonNull Handler handler, @NonNull AudioManager audioManager, int audioStreamType, @NonNull OnAudioStreamVolumeChangedListener listener) {
            super(handler);
            this.mAudioManager = audioManager;
            this.mAudioStreamType = audioStreamType;
            this.mListener = listener;
        }

        public void onChange(boolean selfChange) {
            if (this.mAudioManager == null) {
                AudioStreamVolumeObserver.logger.w("Unable to get the audio manager service.");
                return;
            }
            int currentVolume = this.mAudioManager.getStreamVolume(this.mAudioStreamType);
            if (currentVolume != this.mLastVolume) {
                this.mLastVolume = currentVolume;
                if (this.mListener != null) {
                    this.mListener.onAudioStreamVolumeChanged(this.mAudioStreamType, currentVolume);
                }
            }
        }
    }

    public interface OnAudioStreamVolumeChangedListener {
        void onAudioStreamVolumeChanged(int i, int i2);
    }

    public AudioStreamVolumeObserver(@NonNull Context context) {
        this.mContext = context;
    }

    public void start(int audioStreamType, @NonNull OnAudioStreamVolumeChangedListener listener) {
        stop();
        this.mAudioStreamVolumeContentObserver = new AudioStreamVolumeContentObserver(new Handler(Looper.getMainLooper()), (AudioManager) this.mContext.getSystemService("audio"), audioStreamType, listener);
        this.mContext.getContentResolver().registerContentObserver(System.CONTENT_URI, true, this.mAudioStreamVolumeContentObserver);
    }

    public void stop() {
        if (this.mAudioStreamVolumeContentObserver != null) {
            this.mContext.getContentResolver().unregisterContentObserver(this.mAudioStreamVolumeContentObserver);
            this.mAudioStreamVolumeContentObserver = null;
        }
    }
}
