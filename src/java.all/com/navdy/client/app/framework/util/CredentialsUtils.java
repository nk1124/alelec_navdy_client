package com.navdy.client.app.framework.util;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.log.Logger;

public class CredentialsUtils {
    private static final boolean VERBOSE = false;
    public static final Logger logger = new Logger(CredentialsUtils.class);
    private Context context = NavdyApplication.getAppContext();

    public static String getCredentials(String metaName) {
        String credentials = "";
        try {
            return NavdyApplication.getAppContext().getPackageManager().getApplicationInfo(NavdyApplication.getAppContext().getPackageName(), 128).metaData.getString(metaName);
        } catch (NameNotFoundException e) {
            logger.e("Failed to load meta-data, NameNotFound: " + e.getMessage());
            return credentials;
        } catch (NullPointerException e2) {
            logger.e("Failed to load meta-data, NullPointer: " + e2.getMessage());
            return credentials;
        }
    }
}
