package com.navdy.client.app.framework.navigation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RouteOptions.TransportMode;
import com.here.android.mpa.routing.RouteOptions.Type;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.RoutingError;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.HereMapsManager;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.log.Logger;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class HereRouteManager {
    private static final boolean VERBOSE = false;
    private static final Object initListenersLock = new Object();
    private static final HereRouteManager instance = new HereRouteManager();
    private static final Logger logger = new Logger(HereRouteManager.class);
    private final Queue<OnHereRouteManagerInitialized> initListeners = new LinkedList();
    private final NavdyLocationManager navdyLocationManager = NavdyLocationManager.getInstance();
    private volatile State state = State.INITIALIZING;

    private interface OnHereRouteManagerInitialized {
        void onFailed();

        void onInit();
    }

    public enum Error {
        NONE,
        NO_ROUTES,
        NO_LOCATION,
        HERE_INTERNAL_ERROR
    }

    public interface Listener {
        void onPreCalculation(@NonNull RouteHandle routeHandle);

        void onRouteCalculated(@NonNull Error error, @Nullable Route route);
    }

    public static class RouteHandle {
        private final CoreRouter router;

//        /* synthetic */ RouteHandle(CoreRouter x0, AnonymousClass1 x1) {
//            this(x0);
//        }

        private RouteHandle(CoreRouter router) {
            this.router = router;
        }

        public void cancel() {
            if (this.router.isBusy()) {
                this.router.cancel();
            }
        }
    }

    private enum State {
        INITIALIZING,
        INITIALIZED,
        FAILED
    }

    public static HereRouteManager getInstance() {
        return instance;
    }

    private HereRouteManager() {
        initialize();
    }

    public void calculateRoute(double latitude, double longitude, @NonNull Listener listener) {
        final double d = latitude;
        final double d2 = longitude;
        final Listener listener2 = listener;
        whenInitialized(new OnHereRouteManagerInitialized() {
            public void onInit() {
                Coordinate location = HereRouteManager.this.navdyLocationManager.getSmartStartCoordinates();
                if (location != null) {
                    HereRouteManager.this.calculateRouteWithCoords(new GeoCoordinate(location.latitude.doubleValue(), location.longitude.doubleValue()), new GeoCoordinate(d, d2), listener2);
                }
            }

            public void onFailed() {
                listener2.onRouteCalculated(Error.HERE_INTERNAL_ERROR, null);
            }
        });
    }

    private void calculateRouteWithCoords(GeoCoordinate navdyLocation, GeoCoordinate destination, @NonNull final Listener listener) {
        RoutePlan routePlan = new RoutePlan();
        routePlan.addWaypoint(new RouteWaypoint(navdyLocation));
        routePlan.addWaypoint(new RouteWaypoint(destination));
        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setRouteCount(1);
        routeOptions.setTransportMode(TransportMode.CAR);
        routeOptions.setRouteType(Type.FASTEST);
        routePlan.setRouteOptions(routeOptions);
        CoreRouter router = new CoreRouter();
        listener.onPreCalculation(new RouteHandle(router, null));
        router.calculateRoute(routePlan, new com.here.android.mpa.routing.CoreRouter.Listener() {
            public void onCalculateRouteFinished(List<RouteResult> list, RoutingError routingError) {
                if (routingError != RoutingError.NONE || list == null || list.isEmpty() || list.get(0) == null || ((RouteResult) list.get(0)).getRoute() == null) {
                    if (routingError == RoutingError.NO_END_POINT) {
                        Tracker.tagEvent(Event.NO_END_POINT);
                    }
                    HereRouteManager.logger.e("HereRouteManager Error: Here Internal Error " + routingError);
                    listener.onRouteCalculated(Error.HERE_INTERNAL_ERROR, null);
                    return;
                }
                listener.onRouteCalculated(Error.NONE, ((RouteResult) list.get(0)).getRoute());
            }

            public void onProgress(int i) {
            }
        });
    }

    private void initialize() {
        HereMapsManager.getInstance().addOnInitializedListener(new OnEngineInitListener() {
            public void onEngineInitializationCompleted(com.here.android.mpa.common.OnEngineInitListener.Error error) {
                synchronized (HereRouteManager.initListenersLock) {
                    if (error != com.here.android.mpa.common.OnEngineInitListener.Error.NONE) {
                        HereRouteManager.logger.e("HereRouteManager failed to initialize with error: " + error.name());
                        HereRouteManager.this.state = State.FAILED;
                        while (!HereRouteManager.this.initListeners.isEmpty()) {
                            ((OnHereRouteManagerInitialized) HereRouteManager.this.initListeners.poll()).onFailed();
                        }
                    } else {
                        HereRouteManager.logger.i("HereRouteManager successfully initialized, running initListeners");
                        HereRouteManager.this.state = State.INITIALIZED;
                        while (!HereRouteManager.this.initListeners.isEmpty()) {
                            ((OnHereRouteManagerInitialized) HereRouteManager.this.initListeners.poll()).onInit();
                        }
                    }
                }
            }
        });
    }

    private void whenInitialized(OnHereRouteManagerInitialized callback) {
        if (this.state == State.INITIALIZING) {
            synchronized (initListenersLock) {
                this.initListeners.add(callback);
            }
        } else if (this.state == State.INITIALIZED) {
            callback.onInit();
        }
    }
}
