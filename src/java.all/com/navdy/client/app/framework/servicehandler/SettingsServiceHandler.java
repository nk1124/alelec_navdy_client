package com.navdy.client.app.framework.servicehandler;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.connection.ConnectionStateChange;
import com.navdy.service.library.events.glances.CannedMessagesRequest;
import com.navdy.service.library.events.glances.CannedMessagesUpdate;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesRequest;
import com.navdy.service.library.events.preferences.DisplaySpeakerPreferencesUpdate;
import com.navdy.service.library.events.preferences.DriverProfilePreferencesRequest;
import com.navdy.service.library.events.preferences.DriverProfilePreferencesUpdate;
import com.navdy.service.library.events.preferences.InputPreferencesRequest;
import com.navdy.service.library.events.preferences.InputPreferencesUpdate;
import com.navdy.service.library.events.preferences.NavigationPreferences;
import com.navdy.service.library.events.preferences.NavigationPreferences.RerouteForTraffic;
import com.navdy.service.library.events.preferences.NavigationPreferences.RoutingType;
import com.navdy.service.library.events.preferences.NavigationPreferencesRequest;
import com.navdy.service.library.events.preferences.NavigationPreferencesUpdate;
import com.navdy.service.library.events.preferences.NotificationPreferencesRequest;
import com.navdy.service.library.events.preferences.NotificationPreferencesUpdate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;
import com.squareup.wire.Message;

public class SettingsServiceHandler {
    public static final Logger sLogger = new Logger(SettingsServiceHandler.class);

    public SettingsServiceHandler() {
        BusProvider.getInstance().register(this);
    }

    @Subscribe
    public void onDriverProfileRequest(final DriverProfilePreferencesRequest driverProfilePreferencesRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    long serial = SettingsUtils.getCustomerPreferences().getLong(ProfilePreferences.SERIAL_NUM, ProfilePreferences.SERIAL_NUM_DEFAULT.longValue());
                    if (driverProfilePreferencesRequest.serial_number.longValue() == serial) {
                        SettingsServiceHandler.sLogger.v("Driver profile version up to date");
                        SettingsServiceHandler.this.sendRemoteMessage(new DriverProfilePreferencesUpdate(RequestStatus.REQUEST_VERSION_IS_CURRENT, null, Long.valueOf(serial), null));
                        return;
                    }
                    SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
                } catch (Throwable t) {
                    SettingsServiceHandler.sLogger.e(t);
                    DeviceConnection.postEvent(new DriverProfilePreferencesUpdate(RequestStatus.REQUEST_UNKNOWN_ERROR, null, Long.valueOf(0), null));
                }
            }
        }, 1);
    }

    @Subscribe
    public void onInputRequest(final InputPreferencesRequest inputPreferencesRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    long serial = SettingsUtils.getSharedPreferences().getLong(SettingsConstants.HUD_SERIAL_NUM, 0);
                    if (inputPreferencesRequest.serial_number.longValue() == serial) {
                        SettingsServiceHandler.sLogger.v("Input prefs version up to date");
                        SettingsServiceHandler.this.sendRemoteMessage(new InputPreferencesUpdate(RequestStatus.REQUEST_VERSION_IS_CURRENT, null, Long.valueOf(serial), null));
                        return;
                    }
                    SettingsUtils.sendDialSettingsToTheHudBasedOnSharedPrefValue();
                } catch (Throwable t) {
                    SettingsServiceHandler.sLogger.e(t);
                    DeviceConnection.postEvent(new InputPreferencesUpdate(RequestStatus.REQUEST_UNKNOWN_ERROR, null, Long.valueOf(0), null));
                }
            }
        }, 1);
    }

    @Subscribe
    public void onSpeakerRequest(final DisplaySpeakerPreferencesRequest displaySpeakerPreferencesRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    long serial = SettingsUtils.getSharedPreferences().getLong(SettingsConstants.AUDIO_SERIAL_NUM, 0);
                    if (displaySpeakerPreferencesRequest.serial_number.longValue() == serial) {
                        SettingsServiceHandler.sLogger.v("Speaker prefs version up to date");
                        SettingsServiceHandler.this.sendRemoteMessage(new DisplaySpeakerPreferencesUpdate(RequestStatus.REQUEST_VERSION_IS_CURRENT, null, Long.valueOf(serial), null));
                        return;
                    }
                    SettingsUtils.sendSpeakerSettingsToTheHudBasedOnSharedPrefValue();
                } catch (Throwable t) {
                    SettingsServiceHandler.sLogger.e(t);
                    DeviceConnection.postEvent(new DisplaySpeakerPreferencesUpdate(RequestStatus.REQUEST_UNKNOWN_ERROR, null, Long.valueOf(0), null));
                }
            }
        }, 1);
    }

    @Subscribe
    public void onNavigationRequest(final NavigationPreferencesRequest navigationPreferencesRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    long serial = SettingsUtils.getSharedPreferences().getLong("nav_serial_number", 0);
                    if (navigationPreferencesRequest.serial_number.longValue() == serial) {
                        SettingsServiceHandler.sLogger.v("Nav prefs version up to date");
                        SettingsServiceHandler.this.sendRemoteMessage(new NavigationPreferencesUpdate(RequestStatus.REQUEST_VERSION_IS_CURRENT, null, Long.valueOf(serial), null));
                        return;
                    }
                    SettingsUtils.sendNavSettingsToTheHudBasedOnSharedPrefValue();
                } catch (Throwable t) {
                    SettingsServiceHandler.sLogger.e(t);
                    DeviceConnection.postEvent(new NavigationPreferencesUpdate(RequestStatus.REQUEST_UNKNOWN_ERROR, null, Long.valueOf(0), null));
                }
            }
        }, 1);
    }

    @Subscribe
    public void onCannedMessagesRequest(final CannedMessagesRequest cannedMessagesRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
                    if (sharedPrefs.getBoolean(SettingsConstants.HUD_CANNED_RESPONSE_CAPABLE, false)) {
                        long serial = sharedPrefs.getLong("nav_serial_number", 0);
                        if (cannedMessagesRequest.serial_number.longValue() == serial) {
                            SettingsServiceHandler.sLogger.v("CannedMessage prefs version up to date");
                            SettingsServiceHandler.this.sendRemoteMessage(new CannedMessagesUpdate(RequestStatus.REQUEST_VERSION_IS_CURRENT, null, Long.valueOf(serial), null));
                            return;
                        }
                        SettingsUtils.sendMessagingSettingsToTheHudBasedOnSharedPrefValue();
                        return;
                    }
                    SettingsServiceHandler.sLogger.w("HUD requested canned messages but is not capable of it so ignoring request.");
                } catch (Throwable t) {
                    SettingsServiceHandler.sLogger.e(t);
                    DeviceConnection.postEvent(new CannedMessagesUpdate(RequestStatus.REQUEST_UNKNOWN_ERROR, null, Long.valueOf(0), null));
                }
            }
        }, 1);
    }

    @Subscribe
    public void onNavigationPreferencesUpdate(final NavigationPreferencesUpdate navigationPreferencesUpdate) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                if (navigationPreferencesUpdate == null || navigationPreferencesUpdate.preferences == null) {
                    SettingsServiceHandler.sLogger.e("Received a NavigationPreferencesUpdate with null preferences!");
                    return;
                }
                SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
                if (sharedPrefs != null) {
                    long navSerialNumber = sharedPrefs.getLong("nav_serial_number", 0) + 1;
                    Editor editor = sharedPrefs.edit();
                    NavigationPreferences p = navigationPreferencesUpdate.preferences;
                    boolean shortestRouteIsOn = p.routingType == RoutingType.ROUTING_SHORTEST;
                    boolean autoRecalcIsOn = p.rerouteForTraffic == RerouteForTraffic.REROUTE_AUTOMATIC;
                    editor.putLong("nav_serial_number", navSerialNumber);
                    SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.NAVIGATION_ROUTE_CALCULATION, Boolean.valueOf(shortestRouteIsOn));
                    SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.NAVIGATION_AUTO_RECALC, Boolean.valueOf(autoRecalcIsOn));
                    SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.NAVIGATION_HIGHWAYS, p.allowHighways);
                    SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.NAVIGATION_TOLL_ROADS, p.allowTollRoads);
                    SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.NAVIGATION_FERRIES, p.allowFerries);
                    SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.NAVIGATION_TUNNELS, p.allowTunnels);
                    SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.NAVIGATION_UNPAVED_ROADS, p.allowUnpavedRoads);
                    SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.NAVIGATION_AUTO_TRAINS, p.allowAutoTrains);
                    editor.apply();
                    if (p.spokenTurnByTurn != null || p.spokenSpeedLimitWarnings != null) {
                        editor.putLong(SettingsConstants.AUDIO_SERIAL_NUM, sharedPrefs.getLong(SettingsConstants.AUDIO_SERIAL_NUM, 0) + 1);
                        SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.AUDIO_TURN_BY_TURN_INSTRUCTIONS, p.spokenTurnByTurn);
                        SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.AUDIO_SPEED_WARNINGS, p.spokenSpeedLimitWarnings);
                        SettingsServiceHandler.this.putOnlyIfNotNull(editor, SettingsConstants.AUDIO_CAMERA_WARNINGS, p.spokenCameraWarnings);
                        editor.apply();
                    }
                }
            }
        }, 1);
    }

    private void putOnlyIfNotNull(Editor editor, String key, Boolean value) {
        if (value != null) {
            editor.putBoolean(key, value.booleanValue());
        }
    }

    @Subscribe
    public void onNotificationRequest(final NotificationPreferencesRequest notificationPreferencesRequest) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    long serial = SettingsUtils.getSharedPreferences().getLong(SettingsConstants.GLANCES_SERIAL_NUM, 0);
                    if (notificationPreferencesRequest.serial_number.longValue() == serial) {
                        SettingsServiceHandler.sLogger.v("Glances prefs version up to date");
                        SettingsServiceHandler.this.sendRemoteMessage(new NotificationPreferencesUpdate(RequestStatus.REQUEST_VERSION_IS_CURRENT, null, Long.valueOf(serial), null));
                        return;
                    }
                    GlanceUtils.sendGlancesSettingsToTheHudBasedOnSharedPrefValue();
                } catch (Throwable t) {
                    SettingsServiceHandler.sLogger.e(t);
                    DeviceConnection.postEvent(new NotificationPreferencesUpdate(RequestStatus.REQUEST_UNKNOWN_ERROR, null, Long.valueOf(0), null));
                }
            }
        }, 1);
    }

    @Subscribe
    public void onConnectionStateChange(ConnectionStateChange connectionStateChange) {
        if (DeviceConnection.isConnected()) {
            SettingsUtils.updateAllSettingsIfNecessary();
        }
    }

    private void sendRemoteMessage(Message message) {
        DeviceConnection.postEvent(message);
    }
}
