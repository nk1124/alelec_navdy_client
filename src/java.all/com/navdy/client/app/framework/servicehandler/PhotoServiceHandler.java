package com.navdy.client.app.framework.servicehandler;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.PhoneLookup;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.ImageUtils.StreamFactory;
import com.navdy.client.app.framework.util.MusicUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.photo.PhotoRequest;
import com.navdy.service.library.events.photo.PhotoResponse;
import com.navdy.service.library.events.photo.PhotoType;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Subscribe;
import java.io.FileNotFoundException;
import java.io.InputStream;
import okio.ByteString;

public class PhotoServiceHandler {
    private static final Logger sLogger = new Logger(PhotoServiceHandler.class);
    private Context context;
    private int thumbnailSize = NavdyApplication.getAppContext().getResources().getInteger(R.integer.thumbnailImageDimension);

    public PhotoServiceHandler(Context context) {
        this.context = context;
        BusProvider.getInstance().register(this);
    }

    @Subscribe
    public void onPhotoRequest(final PhotoRequest request) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                Bitmap photo = null;
                try {
                    if (PhotoType.PHOTO_CONTACT.equals(request.photoType)) {
                        photo = PhotoServiceHandler.this.getContactPhoto(request.identifier, PhotoServiceHandler.this.thumbnailSize);
                    } else if (PhotoType.PHOTO_ALBUM_ART.equals(request.photoType)) {
                        photo = MusicUtils.getMusicPhoto(request.identifier);
                    } else if (PhotoType.PHOTO_DRIVER_PROFILE.equals(request.photoType)) {
                        photo = Tracker.getScaledProfilePhoto(PhotoServiceHandler.this.thumbnailSize);
                    }
                    if (photo == null) {
                        PhotoServiceHandler.sLogger.v("PhotoServiceHandler:Cannot find requested photo:" + request.identifier);
                    } else {
                        PhotoServiceHandler.sLogger.v("PhotoServiceHandler:found photo:" + request.identifier);
                    }
                    DeviceConnection.postEvent(PhotoServiceHandler.buildPhotoResponse(photo, request));
                } catch (Throwable t) {
                    PhotoServiceHandler.sLogger.e("PhotoServiceHandler:" + request.identifier, t);
                    DeviceConnection.postEvent(new PhotoResponse(null, RequestStatus.REQUEST_SERVICE_ERROR, null, request.identifier, null, request.photoType));
                }
            }
        }, 1);
    }

    public static String buildPhotoChecksum(Bitmap photo) {
        String checksum = "";
        if (photo == null) {
            sLogger.w("Trying to buildPhotoChecksum from a null photo.");
            return checksum;
        }
        try {
            checksum = IOUtils.hashForBytes(IOUtils.bitmap2ByteBuffer(photo));
        } catch (Exception e) {
            sLogger.d("Error computing checksum for the bitmap data", e);
        }
        return checksum;
    }

    private static PhotoResponse buildPhotoResponse(Bitmap photo, PhotoRequest request) {
        if (photo == null) {
            return new PhotoResponse(null, RequestStatus.REQUEST_NOT_AVAILABLE, null, request.identifier, null, request.photoType);
        }
        boolean changed = true;
        String newHash = null;
        byte[] data = IOUtils.bitmap2ByteBuffer(photo);
        try {
            newHash = IOUtils.hashForBytes(data);
            if (newHash != null && newHash.equals(request.photoChecksum)) {
                changed = false;
            }
        } catch (Throwable e) {
            sLogger.d("Error computing hash for the bitmap data", e);
        }
        ByteString byteStringPhoto = ByteString.of(data);
        sLogger.v("PhotoServiceHandler: changed:" + changed + " size:" + byteStringPhoto.size());
        return new PhotoResponse(changed ? byteStringPhoto : null, RequestStatus.REQUEST_SUCCESS, null, request.identifier, newHash, request.photoType);
    }

    private Bitmap getContactPhoto(String identifier, int thumbnailSize) {
        if (identifier == null || identifier.length() == 0) {
            return null;
        }
        if (!isNumber(identifier)) {
            return getContactPhotoViaName(identifier, thumbnailSize);
        }
        Bitmap bitmap = getContactPhotoViaPhoneNumber(identifier, thumbnailSize);
        if (bitmap == null) {
            return getContactPhotoViaName(identifier, thumbnailSize);
        }
        return bitmap;
    }

    private Bitmap getContactPhotoViaPhoneNumber(String identifier, int thumbnailSize) {
        Uri phoneUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(identifier));
        ContentResolver cr = this.context.getContentResolver();
        Cursor contactCursor = null;
        Bitmap contactPhoto;
        try {
            contactCursor = cr.query(phoneUri, new String[]{"_id"}, null, null, null);
            if (contactCursor == null || !contactCursor.moveToFirst()) {
                IOUtils.closeStream(contactCursor);
                return null;
            }
            contactPhoto = getContactPhoto(cr, contactCursor, thumbnailSize);
            return contactPhoto;
        } catch (Throwable t) {
            contactPhoto = sLogger;
            contactPhoto.e(t);
        } finally {
            IOUtils.closeStream(contactCursor);
        }
    }

    private Bitmap getContactPhotoViaName(String identifier, int thumbnailSize) {
        ContentResolver cr = this.context.getContentResolver();
        String selection = "lower(display_name) LIKE '" + identifier.toLowerCase() + "%'";
        Cursor contact = null;
        Bitmap contactPhoto;
        try {
            contact = cr.query(Contacts.CONTENT_URI, new String[]{"_id"}, selection, null, null);
            if (contact == null || !contact.moveToFirst()) {
                IOUtils.closeStream(contact);
                return null;
            }
            contactPhoto = getContactPhoto(cr, contact, thumbnailSize);
            return contactPhoto;
        } catch (Throwable t) {
            contactPhoto = sLogger;
            contactPhoto.e(t);
        } finally {
            IOUtils.closeStream(contact);
        }
    }

    private Bitmap getContactPhoto(final ContentResolver cr, Cursor contact, int thumbnailSize) {
        final Uri photoUri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contact.getLong(contact.getColumnIndex("_id")));
        if (photoUri != null) {
            return ImageUtils.getScaledBitmap(new StreamFactory() {
                public InputStream getInputStream() throws FileNotFoundException {
                    return Contacts.openContactPhotoInputStream(cr, photoUri);
                }
            }, thumbnailSize, thumbnailSize);
        }
        return null;
    }

    private boolean isNumber(String identifier) {
        try {
            Long.parseLong(identifier.replaceAll("[^a-zA-Z0-9\\._]+", ""));
            return true;
        } catch (Throwable th) {
            return false;
        }
    }
}
