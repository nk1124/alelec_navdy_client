package com.navdy.client.app.framework.servicehandler;

import android.content.Context;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.location.NavdyLocationManager.CarLocationChangedEvent;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Trip;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.service.library.events.TripUpdate;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.squareup.otto.Subscribe;

public class TripUpdateServiceHandler {
    private static final Logger logger = new Logger(TripUpdateServiceHandler.class);
    private static TripUpdateServiceHandler singleton = null;

    private TripUpdateServiceHandler() {
        BusProvider.getInstance().register(this);
    }

    public static TripUpdateServiceHandler getInstance() {
        if (singleton == null) {
            singleton = new TripUpdateServiceHandler();
        }
        return singleton;
    }

    @Subscribe
    public void onTripUpdate(final TripUpdate tripUpdate) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                TripUpdateServiceHandler.this.handleTripUpdate(tripUpdate);
            }
        }, 1);
    }

    private void handleTripUpdate(TripUpdate tripUpdate) {
        if (tripUpdate != null) {
            if (tripUpdate.current_position != null) {
                BusProvider.getInstance().post(new CarLocationChangedEvent(MapUtils.buildNewCoordinate(tripUpdate.current_position.latitude.doubleValue(), tripUpdate.current_position.longitude.doubleValue())));
            }
            if (tripUpdate.trip_number != null) {
                String tripNumber = tripUpdate.trip_number.toString();
                logger.v("TripUpdate received, trip number: " + tripNumber);
                Context appContext = NavdyApplication.getAppContext();
                try {
                    if (NavdyContentProvider.getThisTrip(tripNumber) == null) {
                        logger.v("The following trip was inserted: " + Trip.saveStartingTripUpdate(appContext, tripUpdate));
                        return;
                    }
                    logger.v("Updated this many trips: " + Trip.saveLastTripUpdate(appContext, tripUpdate));
                } catch (Exception e) {
                    logger.e("Something went wrong while trying to save trip info: ", e);
                }
            }
        }
    }
}
