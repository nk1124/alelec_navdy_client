package com.navdy.client.app.framework.servicehandler;

import android.widget.Toast;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.DeviceConnection;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.events.RequestStatus;
import com.navdy.service.library.events.debug.DriveRecordingsRequest;
import com.navdy.service.library.events.debug.DriveRecordingsResponse;
import com.navdy.service.library.events.debug.StartDrivePlaybackEvent;
import com.navdy.service.library.events.debug.StartDrivePlaybackResponse;
import com.navdy.service.library.events.debug.StartDriveRecordingEvent;
import com.navdy.service.library.events.debug.StartDriveRecordingResponse;
import com.navdy.service.library.events.debug.StopDrivePlaybackEvent;
import com.navdy.service.library.events.debug.StopDriveRecordingEvent;
import com.navdy.service.library.events.debug.StopDriveRecordingResponse;
import com.squareup.otto.Subscribe;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class RecordingServiceManager {
    private static final RecordingServiceManager sInstance = new RecordingServiceManager();
    private List<WeakReference<Listener>> mListeners = new ArrayList();

    public interface Listener {
        void onDriveRecordingsResponse(List<String> list);
    }

    public static RecordingServiceManager getInstance() {
        return sInstance;
    }

    private RecordingServiceManager() {
        BusProvider.getInstance().register(this);
    }

    public void addListener(WeakReference<Listener> listener) {
        this.mListeners.add(listener);
    }

    public void removeListener(Listener listener) {
        this.mListeners.remove(new WeakReference(listener));
    }

    public void sendStartDriveRecordingEvent(String label) {
        DeviceConnection.postEvent(new StartDriveRecordingEvent(label));
    }

    public void sendStopDriveRecordingEvent() {
        DeviceConnection.postEvent(new StopDriveRecordingEvent());
    }

    public void sendStartDrivePlaybackEvent(String label) {
        DeviceConnection.postEvent(new StartDrivePlaybackEvent(label, Boolean.valueOf(false)));
    }

    public void sendStopDrivePlaybackEvent() {
        DeviceConnection.postEvent(new StopDrivePlaybackEvent());
    }

    public void sendDriveRecordingsRequest() {
        DeviceConnection.postEvent(new DriveRecordingsRequest());
    }

    @Subscribe
    public void onStartDrivePlaybackResponse(StartDrivePlaybackResponse response) {
        if (response.status == RequestStatus.REQUEST_SUCCESS) {
            Toast.makeText(NavdyApplication.getAppContext(), "Drive playback started successfully", 0).show();
        } else {
            Toast.makeText(NavdyApplication.getAppContext(), "Drive playback ERROR", 0).show();
        }
    }

    @Subscribe
    public void onStartDriveRecordingResponse(StartDriveRecordingResponse response) {
        if (response.status == RequestStatus.REQUEST_SUCCESS) {
            Toast.makeText(NavdyApplication.getAppContext(), "Drive recording started successfully", 0).show();
        } else {
            Toast.makeText(NavdyApplication.getAppContext(), "Drive recording ERROR", 0).show();
        }
    }

    @Subscribe
    public void onDriveRecordingsResponse(DriveRecordingsResponse response) {
        for (WeakReference<Listener> listenerRef : this.mListeners) {
            Listener listener = (Listener) listenerRef.get();
            if (listener != null) {
                listener.onDriveRecordingsResponse(response.recordings);
            }
        }
    }

    @Subscribe
    public void onStopDriveRecordingResponse(StopDriveRecordingResponse response) {
        if (response.status == RequestStatus.REQUEST_SUCCESS) {
            Toast.makeText(NavdyApplication.getAppContext(), "Drive recording was successfully saved!", 0).show();
        } else {
            Toast.makeText(NavdyApplication.getAppContext(), "Drive recording finished with ERROR", 0).show();
        }
    }
}
