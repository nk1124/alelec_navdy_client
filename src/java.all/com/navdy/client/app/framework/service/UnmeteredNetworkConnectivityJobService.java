package com.navdy.client.app.framework.service;

import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build.VERSION;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.service.DataCollectionService;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;

public class UnmeteredNetworkConnectivityJobService extends JobService {
    public static final int UNMETERED_NETWORK_SERVICE = 101;
    public static final Logger logger = new Logger(UnmeteredNetworkConnectivityJobService.class);

    public boolean onStartJob(JobParameters jobParameters) {
        logger.d("onStartJob");
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DataCollectionService.sendDataToS3IfOnWiFi();
            }
        }, 1);
        return false;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                DataCollectionService.getInstance().cancelAnyOngoingUpload();
            }
        }, 1);
        boolean pendingSensorDataExist = SettingsUtils.getSharedPreferences().getBoolean(SettingsConstants.PENDING_SENSOR_DATA_EXIST, false);
        logger.d("onStopJob - JobService will reschedule: " + pendingSensorDataExist);
        return pendingSensorDataExist;
    }

    public static void scheduleNetworkServiceForNougatOrAbove() {
        if (VERSION.SDK_INT >= 24) {
            Context context = NavdyApplication.getAppContext();
            JobScheduler jobScheduler = (JobScheduler) context.getSystemService(JobScheduler.class);
            if (jobScheduler == null || jobScheduler.getPendingJob(101) == null) {
                logger.d("Schedule network connectivity job service");
                JobInfo job = new Builder(101, new ComponentName(context, UnmeteredNetworkConnectivityJobService.class)).setRequiredNetworkType(2).build();
                if (jobScheduler != null) {
                    jobScheduler.schedule(job);
                    return;
                }
                return;
            }
            logger.d("NetworkConnectivityJobService already pending");
        }
    }
}
