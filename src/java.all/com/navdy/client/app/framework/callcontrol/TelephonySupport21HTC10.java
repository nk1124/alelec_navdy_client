package com.navdy.client.app.framework.callcontrol;

import android.content.Context;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.io.IOException;

public class TelephonySupport21HTC10 extends TelephonySupport21 {
    private static final Logger sLogger = new Logger(TelephonySupport21HTC10.class);

    public TelephonySupport21HTC10(Context context) {
        super(context);
    }

    public void acceptRingingCall() {
        super.acceptRingingCall();
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    Runtime.getRuntime().exec("input keyevent 79");
                } catch (IOException e) {
                    TelephonySupport21HTC10.sLogger.e("Exception while accepting call on HTC10");
                }
            }
        }, 1);
    }
}
