package com.navdy.client.app.framework.callcontrol;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.session.MediaController;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;

public class TelephonySupport21_SAMSUNG_G950 extends TelephonySupport21 {
    private static final Logger sLogger = new Logger(TelephonySupport21_SAMSUNG_G950.class);

    public TelephonySupport21_SAMSUNG_G950(Context context) {
        super(context);
    }

    @TargetApi(21)
    protected void reject(MediaController m) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                try {
                    TelephonySupport21_SAMSUNG_G950.sLogger.i("reject call:using telephony interface");
                    TelephonySupport.endPhoneCall();
                } catch (Throwable t) {
                    TelephonySupport21_SAMSUNG_G950.sLogger.e("endCall failed", t);
                }
            }
        }, 4);
    }
}
