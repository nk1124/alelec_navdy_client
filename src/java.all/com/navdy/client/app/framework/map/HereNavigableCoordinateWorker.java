package com.navdy.client.app.framework.map;

import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.maps.model.LatLng;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.Error;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback;
import com.navdy.client.app.framework.models.CalendarEvent;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.DestinationCacheEntry;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.util.ArrayList;
import java.util.List;

public class HereNavigableCoordinateWorker {
    private static final int MIN_WORDS_IN_ADDRESS = 4;
    private static final Object lockObj = new Object();
    private static final Logger logger = new Logger(HereNavigableCoordinateWorker.class);
    private final List<CalendarEvent> calendarEvents;
    private NavigableCoordinateWorkerFinishCallback callback;
    private int currentCalendarEventIndex = -1;
    private final List<CalendarEvent> processedCalendarEvents;
    private final Handler uiThreadHandler;

    public interface NavigableCoordinateWorkerFinishCallback {
        void onFinish(List<CalendarEvent> list);
    }

    public HereNavigableCoordinateWorker(List<CalendarEvent> list, NavigableCoordinateWorkerFinishCallback callback) {
        this.calendarEvents = list;
        this.processedCalendarEvents = new ArrayList();
        this.uiThreadHandler = new Handler(Looper.getMainLooper());
        start(callback);
    }

    private void start(NavigableCoordinateWorkerFinishCallback callback) {
        this.callback = callback;
        processNextCalendarEvent();
    }

    /* JADX WARNING: Missing block: B:18:?, code:
            return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processNextCalendarEvent() {
        synchronized (lockObj) {
            this.currentCalendarEventIndex++;
            if (this.calendarEvents != null && this.calendarEvents.size() > 0 && this.calendarEvents.size() > this.currentCalendarEventIndex) {
                final CalendarEvent calendarEvent = (CalendarEvent) this.calendarEvents.get(this.currentCalendarEventIndex);
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        HereNavigableCoordinateWorker.this.process(calendarEvent);
                    }
                }, 1);
            } else if (this.callback != null) {
                this.uiThreadHandler.post(new Runnable() {
                    public void run() {
                        HereNavigableCoordinateWorker.this.callback.onFinish(HereNavigableCoordinateWorker.this.processedCalendarEvents);
                    }
                });
            }
        }
    }

    private void process(final CalendarEvent calendarEvent) {
        if (StringUtils.isEmptyAfterTrim(calendarEvent.location)) {
            logger.v("Now processing: The address is missing for: " + calendarEvent.toString());
            processNextCalendarEvent();
        } else if (calendarEvent.location.contains(",")) {
            String[] words = calendarEvent.location.replaceAll("\\W", " ").replaceAll("[0-9]", " ").trim().split(" +");
            if (words.length < 4) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < words.length; i++) {
                    sb.append(words[i]);
                    if (i + 1 < words.length) {
                        sb.append(",");
                    }
                }
                logger.v("Now processing: Not enough words: words are: " + sb.toString() + ", event: " + calendarEvent.toString());
                processNextCalendarEvent();
                return;
            }
            OnCompleteCallback onCompleteCallback = new OnCompleteCallback() {
                public void onSuccess(Destination destination) {
                    HereNavigableCoordinateWorker.logger.d("Now processing: calendar event: " + calendarEvent.toString() + "; destination:" + destination);
                    if (destination.precisionLevel.isPrecise() && destination.hasValidNavCoordinates()) {
                        HereNavigableCoordinateWorker.logger.d("Now processing: Using the navCoords");
                        HereNavigableCoordinateWorker.this.addToProcessedEventsListAndCallBack(calendarEvent, destination.navigationLat, destination.navigationLong);
                    } else if (destination.precisionLevel.isPrecise() && destination.hasValidDisplayCoordinates()) {
                        HereNavigableCoordinateWorker.logger.d("Now processing: Using the displayCoords");
                        HereNavigableCoordinateWorker.this.addToProcessedEventsListAndCallBack(calendarEvent, destination.navigationLat, destination.navigationLong);
                    } else {
                        onFailure(destination, Error.BAD_COORDS);
                    }
                }

                public void onFailure(Destination destination, Error error) {
                    HereNavigableCoordinateWorker.logger.d("Now processing: No coordinates found for: " + calendarEvent.toString());
                    HereNavigableCoordinateWorker.this.processNextCalendarEvent();
                }
            };
            DestinationCacheEntry entry = NavdyContentProvider.getCacheEntryIfExists(calendarEvent.location);
            if (entry == null) {
                NavCoordsAddressProcessor.processDestination(new Destination(calendarEvent.displayName, calendarEvent.location), onCompleteCallback);
            } else if (entry.destination != null) {
                logger.v("destination with address " + calendarEvent.location + "found in cache, returning");
                onCompleteCallback.onSuccess(entry.destination);
            } else {
                onCompleteCallback.onFailure(null, Error.CACHE_NO_COORDS);
            }
        } else {
            logger.v("Now processing: The address does not contain a comma: " + calendarEvent.toString());
            processNextCalendarEvent();
        }
    }

    private void addToProcessedEventsListAndCallBack(CalendarEvent calendarEvent, double navigationLat, double navigationLng) {
        synchronized (lockObj) {
            calendarEvent.latLng = new LatLng(navigationLat, navigationLng);
            this.processedCalendarEvents.add(calendarEvent);
        }
        processNextCalendarEvent();
    }
}
