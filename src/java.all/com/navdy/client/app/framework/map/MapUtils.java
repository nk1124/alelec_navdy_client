package com.navdy.client.app.framework.map;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPolyline;
import com.here.android.mpa.mapping.MapPolyline;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.location.Coordinate.Builder;
import com.navdy.service.library.events.navigation.NavigationRouteResult;
import com.navdy.service.library.log.Logger;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jetbrains.annotations.Contract;

public class MapUtils {
    private static final int EARTH_RADIUS = 6371000;
    private static final double MAX_LAT = Math.toRadians(90.0d);
    private static final double MAX_LON = Math.toRadians(180.0d);
    private static final double METERS_PER_FOOT = 0.3048d;
    private static final int METERS_PER_KM = 1000;
    public static final double METERS_PER_MILE = 1609.34d;
    private static final double MIN_LAT = Math.toRadians(-90.0d);
    private static final double MIN_LON = Math.toRadians(-180.0d);
    private static final boolean VERBOSE = false;
    public static final int hereMapSidePadding;
    public static final int hereMapTopDownPadding;
    private static final Logger logger = new Logger(MapUtils.class);

    private enum UnitSystem {
        METRIC,
        IMPERIAL
    }

    static {
        Context context = NavdyApplication.getAppContext();
        BitmapDrawable markerDrawable = (BitmapDrawable) ContextCompat.getDrawable(context, R.drawable.icon_pin_place);
        BitmapDrawable positionMarkerDrawable = (BitmapDrawable) ContextCompat.getDrawable(context, R.drawable.icon_user_location);
        hereMapSidePadding = Math.max(markerDrawable.getBitmap().getWidth(), positionMarkerDrawable.getBitmap().getWidth());
        hereMapTopDownPadding = Math.max(markerDrawable.getBitmap().getHeight(), positionMarkerDrawable.getBitmap().getHeight());
    }

    public static List<LatLng> getGmsLatLongsFromRouteResult(NavigationRouteResult routeResult) {
        List<LatLng> result = new ArrayList();
        if (!(routeResult == null || routeResult.routeLatLongs == null || routeResult.routeLatLongs.size() % 2 == 1)) {
            ListIterator<Float> iterator = routeResult.routeLatLongs.listIterator();
            while (iterator.hasNext()) {
                result.add(new LatLng((double) ((Float) iterator.next()).floatValue(), (double) ((Float) iterator.next()).floatValue()));
            }
        }
        return result;
    }

    public static String formatDistance(int meters, UnitSystem unitSystem) {
        Resources resources = NavdyApplication.getAppContext().getResources();
        Object[] objArr;
        double miles;
        if (unitSystem == UnitSystem.METRIC) {
            double kms;
            if (((float) meters) >= 10000.0f) {
                kms = (double) Math.round(((double) meters) / 1000.0d);
                return resources.getString(R.string.kilometers_abbrev, new Object[]{String.valueOf((int) kms)});
            } else if (meters > 1000) {
                kms = ((double) Math.round((((double) meters) / 1000.0d) * 10.0d)) / 10.0d;
                objArr = new Object[1];
                objArr[0] = String.format(Locale.getDefault(), "%.1f", new Object[]{Double.valueOf(kms)});
                return resources.getString(R.string.kilometers_abbrev, objArr);
            } else {
                return resources.getString(R.string.meters_abbrev, new Object[]{String.valueOf(meters)});
            }
        } else if (((double) meters) >= 16093.4d) {
            miles = (double) Math.round(((double) meters) / 1609.34d);
            return resources.getString(R.string.miles_abbrev, new Object[]{String.valueOf((int) miles)});
        } else if (((double) meters) >= 160.934d) {
            miles = ((double) Math.round((((double) meters) / 1609.34d) * 10.0d)) / 10.0d;
            objArr = new Object[1];
            objArr[0] = String.format(Locale.getDefault(), "%.1f", new Object[]{Double.valueOf(miles)});
            return resources.getString(R.string.miles_abbrev, objArr);
        } else {
            double feet = ((double) Math.round((((double) meters) / METERS_PER_FOOT) * 10.0d)) / 10.0d;
            return resources.getString(R.string.feet_abbrev, new Object[]{String.valueOf(feet)});
        }
    }

    public static void setMarkerImageResource(Marker marker, int resId) {
        if (marker != null) {
            try {
                marker.setIcon(BitmapDescriptorFactory.fromResource(resId));
            } catch (Exception e) {
                logger.e("Unable to set the marker image. marker = " + marker);
            }
        }
    }

    @Nullable
    public static MapPolyline generateRoutePolyline(@NonNull GeoPolyline route) {
        return generatePolyline(route, R.color.blue_here_maps, R.integer.current_route_z_index);
    }

    @Nullable
    public static MapPolyline generateProgressPolyline(@NonNull GeoPolyline progress) {
        return generatePolyline(progress, R.color.black, R.integer.current_progress_z_index);
    }

    @Nullable
    public static MapPolyline generateAlternateRoutePolyline(@NonNull GeoPolyline geoPolyline) {
        return generatePolyline(geoPolyline, R.color.grey_here_maps, R.integer.alternate_route_z_index);
    }

    @Contract("null, _ -> fail; !null, null -> fail")
    public static float distanceBetween(Location location1, Location location2) {
        if (location1 != null && location2 != null) {
            return distanceBetween(location1.getLatitude(), location1.getLongitude(), location2.getLatitude(), location2.getLongitude());
        }
        throw new IllegalArgumentException("one or more arguments are null");
    }

    @Contract("null, _ -> fail; !null, null -> fail")
    public static float distanceBetween(Coordinate coordinate, Location location) {
        if (coordinate != null && location != null) {
            return distanceBetween(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue(), location.getLatitude(), location.getLongitude());
        }
        throw new IllegalArgumentException("one or more arguments are null");
    }

    @Contract("null, _ -> fail; !null, null -> fail")
    public static double distanceBetween(Coordinate first, Coordinate second) {
        if (first != null && second != null) {
            return (double) distanceBetween(first.latitude.doubleValue(), first.longitude.doubleValue(), second.latitude.doubleValue(), second.longitude.doubleValue());
        }
        throw new IllegalArgumentException("one or more arguments are null");
    }

    @Contract("null, _ -> fail; !null, null -> fail")
    public static float distanceBetween(LatLng first, LatLng second) {
        if (first != null && second != null) {
            return distanceBetween(first.latitude, first.longitude, second.latitude, second.longitude);
        }
        throw new IllegalArgumentException("one or more arguments are null");
    }

    @Contract("null, _ -> fail; !null, null -> fail")
    public static float distanceBetween(Coordinate coordinate, LatLng latLng) {
        if (coordinate != null && latLng != null) {
            return distanceBetween(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue(), latLng.latitude, latLng.longitude);
        }
        throw new IllegalArgumentException("one or more arguments are null");
    }

    private static float distanceBetween(double firstLat, double firstLng, double secondLat, double secondLng) {
        float[] results = new float[1];
        Location.distanceBetween(firstLat, firstLng, secondLat, secondLng, results);
        return results[0];
    }

    private static boolean areCloseToEachOther(Destination d1, Destination d2, double coordLimit, float distanceLimit) {
        return areCloseToEachOther(d1.displayLat, d1.displayLong, d2.displayLat, d2.displayLong, coordLimit, distanceLimit) || areCloseToEachOther(d1.navigationLat, d1.navigationLong, d2.navigationLat, d2.navigationLong, coordLimit, distanceLimit) || areCloseToEachOther(d1.displayLat, d1.displayLong, d2.navigationLat, d2.navigationLong, coordLimit, distanceLimit) || areCloseToEachOther(d1.navigationLat, d1.navigationLong, d2.displayLat, d2.displayLong, coordLimit, distanceLimit);
    }

    private static boolean areCloseToEachOther(double latitude1, double longitude1, double latitude2, double longitude2, double coordLimit, float distanceLimit) {
        return !(latitude1 == 0.0d && longitude1 == 0.0d) && (!(latitude2 == 0.0d && longitude2 == 0.0d) && Math.abs(latitude1 - latitude2) < coordLimit && Math.abs(longitude1 - longitude2) < coordLimit && distanceBetween(latitude1, longitude1, latitude2, longitude2) < distanceLimit);
    }

    public static Location[] getBoundingBox(double latitude, double longitude, double distance) {
        double minLon;
        double maxLon;
        double latRad = Math.toRadians(latitude);
        double longRad = Math.toRadians(longitude);
        double angularDistance = distance / 6371000.0d;
        double minLat = latRad - angularDistance;
        double maxLat = latRad + angularDistance;
        if (minLat <= MIN_LAT || maxLat >= MAX_LAT) {
            minLat = Math.max(minLat, MIN_LAT);
            maxLat = Math.min(maxLat, MAX_LAT);
            minLon = MIN_LON;
            maxLon = MAX_LON;
        } else {
            double deltaLon = Math.asin(Math.sin(angularDistance) / Math.cos(latRad));
            minLon = longRad - deltaLon;
            if (minLon < MIN_LON) {
                minLon += 6.283185307179586d;
            }
            maxLon = longRad + deltaLon;
            if (maxLon > MAX_LON) {
                maxLon -= 6.283185307179586d;
            }
        }
        Location min = new Location("");
        min.setLatitude(Math.toDegrees(minLat));
        min.setLongitude(Math.toDegrees(minLon));
        Location max = new Location("");
        max.setLatitude(Math.toDegrees(maxLat));
        max.setLongitude(Math.toDegrees(maxLon));
        return new Location[]{min, max};
    }

    public static boolean isValidSetOfCoordinates(double latitude, double longitude) {
        return (latitude == 0.0d && longitude == 0.0d) ? false : true;
    }

    public static boolean isValidSetOfCoordinates(LatLng latLng) {
        return latLng != null && isValidSetOfCoordinates(latLng.latitude, latLng.longitude);
    }

    public static boolean isValidSetOfCoordinates(Coordinate coordinates) {
        return coordinates != null && isValidSetOfCoordinates(coordinates.latitude.doubleValue(), coordinates.longitude.doubleValue());
    }

    public static boolean isValidSetOfCoordinates(Location location) {
        return location != null && isValidSetOfCoordinates(location.getLatitude(), location.getLongitude());
    }

    public static boolean isValidSetOfCoordinates(GeoCoordinate coordinate) {
        return coordinate != null && isValidSetOfCoordinates(coordinate.getLatitude(), coordinate.getLongitude());
    }

    public static Coordinate buildNewCoordinate(double latitude, double longitude) {
        return new Builder().latitude(Double.valueOf(latitude)).longitude(Double.valueOf(longitude)).build();
    }

    private static MapPolyline generatePolyline(@Nullable GeoPolyline geoPolyline, int colorId, int zIndexId) {
        if (geoPolyline == null) {
            return null;
        }
        MapPolyline mapPolyline = new MapPolyline(geoPolyline);
        Resources resources = NavdyApplication.getAppContext().getResources();
        mapPolyline.setLineWidth(resources.getInteger(R.integer.map_polyline_width));
        mapPolyline.setLineColor(ContextCompat.getColor(NavdyApplication.getAppContext(), colorId));
        mapPolyline.setZIndex(resources.getInteger(zIndexId));
        return mapPolyline;
    }

    public static void doReverseGeocodingFor(@NonNull final LatLng center, @NonNull final Destination destination, @NonNull final Runnable callback) {
        if (!isValidSetOfCoordinates(center)) {
            logger.v("Trying to reverse geocode a location with invalid coordinates");
            callback.run();
        }
        new AsyncTask<Object, Object, List<Address>>() {
            protected List<Address> doInBackground(Object[] params) {
                if (AppInstance.getInstance().canReachInternet()) {
                    try {
                        return new Geocoder(NavdyApplication.getAppContext(), Locale.getDefault()).getFromLocation(center.latitude, center.longitude, 1);
                    } catch (Exception e) {
                        MapUtils.logger.e("Unable to reverse geocode the current position.", e);
                    }
                } else {
                    if (destination.hasValidDisplayCoordinates()) {
                        MapUtils.logger.v("processDestination, destination does not have address, assigning display coordinates as address");
                        destination.setRawAddressNotForDisplay(destination.displayLat + "," + destination.displayLong);
                    } else if (destination.hasValidNavCoordinates()) {
                        MapUtils.logger.v("processDestination, destination does not have address, assigning nav coordinates as address");
                        destination.setRawAddressNotForDisplay(destination.navigationLat + "," + destination.navigationLong);
                    }
                    return null;
                }
            }

            protected void onPostExecute(List<Address> addresses) {
                if (!(addresses == null || addresses.isEmpty())) {
                    Address address = (Address) addresses.get(0);
                    destination.setStreetNumber(address.getFeatureName());
                    destination.setStreetName(address.getThoroughfare());
                    destination.setCity(address.getLocality());
                    destination.setZipCode(address.getPostalCode());
                    destination.setState(address.getAdminArea());
                    destination.setCountry(address.getCountryName());
                    destination.setCountryCode(address.getCountryCode());
                    destination.setName(address.getAddressLine(0));
                    ArrayList<String> addressFragments = new ArrayList();
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        addressFragments.add(address.getAddressLine(i));
                    }
                    String addressString = TextUtils.join(System.getProperty("line.separator"), addressFragments);
                    MapUtils.logger.d("Full address = " + addressString);
                    destination.setRawAddressNotForDisplay(addressString);
                }
                callback.run();
            }
        }.execute(new Object[0]);
    }

    public static LatLng parseLatLng(String string) {
        if (StringUtils.isEmptyAfterTrim(string)) {
            return null;
        }
        String str = string;
        try {
            str = URLDecoder.decode(string, Charset.defaultCharset().name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Matcher matcher = Pattern.compile(StringUtils.LAT_LNG_REGEXP).matcher(str);
        if (matcher.matches() && matcher.groupCount() == 2) {
            try {
                float lat = Float.parseFloat(matcher.group(1));
                float lng = Float.parseFloat(matcher.group(2));
                if (lat == 0.0f && lng == 0.0f) {
                    return null;
                }
                return new LatLng((double) lat, (double) lng);
            } catch (NumberFormatException e2) {
                logger.v("Non-float lat/lng: " + str);
                return null;
            }
        }
        logger.v("Can't parse lat/lng: " + str);
        return null;
    }
}
