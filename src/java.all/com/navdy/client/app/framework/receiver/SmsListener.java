package com.navdy.client.app.framework.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.glances.GlanceConstants;
import com.navdy.client.app.framework.glances.GlancesHelper;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.client.debug.util.Contacts;
import com.navdy.client.debug.util.NotificationBuilder;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.glances.GlanceEvent.Builder;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceActions;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.events.glances.MessageConstants;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;

public class SmsListener extends BroadcastReceiver {
    private static final String ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final Logger sLogger = new Logger(SmsListener.class);

    public void onReceive(Context context, Intent intent) {
        if (GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(GlanceConstants.SMS_PACKAGE)) {
            RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice != null && remoteDevice.getDeviceInfo() != null && !StringUtils.isEmptyAfterTrim(remoteDevice.getDeviceInfo().protocolVersion)) {
                String protocol = remoteDevice.getDeviceInfo().protocolVersion;
                if (intent.getAction().equals(ACTION_SMS_RECEIVED)) {
                    SmsMessage[] msgs = getMessagesFromIntent(intent);
                    if (msgs != null) {
                        sLogger.d("[navdyinfo-sms] sms message received");
                        for (SmsMessage msg : msgs) {
                            String address = msg.getOriginatingAddress();
                            String contactName = Contacts.lookupNameFromPhoneNumber(context, address);
                            String body = msg.getMessageBody();
                            if (GlancesHelper.isHudVersionCompatible(protocol)) {
                                String id = GlancesHelper.getId();
                                List<KeyValue> data = new ArrayList();
                                data.add(new KeyValue(MessageConstants.MESSAGE_FROM.name(), contactName));
                                data.add(new KeyValue(MessageConstants.MESSAGE_FROM_NUMBER.name(), address));
                                data.add(new KeyValue(MessageConstants.MESSAGE_IS_SMS.name(), ""));
                                data.add(new KeyValue(MessageConstants.MESSAGE_BODY.name(), body));
                                List<GlanceActions> actions = new ArrayList();
                                actions.add(GlanceActions.REPLY);
                                GlancesHelper.sendEvent(new Builder().glanceType(GlanceType.GLANCE_TYPE_MESSAGE).provider(GlanceConstants.SMS_PACKAGE).id(id).glanceData(data).postTime(Long.valueOf(System.currentTimeMillis())).actions(actions).build());
                            } else {
                                remoteDevice.postEvent(NotificationBuilder.buildSmsNotification(contactName, address, body, false));
                            }
                        }
                    }
                }
            }
        }
    }

    protected SmsMessage[] getMessagesFromIntent(Intent intent) {
        Object[] messages = (Object[]) intent.getSerializableExtra("pdus");
        int pduCount = messages.length;
        SmsMessage[] msgs = new SmsMessage[pduCount];
        for (int i = 0; i < pduCount; i++) {
            msgs[i] = SmsMessage.createFromPdu((byte[]) messages[i]);
        }
        return msgs;
    }
}
