package com.navdy.client.app.framework.music;

import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.settings.AudioDialogActivity;
import com.navdy.service.library.events.audio.MusicPlaybackState;
import com.navdy.service.library.events.audio.MusicShuffleMode;
import com.navdy.service.library.events.audio.MusicTrackInfo;
import com.navdy.service.library.events.audio.MusicTrackInfo.Builder;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LocalMusicPlayer {
    private static final int PROGRESS_DELAY = 1000;
    private static final int SCRUB_DELAY = 300;
    private static final int SCRUB_STEP = 10000;
    private static final Logger logger = new Logger(LocalMusicPlayer.class);
    private final float DEFAULT_VOLUME = 1.0f;
    private final float DUCKING_VOLUME = 0.23f;
    private OnAudioFocusChangeListener audioFocusChangeListener = new OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            LocalMusicPlayer.logger.i("onAudioFocusChange " + focusChange);
            if (focusChange >= 1) {
                if (LocalMusicPlayer.this.isDuckedDueToAudioFocusLoss) {
                    LocalMusicPlayer.this.isDuckedDueToAudioFocusLoss = false;
                    LocalMusicPlayer.this.unduck();
                }
                if (LocalMusicPlayer.this.isPausedDueToAudioFocusLoss) {
                    LocalMusicPlayer.this.isPausedDueToAudioFocusLoss = false;
                    LocalMusicPlayer.this.play();
                }
            } else if (focusChange == -3) {
                LocalMusicPlayer.this.isDuckedDueToAudioFocusLoss = true;
                LocalMusicPlayer.this.duck();
            } else if (focusChange == -2) {
                LocalMusicPlayer.this.isPausedDueToAudioFocusLoss = true;
                LocalMusicPlayer.this.pause(false);
            } else {
                LocalMusicPlayer.this.pause(true);
            }
        }
    };
    private int currentPosition = -1;
    private int currentTrackIndex = -1;
    private Runnable fastForwardRunnable = new Runnable() {
        public void run() {
            if (LocalMusicPlayer.this.mediaPlayer != null) {
                LocalMusicPlayer.this.seek(10000);
                LocalMusicPlayer.this.handler.postDelayed(this, 300);
                return;
            }
            LocalMusicPlayer.logger.w("Media player is null");
        }
    };
    private long firstUnplayableTrack = -1;
    private Handler handler = new Handler(Looper.getMainLooper());
    private boolean isDuckedDueToAudioFocusLoss = false;
    private boolean isPausedDueToAudioFocusLoss = false;
    private Listener listener;
    private MediaPlayer mediaPlayer;
    private List<MusicTrackInfo> originalList;
    private Runnable progressUpdateRunnable = new Runnable() {
        public void run() {
            if (LocalMusicPlayer.this.listener == null) {
                return;
            }
            if (LocalMusicPlayer.this.mediaPlayer == null || !LocalMusicPlayer.this.mediaPlayer.isPlaying()) {
                LocalMusicPlayer.logger.w("Media player isn't playing - did you release it without clearing callbacks?");
                return;
            }
            LocalMusicPlayer.this.currentPosition = LocalMusicPlayer.this.mediaPlayer.getCurrentPosition();
            LocalMusicPlayer.this.listener.onPositionUpdate(LocalMusicPlayer.this.currentPosition, LocalMusicPlayer.this.mediaPlayer.getDuration());
            LocalMusicPlayer.this.handler.postDelayed(this, 1000);
        }
    };
    private Runnable rewindRunnable = new Runnable() {
        public void run() {
            if (LocalMusicPlayer.this.mediaPlayer != null) {
                LocalMusicPlayer.this.seek(-10000);
                LocalMusicPlayer.this.handler.postDelayed(this, 300);
                return;
            }
            LocalMusicPlayer.logger.w("Media player is null");
        }
    };
    private MusicShuffleMode shuffleMode = MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF;
    private List<MusicTrackInfo> tracks;

    public interface Listener {
        void onMetadataUpdate(MusicTrackInfo musicTrackInfo);

        void onPlaybackStateUpdate(MusicPlaybackState musicPlaybackState);

        void onPositionUpdate(int i, int i2);
    }

    public void initWithQueue(List<MusicTrackInfo> tracks) {
        logger.d("init");
        this.originalList = tracks;
        this.tracks = this.originalList;
        this.shuffleMode = MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF;
        this.currentTrackIndex = -1;
        this.currentPosition = -1;
        this.firstUnplayableTrack = -1;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public Listener getListener() {
        return this.listener;
    }

    public boolean isActive() {
        return this.tracks != null;
    }

    public void play(int index) {
        logger.i("play " + index);
        this.handler.removeCallbacks(this.progressUpdateRunnable);
        if (index == this.currentTrackIndex && this.mediaPlayer != null && this.mediaPlayer.getCurrentPosition() == this.currentPosition) {
            this.mediaPlayer.start();
            return;
        }
        createMediaPlayer(index);
        if (this.mediaPlayer == null) {
            logger.e("MediaPlayer init failed");
            this.currentTrackIndex = index;
            if (((long) index) != this.firstUnplayableTrack) {
                this.handler.post(new Runnable() {
                    public void run() {
                        LocalMusicPlayer.this.next();
                    }
                });
            } else {
                logger.e("No playable tracks in the queue");
            }
            if (this.firstUnplayableTrack == -1) {
                this.firstUnplayableTrack = (long) index;
                return;
            }
            return;
        }
        this.firstUnplayableTrack = -1;
        if (this.isDuckedDueToAudioFocusLoss) {
            this.mediaPlayer.setVolume(0.23f, 0.23f);
        }
        this.mediaPlayer.start();
        if (index == this.currentTrackIndex && this.currentPosition != -1) {
            logger.i("resume - seeking to position " + this.currentPosition);
            this.mediaPlayer.seekTo(this.currentPosition);
        }
        this.currentTrackIndex = index;
        this.mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                LocalMusicPlayer.logger.i("onCompletion");
                LocalMusicPlayer.this.next();
            }
        });
        if (this.listener != null) {
            this.listener.onMetadataUpdate(new Builder((MusicTrackInfo) this.tracks.get(this.currentTrackIndex)).shuffleMode(this.shuffleMode).build());
            this.handler.post(this.progressUpdateRunnable);
        }
    }

    private void createMediaPlayer(int index) {
        if (this.mediaPlayer != null) {
            releaseCurrentPlayer();
        }
        if (index < 0 || index >= this.tracks.size()) {
            logger.e("Index out of range");
            return;
        }
        String trackIdStr = ((MusicTrackInfo) this.tracks.get(index)).trackId;
        try {
            Uri uri = Uri.parse("content://media/external/audio/media/" + Long.parseLong(trackIdStr));
            logger.d("creating media player for index " + index + ", URI: " + uri);
            int result = getAudioManager().requestAudioFocus(this.audioFocusChangeListener, 3, 1);
            if (result != 1) {
                logger.w("Couldn't get audio focus: " + result);
            }
            try {
                this.mediaPlayer = MediaPlayer.create(NavdyApplication.getAppContext(), uri);
            } catch (Throwable t) {
                logger.e("MediaPlayer.create() threw " + t);
            }
        } catch (NumberFormatException e) {
            logger.w("Invalid track ID " + trackIdStr);
        }
    }

    public void play() {
        play(this.currentTrackIndex);
    }

    public void pause(boolean releaseMediaPlayer) {
        logger.i("pause " + this.currentTrackIndex);
        this.handler.removeCallbacks(this.progressUpdateRunnable);
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
            return;
        }
        this.currentPosition = this.mediaPlayer.getCurrentPosition();
        if (this.listener != null) {
            this.listener.onPlaybackStateUpdate(MusicPlaybackState.PLAYBACK_PAUSED);
        }
        this.mediaPlayer.pause();
        if (releaseMediaPlayer) {
            releaseCurrentPlayer();
        }
    }

    public void next() {
        this.currentPosition = -1;
        this.currentTrackIndex++;
        int numTracks = this.tracks.size();
        if (numTracks > 0) {
            this.currentTrackIndex %= numTracks;
        }
        play(this.currentTrackIndex);
    }

    public void previous() {
        this.currentPosition = -1;
        if (this.mediaPlayer == null || this.mediaPlayer.getCurrentPosition() <= AudioDialogActivity.DELAY_MILLIS) {
            this.currentTrackIndex--;
            int numTracks = this.tracks.size();
            if (numTracks > 0) {
                this.currentTrackIndex = (this.currentTrackIndex + numTracks) % numTracks;
            } else {
                this.currentTrackIndex = 0;
            }
            play(this.currentTrackIndex);
            return;
        }
        this.mediaPlayer.seekTo(0);
    }

    public void startFastForward() {
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
        } else {
            this.handler.post(this.fastForwardRunnable);
        }
    }

    public void stopFastForward() {
        this.handler.removeCallbacks(this.fastForwardRunnable);
    }

    public void startRewind() {
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
        } else {
            this.handler.post(this.rewindRunnable);
        }
    }

    public void stopRewind() {
        this.handler.removeCallbacks(this.rewindRunnable);
    }

    public void shuffle(MusicShuffleMode shuffleMode) {
        if (this.shuffleMode == shuffleMode) {
            logger.e("Already set shuffle mode " + shuffleMode);
            return;
        }
        String currentTrackId = null;
        if (this.currentTrackIndex != -1) {
            currentTrackId = ((MusicTrackInfo) this.tracks.get(this.currentTrackIndex)).trackId;
        }
        if (shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_SONGS) {
            List<MusicTrackInfo> shuffledList = new ArrayList(this.tracks);
            Collections.shuffle(shuffledList);
            this.tracks = shuffledList;
        } else if (shuffleMode == MusicShuffleMode.MUSIC_SHUFFLE_MODE_OFF) {
            this.tracks = this.originalList;
        } else {
            logger.e("Unsupported shuffle mode " + shuffleMode);
            return;
        }
        if (currentTrackId != null) {
            for (int i = 0; i < this.tracks.size(); i++) {
                if (StringUtils.equalsOrBothEmptyAfterTrim(((MusicTrackInfo) this.tracks.get(i)).trackId, currentTrackId)) {
                    this.currentTrackIndex = i;
                    break;
                }
            }
        }
        this.shuffleMode = shuffleMode;
        if (this.listener != null && this.currentTrackIndex != -1) {
            this.listener.onMetadataUpdate(new Builder((MusicTrackInfo) this.tracks.get(this.currentTrackIndex)).shuffleMode(shuffleMode).build());
        }
    }

    private void seek(int positionChange) {
        this.currentPosition = this.mediaPlayer.getCurrentPosition();
        this.mediaPlayer.seekTo(this.currentPosition + positionChange);
    }

    private void duck() {
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
        } else {
            this.mediaPlayer.setVolume(0.23f, 0.23f);
        }
    }

    private void unduck() {
        if (this.mediaPlayer == null) {
            logger.e("no mediaPlayer!");
        } else {
            this.mediaPlayer.setVolume(1.0f, 1.0f);
        }
    }

    private void releaseCurrentPlayer() {
        this.handler.removeCallbacksAndMessages(null);
        this.mediaPlayer.reset();
        this.mediaPlayer.release();
        this.mediaPlayer = null;
        if (!this.isDuckedDueToAudioFocusLoss) {
            getAudioManager().abandonAudioFocus(this.audioFocusChangeListener);
        }
    }

    private AudioManager getAudioManager() {
        return (AudioManager) NavdyApplication.getAppContext().getSystemService("audio");
    }
}
