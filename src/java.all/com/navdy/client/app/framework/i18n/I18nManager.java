package com.navdy.client.app.framework.i18n;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.servicehandler.DestinationsServiceHandler.DestinationType;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.VersioningUtils;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

public class I18nManager {
    private static final List<String> IMPERIAL_UNIT_SYSTEM_LOCALES = new ArrayList();
    private static final String LIBERIA_COUNTRY = "LR";
    public static final double METERS_TO_KILOMETERS = 0.001d;
    public static final double METERS_TO_MILES = 6.21371E-4d;
    private static final String MYANMAR_COUNTRY = "MM";
    private static final I18nManager instance = new I18nManager();
    private static final Logger logger = new Logger(I18nManager.class);
    private static final Object unitSystemLock = new Object();
    private UnitSystem currentUnitSystem;
    private final List<WeakReference<Listener>> listeners;

    private interface CallListener {
        void call(Listener listener);
    }

    public interface Listener {
        void onUnitSystemChanged(UnitSystem unitSystem);
    }

    private class LocaleReceiver extends BroadcastReceiver {
        private LocaleReceiver() {
        }

//        /* synthetic */ LocaleReceiver(I18nManager x0, AnonymousClass1 x1) {
//            this();
//        }

        public void onReceive(Context context, Intent intent) {
            if (intent != null && StringUtils.equalsOrBothEmptyAfterTrim(intent.getAction(), "android.intent.action.LOCALE_CHANGED")) {
                I18nManager.logger.i("Locale has changed, new locale is " + Locale.getDefault());
                if (SettingsUtils.getUnitSystem() == null) {
                    UnitSystem unitSystem;
                    if (I18nManager.this.driverCountryIsImperial()) {
                        unitSystem = UnitSystem.UNIT_SYSTEM_IMPERIAL;
                    } else {
                        unitSystem = UnitSystem.UNIT_SYSTEM_METRIC;
                    }
                    synchronized (I18nManager.unitSystemLock) {
                        I18nManager.this.currentUnitSystem = unitSystem;
                        I18nManager.this.callListeners(new CallListener() {
                            public void call(Listener listener) {
                                listener.onUnitSystemChanged(I18nManager.this.currentUnitSystem);
                            }
                        });
                    }
                }
                VersioningUtils.increaseVersionAndSendToDisplay(DestinationType.FAVORITES);
                TaskManager.getInstance().execute(new Runnable() {
                    public void run() {
                        SettingsUtils.incrementDriverProfileSerial();
                        SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
                    }
                }, 1);
            }
        }
    }

    static {
        IMPERIAL_UNIT_SYSTEM_LOCALES.add(Locale.US.getCountry());
        IMPERIAL_UNIT_SYSTEM_LOCALES.add(Locale.UK.getCountry());
        IMPERIAL_UNIT_SYSTEM_LOCALES.add(LIBERIA_COUNTRY);
        IMPERIAL_UNIT_SYSTEM_LOCALES.add(MYANMAR_COUNTRY);
        AddressUtils.initAddressFormats();
    }

    public static I18nManager getInstance() {
        return instance;
    }

    private I18nManager() {
        UnitSystem unitSystem = SettingsUtils.getUnitSystem();
        if (unitSystem == null) {
            if (driverCountryIsImperial()) {
                unitSystem = UnitSystem.UNIT_SYSTEM_IMPERIAL;
            } else {
                unitSystem = UnitSystem.UNIT_SYSTEM_METRIC;
            }
        }
        this.currentUnitSystem = unitSystem;
        NavdyApplication.getAppContext().registerReceiver(new LocaleReceiver(this, null), new IntentFilter("android.intent.action.LOCALE_CHANGED"));
        this.listeners = new ArrayList();
    }

    private boolean driverCountryIsImperial() {
        return IMPERIAL_UNIT_SYSTEM_LOCALES.contains(Locale.getDefault().getCountry());
    }

    public void addListener(Listener listener) {
        UnitSystem unitSystem;
        this.listeners.add(new WeakReference(listener));
        synchronized (unitSystemLock) {
            unitSystem = this.currentUnitSystem;
        }
        listener.onUnitSystemChanged(unitSystem);
    }

    public void removeListener(Listener listener) {
        this.listeners.remove(new WeakReference(listener));
    }

    public UnitSystem getUnitSystem() {
        UnitSystem unitSystem;
        synchronized (unitSystemLock) {
            unitSystem = this.currentUnitSystem;
        }
        return unitSystem;
    }

    public void setUnitSystem(UnitSystem unitSystem) {
        synchronized (unitSystemLock) {
            this.currentUnitSystem = unitSystem;
            SettingsUtils.setUnitSystem(unitSystem);
            callListeners(new CallListener() {
                public void call(Listener listener) {
                    listener.onUnitSystemChanged(I18nManager.this.currentUnitSystem);
                }
            });
        }
    }

    private void callListeners(CallListener callListener) {
        ListIterator<WeakReference<Listener>> iterator = this.listeners.listIterator();
        while (iterator.hasNext()) {
            Listener listener = (Listener) ((WeakReference) iterator.next()).get();
            if (listener != null) {
                callListener.call(listener);
            } else {
                iterator.remove();
            }
        }
    }

    @Nullable
    public Locale getCurrentLocale() {
        Resources resources = NavdyApplication.getAppContext().getResources();
        if (resources == null) {
            return null;
        }
        Configuration configuration = resources.getConfiguration();
        if (configuration == null) {
            return null;
        }
        if (VERSION.SDK_INT >= 24) {
            return configuration.getLocales().get(0);
        }
        return configuration.locale;
    }
}
