package com.navdy.client.app.framework.glances;

import android.app.Notification;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.service.library.events.glances.GenericConstants;
import com.navdy.service.library.events.glances.GlanceEvent.Builder;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.KeyValue;
import java.util.ArrayList;
import java.util.List;

public class GenericNotificationHandler {
    public static void handleGenericNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        Notification notification = sbn.getNotification();
        if (GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName)) {
            String message = getBestAvailableText(notification);
            if (!StringUtils.isEmptyAfterTrim(message)) {
                String id = GlancesHelper.getId();
                List<KeyValue> data = new ArrayList();
                data.add(new KeyValue(GenericConstants.GENERIC_MESSAGE.name(), message));
                GlancesHelper.sendEvent(new Builder().glanceType(GlanceType.GLANCE_TYPE_GENERIC).provider(packageName).id(id).postTime(Long.valueOf(notification.when)).glanceData(data).build());
            }
        }
    }

    @Nullable
    private static String getBestAvailableText(Notification notification) {
        CharSequence ticker = notification.tickerText;
        Bundle extras = NotificationCompat.getExtras(notification);
        if (extras != null) {
            String bigText = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.bigText");
            if (!StringUtils.isEmptyAfterTrim(bigText)) {
                return bigText;
            }
            String text = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
            if (!StringUtils.isEmptyAfterTrim(text)) {
                return text;
            }
            String subText = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.subText");
            if (!StringUtils.isEmptyAfterTrim(subText)) {
                return subText;
            }
            String title = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.title");
            if (!StringUtils.isEmptyAfterTrim(title)) {
                return title;
            }
        }
        if (ticker != null) {
            return ticker.toString();
        }
        return null;
    }
}
