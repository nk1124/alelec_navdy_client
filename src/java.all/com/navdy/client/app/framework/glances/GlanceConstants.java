package com.navdy.client.app.framework.glances;

import com.navdy.service.library.log.Logger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GlanceConstants {
    public static final String ACTION_EMAIL_GUESTS = "Email guests";
    public static final String ACTION_MAP = "Map";
    public static final String ANDROID_BIG_TEXT = "android.bigText";
    public static final String ANDROID_CALENDAR = "com.android.calendar";
    public static final String ANDROID_PEOPLE = "android.people";
    public static final String ANDROID_SUB_TEXT = "android.subText";
    public static final String ANDROID_SUMMARY_TEXT = "android.summaryText";
    public static final String ANDROID_TEMPLATE = "android.template";
    public static final String ANDROID_TEXT = "android.text";
    public static final String ANDROID_TEXT_LINES = "android.textLines";
    public static final String ANDROID_TITLE = "android.title";
    public static final String APPLE_MUSIC = "com.apple.android.music";
    public static final String CAR_EXT = "android.car.EXTENSIONS";
    public static final String CAR_EXT_CONVERSATION = "car_conversation";
    public static final String CAR_EXT_CONVERSATION_MESSAGES = "messages";
    public static final String CAR_EXT_CONVERSATION_MESSAGES_AUTHOR = "author";
    public static final String CAR_EXT_CONVERSATION_MESSAGES_TEXT = "text";
    public static final String CAR_EXT_CONVERSATION_PARTICIPANTS = "participants";
    public static final String COMMA = ",";
    public static final String EMAIL_AT = "@";
    public static final String EMPTY = "";
    public static final String FACEBOOK = "com.facebook.katana";
    public static final String FACEBOOK_MESSENGER = "com.facebook.orca";
    public static final String FUEL_PACKAGE = "com.navdy.fuel";
    public static final boolean GLANCES_APP_GLANCES_ENABLED_DEFAULT = false;
    public static final boolean GLANCES_DRIVING_GLANCES_ENABLED_DEFAULT = true;
    public static final boolean GLANCES_OTHER_APPS_ENABLED_DEFAULT = false;
    public static final String GOOGLE_CALENDAR = "com.google.android.calendar";
    public static final String GOOGLE_HANGOUTS = "com.google.android.talk";
    public static final String GOOGLE_INBOX = "com.google.android.apps.inbox";
    public static final String GOOGLE_MAIL = "com.google.android.gm";
    public static final String GOOGLE_MUSIC = "com.google.android.music";
    public static final String MAIL_TO = "mailto:";
    public static final String MICROSOFT_OUTLOOK = "com.microsoft.office.outlook";
    public static final String MUSIC_PACKAGE = "com.navdy.music";
    public static final String NAPSTER = "com.rhapsody";
    public static final String NEW_LINE = "\n";
    public static final String NOTIFICATION_INBOX_STYLE = "android.app.Notification$InboxStyle";
    public static final String PANDORA = "com.pandora.android";
    public static final String PHONE_PACKAGE = "com.navdy.phone";
    public static final String SAMSUNG_MUSIC = "com.sec.android.app.music";
    public static final String SLACK = "com.Slack";
    public static final String SMS_PACKAGE = "com.navdy.sms";
    public static final String SOUNDCLOUD = "com.soundcloud.android";
    public static final String SPOTIFY = "com.spotify.music";
    public static final String TRAFFIC_PACKAGE = "com.navdy.traffic";
    public static final String TWITTER = "com.twitter.android";
    public static final String WHATS_APP = "com.whatsapp";
    public static final String YOUTUBE_MUSIC = "com.google.android.apps.youtube.music";
    private static final Map<Group, Set<String>> groups = new HashMap<Group, Set<String>>() {
        {
            put(Group.DRIVING_GLANCES, new HashSet());
            put(Group.WHITE_LIST, new HashSet());
            put(Group.EMAIL_GROUP, new HashSet());
            put(Group.MESSAGING_GROUP, new HashSet());
            put(Group.CALENDAR_GROUP, new HashSet());
            put(Group.SOCIAL_GROUP, new HashSet());
            put(Group.MUSIC_GROUP, new HashSet());
            put(Group.IGNORE_GROUP, new HashSet());
        }
    };
    private static final Logger logger = new Logger(GlanceConstants.class);

    public enum Group {
        DRIVING_GLANCES,
        WHITE_LIST,
        EMAIL_GROUP,
        MESSAGING_GROUP,
        CALENDAR_GROUP,
        SOCIAL_GROUP,
        MUSIC_GROUP,
        IGNORE_GROUP
    }

    static {
        Set<String> drivingGlances = (Set) groups.get(Group.DRIVING_GLANCES);
        Set<String> whiteList = (Set) groups.get(Group.WHITE_LIST);
        Set<String> emailGroup = (Set) groups.get(Group.EMAIL_GROUP);
        Set<String> messagingGroup = (Set) groups.get(Group.MESSAGING_GROUP);
        Set<String> calendarGroup = (Set) groups.get(Group.CALENDAR_GROUP);
        Set<String> socialGroup = (Set) groups.get(Group.SOCIAL_GROUP);
        Set<String> musicGroup = (Set) groups.get(Group.MUSIC_GROUP);
        Set<String> ignoreGroup = (Set) groups.get(Group.IGNORE_GROUP);
        emailGroup.add(GOOGLE_MAIL);
        emailGroup.add(GOOGLE_INBOX);
        emailGroup.add(MICROSOFT_OUTLOOK);
        messagingGroup.add(SLACK);
        messagingGroup.add(GOOGLE_HANGOUTS);
        messagingGroup.add(WHATS_APP);
        messagingGroup.add(FACEBOOK_MESSENGER);
        calendarGroup.add(GOOGLE_CALENDAR);
        socialGroup.add(FACEBOOK);
        socialGroup.add(TWITTER);
        musicGroup.add(GOOGLE_MUSIC);
        musicGroup.add(PANDORA);
        musicGroup.add(SPOTIFY);
        musicGroup.add(APPLE_MUSIC);
        musicGroup.add(YOUTUBE_MUSIC);
        musicGroup.add(SAMSUNG_MUSIC);
        musicGroup.add(NAPSTER);
        musicGroup.add(SOUNDCLOUD);
        drivingGlances.add(PHONE_PACKAGE);
        drivingGlances.add(SMS_PACKAGE);
        drivingGlances.add(FUEL_PACKAGE);
        drivingGlances.add(MUSIC_PACKAGE);
        drivingGlances.add(TRAFFIC_PACKAGE);
        whiteList.add(GOOGLE_MAIL);
        whiteList.addAll(messagingGroup);
        whiteList.addAll(calendarGroup);
        whiteList.addAll(socialGroup);
        ignoreGroup.add("com.google.android.gms");
        ignoreGroup.add("com.android.mms");
        ignoreGroup.add("com.android.systemui");
    }

    public static boolean isPackageInGroup(String packageName, Group group) {
        Set<String> packages = (Set) groups.get(group);
        if (packages != null) {
            return packages.contains(packageName);
        }
        logger.e("Cannot get package for group " + String.valueOf(group));
        return false;
    }

    public static void addPackageToGroup(String packageName, Group group) {
        Set<String> packages = (Set) groups.get(group);
        if (packages != null) {
            packages.add(packageName);
        } else {
            logger.e("Cannot get package for group " + String.valueOf(group));
        }
    }
}
