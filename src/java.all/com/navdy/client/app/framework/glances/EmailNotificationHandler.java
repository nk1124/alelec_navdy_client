package com.navdy.client.app.framework.glances;

import android.app.Notification;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import com.navdy.client.app.framework.glances.GlanceConstants.Group;
import com.navdy.client.app.framework.service.NavdyCustomNotificationListenerService;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.service.library.events.glances.EmailConstants;
import com.navdy.service.library.events.glances.GlanceEvent.Builder;
import com.navdy.service.library.events.glances.GlanceEvent.GlanceType;
import com.navdy.service.library.events.glances.KeyValue;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.List;

public class EmailNotificationHandler {
    private static Logger sLogger = NavdyCustomNotificationListenerService.sLogger;

    public static void handleEmailNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        if (GlanceUtils.isThisGlanceEnabledAsWellAsGlobal(packageName) && GlanceConstants.isPackageInGroup(packageName, Group.EMAIL_GROUP)) {
            int i;
            int index;
            Notification notification = sbn.getNotification();
            Bundle extras = NotificationCompat.getExtras(notification);
            String senderOrTitle = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.title");
            String toEmail = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.subText");
            String subject = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.text");
            String body = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.bigText");
            Object textLines = extras.get("android.textLines");
            if (!packageName.equals(GlanceConstants.GOOGLE_MAIL) && textLines != null && (textLines instanceof CharSequence[])) {
                CharSequence[] lines = (CharSequence[]) textLines;
                StringBuilder newBody = new StringBuilder();
                for (i = 0; i < lines.length; i++) {
                    newBody.append(lines[i]);
                    if (i + 1 < lines.length) {
                        newBody.append("\n");
                    }
                }
                if (newBody.length() > 0) {
                    subject = NavdyCustomNotificationListenerService.getStringSafely(extras, "android.summaryText");
                    body = newBody.toString();
                }
            } else if (!(body == null || subject == null)) {
                if (packageName.equals(GlanceConstants.MICROSOFT_OUTLOOK)) {
                    body = body.substring(body.indexOf("\n") + 1);
                    if (subject.contains(body)) {
                        subject = subject.substring(0, subject.indexOf(body));
                    }
                } else if (body.startsWith(subject)) {
                    index = body.indexOf(subject + "\n");
                    if (index != -1) {
                        body = body.substring((subject.length() + index) + 1);
                    }
                }
            }
            if (body == null && subject != null) {
                index = subject.indexOf("\n");
                if (index != -1) {
                    body = subject.substring(index + 1).trim();
                    subject = subject.substring(0, index).trim();
                }
            }
            StringBuilder builder = new StringBuilder();
            String peopleStr = null;
            String[] people = (String[]) extras.get("android.people");
            if (people != null) {
                for (i = 0; i < people.length; i++) {
                    String p = people[i];
                    if (p != null && p.startsWith(GlanceConstants.MAIL_TO)) {
                        p = p.substring(GlanceConstants.MAIL_TO.length());
                    }
                    if (i != 0) {
                        builder.append(",");
                    }
                    builder.append(p);
                }
                peopleStr = builder.toString();
            }
            if (peopleStr == null && senderOrTitle != null) {
                peopleStr = senderOrTitle;
            }
            sLogger.v("[navdyinfo-gmail] senderOrTitle[" + senderOrTitle + "] from[" + peopleStr + "] to[" + toEmail + "] subject[" + subject + "] body[" + body + "]");
            if ((subject == null && body == null) || peopleStr == null) {
                sLogger.v("[navdyinfo-gmail] invalid data");
                return;
            }
            String id = GlancesHelper.getId();
            List<KeyValue> data = new ArrayList();
            if (!StringUtils.isEmptyAfterTrim(peopleStr)) {
                if (peopleStr.contains(GlanceConstants.EMAIL_AT)) {
                    data.add(new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), senderOrTitle));
                    data.add(new KeyValue(EmailConstants.EMAIL_FROM_EMAIL.name(), peopleStr));
                } else {
                    data.add(new KeyValue(EmailConstants.EMAIL_FROM_NAME.name(), peopleStr));
                }
            }
            if (toEmail != null) {
                if (toEmail.contains(GlanceConstants.EMAIL_AT)) {
                    data.add(new KeyValue(EmailConstants.EMAIL_TO_EMAIL.name(), toEmail));
                } else {
                    data.add(new KeyValue(EmailConstants.EMAIL_TO_NAME.name(), toEmail));
                }
            }
            if (!StringUtils.isEmptyAfterTrim(subject)) {
                data.add(new KeyValue(EmailConstants.EMAIL_SUBJECT.name(), subject));
            }
            if (!StringUtils.isEmptyAfterTrim(body)) {
                data.add(new KeyValue(EmailConstants.EMAIL_BODY.name(), body));
            }
            GlancesHelper.sendEvent(new Builder().glanceType(GlanceType.GLANCE_TYPE_EMAIL).provider(packageName).id(id).postTime(Long.valueOf(notification.when)).glanceData(data).build());
            return;
        }
        sLogger.w("email notification not handled [" + packageName + "]");
    }
}
