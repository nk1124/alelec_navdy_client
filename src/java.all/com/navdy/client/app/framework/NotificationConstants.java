package com.navdy.client.app.framework;

public class NotificationConstants {
    public static final int NOTIFICATION_OTA_UPDATE = 1;
    public static final int RETRY_NOTIFICATION_ID = 2;
    public static final int RETRY_UPLOADING_NOTIFICATION_ID = 3;
    public static final int TICKET_SUBMISSIONS_SUCCEEDED_NOTIFICATION_ID = 4;
}
