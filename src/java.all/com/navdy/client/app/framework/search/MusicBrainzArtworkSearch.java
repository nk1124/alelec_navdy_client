package com.navdy.client.app.framework.search;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.util.LruCache;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.droidparts.contract.SQL;
import org.json.JSONException;
import org.json.JSONObject;

public class MusicBrainzArtworkSearch {
    private static final String API_URL_RELEASE_PREFIX = "http://musicbrains.org/ws/2/release/?fmt=json&limit=1&query=";
    private static final String ARTIST_ALBUM_SEPARATOR = " // ";
    private static final int BITMAP_CACHE_SIZE = 10;
    private static final String COVER_ART_URL_POSTFIX = "/front-250";
    private static final String COVER_ART_URL_PREFIX = "http://coverartarchive.org/release/";
    private static final LruCache<String, Bitmap> artworkCache = new LruCache(10);
    private static final Logger sLogger = new Logger(MusicBrainzArtworkSearch.class);

    public static Bitmap getArtwork(String artist, String album) {
        Exception e;
        sLogger.v("Sending artwork query for " + album + " from " + artist);
        boolean isArtistSet = !StringUtils.isEmptyAfterTrim(artist);
        boolean isAlbumSet = !StringUtils.isEmptyAfterTrim(album);
        if (!isArtistSet && !isAlbumSet) {
            return null;
        }
        String cacheKey = buildCacheKey(artist, album);
        Bitmap result = (Bitmap) artworkCache.get(cacheKey);
        if (result != null) {
            sLogger.v("Found artwork in MusicBrainz client class cache");
            return result;
        }
        StringBuilder queryBuilder = new StringBuilder();
        if (isArtistSet) {
            queryBuilder.append("artist:\"").append(artist).append("\"");
        }
        if (isAlbumSet) {
            if (isArtistSet) {
                queryBuilder.append(SQL.AND);
            }
            queryBuilder.append("release:\"").append(album).append("\"");
        }
        if (queryBuilder.length() == 0) {
            sLogger.w("No artist nor album name is given");
            return null;
        }
        try {
            JSONObject albumData = IOUtils.getJSONFromURL(API_URL_RELEASE_PREFIX + URLEncoder.encode(queryBuilder.toString(), "UTF-8"));
            if (albumData == null) {
                sLogger.e("Album not found on MusicBrainz");
                return null;
            }
            try {
                byte[] img = IOUtils.downloadImage(COVER_ART_URL_PREFIX + ((JSONObject) albumData.getJSONArray("releases").get(0)).getString("id") + COVER_ART_URL_POSTFIX);
                result = BitmapFactory.decodeByteArray(img, 0, img.length);
                sLogger.v("Got album artwork from MusicBrainz with size " + img.length);
                artworkCache.put(cacheKey, result);
                return result;
            } catch (JSONException e2) {
                e = e2;
                sLogger.e("Cannot read album's ID from MusicBrainz data", e);
                return null;
            } catch (NullPointerException e3) {
                e = e3;
                sLogger.e("Cannot read album's ID from MusicBrainz data", e);
                return null;
            } catch (IndexOutOfBoundsException e4) {
                e = e4;
                sLogger.e("Cannot read album's ID from MusicBrainz data", e);
                return null;
            }
        } catch (UnsupportedEncodingException e5) {
            sLogger.e("Cannot build query for album ID", e5);
            return null;
        }
    }

    private static String buildCacheKey(@Nullable String artist, @Nullable String album) {
        return ARTIST_ALBUM_SEPARATOR + album;
    }
}
