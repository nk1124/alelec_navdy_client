package com.navdy.client.app.framework;

import android.os.Environment;
import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PathManager {
    private static final String DISKCACHE_DIR = "diskcache-v4";
    private static final String HERE_MAPS_CONFIG_BASE_PATH = ".here-maps";
    private static final Pattern HERE_MAPS_CONFIG_DIRS_PATTERN = Pattern.compile(HERE_MAPS_CONFIG_DIRS_REGEX);
    private static final String HERE_MAPS_CONFIG_DIRS_REGEX = "[0-9]{10}";
    private static final String LOGS_FOLDER = "logs";
    private static final String OTA_INCREMENTAL_UPDATE_FILE_FORMAT = "navdy_ota_%d_to_%d.zip";
    private static final String OTA_UPDATE_FILE_FORMAT = "navdy_ota_%d.zip";
    private static final String OTA_UPDATE_FILE_NAME = "hudupdate.zip";
    private static final String OTA_UPDATE_FOLDER = "update";
    private static final String S3_PROGRESS_FILE_EXTENSION = ".s3";
    private static final String S3_PROGRESS_FILE_PREFIX = "navdy_ota_progress_";
    public static final String TICKETS_FOLDER_NAME = "navdy/support_tickets/";
    private static final String TIMESTAMP_MWCONFIG_LATEST = "1482069896";
    private static final Logger logger = new Logger(PathManager.class);
    private static final PathManager sSingleton = new PathManager();
    private File hereMapsConfigDirs;
    private String mLogsFolder;
    private String mOtaUpdatePath;

    public static PathManager getInstance() {
        return sSingleton;
    }

    private PathManager() {
        String internalAppPath = NavdyApplication.getAppContext().getFilesDir().getAbsolutePath();
        this.mOtaUpdatePath = internalAppPath + File.separator + OTA_UPDATE_FOLDER;
        this.mLogsFolder = internalAppPath + File.separator + LOGS_FOLDER;
        IOUtils.createDirectory(this.mOtaUpdatePath);
        IOUtils.createDirectory(this.mLogsFolder);
        this.hereMapsConfigDirs = new File(internalAppPath + File.separator + HERE_MAPS_CONFIG_BASE_PATH);
    }

    public String getOtaUpdateFolderPath() {
        return this.mOtaUpdatePath;
    }

    public String getOTAUpdateFilePath() {
        return this.mOtaUpdatePath + File.separator + OTA_UPDATE_FILE_NAME;
    }

    public String getTicketFolderPath() {
        return Environment.getExternalStorageDirectory() + File.separator + TICKETS_FOLDER_NAME;
    }

    public String getOtaUpdateFileNameOnHUD(int version) {
        return String.format(OTA_UPDATE_FILE_FORMAT, new Object[]{Integer.valueOf(version)});
    }

    public String getOtaUpdateFileNameOnHUD(int fromVersion, int targetVersion) {
        return String.format(OTA_INCREMENTAL_UPDATE_FILE_FORMAT, new Object[]{Integer.valueOf(fromVersion), Integer.valueOf(targetVersion)});
    }

    public String getS3DownloadProgressFile(int incrementalVersion) {
        return this.mOtaUpdatePath + File.separator + S3_PROGRESS_FILE_PREFIX + incrementalVersion + S3_PROGRESS_FILE_EXTENSION;
    }

    public String getLogsFolder() {
        return this.mLogsFolder;
    }

    public List<String> getHereMapsConfigDirs() {
        List<String> pathList = new ArrayList();
        File[] hereDirs = this.hereMapsConfigDirs.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isDirectory() && PathManager.HERE_MAPS_CONFIG_DIRS_PATTERN.matcher(file.getName()).matches();
            }
        });
        if (hereDirs != null) {
            for (File f : hereDirs) {
                pathList.add(f.getAbsolutePath());
            }
        }
        return pathList;
    }

    public boolean hasDiskCacheDir() {
        return new File(this.hereMapsConfigDirs.getAbsolutePath() + File.separator + DISKCACHE_DIR).exists();
    }

    public String getLatestHereMapsConfigPath() {
        return this.hereMapsConfigDirs.getAbsolutePath() + File.separator + TIMESTAMP_MWCONFIG_LATEST;
    }
}
