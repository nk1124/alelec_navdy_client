package com.navdy.client.app.framework.models;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.providers.NavdyContentProviderConstants;
import com.navdy.service.library.events.TripUpdate;
import com.navdy.service.library.log.Logger;

public class Trip {
    private static Logger logger = new Logger(Trip.class);
    public long arrivedAtDestination;
    public int destinationId;
    public double endLat;
    public double endLong;
    public int endOdometer;
    public long endTime;
    public int id;
    public int offset;
    public double startLat;
    public double startLong;
    public int startOdometer;
    public long startTime;
    public long tripNumber;

    public Trip(int id, long tripNumber, long startTime, int offset, int startOdometer, double startLat, double startLong, long endTime, int endOdometer, double endLat, double endLong, long arrivedAtDestination, int destinationId) {
        this.id = id;
        this.tripNumber = tripNumber;
        this.startTime = startTime;
        this.offset = offset;
        this.startOdometer = startOdometer;
        this.startLat = startLat;
        this.startLong = startLong;
        this.endTime = endTime;
        this.endOdometer = endOdometer;
        this.endLat = endLat;
        this.endLong = endLong;
        this.arrivedAtDestination = arrivedAtDestination;
        this.destinationId = destinationId;
    }

    public Uri saveToDb(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver != null) {
            ContentValues tripValues = new ContentValues();
            tripValues.put(NavdyContentProviderConstants.TRIPS_TRIP_NUMBER, Long.valueOf(this.tripNumber));
            tripValues.put(NavdyContentProviderConstants.TRIPS_START_TIME, Long.valueOf(this.startTime));
            tripValues.put(NavdyContentProviderConstants.TRIPS_START_TIME_ZONE_N_DST, Integer.valueOf(this.offset));
            tripValues.put(NavdyContentProviderConstants.TRIPS_START_ODOMETER, Integer.valueOf(this.startOdometer));
            tripValues.put(NavdyContentProviderConstants.TRIPS_START_LAT, Double.valueOf(this.startLat));
            tripValues.put(NavdyContentProviderConstants.TRIPS_START_LONG, Double.valueOf(this.startLong));
            tripValues.put(NavdyContentProviderConstants.TRIPS_END_TIME, Long.valueOf(this.endTime));
            tripValues.put(NavdyContentProviderConstants.TRIPS_END_ODOMETER, Integer.valueOf(this.endOdometer));
            tripValues.put(NavdyContentProviderConstants.TRIPS_END_LAT, Double.valueOf(this.endLat));
            tripValues.put(NavdyContentProviderConstants.TRIPS_END_LONG, Double.valueOf(this.endLong));
            tripValues.put(NavdyContentProviderConstants.TRIPS_ARRIVED_AT_DESTINATION, Long.valueOf(this.arrivedAtDestination));
            tripValues.put(NavdyContentProviderConstants.TRIPS_DESTINATION_ID, Integer.valueOf(this.destinationId));
            return contentResolver.insert(NavdyContentProviderConstants.TRIPS_CONTENT_URI, tripValues);
        }
        logger.e("Unable to get contentResolver !");
        return null;
    }

    public static Uri saveStartingTripUpdate(Context context, TripUpdate tripUpdate) {
        logger.v("inserting trip_number: " + tripUpdate.trip_number);
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null) {
            return null;
        }
        ContentValues contentValues = new ContentValues();
        int destinationId = 0;
        if (!StringUtils.isEmptyAfterTrim(tripUpdate.chosen_destination_id)) {
            destinationId = Integer.parseInt(tripUpdate.chosen_destination_id);
        }
        int offset = SystemUtils.getTimeZoneAndDaylightSavingOffset(tripUpdate.timestamp);
        contentValues.put(NavdyContentProviderConstants.TRIPS_TRIP_NUMBER, tripUpdate.trip_number);
        contentValues.put(NavdyContentProviderConstants.TRIPS_START_TIME, tripUpdate.timestamp);
        contentValues.put(NavdyContentProviderConstants.TRIPS_START_TIME_ZONE_N_DST, Integer.valueOf(offset));
        contentValues.put(NavdyContentProviderConstants.TRIPS_START_ODOMETER, tripUpdate.distance_traveled);
        contentValues.put(NavdyContentProviderConstants.TRIPS_START_LAT, tripUpdate.current_position.latitude);
        contentValues.put(NavdyContentProviderConstants.TRIPS_START_LONG, tripUpdate.current_position.longitude);
        contentValues.put(NavdyContentProviderConstants.TRIPS_DESTINATION_ID, Integer.valueOf(destinationId));
        return contentResolver.insert(NavdyContentProviderConstants.TRIPS_CONTENT_URI, contentValues);
    }

    public static int saveLastTripUpdate(Context context, TripUpdate tripUpdate) {
        int arrivedAtDestination = 0;
        if (!StringUtils.isEmptyAfterTrim(tripUpdate.arrived_at_destination_id)) {
            arrivedAtDestination = 1;
        }
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null) {
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(NavdyContentProviderConstants.TRIPS_END_TIME, tripUpdate.timestamp);
        contentValues.put(NavdyContentProviderConstants.TRIPS_END_ODOMETER, tripUpdate.distance_traveled);
        contentValues.put(NavdyContentProviderConstants.TRIPS_END_LAT, tripUpdate.current_position.latitude);
        contentValues.put(NavdyContentProviderConstants.TRIPS_END_LONG, tripUpdate.current_position.longitude);
        contentValues.put(NavdyContentProviderConstants.TRIPS_ARRIVED_AT_DESTINATION, Integer.valueOf(arrivedAtDestination));
        return contentResolver.update(NavdyContentProviderConstants.TRIPS_CONTENT_URI, contentValues, String.format("%s=?", new Object[]{NavdyContentProviderConstants.TRIPS_TRIP_NUMBER}), new String[]{String.valueOf(tripUpdate.trip_number)});
    }

    public int setDestinationIdAndSaveToDb(int destId) {
        logger.v("updating trip with destination id: " + this.destinationId);
        Context context = NavdyApplication.getAppContext();
        this.destinationId = destId;
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null) {
            return -1;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(NavdyContentProviderConstants.TRIPS_DESTINATION_ID, Integer.valueOf(this.destinationId));
        return contentResolver.update(NavdyContentProviderConstants.TRIPS_CONTENT_URI, contentValues, String.format("%s=?", new Object[]{NavdyContentProviderConstants.TRIPS_TRIP_NUMBER}), new String[]{String.valueOf(this.tripNumber)});
    }

    public String toString() {
        return "Trip{id=" + this.id + ",\ttripNumber=" + this.tripNumber + ",\tstartTime=" + this.startTime + ",\toffset=" + this.offset + ",\tstartOdometer=" + this.startOdometer + ",\tstartLat=" + this.startLat + ",\tstartLong=" + this.startLong + ",\tendTime=" + this.endTime + ",\tendOdometer=" + this.endOdometer + ",\tendLat=" + this.endLat + ",\tendLong=" + this.endLong + ",\tarrivedAtDestination=" + this.arrivedAtDestination + ",\tdestinationId=" + this.destinationId + '}';
    }
}
