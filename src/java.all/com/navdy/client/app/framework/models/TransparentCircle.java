package com.navdy.client.app.framework.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.View;
import com.navdy.service.library.log.Logger;

public class TransparentCircle extends View {
    private static final boolean VERBOSE = false;
    private Bitmap bitmap;
    private Canvas canvas;
    private Paint eraser;
    private Logger logger = new Logger(TransparentCircle.class);

    public TransparentCircle(Context context) {
        super(context);
        initPaintObjectAsEraser();
    }

    public TransparentCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaintObjectAsEraser();
    }

    public TransparentCircle(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initPaintObjectAsEraser();
    }

    private void initPaintObjectAsEraser() {
        this.eraser = new Paint();
        this.eraser.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        this.eraser.setAntiAlias(true);
    }

    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        if (!(width == oldWidth && height == oldHeight)) {
            this.bitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
            this.canvas = new Canvas(this.bitmap);
        }
        super.onSizeChanged(width, height, oldWidth, oldHeight);
    }

    protected void onDraw(Canvas canvas) {
        int radius;
        int width = getWidth();
        int height = getHeight();
        if (width > height) {
            radius = height / 2;
        } else {
            radius = width / 2;
        }
        this.bitmap.eraseColor(0);
        this.canvas.drawColor(-16777216);
        this.canvas.drawCircle((float) (width / 2), (float) (height / 2), (float) radius, this.eraser);
        canvas.drawBitmap(this.bitmap, 0.0f, 0.0f, null);
        super.onDraw(canvas);
    }
}
