package com.navdy.client.app.framework.models;

import com.google.android.gms.maps.model.LatLng;
import com.navdy.client.app.ui.homescreen.CalendarUtils;
import com.navdy.service.library.events.destination.Destination;

public class CalendarEvent {
    public boolean allDay = false;
    public long calendarId = 0;
    public Destination destination;
    public long displayColor = 0;
    public String displayName = "";
    public long endTimestamp = 0;
    public LatLng latLng;
    public String location = "";
    public long startTimestamp = 0;

    public CalendarEvent(String displayName, long calendarId, long startTimestamp, long endTimestamp, String location, long displayColor, boolean allDay) {
        this.displayName = displayName;
        this.calendarId = calendarId;
        this.startTimestamp = startTimestamp;
        this.location = location;
        this.endTimestamp = endTimestamp;
        this.displayColor = displayColor;
        this.allDay = allDay;
    }

    public String toString() {
        return String.format("display_name: %s, \tcalendarId: %s\tlocation: %s\tstart_time: %s\tend_time: %s\tdisplay_color: %s\tall_day: %s", new Object[]{this.displayName, Long.valueOf(this.calendarId), this.location, Long.valueOf(this.startTimestamp), Long.valueOf(this.endTimestamp), Long.valueOf(this.displayColor), Boolean.valueOf(this.allDay)});
    }

    public com.navdy.service.library.events.calendars.CalendarEvent toProtobufObject() {
        return new com.navdy.service.library.events.calendars.CalendarEvent(this.displayName, Long.valueOf(this.startTimestamp), Long.valueOf(this.endTimestamp), Boolean.valueOf(this.allDay), Integer.valueOf((int) this.displayColor), this.location, String.valueOf(this.calendarId), this.destination);
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public String getDateAndTime() {
        return CalendarUtils.getDateAndTime(this.startTimestamp);
    }

    public String getStartTime() {
        return CalendarUtils.getTime(this.startTimestamp);
    }

    public String getEndTime() {
        return CalendarUtils.getTime(this.endTimestamp);
    }
}
