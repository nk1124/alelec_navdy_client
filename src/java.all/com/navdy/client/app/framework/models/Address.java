package com.navdy.client.app.framework.models;

import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import org.droidparts.contract.SQL.DDL;

public class Address {
    private static final boolean VERBOSE = false;
    public static final Logger logger = new Logger(Address.class);
    public String city;
    public String country;
    public String label;
    public String name;
    public String postBox;
    public String postCode;
    public String state;
    public String street;
    public int type;

    public Address(String pobox, String street, String city, String state, String postcode, String country, int type, String name, String label) {
        this.postBox = pobox;
        this.street = street;
        this.city = city;
        this.state = state;
        this.postCode = postcode;
        this.country = country;
        this.name = name;
        this.type = type;
        this.label = label;
    }

    public String toString() {
        return "Address{type=" + this.type + ", postBox='" + this.postBox + '\'' + ", street='" + this.street + '\'' + ", city='" + this.city + '\'' + ", state='" + this.state + '\'' + ", postCode='" + this.postCode + '\'' + ", country='" + this.country + '\'' + ", name='" + this.name + '\'' + ", label='" + this.label + '\'' + '}';
    }

    public String getFullAddress() {
        if (StringUtils.isEmptyAfterTrim(this.city)) {
            return this.street;
        }
        ArrayList<String> fullAddress = new ArrayList();
        if (!StringUtils.isEmptyAfterTrim(this.street)) {
            fullAddress.add(this.street);
        }
        if (!StringUtils.isEmptyAfterTrim(this.city)) {
            fullAddress.add(this.city);
        }
        if (!(StringUtils.isEmptyAfterTrim(this.state) && StringUtils.isEmptyAfterTrim(this.postCode))) {
            String stateAndZipCode = "";
            if (!StringUtils.isEmptyAfterTrim(this.state)) {
                stateAndZipCode = this.state;
            }
            if (!StringUtils.isEmptyAfterTrim(this.state) && !StringUtils.isEmptyAfterTrim(this.postCode)) {
                stateAndZipCode = stateAndZipCode + " " + this.postCode;
            } else if (!StringUtils.isEmptyAfterTrim(this.postCode)) {
                stateAndZipCode = this.postCode;
            }
            fullAddress.add(stateAndZipCode);
        }
        if (!StringUtils.isEmptyAfterTrim(this.country)) {
            fullAddress.add(this.country);
        }
        return StringUtils.join(fullAddress, DDL.SEPARATOR);
    }

    public boolean equals(Object obj) {
        return (obj instanceof Address) && this.type == ((Address) obj).type && StringUtils.equalsOrBothEmptyAfterTrim(this.postBox, ((Address) obj).postBox) && StringUtils.equalsOrBothEmptyAfterTrim(this.street, ((Address) obj).street) && StringUtils.equalsOrBothEmptyAfterTrim(this.city, ((Address) obj).city) && StringUtils.equalsOrBothEmptyAfterTrim(this.state, ((Address) obj).state) && StringUtils.equalsOrBothEmptyAfterTrim(this.postCode, ((Address) obj).postCode) && StringUtils.equalsOrBothEmptyAfterTrim(this.country, ((Address) obj).country);
    }

    public int hashCode() {
        int hashCode;
        int i = 0;
        int hashCode2 = ((this.type * 31) + (this.postBox != null ? this.postBox.hashCode() : 0)) * 31;
        if (this.street != null) {
            hashCode = this.street.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 31;
        if (this.city != null) {
            hashCode = this.city.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 31;
        if (this.state != null) {
            hashCode = this.state.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 31;
        if (this.postCode != null) {
            hashCode = this.postCode.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 31;
        if (this.country != null) {
            i = this.country.hashCode();
        }
        return hashCode + i;
    }
}
