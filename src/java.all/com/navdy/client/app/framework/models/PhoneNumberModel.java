package com.navdy.client.app.framework.models;

import com.navdy.service.library.events.contacts.PhoneNumberType;

public class PhoneNumberModel {
    public String customType;
    public boolean isPrimary;
    public String number;
    public int type;

    public PhoneNumberModel(String number, int type, String customType, boolean isPrimary) {
        this.number = number;
        this.type = type;
        this.customType = customType;
        this.isPrimary = isPrimary;
    }

    boolean isFax() {
        return this.type == 5 || this.type == 4 || this.type == 13;
    }

    PhoneNumberType getPhoneNumberProtobufType() {
        switch (this.type) {
            case 1:
                return PhoneNumberType.PHONE_NUMBER_HOME;
            case 2:
                return PhoneNumberType.PHONE_NUMBER_MOBILE;
            case 3:
                return PhoneNumberType.PHONE_NUMBER_WORK;
            default:
                return PhoneNumberType.PHONE_NUMBER_OTHER;
        }
    }

    public String toString() {
        return "PhoneNumberModel{number='" + this.number + '\'' + ", type=" + this.type + ", customType='" + this.customType + '\'' + ", isPrimary=" + this.isPrimary + '}';
    }
}
