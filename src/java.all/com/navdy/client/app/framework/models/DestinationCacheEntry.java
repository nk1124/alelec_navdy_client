package com.navdy.client.app.framework.models;

public class DestinationCacheEntry {
    public Destination destination;
    public int destinationId;
    public int id = -1;
    public long lastResponseDate;
    public String locationString;

    public DestinationCacheEntry(int id, long lastResponseDate, String locationString, int destinationId) {
        this.id = id;
        this.lastResponseDate = lastResponseDate;
        this.locationString = locationString;
        this.destinationId = destinationId;
    }

    public String toString() {
        return "DestinationCacheEntry{id=" + this.id + ", lastResponseDate=" + this.lastResponseDate + ", locationString='" + this.locationString + '\'' + ", destinationId=" + this.destinationId + ", destination=" + this.destination + '}';
    }
}
