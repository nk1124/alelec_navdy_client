package com.navdy.client.app.framework.models;

public class Calendar {
    public String accountName = "";
    public String accountType = "";
    public int calendarColor = 0;
    public String displayName = "";
    public long id = 0;
    public String ownerAccount = "";
    public CalendarListItemType type;
    public boolean visible = true;

    public enum CalendarListItemType {
        GLOBAL_SWITCH(0),
        TITLE(1),
        CALENDAR(2);
        
        private int value;

        private CalendarListItemType(int value) {
            this.value = 0;
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static CalendarListItemType fromValue(int value) {
            return values()[value];
        }
    }

    public Calendar(CalendarListItemType type) {
        this.type = type;
    }

    public Calendar(CalendarListItemType type, String displayName) {
        this.type = type;
        this.displayName = displayName;
    }

    public Calendar(CalendarListItemType type, long id, String displayName, int calendarColor, String accountName, String accountType, String ownerAccount, boolean visible) {
        this.type = type;
        this.id = id;
        this.displayName = displayName;
        this.calendarColor = calendarColor;
        this.accountName = accountName;
        this.accountType = accountType;
        this.ownerAccount = ownerAccount;
        this.visible = visible;
    }

    public String toString() {
        return "Calendar{type=" + this.type + ", id=" + this.id + ", displayName='" + this.displayName + '\'' + ", calendarColor=" + this.calendarColor + ", calendarName='" + this.accountName + '\'' + ", accountType='" + this.accountType + '\'' + ", ownerAccount='" + this.ownerAccount + '\'' + ", visible='" + this.visible + '\'' + '}';
    }
}
