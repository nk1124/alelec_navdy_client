package com.navdy.client.app.framework;

import com.localytics.android.Localytics;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class LocalyticsManager {
    private static volatile LocalyticsManager instance = null;
    private AtomicBoolean localyticsIsInitialized = new AtomicBoolean(false);
    private ArrayList<Runnable> localyticsPendingTasks = new ArrayList();
    private Logger logger = new Logger(LocalyticsManager.class);

    private LocalyticsManager() {
    }

    public static LocalyticsManager getInstance() {
        if (instance == null) {
            instance = new LocalyticsManager();
        }
        return instance;
    }

    public synchronized void initLocalytics() {
        this.logger.v("initializing Localytics");
        Localytics.integrate(NavdyApplication.getAppContext());
        Localytics.registerPush(NavdyApplication.getAppContext().getString(R.string.google_cloud_sender_id));
        this.localyticsIsInitialized.set(true);
        if (this.localyticsPendingTasks.size() > 0) {
            this.logger.v("Localytics is initialized. Now running " + this.localyticsPendingTasks.size() + " pending tasks.");
        } else {
            this.logger.v("Localytics is initialized. There are no pending tasks.");
        }
        Iterator it = this.localyticsPendingTasks.iterator();
        while (it.hasNext()) {
            ((Runnable) it.next()).run();
        }
    }

    private synchronized void addToLocalyticsPendingTasks(Runnable r) {
        this.localyticsPendingTasks.add(r);
        this.logger.v("Added 1 new task to the list of tasks waiting for Localytics initialization. The queue now has " + this.localyticsPendingTasks.size() + " pending tasks.");
    }

    public static void runWhenReady(Runnable r) {
        LocalyticsManager lm = getInstance();
        if (lm.localyticsIsInitialized.get()) {
            r.run();
        } else {
            lm.addToLocalyticsPendingTasks(r);
        }
    }

    public static void tagScreen(final String screen) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.tagScreen(screen);
            }
        });
    }

    public static void tagEvent(final String eventName) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.tagEvent(eventName);
            }
        });
    }

    public static void tagEvent(final String eventName, final Map<String, String> attributes) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.tagEvent(eventName, attributes);
            }
        });
    }

    public static void tagEvent(final String eventName, final Map<String, String> attributes, final long customerValueIncrease) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.tagEvent(eventName, attributes, customerValueIncrease);
            }
        });
    }

    public static void setCustomerEmail(final String email) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.setCustomerEmail(email);
            }
        });
    }

    public static void setCustomerFullName(final String fullName) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.setCustomerFullName(fullName);
            }
        });
    }

    public static void setProfileAttribute(final String key, final String value) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.setProfileAttribute(key, value);
            }
        });
    }

    public static void setProfileAttribute(final String key, final long value) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.setProfileAttribute(key, value);
            }
        });
    }

    public static void setProfileAttribute(final String key, final Date value) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.setProfileAttribute(key, value);
            }
        });
    }

    public static void setProfileAttribute(final String key, final String[] value) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.setProfileAttribute(key, value);
            }
        });
    }

    public static void setProfileAttribute(final String key, final long[] value) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.setProfileAttribute(key, value);
            }
        });
    }

    public static void setProfileAttribute(final String key, final Date[] value) {
        runWhenReady(new Runnable() {
            public void run() {
                Localytics.setProfileAttribute(key, value);
            }
        });
    }
}
