package com.navdy.client.app.framework.suggestion;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Pair;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.Route.TrafficPenaltyMode;
import com.alelec.navdyclient.BuildConfig;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Suggestion;
import com.navdy.client.app.framework.models.Suggestion.SuggestionType;
import com.navdy.client.app.framework.models.Trip;
import com.navdy.client.app.framework.navigation.HereRouteManager;
import com.navdy.client.app.framework.navigation.HereRouteManager.Error;
import com.navdy.client.app.framework.navigation.HereRouteManager.Listener;
import com.navdy.client.app.framework.navigation.HereRouteManager.RouteHandle;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.State;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SuggestionManager;
import com.navdy.client.app.framework.util.SuggestionManager.SuggestionBuildListener;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.client.ota.impl.OTAUpdateManagerImpl;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.places.SuggestedDestination;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.otto.ThreadEnforcer;
import com.squareup.wire.Message;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.droidparts.contract.SQL.DDL;

public class DestinationSuggestionService extends IntentService {
    private static final String BUCKET_NAME = "navdy-trip-data";
    public static final String COMMAND_AUTO_UPLOAD_TRIP_DATA = "COMMAND_AUTO_UPLOAD";
    public static final String COMMAND_POPULATE = "COMMAND_POPULATE";
    public static final String COMMAND_RESET = "COMMAND_RESET";
    public static final String COMMAND_SUGGEST = "COMMAND_SUGGEST";
    public static final String COMMAND_UPLOAD_TRIP_DATA = "COMMAND_UPLOAD";
    private static final String EXTRA_LOCAL = "EXTRA_LOCAL";
    private static final long MINIMUM_INTERVAL_BETWEEN_UPLOADS = TimeUnit.DAYS.toMillis(1);
    private static Destination NO_SUGGESTION = null;
    public static final String PLACE_SUGGESTION_LIBRARY_NAME = "placesuggestion";
    private static final String PREFERENCE_LAST_UPLOAD_ID = "PREF_LAST_UPLOAD_ID";
    private static final String PREFERENCE_LAST_UPLOAD_TIME = "PREF_LAST_UPLOAD_TIME";
    private static final String SERVICE_NAME = "SERVICE_NAME";
    public static final int SUGGESTED_DESTINATION_TRESHOLD = 200;
    public static final int TRIP_TRESHOLD_FOR_UPLOAD = 10;
    public static final int TRIP_UPLOAD_SERVICE_REQ = 256;
    private static boolean sColdStart = true;
    private static final Logger sLogger = new Logger(DestinationSuggestionService.class);
    private static long sMaxIdentifier = -1;

    private enum UploadResult {
        SUCCESS,
        FAILURE
    }

    public static native void learn(double d, double d2, long j, double d3, double d4, int i, int i2, int i3);

    public static native void reset();

    public static native SuggestedDestination suggestDestination(double d, double d2, int i, int i2, int i3);

    static {
        NO_SUGGESTION = new Destination();
        System.loadLibrary(PLACE_SUGGESTION_LIBRARY_NAME);
        NO_SUGGESTION = new Destination();
        NO_SUGGESTION.setId(-1);
    }

    public void onCreate() {
        super.onCreate();
    }

    public DestinationSuggestionService() {
        super(SERVICE_NAME);
    }

    protected void onHandleIntent(Intent intent) {
        if (intent == null) {
            sLogger.e("received a call for onHandleIntent with a null intent!");
            return;
        }
        String action = intent.getAction();
        if (COMMAND_SUGGEST.equals(action)) {
            handleSuggest(intent);
        } else if (COMMAND_POPULATE.equals(action)) {
            handlePopulate();
        } else if (COMMAND_UPLOAD_TRIP_DATA.equals(action)) {
            handleUploadTripData();
        } else if (COMMAND_AUTO_UPLOAD_TRIP_DATA.equals(action)) {
            handleAutoTripUpload();
        } else if (COMMAND_RESET.equals(action)) {
            reset();
            sColdStart = true;
        }
    }

    private void handleSuggest(Intent intent) {
        boolean forThePhoneSuggestionList = intent.getBooleanExtra(EXTRA_LOCAL, true);
        sLogger.d("onHandleIntent : Command suggest, forThePhoneSuggestionList = " + forThePhoneSuggestionList);
        try {
            learnAllTrips();
            if (forThePhoneSuggestionList) {
                suggestDestinationBasedOnContext(true);
                return;
            }
            RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice == null || !remoteDevice.isConnected()) {
                sLogger.d("Remote device is not connected, so not making a suggestion");
            } else if (NavdyRouteHandler.getInstance().getCurrentState() == State.EN_ROUTE) {
                sLogger.d("There is an active trip going on in HUD, so not sending suggestion");
            } else {
                SuggestionManager.buildCalendarSuggestions(new SuggestionBuildListener() {
                    public void onSuggestionBuildComplete(ArrayList<Suggestion> suggestions) {
                        if (DestinationSuggestionService.this.firstSuggestionHasValidDestination(suggestions)) {
                            DestinationSuggestionService.this.sendSuggestionToHud(new Suggestion(((Suggestion) suggestions.get(0)).destination, SuggestionType.CALENDAR), SuggestedDestination.SuggestionType.SUGGESTION_CALENDAR);
                            return;
                        }
                        DestinationSuggestionService.this.suggestDestinationBasedOnContext(false);
                    }
                });
            }
        } catch (Throwable t) {
            sLogger.e("Error during destination suggestion ", t);
        }
    }

    private boolean firstSuggestionHasValidDestination(ArrayList<Suggestion> suggestions) {
        return (suggestions == null || suggestions.size() <= 0 || suggestions.get(0) == null || ((Suggestion) suggestions.get(0)).destination == null || !((Suggestion) suggestions.get(0)).destination.hasOneValidSetOfCoordinates()) ? false : true;
    }

    private void suggestDestinationBasedOnContext(boolean forThePhoneSuggestionList) {
        sLogger.d("Suggest Destination based on the context For suggestion list :" + forThePhoneSuggestionList);
        Date date = new Date(System.currentTimeMillis());
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTime(date);
        int hour = cal.get(11);
        int minute = cal.get(12);
        int dayOfTheWeek = cal.get(7);
        SuggestedDestination destination = null;
        Coordinate currentCoordinate = NavdyLocationManager.getInstance().getSmartStartCoordinates();
        if (currentCoordinate != null) {
            sLogger.d("Current location is known: " + currentCoordinate.latitude + DDL.SEPARATOR + currentCoordinate.longitude);
            destination = suggestDestination(currentCoordinate.latitude.doubleValue(), currentCoordinate.longitude.doubleValue(), hour, minute, dayOfTheWeek);
        } else {
            sLogger.d("Current location is unknown");
            if (forThePhoneSuggestionList) {
                destination = suggestDestination(0.0d, 0.0d, hour, minute, dayOfTheWeek);
            }
        }
        Destination savedDestination = null;
        if (destination != null) {
            savedDestination = NavdyContentProvider.getThisDestination((int) destination.uniqueIdentifier);
            if (savedDestination != null) {
                Location currentLocation = new Location("");
                if (currentCoordinate != null) {
                    currentLocation.setLatitude(currentCoordinate.latitude.doubleValue());
                    currentLocation.setLongitude(currentCoordinate.longitude.doubleValue());
                }
                Location location = new Location("");
                location.setLatitude(savedDestination.displayLat);
                location.setLongitude(savedDestination.displayLong);
                if (((double) location.distanceTo(currentLocation)) <= 200.0d) {
                    if (forThePhoneSuggestionList) {
                        BusProvider.getInstance().post(NO_SUGGESTION);
                    }
                    sLogger.d("Not suggesting as the destination with the identifier is very close to the current location");
                    sLogger.d("DestinationEngine , suggested destiantion,  ID: " + destination.uniqueIdentifier + ", Lat: " + destination.latitude + ", Long: " + destination.longitude + ", Frequency: " + destination.frequency + ", Probability: " + destination.probability);
                    return;
                } else if (forThePhoneSuggestionList) {
                    BusProvider.getInstance().post(savedDestination);
                } else {
                    sLogger.d("DestinationEngine: suggested destiantion: ID = " + destination.uniqueIdentifier + ", Lat = " + destination.latitude + ", Long = " + destination.longitude + ", Frequency = " + destination.frequency + ", Probability = " + destination.probability);
                    Suggestion suggestion = new Suggestion(savedDestination, SuggestionType.RECOMMENDATION);
                    sendSuggestionToHud(suggestion, SuggestedDestination.SuggestionType.SUGGESTION_RECOMMENDATION);
                }
            }
        }
        if (forThePhoneSuggestionList && savedDestination == null) {
            BusProvider.getInstance().post(NO_SUGGESTION);
        }
    }

    private void sendSuggestionToHud(@NonNull Suggestion suggestion, final SuggestedDestination.SuggestionType suggestionType) {
        final com.navdy.service.library.events.destination.Destination destinationMessage = suggestion.toProtobufDestinationObject();
        if (destinationMessage != null) {
            double latitude;
            double longitude;
            sLogger.d("DestinationEngine , destination message,  Title :" + destinationMessage.destination_title + ", Subtitle :" + destinationMessage.destination_subtitle + ", Type :" + destinationMessage.suggestion_type + ", Is Recommendation :" + destinationMessage.is_recommendation + ", Address :" + destinationMessage.full_address + ", Unique ID: " + destinationMessage.identifier);
            if (destinationMessage.navigation_position.latitude.doubleValue() == 0.0d && destinationMessage.navigation_position.longitude.doubleValue() == 0.0d) {
                latitude = destinationMessage.display_position.latitude.doubleValue();
                longitude = destinationMessage.display_position.longitude.doubleValue();
            } else {
                latitude = destinationMessage.navigation_position.latitude.doubleValue();
                longitude = destinationMessage.navigation_position.longitude.doubleValue();
            }
            HereRouteManager.getInstance().calculateRoute(latitude, longitude, new Listener() {
                public void onPreCalculation(@NonNull RouteHandle routeHandle) {
                }

                public void onRouteCalculated(@NonNull Error error, Route route) {
                    int routeDurationWithTraffic = -1;
                    if (error == Error.NONE && route.getTta(TrafficPenaltyMode.OPTIMAL, Route.WHOLE_ROUTE) != null) {
                        routeDurationWithTraffic = route.getTta(TrafficPenaltyMode.OPTIMAL, Route.WHOLE_ROUTE).getDuration();
                    }
                    DestinationSuggestionService.this.sendSuggestion(new SuggestedDestination(destinationMessage, Integer.valueOf(routeDurationWithTraffic), suggestionType));
                }
            });
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x01cb A:{SYNTHETIC, Splitter: B:31:0x01cb} */
    /* JADX WARNING: Removed duplicated region for block: B:92:? A:{SYNTHETIC, RETURN} */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01d0 A:{Catch:{ IOException -> 0x02a7 }} */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02b4 A:{SYNTHETIC, Splitter: B:68:0x02b4} */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x02b9 A:{Catch:{ IOException -> 0x02bd }} */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x01cb A:{SYNTHETIC, Splitter: B:31:0x01cb} */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01d0 A:{Catch:{ IOException -> 0x02a7 }} */
    /* JADX WARNING: Removed duplicated region for block: B:92:? A:{SYNTHETIC, RETURN} */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02b4 A:{SYNTHETIC, Splitter: B:68:0x02b4} */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x02b9 A:{Catch:{ IOException -> 0x02bd }} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handlePopulate() {
        Throwable t;
        Throwable th;
        Reader reader;
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "data.csv");
        if (file.exists()) {
            FileReader reader2 = null;
            BufferedReader buffReader = null;
            try {
                Reader fileReader = new FileReader(file);
                try {
                    int hour;
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    while (true) {
                        try {
                            String line = bufferedReader.readLine();
                            if (line == null) {
                                break;
                            }
                            if (!line.startsWith("Z_PK")) {
                                String[] parts = line.split(",");
                                long id = Long.parseLong(parts[0]);
                                long tripNumber = Long.parseLong(parts[5]);
                                long arrivedDestination = !StringUtils.isEmptyAfterTrim(parts[6]) ? Long.parseLong(parts[6]) : -1;
                                long chosenDestination = !StringUtils.isEmptyAfterTrim(parts[7]) ? Long.parseLong(parts[7]) : -1;
                                long inferredDestination = !StringUtils.isEmptyAfterTrim(parts[8]) ? Long.parseLong(parts[8]) : -1;
                                long destinationId = arrivedDestination != -1 ? arrivedDestination : chosenDestination != -1 ? chosenDestination : inferredDestination != -1 ? inferredDestination : -1;
                                double destLatitude = Double.parseDouble(parts[9]);
                                double destLongitude = Double.parseDouble(parts[10]);
                                long endTime = (long) (Double.parseDouble(parts[11]) + 9.783072E8d);
                                double startLatitude = Double.parseDouble(parts[12]);
                                double startLongitude = Double.parseDouble(parts[13]);
                                long startTime = (long) (Double.parseDouble(parts[14]) + 9.783072E8d);
                                sLogger.d("Start Time " + startTime);
                                int offset = SystemUtils.getTimeZoneAndDaylightSavingOffset(Long.valueOf(startTime));
                                Calendar cal = Calendar.getInstance(Locale.getDefault());
                                cal.setTime(new Date(1000 * startTime));
                                hour = cal.get(11);
                                int minute = cal.get(12);
                                int dayOfTheWeek = cal.get(7);
                                sLogger.d("MM/DD/YYY " + cal.get(2) + S3Constants.S3_FILE_DELIMITER + cal.get(5) + S3Constants.S3_FILE_DELIMITER + cal.get(1) + " , Day :" + dayOfTheWeek);
                                new Trip((int) id, tripNumber, startTime, offset, 0, startLatitude, startLongitude, endTime, 0, destLatitude, destLongitude, arrivedDestination, (int) destinationId).saveToDb(this);
                                learn(destLatitude, destLongitude, destinationId, startLatitude, startLongitude, hour, minute, dayOfTheWeek);
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            buffReader = bufferedReader;
                            reader = fileReader;
                            if (buffReader != null) {
                            }
                            if (reader2 != null) {
                            }
                            throw th;
                        }
                    }
                    for (int day = 1; day <= 7; day++) {
                        for (hour = 0; hour < 24; hour++) {
                            sLogger.d("Day :" + day + ", Hour :" + hour);
                            SuggestedDestination destination = suggestDestination(0.0d, 0.0d, hour, 0, day);
                            if (destination != null) {
                                sLogger.d("Destination :" + destination.latitude + DDL.SEPARATOR + destination.longitude + " ; ID :" + destination.uniqueIdentifier);
                            }
                        }
                    }
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (IOException e) {
                            sLogger.e("Error closing the buffered reader");
                            return;
                        }
                    }
                    if (fileReader != null) {
                        fileReader.close();
                    }
                } catch (Throwable th3) {
                    th = th3;
                    reader = fileReader;
                    if (buffReader != null) {
                    }
                    if (reader2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th4) {
                t = th4;
                sLogger.e("Exception ", t);
                if (buffReader != null) {
                    try {
                        buffReader.close();
                    } catch (IOException e2) {
                        sLogger.e("Error closing the buffered reader");
                        return;
                    }
                }
                if (reader2 != null) {
                    reader2.close();
                }
            }
        }
    }

    private void handleAutoTripUpload() {
        sLogger.d("handleAutoTripUpload : checking if we need an auto upload");
        final SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        long lastUploadTimeStamp = sharedPreferences.getLong(PREFERENCE_LAST_UPLOAD_TIME, 0);
        long currentTime = System.currentTimeMillis();
        long lastUploadedTripId = sharedPreferences.getLong(PREFERENCE_LAST_UPLOAD_ID, -1);
        final long maxTripId = NavdyContentProvider.getMaxTripId();
        boolean dataBaseChanged = maxTripId < lastUploadedTripId;
        if (dataBaseChanged) {
            sLogger.d("Database has changed");
        }
        if (dataBaseChanged || ((currentTime - lastUploadTimeStamp > MINIMUM_INTERVAL_BETWEEN_UPLOADS && (lastUploadedTripId == -1 || lastUploadedTripId < maxTripId)) || maxTripId > 10 + lastUploadedTripId)) {
            sLogger.d("Uploading the trip DB");
            Cursor tripCursor = null;
            if (lastUploadedTripId > 0) {
                try {
                    tripCursor = getTripCursor(lastUploadedTripId);
                } catch (Throwable th) {
                    IOUtils.closeStream(tripCursor);
                }
            } else {
                tripCursor = NavdyContentProvider.getTripsCursor();
            }
            String uniqueName = (SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.FULL_NAME, SettingsConstants.LAST_HUD_UUID_DEFAULT) + "_" + Build.MANUFACTURER + "_" + Build.MODEL).replaceAll("[^a-zA-Z0-9.-]", "_") + new SimpleDateFormat("_MM_dd_yyyy_HH_mm_ss_SSS", Locale.US).format(new Date(System.currentTimeMillis())) + "_Android_V" + BuildConfig.VERSION_CODE + ".csv";
            sLogger.d("Uploading the file to S3, Key : " + uniqueName);
            final Bus bus = new Bus(ThreadEnforcer.ANY);
            bus.register(new Object() {
                @Subscribe
                public void onProgress(UploadResult uploadResult) {
                    if (uploadResult == UploadResult.SUCCESS) {
                        sharedPreferences.edit().putLong(DestinationSuggestionService.PREFERENCE_LAST_UPLOAD_TIME, System.currentTimeMillis()).putLong(DestinationSuggestionService.PREFERENCE_LAST_UPLOAD_ID, maxTripId).apply();
                        DestinationSuggestionService.this.scheduleNextCheckForUpdate();
                    }
                    bus.unregister(this);
                }
            });
            uploadToS3(uniqueName, tripCursor, bus);
            IOUtils.closeStream(tripCursor);
            scheduleNextCheckForUpdate();
        }
    }

    private void scheduleNextCheckForUpdate() {
        sLogger.d("Scheduling for next check for update");
        Intent intent = new Intent(this, DestinationSuggestionService.class);
        intent.setAction(COMMAND_AUTO_UPLOAD_TRIP_DATA);
        ((AlarmManager) getSystemService("alarm")).set(1, System.currentTimeMillis() + MINIMUM_INTERVAL_BETWEEN_UPLOADS, PendingIntent.getService(this, 256, intent, 268435456));
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x0158 A:{SYNTHETIC, Splitter: B:45:0x0158} */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0168 A:{SYNTHETIC, Splitter: B:49:0x0168} */
    /* JADX WARNING: Removed duplicated region for block: B:119:? A:{SYNTHETIC, RETURN} */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x016d A:{SYNTHETIC, Splitter: B:52:0x016d} */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01b8 A:{ExcHandler: all (th java.lang.Throwable), Splitter: B:20:0x005f} */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01bc A:{SYNTHETIC, Splitter: B:80:0x01bc} */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01c1 A:{SYNTHETIC, Splitter: B:83:0x01c1} */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing block: B:72:0x01a6, code:
            r8 = move-exception;
     */
    /* JADX WARNING: Missing block: B:73:0x01a7, code:
            if (r19 != null) goto L_0x01a9;
     */
    /* JADX WARNING: Missing block: B:75:?, code:
            r19.post(com.navdy.client.app.framework.suggestion.DestinationSuggestionService.UploadResult.FAILURE);
     */
    /* JADX WARNING: Missing block: B:76:0x01b0, code:
            sLogger.e("Error closing the cursor ", r8);
     */
    /* JADX WARNING: Missing block: B:77:0x01b8, code:
            r10 = th;
     */
    /* JADX WARNING: Missing block: B:78:0x01b9, code:
            r4 = r5;
     */
    /* JADX WARNING: Missing block: B:81:?, code:
            r18.close();
     */
    /* JADX WARNING: Missing block: B:84:?, code:
            r4.close();
     */
    /* JADX WARNING: Missing block: B:106:0x020b, code:
            r8 = move-exception;
     */
    /* JADX WARNING: Missing block: B:107:0x020c, code:
            sLogger.e("Error closing the cursor ", r8);
     */
    /* JADX WARNING: Missing block: B:108:0x0214, code:
            r2 = move-exception;
     */
    /* JADX WARNING: Missing block: B:109:0x0215, code:
            sLogger.e("Error closing the file writer ", r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void uploadToS3(String fileName, Cursor tripDbCursor, Bus bus) {
        FileWriter fileWriter = null;
        try {
            File file = new File(getFilesDir() + File.separator + fileName);
            if (file.exists()) {
                IOUtils.deleteFile(NavdyApplication.getAppContext(), file.getAbsolutePath());
            }
            if (file.createNewFile()) {
                FileWriter fileWriter2 = new FileWriter(file, true);
                if (tripDbCursor != null) {
                    try {
                        if (tripDbCursor.getCount() > 0) {
                            fileWriter2.write("TRIP_ID,TRIP_NUMBER,TRIP_START_TIME,TRIP_START_TIME_ZONE_N_DST,TRIP_ODOMETER,TRIP_LATITUDE,TRIP_LONGITUDE,TRIP_END_TIME,TRIP_END_ODOMETER,TRIP_END_LATITUDE,TRIP_END_LONGITUDE,TRIP_ARRIVED_AT_DESTINATION,TRIP_DESTINATION_ID,TRIP_DESTINATION_LAT,TRIP_DESTINATION_LONG\n");
                            while (tripDbCursor.moveToNext()) {
                                Trip trip = NavdyContentProvider.getTripsItemAt(tripDbCursor, tripDbCursor.getPosition());
                                if (trip != null) {
                                    String str;
                                    double d;
                                    Destination savedDestination = NavdyContentProvider.getThisDestination(trip.destinationId);
                                    StringBuilder append = new StringBuilder().append(trip.id).append(",").append(trip.tripNumber).append(",").append(trip.startTime).append(",").append(trip.offset).append(",").append(trip.startOdometer).append(",").append(trip.startLat).append(",").append(trip.startLong).append(",").append(trip.endTime).append(",").append(trip.endOdometer).append(",").append(trip.endLat).append(",").append(trip.endLong).append(",");
                                    if (trip.arrivedAtDestination == 0) {
                                        str = "false";
                                    } else {
                                        str = "true";
                                    }
                                    StringBuilder append2 = append.append(str).append(",").append(trip.destinationId <= 0 ? "NA" : Integer.valueOf(trip.destinationId)).append(",");
                                    if (savedDestination != null) {
                                        d = savedDestination.navigationLat;
                                    } else {
                                        d = 0.0d;
                                    }
                                    append2 = append2.append(d).append(",");
                                    if (savedDestination != null) {
                                        d = savedDestination.navigationLong;
                                    } else {
                                        d = 0.0d;
                                    }
                                    fileWriter2.write(append2.append(d).append("\n").toString());
                                }
                            }
                            tripDbCursor.close();
                            fileWriter2.flush();
                            uploadFileToS3(file, bus);
                            if (tripDbCursor != null) {
                                try {
                                    tripDbCursor.close();
                                } catch (Throwable t) {
                                    sLogger.e("Error closing the cursor ", t);
                                }
                            }
                            if (fileWriter2 != null) {
                                try {
                                    fileWriter2.close();
                                    fileWriter = fileWriter2;
                                    return;
                                } catch (IOException e) {
                                    sLogger.e("Error closing the file writer ", e);
                                    fileWriter = fileWriter2;
                                    return;
                                }
                            }
                            return;
                        }
                    } catch (Throwable th) {
                    }
                }
                if (bus != null) {
                    bus.post(UploadResult.FAILURE);
                }
                if (tripDbCursor != null) {
                    try {
                        tripDbCursor.close();
                    } catch (Throwable t2) {
                        sLogger.e("Error closing the cursor ", t2);
                    }
                }
                if (fileWriter2 != null) {
                    try {
                        fileWriter2.close();
                    } catch (IOException e2) {
                        sLogger.e("Error closing the file writer ", e2);
                    }
                }
                fileWriter = fileWriter2;
                return;
            }
            if (tripDbCursor != null) {
                try {
                    tripDbCursor.close();
                } catch (Throwable t22) {
                    sLogger.e("Error closing the cursor ", t22);
                }
            }
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e22) {
                    sLogger.e("Error closing the file writer ", e22);
                }
            }
        } catch (Throwable th2) {
            if (bus != null) {
                try {
                    bus.post(UploadResult.FAILURE);
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    if (tripDbCursor != null) {
                    }
                    if (fileWriter != null) {
                    }
                    throw th4;
                }
            }
            sLogger.e("Error while dumping the trip database to the csv file");
            if (tripDbCursor != null) {
                try {
                    tripDbCursor.close();
                } catch (Throwable t222) {
                    sLogger.e("Error closing the cursor ", t222);
                }
            }
            if (fileWriter == null) {
                try {
                    fileWriter.close();
                } catch (IOException e222) {
                    sLogger.e("Error closing the file writer ", e222);
                }
            }
        }
    }

    private void handleUploadTripData() {
        sLogger.d("Uploading the trip data base to the S3");
        String fullName = SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.FULL_NAME, SettingsConstants.LAST_HUD_UUID_DEFAULT).replaceAll("[^a-zA-Z0-9.-]", "_");
        uploadToS3(fullName + "_" + Build.SERIAL + new SimpleDateFormat("_MM_dd_yyyy_HH_mm_ss_SSS", Locale.US).format(new Date(System.currentTimeMillis())) + ".csv", getTripCursor(), null);
    }

    private void learnAllTrips() {
        Cursor cursor = null;
        try {
            cursor = getTripCursor();
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Trip trip = NavdyContentProvider.getTripsItemAt(cursor, cursor.getPosition());
                    if (trip != null) {
                        learn(trip);
                    }
                }
                sColdStart = false;
            }
            IOUtils.closeStream(cursor);
        } catch (Throwable th) {
            IOUtils.closeStream(cursor);
        }
    }

    private void uploadFileToS3(final File file, final Bus bus) {
        if (file != null && file.exists()) {
            sLogger.d("Length of the file being uploaded: " + file.length());
            if (file.length() != 0) {
                new TransferUtility(OTAUpdateManagerImpl.createS3Client(), this).upload(BUCKET_NAME, file.getName(), file).setTransferListener(new TransferListener() {
                    public void onStateChanged(int id, TransferState state) {
                        DestinationSuggestionService.sLogger.d("Uploading File " + file.getName() + ", onStatChanged : " + id + DDL.SEPARATOR + state.name());
                        if (state == TransferState.COMPLETED) {
                            if (bus != null) {
                                bus.post(UploadResult.SUCCESS);
                            }
                        } else if (state == TransferState.FAILED && bus != null) {
                            bus.post(UploadResult.FAILURE);
                        }
                    }

                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        DestinationSuggestionService.sLogger.d("Uploading File " + file.getName() + ", onProgressChanged : " + id + ", Size :" + bytesCurrent + ", of " + bytesTotal);
                    }

                    public void onError(int id, Exception ex) {
                        DestinationSuggestionService.sLogger.e("Uploading File " + file.getName() + ", onError : " + id, ex);
                        if (bus != null) {
                            bus.post(UploadResult.FAILURE);
                        }
                    }
                });
            } else if (bus != null) {
                bus.post(UploadResult.FAILURE);
            }
        }
    }

    private void sendSuggestion(SuggestedDestination suggestedDestination) {
        RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
        if (remoteDevice != null && remoteDevice.isConnected()) {
            remoteDevice.postEvent((Message) suggestedDestination);
        }
    }

    private Cursor getTripCursor() {
        if (sColdStart) {
            sLogger.d("cold start, getting all the trips for learning");
            return NavdyContentProvider.getTripsCursor();
        }
        sLogger.d("warm start, getting trips greater than " + sMaxIdentifier);
        return getTripCursor(sMaxIdentifier);
    }

    private Cursor getTripCursor(long maxId) {
        return NavdyContentProvider.getTripsCursor(new Pair("_id > ?", new String[]{Long.toString(maxId)}));
    }

    private void learn(Trip trip) {
        if (((long) trip.id) > sMaxIdentifier) {
            sMaxIdentifier = (long) trip.id;
        }
        long destinationIdentifier = trip.destinationId > 0 ? (long) trip.destinationId : -1;
        Destination savedDestination = NavdyContentProvider.getThisDestination((int) destinationIdentifier);
        if (savedDestination == null || !savedDestination.doNotSuggest) {
            Calendar cal = Calendar.getInstance(Locale.getDefault());
            cal.setTime(new Date(trip.startTime));
            learn(trip.endLat, trip.endLong, destinationIdentifier, trip.startLat, trip.startLong, cal.get(11), cal.get(12), cal.get(7));
        }
    }

    public static void suggestDestination(Context context, boolean local) {
        Intent intent = new Intent(context, DestinationSuggestionService.class);
        intent.setAction(COMMAND_SUGGEST);
        intent.putExtra(EXTRA_LOCAL, local);
        context.startService(intent);
    }

    public static void uploadTripDatabase(Context context, boolean manual) {
        if (manual) {
            Intent intent = new Intent(context, DestinationSuggestionService.class);
            intent.setAction(manual ? COMMAND_UPLOAD_TRIP_DATA : COMMAND_AUTO_UPLOAD_TRIP_DATA);
            context.startService(intent);
            return;
        }
        sLogger.d("UploadTripDatabase :Not a debug build so skipping");
    }

    public static void reset(Context context) {
        Intent intent = new Intent(context, DestinationSuggestionService.class);
        intent.setAction(COMMAND_RESET);
        context.startService(intent);
    }
}
