package com.navdy.client.app.framework.suggestion;

public class SuggestedDestination {
    public long frequency;
    public double latitude;
    public double longitude;
    public double probability;
    public long uniqueIdentifier;
}
