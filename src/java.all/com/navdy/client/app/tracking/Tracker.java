package com.navdy.client.app.tracking;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.localytics.android.Localytics;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.LocalyticsManager;
import com.navdy.client.app.framework.util.ImageUtils;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.util.IOUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public final class Tracker {
    private static final boolean VERBOSE = false;
    private static final Logger logger = new Logger(Tracker.class);

    public static void tagEvent(String eventName) {
        LocalyticsManager.tagEvent(eventName);
    }

    public static void tagEvent(String eventName, Map<String, String> attributes) {
        LocalyticsManager.tagEvent(eventName, attributes);
    }

    public static void tagScreen(String screen) {
        LocalyticsManager.tagScreen(screen);
        HashMap<String, String> attributes = new HashMap(1);
        attributes.put("Current_Screen", screen);
        tagEvent(Event.SCREEN_VIEWED, attributes);
    }

    public static void registerUser(String email, String fullName) {
        LocalyticsManager.setCustomerEmail(email);
        LocalyticsManager.setCustomerFullName(fullName);
        Editor editor = SettingsUtils.getCustomerPreferences().edit();
        editor.putString("email", email);
        editor.putString(ProfilePreferences.FULL_NAME, fullName);
        editor.apply();
        SettingsUtils.incrementDriverProfileSerial();
    }

    public static void saveUserPhotoToInternalStorage(Bitmap photo) {
        saveBitmapToInternalStorage(photo, ProfilePreferences.USER_PHOTO_FILE_NAME);
    }

    public static void saveObdImageToInternalStorage(Bitmap photo) {
        saveBitmapToInternalStorage(photo, ProfilePreferences.OBD_IMAGE_FILE_NAME);
    }

    public static void resetPhotoAndLocation(@NonNull SharedPreferences customerPrefs) {
        customerPrefs.edit().remove(ProfilePreferences.CAR_OBD_LOCATION_NOTE).remove(ProfilePreferences.CAR_OBD_LOCATION_ACCESS_NOTE).remove(ProfilePreferences.CAR_OBD_LOCATION_NUM).apply();
        deleteObdImageFromInternalStorage();
    }

    private static void deleteObdImageFromInternalStorage() {
        deleteImageFromInternalStorage(ProfilePreferences.OBD_IMAGE_FILE_NAME);
    }

    private static void deleteImageFromInternalStorage(String filename) {
        try {
            IOUtils.deleteFile(NavdyApplication.getAppContext(), NavdyApplication.getAppContext().getFilesDir() + File.pathSeparator + filename);
        } catch (Throwable e) {
            logger.e(e);
        }
    }

    private static void saveBitmapToInternalStorage(Bitmap photo, String filename, CompressFormat compressFormat, int quality) {
        if (photo == null) {
            deleteImageFromInternalStorage(filename);
            return;
        }
        FileOutputStream outputStream = null;
        try {
            outputStream = NavdyApplication.getAppContext().openFileOutput(filename, 0);
            photo.compress(compressFormat, quality, outputStream);
        } catch (Throwable e) {
            logger.e(e);
        } finally {
            IOUtils.closeStream(outputStream);
        }
    }

    public static void saveBitmapToInternalStorage(Bitmap photo, String filename) {
        saveBitmapToInternalStorage(photo, filename, CompressFormat.JPEG, 90);
    }

    @Nullable
    public static Bitmap getUserProfilePhoto() {
        return getBitmapFromInternalStorage(ProfilePreferences.USER_PHOTO_FILE_NAME);
    }

    public static Bitmap getScaledProfilePhoto(int size) {
        try {
            File file = new File(NavdyApplication.getAppContext().getFilesDir() + S3Constants.S3_FILE_DELIMITER + ProfilePreferences.USER_PHOTO_FILE_NAME);
            if (!file.exists()) {
                return null;
            }
            Bitmap bitmap = ImageUtils.getScaledBitmap(file, size, size);
            if (bitmap == null) {
                return null;
            }
            logger.v("PhotoServiceHandler dim=" + size);
            return bitmap;
        } catch (Throwable t) {
            logger.e("PhotoServiceHandler:", t);
            return null;
        }
    }

    @Nullable
    public static Bitmap getObdImage() {
        return getBitmapFromInternalStorage(ProfilePreferences.OBD_IMAGE_FILE_NAME);
    }

    @Nullable
    public static Bitmap getBitmapFromInternalStorage(String filename) {
        filename = NavdyApplication.getAppContext().getFilesDir() + S3Constants.S3_FILE_DELIMITER + filename;
        if (!new File(filename).exists()) {
            return null;
        }
        Bitmap bitmap = null;
        try {
            return BitmapFactory.decodeFile(filename);
        } catch (Throwable e) {
            logger.e(e);
            return bitmap;
        }
    }

    public static void eraseUserProfilePhoto() {
        try {
            new File(NavdyApplication.getAppContext().getFilesDir() + S3Constants.S3_FILE_DELIMITER + ProfilePreferences.USER_PHOTO_FILE_NAME).delete();
        } catch (Exception e) {
            logger.e("Unable to erase user profile photo.", e);
        }
    }

    public static boolean isUserRegistered() {
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        return (StringUtils.isEmptyAfterTrim(customerPrefs.getString(ProfilePreferences.FULL_NAME, null)) || StringUtils.isEmptyAfterTrim(customerPrefs.getString("email", null))) ? false : true;
    }

    public static boolean hasUserSkipped() {
        return SettingsUtils.getCustomerPreferences().getBoolean(ProfilePreferences.SKIPPED_SIGNUP, false);
    }

    public static int calculateInSampleSize(Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static boolean weHaveCarInfo() {
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        return (StringUtils.isEmptyAfterTrim(customerPrefs.getString(ProfilePreferences.CAR_YEAR, "")) || StringUtils.isEmptyAfterTrim(customerPrefs.getString(ProfilePreferences.CAR_MAKE, "")) || StringUtils.isEmptyAfterTrim(customerPrefs.getString(ProfilePreferences.CAR_MODEL, ""))) ? false : true;
    }

    public static String getUserId() {
        String str = null;
        try {
            return Localytics.getCustomerId();
        } catch (Exception e) {
            SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
            String name = customerPrefs.getString(ProfilePreferences.FULL_NAME, str);
            String email = customerPrefs.getString("email", str);
            if (StringUtils.isEmptyAfterTrim(name) || StringUtils.isEmptyAfterTrim(email)) {
                return str;
            }
            return name + " <" + email + ">";
        }
    }
}
