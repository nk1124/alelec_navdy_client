package com.navdy.client.app.tracking;

import android.content.Context;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.FAVORITE_TYPE_VALUES;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.util.SystemUtils;
import java.util.HashMap;

public final class SetDestinationTracker {
    private static final boolean VERBOSE = false;
    private static final SetDestinationTracker instance = new SetDestinationTracker();
    private static final Logger logger = new Logger(SetDestinationTracker.class);
    private String destinationType = "";
    private String favoriteType = "";
    private String sourceValue = "";

    private SetDestinationTracker() {
    }

    public static SetDestinationTracker getInstance() {
        return instance;
    }

    public void setDestinationType(String destinationType) {
        if (!StringUtils.isEmptyAfterTrim(destinationType)) {
            this.destinationType = destinationType;
        }
    }

    public void setSourceValue(String sourceValue) {
        if (!StringUtils.isEmptyAfterTrim(sourceValue)) {
            this.sourceValue = sourceValue;
        }
    }

    public void setFavoriteType(String favoriteType) {
        if (!StringUtils.isEmptyAfterTrim(favoriteType)) {
            this.favoriteType = favoriteType;
        }
    }

    public String getDestinationType() {
        return this.destinationType;
    }

    public String getSourceValue() {
        return this.sourceValue;
    }

    public String getFavoriteType() {
        return this.favoriteType;
    }

    public void resetValues() {
        this.destinationType = "";
        this.sourceValue = "";
        this.favoriteType = "";
    }

    public void tagSetDestinationEvent(final Destination destination) {
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                HashMap<String, String> attributes = new HashMap();
                Context context = NavdyApplication.getAppContext();
                boolean isConnected = AppInstance.getInstance().isDeviceConnected();
                boolean isOnline = SystemUtils.isConnectedToNetwork(context);
                if (SetDestinationTracker.this.favoriteType.isEmpty()) {
                    SetDestinationTracker.this.favoriteType = FAVORITE_TYPE_VALUES.getFavoriteTypeValue(destination);
                }
                attributes.put(DestinationAttributes.NEXT_TRIP, String.valueOf(!isConnected));
                attributes.put(DestinationAttributes.ONLINE, String.valueOf(isOnline));
                attributes.put("Type", SetDestinationTracker.this.destinationType);
                attributes.put(DestinationAttributes.SOURCE, SetDestinationTracker.this.sourceValue);
                attributes.put(DestinationAttributes.FAVORITE_TYPE, SetDestinationTracker.this.favoriteType);
                Tracker.tagEvent(Event.SET_DESTINATION, attributes);
                SetDestinationTracker.this.resetValues();
            }
        }, 3);
    }
}
