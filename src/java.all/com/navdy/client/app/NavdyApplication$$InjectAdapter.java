package com.navdy.client.app;

import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.service.library.network.http.IHttpManager;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class NavdyApplication$$InjectAdapter extends Binding<NavdyApplication> implements Provider<NavdyApplication>, MembersInjector<NavdyApplication> {
    private Binding<TTSAudioRouter> mAudioRouter;
    private Binding<IHttpManager> mHttpManager;

    public NavdyApplication$$InjectAdapter() {
        super("com.navdy.client.app.NavdyApplication", "members/com.navdy.client.app.NavdyApplication", false, NavdyApplication.class);
    }

    public void attach(Linker linker) {
        this.mHttpManager = linker.requestBinding("com.navdy.service.library.network.http.IHttpManager", NavdyApplication.class, getClass().getClassLoader());
        this.mAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", NavdyApplication.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mHttpManager);
        injectMembersBindings.add(this.mAudioRouter);
    }

    public NavdyApplication get() {
        NavdyApplication result = new NavdyApplication();
        injectMembers(result);
        return result;
    }

    public void injectMembers(NavdyApplication object) {
        object.mHttpManager = (IHttpManager) this.mHttpManager.get();
        object.mAudioRouter = (TTSAudioRouter) this.mAudioRouter.get();
    }
}
