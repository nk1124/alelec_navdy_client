package com.navdy.client.app.providers;

import android.net.Uri;
import com.navdy.client.debug.util.S3Constants;
import java.util.concurrent.TimeUnit;

public class NavdyContentProviderConstants {
    public static final String DB_NAME = "navdy";
    static final int DB_VERSION = 19;
    public static final String DESTINATIONS_ADDRESS = "address";
    public static final String DESTINATIONS_CITY = "city";
    public static final String DESTINATIONS_CONTACT_LOOKUP_KEY = "contact_lookup_key";
    public static final Uri DESTINATIONS_CONTENT_URI = Uri.parse(DESTINATIONS_URL);
    public static final String DESTINATIONS_COUNTRY = "country";
    public static final String DESTINATIONS_COUNTRY_CODE = "countryCode";
    public static final String DESTINATIONS_DISPLAY_LAT = "displat";
    public static final String DESTINATIONS_DISPLAY_LONG = "displong";
    public static final String DESTINATIONS_DO_NOT_SUGGEST = "do_not_suggest";
    public static final String DESTINATIONS_FAVORITE_TYPE = "is_special";
    public static final String DESTINATIONS_ID = "_id";
    public static final String DESTINATIONS_LABEL = "favorite_label";
    public static final String DESTINATIONS_LAST_CONTACT_LOOKUP = "last_contact_lookup";
    public static final String DESTINATIONS_LAST_KNOWN_CONTACT_ID = "last_known_contact_id";
    public static final String DESTINATIONS_LAST_PLACE_ID_REFRESH = "last_place_id_refresh";
    public static final String DESTINATIONS_LAST_ROUTED_DATE = "last_routed_date";
    public static final String DESTINATIONS_NAME = "place_name";
    public static final String DESTINATIONS_NAVIGATION_LAT = "navlat";
    public static final String DESTINATIONS_NAVIGATION_LONG = "navlong";
    public static final String DESTINATIONS_ORDER = "favorite_listing_order";
    public static final String DESTINATIONS_PLACE_DETAIL_JSON = "place_detail_json";
    public static final String DESTINATIONS_PLACE_ID = "place_id";
    public static final String DESTINATIONS_PRECISION_LEVEL = "precision_level";
    static final String[] DESTINATIONS_PROJECTION = new String[]{"_id", DESTINATIONS_PLACE_ID, DESTINATIONS_LAST_PLACE_ID_REFRESH, DESTINATIONS_DISPLAY_LAT, DESTINATIONS_DISPLAY_LONG, DESTINATIONS_NAVIGATION_LAT, DESTINATIONS_NAVIGATION_LONG, DESTINATIONS_NAME, DESTINATIONS_ADDRESS, DESTINATIONS_STREET_NUMBER, DESTINATIONS_STREET_NAME, DESTINATIONS_CITY, "state", DESTINATIONS_ZIP_CODE, DESTINATIONS_COUNTRY, DESTINATIONS_COUNTRY_CODE, DESTINATIONS_DO_NOT_SUGGEST, DESTINATIONS_LAST_ROUTED_DATE, DESTINATIONS_LABEL, DESTINATIONS_ORDER, DESTINATIONS_FAVORITE_TYPE, DESTINATIONS_PLACE_DETAIL_JSON, DESTINATIONS_PRECISION_LEVEL, "type", DESTINATIONS_CONTACT_LOOKUP_KEY, DESTINATIONS_LAST_KNOWN_CONTACT_ID, DESTINATIONS_LAST_CONTACT_LOOKUP};
    public static final String DESTINATIONS_STATE = "state";
    public static final String DESTINATIONS_STREET_NAME = "street_name";
    public static final String DESTINATIONS_STREET_NUMBER = "street_number";
    static final String DESTINATIONS_TABLE_NAME = "destinations";
    public static final String DESTINATIONS_TYPE = "type";
    static final int DESTINATIONS_URI_CODE = 1;
    static final String DESTINATIONS_URI_PATH = "destinations";
    private static final String DESTINATIONS_URL = ("content://" + PROVIDER_NAME + S3Constants.S3_FILE_DELIMITER + "destinations");
    public static final String DESTINATIONS_ZIP_CODE = "zip_code";
    static final Uri DESTINATION_CACHE_CONTENT_URI = Uri.parse(DESTINATION_CACHE_URL);
    static final String DESTINATION_CACHE_DESTIANTION_ID = "destination_id";
    static final String DESTINATION_CACHE_ID = "_id";
    static final String DESTINATION_CACHE_LAST_RESPONSE_DATE = "last_response_date";
    static final String DESTINATION_CACHE_LOCATION_STRING = "location";
    static final String[] DESTINATION_CACHE_PROJECTION = new String[]{"_id", DESTINATION_CACHE_LAST_RESPONSE_DATE, DESTINATION_CACHE_LOCATION_STRING, "destination_id"};
    static final String DESTINATION_CACHE_TABLE_NAME = "destination_cache";
    static final int DESTINATION_CACHE_URI_CODE = 4;
    static final String DESTINATION_CACHE_URI_PATH = "destination_cache";
    private static final String DESTINATION_CACHE_URL = ("content://" + PROVIDER_NAME + S3Constants.S3_FILE_DELIMITER + "destination_cache");
    public static final int FAVORITE_CONTACT = -4;
    public static final int FAVORITE_CUSTOM = -1;
    public static final int FAVORITE_HOME = -3;
    public static final int FAVORITE_WORK = -2;
    static final long MAX_DESTINATION_CACHE_AGE = TimeUnit.DAYS.toMillis(30);
    static final long MAX_DESTINATION_CACHE_SIZE = 1000;
    public static final int NOT_A_FAVORITE = 0;
    public static final Uri PLAYLISTS_CONTENT_URI = Uri.parse(PLAYLISTS_URL);
    public static final String PLAYLISTS_ID = "playlist_id";
    public static final String PLAYLISTS_NAME = "playlist_name";
    static final String[] PLAYLISTS_PROJECTION = new String[]{"playlist_id", "playlist_name"};
    public static final String PLAYLISTS_TABLE_NAME = "playlists";
    public static final int PLAYLISTS_URI_CODE = 5;
    public static final String PLAYLISTS_URI_PATH = "playlists";
    private static final String PLAYLISTS_URL = ("content://" + PROVIDER_NAME + S3Constants.S3_FILE_DELIMITER + "playlists");
    public static final String PLAYLIST_MEMBERS_ALBUM = "album";
    public static final String PLAYLIST_MEMBERS_ARTIST = "artist";
    public static final Uri PLAYLIST_MEMBERS_CONTENT_URI = Uri.parse(PLAYLIST_MEMBERS_URL);
    public static final String PLAYLIST_MEMBERS_PLAYLIST_ID = "playlist_id";
    static final String[] PLAYLIST_MEMBERS_PROJECTION = new String[]{"playlist_id", "SourceId", "artist", "album", "title"};
    public static final String PLAYLIST_MEMBERS_SOURCE_ID = "SourceId";
    public static final String PLAYLIST_MEMBERS_TABLE_NAME = "playlist_members";
    public static final String PLAYLIST_MEMBERS_TITLE = "title";
    public static final int PLAYLIST_MEMBERS_URI_CODE = 6;
    public static final String PLAYLIST_MEMBERS_URI_PATH = "playlist_members";
    private static final String PLAYLIST_MEMBERS_URL = ("content://" + PROVIDER_NAME + S3Constants.S3_FILE_DELIMITER + "playlist_members");
    static final String PROVIDER_NAME = NavdyContentProvider.class.getName();
    static final Uri SEARCH_HISTORY_CONTENT_URI = Uri.parse(SEARCH_HISTORY_URL);
    static final String SEARCH_HISTORY_DATE = "last_searched_on";
    public static final String SEARCH_HISTORY_ID = "_id";
    static final String[] SEARCH_HISTORY_PROJECTION = new String[]{"_id", SEARCH_HISTORY_DATE, "query"};
    public static final String SEARCH_HISTORY_QUERY = "query";
    static final String SEARCH_HISTORY_TABLE_NAME = "search_history";
    static final int SEARCH_HISTORY_URI_CODE = 3;
    static final String SEARCH_HISTORY_URI_PATH = "search_history";
    private static final String SEARCH_HISTORY_URL = ("content://" + PROVIDER_NAME + S3Constants.S3_FILE_DELIMITER + "search_history");
    public static final String TRIPS_ARRIVED_AT_DESTINATION = "arrived_at_destination";
    public static final Uri TRIPS_CONTENT_URI = Uri.parse(TRIPS_URL);
    public static final String TRIPS_DESTINATION_ID = "destination_id";
    public static final String TRIPS_END_LAT = "end_lat";
    public static final String TRIPS_END_LONG = "end_long";
    public static final String TRIPS_END_ODOMETER = "end_odometer";
    public static final String TRIPS_END_TIME = "end_time";
    public static final String TRIPS_ID = "_id";
    static final String[] TRIPS_PROJECTION = new String[]{"_id", TRIPS_TRIP_NUMBER, TRIPS_START_TIME, TRIPS_START_TIME_ZONE_N_DST, TRIPS_START_ODOMETER, TRIPS_START_LAT, TRIPS_START_LONG, TRIPS_END_TIME, TRIPS_END_ODOMETER, TRIPS_END_LAT, TRIPS_END_LONG, TRIPS_ARRIVED_AT_DESTINATION, "destination_id"};
    public static final String TRIPS_START_LAT = "start_lat";
    public static final String TRIPS_START_LONG = "start_long";
    public static final String TRIPS_START_ODOMETER = "start_odometer";
    public static final String TRIPS_START_TIME = "start_time";
    public static final String TRIPS_START_TIME_ZONE_N_DST = "start_time_zone_n_dst";
    static final String TRIPS_TABLE_NAME = "trips";
    public static final String TRIPS_TRIP_NUMBER = "trip_number";
    static final int TRIPS_URI_CODE = 2;
    static final String TRIPS_URI_PATH = "trips";
    private static final String TRIPS_URL = ("content://" + PROVIDER_NAME + S3Constants.S3_FILE_DELIMITER + "trips");
}
