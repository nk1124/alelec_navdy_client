package com.navdy.client.app.ui.routing;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPolyline;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.Map.Animation;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapPolyline;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen;
import com.navdy.client.app.ui.base.BaseHereMapFragment;
import com.navdy.client.app.ui.base.BaseHereMapFragment.Error;
import com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentInitialized;
import com.navdy.client.app.ui.base.BaseHereMapFragment.OnHereMapFragmentReady;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.squareup.otto.Subscribe;

public class ActiveTripActivity extends BaseToolbarActivity implements NavdyRouteListener {
    private static final int HERE_PADDING = MapUtils.hereMapSidePadding;
    private static final float MAP_ORIENTATION = 0.0f;
    private MapPolyline currentProgressMapPolyline;
    private MapMarker destinationMapMarker;
    private BaseHereMapFragment hereMapFragment;
    private final NavdyRouteHandler navdyRouteHandler = NavdyRouteHandler.getInstance();
    private MapPolyline routeMapPolyline;

    public static void startActiveTripActivity(Context context) {
        if (context == null || !(context instanceof Activity)) {
            sLogger.e("startActiveTripActivity, context is invalid");
        } else if (context instanceof ActiveTripActivity) {
            sLogger.v("starting ActiveTripActivity from itself, no-op");
        } else {
            context.startActivity(new Intent(context, ActiveTripActivity.class));
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.active_trip_activity);
        new ToolbarBuilder().title((int) R.string.active_trip_title).build();
        this.hereMapFragment = (BaseHereMapFragment) getFragmentManager().findFragmentById(R.id.map_fragment);
        View northUpImg = findViewById(R.id.north_up_button);
        if (this.hereMapFragment != null) {
            Resources res = NavdyApplication.getAppContext().getResources();
            this.hereMapFragment.setUsableArea(res.getDimensionPixelSize(R.dimen.search_bar_margin) + MapUtils.hereMapTopDownPadding, (northUpImg.getWidth() + (res.getDimensionPixelSize(R.dimen.util_button_margin) * 2)) + MapUtils.hereMapSidePadding, HERE_PADDING, HERE_PADDING);
            this.hereMapFragment.whenInitialized(new OnHereMapFragmentInitialized() {
                public void onInit(@NonNull Map hereMap) {
                    hereMap.setTrafficInfoVisible(true);
                }

                public void onError(@NonNull Error error) {
                    ActiveTripActivity.this.logger.e("HereMapFragment failed to initialize");
                }
            });
        }
        TextView end = (TextView) findViewById(R.id.end_trip);
        if (end != null) {
            end.setVisibility(VISIBLE);
        }
    }

    protected void onResume() {
        super.onResume();
        this.navdyRouteHandler.addListener(this);
        updateOfflineBannerVisibility();
        Tracker.tagScreen(Screen.ACTIVE_TRIP);
    }

    protected void onPause() {
        this.navdyRouteHandler.removeListener(this);
        super.onPause();
    }

    public void centerOnDriver(View view) {
        if (this.hereMapFragment == null) {
            this.logger.e("Calling centerOnDriver with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new OnHereMapFragmentInitialized() {
                public void onInit(@NonNull Map hereMap) {
                    Location location = NavdyLocationManager.getInstance().getSmartStartLocation();
                    if (location != null) {
                        hereMap.setCenter(new GeoCoordinate(location.getLatitude(), location.getLongitude()), Animation.BOW);
                    }
                }

                public void onError(@NonNull Error error) {
                }
            });
        }
    }

    public void northUpOrientation(View view) {
        if (this.hereMapFragment == null) {
            this.logger.e("Calling northUpOrientation with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new OnHereMapFragmentInitialized() {
                public void onInit(@NonNull Map hereMap) {
                    hereMap.setOrientation(0.0f, Animation.BOW);
                }

                public void onError(@NonNull Error error) {
                }
            });
        }
    }

    public void onEndTripClick(View view) {
        showQuestionDialog(R.string.are_you_sure, R.string.do_you_want_to_end_trip, R.string.end_trip, R.string.cancel, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == -1) {
                    ActiveTripActivity.this.navdyRouteHandler.stopRouting();
                    ActiveTripActivity.this.finish();
                }
            }
        }, null);
    }

    public void onPendingRouteCalculating(@NonNull Destination destination) {
        finish();
    }

    public void onPendingRouteCalculated(@NonNull NavdyRouteHandler.Error error, @Nullable NavdyRouteInfo pendingRoute) {
        finish();
    }

    public void onRouteCalculating(@NonNull final Destination destination) {
        clearMapObjects();
        if (this.hereMapFragment == null) {
            this.logger.e("Calling hereMapFragment with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenReady(new OnHereMapFragmentReady() {
                public void onReady(@NonNull Map hereMap) {
                    ActiveTripActivity.this.destinationMapMarker = destination.getHereMapMarker();
                    if (ActiveTripActivity.this.destinationMapMarker != null) {
                        hereMap.addMapObject(ActiveTripActivity.this.destinationMapMarker);
                    }
                    ActiveTripActivity.this.hereMapFragment.centerOnUserLocationAndDestination(destination, Animation.LINEAR);
                }

                public void onError(@NonNull Error error) {
                }
            });
        }
    }

    public void onRouteCalculated(@NonNull NavdyRouteHandler.Error error, @NonNull NavdyRouteInfo route) {
    }

    public void onRouteStarted(@NonNull NavdyRouteInfo route) {
        startMapRoute(route);
    }

    public void onTripProgress(@NonNull final NavdyRouteInfo progress) {
        if (this.hereMapFragment == null) {
            this.logger.e("Calling onTripProgress with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new OnHereMapFragmentInitialized() {
                public void onInit(@NonNull Map hereMap) {
                    if (ActiveTripActivity.this.currentProgressMapPolyline != null) {
                        hereMap.removeMapObject(ActiveTripActivity.this.currentProgressMapPolyline);
                    }
                    GeoPolyline progressPolyline = progress.getProgress();
                    if (progressPolyline != null) {
                        ActiveTripActivity.this.currentProgressMapPolyline = MapUtils.generateProgressPolyline(progressPolyline);
                        hereMap.addMapObject(ActiveTripActivity.this.currentProgressMapPolyline);
                    }
                }

                public void onError(@NonNull Error error) {
                }
            });
        }
    }

    public void onReroute() {
    }

    public void onRouteArrived(@NonNull Destination destination) {
    }

    public void onStopRoute() {
        finish();
    }

    private void clearMapObjects() {
        if (this.hereMapFragment == null) {
            this.logger.e("Calling clearMapObjects with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenInitialized(new OnHereMapFragmentInitialized() {
                public void onInit(@NonNull Map hereMap) {
                    if (ActiveTripActivity.this.destinationMapMarker != null) {
                        hereMap.removeMapObject(ActiveTripActivity.this.destinationMapMarker);
                        ActiveTripActivity.this.destinationMapMarker = null;
                    }
                    if (ActiveTripActivity.this.routeMapPolyline != null) {
                        hereMap.removeMapObject(ActiveTripActivity.this.routeMapPolyline);
                        ActiveTripActivity.this.routeMapPolyline = null;
                    }
                    if (ActiveTripActivity.this.currentProgressMapPolyline != null) {
                        hereMap.removeMapObject(ActiveTripActivity.this.currentProgressMapPolyline);
                        ActiveTripActivity.this.currentProgressMapPolyline = null;
                    }
                }

                public void onError(@NonNull Error error) {
                }
            });
        }
    }

    private void startMapRoute(@NonNull final NavdyRouteInfo route) {
        clearMapObjects();
        if (this.hereMapFragment == null) {
            this.logger.e("Calling startMapRoute with a null hereMapFragment");
        } else {
            this.hereMapFragment.whenReady(new OnHereMapFragmentReady() {
                public void onReady(@NonNull Map hereMap) {
                    ActiveTripActivity.this.destinationMapMarker = route.getDestination().getHereMapMarker();
                    if (ActiveTripActivity.this.destinationMapMarker != null) {
                        hereMap.addMapObject(ActiveTripActivity.this.destinationMapMarker);
                    }
                    GeoPolyline geoPolyline = route.getRoute();
                    if (geoPolyline != null) {
                        ActiveTripActivity.this.routeMapPolyline = MapUtils.generateRoutePolyline(geoPolyline);
                        hereMap.addMapObject(ActiveTripActivity.this.routeMapPolyline);
                        ActiveTripActivity.this.hereMapFragment.centerOnRoute(geoPolyline);
                        return;
                    }
                    ActiveTripActivity.this.routeMapPolyline = null;
                }

                public void onError(@NonNull Error error) {
                }
            });
        }
    }

    @Subscribe
    public void handleReachabilityStateChange(ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        View offlineBanner = findViewById(R.id.offline_banner);
        if (offlineBanner != null) {
            offlineBanner.setVisibility(AppInstance.getInstance().canReachInternet() ? GONE : VISIBLE);
        }
    }

    public void onRefreshConnectivityClick(View view) {
        AppInstance.getInstance().checkForNetwork();
    }
}
