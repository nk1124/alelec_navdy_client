package com.navdy.client.app.ui.routing;

import com.navdy.client.app.framework.servicehandler.NetworkStatusManager;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class RoutingActivity$$InjectAdapter extends Binding<RoutingActivity> implements Provider<RoutingActivity>, MembersInjector<RoutingActivity> {
    private Binding<NetworkStatusManager> networkStatusManager;
    private Binding<BaseToolbarActivity> supertype;

    public RoutingActivity$$InjectAdapter() {
        super("com.navdy.client.app.ui.routing.RoutingActivity", "members/com.navdy.client.app.ui.routing.RoutingActivity", false, RoutingActivity.class);
    }

    public void attach(Linker linker) {
        this.networkStatusManager = linker.requestBinding("com.navdy.client.app.framework.servicehandler.NetworkStatusManager", RoutingActivity.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.client.app.ui.base.BaseToolbarActivity", RoutingActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.networkStatusManager);
        injectMembersBindings.add(this.supertype);
    }

    public RoutingActivity get() {
        RoutingActivity result = new RoutingActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(RoutingActivity object) {
        object.networkStatusManager = (NetworkStatusManager) this.networkStatusManager.get();
        this.supertype.injectMembers(object);
    }
}
