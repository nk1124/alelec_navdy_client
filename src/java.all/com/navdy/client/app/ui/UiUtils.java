package com.navdy.client.app.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Point;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.view.Display;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.service.library.util.IOUtils;
import java.io.ByteArrayOutputStream;

public class UiUtils {
    public static final float BLUR_RADIUS = 16.0f;
    private static RenderScript rs = null;

    public static int convertDpToPx(float dp) {
        return (int) TypedValue.applyDimension(1, dp, NavdyApplication.getAppContext().getResources().getDisplayMetrics());
    }

    public static Bitmap blurThisBitmap(Bitmap bitmapOriginal) {
        try {
            return blurThisBitmapTheFancyWay(bitmapOriginal);
        } catch (Exception e) {
            return blurThisBitmapTheGhettoWay(bitmapOriginal);
        }
    }

    private static Bitmap blurThisBitmapTheGhettoWay(Bitmap bitmapOriginal) {
        Throwable th;
        Options options = new Options();
        options.inSampleSize = 8;
        ByteArrayOutputStream stream = null;
        try {
            ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
            try {
                bitmapOriginal.compress(CompressFormat.PNG, 50, stream2);
                byte[] byteArray = stream2.toByteArray();
                Bitmap blurryBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
                IOUtils.closeStream(stream2);
                return blurryBitmap;
            } catch (Throwable th2) {
                th = th2;
                stream = stream2;
                IOUtils.closeStream(stream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            IOUtils.closeStream(stream);
            throw th;
        }
    }

    private static Bitmap blurThisBitmapTheFancyWay(Bitmap bitmapOriginal) {
        Context applicationContext = NavdyApplication.getAppContext();
        if (rs == null) {
            rs = RenderScript.create(applicationContext);
        }
        Allocation input = null;
        Allocation output = null;
        ScriptIntrinsicBlur script = null;
        try {
            input = Allocation.createFromBitmap(rs, bitmapOriginal);
            output = Allocation.createTyped(rs, input.getType());
            script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            script.setRadius(16.0f);
            script.setInput(input);
            script.forEach(output);
            Bitmap blurryBitmap = bitmapOriginal.copy(Config.ARGB_8888, false);
            output.copyTo(blurryBitmap);
            return blurryBitmap;
        } finally {
            if (input != null) {
                input.destroy();
            }
            if (output != null) {
                output.destroy();
            }
            if (script != null) {
                script.destroy();
            }
        }
    }

    public static int getGoogleMapBottomPadding(@NonNull Resources resources) {
        return (resources.getDimensionPixelSize(R.dimen.card_height) + resources.getDimensionPixelSize(R.dimen.section_header_height)) + resources.getDimensionPixelSize(R.dimen.google_map_bottom_overlap);
    }

    public static int getHereMapBottomPadding(@NonNull Resources resources) {
        return resources.getDimensionPixelSize(R.dimen.trip_card_height) + resources.getDimensionPixelSize(R.dimen.here_map_bottom_overlap);
    }

    public static int getTabsHeight(@NonNull Resources resources) {
        return resources.getDimensionPixelSize(R.dimen.tab_bar_height);
    }

    public static int getDisplayHeight(@NonNull Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static int getActionBarHeight(@NonNull Activity activity) {
        TypedValue tv = new TypedValue();
        if (activity.getTheme().resolveAttribute(16843499, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
        }
        return 0;
    }
}
