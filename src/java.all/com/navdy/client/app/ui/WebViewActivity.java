package com.navdy.client.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.common.GoogleApiAvailability;
import com.alelec.navdyclient.R;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.Web;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.service.library.util.IOUtils;

public class WebViewActivity extends BaseToolbarActivity {
    public static final String ATTRIBUTION = "Attributions";
    public static final String EXTRA_HTML = "extra_html";
    public static final String EXTRA_PREVENT_LINKS = "prevent_links";
    public static final String EXTRA_TYPE = "type";
    public static final String PRIVACY = "Privacy";
    public static final String TERMS = "Terms";
    public static final String ZENDESK = "Zendesk";
    private String type;

    /* JADX WARNING: Missing block: B:20:0x00b2, code:
            if (r4.equals(PRIVACY) != false) goto L_0x003f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void onCreate(Bundle savedInstanceState) {
        boolean z = false;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.web_view_activity);
        WebView webView = (WebView) findViewById(R.id.web_view);
        if (webView == null) {
            BaseActivity.showLongToast(R.string.unable_to_load_webview, new Object[0]);
            finish();
            return;
        }
        Intent intent = getIntent();
        this.type = intent.getStringExtra("type");
        boolean preventLinks = intent.getBooleanExtra(EXTRA_PREVENT_LINKS, false);
        String str = this.type;
        switch (str.hashCode()) {
            case -852110348:
                if (str.equals(ATTRIBUTION)) {
                    z = true;
                    break;
                }
            case 80697703:
                if (str.equals(TERMS)) {
                    z = true;
                    break;
                }
            case 1350155112:
                break;
            default:
                z = true;
                break;
        }
        switch (z) {
            case false:
                new ToolbarBuilder().title((int) R.string.privacy_policy).build();
                break;
            case true:
                new ToolbarBuilder().title((int) R.string.terms_of_service).build();
                break;
            case true:
                new ToolbarBuilder().title((int) R.string.acknowledgments).build();
                break;
        }
        if (preventLinks || PRIVACY.equals(this.type) || TERMS.equals(this.type)) {
            webView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return true;
                }

                public void onLoadResource(WebView view, String url) {
                }
            });
        } else if (ZENDESK.equals(this.type)) {
            webView.setWebViewClient(new WebViewClient() {
                public void onPageFinished(WebView finishedWebView, String url) {
                    try {
                        WebViewActivity.this.injectMobileStyleForZendeskPage(finishedWebView);
                    } catch (Exception e) {
                        WebViewActivity.this.logger.e("IOException: " + e);
                    }
                    super.onPageFinished(finishedWebView, url);
                }
            });
        }
        try {
            if (ATTRIBUTION.equals(this.type)) {
                String attribution = IOUtils.convertInputStreamToString(getResources().openRawResource(R.raw.attribution), "UTF-8");
                webView.getSettings().setUseWideViewPort(true);
                webView.loadDataWithBaseURL(null, attribution + GoogleApiAvailability.getInstance().getOpenSourceSoftwareLicenseInfo(this), "text/plain", null, null);
                return;
            }
            String fileName = null;
            if (PRIVACY.equals(this.type)) {
                fileName = "file:///android_res/raw/privacypolicy.html";
            } else if (TERMS.equals(this.type)) {
                fileName = "file:///android_res/raw/terms.html";
            } else if (ZENDESK.equals(this.type)) {
                fileName = intent.getStringExtra(EXTRA_HTML);
            }
            webView.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
            webView.loadUrl(fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Web.tag(this.type));
    }

    public void injectMobileStyleForZendeskPage(WebView webView) {
        try {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl("javascript:(function() {var parent = document.getElementsByTagName('head').item(0);var style = document.createElement('style');style.type = 'text/css';style.appendChild(document.createTextNode(\".request-nav, .footer, .comments-title, .comment-list, .comment-load-more-comments, .comment-closed-notification, .comment-submit-request { display: none !important }\"));parent.appendChild(style);})()");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
