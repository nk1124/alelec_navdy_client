package com.navdy.client.app.ui.settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.Switch;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.i18n.I18nManager;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.glances.GlanceUtils;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.DialLongPressAction;
import com.navdy.service.library.events.preferences.DriverProfilePreferences.UnitSystem;
import com.navdy.service.library.task.TaskManager;

public class GeneralSettingsActivity extends BaseEditActivity {
    private final BusProvider bus = BusProvider.getInstance();
    private Switch gestureSwitch;
    private RadioButton googleNow;
    private final I18nManager i18nManager = I18nManager.getInstance();
    private Switch limitCellDataSwitch;
    private final OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
            GeneralSettingsActivity.this.somethingChanged = true;
        }
    };
    private RadioButton placeSearch;
    private final SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
    private Switch smartSuggestionsSwitch;
    private final OnClickListener unitSystemClickListener = new OnClickListener() {
        public void onClick(View v) {
            GeneralSettingsActivity.this.somethingChanged = true;
        }
    };
    private RadioButton unitSystemImperial;
    private RadioButton unitSystemMetric;

    public static class LimitCellDataEvent {
        public final boolean hasLimitedCellData;

        LimitCellDataEvent(boolean hasLimitedCellData) {
            this.hasLimitedCellData = hasLimitedCellData;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.settings_general);
        new ToolbarBuilder().title((int) R.string.menu_general).build();
    }

    protected void onResume() {
        super.onResume();
        initUnitSystemSettings();
        initDialLongPress();
        initGesturesSettings();
        initLimitCellularDataSettings();
        initSmartSuggestionsSettings();
    }

    public void onSmartSuggestionsClick(View view) {
        this.smartSuggestionsSwitch.toggle();
    }

    public void onLimitCellularDataClick(View view) {
        this.limitCellDataSwitch.toggle();
    }

    public void onAllowGesturesClick(View view) {
        this.gestureSwitch.toggle();
    }

    protected void saveChanges() {
        int value;
        if (this.unitSystemMetric.isChecked()) {
            this.i18nManager.setUnitSystem(UnitSystem.UNIT_SYSTEM_METRIC);
        } else if (this.unitSystemImperial.isChecked()) {
            this.i18nManager.setUnitSystem(UnitSystem.UNIT_SYSTEM_IMPERIAL);
        } else {
            this.logger.e("invalid state for radio group");
        }
        GlanceUtils.saveGlancesConfigurationChanges(SettingsConstants.HUD_GESTURE, this.gestureSwitch.isChecked());
        SettingsUtils.sendHudSettings(this.gestureSwitch.isChecked());
        if (SettingsUtils.isLimitingCellularData() != this.limitCellDataSwitch.isChecked()) {
            SettingsUtils.setLimitCellularData(this.limitCellDataSwitch.isChecked());
            this.bus.post(new LimitCellDataEvent(this.limitCellDataSwitch.isChecked()));
        }
        Editor putBoolean = this.sharedPrefs.edit().putBoolean(SettingsConstants.RECOMMENDATIONS_ENABLED, this.smartSuggestionsSwitch.isChecked());
        String str = SettingsConstants.DIAL_LONG_PRESS_ACTION;
        if (this.placeSearch.isChecked()) {
            value = DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH.getValue();
        } else {
            value = DialLongPressAction.DIAL_LONG_PRESS_VOICE_ASSISTANT.getValue();
        }
        putBoolean.putInt(str, value).apply();
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                SettingsUtils.sendDriverProfileToTheHudBasedOnSharedPrefValue();
            }
        }, 1);
    }

    private void initUnitSystemSettings() {
        this.unitSystemMetric = (RadioButton) findViewById(R.id.unit_system_metric);
        this.unitSystemImperial = (RadioButton) findViewById(R.id.unit_system_imperial);
        UnitSystem unitSystem = this.i18nManager.getUnitSystem();
        if (unitSystem == UnitSystem.UNIT_SYSTEM_METRIC) {
            this.unitSystemMetric.setChecked(true);
        } else if (unitSystem == UnitSystem.UNIT_SYSTEM_IMPERIAL) {
            this.unitSystemImperial.setChecked(true);
        } else {
            this.logger.e("invalid unit system");
        }
        this.unitSystemMetric.setOnClickListener(this.unitSystemClickListener);
        this.unitSystemImperial.setOnClickListener(this.unitSystemClickListener);
    }

    private void initDialLongPress() {
        this.googleNow = (RadioButton) findViewById(R.id.dial_long_press_google_now);
        this.placeSearch = (RadioButton) findViewById(R.id.dial_long_press_place_search);
        SharedPreferences settingsPrefs = SettingsUtils.getSharedPreferences();
        if (settingsPrefs.getBoolean(SettingsConstants.HUD_DIAL_LONG_PRESS_CAPABLE, false)) {
            if (settingsPrefs.getInt(SettingsConstants.DIAL_LONG_PRESS_ACTION, SettingsConstants.DIAL_LONG_PRESS_ACTION_DEFAULT) != DialLongPressAction.DIAL_LONG_PRESS_PLACE_SEARCH.getValue()) {
                this.googleNow.setChecked(true);
            } else {
                this.placeSearch.setChecked(true);
            }
            this.googleNow.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    GeneralSettingsActivity.this.somethingChanged = true;
                    Tracker.tagEvent(Event.SWITCHED_TO_DEFAULT);
                }
            });
            this.placeSearch.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    GeneralSettingsActivity.this.somethingChanged = true;
                    Tracker.tagEvent(Event.SWITCHED_TO_PLACE_SEARCH);
                }
            });
            return;
        }
        hideIfFound(R.id.dial_long_press);
        hideIfFound(R.id.dial_long_press_group);
        hideIfFound(R.id.dial_long_press_desc);
    }

    private void hideIfFound(@IdRes int resId) {
        View v = findViewById(resId);
        if (v != null) {
            v.setVisibility(GONE);
        }
    }

    private void initGesturesSettings() {
        boolean gesturesAreEnabled = this.sharedPrefs.getBoolean(SettingsConstants.HUD_GESTURE, true);
        this.gestureSwitch = (Switch) findViewById(R.id.allow_gestures_switch);
        if (this.gestureSwitch != null) {
            this.gestureSwitch.setChecked(gesturesAreEnabled);
            this.gestureSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }

    private void initLimitCellularDataSettings() {
        boolean limitCellularDataEnabled = SettingsUtils.isLimitingCellularData();
        this.limitCellDataSwitch = (Switch) findViewById(R.id.limit_cell_data_switch);
        if (this.limitCellDataSwitch != null) {
            this.limitCellDataSwitch.setChecked(limitCellularDataEnabled);
            this.limitCellDataSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }

    private void initSmartSuggestionsSettings() {
        boolean smartSuggestionsEnabled = this.sharedPrefs.getBoolean(SettingsConstants.RECOMMENDATIONS_ENABLED, true);
        this.smartSuggestionsSwitch = (Switch) findViewById(R.id.smart_suggestions_switch);
        if (this.smartSuggestionsSwitch != null) {
            this.smartSuggestionsSwitch.setChecked(smartSuggestionsEnabled);
            this.smartSuggestionsSwitch.setOnCheckedChangeListener(this.onCheckedChangeListener);
        }
    }

    public void onLearnMoreClick(View view) {
        startActivity(new Intent(getApplicationContext(), GestureDialogActivity.class));
    }
}
