package com.navdy.client.app.ui.settings;

import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import com.alelec.navdyclient.R;

class CalendarGlobalSwitchViewHolder extends ViewHolder {
    CalendarGlobalSwitchViewHolder(View v, final OnCheckedChangeListener checkedChangeListener) {
        super(v);
        final Switch sweetch = (Switch) v.findViewById(R.id.sweetch);
        if (sweetch != null) {
            sweetch.setText(R.string.show_all_calendars);
            v.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    sweetch.callOnClick();
                }
            });
            SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
            if (sharedPrefs != null) {
                sweetch.setChecked(sharedPrefs.getBoolean(SettingsConstants.CALENDARS_ENABLED, true));
            }
            sweetch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    SharedPreferences sharedPrefs = SettingsUtils.getSharedPreferences();
                    if (sharedPrefs != null) {
                        sharedPrefs.edit().putBoolean(SettingsConstants.CALENDARS_ENABLED, isChecked).apply();
                        if (checkedChangeListener != null) {
                            checkedChangeListener.onCheckedChanged(buttonView, isChecked);
                        }
                    }
                }
            });
        }
    }
}
