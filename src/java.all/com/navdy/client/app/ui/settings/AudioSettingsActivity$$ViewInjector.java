package com.navdy.client.app.ui.settings;

import android.view.View;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import butterknife.internal.DebouncingOnClickListener;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.customviews.MultipleChoiceLayout;

public class AudioSettingsActivity$$ViewInjector {
    public static void inject(Finder finder, final AudioSettingsActivity target, Object source) {
        target.mWelcomeMessageSwitch = (Switch) finder.findRequiredView(source, R.id.welcome_message, "field 'mWelcomeMessageSwitch'");
        target.mSppedLimitWarnings = (Switch) finder.findRequiredView(source, R.id.speed_warnings, "field 'mSppedLimitWarnings'");
        target.mCameraWarnings = (Switch) finder.findRequiredView(source, R.id.camera_warnings, "field 'mCameraWarnings'");
        target.mTurnByTurnNavigation = (Switch) finder.findRequiredView(source, R.id.turn_by_turn_instructions, "field 'mTurnByTurnNavigation'");
        View view = finder.findRequiredView(source, R.id.speaker, "field 'mSpeaker' and method 'onClick'");
        target.mSpeaker = (RadioButton) view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onClick(p0);
            }
        });
        view = finder.findRequiredView(source, R.id.bluetooth, "field 'mBluetooth' and method 'onClick'");
        target.mBluetooth = (RadioButton) view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onClick(p0);
            }
        });
        target.mSmartBluetooth = (RadioButton) finder.findRequiredView(source, R.id.smart_bluetooth, "field 'mSmartBluetooth'");
        target.mTxtVoiceName = (TextView) finder.findRequiredView(source, R.id.txt_voice_name, "field 'mTxtVoiceName'");
        view = finder.findRequiredView(source, R.id.preference_voice, "field 'mVoicePreference' and method 'onClick'");
        target.mVoicePreference = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onClick(p0);
            }
        });
        view = finder.findRequiredView(source, R.id.preference_speech_delay, "field 'mSpeechDelayPreference' and method 'onClick'");
        target.mSpeechDelayPreference = view;
        view.setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onClick(p0);
            }
        });
        target.tvSpeechLevel = (TextView) finder.findRequiredView(source, R.id.speech_level, "field 'tvSpeechLevel'");
        target.mMainDescription = (TextView) finder.findRequiredView(source, R.id.audio_settings_description, "field 'mMainDescription'");
        target.speechLevelSettings = (MultipleChoiceLayout) finder.findRequiredView(source, R.id.audio_settings_speech_level, "field 'speechLevelSettings'");
        finder.findRequiredView(source, R.id.smart_bluetooth_container, "method 'onClick'").setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onClick(p0);
            }
        });
        finder.findRequiredView(source, R.id.btn_test_audio, "method 'onClick'").setOnClickListener(new DebouncingOnClickListener() {
            public void doClick(View p0) {
                target.onClick(p0);
            }
        });
    }

    public static void reset(AudioSettingsActivity target) {
        target.mWelcomeMessageSwitch = null;
        target.mSppedLimitWarnings = null;
        target.mCameraWarnings = null;
        target.mTurnByTurnNavigation = null;
        target.mSpeaker = null;
        target.mBluetooth = null;
        target.mSmartBluetooth = null;
        target.mTxtVoiceName = null;
        target.mVoicePreference = null;
        target.mSpeechDelayPreference = null;
        target.tvSpeechLevel = null;
        target.mMainDescription = null;
        target.speechLevelSettings = null;
    }
}
