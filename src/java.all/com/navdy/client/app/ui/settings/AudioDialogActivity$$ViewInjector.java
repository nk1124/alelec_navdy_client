package com.navdy.client.app.ui.settings;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import butterknife.internal.DebouncingOnClickListener;
import com.alelec.navdyclient.R;

public class AudioDialogActivity$$ViewInjector {
    public static void inject(Finder finder, final AudioDialogActivity target, Object source) {
        target.statusTitle = (TextView) finder.findOptionalView(source, R.id.title);
        target.statusImage = (ImageView) finder.findOptionalView(source, R.id.status_image);
        target.volumeProgress = (ProgressBar) finder.findOptionalView(source, R.id.volume);
        target.outputDeviceName = (TextView) finder.findOptionalView(source, R.id.audio_output_device_name);
        View view = finder.findOptionalView(source, R.id.settings);
        if (view != null) {
            view.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View p0) {
                    target.onClick(p0);
                }
            });
        }
        view = finder.findOptionalView(source, R.id.ok);
        if (view != null) {
            view.setOnClickListener(new DebouncingOnClickListener() {
                public void doClick(View p0) {
                    target.onClick(p0);
                }
            });
        }
    }

    public static void reset(AudioDialogActivity target) {
        target.statusTitle = null;
        target.statusImage = null;
        target.volumeProgress = null;
        target.outputDeviceName = null;
    }
}
