package com.navdy.client.app.ui.settings;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import com.alelec.navdyclient.R;

public class CalendarTitleViewHolder extends ViewHolder {
    private TextView name = null;

    public CalendarTitleViewHolder(View v) {
        super(v);
        if (v instanceof TextView) {
            this.name = (TextView) v;
        } else {
            this.name = (TextView) v.findViewById(R.id.calendar_name);
        }
    }

    public void setName(String name) {
        if (this.name != null) {
            this.name.setText(name);
        }
    }
}
