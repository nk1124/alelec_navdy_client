package com.navdy.client.app.ui.settings;

import android.content.SharedPreferences;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.ui.base.BaseEditActivity;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class AudioSettingsActivity$$InjectAdapter extends Binding<AudioSettingsActivity> implements Provider<AudioSettingsActivity>, MembersInjector<AudioSettingsActivity> {
    private Binding<TTSAudioRouter> mAudioRouter;
    private Binding<SharedPreferences> mSharedPrefs;
    private Binding<BaseEditActivity> supertype;

    public AudioSettingsActivity$$InjectAdapter() {
        super("com.navdy.client.app.ui.settings.AudioSettingsActivity", "members/com.navdy.client.app.ui.settings.AudioSettingsActivity", false, AudioSettingsActivity.class);
    }

    public void attach(Linker linker) {
        this.mSharedPrefs = linker.requestBinding("android.content.SharedPreferences", AudioSettingsActivity.class, getClass().getClassLoader());
        this.mAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", AudioSettingsActivity.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.client.app.ui.base.BaseEditActivity", AudioSettingsActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mSharedPrefs);
        injectMembersBindings.add(this.mAudioRouter);
        injectMembersBindings.add(this.supertype);
    }

    public AudioSettingsActivity get() {
        AudioSettingsActivity result = new AudioSettingsActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(AudioSettingsActivity object) {
        object.mSharedPrefs = (SharedPreferences) this.mSharedPrefs.get();
        object.mAudioRouter = (TTSAudioRouter) this.mAudioRouter.get();
        this.supertype.injectMembers(object);
    }
}
