package com.navdy.client.app.ui.base;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.client.app.ui.SimpleDialogFragment;
import com.navdy.client.app.ui.SimpleDialogFragment.DialogProvider;
import com.navdy.service.library.log.Logger;

public class BaseFragment extends Fragment implements DialogProvider {
    private static final String FRAGMENT_DIALOG = "DIALOG";
    protected BaseActivity baseActivity;
    protected final Handler handler = new Handler();
    protected final Logger logger = new Logger(getClass());
    private volatile boolean mIsInForeground = false;
    private ProgressDialog progressDialog = null;

    public void onCreate(Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        super.onCreate(savedInstanceState);
    }

    public void onPause() {
        this.logger.v("::onPause");
        super.onPause();
        this.mIsInForeground = false;
        hideProgressDialog();
        BusProvider.getInstance().unregister(this);
    }

    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
        this.mIsInForeground = true;
        BusProvider.getInstance().register(this);
    }

    public boolean isInForeground() {
        return this.mIsInForeground;
    }

    public void onAttach(Activity activity) {
        this.logger.v("::onAttach");
        super.onAttach(activity);
        if (activity instanceof BaseActivity) {
            this.baseActivity = (BaseActivity) activity;
        }
    }

    public void onDetach() {
        this.logger.v("::onDetach");
        this.baseActivity = null;
        super.onDetach();
    }

    public void onDestroy() {
        this.logger.v("::onDestroy");
        dismissProgressDialog();
        super.onDestroy();
    }

    public boolean isAlive() {
        return (this.baseActivity == null || this.baseActivity.isActivityDestroyed()) ? false : true;
    }

    public void showSimpleDialog(int id, String title, String message) {
        removeDialog();
        FragmentManager manager = getFragmentManager();
        if (manager != null) {
            Bundle bundle = new Bundle();
            bundle.putString("title", title);
            bundle.putString("message", message);
            SimpleDialogFragment.newInstance(id, bundle).show(manager, FRAGMENT_DIALOG);
        }
    }

    public void removeDialog() {
        if (isInForeground()) {
            FragmentManager manager = getFragmentManager();
            if (manager != null) {
                Fragment fragment = manager.findFragmentByTag(FRAGMENT_DIALOG);
                if (fragment != null) {
                    getFragmentManager().beginTransaction().remove(fragment).commit();
                }
            }
        }
    }

    public Dialog createDialog(int id, Bundle arguments) {
        if (arguments == null) {
            return null;
        }
        if (arguments.get("title") == null && arguments.get("message") == null) {
            return null;
        }
        Builder builder = new Builder(getActivity());
        builder.setCancelable(true);
        String title = arguments.getString("title");
        if (title != null) {
            builder.setTitle(title);
        }
        String message = arguments.getString("message");
        if (message != null) {
            builder.setMessage(message);
        }
        builder.setPositiveButton(getString(R.string.ok), null);
        return builder.create();
    }

    public void showProgressDialog() {
        this.progressDialog = BaseActivity.showProgressDialog(getActivity(), this.progressDialog);
    }

    public void hideProgressDialog() {
        BaseActivity.hideProgressDialog(getActivity(), this.progressDialog);
    }

    public void dismissProgressDialog() {
        BaseActivity.dismissProgressDialog(getActivity(), this.progressDialog);
    }
}
