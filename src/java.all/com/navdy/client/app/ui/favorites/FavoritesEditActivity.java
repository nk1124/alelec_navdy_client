package com.navdy.client.app.ui.favorites;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.providers.NavdyContentProvider.QueryResultCallback;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseEditActivity;
import com.navdy.client.app.ui.base.BaseGoogleMapFragment;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.bluetooth.BluetoothFramelayoutFragment;
import com.navdy.client.app.ui.settings.BluetoothPairActivity;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.task.TaskManager;

public class FavoritesEditActivity extends BaseEditActivity {
    public static final String EXTRA_FAVORITE = "favorite";
    private Destination destination = new Destination();
    private BaseGoogleMapFragment googleMapFragment;
    private EditText label;
    private Logger logger = new Logger(getClass());
    private Toolbar myToolbar;
    private RelativeLayout offlineBanner;

    public static void startFavoriteEditActivity(Activity activity, Parcelable favorite) {
        Intent intent = new Intent(activity.getApplicationContext(), FavoritesEditActivity.class);
        intent.putExtra(EXTRA_FAVORITE, favorite);
        activity.startActivity(intent);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.favorites_edit_activity);
        Injector.inject(NavdyApplication.getAppContext(), this);
        this.myToolbar = new ToolbarBuilder().title((int) R.string.edit_favorite).build();
        this.label = (EditText) findViewById(R.id.label_input);
        TextView address = (TextView) findViewById(R.id.address_input);
        this.offlineBanner = (RelativeLayout) findViewById(R.id.offline_banner);
        this.googleMapFragment = (BaseGoogleMapFragment) getFragmentManager().findFragmentById(R.id.map);
        Intent in = getIntent();
        if (in != null) {
            this.destination = (Destination) in.getParcelableExtra(EXTRA_FAVORITE);
        }
        if (!(address == null || this.destination == null)) {
            address.setText(StringUtils.join(this.destination.getAddressLines(), "\n"));
        }
        if (!(this.label == null || this.destination == null)) {
            if (this.destination.favoriteType == -3 || this.destination.favoriteType == -2) {
                TextView labelText = (TextView) findViewById(R.id.label_text);
                if (labelText != null) {
                    this.label.setVisibility(INVISIBLE);
                    if (this.destination.favoriteType == -3) {
                        labelText.setText(R.string.home);
                    } else {
                        labelText.setText(R.string.work);
                    }
                    labelText.setVisibility(VISIBLE);
                }
            } else {
                this.label.setText(this.destination.getFavoriteLabel());
                this.label.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (!StringUtils.equalsOrBothEmptyAfterTrim(FavoritesEditActivity.this.destination.getFavoriteLabel(), s)) {
                            FavoritesEditActivity.this.somethingChanged = true;
                        }
                    }

                    public void afterTextChanged(Editable s) {
                    }
                });
            }
        }
        if (!AppInstance.getInstance().canReachInternet()) {
            this.offlineBanner.setVisibility(VISIBLE);
        }
        if (this.googleMapFragment != null) {
            this.googleMapFragment.getMapAsync(new OnMapReadyCallback() {
                public void onMapReady(GoogleMap googleMap) {
                    LatLng position = null;
                    if (FavoritesEditActivity.this.destination != null) {
                        if (FavoritesEditActivity.this.destination.hasValidDisplayCoordinates()) {
                            position = new LatLng(FavoritesEditActivity.this.destination.displayLat, FavoritesEditActivity.this.destination.displayLong);
                        } else if (FavoritesEditActivity.this.destination.hasValidNavCoordinates()) {
                            position = new LatLng(FavoritesEditActivity.this.destination.navigationLat, FavoritesEditActivity.this.destination.navigationLong);
                        }
                    }
                    if (position != null) {
                        FavoritesEditActivity.this.googleMapFragment.moveMap(position, 14.0f, false);
                        int pinAsset = FavoritesEditActivity.this.destination.getPinAsset();
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.title(FavoritesEditActivity.this.destination.getFavoriteLabel());
                        markerOptions.draggable(false);
                        markerOptions.position(position);
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(pinAsset));
                        googleMap.addMarker(markerOptions);
                        Location location = new Location("");
                        location.setLatitude(position.latitude);
                        location.setLongitude(position.longitude);
                        FavoritesEditActivity.this.googleMapFragment.moveMap(location, 14.0f, false);
                        return;
                    }
                    FavoritesEditActivity.this.googleMapFragment.centerOnLastKnownLocation(false);
                }
            });
        }
    }

    protected void onResume() {
        super.onResume();
        Tracker.tagScreen(Screen.EDIT_FAVORITES);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        new Intent(getApplicationContext(), BluetoothPairActivity.class).putExtra(BluetoothFramelayoutFragment.EXTRA_KILL_ACTIVITY_ON_CANCEL, true);
        this.myToolbar.getMenu().add(0, R.id.menu_delete, 0, R.string.delete_btn).setIcon(R.drawable.ic_delete).setShowAsAction(2);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            case R.id.menu_delete /*2131756065*/:
                onDelete(null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onDelete(View view) {
        if (!this.destination.isPersisted()) {
            this.logger.e("Trying to delete a favorite that is not in DB: " + this.destination);
        }
        showProgressDialog();
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                FavoritesEditActivity.this.destination.deleteFavoriteFromDbAsync(new QueryResultCallback() {
                    public void onQueryCompleted(int nbRows, @Nullable Uri uri) {
                        FavoritesEditActivity.this.hideProgressDialog();
                        if (nbRows <= 0) {
                            FavoritesEditActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    BaseActivity.showLongToast(R.string.toast_favorite_delete_error, new Object[0]);
                                }
                            });
                        }
                        FavoritesEditActivity.this.saveOnExit = false;
                        FavoritesEditActivity.this.finish();
                    }
                });
            }
        }, 3);
    }

    protected void saveChanges() {
        if (this.saveOnExit) {
            if (this.destination.id == 0) {
                this.destination.reloadSelfFromDatabaseAsync(new Runnable() {
                    public void run() {
                        if (FavoritesEditActivity.this.destination.id <= 0) {
                            FavoritesEditActivity.this.logger.e("Missing favorite id. Currently unable to save.");
                            BaseActivity.showLongToast(R.string.toast_favorite_edit_error, new Object[0]);
                            return;
                        }
                        FavoritesEditActivity.this.saveChanges();
                    }
                });
            }
            final String labelString = this.label.getText().toString().trim();
            new AsyncTask<Object, Object, Boolean>() {
                protected Boolean doInBackground(Object... params) {
                    boolean success = false;
                    if (!StringUtils.isEmptyAfterTrim(labelString)) {
                        if (StringUtils.equalsOrBothEmptyAfterTrim(FavoritesEditActivity.this.destination.getFavoriteLabel(), labelString)) {
                            cancel(true);
                        } else {
                            FavoritesEditActivity.this.destination.setFavoriteLabel(labelString);
                            success = FavoritesEditActivity.this.destination.updateOnlyFavoriteFieldsInDb() > 0;
                        }
                    }
                    return Boolean.valueOf(success);
                }

                protected void onPostExecute(Boolean success) {
                    if (FavoritesEditActivity.this.destination.favoriteType != -1) {
                        return;
                    }
                    if (success.booleanValue()) {
                        BaseActivity.showLongToast(R.string.toast_favorite_edit_saved, new Object[0]);
                    } else {
                        BaseActivity.showLongToast(R.string.toast_favorite_edit_error, new Object[0]);
                    }
                }
            }.execute(new Object[0]);
        }
    }

    public void onRefreshConnectivityClick(View view) {
        AppInstance.getInstance().checkForNetwork();
    }
}
