package com.navdy.client.app.ui.favorites;

public interface ItemTouchHelperAdapter {
    void onItemDismiss(int i);

    void onItemMove(int i, int i2);

    void setupDynamicShortcuts();
}
