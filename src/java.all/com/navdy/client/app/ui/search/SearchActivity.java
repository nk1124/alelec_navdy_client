package com.navdy.client.app.ui.search;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.google.android.gms.actions.SearchIntents;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.SphericalUtil;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.LocalyticsManager;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor;
import com.navdy.client.app.framework.map.NavCoordsAddressProcessor.OnCompleteCallback;
import com.navdy.client.app.framework.models.ContactModel;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.SearchType;
import com.navdy.client.app.framework.models.GoogleTextSearchDestinationResult;
import com.navdy.client.app.framework.search.GooglePlacesSearch;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Error;
import com.navdy.client.app.framework.search.GooglePlacesSearch.GoogleSearchListener;
import com.navdy.client.app.framework.search.GooglePlacesSearch.Query;
import com.navdy.client.app.framework.search.GooglePlacesSearch.ServiceTypes;
import com.navdy.client.app.framework.search.NavdySearch;
import com.navdy.client.app.framework.search.NavdySearch.SearchCallback;
import com.navdy.client.app.framework.search.SearchResults;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent;
import com.navdy.client.app.framework.util.ContactsManager;
import com.navdy.client.app.framework.util.ContactsManager.Attributes;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.SystemUtils;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.providers.NavdyContentProvider;
import com.navdy.client.app.tracking.SetDestinationTracker;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.DestinationAttributes.TYPE_VALUES;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.ui.UiUtils;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.base.BaseGoogleMapFragment;
import com.navdy.client.app.ui.base.BaseSupportFragment;
import com.navdy.client.app.ui.customviews.DestinationImageView;
import com.navdy.client.app.ui.details.DetailsActivity;
import com.navdy.client.app.ui.search.SearchConstants.SEARCH_TYPES;
import com.navdy.service.library.events.location.Coordinate;
import com.navdy.service.library.events.places.PlacesSearchResult;
import com.navdy.service.library.task.TaskManager;
import com.navdy.service.library.task.TaskManager.TaskPriority;
import com.squareup.otto.Subscribe;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Inject;

public class SearchActivity extends BaseActivity implements LocationListener, OnClickListener, ConnectionCallbacks, GoogleSearchListener, OnConnectionFailedListener {
    private static final int AUTO_COMPLETE_MARGIN_BOTTOM = UiUtils.convertDpToPx(2.0f);
    private static final int AUTO_COMPLETE_MARGIN_LEFT = UiUtils.convertDpToPx(10.5f);
    private static final int AUTO_COMPLETE_MARGIN_RIGHT = UiUtils.convertDpToPx(10.5f);
    private static final int AUTO_COMPLETE_MARGIN_TOP = UiUtils.convertDpToPx(-14.0f);
    private static final String EXTRA_LOCATION = "location";
    private static final String EXTRA_SPEECH_RECOGNITION = "speech_recognition";
    private static final int KEY_LISTENER_DELAY = 500;
    private static final int VOICE_RECOGNITION_REQUEST_CODE = 42;
    private static boolean hasDisplayedOfflineMessage = false;
    private RelativeLayout card;
    private ImageButton centerMap;
    private EditText editText;
    private ImageButton fab;
    private boolean fromActionSendIntent = false;
    private BaseGoogleMapFragment googleMapFragment;
    private GooglePlacesSearch googlePlacesSearch;
    private AtomicBoolean ignoreAutoCompleteDelay = new AtomicBoolean(false);
    private AtomicBoolean ignoreNextKeystrokeKey = new AtomicBoolean(false);
    private boolean isComingFromGoogleNow = false;
    private boolean isShowingMap = false;
    private Location lastLocation;
    @Inject
    TTSAudioRouter mAudioRouter;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private final NavdyLocationManager navdyLocationManager = NavdyLocationManager.getInstance();
    private RelativeLayout offlineBanner;
    private Marker previousSelectedMarker;
    private boolean requestedLocationUpdates;
    private Destination requestedPlaceDetailsDestinationBackup;
    private boolean rightButtonIsMic = true;
    private SearchRecyclerAdapter searchAdapter;
    private RecyclerView searchRecycler;
    private int searchType;
    private boolean showServices = true;

    private class ServiceAndHistoryAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
        private ServiceAndHistoryAsyncTask() {
        }

//        /* synthetic */ ServiceAndHistoryAsyncTask(SearchActivity x0, AnonymousClass1 x1) {
//            this();
//        }

        protected void onPreExecute() {
            SearchActivity.this.searchAdapter.clear();
        }

        protected Result doInBackground(Params[] paramsArr) {
            SearchActivity.this.searchAdapter.setUpServicesAndHistory(SearchActivity.this.showServices);
            return null;
        }

        protected void onPostExecute(Result result) {
            SearchActivity.this.searchAdapter.notifyDataSetChanged();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.search_activity);
        Injector.inject(NavdyApplication.getAppContext(), this);
        checkServices();
        setupToolbarButtons();
        this.searchRecycler = (RecyclerView) findViewById(R.id.search_recycler_view);
        this.card = (RelativeLayout) findViewById(R.id.card);
        this.offlineBanner = (RelativeLayout) findViewById(R.id.offline_banner);
        this.fab = (ImageButton) findViewById(R.id.fab);
        this.centerMap = (ImageButton) findViewById(R.id.center_on_driver_button);
        this.googleMapFragment = (BaseGoogleMapFragment) getFragmentManager().findFragmentById(R.id.map);
        requestLocationPermission(new Runnable() {
            public void run() {
                SearchActivity.this.buildAndConnectGoogleApiClient();
                Context context = SearchActivity.this.getApplicationContext();
                if (ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 && ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                    SearchActivity.this.lastLocation = LocationServices.FusedLocationApi.getLastLocation(SearchActivity.this.mGoogleApiClient);
                }
                SearchActivity.this.getLatLngBounds();
                SearchActivity.this.requestContactsPermission();
            }
        }, new Runnable() {
            public void run() {
                BaseActivity.showLongToast(R.string.cant_search_without_location_permission, new Object[0]);
                SearchActivity.this.requestContactsPermission();
            }
        });
        setRecyclerView();
        setEditTextListener();
        this.googlePlacesSearch = new GooglePlacesSearch(this, this.handler);
        setHintText();
        Intent intent = getIntent();
        if (intent.getBooleanExtra(EXTRA_SPEECH_RECOGNITION, false)) {
            startVoiceRecognitionActivity();
        } else {
            boolean z;
            String action = intent.getAction();
            boolean isGoogleNowSearch = SearchIntents.ACTION_SEARCH.equals(action);
            this.fromActionSendIntent = SearchConstants.ACTION_SEND.equals(action);
            boolean isDestinationFinderSearch = SearchConstants.ACTION_SEARCH.equals(action);
            if (isGoogleNowSearch || isDestinationFinderSearch) {
                z = true;
            } else {
                z = false;
            }
            this.isComingFromGoogleNow = z;
            if (isGoogleNowSearch) {
                LocalyticsManager.tagEvent(Event.SEARCH_USING_GOOGLE_NOW);
            }
            if (isDestinationFinderSearch) {
                LocalyticsManager.tagEvent(Event.SEARCH_USING_DESTINATION_FINDER);
            }
            this.logger.d("Search is coming from Google Now = " + this.isComingFromGoogleNow);
            if ((this.isComingFromGoogleNow || this.fromActionSendIntent) && intent.hasExtra("query")) {
                String query = intent.getStringExtra("query");
                if (!StringUtils.isEmptyAfterTrim(query)) {
                    showProgressDialog();
                    if (this.editText != null) {
                        this.ignoreNextKeystrokeKey.set(true);
                        this.editText.setText(query);
                    }
                    runTextSearchOrAskHud(query);
                }
            }
        }
        SetDestinationTracker.getInstance().setSourceValue("Search");
        showAutoComplete();
        findViewById(R.id.touchDetection).setOnTouchListener(new OnTouchListener() {
            @SuppressLint({"ClickableViewAccessibility"})
            public boolean onTouch(View v, MotionEvent event) {
                SystemUtils.dismissKeyboard(SearchActivity.this);
                SearchActivity.this.searchRecycler.requestFocus();
                return false;
            }
        });
    }

    protected void onResume() {
        super.onResume();
        updateOfflineBannerVisibility();
        Tracker.tagScreen("Search");
    }

    public void onBackPressed() {
        if (this.editText == null || this.editText.getText().length() <= 0) {
            super.onBackPressed();
        } else {
            resetEditText();
        }
    }

    private void requestContactsPermission() {
        requestContactsPermission(null, new Runnable() {
            public void run() {
                BaseActivity.showLongToast(R.string.cant_search_contacts_without_location_permission, new Object[0]);
            }
        });
    }

    private void setupToolbarButtons() {
        ImageButton backButton = (ImageButton) findViewById(R.id.back_button);
        if (backButton != null) {
            backButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    SearchActivity.this.onBackPressed();
                }
            });
            ImageButton rightButton = (ImageButton) findViewById(R.id.right_button);
            if (rightButton != null) {
                rightButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View view) {
                        if (SearchActivity.this.rightButtonIsMic) {
                            SystemUtils.dismissKeyboard(SearchActivity.this);
                            SearchActivity.this.startVoiceRecognitionActivity();
                            return;
                        }
                        SearchActivity.this.resetEditText();
                    }
                });
            }
        }
    }

    private void resetEditText() {
        setCurrentMode(Mode.EMPTY);
        this.editText.setText("");
    }

    private void updateRightButton() {
        ImageButton rightButton = (ImageButton) findViewById(R.id.right_button);
        if (rightButton != null) {
            rightButton.setImageResource(this.rightButtonIsMic ? R.drawable.icon_searchbar_mic : R.drawable.icon_searchbar_close);
        }
    }

    private void startVoiceRecognitionActivity() {
        Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        intent.putExtra("calling_package", getClass().getPackage().getName());
        intent.putExtra("android.speech.extra.PROMPT", getString(R.string.search_or_say_address));
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        intent.putExtra("android.speech.extra.MAX_RESULTS", 5);
        try {
            startActivityForResult(intent, 42);
        } catch (Exception e) {
            openMarketAppFor("com.google.android.googlequicksearchbox");
        }
    }

    private void checkServices() {
        Intent intent = getIntent();
        if (intent != null) {
            this.searchType = intent.getIntExtra(SearchConstants.SEARCH_TYPE_EXTRA, SEARCH_TYPES.SEARCH.getCode());
            this.showServices = this.searchType == SEARCH_TYPES.SEARCH.getCode();
        }
    }

    private void setRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(1);
        this.searchAdapter = new SearchRecyclerAdapter(this.searchType, this.googleMapFragment, this.card);
        if (this.searchRecycler != null) {
            this.searchRecycler.setAdapter(this.searchAdapter);
        }
        new ServiceAndHistoryAsyncTask(this, null).execute(new Void[0]);
        this.searchAdapter.setClickListener(this);
        if (this.searchRecycler != null) {
            this.searchRecycler.setLayoutManager(layoutManager);
        }
    }

    private void setHintText() {
        if (this.searchType == SEARCH_TYPES.SEARCH.getCode()) {
            this.editText.setHint(R.string.search_hint);
        } else if (this.searchType == SEARCH_TYPES.WORK.getCode()) {
            this.editText.setHint(R.string.search_hint_work);
        } else if (this.searchType == SEARCH_TYPES.HOME.getCode()) {
            this.editText.setHint(R.string.search_hint_home);
        } else if (this.searchType == SEARCH_TYPES.FAVORITE.getCode()) {
            this.editText.setHint(R.string.search_hint_favorite);
        }
    }

    private synchronized void buildAndConnectGoogleApiClient() {
        this.mGoogleApiClient = new Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(Places.GEO_DATA_API).addApi(Places.PLACE_DETECTION_API).addApi(LocationServices.API).build();
        this.mGoogleApiClient.connect();
    }

    private void getLatLngBounds() {
        this.logger.v("getLatLngBounds");
        if (this.lastLocation == null) {
            Coordinate coordinate = this.navdyLocationManager.getSmartStartCoordinates();
            if (coordinate != null) {
                this.lastLocation = new Location("");
                this.lastLocation.setLatitude(coordinate.latitude.doubleValue());
                this.lastLocation.setLongitude(coordinate.longitude.doubleValue());
            }
        }
        if (this.lastLocation != null) {
            LatLng latLng = new LatLng(this.lastLocation.getLatitude(), this.lastLocation.getLongitude());
            LatLng northEast = SphericalUtil.computeOffset(SphericalUtil.computeOffset(latLng, 1609.0d, 0.0d), 1609.0d, 90.0d);
            LatLng southWest = SphericalUtil.computeOffset(SphericalUtil.computeOffset(latLng, 1609.0d, 180.0d), 1609.0d, 270.0d);
            LatLngBounds latLngBounds = new LatLngBounds(new LatLng(southWest.latitude, southWest.longitude), new LatLng(northEast.latitude, northEast.longitude));
        }
    }

    private void clearAndApplyListOrDropDownLook() {
        clear();
        applyListOrDropDownLook();
    }

    private void applyListOrDropDownLook() {
        boolean isAutoComplete;
        LayoutParams layoutParams;
        int height = -1;
        int marginTop = 0;
        int marginLeft = 0;
        int marginRight = 0;
        int marginBottom = 0;
        if (this.searchAdapter == null || this.searchAdapter.currentMode != Mode.AUTO_COMPLETE) {
            isAutoComplete = false;
        } else {
            isAutoComplete = true;
        }
        if (isAutoComplete) {
            height = -2;
            marginTop = AUTO_COMPLETE_MARGIN_TOP;
            marginLeft = AUTO_COMPLETE_MARGIN_LEFT;
            marginRight = AUTO_COMPLETE_MARGIN_RIGHT;
            marginBottom = AUTO_COMPLETE_MARGIN_BOTTOM;
        }
        boolean canReachInternet = AppInstance.getInstance().canReachInternet();
        if (this.searchRecycler != null) {
            layoutParams = new LayoutParams(-1, height);
            if (canReachInternet) {
                layoutParams.setMargins(marginLeft, marginTop, marginRight, marginBottom);
            } else {
                layoutParams.setMargins(marginLeft, 0, marginRight, marginBottom);
            }
            layoutParams.addRule(3, R.id.offline_banner);
            this.searchRecycler.setLayoutParams(layoutParams);
            if (isAutoComplete) {
                this.searchRecycler.setBackgroundResource(R.drawable.autocomplete_bg);
            } else {
                this.searchRecycler.setBackgroundResource(R.color.white);
            }
            this.searchRecycler.scrollToPosition(0);
        }
        if (this.offlineBanner != null) {
            layoutParams = new LayoutParams(-1, -2);
            layoutParams.setMargins(marginLeft, marginTop, marginRight, 0);
            layoutParams.addRule(3, R.id.search_toolbar);
            this.offlineBanner.setLayoutParams(layoutParams);
        }
        updatePoweredByGoogleHeaderVisibility(canReachInternet);
    }

    private void runAutoCompleteSearch(final String constraint) {
        if (constraint != null) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    final List<ContactModel> contactList = ContactsManager.getInstance().getContactsAndLoadPhotos(constraint, EnumSet.of(Attributes.ADDRESS));
                    new GooglePlacesSearch(new GoogleSearchListener() {
                        public void onGoogleSearchResult(final List<GoogleTextSearchDestinationResult> autocompleteResults, Query queryType, Error error) {
                            SearchActivity.this.logger.v("runGoogleQueryAutoComplete");
                            new AsyncTask<String, Void, Pair<ArrayList<Destination>, ArrayList<String>>>() {
                                protected Pair<ArrayList<Destination>, ArrayList<String>> doInBackground(String... params) {
                                    return new Pair(NavdyContentProvider.getDestinationsFromSearchQuery(constraint), NavdyContentProvider.getSearchHistoryFromSearchQuery(constraint));
                                }

                                protected void onPostExecute(Pair<ArrayList<Destination>, ArrayList<String>> pair) {
                                    SetDestinationTracker.getInstance().setDestinationType(TYPE_VALUES.SEARCH_AUTOCOMPLETE);
                                    ArrayList<Destination> knownDestinations = pair.first;
                                    ArrayList<String> previousSearches = pair.second;
                                    if (StringUtils.equalsOrBothEmptyAfterTrim(constraint, SearchActivity.this.editText.getText().toString())) {
                                        SearchActivity.this.clearAndApplyListOrDropDownLook();
                                        SearchActivity.this.searchAdapter.setLastQuery(constraint);
                                        SearchActivity.this.searchAdapter.updateRecyclerViewWithContactsForAutoComplete(contactList);
                                        SearchActivity.this.searchAdapter.addNoResultsItemIfEmpty(contactList, knownDestinations, autocompleteResults, previousSearches);
                                        SearchActivity.this.searchAdapter.updateRecyclerViewWithDatabaseData(knownDestinations, constraint);
                                        SearchActivity.this.searchAdapter.updateRecyclerViewWithAutocomplete(autocompleteResults, previousSearches, constraint);
                                        SearchActivity.this.showAutoComplete();
                                    }
                                }
                            }.execute(new String[]{constraint});
                        }
                    }, SearchActivity.this.handler).runQueryAutoCompleteWebApi(constraint);
                }
            }, 1);
        }
    }

    private void runOfflineAutocompleteSearch(final String constraint) {
        if (constraint != null) {
            new AsyncTask<Void, Void, Pair<List<ContactModel>, ArrayList<Destination>>>() {
                protected Pair<List<ContactModel>, ArrayList<Destination>> doInBackground(Void... unusedParams) {
                    SearchActivity.this.logger.v("runOfflineAutocompleteSearch doInBackground");
                    return new Pair(ContactsManager.getInstance().getContactsAndLoadPhotos(constraint, EnumSet.of(Attributes.ADDRESS)), NavdyContentProvider.getDestinationsFromSearchQuery(constraint));
                }

                protected void onPostExecute(Pair<List<ContactModel>, ArrayList<Destination>> pair) {
                    SearchActivity.this.logger.v("runOfflineAutocompleteSearch onPostExecute");
                    List<ContactModel> contacts = pair.first;
                    ArrayList<Destination> knownDestinations = pair.second;
                    String query = SearchActivity.this.editText.getText().toString();
                    if (StringUtils.equalsOrBothEmptyAfterTrim(constraint, query)) {
                        SearchActivity.this.clearAndApplyListOrDropDownLook();
                        SearchActivity.this.searchAdapter.setLastQuery(query);
                        SearchActivity.this.searchAdapter.updateRecyclerViewWithContactsForTextSearch(contacts);
                        SearchActivity.this.searchAdapter.updateRecyclerViewWithDatabaseData(knownDestinations, SearchActivity.this.editText.getText().toString());
                        SearchActivity.this.searchAdapter.addNoResultsItemIfEmpty(contacts, knownDestinations);
                        if (AppInstance.getInstance().isDeviceConnected()) {
                            SearchActivity.this.searchAdapter.createSearchForMoreFooter(SearchActivity.this.editText.getText().toString());
                        }
                        SearchActivity.this.showAutoComplete();
                    }
                }
            }.execute(new Void[0]);
        }
    }

    private void setEditTextListener() {
        try {
            this.editText = (EditText) findViewById(R.id.search_edit_text);
            this.editText.addTextChangedListener(new TextWatcher() {
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                public void afterTextChanged(Editable s) {
                    SearchActivity.this.handler.removeCallbacksAndMessages(null);
                    final String string = s.toString();
                    boolean textIsEmpty = StringUtils.isEmptyAfterTrim(string);
                    if (textIsEmpty) {
                        SearchActivity.this.setCurrentMode(Mode.EMPTY);
                    } else {
                        SearchActivity.this.setCurrentMode(Mode.AUTO_COMPLETE);
                    }
                    SearchActivity.this.clearAndApplyListOrDropDownLook();
                    if (textIsEmpty) {
                        SearchActivity.this.rightButtonIsMic = true;
                        SearchActivity.this.updateRightButton();
                        SearchActivity.this.showList();
                        SearchActivity.this.fab.setVisibility(GONE);
                        new ServiceAndHistoryAsyncTask(SearchActivity.this, null).execute(new Void[0]);
                        return;
                    }
                    SearchActivity.this.rightButtonIsMic = false;
                    SearchActivity.this.updateRightButton();
                    if (SearchActivity.this.ignoreNextKeystrokeKey.get()) {
                        SearchActivity.this.ignoreNextKeystrokeKey.set(false);
                        return;
                    }
                    SystemUtils.ensureOnMainThread();
                    int keyListenerDelay = 500;
                    if (SearchActivity.this.ignoreAutoCompleteDelay.get()) {
                        SearchActivity.this.ignoreAutoCompleteDelay.set(false);
                        keyListenerDelay = 0;
                    }
                    SearchActivity.this.handler.postDelayed(new Runnable() {
                        public void run() {
                            if (com.navdy.service.library.util.SystemUtils.isConnectedToNetwork(NavdyApplication.getAppContext())) {
                                SearchActivity.this.runAutoCompleteSearch(string);
                            } else {
                                SearchActivity.this.runOfflineAutocompleteSearch(string);
                            }
                        }
                    }, (long) keyListenerDelay);
                }
            });
            this.editText.setOnKeyListener(new OnKeyListener() {
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == 66 && event.getAction() == 0) {
                        SearchActivity.this.runTextSearchOrAskHud(SearchActivity.this.editText.getText().toString());
                    }
                    return false;
                }
            });
        } catch (Throwable e) {
            this.logger.e(e);
        }
    }

    private boolean runTextSearchOrAskHud(final String query) {
        SystemUtils.dismissKeyboard(this);
        if (StringUtils.isEmptyAfterTrim(query)) {
            return true;
        }
        setCurrentMode(Mode.FULL_SEARCH);
        showProgressDialog();
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                NavdyContentProvider.addToSearchHistory(query);
                return null;
            }
        }.execute(new Void[0]);
        new NavdySearch(new SearchCallback() {
            public void onSearchStarted() {
            }

            public void onSearchCompleted(SearchResults searchResults) {
                SearchActivity.this.putSearchResultExtraAndFinishIfNoUiRequired(searchResults);
                SearchActivity.this.addDistancesToDestinationsIfNeeded(searchResults);
                SearchActivity.this.updateRecyclerViewWithSearchResults(searchResults, true);
                SearchActivity.this.hideProgressDialog();
            }
        }, true).runSearch(query);
        return false;
    }

    private void putSearchResultExtraAndFinishIfNoUiRequired(SearchResults searchResults) {
        if (this.isComingFromGoogleNow || this.fromActionSendIntent) {
            Destination firstDestination = searchResults.getFirstDestination();
            if (firstDestination != null) {
                putSearchResultExtraAndFinish(firstDestination);
            } else {
                this.logger.e("Unable to get search results for Google now or hud voice search query!");
            }
        }
    }

    private void updateRecyclerViewWithSearchResults(final SearchResults searchResults, final boolean showNoResultsFoundDialog) {
        runOnUiThread(new Runnable() {
            public void run() {
                boolean showMapFab = true;
                SearchActivity.this.clearAndApplyListOrDropDownLook();
                SearchActivity.this.searchAdapter.updateRecyclerViewWithContactsForTextSearch(searchResults.getContacts());
                List<GoogleTextSearchDestinationResult> googleResults = searchResults.getGoogleResults();
                SearchActivity.this.searchAdapter.updateRecyclerViewWithTextSearchResults(googleResults);
                List<PlacesSearchResult> hudDestinations = searchResults.getHudSearchResults();
                SearchActivity.this.searchAdapter.updateRecyclerViewWithPlaceSearchResults(hudDestinations);
                ArrayList<Destination> destinationsFromDatabase = searchResults.getDestinationsFromDatabase();
                SearchActivity.this.searchAdapter.updateRecyclerViewWithDatabaseData(destinationsFromDatabase, searchResults.getQuery());
                if (showNoResultsFoundDialog) {
                    SearchActivity.this.showNoResultsFoundDialogIfAllAreEmpty(contacts, googleResults, hudDestinations, destinationsFromDatabase);
                }
                SearchActivity.this.hideProgressDialog();
                if ((googleResults == null || googleResults.isEmpty()) && ((hudDestinations == null || hudDestinations.isEmpty()) && (destinationsFromDatabase == null || destinationsFromDatabase.isEmpty()))) {
                    showMapFab = false;
                }
                SearchActivity.this.showList(showMapFab);
            }
        });
    }

    private void addDistancesToDestinationsIfNeeded(SearchResults searchResults) {
        final List<GoogleTextSearchDestinationResult> googleResults = searchResults.getGoogleResults();
        boolean weHaveGoogleResults = (googleResults == null || googleResults.isEmpty()) ? false : true;
        if (weHaveGoogleResults && !this.isComingFromGoogleNow) {
            TaskManager.getInstance().execute(new Runnable() {
                public void run() {
                    SearchActivity.this.addDistancesToDestinations(googleResults);
                }
            }, 1, TaskPriority.LOW);
        }
    }

    private void runServiceSearch(ServiceTypes serviceType) {
        if (serviceType != null) {
            setCurrentMode(Mode.FULL_SEARCH);
            if (AppInstance.getInstance().canReachInternet()) {
                this.rightButtonIsMic = false;
                updateRightButton();
                NavdySearch.runServiceSearch(this.googlePlacesSearch, serviceType);
            } else if (AppInstance.getInstance().isDeviceConnected()) {
                new NavdySearch(new SearchCallback() {
                    public void onSearchStarted() {
                    }

                    public void onSearchCompleted(SearchResults searchResults) {
                        SearchActivity.this.putSearchResultExtraAndFinishIfNoUiRequired(searchResults);
                        SearchActivity.this.updateRecyclerViewWithSearchResults(searchResults, true);
                    }
                }, true).runSearch(serviceType.getHudServiceType());
            } else {
                hideProgressDialog();
            }
        }
    }

    private void setCurrentMode(Mode newMode) {
        if (this.searchAdapter != null) {
            this.logger.d("Setting current mode to " + newMode.name());
            this.searchAdapter.currentMode = newMode;
            return;
        }
        this.logger.w("Unable to current mode to " + newMode.name() + " because the searchAdapter is null");
    }

    public void onGoogleSearchResult(@Nullable List<GoogleTextSearchDestinationResult> destinations, Query queryType, Error error) {
        this.logger.v("onGoogleSearchResult: " + queryType);
        if (destinations == null) {
            destinations = new ArrayList(0);
        }
        SetDestinationTracker setDestinationTracker = SetDestinationTracker.getInstance();
        if (queryType == Query.TEXTSEARCH) {
            setDestinationTracker.setDestinationType(TYPE_VALUES.SEARCH_RESULT);
        }
        switch (queryType) {
            case NEARBY:
            case TEXTSEARCH:
                if (!(destinations.isEmpty() || this.isComingFromGoogleNow)) {
                    final List<GoogleTextSearchDestinationResult> finalDestinations = destinations;
                    TaskManager.getInstance().execute(new Runnable() {
                        public void run() {
                            SearchActivity.this.addDistancesToDestinations(finalDestinations);
                        }
                    }, 1, TaskPriority.LOW);
                }
                updateRecyclerViewForServiceSearch(destinations);
                this.searchAdapter.updateNearbySearchResultsWithKnownDestinationData();
                hideProgressDialog();
                return;
            case DETAILS:
                this.logger.v("successfully received detail callback from web service: " + destinations);
                Destination destination = new Destination();
                if (destinations.isEmpty()) {
                    destination = this.requestedPlaceDetailsDestinationBackup;
                } else {
                    GoogleTextSearchDestinationResult result = (GoogleTextSearchDestinationResult) destinations.get(0);
                    if (result != null) {
                        result.toModelDestinationObject(SearchType.DETAILS, destination);
                        destination.persistPlaceDetailInfo();
                        destination.updateDestinationListsAsync();
                    }
                }
                if (destination != null) {
                    putSearchResultExtraAndFinish(destination);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void updateRecyclerViewForServiceSearch(final List<GoogleTextSearchDestinationResult> destinations) {
        runOnUiThread(new Runnable() {
            public void run() {
                SearchActivity.this.clearAndApplyListOrDropDownLook();
                SearchActivity.this.searchAdapter.updateRecyclerViewWithTextSearchResults(destinations);
                if (destinations == null || destinations.size() <= 0) {
                    SearchActivity.this.searchAdapter.addNoResultsItem();
                    SearchActivity.this.showNoResultsFoundDialog();
                }
                SearchActivity.this.showList();
            }
        });
    }

    @WorkerThread
    private void addDistancesToDestinations(List<GoogleTextSearchDestinationResult> destinations) {
        SystemUtils.ensureNotOnMainThread();
        this.logger.v("addDistancesToDestinations");
        if (destinations != null && destinations.size() > 0) {
            for (GoogleTextSearchDestinationResult destination : destinations) {
                try {
                    double destinationLat = Double.parseDouble(destination.lat);
                    double destinationLng = Double.parseDouble(destination.lng);
                    Location destinationLocation = new Location("");
                    destinationLocation.setLatitude(destinationLat);
                    destinationLocation.setLongitude(destinationLng);
                    if (this.lastLocation != null) {
                        destination.distance = (double) this.lastLocation.distanceTo(destinationLocation);
                    }
                } catch (Throwable e) {
                    this.logger.e(e);
                    throw e;
                }
            }
            this.logger.v("status of destinations: " + destinations);
        }
    }

    private void requestLocationUpdate() {
        createLocationRequest();
        this.logger.i("calling requestLocationUpdates from FusedLocationApi");
        Context context = getApplicationContext();
        if (ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 && ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mGoogleApiClient, this.mLocationRequest, (LocationListener) this);
        }
        this.requestedLocationUpdates = true;
    }

    private void createLocationRequest() {
        this.logger.i("creating LocationRequest Object");
        this.mLocationRequest = LocationRequest.create().setPriority(100).setInterval(10000).setFastestInterval(1000);
    }

    private void stopLocationUpdates() {
        if (this.requestedLocationUpdates) {
            LocationServices.FusedLocationApi.removeLocationUpdates(this.mGoogleApiClient, (LocationListener) this);
            this.requestedLocationUpdates = false;
        }
    }

    public void onLocationChanged(Location location) {
        if (this.lastLocation == null) {
            this.logger.v("Location changed");
            this.lastLocation = location;
            stopLocationUpdates();
        }
    }

    public void onConnected(Bundle bundle) {
        this.logger.i("GoogleApiClient connected.");
        Context context = getApplicationContext();
        if (ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_FINE_LOCATION") == 0 && ContextCompat.checkSelfPermission(context, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            this.lastLocation = LocationServices.FusedLocationApi.getLastLocation(this.mGoogleApiClient);
        }
        if (this.lastLocation == null) {
            this.logger.e("lastLocation was null. Now requesting location update.");
            requestLocationUpdate();
        }
    }

    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        this.logger.e("onConnectionFailed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
    }

    public void onConnectionSuspended(int i) {
        this.logger.e("GoogleApiClient connection suspended.");
    }

    public void onClick(View v) {
        SetDestinationTracker setDestinationTracker = SetDestinationTracker.getInstance();
        int position = ((Integer) v.getTag()).intValue();
        this.logger.v("item clicked at position: " + position);
        final Destination d = this.searchAdapter != null ? this.searchAdapter.getDestinationAt(position) : null;
        if (d == null) {
            this.logger.e("Unable to retrieve a destination from position: " + position);
        } else if (d.searchResultType == SearchType.NO_RESULTS.getValue()) {
        } else {
            if (d.searchResultType == SearchType.SEARCH_HISTORY.getValue()) {
                if (v.getId() == R.id.nav_button) {
                    new AsyncTask<Integer, Void, Integer>() {
                        protected void onPreExecute() {
                            super.onPreExecute();
                            SearchActivity.this.showProgressDialog();
                            SearchActivity.this.clearAndApplyListOrDropDownLook();
                        }

                        protected Integer doInBackground(Integer... ids) {
                            if (ids == null || ids.length != 1) {
                                return Integer.valueOf(-1);
                            }
                            int id = NavdyContentProvider.removeFromSearchHistory(ids[0].intValue());
                            SearchActivity.this.searchAdapter.setUpServicesAndHistory(SearchActivity.this.showServices);
                            return Integer.valueOf(id);
                        }

                        protected void onPostExecute(Integer nbRows) {
                            SearchActivity.this.searchAdapter.notifyDataSetChanged();
                            SearchActivity.this.hideProgressDialog();
                            if (nbRows.intValue() < 1) {
                                BaseActivity.showLongToast(R.string.unable_to_remove_search_history, new Object[0]);
                            }
                        }
                    }.execute(new Integer[]{Integer.valueOf(d.id)});
                    return;
                }
                this.ignoreAutoCompleteDelay.set(true);
                this.editText.setText(d.name);
                this.editText.setSelection(d.name.length());
                this.searchRecycler.requestFocus();
                new AsyncTask<Void, Void, Void>() {
                    protected Void doInBackground(Void... params) {
                        NavdyContentProvider.addToSearchHistory(d.name);
                        return null;
                    }
                }.execute(new Void[0]);
            } else if (d.searchResultType == SearchType.AUTOCOMPLETE.getValue() && StringUtils.isEmptyAfterTrim(d.placeId)) {
                setCurrentMode(Mode.FULL_SEARCH);
                this.ignoreNextKeystrokeKey.set(true);
                this.editText.setText(d.name);
                this.editText.setSelection(d.name.length());
                SystemUtils.dismissKeyboard(this);
                runTextSearchOrAskHud(d.name);
            } else if (d.searchResultType == SearchType.MORE.getValue()) {
                runTextSearchOrAskHud(this.editText.getText().toString());
            } else if (d.searchResultType == SearchType.CONTACT_ADDRESS_SELECTION.getValue()) {
                if (this.searchAdapter != null) {
                    SearchItem si = this.searchAdapter.getItemAt(position);
                    if (si != null && si.contact != null) {
                        setCurrentMode(Mode.FULL_SEARCH);
                        clearAndApplyListOrDropDownLook();
                        this.searchAdapter.updateRecyclerViewWithContact(si.contact);
                    }
                }
            } else if (v.getId() != R.id.nav_button && !isFavoriteSearch(this.searchType)) {
                Intent i = new Intent(this, DetailsActivity.class);
                i.putExtra(SearchConstants.SEARCH_RESULT, d);
                i.putExtra(EXTRA_LOCATION, this.lastLocation);
                startActivityForResult(i, 0);
            } else if (!AppInstance.getInstance().canReachInternet() && !hasDisplayedOfflineMessage && !isFavoriteSearch(this.searchType)) {
                createOfflineDialogForOnClick(v).show();
            } else if (d.searchResultType == SearchType.AUTOCOMPLETE.getValue()) {
                setDestinationTracker.setDestinationType(TYPE_VALUES.SEARCH_AUTOCOMPLETE);
                setResultForAutocompleteAndFinish(d);
            } else if (d.searchResultType == SearchType.CONTACT.getValue()) {
                setDestinationTracker.setDestinationType(TYPE_VALUES.SEARCH_CONTACT);
                setResultForContactAndFinish(d);
            } else {
                putSearchResultExtraAndFinish(d);
            }
        }
    }

    public void onServiceClick(View v) {
        this.logger.v("User clicked on a service search.");
        showProgressDialog();
        this.ignoreNextKeystrokeKey.set(true);
        SetDestinationTracker.getInstance().setDestinationType(TYPE_VALUES.SEARCH_QUICK);
        switch (v.getId()) {
            case R.id.service_gas_icon /*2131755880*/:
                this.editText.setText(R.string.gas);
                runServiceSearch(ServiceTypes.GAS);
                break;
            case R.id.service_food_icon /*2131755881*/:
                this.editText.setText(R.string.food);
                runServiceSearch(ServiceTypes.FOOD);
                break;
            case R.id.service_atm_icon /*2131755882*/:
                this.editText.setText(R.string.atm);
                runServiceSearch(ServiceTypes.ATM);
                break;
            case R.id.service_parking_icon /*2131755883*/:
                this.editText.setText(R.string.parking);
                runServiceSearch(ServiceTypes.PARKING);
                break;
            default:
                this.logger.e("Unknown service view in onServiceClick: " + v.getId());
                break;
        }
        this.editText.setSelection(this.editText.getText().length());
        SystemUtils.dismissKeyboard(this);
    }

    static boolean isFavoriteSearch(int searchType) {
        return searchType == 1 || searchType == 2 || searchType == 3;
    }

    private void setResultForAutocompleteAndFinish(Destination d) {
        showProgressDialog();
        this.requestedPlaceDetailsDestinationBackup = d;
        this.logger.d("Getting place details for: " + d);
        this.googlePlacesSearch.runDetailsSearchWebApi(d.placeId);
    }

    private Dialog createOfflineDialogForOnClick(final View searchRow) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        String title = getString(R.string.offline_mode);
        if (!StringUtils.isEmptyAfterTrim(title)) {
            builder.setTitle(title);
        }
        String message = getString(R.string.offline_mode_explanation);
        if (!StringUtils.isEmptyAfterTrim(message)) {
            builder.setMessage(message);
        }
        builder.setPositiveButton(getString(R.string.send_to_navdy), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                SearchActivity.hasDisplayedOfflineMessage = true;
                SearchActivity.this.onClick(searchRow);
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), null);
        return builder.create();
    }

    private void setResultForContactAndFinish(final Destination d) {
        showProgressDialog();
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                if (d != null) {
                    NavdyContentProvider.addToSearchHistory(d.name);
                }
                return null;
            }
        }.execute(new Void[0]);
        TaskManager.getInstance().execute(new Runnable() {
            public void run() {
                SearchActivity.this.logger.v("setResultForContactAndFinish: " + d.rawAddressNotForDisplay);
                NavCoordsAddressProcessor.processDestination(d, new OnCompleteCallback() {
                    public void onSuccess(Destination destination) {
                        SearchActivity.this.putSearchResultExtraAndFinish(destination);
                    }

                    public void onFailure(Destination destination, NavCoordsAddressProcessor.Error error) {
                        SearchActivity.this.logger.v("navigationHelper failed to get latLng for ContactModel Destination");
                        SearchActivity.this.putSearchResultExtraAndFinish(destination);
                    }
                });
            }
        }, 3);
    }

    private void putSearchResultExtraAndFinish(final Destination destination) {
        showProgressDialog();
        new AsyncTask<Void, Void, Destination>() {
            protected Destination doInBackground(Void... voids) {
                SearchActivity.this.logger.v("Destination address: " + destination);
                if (destination == null) {
                    return null;
                }
                NavdyContentProvider.addToSearchHistory(destination.name);
                destination.reloadSelfFromDatabase();
                return destination;
            }

            protected void onPostExecute(Destination d) {
                Intent result = new Intent();
                result.putExtra(SearchConstants.SEARCH_RESULT, d);
                if (SearchActivity.this.isShowingMap) {
                    SetDestinationTracker.getInstance().setDestinationType(TYPE_VALUES.SEARCH_MAP);
                }
                if (SearchActivity.this.isComingFromGoogleNow) {
                    android.util.Pair<String, String> pair = d.getTitleAndSubtitle();
                    if (!(pair == null || StringUtils.isEmptyAfterTrim((CharSequence) pair.first))) {
                        SearchActivity.this.mAudioRouter.processTTSRequest(SearchActivity.this.getString(R.string.ok_navigating_to_x, new Object[]{pair.first}), null, false);
                    }
                    result.putExtra(SearchConstants.EXTRA_IS_VOICE_SEARCH, true);
                }
                SearchActivity.this.setResult(-1, result);
                SearchActivity.this.hideProgressDialog();
                SearchActivity.this.finish();
            }
        }.execute(new Void[0]);
    }

    private void showNoResultsFoundDialogIfAllAreEmpty(List... lists) {
        if (lists == null || lists.length <= 0) {
            showNoResultsFoundDialog();
            return;
        }
        int length = lists.length;
        int i = 0;
        while (i < length) {
            List list = lists[i];
            if (list == null || list.isEmpty()) {
                i++;
            } else {
                return;
            }
        }
        showNoResultsFoundDialog();
    }

    private void showNoResultsFoundDialog() {
        if (!isActivityDestroyed()) {
            if (this.searchAdapter != null) {
                clear();
                setCurrentMode(Mode.AUTO_COMPLETE);
                this.searchAdapter.addSearchPoweredByGoogleHeader();
                this.searchAdapter.addNoResultsItem();
            }
            String title = getString(R.string.no_results_found);
            if (this.isComingFromGoogleNow) {
                this.mAudioRouter.processTTSRequest(title, null, false);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            if (!StringUtils.isEmptyAfterTrim(title)) {
                builder.setTitle(title);
            }
            builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    SearchActivity.this.editText.requestFocus();
                    ((InputMethodManager) SearchActivity.this.getSystemService("input_method")).toggleSoftInput(2, 0);
                }
            });
            builder.create().show();
        }
    }

    private void clear() {
        if (this.searchAdapter != null) {
            this.searchAdapter.clear();
        }
        if (this.searchRecycler != null) {
            this.searchRecycler.getRecycledViewPool().clear();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 1) {
            finish();
        } else if (requestCode == 42 && resultCode == -1) {
            ArrayList<String> matches = data.getStringArrayListExtra("android.speech.extra.RESULTS");
            if (matches == null || StringUtils.isEmptyAfterTrim((CharSequence) matches.get(0))) {
                BaseActivity.showLongToast(R.string.unable_to_hear_any_search, new Object[0]);
                return;
            }
            String query = (String) matches.get(0);
            setEditTextWithoutTriggeringAutocomplete(query);
            runTextSearchOrAskHud(query);
        }
    }

    private void setEditTextWithoutTriggeringAutocomplete(String query) {
        this.editText.setText(query);
        this.editText.setSelection(query.length());
        this.handler.removeCallbacksAndMessages(null);
        this.rightButtonIsMic = false;
        updateRightButton();
    }

    public static void startSearchActivityFor(SEARCH_TYPES searchType, boolean searchMic, Activity activity) {
        Intent i = new Intent(activity.getApplicationContext(), SearchActivity.class);
        i.putExtra(SearchConstants.SEARCH_TYPE_EXTRA, searchType.getCode());
        i.putExtra(EXTRA_SPEECH_RECOGNITION, searchMic);
        activity.startActivityForResult(i, searchType.getCode());
    }

    public void onFabClick(View view) {
        if (this.isShowingMap) {
            showList();
        } else {
            showMap();
        }
    }

    private void showMap() {
        this.isShowingMap = true;
        unselectCurrentMarker();
        SystemUtils.dismissKeyboard(this);
        this.previousSelectedMarker = null;
        BaseSupportFragment.showThisFragment(this.googleMapFragment, this);
        this.centerMap.setVisibility(VISIBLE);
        this.fab.setVisibility(VISIBLE);
        this.fab.setImageResource(R.drawable.icon_fab_search_list);
        this.searchRecycler.setVisibility(GONE);
        this.searchAdapter.centerMapOnMarkers(false);
        if (this.googleMapFragment != null) {
            this.googleMapFragment.getMapAsync(new OnMapReadyCallback() {
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {
                        public boolean onMarkerClick(Marker marker) {
                            if (marker != SearchActivity.this.previousSelectedMarker) {
                                SearchActivity.this.unselectCurrentMarker();
                                SearchActivity.this.selectThisMarker(marker);
                            }
                            return false;
                        }
                    });
                    googleMap.setOnMapClickListener(new OnMapClickListener() {
                        public void onMapClick(LatLng latLng) {
                            SearchActivity.this.unselectCurrentMarker();
                        }
                    });
                }
            });
        }
        showInstructionalCard();
    }

    private void unselectCurrentMarker() {
        updatePin(this.previousSelectedMarker, false);
        this.previousSelectedMarker = null;
        showInstructionalCard();
    }

    private void selectThisMarker(@Nullable Marker marker) {
        Integer destinationIndex = updatePin(marker, true);
        this.previousSelectedMarker = marker;
        showCardFor(destinationIndex);
    }

    @Nullable
    private Integer updatePin(@Nullable Marker marker, boolean isSelected) {
        if (marker == null) {
            return null;
        }
        float f;
        Integer destinationIndex = null;
        if (this.searchAdapter != null) {
            destinationIndex = this.searchAdapter.getDestinationIndexForMarker(marker.getId());
            if (destinationIndex == null) {
                return null;
            }
            Destination destination = this.searchAdapter.getDestinationAt(destinationIndex.intValue());
            if (destination != null) {
                int pinAsset;
                if (isSelected) {
                    pinAsset = destination.getPinAsset();
                } else {
                    pinAsset = destination.getUnselectedPinAsset();
                }
                MapUtils.setMarkerImageResource(marker, pinAsset);
            }
        }
        if (isSelected) {
            f = 1.0f;
        } else {
            f = 0.0f;
        }
        marker.setZIndex(f);
        return destinationIndex;
    }

    private void showAutoComplete() {
        hideProgressDialog();
        showList();
        this.fab.setVisibility(GONE);
    }

    private void showList() {
        showList(true);
    }

    private void showList(boolean showMapFab) {
        int i = 0;
        this.isShowingMap = false;
        this.searchRecycler.setVisibility(VISIBLE);
        this.fab.setImageResource(R.drawable.icon_fab_search_map);
        ImageButton imageButton = this.fab;
        if (!showMapFab) {
            i = 8;
        }
        imageButton.setVisibility(i);
        this.editText.requestFocus();
        BaseSupportFragment.hideThisFragment(this.googleMapFragment, this);
        this.centerMap.setVisibility(GONE);
        hideCard();
    }

    public void centerOnDriver(View view) {
        this.searchAdapter.centerMapOnMarkers(true);
    }

    private void showCardFor(@Nullable Integer destinationIndex) {
        if (destinationIndex != null && destinationIndex.intValue() >= 0) {
            this.card.setVisibility(VISIBLE);
            this.searchAdapter.setCardData(new SearchViewHolder(this.card), destinationIndex.intValue());
        }
    }

    private void hideCard() {
        this.card.setVisibility(GONE);
    }

    private void showInstructionalCard() {
        DestinationImageView icon = (DestinationImageView) this.card.findViewById(R.id.search_row_image);
        View row = this.card.findViewById(R.id.search_row);
        View navBtn = this.card.findViewById(R.id.nav_button);
        TextView title = (TextView) this.card.findViewById(R.id.search_row_title);
        TextView details = (TextView) this.card.findViewById(R.id.search_row_details);
        View price = this.card.findViewById(R.id.search_row_price);
        View distance = this.card.findViewById(R.id.search_row_distance);
        this.card.setVisibility(VISIBLE);
        row.setOnClickListener(null);
        icon.setImageResource(R.drawable.icon_badge_pick);
        navBtn.setVisibility(GONE);
        title.setText(R.string.pick_a_result);
        details.setText(R.string.tap_a_pin);
        distance.setVisibility(GONE);
        price.setVisibility(GONE);
    }

    @Subscribe
    public void handleReachabilityStateChange(ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            this.logger.v("reachabilityEvent: " + reachabilityEvent.isReachable);
            if (this.editText != null && StringUtils.isEmptyAfterTrim(this.editText.getText())) {
                this.searchAdapter.notifyDataSetChanged();
            }
            updateOfflineBannerVisibility();
            applyListOrDropDownLook();
        }
    }

    public void updateOfflineBannerVisibility() {
        boolean canReachInternet = AppInstance.getInstance().canReachInternet();
        this.offlineBanner.setVisibility(canReachInternet ? 8 : 0);
        updatePoweredByGoogleHeaderVisibility(canReachInternet);
    }

    public void updatePoweredByGoogleHeaderVisibility(boolean canReachInternet) {
        if (this.searchAdapter == null) {
            return;
        }
        if (canReachInternet) {
            this.searchAdapter.addSearchPoweredByGoogleHeader();
        } else {
            this.searchAdapter.removeSearchPoweredByGoogleHeader();
        }
    }

    public void onRefreshConnectivityClick(View view) {
        AppInstance.getInstance().checkForNetwork();
    }
}
