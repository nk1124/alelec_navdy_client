package com.navdy.client.app.ui.search;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.location.NavdyLocationManager;
import com.navdy.client.app.framework.map.MapUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.Precision;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener;
import com.navdy.client.app.framework.servicehandler.NetworkStatusManager.ReachabilityEvent;
import com.navdy.client.app.tracking.SetDestinationTracker;
import com.navdy.client.app.ui.base.BaseGoogleMapFragment;
import com.navdy.client.app.ui.base.BaseGoogleMapFragment.OnShowMapListener;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.customviews.UnitSystemTextView;
import com.navdy.client.app.ui.details.DetailsActivity;
import com.navdy.client.app.ui.routing.RoutingActivity;
import com.navdy.service.library.events.location.Coordinate;
import com.squareup.otto.Subscribe;

public class DropPinActivity extends BaseToolbarActivity implements NavdyRouteListener {
    private static final int ADDRESS_LOOKUP_DELAY = 500;
    public static final int NB_CAMERA_CHANGE_B4_SHOW_ADDRESS = 0;
    private static final int SHOW_DETAILS_SCREEN = 42;
    private View cardRow;
    private Destination currentDestination = new Destination();
    private UnitSystemTextView distanceText;
    private ImageView dropPin;
    private TextView firstLine;
    private BaseGoogleMapFragment googleMapFragment;
    private ImageView illustration;
    private int mapHasMoved = 0;
    private ImageView navButton;
    private final NavdyRouteHandler navdyRouteHandler = NavdyRouteHandler.getInstance();
    private final OnClickListener onCardViewClick = new OnClickListener() {
        public void onClick(View v) {
            new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... params) {
                    DropPinActivity.this.currentDestination.handleNewCoordsAndAddress(DropPinActivity.this.currentDestination.displayLat, DropPinActivity.this.currentDestination.displayLong, DropPinActivity.this.currentDestination.navigationLat, DropPinActivity.this.currentDestination.navigationLong, null, Precision.PRECISE);
                    return null;
                }

                protected void onPostExecute(Void aVoid) {
                    Intent i = new Intent(DropPinActivity.this.getApplicationContext(), DetailsActivity.class);
                    i.putExtra(SearchConstants.SEARCH_RESULT, DropPinActivity.this.currentDestination);
                    DropPinActivity.this.startActivityForResult(i, 42);
                }
            }.execute(new Void[0]);
        }
    };
    private TextView secondLine;

    public void centerOnMyLocation(View view) {
        Coordinate coordinate = NavdyLocationManager.getInstance().getSmartStartCoordinates();
        if (coordinate != null && this.googleMapFragment != null) {
            this.googleMapFragment.moveMap(new LatLng(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue()), 14.0f, true);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 42 && resultCode == 1) {
            finish();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.drop_pin_activity);
        new ToolbarBuilder().title((int) R.string.pick_a_place).build();
        this.dropPin = (ImageView) findViewById(R.id.drop_pin);
        this.illustration = (ImageView) findViewById(R.id.illustration);
        this.firstLine = (TextView) findViewById(R.id.card_first_line);
        this.secondLine = (TextView) findViewById(R.id.card_second_line);
        this.navButton = (ImageView) findViewById(R.id.nav_button);
        this.cardRow = findViewById(R.id.card_row);
        this.distanceText = (UnitSystemTextView) findViewById(R.id.card_sub_illustration_text);
        TextView thirdLine = (TextView) findViewById(R.id.card_third_line);
        if (this.dropPin != null) {
            this.dropPin.setVisibility(GONE);
        }
        if (this.illustration != null) {
            this.illustration.setImageResource(R.drawable.icon_pin_plain_grey_sm);
        }
        if (this.firstLine != null) {
            this.firstLine.setText(R.string.pick_a_place);
        }
        if (this.secondLine != null) {
            this.secondLine.setText(R.string.drag_to_select_place);
        }
        if (thirdLine != null) {
            thirdLine.setVisibility(GONE);
        }
        if (this.navButton != null) {
            this.navButton.setVisibility(GONE);
            this.navButton.setImageResource(R.drawable.button_send_route);
            this.navButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    SetDestinationTracker.getInstance().tagSetDestinationEvent(DropPinActivity.this.currentDestination);
                    new AsyncTask<Void, Void, Void>() {
                        protected Void doInBackground(Void... params) {
                            DropPinActivity.this.currentDestination.handleNewCoordsAndAddress(DropPinActivity.this.currentDestination.displayLat, DropPinActivity.this.currentDestination.displayLong, DropPinActivity.this.currentDestination.navigationLat, DropPinActivity.this.currentDestination.navigationLong, null, Precision.PRECISE);
                            return null;
                        }

                        protected void onPostExecute(Void aVoid) {
                            DropPinActivity.this.navdyRouteHandler.requestNewRoute(DropPinActivity.this.currentDestination);
                        }
                    }.execute(new Void[0]);
                }
            });
        }
        if (this.cardRow != null) {
            this.cardRow.setOnClickListener(this.onCardViewClick);
        }
        this.googleMapFragment = (BaseGoogleMapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (this.googleMapFragment != null) {
            this.googleMapFragment.hide();
            Coordinate coordinate = NavdyLocationManager.getInstance().getSmartStartCoordinates();
            if (coordinate != null) {
                this.googleMapFragment.moveMap(new LatLng(coordinate.latitude.doubleValue(), coordinate.longitude.doubleValue()), 14.0f, false);
            }
        }
        SetDestinationTracker setDestinationTracker = SetDestinationTracker.getInstance();
        setDestinationTracker.setSourceValue("Drop_Pin");
        setDestinationTracker.setDestinationType("Drop_Pin");
    }

    protected void onResume() {
        super.onResume();
        updateOfflineBannerVisibility();
        if (this.googleMapFragment != null) {
            this.googleMapFragment.show(new OnShowMapListener() {
                public void onShow() {
                    DropPinActivity.this.setUpPinDrop();
                }
            });
        }
        this.navdyRouteHandler.addListener(this);
    }

    protected void onPause() {
        this.navdyRouteHandler.removeListener(this);
        super.onPause();
    }

    public void onPendingRouteCalculating(@NonNull Destination destination) {
        finish();
    }

    public void onPendingRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo pendingRoute) {
        finish();
    }

    public void onRouteCalculating(@NonNull Destination destination) {
        RoutingActivity.startRoutingActivity(this);
        finish();
    }

    public void onRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo route) {
        if (error == Error.NONE) {
            finish();
        }
    }

    public void onRouteStarted(@NonNull NavdyRouteInfo route) {
        finish();
    }

    public void onTripProgress(@NonNull NavdyRouteInfo progress) {
        finish();
    }

    public void onReroute() {
        finish();
    }

    public void onRouteArrived(@NonNull Destination destination) {
    }

    public void onStopRoute() {
    }

    private void setUpPinDrop() {
        View container = findViewById(R.id.map_touch_listener);
        if (container != null) {
            container.setOnTouchListener(new OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == 0) {
                        DropPinActivity.this.showDroppedPin();
                    }
                    return false;
                }
            });
        }
        if (this.googleMapFragment == null) {
            this.logger.e("Calling setUpPinDrop with a null googleMapFragment");
        } else {
            this.googleMapFragment.getMapAsync(new OnMapReadyCallback() {
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.setOnMapClickListener(new OnMapClickListener() {
                        public void onMapClick(LatLng latLng) {
                            if (latLng != null) {
                                DropPinActivity.this.googleMapFragment.moveMap(new LatLng(latLng.latitude, latLng.longitude), 14.0f, true);
                            }
                        }
                    });
                    googleMap.setOnCameraChangeListener(new OnCameraChangeListener() {
                        public void onCameraChange(final CameraPosition cameraPosition) {
                            DropPinActivity.this.handler.removeCallbacksAndMessages(null);
                            if (DropPinActivity.this.mapHasMoved < 0) {
                                DropPinActivity.this.mapHasMoved = DropPinActivity.this.mapHasMoved + 1;
                                return;
                            }
                            int delay = 500;
                            if (DropPinActivity.this.mapHasMoved == 0) {
                                DropPinActivity.this.mapHasMoved = DropPinActivity.this.mapHasMoved + 1;
                                delay = 0;
                                if (DropPinActivity.this.dropPin != null) {
                                    TranslateAnimation animation = new TranslateAnimation(0.0f, 0.0f, -50.0f, 0.0f);
                                    animation.setDuration(200);
                                    animation.setInterpolator(new DecelerateInterpolator());
                                    DropPinActivity.this.dropPin.startAnimation(animation);
                                    DropPinActivity.this.dropPin.setVisibility(VISIBLE);
                                }
                                if (DropPinActivity.this.illustration != null) {
                                    DropPinActivity.this.illustration.setImageResource(R.drawable.icon_badge_place);
                                }
                                if (DropPinActivity.this.navButton != null) {
                                    DropPinActivity.this.navButton.setVisibility(VISIBLE);
                                }
                            }
                            DropPinActivity.this.showUpdatingLocation();
                            DropPinActivity.this.handler.postDelayed(new Runnable() {
                                public void run() {
                                    final LatLng center = cameraPosition.target;
                                    Runnable reverseGeoCode = new Runnable() {
                                        public void run() {
                                            if (DropPinActivity.this.currentDestination.hasDetailedAddress()) {
                                                Pair<String, String> splitAddress = DropPinActivity.this.currentDestination.getSplitAddress();
                                                if (DropPinActivity.this.firstLine != null) {
                                                    DropPinActivity.this.firstLine.setText((CharSequence) splitAddress.first);
                                                }
                                                if (DropPinActivity.this.secondLine != null) {
                                                    DropPinActivity.this.secondLine.setText((CharSequence) splitAddress.second);
                                                }
                                            } else {
                                                if (DropPinActivity.this.firstLine != null) {
                                                    DropPinActivity.this.firstLine.setText(R.string.dropped_pin);
                                                }
                                                if (DropPinActivity.this.secondLine != null) {
                                                    DropPinActivity.this.secondLine.setText(DropPinActivity.this.getString(R.string.lat_long, new Object[]{String.valueOf(center.latitude), String.valueOf(center.longitude)}));
                                                }
                                            }
                                            Coordinate userLocation = NavdyLocationManager.getInstance().getSmartStartCoordinates();
                                            if (!(DropPinActivity.this.distanceText == null || userLocation == null)) {
                                                DropPinActivity.this.distanceText.setDistance((double) MapUtils.distanceBetween(userLocation, center));
                                                DropPinActivity.this.distanceText.setVisibility(VISIBLE);
                                            }
                                            DropPinActivity.this.navButton.setVisibility(VISIBLE);
                                            DropPinActivity.this.cardRow.setOnClickListener(DropPinActivity.this.onCardViewClick);
                                        }
                                    };
                                    DropPinActivity.this.currentDestination = new Destination();
                                    DropPinActivity.this.currentDestination.setCoordsToSame(center);
                                    MapUtils.doReverseGeocodingFor(center, DropPinActivity.this.currentDestination, reverseGeoCode);
                                }
                            }, (long) delay);
                        }
                    });
                }
            });
        }
    }

    private void showDroppedPin() {
        showThis(R.string.dropped_pin);
    }

    private void showUpdatingLocation() {
        showThis(R.string.updating_location);
    }

    private void showThis(@StringRes int stringRes) {
        this.firstLine.setText(stringRes);
        this.secondLine.setText("");
        this.navButton.setVisibility(GONE);
        this.cardRow.setOnClickListener(null);
        this.distanceText.setVisibility(INVISIBLE);
    }

    @Subscribe
    public void handleReachabilityStateChange(ReachabilityEvent reachabilityEvent) {
        if (reachabilityEvent != null) {
            updateOfflineBannerVisibility();
        }
    }

    private void updateOfflineBannerVisibility() {
        View offlineBanner = findViewById(R.id.offline_banner);
        if (offlineBanner != null) {
            offlineBanner.setVisibility(AppInstance.getInstance().canReachInternet() ? GONE : VISIBLE);
        }
    }

    public void onRefreshConnectivityClick(View view) {
        AppInstance.getInstance().checkForNetwork();
    }
}
