package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.navdy.client.app.framework.models.MountInfo;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.base.BaseActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class CheckMountActivity extends BaseActivity {
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemUI();
        final String box = SettingsUtils.getSharedPreferences().getString(SettingsConstants.BOX, "Old_Box");
        showProgressDialog();
        new AsyncTask<Void, Void, MountInfo>() {
            protected MountInfo doInBackground(Void... params) {
                return SettingsUtils.getMountInfo();
            }

            protected void onPostExecute(MountInfo mountInfo) {
                CheckMountActivity.this.hideProgressDialog();
                boolean noneOfTheMountsWillWork = mountInfo.noneOfTheMountsWillWork();
                Intent intent;
                if ((mountInfo.shortSupported || !TextUtils.equals(box, "New_Box")) && !noneOfTheMountsWillWork) {
                    intent = new Intent(CheckMountActivity.this.getApplicationContext(), InstallActivity.class);
                    if (StringUtils.equalsOrBothEmptyAfterTrim(box, "New_Box")) {
                        intent.putExtra("extra_mount", MountType.SHORT.getValue());
                    } else {
                        intent.putExtra("extra_mount", mountInfo.recommendedMount.getValue());
                    }
                    CheckMountActivity.this.startActivity(intent);
                } else {
                    intent = new Intent(CheckMountActivity.this.getApplicationContext(), MountPickerActivity.class);
                    if (noneOfTheMountsWillWork) {
                        intent.putExtra(MountPickerActivity.EXTRA_USE_CASE, 3);
                    }
                    CheckMountActivity.this.startActivity(intent);
                }
                CheckMountActivity.this.finish();
            }
        }.execute(new Void[0]);
    }
}
