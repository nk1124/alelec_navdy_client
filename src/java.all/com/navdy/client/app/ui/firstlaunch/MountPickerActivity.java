package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.models.MountInfo;
import com.navdy.client.app.framework.models.MountInfo.MountType;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Attributes.InstallAttributes;
import com.navdy.client.app.tracking.TrackerConstants.Event;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.settings.ContactUsActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;
import java.util.HashMap;

public class MountPickerActivity extends BaseToolbarActivity {
    public static final String EXTRA_USE_CASE = "extra_use_case";
    public static final int MEDIUM_MOUNT_RECOMMENDED = 0;
    public static final int MEDIUM_TALL_IS_TOO_HIGH = 2;
    public static final int NONE_OF_THE_MOUNTS_WILL_WORK_4_U = 3;
    public static final int SHORT_IS_TOO_LOW = 1;
    String box;
    MountInfo mountInfo = new MountInfo();
    private int usecase;

    protected void onCreate(Bundle savedInstanceState) {
        int imageRes;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_mount_picker);
        new ToolbarBuilder().title(SettingsUtils.getCarYearMakeModelString()).build();
        this.usecase = getIntent().getIntExtra(EXTRA_USE_CASE, 0);
        this.box = SettingsUtils.getSharedPreferences().getString(SettingsConstants.BOX, "Old_Box");
        final String modelString = SettingsUtils.getCustomerPreferences().getString(ProfilePreferences.CAR_MODEL, "");
        final TextView shortMountPill = (TextView) findViewById(R.id.short_mount_pill);
        final TextView mediumMountPill = (TextView) findViewById(R.id.medium_mount_pill);
        final TextView title = (TextView) findViewById(R.id.wrong_mount_title);
        final TextView desc = (TextView) findViewById(R.id.wrong_mount_description);
        if (title != null) {
            title.setText(getString(R.string.mount_picker_title, new Object[]{modelString}));
        }
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                MountPickerActivity.this.mountInfo = SettingsUtils.getMountInfo();
                return null;
            }

            protected void onPostExecute(Void aVoid) {
                MountPickerActivity.this.handleMountInfo(MountPickerActivity.this.box, modelString, shortMountPill, mediumMountPill, title, desc);
            }
        }.execute(new Void[0]);
        TextView shortMountDescription = (TextView) findViewById(R.id.short_mount_description);
        if (shortMountDescription != null) {
            shortMountDescription.setText(R.string.lowers_the_display);
        }
        if (StringUtils.equalsOrBothEmptyAfterTrim(this.box, "Old_Box")) {
            imageRes = R.drawable.image_2016_short_mount_sm;
        } else {
            imageRes = R.drawable.image_2017_short_mount_sm;
        }
        loadImage(R.id.short_mount_illustration, imageRes);
        loadImage(R.id.medium_mount_illustration, R.drawable.image_medium_mount_sm);
    }

    private void handleMountInfo(String box, String modelString, TextView shortMountPill, TextView mediumMountPill, TextView title, TextView desc) {
        if (this.mountInfo.noneOfTheMountsWillWork()) {
            this.usecase = 3;
        }
        switch (this.usecase) {
            case 1:
                loadImage(R.id.illustration, R.drawable.asset_install_medium_pov);
                if (shortMountPill != null) {
                    shortMountPill.setText(R.string.lens_is_too_low);
                    shortMountPill.setBackgroundResource(R.drawable.pill_red);
                    shortMountPill.setVisibility(VISIBLE);
                }
                if (mediumMountPill != null) {
                    mediumMountPill.setVisibility(GONE);
                    break;
                }
                break;
            case 2:
                if (StringUtils.equalsOrBothEmptyAfterTrim(box, "Old_Box")) {
                    loadImage(R.id.illustration, R.drawable.asset_install_2016_short_pov);
                } else {
                    loadImage(R.id.illustration, R.drawable.asset_install_2017_short_pov);
                }
                if (shortMountPill != null) {
                    shortMountPill.setVisibility(GONE);
                }
                if (mediumMountPill != null) {
                    mediumMountPill.setText(R.string.lens_is_too_high);
                    mediumMountPill.setBackgroundResource(R.drawable.pill_red);
                    mediumMountPill.setVisibility(VISIBLE);
                    break;
                }
                break;
            case 3:
                if (StringUtils.equalsOrBothEmptyAfterTrim(box, "Old_Box")) {
                    loadImage(R.id.illustration, R.drawable.img_installation_lens_2016_short_low);
                } else {
                    loadImage(R.id.illustration, R.drawable.img_installation_lens_2017_short_low);
                }
                if (shortMountPill != null) {
                    shortMountPill.setText(R.string.not_recommended);
                    shortMountPill.setBackgroundResource(R.drawable.pill_red);
                    shortMountPill.setVisibility(VISIBLE);
                }
                if (mediumMountPill != null) {
                    mediumMountPill.setText(R.string.not_recommended);
                    mediumMountPill.setBackgroundResource(R.drawable.pill_red);
                    mediumMountPill.setVisibility(VISIBLE);
                }
                if (title != null) {
                    title.setText(R.string.no_mount_will_work_title);
                }
                if (desc != null) {
                    desc.setText(StringUtils.fromHtml(getString(R.string.no_mount_will_work_desc, new Object[]{modelString})));
                    desc.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            MountPickerActivity.this.startActivity(new Intent(MountPickerActivity.this.getApplicationContext(), ContactUsActivity.class));
                        }
                    });
                    break;
                }
                break;
            default:
                loadImage(R.id.illustration, R.drawable.asset_install_medium_pov);
                if (shortMountPill != null) {
                    shortMountPill.setVisibility(GONE);
                }
                if (mediumMountPill != null) {
                    mediumMountPill.setText(getString(R.string.recommended_for_your_car, new Object[]{modelString}));
                    mediumMountPill.setBackgroundResource(R.drawable.pill_blue);
                    mediumMountPill.setVisibility(VISIBLE);
                    break;
                }
                break;
        }
        ImageView backBtn = (ImageView) findViewById(R.id.backBtn);
        TextView selectToContinue = (TextView) findViewById(R.id.select_mount_to_continue);
        if (this.usecase == 3) {
            if (selectToContinue != null) {
                selectToContinue.setVisibility(INVISIBLE);
            }
            if (backBtn != null) {
                backBtn.setImageResource(R.drawable.button_back_black);
                return;
            }
            return;
        }
        if (selectToContinue != null) {
            selectToContinue.setVisibility(VISIBLE);
        }
        if (backBtn != null) {
            backBtn.setImageResource(R.drawable.btn_back);
        }
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        Tracker.tagScreen(getTrackerScreen());
    }

    public void onShortMountClick(View view) {
        Intent intent;
        tagMountClickEvent(InstallAttributes.MOUNT_PICKER_MOUNT_TYPE_SHORT);
        if (this.mountInfo.shortSupported) {
            intent = new Intent(getApplicationContext(), InstallActivity.class);
            intent.putExtra("extra_step", R.layout.fle_install_short_mount);
        } else {
            intent = new Intent(getApplicationContext(), MountNotRecommendedActivity.class);
            intent.putExtra("extra_mount", MountType.SHORT.getValue());
        }
        startActivity(intent);
    }

    public void onMediumTallMountClick(View view) {
        Intent intent;
        tagMountClickEvent(InstallAttributes.MOUNT_PICKER_MOUNT_TYPE_MED_TALL);
        if (!this.mountInfo.mediumSupported && !this.mountInfo.tallSupported) {
            intent = new Intent(getApplicationContext(), MountNotRecommendedActivity.class);
            intent.putExtra("extra_mount", MountType.TALL.getValue());
        } else if (StringUtils.equalsOrBothEmptyAfterTrim(this.box, "New_Box")) {
            intent = new Intent(getApplicationContext(), YouWillNeedTheMountKitActivity.class);
        } else {
            intent = new Intent(getApplicationContext(), InstallActivity.class);
            intent.putExtra("extra_step", R.layout.fle_install_medium_or_tall_mount);
        }
        startActivity(intent);
    }

    public void onBackClick(View view) {
        finish();
    }

    @NonNull
    private String getTrackerScreen() {
        switch (this.usecase) {
            case 1:
                return Install.SHORT_IS_TOO_LOW;
            case 2:
                return Install.MEDIUM_TALL_IS_TOO_HIGH;
            case 3:
                return Install.NO_MOUNT_SUPPORTED;
            default:
                return Install.MEDIUM_MOUNT_RECOMMENDED;
        }
    }

    private void tagMountClickEvent(String mountPickerMountTypeShort) {
        HashMap<String, String> attributes = new HashMap(2);
        attributes.put(InstallAttributes.MOUNT_PICKER_MOUNT_TYPE, mountPickerMountTypeShort);
        attributes.put(InstallAttributes.MOUNT_PICKER_USE_CASE, getTrackerScreen());
        Tracker.tagEvent(Event.Install.MOUNT_PICKER_SELECTION, attributes);
    }
}
