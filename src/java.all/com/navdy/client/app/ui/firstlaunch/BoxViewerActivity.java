package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsConstants.ProfilePreferences;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class BoxViewerActivity extends BaseToolbarActivity {
    public static final String EXTRA_BOX = "extra_box";
    private String box;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_box_viewer);
        View next = findViewById(R.id.next);
        this.box = getIntent().getStringExtra(EXTRA_BOX);
        if (StringUtils.isEmptyAfterTrim(this.box) && next != null) {
            next.performClick();
        }
    }

    public void onNextClick(View view) {
        SharedPreferences sharedPreferences = SettingsUtils.getSharedPreferences();
        if (sharedPreferences != null) {
            String lastConfig = sharedPreferences.getString(SettingsConstants.LAST_CONFIG, "");
            String currentConfig = getCurrentConfig();
            if (!StringUtils.equalsOrBothEmptyAfterTrim(lastConfig, currentConfig)) {
                sharedPreferences.edit().putInt(SettingsConstants.NB_CONFIG, sharedPreferences.getInt(SettingsConstants.NB_CONFIG, 0) + 1).putString(SettingsConstants.LAST_CONFIG, currentConfig).apply();
            }
            sharedPreferences.edit().putBoolean(SettingsConstants.HAS_SKIPPED_INSTALL, false).apply();
        }
        Intent intent = new Intent(getApplicationContext(), EditCarInfoActivity.class);
        intent.putExtra("extra_next_step", "installation_flow");
        startActivity(intent);
        finish();
    }

    private String getCurrentConfig() {
        SharedPreferences customerPrefs = SettingsUtils.getCustomerPreferences();
        String carYear = customerPrefs.getString(ProfilePreferences.CAR_YEAR, "");
        String carMake = customerPrefs.getString(ProfilePreferences.CAR_MAKE, "");
        String carModel = customerPrefs.getString(ProfilePreferences.CAR_MODEL, "");
        return carYear + carMake + carModel + this.box + InstallPagerAdapter.getCurrentMountType(SettingsUtils.getSharedPreferences()).getValue();
    }

    protected void onResume() {
        hideSystemUI();
        TextView title = (TextView) findViewById(R.id.title);
        TextView subtitle = (TextView) findViewById(R.id.subtitle);
        String str = this.box;
        Object obj = -1;
        switch (str.hashCode()) {
            case -784816532:
                if (str.equals("New_Box")) {
                    obj = null;
                    break;
                }
                break;
            case 285544307:
                if (str.equals("Old_Box")) {
                    obj = 2;
                    break;
                }
                break;
            case 1554803820:
                if (str.equals("New_Box_Plus_Mounts")) {
                    obj = 1;
                    break;
                }
                break;
        }
        switch (obj) {
            case null:
                loadImage(R.id.box_photo, R.drawable.image_navdy_box);
                if (title != null) {
                    title.setText(R.string.new_box_title);
                }
                if (subtitle != null) {
                    subtitle.setText(R.string.new_box_long_description);
                }
                Tracker.tagScreen(Install.BOX_VIEWER_NEW_BOX);
                break;
            case 1:
                loadImage(R.id.box_photo, R.drawable.image_navdy_plus_kit);
                if (title != null) {
                    title.setText(R.string.new_box_plus_mounts_title);
                }
                if (subtitle != null) {
                    subtitle.setText(R.string.new_box_plus_mounts_long_description);
                }
                Tracker.tagScreen(Install.BOX_VIEWER_NEW_BOX_PLUS_MOUNTS);
                break;
            case 2:
                loadImage(R.id.box_photo, R.drawable.image_navdy_oversized);
                if (title != null) {
                    title.setText(R.string.old_box_title);
                }
                if (subtitle != null) {
                    subtitle.setText(R.string.old_box_long_description);
                }
                Tracker.tagScreen(Install.BOX_VIEWER_OLD_BOX);
                break;
            default:
                this.logger.e("Invalid box type: " + this.box);
                finish();
                break;
        }
        super.onResume();
    }

    public void onBackClick(View view) {
        finish();
    }
}
