package com.navdy.client.app.ui.firstlaunch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.ui.base.BaseSupportFragment;

public class AppSetupBottomCardGenericFragment extends BaseSupportFragment {
    public static final String SCREEN = "screen";
    private TextView blueText;
    private Button button;
    private TextView description;
    private AppSetupScreen screen = AppSetupPagerAdapter.getScreen(0);
    private TextView title;

    public void setArguments(Bundle args) {
        super.setArguments(args);
        this.screen = (AppSetupScreen) args.getParcelable("screen");
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fle_app_setup_bottom_card_generic, null);
        this.title = (TextView) rootView.findViewById(R.id.title);
        this.description = (TextView) rootView.findViewById(R.id.desc);
        this.blueText = (TextView) rootView.findViewById(R.id.blue_text);
        this.button = (Button) rootView.findViewById(R.id.next_step);
        showNormal();
        return rootView;
    }

    public void showNormal() {
        showThis(this.screen.titleRes, this.screen.descriptionRes, this.screen.blueTextRes, this.screen.buttonRes);
    }

    public void showFail() {
        showThis(this.screen.titleFailRes, this.screen.descriptionFailRes, this.screen.blueTextRes, this.screen.buttonFailRes);
    }

    private void showThis(int titleRes, int descriptionRes, int blueTextRes, int buttonRes) {
        if (this.title != null) {
            if (titleRes > 0) {
                this.title.setText(titleRes);
                this.title.setVisibility(VISIBLE);
            } else {
                this.title.setVisibility(GONE);
            }
        }
        if (this.description != null) {
            if (descriptionRes > 0) {
                this.description.setText(StringUtils.fromHtml(descriptionRes));
                this.description.setVisibility(VISIBLE);
            } else {
                this.description.setVisibility(GONE);
            }
        }
        if (this.blueText != null) {
            if (blueTextRes > 0) {
                this.blueText.setText(blueTextRes);
                this.blueText.setVisibility(VISIBLE);
            } else {
                this.blueText.setVisibility(GONE);
            }
        }
        if (this.button == null) {
            return;
        }
        if (buttonRes > 0) {
            this.button.setText(buttonRes);
            this.button.setVisibility(VISIBLE);
            return;
        }
        this.button.setVisibility(GONE);
    }
}
