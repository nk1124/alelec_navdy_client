package com.navdy.client.app.ui.homescreen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.base.BaseActivity;

public class InfoDialogActivity extends BaseActivity {
    public static final String EXTRA_LAYOUT = "layout";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent == null) {
            finish();
        } else {
            setContentView(intent.getIntExtra(EXTRA_LAYOUT, R.layout.dialog_voice_search));
        }
    }

    public void onButtonClick(View v) {
        finish();
    }

    public void onCloseClick(View view) {
        onBackPressed();
    }
}
