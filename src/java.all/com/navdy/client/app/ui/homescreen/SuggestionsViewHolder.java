package com.navdy.client.app.ui.homescreen;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.customviews.UnitSystemTextView;

public class SuggestionsViewHolder extends ViewHolder {
    public View background;
    public TextView firstLine;
    public ImageView icon;
    public ImageButton infoButton;
    public ImageView rightChevron;
    public View row;
    public TextView secondLine;
    public View separator;
    public UnitSystemTextView subIllustrationText;
    public TextView thirdLine;

    @SuppressLint({"ResourceType"})
    public SuggestionsViewHolder(View itemView) {
        super(itemView);
        this.background = itemView.findViewById(R.id.background);
        this.row = itemView.findViewById(R.id.card_row);
        this.separator = itemView.findViewById(R.id.separator);
        this.icon = (ImageView) itemView.findViewById(R.id.illustration);
        this.firstLine = (TextView) itemView.findViewById(R.id.card_first_line);
        this.secondLine = (TextView) itemView.findViewById(R.id.card_second_line);
        this.subIllustrationText = (UnitSystemTextView) itemView.findViewById(R.id.card_sub_illustration_text);
        this.thirdLine = (TextView) itemView.findViewById(R.id.card_third_line);
        this.rightChevron = (ImageView) itemView.findViewById(R.id.right_chevron);
        this.infoButton = (ImageButton) itemView.findViewById(R.id.nav_button);
        if (this.thirdLine != null) {
            this.thirdLine.setVisibility(GONE);
        }
    }
}
