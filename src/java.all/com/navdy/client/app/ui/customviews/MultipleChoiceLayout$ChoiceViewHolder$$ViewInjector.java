package com.navdy.client.app.ui.customviews;

import android.widget.TextView;
import butterknife.ButterKnife.Finder;
import com.alelec.navdyclient.R;

public class MultipleChoiceLayout$ChoiceViewHolder$$ViewInjector {
    public static void inject(Finder finder, ChoiceViewHolder target, Object source) {
        target.textView = (TextView) finder.findRequiredView(source, R.id.text, "field 'textView'");
        target.highlightView = finder.findRequiredView(source, R.id.highlight, "field 'highlightView'");
    }

    public static void reset(ChoiceViewHolder target) {
        target.textView = null;
        target.highlightView = null;
    }
}
