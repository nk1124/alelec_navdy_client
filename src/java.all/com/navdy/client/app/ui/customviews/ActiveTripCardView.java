package com.navdy.client.app.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import com.alelec.navdyclient.R;
import com.navdy.client.app.ui.routing.ActiveTripActivity;
import com.navdy.service.library.log.Logger;

public class ActiveTripCardView extends TripCardView {
    private static final Logger logger = new Logger(ActiveTripCardView.class);
    private final Context context;

    public ActiveTripCardView(Context context) {
        this(context, null, 0);
    }

    public ActiveTripCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ActiveTripCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ActiveTripCardView.logger.v("onClick, start active trip activity");
                ActiveTripCardView.this.handleOnClick();
            }
        });
    }

    public void handleOnClick() {
        ActiveTripActivity.startActiveTripActivity(this.context);
    }

    protected String appendPrefix(String name) {
        return getResources().getString(R.string.active_trip_prefix, new Object[]{name});
    }
}
