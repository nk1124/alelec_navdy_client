package com.navdy.client.app.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.navdy.client.app.framework.util.TimeStampUtils;

public class ArrivalTimeTextView extends TextView {
    private static final long UPDATE_INTERVAL = 60000;
    private int currentTimeToArrival;
    private final Runnable updateTime;

    public ArrivalTimeTextView(Context context) {
        this(context, null);
    }

    public ArrivalTimeTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ArrivalTimeTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.updateTime = new Runnable() {
            public void run() {
                ArrivalTimeTextView.this.setText(TimeStampUtils.getArrivalTimeString(Integer.valueOf(ArrivalTimeTextView.this.currentTimeToArrival)));
                ArrivalTimeTextView.this.postDelayed(this, 60000);
            }
        };
    }

    public void setTimeToArrival(int timeToArrival) {
        removeCallbacks(this.updateTime);
        this.currentTimeToArrival = timeToArrival;
        post(this.updateTime);
    }

    protected void onDetachedFromWindow() {
        removeCallbacks(this.updateTime);
        super.onDetachedFromWindow();
    }
}
