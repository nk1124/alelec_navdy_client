package com.navdy.client.app.ui.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;
import com.alelec.navdyclient.R;

public class MultipleChoiceLayout extends RecyclerView {
    private MultipleChoiceAdapter adapter;
    private ChoiceListener choiceListener;
    private LayoutManager layoutManager;
    private int regularTextColor;
    private int selectedColor;

    public interface ChoiceListener {
        void onChoiceSelected(String str, int i);
    }

    static class ChoiceViewHolder extends ViewHolder {
        @InjectView(2131755271)
        View highlightView;
        @InjectView(2131755021)
        TextView textView;

        public ChoiceViewHolder(View view) {
            super(view);
            ButterKnife.inject((Object) this, view);
        }
    }

    class MultipleChoiceAdapter extends Adapter<ChoiceViewHolder> {
        private int selectedIndex;
        private CharSequence[] texts;

        MultipleChoiceAdapter() {
        }

        public void setTexts(CharSequence[] texts) {
            this.texts = texts;
            notifyDataSetChanged();
        }

        public ChoiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ChoiceViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.choice_selected_text, parent, false));
        }

        public void onBindViewHolder(ChoiceViewHolder holder, final int position) {
            final CharSequence text = this.texts[position];
            holder.textView.setText(text);
            if (position == this.selectedIndex) {
                holder.textView.setTextColor(MultipleChoiceLayout.this.selectedColor);
                holder.highlightView.setVisibility(VISIBLE);
            } else {
                holder.textView.setTextColor(MultipleChoiceLayout.this.regularTextColor);
                holder.highlightView.setVisibility(GONE);
            }
            holder.itemView.setClickable(true);
            holder.itemView.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    MultipleChoiceAdapter.this.setSelectedIndex(position);
                    if (MultipleChoiceLayout.this.choiceListener != null) {
                        MultipleChoiceLayout.this.choiceListener.onChoiceSelected((String) text, position);
                    }
                }
            });
        }

        public int getItemCount() {
            return this.texts != null ? this.texts.length : 0;
        }

        public void setSelectedIndex(int selectedIndex) {
            this.selectedIndex = selectedIndex;
            notifyDataSetChanged();
        }
    }

    public MultipleChoiceLayout(Context context) {
        this(context, null);
    }

    public MultipleChoiceLayout(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public MultipleChoiceLayout(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray customAttributes = context.obtainStyledAttributes(attrs, R.styleable.MultipleChoiceLayout, defStyle, 0);
        this.adapter = new MultipleChoiceAdapter();
        setAdapter(this.adapter);
        int spanCount = 0;
        if (customAttributes != null) {
            CharSequence[] texts = customAttributes.getTextArray(0);
            customAttributes.recycle();
            this.adapter.setTexts(texts);
            spanCount = texts.length;
        }
        this.layoutManager = new GridLayoutManager(context, spanCount);
        setLayoutManager(this.layoutManager);
        this.regularTextColor = context.getResources().getColor(R.color.grey_4);
        this.selectedColor = context.getResources().getColor(R.color.navdy_blue);
    }

    public void setSelectedIndex(int index) {
        this.adapter.setSelectedIndex(index);
    }

    public MultipleChoiceAdapter getAdapter() {
        return this.adapter;
    }

    public void setChoiceListener(ChoiceListener choiceListener) {
        this.choiceListener = choiceListener;
    }
}
