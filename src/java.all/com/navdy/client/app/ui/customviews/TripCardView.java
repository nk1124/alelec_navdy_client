package com.navdy.client.app.ui.customviews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.i18n.AddressUtils;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.Error;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteInfo;
import com.navdy.client.app.framework.navigation.NavdyRouteHandler.NavdyRouteListener;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.framework.util.TimeStampUtils;
import com.navdy.service.library.log.Logger;

public class TripCardView extends RelativeLayout implements NavdyRouteListener {
    private static final int COLOR_RED = ContextCompat.getColor(NavdyApplication.getAppContext(), R.color.red);
    private static final boolean VERBOSE = false;
    private static final Logger logger = new Logger(TripCardView.class);
    private View bottomSheet;
    private View bottomSheetCalculating;
    private TextView bottomSheetCalculatingText;
    protected ImageView icon;
    protected final NavdyRouteHandler navdyRouteHandler;
    protected TextView tripAddressFirstLine;
    protected TextView tripAddressSecondLine;
    protected TextView tripName;
    private ArrivalTimeTextView tripOverviewArrivalTime;
    private UnitSystemTextView tripOverviewDistance;
    private TextView tripOverviewETA;

    public TripCardView(Context context) {
        this(context, null, 0);
    }

    public TripCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TripCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.navdyRouteHandler = NavdyRouteHandler.getInstance();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.icon = (ImageView) findViewById(R.id.illustration);
        this.tripName = (TextView) findViewById(R.id.trip_name);
        this.tripAddressFirstLine = (TextView) findViewById(R.id.trip_address_first_line);
        this.tripAddressSecondLine = (TextView) findViewById(R.id.trip_address_second_line);
        this.tripOverviewDistance = (UnitSystemTextView) findViewById(R.id.trip_card_distance);
        this.tripOverviewETA = (TextView) findViewById(R.id.trip_card_eta);
        this.tripOverviewArrivalTime = (ArrivalTimeTextView) findViewById(R.id.trip_card_arrival_time);
        this.bottomSheet = findViewById(R.id.trip_card_bottom_sheet);
        this.bottomSheetCalculating = findViewById(R.id.trip_card_bottom_sheet_calculating);
        this.bottomSheetCalculatingText = (TextView) findViewById(R.id.trip_card_bottom_sheet_calculating_text);
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.navdyRouteHandler.addListener(this);
    }

    protected void onDetachedFromWindow() {
        this.navdyRouteHandler.removeListener(this);
        super.onDetachedFromWindow();
    }

    public void onPendingRouteCalculating(@NonNull Destination destination) {
        setNameAddressAndIcon(destination);
        showCalculating();
    }

    public void onPendingRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo pendingRoute) {
        if (error != Error.NONE) {
            setNameAddressAndIcon(pendingRoute.getDestination());
            showError();
            return;
        }
        updateFields(pendingRoute.getDestination(), pendingRoute.getDistanceToDestination(), pendingRoute.getTimeToDestination(), pendingRoute.isTrafficHeavy);
    }

    public void onRouteCalculating(@NonNull Destination destination) {
        updateFields(destination, -1, -1, false);
        showCalculating();
    }

    public void onRouteCalculated(@NonNull Error error, @NonNull NavdyRouteInfo route) {
        if (error != Error.NONE) {
            updateFields(route.getDestination(), -1, -1, false);
            showError();
            return;
        }
        updateFields(route.getDestination(), route.getDistanceToDestination(), route.getTimeToDestination(), route.isTrafficHeavy);
    }

    public void onRouteStarted(@NonNull NavdyRouteInfo route) {
        updateFields(route.getDestination(), route.getDistanceToDestination(), route.getTimeToDestination(), route.isTrafficHeavy);
    }

    public void onTripProgress(@NonNull NavdyRouteInfo progress) {
        updateFields(progress.getDestination(), progress.getDistanceToDestination(), progress.getTimeToDestination(), progress.isTrafficHeavy);
    }

    public void onReroute() {
        showCalculating();
    }

    public void onRouteArrived(@NonNull Destination destination) {
    }

    public void onStopRoute() {
    }

    private void showCalculating() {
        this.bottomSheetCalculatingText.setText(R.string.calculating_dot_dot_dot);
        this.bottomSheet.setVisibility(INVISIBLE);
        this.bottomSheetCalculating.setVisibility(VISIBLE);
    }

    private void showError() {
        this.bottomSheetCalculatingText.setText(R.string.error_please_retry_route);
        this.bottomSheet.setVisibility(INVISIBLE);
        this.bottomSheetCalculating.setVisibility(VISIBLE);
    }

    private void updateFields(Destination destination, int distance, int timeToDestination, boolean isTrafficHeavy) {
        if (distance < 0 || timeToDestination < 0) {
            showCalculating();
        } else {
            if (isTrafficHeavy) {
                this.tripOverviewArrivalTime.setTextColor(COLOR_RED);
            }
            hideMessages();
            updateTextFields(distance, timeToDestination);
        }
        setNameAddressAndIcon(destination);
    }

    private void hideMessages() {
        this.bottomSheetCalculating.setVisibility(GONE);
        this.bottomSheet.setVisibility(VISIBLE);
    }

    private void updateTextFields(int distance, int durationWithTraffic) {
        this.tripOverviewETA.setText(TimeStampUtils.getDurationStringInHoursAndMinutes(Integer.valueOf(durationWithTraffic)));
        this.tripOverviewDistance.setDistance((double) distance);
        this.tripOverviewArrivalTime.setTimeToArrival(durationWithTraffic);
    }

    protected String appendPrefix(String name) {
        return name;
    }

    public void setNameAddressAndIcon(Destination destination) {
        if (destination == null) {
            logger.d("Destination object is null");
            return;
        }
        if (this.icon != null) {
            int asset;
            if (this instanceof PendingTripCardView) {
                asset = destination.getBadgeAssetForPendingTrip();
            } else {
                asset = destination.getBadgeAssetForActiveTrip();
            }
            this.icon.setImageResource(asset);
        }
        Pair<String, String> titleAndSubtitle = destination.getTitleAndSubtitle();
        if (StringUtils.isEmptyAfterTrim((CharSequence) titleAndSubtitle.first)) {
            logger.d("titleAndSubtitle is empty: " + titleAndSubtitle.toString());
            return;
        }
        if (this.tripName != null) {
            this.tripName.setText(appendPrefix((String) titleAndSubtitle.first));
        } else {
            logger.w("Current route has no name!");
        }
        if (this.tripAddressFirstLine != null) {
            this.tripAddressFirstLine.setText(AddressUtils.sanitizeAddress((String) titleAndSubtitle.second));
        } else {
            logger.w("Current route has no address!");
        }
    }

    public boolean isInEditMode() {
        return false;
    }
}
