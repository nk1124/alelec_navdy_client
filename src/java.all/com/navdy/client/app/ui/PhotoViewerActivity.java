package com.navdy.client.app.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.util.StringUtils;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.ui.base.BaseToolbarActivity;

public class PhotoViewerActivity extends BaseToolbarActivity {
    public static final String EXTRA_FILE_PATH = "EXTRA_FILE_PATH";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.photo_viewer);
        String filePath = getIntent().getStringExtra(EXTRA_FILE_PATH);
        if (StringUtils.isEmptyAfterTrim(filePath)) {
            this.logger.e("Image path not specified !");
            finish();
            return;
        }
        ImageView image = (ImageView) findViewById(R.id.image);
        if (image == null) {
            this.logger.e("Image layout element not found !");
            finish();
            return;
        }
        Bitmap bitmap = Tracker.getBitmapFromInternalStorage(filePath);
        if (bitmap == null) {
            this.logger.e("Unable to get a bitmap from the filePath: " + filePath + " !");
            image.setImageResource(R.drawable.image_generic_port);
            return;
        }
        image.setImageBitmap(bitmap);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
    }

    public void onDoneClick(View view) {
        finish();
    }
}
