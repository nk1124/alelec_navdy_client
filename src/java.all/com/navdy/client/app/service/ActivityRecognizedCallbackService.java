package com.navdy.client.app.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.SystemClock;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.navdy.client.app.NavdyApplication;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.client.app.framework.Injector;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.service.library.device.RemoteDevice;
import com.navdy.service.library.events.hudcontrol.AccelerateShutdown.AccelerateReason;
import com.navdy.service.library.events.hudcontrol.AccelerateShutdown.Builder;
import com.navdy.service.library.log.Logger;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;

public class ActivityRecognizedCallbackService extends IntentService {
    public static final int MINIMUM_CONFIDENCE_THRESHOLD = 50;
    private static long STATE_EXPIRY_INTERVAL = TimeUnit.MINUTES.toMillis(3);
    public static final int STATE_IN_VEHICLE = 1;
    public static final int STATE_ON_FOOT = 2;
    public static final int STATE_UNKNOWN = 0;
    public static final int WALKING_ACTIVITY_CONFIDENCE_MINIMUM_THRESHOLD = 75;
    private static int currentState = 0;
    private static int lastState = 0;
    private static long lastStateUpdateTime;
    public static final Logger sLogger = new Logger(ActivityRecognizedCallbackService.class);
    long[] DEBUG_VIBRATE_PATTERN = new long[]{0, 500, 100, 500, 100, 500};
    @Inject
    TTSAudioRouter audioRouter;

    public ActivityRecognizedCallbackService() {
        super("ActivityRecognizedCallbackService");
    }

    public void onCreate() {
        super.onCreate();
        Injector.inject(NavdyApplication.getAppContext(), this);
    }

    protected void onHandleIntent(Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            sLogger.d("onHandleIntent, Activity recognized callback has results");
            handleDetectedActivities(ActivityRecognitionResult.extractResult(intent).getProbableActivities());
        }
    }

    private void handleDetectedActivities(List<DetectedActivity> probableActivities) {
        boolean isInVehicle = false;
        int isInVehicleConfidence = 0;
        boolean isOnFoot = false;
        int isOnFootConfidence = 0;
        for (DetectedActivity activity : probableActivities) {
            switch (activity.getType()) {
                case 0:
                    isInVehicle = true;
                    isInVehicleConfidence = activity.getConfidence();
                    break;
                case 2:
                    isOnFoot = true;
                    isOnFootConfidence = activity.getConfidence();
                    break;
            }
            if (isInVehicle || isOnFoot) {
                sLogger.d("Activity " + activity + ", Confidence : " + activity.getConfidence());
            }
        }
        if (SystemClock.elapsedRealtime() - lastStateUpdateTime > STATE_EXPIRY_INTERVAL) {
            sLogger.d("Last update is expired");
            lastState = currentState;
            setCurrentState(0);
        } else {
            lastState = currentState;
        }
        if (!isOnFoot || isOnFootConfidence <= 50) {
            if (!isInVehicle || isInVehicleConfidence <= 50) {
                sLogger.d("Currently UNKNOWN");
            } else {
                setCurrentState(1);
            }
        } else if (!isInVehicle || isOnFootConfidence > isInVehicleConfidence) {
            setCurrentState(2);
        } else {
            setCurrentState(1);
        }
        DataCollectionService.getInstance().handleDetectedActivities(isInVehicle, isInVehicleConfidence);
    }

    private void setCurrentState(int state) {
        sLogger.d("setCurrentState " + state);
        currentState = state;
        lastStateUpdateTime = SystemClock.elapsedRealtime();
        String message;
        switch (state) {
            case 1:
                sLogger.d("Currently IN_VEHICLE");
                message = "You got into your vehicle";
                break;
            case 2:
                sLogger.d("Currently ON_FOOT");
                message = "You are walking now";
                break;
            default:
                return;
        }
        if (lastState != currentState) {
        }
        if (lastState == 1 && currentState == 2) {
            sLogger.d("Transition from in vehicle to on foot, time to send accelerate shutdown");
            RemoteDevice remoteDevice = AppInstance.getInstance().getRemoteDevice();
            if (remoteDevice != null) {
                remoteDevice.postEvent(new Builder().reason(AccelerateReason.ACCELERATE_REASON_WALKING).build());
            }
        }
    }
}
