package com.navdy.client;

import android.app.backup.BackupAgentHelper;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.app.backup.FullBackupDataOutput;
import android.os.ParcelFileDescriptor;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.io.IOException;

public class NavdyBackupAgent extends BackupAgentHelper {
    Logger logger = new Logger(NavdyBackupAgent.class);

    public void onCreate() {
        super.onCreate();
        this.logger.v("onCreate");
    }

    public void onBackup(ParcelFileDescriptor oldState, BackupDataOutput data, ParcelFileDescriptor newState) throws IOException {
        super.onBackup(oldState, data, newState);
        this.logger.v("onBackup: oldState = " + oldState + ", data = " + data + ", newState = " + newState);
    }

    public void onFullBackup(FullBackupDataOutput data) throws IOException {
        super.onFullBackup(data);
        this.logger.v("onFullBackup: data = " + data);
    }

    public void onQuotaExceeded(long backupDataBytes, long quotaBytes) {
        super.onQuotaExceeded(backupDataBytes, quotaBytes);
        this.logger.e("Backup quota exceeded! backupDataBytes = " + backupDataBytes + ", quotaBytes = " + quotaBytes);
    }

    public void onRestore(BackupDataInput data, int appVersionCode, ParcelFileDescriptor newState) throws IOException {
        super.onRestore(data, appVersionCode, newState);
        this.logger.v("onRestore: data = " + data + ", appVersionCode = " + appVersionCode + ", newState = " + newState);
    }

    public void onRestoreFile(ParcelFileDescriptor data, long size, File destination, int type, long mode, long mtime) throws IOException {
        super.onRestoreFile(data, size, destination, type, mode, mtime);
        this.logger.v("onRestoreFile: data = " + data + ", size = " + size + ", destination = " + destination + ", type = " + type + ", mode = " + mode + ", mtime = " + mtime);
    }

    public void onRestoreFinished() {
        super.onRestoreFinished();
        this.logger.v("onRestoreFinished");
        SettingsUtils.getSharedPreferences().edit().remove(SettingsConstants.GMS_IS_MISSING).remove(SettingsConstants.GMS_VERSION_NUMBER).apply();
    }

    public void onDestroy() {
        super.onDestroy();
        this.logger.v("onDestroy");
    }
}
