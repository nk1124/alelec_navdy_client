package com.navdy.proxy;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.navdy.service.library.log.Logger;
import java.io.IOException;
import java.io.OutputStream;

public class ProxyOutputThread extends Thread {
    private static final boolean VERBOSE_DBG = false;
    private static final Logger sLogger = new Logger(ProxyOutputThread.class);
    public Handler mHandler;
    OutputStream mOutputStream;
    ProxyThread mProxyThread;

    public ProxyOutputThread(ProxyThread proxyThread, OutputStream outputStream) {
        setName(ProxyOutputThread.class.getSimpleName());
        this.mOutputStream = outputStream;
        this.mProxyThread = proxyThread;
    }

    public synchronized Handler getHandler() {
        while (this.mHandler == null) {
            try {
                wait();
            } catch (InterruptedException e) {
                if (this.mHandler != null) {
                    break;
                }
            }
        }
        return this.mHandler;
    }

    public void run() {
        Looper.prepare();
        synchronized (this) {
            this.mHandler = new Handler() {
                public void handleMessage(Message msg) {
                    try {
                        ProxyOutputThread.this.mOutputStream.write((byte[]) msg.obj);
                    } catch (IOException e) {
                        ProxyOutputThread.sLogger.e("error writing to output", e);
                        Looper.myLooper().quit();
                    }
                }
            };
            notifyAll();
        }
        sLogger.i("entering loop");
        Looper.loop();
        sLogger.i("loop exited");
        this.mProxyThread.outputThreadWillFinish();
    }
}
