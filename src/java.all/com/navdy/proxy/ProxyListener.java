package com.navdy.proxy;

import com.navdy.service.library.device.connection.ProxyService;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.SocketAcceptor;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;

public class ProxyListener extends ProxyService {
    public static final int MAX_RETRY_INTERVAL = 16000;
    public static final int MIN_RETRY_INTERVAL = 1000;
    private static final Logger sLogger = new Logger(ProxyListener.class);
    private volatile boolean closing = false;
    private SocketAcceptor mSocketAcceptor;
    private int retryInterval = 1000;

    public ProxyListener(SocketAcceptor acceptor) {
        setName(ProxyListener.class.getSimpleName());
        this.mSocketAcceptor = acceptor;
    }

    public void run() {
        sLogger.d("start: " + this.mSocketAcceptor);
        while (!this.closing) {
            try {
                SocketAdapter fromSocket = this.mSocketAcceptor.accept();
                this.retryInterval = 1000;
                sLogger.v("accepted connection (" + fromSocket + ")");
                try {
                    new ProxyThread(fromSocket).start();
                } catch (Exception e) {
                    sLogger.w("Exception while starting proxy", e);
                    IOUtils.closeStream(fromSocket);
                }
            } catch (Exception e2) {
                if (this.closing) {
                    continue;
                } else {
                    sLogger.e(e2.toString());
                    try {
                        Thread.sleep((long) this.retryInterval);
                    } catch (InterruptedException e3) {
                    }
                    this.retryInterval *= 2;
                    if (this.retryInterval > MAX_RETRY_INTERVAL) {
                        sLogger.e("Giving up on proxy - assuming that we'll be restarted");
                        break;
                    }
                }
            }
        }
        sLogger.d("end: " + this.mSocketAcceptor);
    }

    public void cancel() {
        try {
            this.closing = true;
            this.mSocketAcceptor.close();
        } catch (IOException e) {
            sLogger.e("error closing socket acceptor: " + e);
        }
    }
}
