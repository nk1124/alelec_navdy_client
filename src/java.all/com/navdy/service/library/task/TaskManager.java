package com.navdy.service.library.task;

import android.util.SparseArray;
import com.navdy.service.library.log.Logger;
import java.io.Serializable;
import java.util.Comparator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public final class TaskManager {
    private static final int INITIAL_CAPACITY = 10;
    private static final boolean VERBOSE = false;
    private static AtomicLong orderCounter = new AtomicLong(1);
    private static Logger sLogger = new Logger(TaskManager.class);
    private static final Comparator<Runnable> sPriorityComparator = new PriorityTaskComparator();
    private static final TaskManager sSingleton = new TaskManager();
    private static final AtomicInteger threadNameCounter = new AtomicInteger(0);
    private final SparseArray<ExecutorService> executors = new SparseArray();
    private boolean initialized;

    private static class PriorityRunnable implements Runnable {
        final long order;
        final int priority;
        final Runnable runnable;

        PriorityRunnable(Runnable runnable, TaskPriority priority, long order) {
            this.runnable = runnable;
            this.priority = priority.getValue();
            this.order = order;
        }

        public void run() {
            try {
                this.runnable.run();
            } catch (Throwable ex) {
                TaskManager.sLogger.e("TaskManager:", ex);
            }
        }
    }

    private static final class PriorityTask<T> extends FutureTask<T> implements Comparable<PriorityTask<T>> {
        private final PriorityRunnable priorityRunnable;

        public PriorityTask(PriorityRunnable runnable, Callable<T> tCallable) {
            super(tCallable);
            this.priorityRunnable = runnable;
        }

        public PriorityTask(PriorityRunnable runnable, T result) {
            super(runnable, result);
            this.priorityRunnable = runnable;
        }

        public int compareTo(PriorityTask<T> o) {
            int diff = this.priorityRunnable.priority - o.priorityRunnable.priority;
            if (diff == 0) {
                return (int) (this.priorityRunnable.order - o.priorityRunnable.order);
            }
            return diff;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof PriorityTask)) {
                return false;
            }
            PriorityTask other = (PriorityTask) obj;
            if (this.priorityRunnable.priority == other.priorityRunnable.priority && this.priorityRunnable.order == other.priorityRunnable.order) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (int) this.priorityRunnable.order;
        }
    }

    private static class PriorityTaskComparator implements Comparator<Runnable>, Serializable {
        private PriorityTaskComparator() {
        }

        /* synthetic */ PriorityTaskComparator(AnonymousClass1 x0) {
            this();
        }

        public int compare(Runnable left, Runnable right) {
            return ((PriorityTask) left).compareTo((PriorityTask) right);
        }
    }

    public static class TaskManagerExecutor extends ThreadPoolExecutor {
        TaskManagerExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
        }

        protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
            return new PriorityTask((PriorityRunnable) runnable, (Object) value);
        }
    }

    public enum TaskPriority {
        LOW(1),
        NORMAL(2),
        HIGH(3);
        
        private final int val;

        private TaskPriority(int val) {
            this.val = val;
        }

        public int getValue() {
            return this.val;
        }
    }

    public static TaskManager getInstance() {
        return sSingleton;
    }

    private TaskManager() {
    }

    public void init() {
        if (this.initialized) {
            throw new IllegalStateException("already initialized");
        }
        this.initialized = true;
    }

    public void addTaskQueue(int queueId, int poolSize) {
        if (this.initialized) {
            throw new IllegalStateException("already initialized");
        } else if (this.executors.get(queueId) != null) {
            throw new IllegalArgumentException("already exists");
        } else {
            this.executors.put(queueId, createExecutor(poolSize));
        }
    }

    public ExecutorService getExecutor(int queueId) {
        return (ExecutorService) this.executors.get(queueId);
    }

    private ExecutorService createExecutor(int poolSize) {
        return new TaskManagerExecutor(poolSize, poolSize, 60, TimeUnit.SECONDS, new PriorityBlockingQueue(10, sPriorityComparator), new ThreadFactory() {
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setName("navdy_" + TaskManager.threadNameCounter.incrementAndGet());
                thread.setDaemon(true);
                return thread;
            }
        });
    }

    public Future execute(Runnable runnable, int queueId, TaskPriority taskPriority) {
        if (!this.initialized) {
            sLogger.w("Task manager not initialized", new Throwable());
            return null;
        } else if (runnable != null) {
            return ((ExecutorService) this.executors.get(queueId)).submit(new PriorityRunnable(runnable, taskPriority, orderCounter.getAndIncrement()));
        } else {
            throw new IllegalArgumentException();
        }
    }

    public Future execute(Runnable runnable, int queueId) {
        return execute(runnable, queueId, TaskPriority.NORMAL);
    }
}
