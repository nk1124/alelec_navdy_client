package com.navdy.service.library.network;

import com.navdy.service.library.device.connection.ServiceAddress;
import java.io.IOException;

public class TCPSocketFactory implements SocketFactory {
    private final String host;
    private final int port;

    public TCPSocketFactory(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public TCPSocketFactory(ServiceAddress address) {
        this(address.getAddress(), Integer.parseInt(address.getService()));
    }

    public SocketAdapter build() throws IOException {
        return new TCPSocketAdapter(this.host, this.port);
    }
}
