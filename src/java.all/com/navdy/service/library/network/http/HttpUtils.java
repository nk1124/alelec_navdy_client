package com.navdy.service.library.network.http;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class HttpUtils {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final String MIME_TYPE_TEXT = "text/plain";
    public static final String MIME_TYPE_ZIP = "application/zip";

    public static RequestBody getJsonRequestBody(String jsonData) {
        return RequestBody.create(JSON, jsonData);
    }
}
