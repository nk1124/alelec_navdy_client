package com.navdy.service.library.file;

import android.content.Context;
import android.text.TextUtils;
import com.here.odnp.util.OdnpConstants;
import com.navdy.service.library.events.file.FileTransferData;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferRequest;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferResponse.Builder;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.log.Logger;
import com.squareup.wire.Wire;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import okio.ByteString;

public class FileTransferSessionManager implements IFileTransferManager {
    private static final int MAX_SESSIONS = 5;
    private static final int SESSION_TIMEOUT_INTERVAL = 60000;
    private static final Logger sLogger = new Logger(FileTransferSessionManager.class);
    private Context mContext;
    private IFileTransferAuthority mFileTransferAuthority;
    private HashMap<Integer, FileTransferSession> mFileTransferSessionsIdIndexed;
    private HashMap<String, FileTransferSession> mFileTransferSessionsPathIndexed;
    private AtomicInteger mTransferId = new AtomicInteger(0);

    public FileTransferSessionManager(Context context, IFileTransferAuthority authority) {
        this.mContext = context;
        this.mFileTransferAuthority = authority;
        this.mFileTransferSessionsPathIndexed = new HashMap();
        this.mFileTransferSessionsIdIndexed = new HashMap();
    }

    public FileTransferResponse handleFileTransferRequest(FileTransferRequest request) {
        if (request == null) {
            return null;
        }
        try {
            if (!this.mFileTransferAuthority.isFileTypeAllowed(request.fileType)) {
                return new Builder().success(Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(FileTransferError.FILE_TRANSFER_PERMISSION_DENIED).build();
            }
            FileTransferSession session;
            FileTransferResponse response;
            if (request.fileType == FileType.FILE_TYPE_PERF_TEST) {
                session = new FileTransferSession(this.mTransferId.incrementAndGet());
                response = session.initTestData(this.mContext, request.fileType, TransferDataSource.TEST_DATA_NAME, request.fileSize.longValue());
                if (!response.success.booleanValue()) {
                    return response;
                }
                this.mFileTransferSessionsIdIndexed.put(Integer.valueOf(session.mTransferId), session);
                return response;
            } else if (isPullRequest(request.fileType)) {
                session = new FileTransferSession(this.mTransferId.incrementAndGet());
                String tempLogFolder = this.mFileTransferAuthority.getDirectoryForFileType(request.fileType);
                String absoluteFilePath = this.mFileTransferAuthority.getFileToSend(request.fileType);
                if (TextUtils.isEmpty(absoluteFilePath)) {
                    return new Builder().success(Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                }
                File fileToSend = new File(absoluteFilePath);
                if (!fileToSend.exists()) {
                    return new Builder().success(Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
                }
                response = session.initPull(this.mContext, request.fileType, tempLogFolder, fileToSend.getName(), ((Boolean) Wire.get(request.supportsAcks, FileTransferRequest.DEFAULT_SUPPORTSACKS)).booleanValue());
                if (!response.success.booleanValue()) {
                    return response;
                }
                this.mFileTransferSessionsIdIndexed.put(Integer.valueOf(session.mTransferId), session);
                return response;
            } else {
                String destinationFolder = this.mFileTransferAuthority.getDirectoryForFileType(request.fileType);
                String absolutePath = absolutePath(this.mContext, destinationFolder, request.destinationFileName);
                session = (FileTransferSession) this.mFileTransferSessionsPathIndexed.get(absolutePath);
                if (session == null && this.mFileTransferSessionsPathIndexed.size() == 5 && endSessions(true) == 0) {
                    return new Builder().success(Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(FileTransferError.FILE_TRANSFER_HOST_BUSY).build();
                }
                endFileTransferSession(session, false);
                session = new FileTransferSession(this.mTransferId.incrementAndGet());
                response = session.initFileTransfer(this.mContext, request.fileType, destinationFolder, request.destinationFileName, request.fileSize.longValue(), request.offset.longValue(), request.fileDataChecksum, Boolean.TRUE.equals(request.override));
                if (!response.success.booleanValue()) {
                    return response;
                }
                this.mFileTransferSessionsIdIndexed.put(Integer.valueOf(session.mTransferId), session);
                this.mFileTransferSessionsPathIndexed.put(absolutePath, session);
                return response;
            }
        } catch (Throwable t) {
            sLogger.e("Exception in handle file request ", t);
            return new Builder().success(Boolean.valueOf(false)).destinationFileName(request.destinationFileName).fileType(request.fileType).error(FileTransferError.FILE_TRANSFER_IO_ERROR).build();
        }
    }

    private int endSessions(boolean onlyInactiveSession) {
        long currentTimeStamp = System.currentTimeMillis();
        int removed = 0;
        Iterator<Integer> keysIterator = this.mFileTransferSessionsIdIndexed.keySet().iterator();
        while (keysIterator.hasNext()) {
            FileTransferSession session = (FileTransferSession) this.mFileTransferSessionsIdIndexed.get(keysIterator.next());
            if (!onlyInactiveSession || (session != null && currentTimeStamp - session.getLastActivity() > OdnpConstants.ONE_MINUTE_IN_MS)) {
                keysIterator.remove();
                endFileTransferSession(session, false);
                removed++;
            }
        }
        return removed;
    }

    public void stop() {
        endSessions(false);
    }

    public boolean handleFileTransferStatus(FileTransferStatus fileTransferStatus) {
        if (fileTransferStatus == null) {
            return false;
        }
        FileTransferSession session = (FileTransferSession) this.mFileTransferSessionsIdIndexed.get(fileTransferStatus.transferId);
        if (session != null) {
            return session.handleFileTransferStatus(fileTransferStatus);
        }
        return false;
    }

    public FileTransferStatus handleFileTransferData(FileTransferData data) {
        FileTransferSession session = (FileTransferSession) this.mFileTransferSessionsIdIndexed.get(data.transferId);
        if (session == null) {
            return new FileTransferStatus.Builder().success(Boolean.valueOf(false)).transferComplete(Boolean.valueOf(false)).error(FileTransferError.FILE_TRANSFER_NOT_INITIATED).build();
        }
        FileTransferStatus status = session.appendChunk(this.mContext, data);
        if (!status.transferComplete.booleanValue() && (status.error == null || status.error == FileTransferError.FILE_TRANSFER_NO_ERROR)) {
            return status;
        }
        endFileTransferSession(session, false);
        return status;
    }

    public FileTransferData getNextChunk(int transferId) throws Throwable {
        FileTransferSession session = (FileTransferSession) this.mFileTransferSessionsIdIndexed.get(Integer.valueOf(transferId));
        if (session == null) {
            return null;
        }
        FileTransferData data = session.getNextChunk();
        if (data != null && (!data.lastChunk.booleanValue() || session.isFlowControlEnabled())) {
            return data;
        }
        endFileTransferSession(session, true);
        return data;
    }

    public FileType getFileType(int transferId) {
        FileTransferSession session = (FileTransferSession) this.mFileTransferSessionsIdIndexed.get(Integer.valueOf(transferId));
        if (session != null) {
            return session.getFileType();
        }
        return null;
    }

    private void endFileTransferSession(FileTransferSession session, boolean deleteFile) {
        if (session != null) {
            String fileName;
            String destFolder;
            synchronized (session) {
                fileName = session.mFileName;
                destFolder = session.mDestinationFolder;
            }
            this.mFileTransferSessionsPathIndexed.remove(absolutePath(this.mContext, destFolder, fileName));
            this.mFileTransferSessionsIdIndexed.remove(Integer.valueOf(session.mTransferId));
            session.endSession(this.mContext, deleteFile);
        }
    }

    public void endFileTransferSession(int transferId, boolean deleteFile) {
        FileTransferSession session = (FileTransferSession) this.mFileTransferSessionsIdIndexed.get(Integer.valueOf(transferId));
        if (session != null) {
            endFileTransferSession(session, deleteFile);
        }
    }

    public String absolutePathForTransferId(int transferId) {
        FileTransferSession transferSession = (FileTransferSession) this.mFileTransferSessionsIdIndexed.get(Integer.valueOf(transferId));
        if (transferSession == null) {
            return null;
        }
        String fileName;
        String destFolder;
        synchronized (transferSession) {
            fileName = transferSession.mFileName;
            destFolder = transferSession.mDestinationFolder;
        }
        return absolutePath(this.mContext, destFolder, fileName);
    }

    public static final String absolutePath(Context context, String destinationFolder, String destinationFileName) {
        StringBuilder stringBuilder = new StringBuilder();
        if (destinationFolder == null) {
            destinationFolder = context.getFilesDir();
        }
        return stringBuilder.append(destinationFolder).append(File.separator).append(destinationFileName).toString();
    }

    public static FileTransferData prepareFileTransferData(int transferId, TransferDataSource source, int chunkIndex, int chunkSize, long startOffset, long alreadyTransferred) throws Throwable {
        try {
            long remainingBytes = source.length() - alreadyTransferred;
            int nextChunkSize = chunkSize;
            boolean lastChunk = false;
            if (remainingBytes < ((long) chunkSize)) {
                nextChunkSize = (int) remainingBytes;
                lastChunk = true;
            }
            byte[] chunk = new byte[nextChunkSize];
            source.seek(alreadyTransferred);
            source.read(chunk);
            FileTransferData.Builder fileTransferDataBuilder = new FileTransferData.Builder().transferId(Integer.valueOf(transferId)).chunkIndex(Integer.valueOf(chunkIndex));
            if (lastChunk) {
                fileTransferDataBuilder.fileCheckSum(source.checkSum());
            }
            return fileTransferDataBuilder.dataBytes(ByteString.of(chunk)).lastChunk(Boolean.valueOf(lastChunk)).build();
        } catch (Throwable t) {
            sLogger.e("Exception while preparing the chunk data " + t);
        }
    }

    public static boolean isPullRequest(FileType fileType) {
        switch (fileType) {
            case FILE_TYPE_LOGS:
                return true;
            default:
                return false;
        }
    }
}
