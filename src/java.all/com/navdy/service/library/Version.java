package com.navdy.service.library;

public class Version {
    public static final Version PROTOCOL_VERSION = new Version(1, 0);
    public final int majorVersion;
    public final int minorVersion;

    public Version(int majorVersion, int minorVersion) {
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
    }

    public String toString() {
        return this.majorVersion + "." + this.minorVersion;
    }
}
