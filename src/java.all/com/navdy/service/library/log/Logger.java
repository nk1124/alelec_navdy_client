package com.navdy.service.library.log;

import android.util.Log;

public final class Logger {
    public static final String ACTION_RELOAD = "com.navdy.service.library.log.action.RELOAD";
    public static final String DEFAULT_TAG = "Navdy";
    private static volatile long logLevelChange = -1;
    private static LogAppender[] sAppenders = new LogAppender[0];
    private volatile boolean[] loggable;
    private long startTime;
    private final String tagName;
    private volatile long timestamp = -2;

    public static void reloadLogLevels() {
        logLevelChange = System.currentTimeMillis();
    }

    public static void init(LogAppender[] appenders) {
        if (appenders == null || appenders.length == 0) {
            throw new IllegalArgumentException();
        }
        sAppenders = (LogAppender[]) appenders.clone();
    }

    public static synchronized void addAppender(LogAppender appender) {
        synchronized (Logger.class) {
            if (appender == null) {
                throw new IllegalArgumentException();
            }
            LogAppender[] appenders = new LogAppender[(sAppenders.length + 1)];
            for (int i = 0; i < appenders.length; i++) {
                if (i < appenders.length - 1) {
                    appenders[i] = sAppenders[i];
                } else {
                    appenders[i] = appender;
                }
            }
            sAppenders = appenders;
        }
    }

    public static void flush() {
        for (LogAppender flush : sAppenders) {
            flush.flush();
        }
    }

    public static void close() {
        for (LogAppender close : sAppenders) {
            close.close();
        }
    }

    public Logger(String tagName) {
        if (tagName != null) {
            this.tagName = tagName.substring(0, Math.min(tagName.length(), 23));
        } else {
            this.tagName = "Navdy";
        }
    }

    public Logger(Class clazz) {
        if (clazz != null) {
            String name = clazz.getSimpleName();
            this.tagName = name.substring(0, Math.min(name.length(), 23));
            return;
        }
        this.tagName = "Navdy";
    }

    public boolean isLoggable(int level) {
        if (this.timestamp < logLevelChange) {
            if (this.loggable == null) {
                this.loggable = new boolean[8];
            }
            for (int i = 2; i <= 7; i++) {
                this.loggable[i] = Log.isLoggable(this.tagName, i);
            }
            this.timestamp = System.currentTimeMillis();
        }
        return this.loggable[level];
    }

    public void v(String msg) {
        for (LogAppender v : sAppenders) {
            v.v(this.tagName, msg);
        }
    }

    public void v(String msg, Throwable tr) {
        for (LogAppender v : sAppenders) {
            v.v(this.tagName, msg, tr);
        }
    }

    public void d(String msg) {
        for (LogAppender d : sAppenders) {
            d.d(this.tagName, msg);
        }
    }

    public void d(String msg, Throwable tr) {
        for (LogAppender d : sAppenders) {
            d.d(this.tagName, msg, tr);
        }
    }

    public void i(String msg) {
        for (LogAppender i : sAppenders) {
            i.i(this.tagName, msg);
        }
    }

    public void i(String msg, Throwable tr) {
        for (LogAppender i : sAppenders) {
            i.i(this.tagName, msg, tr);
        }
    }

    public void w(String msg) {
        for (LogAppender w : sAppenders) {
            w.w(this.tagName, msg);
        }
    }

    public void w(String msg, Throwable tr) {
        for (LogAppender w : sAppenders) {
            w.w(this.tagName, msg, tr);
        }
    }

    public void e(String msg) {
        for (LogAppender e : sAppenders) {
            e.e(this.tagName, msg);
        }
    }

    public void e(String msg, Throwable tr) {
        for (LogAppender e : sAppenders) {
            e.e(this.tagName, msg, tr);
        }
    }

    public void e(Throwable tr) {
        e("", tr);
    }

    public void w(Throwable tr) {
        w("", tr);
    }

    public void d(Throwable tr) {
        d("", tr);
    }

    public void v(Throwable tr) {
        v("", tr);
    }

    public void i(Throwable tr) {
        i("", tr);
    }

    public void recordStartTime() {
        this.startTime = System.currentTimeMillis();
    }

    public void logTimeTaken(String message) {
        d(message + " " + (System.currentTimeMillis() - this.startTime) + " ms");
    }
}
