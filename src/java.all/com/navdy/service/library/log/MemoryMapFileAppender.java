package com.navdy.service.library.log;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Process;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.navdy.service.library.util.IOUtils;
import com.navdy.service.library.util.SystemUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.io.Writer;
import java.net.UnknownHostException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class MemoryMapFileAppender implements LogAppender {
    private static final String COLON = ":";
    private static final String CURRENT_POINTER_PREF = "pointer";
    private static final String CURRENT_POINTER_PREF_FILE_SUFFIX = "_current_log_pointer";
    private static final long MIN_FILE_SIZE = 16384;
    private static final String NEWLINE = "\r\n";
    private static final byte[] ROLLOVER_MARKER = "\r\n<<<<rolling>>>>\r\n".getBytes();
    private static final int ROLLOVER_META_LEN = ROLLOVER_MARKER.length;
    private static final String SLASH = "/";
    private static final String SPACE = " ";
    private static final String TAG = "MemoryMapFileAppender";
    private StringBuilder builder;
    private Context context;
    private Editor currentPointerPrefEditor;
    private String currentPointerPrefFileName;
    private DateFormat dateFormat;
    private String fileName;
    private long fileSize;
    private int maxFiles;
    private MappedByteBuffer memoryMap;

    public MemoryMapFileAppender(Context context, String path, String fileName, long fileSize, int maxFiles) {
        this(context, path, fileName, fileSize, maxFiles, true);
    }

    public MemoryMapFileAppender(Context context, String path, String fileName, long fileSize, int maxFiles, boolean useProcessName) {
        this.maxFiles = 0;
        this.builder = new StringBuilder(20480);
        this.dateFormat = new SimpleDateFormat("MM-dd HH:mm:ss.SSS", Locale.US);
        if (context == null || TextUtils.isEmpty(fileName)) {
            throw new IllegalArgumentException();
        }
        this.maxFiles = maxFiles;
        if (fileSize < 16384) {
            fileSize = 16384;
        }
        String processName = "";
        if (useProcessName) {
            processName = "_" + SystemUtils.getProcessName(context, Process.myPid());
        }
        this.context = context;
        this.fileName = path + File.separator + fileName + processName;
        this.fileSize = fileSize;
        this.currentPointerPrefFileName = processName + CURRENT_POINTER_PREF_FILE_SUFFIX;
        try {
            Log.d(TAG, "MemoryMapFileAppender::ctor::start");
            IOUtils.createDirectory(path);
            SharedPreferences prefs = context.getSharedPreferences(this.currentPointerPrefFileName, 0);
            int currentPointer = prefs.getInt(CURRENT_POINTER_PREF, 0);
            if (currentPointer > 0 && ((long) currentPointer) < fileSize) {
                prefs.edit().remove(CURRENT_POINTER_PREF).apply();
                Log.i(TAG, "MemoryMapFileAppender, pos set = " + currentPointer + " for " + this.fileName);
            }
            setMemoryMap(this.fileName + ".txt", currentPointer, fileSize);
            Log.d(TAG, "MemoryMapFileAppender::ctor::end");
        } catch (Throwable t) {
            Log.e(TAG, "MemoryMapFileAppender.ctor()::", t);
        }
    }

    protected void append(String type, String tag, String msg, Throwable t) {
        try {
            synchronized (this) {
                this.builder.setLength(0);
                this.builder.append(this.dateFormat.format(Long.valueOf(System.currentTimeMillis())));
                this.builder.append(SPACE);
                this.builder.append(type);
                this.builder.append("/");
                this.builder.append(tag);
                this.builder.append(COLON);
                this.builder.append(SPACE);
                if (msg != null) {
                    this.builder.append(msg);
                }
                if (t != null) {
                    this.builder.append("\r\n");
                    this.builder.append(getStackTraceString(t));
                }
                this.builder.append("\r\n");
                append(this.builder.toString());
            }
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    protected void append(String str) {
        try {
            if (this.memoryMap != null && str != null) {
                byte[] data = str.getBytes();
                int data_len = data.length;
                if (((long) (ROLLOVER_META_LEN + data_len)) < this.fileSize) {
                    if (((long) (this.memoryMap.position() + data_len)) >= this.fileSize) {
                        rollOver();
                    }
                    this.memoryMap.put(data);
                }
            }
        } catch (Throwable t) {
            Log.e(TAG, "MemoryMapFileAppender.append()::", t);
        }
    }

    private void setMemoryMap(String outputPath, int position, long size) {
        Throwable t;
        RandomAccessFile dataFileHandle = null;
        FileChannel dataFileChannel = null;
        try {
            File file = new File(outputPath);
            if (position == 0 && file.exists()) {
                rollFiles();
            }
            RandomAccessFile dataFileHandle2 = new RandomAccessFile(outputPath, "rw");
            try {
                dataFileChannel = dataFileHandle2.getChannel();
                this.memoryMap = dataFileChannel.map(MapMode.READ_WRITE, 0, 1 + size);
                if (position > 0) {
                    this.memoryMap.position(position);
                }
                IOUtils.closeStream(dataFileHandle2);
                dataFileHandle = null;
                IOUtils.closeStream(dataFileChannel);
            } catch (Throwable th) {
                t = th;
                dataFileHandle = dataFileHandle2;
                IOUtils.closeStream(dataFileHandle);
                IOUtils.closeStream(dataFileChannel);
                this.memoryMap = null;
                Log.e(TAG, "setMemoryMap::", t);
            }
        } catch (Throwable th2) {
            t = th2;
            IOUtils.closeStream(dataFileHandle);
            IOUtils.closeStream(dataFileChannel);
            this.memoryMap = null;
            Log.e(TAG, "setMemoryMap::", t);
        }
    }

    private void rollFiles() {
        if (this.maxFiles > 0) {
            IOUtils.deleteFile(this.context, this.fileName + "." + getFormatedNumber(0) + ".txt");
        }
        int i = 0;
        while (i < this.maxFiles) {
            File src;
            if (i == this.maxFiles - 1) {
                src = new File(this.fileName + ".txt");
            } else {
                src = new File(this.fileName + "." + getFormatedNumber(i + 1) + ".txt");
            }
            if (src.exists() && !src.renameTo(new File(this.fileName + "." + getFormatedNumber(i) + ".txt"))) {
                Log.w(TAG, "Unable to rename " + this.fileName + ".txt");
            }
            i++;
        }
    }

    private String getFormatedNumber(int i) {
        return String.format("%0" + ((int) (Math.log10((double) this.maxFiles) + 1.0d)) + "d", new Object[]{Integer.valueOf(i)});
    }

    private synchronized void rollOver() {
        if (this.maxFiles == 0) {
            this.memoryMap.position(0);
            this.memoryMap.put(ROLLOVER_MARKER);
        } else {
            this.memoryMap.force();
            rollFiles();
            setMemoryMap(this.fileName + ".txt", 0, this.fileSize);
        }
    }

    public void v(String tag, String msg) {
        append("V", tag, msg, null);
    }

    public void v(String tag, String msg, Throwable tr) {
        append("V", tag, msg, tr);
    }

    public void d(String tag, String msg) {
        append("D", tag, msg, null);
    }

    public void d(String tag, String msg, Throwable tr) {
        append("D", tag, msg, tr);
    }

    public void i(String tag, String msg) {
        append("I", tag, msg, null);
    }

    public void i(String tag, String msg, Throwable tr) {
        append("I", tag, msg, tr);
    }

    public void w(String tag, String msg) {
        append("W", tag, msg, null);
    }

    public void w(String tag, String msg, Throwable tr) {
        append("W", tag, msg, tr);
    }

    public void e(String tag, String msg) {
        append("W", tag, msg, null);
    }

    public void e(String tag, String msg, Throwable tr) {
        append("W", tag, msg, tr);
    }

    public void close() {
        synchronized (this) {
            Log.d(TAG, "MemoryMapFileAppender:closing");
            flush();
            this.memoryMap = null;
        }
    }

    private void truncateFileAtCurrentPosition(String fileName) {
        FileNotFoundException e;
        Throwable th;
        IOException e2;
        long position = (long) this.memoryMap.position();
        RandomAccessFile raf = null;
        try {
            RandomAccessFile raf2 = new RandomAccessFile(fileName, "rws");
            try {
                raf2.setLength(position);
                IOUtils.closeStream(raf2);
                raf = raf2;
            } catch (FileNotFoundException e3) {
                e = e3;
                raf = raf2;
                try {
                    e.printStackTrace();
                    IOUtils.closeStream(raf);
                } catch (Throwable th2) {
                    th = th2;
                    IOUtils.closeStream(raf);
                    throw th;
                }
            } catch (IOException e4) {
                e2 = e4;
                raf = raf2;
                e2.printStackTrace();
                IOUtils.closeStream(raf);
            } catch (Throwable th3) {
                th = th3;
                raf = raf2;
                IOUtils.closeStream(raf);
                throw th;
            }
        } catch (FileNotFoundException e5) {
            e = e5;
            e.printStackTrace();
            IOUtils.closeStream(raf);
        } catch (IOException e6) {
            e2 = e6;
            e2.printStackTrace();
            IOUtils.closeStream(raf);
        }
    }

    public void flush() {
        synchronized (this) {
            try {
                if (this.memoryMap != null) {
                    int curPos = this.memoryMap.position();
                    this.memoryMap.force();
                    if (curPos > 0) {
                        if (this.currentPointerPrefEditor == null) {
                            this.currentPointerPrefEditor = this.context.getSharedPreferences(this.currentPointerPrefFileName, 0).edit();
                        }
                        this.currentPointerPrefEditor.putInt(CURRENT_POINTER_PREF, curPos).commit();
                        Log.d(TAG, "MemoryMapFileAppender:stored pref pos:" + curPos);
                    }
                }
            } catch (Throwable t) {
                Log.d(TAG, "MemoryMapFileAppender:flush", t);
            }
        }
    }

    public static String getStackTraceString(Throwable tr) {
        if (tr == null) {
            return "";
        }
        for (Throwable t = tr; t != null; t = t.getCause()) {
            if (t instanceof UnknownHostException) {
                return "";
            }
        }
        Writer sw = new StringWriter();
        PrintWriter pw = new FastPrintWriter(sw, false, 256);
        tr.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

    @NonNull
    public ArrayList<File> getLogFiles() {
        ArrayList<File> files = new ArrayList(this.maxFiles);
        for (int i = 0; i < this.maxFiles; i++) {
            String currentFile = this.fileName + "." + getFormatedNumber(i) + ".txt";
            if (i == this.maxFiles - 1) {
                try {
                    IOUtils.copyFile(this.fileName + ".txt", currentFile);
                    truncateFileAtCurrentPosition(currentFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            File file = new File(currentFile);
            if (file.exists() && file.length() > 0) {
                files.add(file);
            }
        }
        return files;
    }
}
