package com.navdy.service.library.log;

import android.util.Log;

public class LogcatAppender implements LogAppender {
    public void v(String tag, String msg) {
        Log.v(tag, msg);
    }

    public void v(String tag, String msg, Throwable tr) {
        Log.v(tag, msg, tr);
    }

    public void d(String tag, String msg) {
        Log.d(tag, msg);
    }

    public void d(String tag, String msg, Throwable tr) {
        Log.d(tag, msg, tr);
    }

    public void i(String tag, String msg) {
        Log.i(tag, msg);
    }

    public void i(String tag, String msg, Throwable tr) {
        Log.i(tag, msg, tr);
    }

    public void w(String tag, String msg) {
        Log.w(tag, msg);
    }

    public void w(String tag, String msg, Throwable tr) {
        Log.w(tag, msg, tr);
    }

    public void e(String tag, String msg) {
        Log.e(tag, msg);
    }

    public void e(String tag, String msg, Throwable tr) {
        Log.e(tag, msg, tr);
    }

    public void flush() {
    }

    public void close() {
    }
}
