package com.navdy.service.library.device.discovery;

import android.content.Context;
import com.navdy.service.library.device.connection.ConnectionType;

public class TCPRemoteDeviceScanner extends MDNSRemoteDeviceScanner {
    public static final String TAG = "TCPRemoteDeviceScanner";

    public TCPRemoteDeviceScanner(Context context) {
        super(context, ConnectionType.TCP_PROTOBUF.getServiceType());
    }
}
