package com.navdy.service.library.device.discovery;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdManager.DiscoveryListener;
import android.net.nsd.NsdManager.ResolveListener;
import android.net.nsd.NsdServiceInfo;
import android.os.Handler;
import android.os.Looper;
import com.navdy.service.library.device.connection.ConnectionInfo;
import com.navdy.service.library.log.Logger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

public class MDNSRemoteDeviceScanner extends RemoteDeviceScanner {
    public static final Logger sLogger = new Logger(MDNSRemoteDeviceScanner.class);
    protected Timer discoveryTimer;
    protected DiscoveryListener mDiscoveryListener;
    protected boolean mDiscoveryRunning;
    protected Handler mHandler = new Handler(Looper.getMainLooper());
    protected NsdManager mNsdManager;
    protected ArrayList<ResolveListener> mResolveListeners = new ArrayList();
    protected NsdServiceInfo mResolvingService;
    protected final String mServiceType;
    protected Queue<NsdServiceInfo> mUnresolvedServices = new LinkedList();

    public MDNSRemoteDeviceScanner(Context context, String serviceType) {
        super(context);
        this.mServiceType = serviceType;
    }

    public boolean startScan() {
        sLogger.e("Starting scan: " + this.mServiceType);
        initNsd();
        if (this.mNsdManager == null) {
            sLogger.e("Can't scan: no NsdManager: " + this.mServiceType);
            return false;
        } else if (this.discoveryTimer != null || this.mDiscoveryRunning) {
            sLogger.e("Can't start: already started " + this.mServiceType);
            return true;
        } else {
            this.discoveryTimer = new Timer();
            this.discoveryTimer.schedule(new TimerTask() {
                public void run() {
                    MDNSRemoteDeviceScanner.sLogger.e("Scan timer fired: " + MDNSRemoteDeviceScanner.this.mServiceType);
                    MDNSRemoteDeviceScanner.this.mNsdManager.discoverServices(MDNSRemoteDeviceScanner.this.mServiceType, 1, MDNSRemoteDeviceScanner.this.mDiscoveryListener);
                    MDNSRemoteDeviceScanner.sLogger.e("Scan started: " + MDNSRemoteDeviceScanner.this.mServiceType);
                }
            }, 2000);
            return true;
        }
    }

    public boolean stopScan() {
        sLogger.e("Stopping scan: " + this.mServiceType);
        if (this.mNsdManager == null) {
            sLogger.e("Can't stop: already stopped: " + this.mServiceType);
            return false;
        } else if (this.discoveryTimer != null) {
            this.discoveryTimer.cancel();
            this.discoveryTimer = null;
            return true;
        } else {
            if (this.mDiscoveryListener != null) {
                try {
                    this.mNsdManager.stopServiceDiscovery(this.mDiscoveryListener);
                } catch (Exception e) {
                    if (this.mDiscoveryRunning) {
                        sLogger.e("Problem stopping nsd service discovery", e);
                    }
                }
                this.mDiscoveryListener = null;
                this.mDiscoveryRunning = false;
            }
            return true;
        }
    }

    public void initNsd() {
        initializeDiscoveryListener();
    }

    public void initializeDiscoveryListener() {
        if (this.mNsdManager == null) {
            this.mNsdManager = (NsdManager) this.mContext.getSystemService("servicediscovery");
        }
        if (this.mDiscoveryListener == null) {
            this.mDiscoveryListener = new DiscoveryListener() {
                public void onDiscoveryStarted(String regType) {
                    MDNSRemoteDeviceScanner.this.mDiscoveryRunning = true;
                    MDNSRemoteDeviceScanner.this.discoveryTimer = null;
                    MDNSRemoteDeviceScanner.sLogger.d("Service discovery started");
                    MDNSRemoteDeviceScanner.this.dispatchOnScanStarted();
                }

                public void onServiceFound(NsdServiceInfo service) {
                    MDNSRemoteDeviceScanner.sLogger.d("Service discovery success " + service);
                    if (ConnectionInfo.isValidNavdyServiceInfo(service)) {
                        MDNSRemoteDeviceScanner.this.resolveService(service);
                    }
                }

                public void onServiceLost(NsdServiceInfo service) {
                    MDNSRemoteDeviceScanner.sLogger.e("service lost" + service);
                }

                public void onDiscoveryStopped(String serviceType) {
                    MDNSRemoteDeviceScanner.this.mDiscoveryRunning = false;
                    MDNSRemoteDeviceScanner.sLogger.i("Discovery stopped: " + serviceType);
                    MDNSRemoteDeviceScanner.this.dispatchOnScanStopped();
                }

                public void onStartDiscoveryFailed(String serviceType, int errorCode) {
                    MDNSRemoteDeviceScanner.sLogger.e("Start Discovery failed: Error code:" + errorCode);
                }

                public void onStopDiscoveryFailed(String serviceType, int errorCode) {
                    MDNSRemoteDeviceScanner.sLogger.e("Stop Discovery failed: Error code:" + errorCode);
                }
            };
        }
    }

    protected void resolveService(NsdServiceInfo service) {
        if (this.mResolvingService == null) {
            this.mResolvingService = service;
            ResolveListener resolveListener = getNewResolveListener();
            this.mResolveListeners.add(resolveListener);
            this.mNsdManager.resolveService(service, resolveListener);
            return;
        }
        this.mUnresolvedServices.add(service);
    }

    protected void resolveComplete() {
        this.mResolvingService = null;
        if (this.mUnresolvedServices.size() > 0) {
            resolveService((NsdServiceInfo) this.mUnresolvedServices.remove());
        }
    }

    public ResolveListener getNewResolveListener() {
        return new ResolveListener() {
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
                MDNSRemoteDeviceScanner.this.mResolveListeners.remove(this);
                MDNSRemoteDeviceScanner.sLogger.e("Resolve failed" + errorCode);
                MDNSRemoteDeviceScanner.this.resolveComplete();
            }

            public void onServiceResolved(NsdServiceInfo serviceInfo) {
                MDNSRemoteDeviceScanner.this.mResolveListeners.remove(this);
                MDNSRemoteDeviceScanner.sLogger.e("Resolve Succeeded. " + serviceInfo);
                try {
                    final ConnectionInfo connectionInfo = ConnectionInfo.fromServiceInfo(serviceInfo);
                    MDNSRemoteDeviceScanner.this.mHandler.post(new Runnable() {
                        public void run() {
                            MDNSRemoteDeviceScanner.this.dispatchOnDiscovered(connectionInfo);
                        }
                    });
                    MDNSRemoteDeviceScanner.this.resolveComplete();
                } catch (IllegalArgumentException e) {
                    MDNSRemoteDeviceScanner.sLogger.e("Unable to process serviceInfo");
                }
            }
        };
    }
}
