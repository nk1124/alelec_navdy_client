package com.navdy.service.library.device.link;

import android.os.Handler;
import com.navdy.service.library.device.RemoteDevice.PostEventHandler;
import com.navdy.service.library.device.RemoteDevice.PostEventStatus;

public class EventRequest {
    public final PostEventHandler eventCompleteHandler;
    public final byte[] eventData;
    private final Handler handler;

    public EventRequest(Handler handler, byte[] eventData, PostEventHandler eventCompleteHandler) {
        this.eventData = (byte[]) eventData.clone();
        this.eventCompleteHandler = eventCompleteHandler;
        this.handler = handler;
    }

    public void callCompletionHandlers(final PostEventStatus status) {
        this.handler.post(new Runnable() {
            public void run() {
                if (EventRequest.this.eventCompleteHandler != null) {
                    EventRequest.this.eventCompleteHandler.onComplete(status);
                }
            }
        });
    }
}
