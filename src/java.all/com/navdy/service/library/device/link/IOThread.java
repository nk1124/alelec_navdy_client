package com.navdy.service.library.device.link;

import com.navdy.service.library.log.Logger;

public class IOThread extends Thread {
    private static final int SHUTDOWN_TIMEOUT = 1000;
    protected volatile boolean closing = false;
    protected final Logger logger = new Logger(getClass());

    public void cancel() {
        this.closing = true;
        interrupt();
        try {
            join(1000);
        } catch (InterruptedException e) {
            this.logger.e("Interrupted");
        }
        if (isAlive()) {
            this.logger.w("Thread still alive after join");
        }
    }

    public boolean isClosing() {
        return this.closing;
    }
}
