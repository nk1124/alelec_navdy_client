package com.navdy.service.library.device.link;

import com.navdy.service.library.device.connection.Connection.DisconnectCause;
import com.navdy.service.library.device.connection.ConnectionType;
import com.navdy.service.library.events.NavdyEventReader;
import com.navdy.service.library.util.IOUtils;
import java.io.InputStream;

public class ReaderThread extends IOThread {
    private final ConnectionType connectionType;
    private boolean isNetworkLinkReadyByDefault = false;
    private final LinkListener linkListener;
    private final NavdyEventReader mmEventReader;
    private InputStream mmInStream;

    public ReaderThread(ConnectionType connectionType, InputStream inputStream, LinkListener listener, boolean isNetworkLinkReadyWhenConnected) {
        setName("ReaderThread");
        this.logger.d("create ReaderThread");
        this.mmInStream = inputStream;
        this.mmEventReader = new NavdyEventReader(this.mmInStream);
        this.linkListener = listener;
        this.connectionType = connectionType;
        this.isNetworkLinkReadyByDefault = isNetworkLinkReadyWhenConnected;
    }

    public void run() {
        this.logger.i("BEGIN");
        DisconnectCause cause = DisconnectCause.NORMAL;
        this.linkListener.linkEstablished(this.connectionType);
        if (this.isNetworkLinkReadyByDefault) {
            this.linkListener.onNetworkLinkReady();
        }
        while (!this.closing) {
            try {
                if (this.logger.isLoggable(3)) {
                    this.logger.v("before read");
                }
                byte[] eventData = this.mmEventReader.readBytes();
                if (eventData == null || eventData.length <= 524288) {
                    if (this.logger.isLoggable(3)) {
                        this.logger.v("after read len:" + (eventData == null ? -1 : eventData.length));
                    }
                    if (eventData == null) {
                        if (!this.closing) {
                            this.logger.e("no event data parsed. assuming disconnect.");
                            cause = DisconnectCause.ABORTED;
                        }
                        this.logger.i("Signaling connection lost cause:" + cause);
                        this.linkListener.linkLost(this.connectionType, cause);
                        this.logger.i("Exiting thread");
                        IOUtils.closeStream(this.mmInStream);
                        this.mmInStream = null;
                    }
                    this.linkListener.onNavdyEventReceived(eventData);
                } else {
                    throw new RuntimeException("reader Max packet size exceeded [" + eventData.length + "] bytes[" + IOUtils.bytesToHexString(eventData, 0, 50) + "]");
                }
            } catch (Exception e) {
                if (!this.closing) {
                    this.logger.e("disconnected: " + e.getMessage());
                    cause = DisconnectCause.ABORTED;
                }
            }
        }
        this.logger.i("Signaling connection lost cause:" + cause);
        this.linkListener.linkLost(this.connectionType, cause);
        this.logger.i("Exiting thread");
        IOUtils.closeStream(this.mmInStream);
        this.mmInStream = null;
    }

    public void cancel() {
        super.cancel();
        IOUtils.closeStream(this.mmInStream);
        this.mmInStream = null;
    }
}
