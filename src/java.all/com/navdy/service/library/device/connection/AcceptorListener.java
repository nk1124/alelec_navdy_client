package com.navdy.service.library.device.connection;

import android.content.Context;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.service.library.network.SocketAcceptor;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;

public class AcceptorListener extends ConnectionListener {
    private SocketAcceptor acceptor;
    private ConnectionType connectionType;

    private class AcceptThread extends ConnectionListener.AcceptThread {
        private volatile boolean closing;

        public AcceptThread() throws IOException {
            super();
            setName("AcceptThread-" + AcceptorListener.this.acceptor.getClass().getSimpleName());
        }

        public void run() {
            AcceptorListener.this.logger.d(getName() + " started, closing:" + this.closing);
            AcceptorListener.this.dispatchStarted();
            SocketAdapter socket = null;
            try {
                socket = AcceptorListener.this.acceptor.accept();
                AcceptorListener.this.dispatchConnected(new SocketConnection(AcceptorListener.this.acceptor.getRemoteConnectionInfo(socket, AcceptorListener.this.connectionType), socket));
            } catch (Throwable e) {
                if (!this.closing) {
                    AcceptorListener.this.logger.e("Socket accept() failed", e);
                }
                IOUtils.closeStream(socket);
            } finally {
                cancel();
            }
            AcceptorListener.this.dispatchStopped();
            AcceptorListener.this.logger.i("END " + getName());
        }

        public void cancel() {
            AcceptorListener.this.logger.d("Socket cancel " + this);
            this.closing = true;
            IOUtils.closeStream(AcceptorListener.this.acceptor);
        }
    }

    public AcceptorListener(Context context, SocketAcceptor acceptor, ConnectionType connectionType) {
        super(context, "Acceptor/" + acceptor.getClass().getSimpleName());
        this.acceptor = acceptor;
        this.connectionType = connectionType;
    }

    public ConnectionType getType() {
        return this.connectionType;
    }

    protected AcceptThread getNewAcceptThread() throws IOException {
        return new AcceptThread();
    }

    public String toString() {
        return getClass().getSimpleName() + (this.acceptor != null ? S3Constants.S3_FILE_DELIMITER + this.acceptor.getClass().getSimpleName() : "");
    }
}
