package com.navdy.service.library.device.connection;

import android.content.Context;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.BTSocketFactory;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.network.TCPSocketFactory;
import com.navdy.service.library.util.Listenable;
import java.util.HashMap;
import java.util.Map;

public abstract class Connection extends Listenable<Listenable.Listener> {
    private static Map<ConnectionType, ConnectionFactory> factoryMap = new HashMap();
    private static ConnectionFactory sDefaultFactory = new ConnectionFactory() {
        public Connection build(Context context, ConnectionInfo connectionInfo) {
            switch (AnonymousClass5.$SwitchMap$com$navdy$service$library$device$connection$ConnectionType[connectionInfo.getType().ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                    ServiceAddress address = connectionInfo.getAddress();
                    if (address.getService().equals(ConnectionService.ACCESSORY_IAP2.toString())) {
                        address = new ServiceAddress(address.getAddress(), ConnectionService.DEVICE_IAP2.toString(), address.getProtocol());
                    }
                    return new SocketConnection(connectionInfo, new BTSocketFactory(address));
                case 5:
                    return new SocketConnection(connectionInfo, new TCPSocketFactory(connectionInfo.getAddress()));
                default:
                    throw new IllegalArgumentException("Unknown connection class for type: " + connectionInfo.getType());
            }
        }
    };
    protected Logger logger = new Logger(getClass());
    protected ConnectionInfo mConnectionInfo;
    protected Status mStatus;

    public interface Listener extends com.navdy.service.library.util.Listenable.Listener {
        void onConnected(Connection connection);

        void onConnectionFailed(Connection connection, ConnectionFailureCause connectionFailureCause);

        void onDisconnected(Connection connection, DisconnectCause disconnectCause);
    }

    public interface ConnectionFactory {
        Connection build(Context context, ConnectionInfo connectionInfo);
    }

    protected interface EventDispatcher extends EventDispatcher<Connection, Listener> {
    }

    /* renamed from: com.navdy.service.library.device.connection.Connection$5 */
    static /* synthetic */ class AnonymousClass5 {
        static final /* synthetic */ int[] $SwitchMap$com$navdy$service$library$device$connection$ConnectionType = new int[ConnectionType.values().length];

        static {
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[ConnectionType.BT_PROTOBUF.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[ConnectionType.BT_TUNNEL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[ConnectionType.EA_PROTOBUF.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[ConnectionType.BT_IAP2_LINK.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$navdy$service$library$device$connection$ConnectionType[ConnectionType.TCP_PROTOBUF.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    public enum ConnectionFailureCause {
        UNKNOWN,
        CONNECTION_REFUSED,
        CONNECTION_TIMED_OUT
    }

    public enum DisconnectCause {
        UNKNOWN,
        NORMAL,
        ABORTED
    }

    public enum Status {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTING
    }

    public abstract boolean connect();

    public abstract boolean disconnect();

    public abstract SocketAdapter getSocket();

    public static Connection instantiateFromConnectionInfo(Context context, ConnectionInfo connectionInfo) {
        ConnectionFactory factory = (ConnectionFactory) factoryMap.get(connectionInfo.getType());
        if (factory != null) {
            return factory.build(context, connectionInfo);
        }
        throw new IllegalArgumentException("Unknown connection class for type: " + connectionInfo.getType());
    }

    static {
        registerConnectionType(ConnectionType.BT_PROTOBUF, sDefaultFactory);
        registerConnectionType(ConnectionType.BT_TUNNEL, sDefaultFactory);
        registerConnectionType(ConnectionType.TCP_PROTOBUF, sDefaultFactory);
        registerConnectionType(ConnectionType.BT_IAP2_LINK, sDefaultFactory);
        registerConnectionType(ConnectionType.EA_PROTOBUF, sDefaultFactory);
    }

    public static void registerConnectionType(ConnectionType type, ConnectionFactory factory) {
        factoryMap.put(type, factory);
    }

    public Connection(ConnectionInfo connectionInfo) {
        this.mConnectionInfo = connectionInfo;
        this.mStatus = Status.DISCONNECTED;
    }

    public Status getStatus() {
        return this.mStatus;
    }

    public ConnectionType getType() {
        return this.mConnectionInfo.getType();
    }

    public ConnectionInfo getConnectionInfo() {
        return this.mConnectionInfo;
    }

    protected void dispatchConnectEvent() {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(Connection source, Listener listener) {
                listener.onConnected(source);
            }
        });
    }

    protected void dispatchConnectionFailedEvent(final ConnectionFailureCause cause) {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(Connection source, Listener listener) {
                listener.onConnectionFailed(source, cause);
            }
        });
    }

    protected void dispatchDisconnectEvent(final DisconnectCause cause) {
        dispatchToListeners(new EventDispatcher() {
            public void dispatchEvent(Connection source, Listener listener) {
                listener.onDisconnected(source, cause);
            }
        });
    }
}
