package com.navdy.service.library.device.connection;

public abstract class ProxyService extends Thread {
    public abstract void cancel();
}
