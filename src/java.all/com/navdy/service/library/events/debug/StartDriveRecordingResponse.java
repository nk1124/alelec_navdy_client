package com.navdy.service.library.events.debug;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class StartDriveRecordingResponse extends Message {
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;

    public static final class Builder extends com.squareup.wire.Message.Builder<StartDriveRecordingResponse> {
        public RequestStatus status;

        public Builder(StartDriveRecordingResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public StartDriveRecordingResponse build() {
            checkRequiredFields();
            return new StartDriveRecordingResponse(this);
        }
    }

    public StartDriveRecordingResponse(RequestStatus status) {
        this.status = status;
    }

    private StartDriveRecordingResponse(Builder builder) {
        this(builder.status);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof StartDriveRecordingResponse) {
            return equals((Object) this.status, (Object) ((StartDriveRecordingResponse) other).status);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.status != null ? this.status.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
