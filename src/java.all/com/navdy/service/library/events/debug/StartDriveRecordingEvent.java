package com.navdy.service.library.events.debug;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class StartDriveRecordingEvent extends Message {
    public static final String DEFAULT_LABEL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String label;

    public static final class Builder extends com.squareup.wire.Message.Builder<StartDriveRecordingEvent> {
        public String label;

        public Builder(StartDriveRecordingEvent message) {
            super(message);
            if (message != null) {
                this.label = message.label;
            }
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public StartDriveRecordingEvent build() {
            checkRequiredFields();
            return new StartDriveRecordingEvent(this);
        }
    }

    public StartDriveRecordingEvent(String label) {
        this.label = label;
    }

    private StartDriveRecordingEvent(Builder builder) {
        this(builder.label);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof StartDriveRecordingEvent) {
            return equals((Object) this.label, (Object) ((StartDriveRecordingEvent) other).label);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.label != null ? this.label.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
