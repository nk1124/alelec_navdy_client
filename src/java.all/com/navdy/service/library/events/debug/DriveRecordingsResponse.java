package com.navdy.service.library.events.debug;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class DriveRecordingsResponse extends Message {
    public static final List<String> DEFAULT_RECORDINGS = Collections.emptyList();
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, tag = 1, type = Datatype.STRING)
    public final List<String> recordings;

    public static final class Builder extends com.squareup.wire.Message.Builder<DriveRecordingsResponse> {
        public List<String> recordings;

        public Builder(DriveRecordingsResponse message) {
            super(message);
            if (message != null) {
                this.recordings = Message.copyOf(message.recordings);
            }
        }

        public Builder recordings(List<String> recordings) {
            this.recordings = com.squareup.wire.Message.Builder.checkForNulls(recordings);
            return this;
        }

        public DriveRecordingsResponse build() {
            return new DriveRecordingsResponse(this);
        }
    }

    public DriveRecordingsResponse(List<String> recordings) {
        this.recordings = Message.immutableCopyOf(recordings);
    }

    private DriveRecordingsResponse(Builder builder) {
        this(builder.recordings);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof DriveRecordingsResponse) {
            return equals(this.recordings, ((DriveRecordingsResponse) other).recordings);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.recordings != null ? this.recordings.hashCode() : 1;
        this.hashCode = hashCode;
        return hashCode;
    }
}
