package com.navdy.service.library.events;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class DeviceInfo extends Message {
    public static final String DEFAULT_BUILDTYPE = "user";
    public static final String DEFAULT_CLIENTVERSION = "";
    public static final String DEFAULT_DEVICEID = "";
    public static final String DEFAULT_DEVICEMAKE = "unknown";
    public static final String DEFAULT_DEVICENAME = "";
    public static final String DEFAULT_DEVICEUUID = "";
    public static final Boolean DEFAULT_FORCEFULLUPDATE = Boolean.valueOf(false);
    public static final String DEFAULT_KERNELVERSION = "";
    public static final List<LegacyCapability> DEFAULT_LEGACYCAPABILITIES = Collections.emptyList();
    public static final String DEFAULT_MODEL = "";
    public static final List<String> DEFAULT_MUSICPLAYERS_OBSOLETE = Collections.emptyList();
    public static final Platform DEFAULT_PLATFORM = Platform.PLATFORM_iOS;
    public static final String DEFAULT_PROTOCOLVERSION = "";
    public static final Integer DEFAULT_SYSTEMAPILEVEL = Integer.valueOf(0);
    public static final String DEFAULT_SYSTEMVERSION = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 11, type = Datatype.STRING)
    public final String buildType;
    @ProtoField(tag = 16)
    public final Capabilities capabilities;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.STRING)
    public final String clientVersion;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String deviceId;
    @ProtoField(tag = 12, type = Datatype.STRING)
    public final String deviceMake;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.STRING)
    public final String deviceName;
    @ProtoField(label = Label.REQUIRED, tag = 7, type = Datatype.STRING)
    public final String deviceUuid;
    @ProtoField(tag = 13, type = Datatype.BOOL)
    public final Boolean forceFullUpdate;
    @ProtoField(tag = 9, type = Datatype.STRING)
    public final String kernelVersion;
    @ProtoField(deprecated = true, enumType = LegacyCapability.class, label = Label.REPEATED, tag = 14, type = Datatype.ENUM)
    @Deprecated
    public final List<LegacyCapability> legacyCapabilities;
    @ProtoField(label = Label.REQUIRED, tag = 6, type = Datatype.STRING)
    public final String model;
    @ProtoField(deprecated = true, label = Label.REPEATED, tag = 15, type = Datatype.STRING)
    @Deprecated
    public final List<String> musicPlayers_OBSOLETE;
    @ProtoField(tag = 10, type = Datatype.ENUM)
    public final Platform platform;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.STRING)
    public final String protocolVersion;
    @ProtoField(tag = 8, type = Datatype.INT32)
    public final Integer systemApiLevel;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.STRING)
    public final String systemVersion;

    public static final class Builder extends com.squareup.wire.Message.Builder<DeviceInfo> {
        public String buildType;
        public Capabilities capabilities;
        public String clientVersion;
        public String deviceId;
        public String deviceMake;
        public String deviceName;
        public String deviceUuid;
        public Boolean forceFullUpdate;
        public String kernelVersion;
        public List<LegacyCapability> legacyCapabilities;
        public String model;
        public List<String> musicPlayers_OBSOLETE;
        public Platform platform;
        public String protocolVersion;
        public Integer systemApiLevel;
        public String systemVersion;

        public Builder(DeviceInfo message) {
            super(message);
            if (message != null) {
                this.deviceId = message.deviceId;
                this.clientVersion = message.clientVersion;
                this.protocolVersion = message.protocolVersion;
                this.deviceName = message.deviceName;
                this.systemVersion = message.systemVersion;
                this.model = message.model;
                this.deviceUuid = message.deviceUuid;
                this.systemApiLevel = message.systemApiLevel;
                this.kernelVersion = message.kernelVersion;
                this.platform = message.platform;
                this.buildType = message.buildType;
                this.deviceMake = message.deviceMake;
                this.forceFullUpdate = message.forceFullUpdate;
                this.legacyCapabilities = Message.copyOf(message.legacyCapabilities);
                this.musicPlayers_OBSOLETE = Message.copyOf(message.musicPlayers_OBSOLETE);
                this.capabilities = message.capabilities;
            }
        }

        public Builder deviceId(String deviceId) {
            this.deviceId = deviceId;
            return this;
        }

        public Builder clientVersion(String clientVersion) {
            this.clientVersion = clientVersion;
            return this;
        }

        public Builder protocolVersion(String protocolVersion) {
            this.protocolVersion = protocolVersion;
            return this;
        }

        public Builder deviceName(String deviceName) {
            this.deviceName = deviceName;
            return this;
        }

        public Builder systemVersion(String systemVersion) {
            this.systemVersion = systemVersion;
            return this;
        }

        public Builder model(String model) {
            this.model = model;
            return this;
        }

        public Builder deviceUuid(String deviceUuid) {
            this.deviceUuid = deviceUuid;
            return this;
        }

        public Builder systemApiLevel(Integer systemApiLevel) {
            this.systemApiLevel = systemApiLevel;
            return this;
        }

        public Builder kernelVersion(String kernelVersion) {
            this.kernelVersion = kernelVersion;
            return this;
        }

        public Builder platform(Platform platform) {
            this.platform = platform;
            return this;
        }

        public Builder buildType(String buildType) {
            this.buildType = buildType;
            return this;
        }

        public Builder deviceMake(String deviceMake) {
            this.deviceMake = deviceMake;
            return this;
        }

        public Builder forceFullUpdate(Boolean forceFullUpdate) {
            this.forceFullUpdate = forceFullUpdate;
            return this;
        }

        @Deprecated
        public Builder legacyCapabilities(List<LegacyCapability> legacyCapabilities) {
            this.legacyCapabilities = com.squareup.wire.Message.Builder.checkForNulls(legacyCapabilities);
            return this;
        }

        @Deprecated
        public Builder musicPlayers_OBSOLETE(List<String> musicPlayers_OBSOLETE) {
            this.musicPlayers_OBSOLETE = com.squareup.wire.Message.Builder.checkForNulls(musicPlayers_OBSOLETE);
            return this;
        }

        public Builder capabilities(Capabilities capabilities) {
            this.capabilities = capabilities;
            return this;
        }

        public DeviceInfo build() {
            checkRequiredFields();
            return new DeviceInfo(this);
        }
    }

    public enum Platform implements ProtoEnum {
        PLATFORM_iOS(1),
        PLATFORM_Android(2);
        
        private final int value;

        private Platform(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DeviceInfo(String deviceId, String clientVersion, String protocolVersion, String deviceName, String systemVersion, String model, String deviceUuid, Integer systemApiLevel, String kernelVersion, Platform platform, String buildType, String deviceMake, Boolean forceFullUpdate, List<LegacyCapability> legacyCapabilities, List<String> musicPlayers_OBSOLETE, Capabilities capabilities) {
        this.deviceId = deviceId;
        this.clientVersion = clientVersion;
        this.protocolVersion = protocolVersion;
        this.deviceName = deviceName;
        this.systemVersion = systemVersion;
        this.model = model;
        this.deviceUuid = deviceUuid;
        this.systemApiLevel = systemApiLevel;
        this.kernelVersion = kernelVersion;
        this.platform = platform;
        this.buildType = buildType;
        this.deviceMake = deviceMake;
        this.forceFullUpdate = forceFullUpdate;
        this.legacyCapabilities = Message.immutableCopyOf(legacyCapabilities);
        this.musicPlayers_OBSOLETE = Message.immutableCopyOf(musicPlayers_OBSOLETE);
        this.capabilities = capabilities;
    }

    private DeviceInfo(Builder builder) {
        this(builder.deviceId, builder.clientVersion, builder.protocolVersion, builder.deviceName, builder.systemVersion, builder.model, builder.deviceUuid, builder.systemApiLevel, builder.kernelVersion, builder.platform, builder.buildType, builder.deviceMake, builder.forceFullUpdate, builder.legacyCapabilities, builder.musicPlayers_OBSOLETE, builder.capabilities);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof DeviceInfo)) {
            return false;
        }
        DeviceInfo o = (DeviceInfo) other;
        if (equals((Object) this.deviceId, (Object) o.deviceId) && equals((Object) this.clientVersion, (Object) o.clientVersion) && equals((Object) this.protocolVersion, (Object) o.protocolVersion) && equals((Object) this.deviceName, (Object) o.deviceName) && equals((Object) this.systemVersion, (Object) o.systemVersion) && equals((Object) this.model, (Object) o.model) && equals((Object) this.deviceUuid, (Object) o.deviceUuid) && equals((Object) this.systemApiLevel, (Object) o.systemApiLevel) && equals((Object) this.kernelVersion, (Object) o.kernelVersion) && equals((Object) this.platform, (Object) o.platform) && equals((Object) this.buildType, (Object) o.buildType) && equals((Object) this.deviceMake, (Object) o.deviceMake) && equals((Object) this.forceFullUpdate, (Object) o.forceFullUpdate) && equals(this.legacyCapabilities, o.legacyCapabilities) && equals(this.musicPlayers_OBSOLETE, o.musicPlayers_OBSOLETE) && equals((Object) this.capabilities, (Object) o.capabilities)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 1;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.deviceId != null ? this.deviceId.hashCode() : 0) * 37;
        if (this.clientVersion != null) {
            hashCode = this.clientVersion.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.protocolVersion != null) {
            hashCode = this.protocolVersion.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.deviceName != null) {
            hashCode = this.deviceName.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.systemVersion != null) {
            hashCode = this.systemVersion.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.model != null) {
            hashCode = this.model.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.deviceUuid != null) {
            hashCode = this.deviceUuid.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.systemApiLevel != null) {
            hashCode = this.systemApiLevel.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.kernelVersion != null) {
            hashCode = this.kernelVersion.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.platform != null) {
            hashCode = this.platform.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.buildType != null) {
            hashCode = this.buildType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.deviceMake != null) {
            hashCode = this.deviceMake.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.forceFullUpdate != null) {
            hashCode = this.forceFullUpdate.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.legacyCapabilities != null) {
            hashCode = this.legacyCapabilities.hashCode();
        } else {
            hashCode = 1;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.musicPlayers_OBSOLETE != null) {
            i = this.musicPlayers_OBSOLETE.hashCode();
        }
        hashCode = (hashCode + i) * 37;
        if (this.capabilities != null) {
            i2 = this.capabilities.hashCode();
        }
        result = hashCode + i2;
        this.hashCode = result;
        return result;
    }
}
