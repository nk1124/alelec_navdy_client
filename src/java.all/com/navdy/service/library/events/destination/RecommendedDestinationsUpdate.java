package com.navdy.service.library.events.destination;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class RecommendedDestinationsUpdate extends Message {
    public static final List<Destination> DEFAULT_DESTINATIONS = Collections.emptyList();
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = Destination.class, tag = 4)
    public final List<Destination> destinations;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<RecommendedDestinationsUpdate> {
        public List<Destination> destinations;
        public Long serial_number;
        public RequestStatus status;
        public String statusDetail;

        public Builder(RecommendedDestinationsUpdate message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.serial_number = message.serial_number;
                this.destinations = Message.copyOf(message.destinations);
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public Builder destinations(List<Destination> destinations) {
            this.destinations = com.squareup.wire.Message.Builder.checkForNulls(destinations);
            return this;
        }

        public RecommendedDestinationsUpdate build() {
            checkRequiredFields();
            return new RecommendedDestinationsUpdate(this);
        }
    }

    public RecommendedDestinationsUpdate(RequestStatus status, String statusDetail, Long serial_number, List<Destination> destinations) {
        this.status = status;
        this.statusDetail = statusDetail;
        this.serial_number = serial_number;
        this.destinations = Message.immutableCopyOf(destinations);
    }

    private RecommendedDestinationsUpdate(Builder builder) {
        this(builder.status, builder.statusDetail, builder.serial_number, builder.destinations);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof RecommendedDestinationsUpdate)) {
            return false;
        }
        RecommendedDestinationsUpdate o = (RecommendedDestinationsUpdate) other;
        if (equals((Object) this.status, (Object) o.status) && equals((Object) this.statusDetail, (Object) o.statusDetail) && equals((Object) this.serial_number, (Object) o.serial_number) && equals(this.destinations, o.destinations)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.statusDetail != null) {
            hashCode = this.statusDetail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.serial_number != null) {
            i = this.serial_number.hashCode();
        }
        result = ((hashCode + i) * 37) + (this.destinations != null ? this.destinations.hashCode() : 1);
        this.hashCode = result;
        return result;
    }
}
