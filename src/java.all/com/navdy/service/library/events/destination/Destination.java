package com.navdy.service.library.events.destination;

import com.navdy.service.library.events.contacts.Contact;
import com.navdy.service.library.events.contacts.PhoneNumber;
import com.navdy.service.library.events.location.LatLong;
import com.navdy.service.library.events.places.PlaceType;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class Destination extends Message {
    public static final List<Contact> DEFAULT_CONTACTS = Collections.emptyList();
    public static final String DEFAULT_DESTINATIONDISTANCE = "";
    public static final Integer DEFAULT_DESTINATIONICON = Integer.valueOf(0);
    public static final Integer DEFAULT_DESTINATIONICONBKCOLOR = Integer.valueOf(0);
    public static final String DEFAULT_DESTINATION_SUBTITLE = "";
    public static final String DEFAULT_DESTINATION_TITLE = "";
    public static final FavoriteType DEFAULT_FAVORITE_TYPE = FavoriteType.FAVORITE_NONE;
    public static final String DEFAULT_FULL_ADDRESS = "";
    public static final String DEFAULT_IDENTIFIER = "";
    public static final Boolean DEFAULT_IS_RECOMMENDATION = Boolean.valueOf(false);
    public static final Long DEFAULT_LAST_NAVIGATED_TO = Long.valueOf(0);
    public static final List<PhoneNumber> DEFAULT_PHONENUMBERS = Collections.emptyList();
    public static final String DEFAULT_PLACE_ID = "";
    public static final PlaceType DEFAULT_PLACE_TYPE = PlaceType.PLACE_TYPE_UNKNOWN;
    public static final SuggestionType DEFAULT_SUGGESTION_TYPE = SuggestionType.SUGGESTION_NONE;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REPEATED, messageType = Contact.class, tag = 18)
    public final List<Contact> contacts;
    @ProtoField(tag = 16, type = Datatype.STRING)
    public final String destinationDistance;
    @ProtoField(tag = 13, type = Datatype.INT32)
    public final Integer destinationIcon;
    @ProtoField(tag = 14, type = Datatype.INT32)
    public final Integer destinationIconBkColor;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.STRING)
    public final String destination_subtitle;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.STRING)
    public final String destination_title;
    @ProtoField(tag = 2)
    public final LatLong display_position;
    @ProtoField(label = Label.REQUIRED, tag = 7, type = Datatype.ENUM)
    public final FavoriteType favorite_type;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.STRING)
    public final String full_address;
    @ProtoField(label = Label.REQUIRED, tag = 8, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 10, type = Datatype.BOOL)
    public final Boolean is_recommendation;
    @ProtoField(tag = 11, type = Datatype.INT64)
    public final Long last_navigated_to;
    @ProtoField(label = Label.REQUIRED, tag = 1)
    public final LatLong navigation_position;
    @ProtoField(label = Label.REPEATED, messageType = PhoneNumber.class, tag = 17)
    public final List<PhoneNumber> phoneNumbers;
    @ProtoField(tag = 15, type = Datatype.STRING)
    public final String place_id;
    @ProtoField(tag = 12, type = Datatype.ENUM)
    public final PlaceType place_type;
    @ProtoField(tag = 9, type = Datatype.ENUM)
    public final SuggestionType suggestion_type;

    public static final class Builder extends com.squareup.wire.Message.Builder<Destination> {
        public List<Contact> contacts;
        public String destinationDistance;
        public Integer destinationIcon;
        public Integer destinationIconBkColor;
        public String destination_subtitle;
        public String destination_title;
        public LatLong display_position;
        public FavoriteType favorite_type;
        public String full_address;
        public String identifier;
        public Boolean is_recommendation;
        public Long last_navigated_to;
        public LatLong navigation_position;
        public List<PhoneNumber> phoneNumbers;
        public String place_id;
        public PlaceType place_type;
        public SuggestionType suggestion_type;

        public Builder(Destination message) {
            super(message);
            if (message != null) {
                this.navigation_position = message.navigation_position;
                this.display_position = message.display_position;
                this.full_address = message.full_address;
                this.destination_title = message.destination_title;
                this.destination_subtitle = message.destination_subtitle;
                this.favorite_type = message.favorite_type;
                this.identifier = message.identifier;
                this.suggestion_type = message.suggestion_type;
                this.is_recommendation = message.is_recommendation;
                this.last_navigated_to = message.last_navigated_to;
                this.place_type = message.place_type;
                this.destinationIcon = message.destinationIcon;
                this.destinationIconBkColor = message.destinationIconBkColor;
                this.place_id = message.place_id;
                this.destinationDistance = message.destinationDistance;
                this.phoneNumbers = Message.copyOf(message.phoneNumbers);
                this.contacts = Message.copyOf(message.contacts);
            }
        }

        public Builder navigation_position(LatLong navigation_position) {
            this.navigation_position = navigation_position;
            return this;
        }

        public Builder display_position(LatLong display_position) {
            this.display_position = display_position;
            return this;
        }

        public Builder full_address(String full_address) {
            this.full_address = full_address;
            return this;
        }

        public Builder destination_title(String destination_title) {
            this.destination_title = destination_title;
            return this;
        }

        public Builder destination_subtitle(String destination_subtitle) {
            this.destination_subtitle = destination_subtitle;
            return this;
        }

        public Builder favorite_type(FavoriteType favorite_type) {
            this.favorite_type = favorite_type;
            return this;
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder suggestion_type(SuggestionType suggestion_type) {
            this.suggestion_type = suggestion_type;
            return this;
        }

        public Builder is_recommendation(Boolean is_recommendation) {
            this.is_recommendation = is_recommendation;
            return this;
        }

        public Builder last_navigated_to(Long last_navigated_to) {
            this.last_navigated_to = last_navigated_to;
            return this;
        }

        public Builder place_type(PlaceType place_type) {
            this.place_type = place_type;
            return this;
        }

        public Builder destinationIcon(Integer destinationIcon) {
            this.destinationIcon = destinationIcon;
            return this;
        }

        public Builder destinationIconBkColor(Integer destinationIconBkColor) {
            this.destinationIconBkColor = destinationIconBkColor;
            return this;
        }

        public Builder place_id(String place_id) {
            this.place_id = place_id;
            return this;
        }

        public Builder destinationDistance(String destinationDistance) {
            this.destinationDistance = destinationDistance;
            return this;
        }

        public Builder phoneNumbers(List<PhoneNumber> phoneNumbers) {
            this.phoneNumbers = com.squareup.wire.Message.Builder.checkForNulls(phoneNumbers);
            return this;
        }

        public Builder contacts(List<Contact> contacts) {
            this.contacts = com.squareup.wire.Message.Builder.checkForNulls(contacts);
            return this;
        }

        public Destination build() {
            checkRequiredFields();
            return new Destination(this);
        }
    }

    public enum FavoriteType implements ProtoEnum {
        FAVORITE_NONE(0),
        FAVORITE_HOME(1),
        FAVORITE_WORK(2),
        FAVORITE_CONTACT(3),
        FAVORITE_CUSTOM(10);
        
        private final int value;

        private FavoriteType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public enum SuggestionType implements ProtoEnum {
        SUGGESTION_NONE(0),
        SUGGESTION_CALENDAR(1),
        SUGGESTION_RECENT(2);
        
        private final int value;

        private SuggestionType(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public Destination(LatLong navigation_position, LatLong display_position, String full_address, String destination_title, String destination_subtitle, FavoriteType favorite_type, String identifier, SuggestionType suggestion_type, Boolean is_recommendation, Long last_navigated_to, PlaceType place_type, Integer destinationIcon, Integer destinationIconBkColor, String place_id, String destinationDistance, List<PhoneNumber> phoneNumbers, List<Contact> contacts) {
        this.navigation_position = navigation_position;
        this.display_position = display_position;
        this.full_address = full_address;
        this.destination_title = destination_title;
        this.destination_subtitle = destination_subtitle;
        this.favorite_type = favorite_type;
        this.identifier = identifier;
        this.suggestion_type = suggestion_type;
        this.is_recommendation = is_recommendation;
        this.last_navigated_to = last_navigated_to;
        this.place_type = place_type;
        this.destinationIcon = destinationIcon;
        this.destinationIconBkColor = destinationIconBkColor;
        this.place_id = place_id;
        this.destinationDistance = destinationDistance;
        this.phoneNumbers = Message.immutableCopyOf(phoneNumbers);
        this.contacts = Message.immutableCopyOf(contacts);
    }

    private Destination(Builder builder) {
        this(builder.navigation_position, builder.display_position, builder.full_address, builder.destination_title, builder.destination_subtitle, builder.favorite_type, builder.identifier, builder.suggestion_type, builder.is_recommendation, builder.last_navigated_to, builder.place_type, builder.destinationIcon, builder.destinationIconBkColor, builder.place_id, builder.destinationDistance, builder.phoneNumbers, builder.contacts);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof Destination)) {
            return false;
        }
        Destination o = (Destination) other;
        if (equals((Object) this.navigation_position, (Object) o.navigation_position) && equals((Object) this.display_position, (Object) o.display_position) && equals((Object) this.full_address, (Object) o.full_address) && equals((Object) this.destination_title, (Object) o.destination_title) && equals((Object) this.destination_subtitle, (Object) o.destination_subtitle) && equals((Object) this.favorite_type, (Object) o.favorite_type) && equals((Object) this.identifier, (Object) o.identifier) && equals((Object) this.suggestion_type, (Object) o.suggestion_type) && equals((Object) this.is_recommendation, (Object) o.is_recommendation) && equals((Object) this.last_navigated_to, (Object) o.last_navigated_to) && equals((Object) this.place_type, (Object) o.place_type) && equals((Object) this.destinationIcon, (Object) o.destinationIcon) && equals((Object) this.destinationIconBkColor, (Object) o.destinationIconBkColor) && equals((Object) this.place_id, (Object) o.place_id) && equals((Object) this.destinationDistance, (Object) o.destinationDistance) && equals(this.phoneNumbers, o.phoneNumbers) && equals(this.contacts, o.contacts)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 1;
        int i2 = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.navigation_position != null ? this.navigation_position.hashCode() : 0) * 37;
        if (this.display_position != null) {
            hashCode = this.display_position.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.full_address != null) {
            hashCode = this.full_address.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destination_title != null) {
            hashCode = this.destination_title.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destination_subtitle != null) {
            hashCode = this.destination_subtitle.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.favorite_type != null) {
            hashCode = this.favorite_type.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.identifier != null) {
            hashCode = this.identifier.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.suggestion_type != null) {
            hashCode = this.suggestion_type.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.is_recommendation != null) {
            hashCode = this.is_recommendation.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.last_navigated_to != null) {
            hashCode = this.last_navigated_to.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.place_type != null) {
            hashCode = this.place_type.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destinationIcon != null) {
            hashCode = this.destinationIcon.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destinationIconBkColor != null) {
            hashCode = this.destinationIconBkColor.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.place_id != null) {
            hashCode = this.place_id.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.destinationDistance != null) {
            i2 = this.destinationDistance.hashCode();
        }
        hashCode = (hashCode + i2) * 37;
        if (this.phoneNumbers != null) {
            i2 = this.phoneNumbers.hashCode();
        } else {
            i2 = 1;
        }
        i2 = (hashCode + i2) * 37;
        if (this.contacts != null) {
            i = this.contacts.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
