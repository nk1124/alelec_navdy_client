package com.navdy.service.library.events;

import com.navdy.service.library.log.Logger;
import com.squareup.wire.Wire;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class NavdyEventReader {
    private static final int FRAME_LENGTH = 5;
    private static final Logger sLogger = new Logger(NavdyEventReader.class);
    protected DataInputStream mInputStream;
    protected Wire mWire = new Wire(Ext_NavdyEvent.class);

    public NavdyEventReader(InputStream input) {
        this.mInputStream = new DataInputStream(input);
    }

    public byte[] readBytes() throws IOException {
        int size = readSize();
        if (size <= 0) {
            return null;
        }
        return readBytes(size);
    }

    private int readSize() throws IOException {
        try {
            Frame frame = null;
            try {
                frame = (Frame) this.mWire.parseFrom(readBytes(5), Frame.class);
            } catch (Throwable t) {
                sLogger.e("Failed to parse frame", t);
            }
            if (frame == null) {
                return 0;
            }
            return frame.size.intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    private byte[] readBytes(int size) throws IOException {
        byte[] buf = new byte[size];
        this.mInputStream.readFully(buf);
        return buf;
    }
}
