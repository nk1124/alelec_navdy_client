package com.navdy.service.library.events.photo;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import okio.ByteString;

public final class PhotoResponse extends Message {
    public static final String DEFAULT_IDENTIFIER = "";
    public static final ByteString DEFAULT_PHOTO = ByteString.EMPTY;
    public static final String DEFAULT_PHOTOCHECKSUM = "";
    public static final PhotoType DEFAULT_PHOTOTYPE = PhotoType.PHOTO_CONTACT;
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_STATUSDETAIL = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 1, type = Datatype.BYTES)
    public final ByteString photo;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String photoChecksum;
    @ProtoField(tag = 6, type = Datatype.ENUM)
    public final PhotoType photoType;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String statusDetail;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhotoResponse> {
        public String identifier;
        public ByteString photo;
        public String photoChecksum;
        public PhotoType photoType;
        public RequestStatus status;
        public String statusDetail;

        public Builder(PhotoResponse message) {
            super(message);
            if (message != null) {
                this.photo = message.photo;
                this.status = message.status;
                this.statusDetail = message.statusDetail;
                this.identifier = message.identifier;
                this.photoChecksum = message.photoChecksum;
                this.photoType = message.photoType;
            }
        }

        public Builder photo(ByteString photo) {
            this.photo = photo;
            return this;
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder statusDetail(String statusDetail) {
            this.statusDetail = statusDetail;
            return this;
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder photoChecksum(String photoChecksum) {
            this.photoChecksum = photoChecksum;
            return this;
        }

        public Builder photoType(PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }

        public PhotoResponse build() {
            checkRequiredFields();
            return new PhotoResponse(this);
        }
    }

    public PhotoResponse(ByteString photo, RequestStatus status, String statusDetail, String identifier, String photoChecksum, PhotoType photoType) {
        this.photo = photo;
        this.status = status;
        this.statusDetail = statusDetail;
        this.identifier = identifier;
        this.photoChecksum = photoChecksum;
        this.photoType = photoType;
    }

    private PhotoResponse(Builder builder) {
        this(builder.photo, builder.status, builder.statusDetail, builder.identifier, builder.photoChecksum, builder.photoType);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhotoResponse)) {
            return false;
        }
        PhotoResponse o = (PhotoResponse) other;
        if (equals((Object) this.photo, (Object) o.photo) && equals((Object) this.status, (Object) o.status) && equals((Object) this.statusDetail, (Object) o.statusDetail) && equals((Object) this.identifier, (Object) o.identifier) && equals((Object) this.photoChecksum, (Object) o.photoChecksum) && equals((Object) this.photoType, (Object) o.photoType)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.photo != null ? this.photo.hashCode() : 0) * 37;
        if (this.status != null) {
            hashCode = this.status.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.statusDetail != null) {
            hashCode = this.statusDetail.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.identifier != null) {
            hashCode = this.identifier.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.photoChecksum != null) {
            hashCode = this.photoChecksum.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.photoType != null) {
            i = this.photoType.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
