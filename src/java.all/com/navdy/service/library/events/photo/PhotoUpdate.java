package com.navdy.service.library.events.photo;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import okio.ByteString;

public final class PhotoUpdate extends Message {
    public static final String DEFAULT_IDENTIFIER = "";
    public static final ByteString DEFAULT_PHOTO = ByteString.EMPTY;
    public static final PhotoType DEFAULT_PHOTOTYPE = PhotoType.PHOTO_ALBUM_ART;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String identifier;
    @ProtoField(tag = 2, type = Datatype.BYTES)
    public final ByteString photo;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final PhotoType photoType;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhotoUpdate> {
        public String identifier;
        public ByteString photo;
        public PhotoType photoType;

        public Builder(PhotoUpdate message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
                this.photo = message.photo;
                this.photoType = message.photoType;
            }
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder photo(ByteString photo) {
            this.photo = photo;
            return this;
        }

        public Builder photoType(PhotoType photoType) {
            this.photoType = photoType;
            return this;
        }

        public PhotoUpdate build() {
            checkRequiredFields();
            return new PhotoUpdate(this);
        }
    }

    public PhotoUpdate(String identifier, ByteString photo, PhotoType photoType) {
        this.identifier = identifier;
        this.photo = photo;
        this.photoType = photoType;
    }

    private PhotoUpdate(Builder builder) {
        this(builder.identifier, builder.photo, builder.photoType);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhotoUpdate)) {
            return false;
        }
        PhotoUpdate o = (PhotoUpdate) other;
        if (equals((Object) this.identifier, (Object) o.identifier) && equals((Object) this.photo, (Object) o.photo) && equals((Object) this.photoType, (Object) o.photoType)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.identifier != null ? this.identifier.hashCode() : 0) * 37;
        if (this.photo != null) {
            hashCode = this.photo.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.photoType != null) {
            i = this.photoType.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
