package com.navdy.service.library.events.photo;

import com.squareup.wire.ProtoEnum;

public enum PhotoType implements ProtoEnum {
    PHOTO_CONTACT(0),
    PHOTO_ALBUM_ART(1),
    PHOTO_DRIVER_PROFILE(2);
    
    private final int value;

    private PhotoType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
