package com.navdy.service.library.events.contacts;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class FavoriteContactsRequest extends Message {
    public static final Integer DEFAULT_MAXCONTACTS = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.INT32)
    public final Integer maxContacts;

    public static final class Builder extends com.squareup.wire.Message.Builder<FavoriteContactsRequest> {
        public Integer maxContacts;

        public Builder(FavoriteContactsRequest message) {
            super(message);
            if (message != null) {
                this.maxContacts = message.maxContacts;
            }
        }

        public Builder maxContacts(Integer maxContacts) {
            this.maxContacts = maxContacts;
            return this;
        }

        public FavoriteContactsRequest build() {
            return new FavoriteContactsRequest(this);
        }
    }

    public FavoriteContactsRequest(Integer maxContacts) {
        this.maxContacts = maxContacts;
    }

    private FavoriteContactsRequest(Builder builder) {
        this(builder.maxContacts);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof FavoriteContactsRequest) {
            return equals((Object) this.maxContacts, (Object) ((FavoriteContactsRequest) other).maxContacts);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.maxContacts != null ? this.maxContacts.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
