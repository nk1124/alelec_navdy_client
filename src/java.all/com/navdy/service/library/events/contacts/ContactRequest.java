package com.navdy.service.library.events.contacts;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class ContactRequest extends Message {
    public static final String DEFAULT_IDENTIFIER = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String identifier;

    public static final class Builder extends com.squareup.wire.Message.Builder<ContactRequest> {
        public String identifier;

        public Builder(ContactRequest message) {
            super(message);
            if (message != null) {
                this.identifier = message.identifier;
            }
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public ContactRequest build() {
            return new ContactRequest(this);
        }
    }

    public ContactRequest(String identifier) {
        this.identifier = identifier;
    }

    private ContactRequest(Builder builder) {
        this(builder.identifier);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof ContactRequest) {
            return equals((Object) this.identifier, (Object) ((ContactRequest) other).identifier);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.identifier != null ? this.identifier.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
