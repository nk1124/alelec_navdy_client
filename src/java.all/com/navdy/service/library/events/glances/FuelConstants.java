package com.navdy.service.library.events.glances;

import com.squareup.wire.ProtoEnum;

public enum FuelConstants implements ProtoEnum {
    FUEL_LEVEL(0),
    NO_ROUTE(1),
    GAS_STATION_NAME(2),
    GAS_STATION_ADDRESS(3),
    GAS_STATION_DISTANCE(4);
    
    private final int value;

    private FuelConstants(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
