package com.navdy.service.library.events.connection;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class ConnectionStateChange extends Message {
    public static final String DEFAULT_REMOTEDEVICEID = "";
    public static final ConnectionState DEFAULT_STATE = ConnectionState.CONNECTION_CONNECTED;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String remoteDeviceId;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.ENUM)
    public final ConnectionState state;

    public static final class Builder extends com.squareup.wire.Message.Builder<ConnectionStateChange> {
        public String remoteDeviceId;
        public ConnectionState state;

        public Builder(ConnectionStateChange message) {
            super(message);
            if (message != null) {
                this.remoteDeviceId = message.remoteDeviceId;
                this.state = message.state;
            }
        }

        public Builder remoteDeviceId(String remoteDeviceId) {
            this.remoteDeviceId = remoteDeviceId;
            return this;
        }

        public Builder state(ConnectionState state) {
            this.state = state;
            return this;
        }

        public ConnectionStateChange build() {
            checkRequiredFields();
            return new ConnectionStateChange(this, null);
        }
    }

    public enum ConnectionState implements ProtoEnum {
        CONNECTION_CONNECTED(1),
        CONNECTION_DISCONNECTED(2),
        CONNECTION_LINK_ESTABLISHED(3),
        CONNECTION_LINK_LOST(4),
        CONNECTION_VERIFIED(5);
        
        private final int value;

        private ConnectionState(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public ConnectionStateChange(String remoteDeviceId, ConnectionState state) {
        this.remoteDeviceId = remoteDeviceId;
        this.state = state;
    }

    private ConnectionStateChange(Builder builder) {
        this(builder.remoteDeviceId, builder.state);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ConnectionStateChange)) {
            return false;
        }
        ConnectionStateChange o = (ConnectionStateChange) other;
        if (equals((Object) this.remoteDeviceId, (Object) o.remoteDeviceId) && equals((Object) this.state, (Object) o.state)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.remoteDeviceId != null) {
            result = this.remoteDeviceId.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.state != null) {
            i = this.state.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
