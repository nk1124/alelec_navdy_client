package com.navdy.service.library.events.connection;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class ConnectionStatus extends Message {
    public static final String DEFAULT_REMOTEDEVICEID = "";
    public static final Status DEFAULT_STATUS = Status.CONNECTION_PAIRING;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String remoteDeviceId;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Status status;

    public static final class Builder extends com.squareup.wire.Message.Builder<ConnectionStatus> {
        public String remoteDeviceId;
        public Status status;

        public Builder(ConnectionStatus message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.remoteDeviceId = message.remoteDeviceId;
            }
        }

        public Builder status(Status status) {
            this.status = status;
            return this;
        }

        public Builder remoteDeviceId(String remoteDeviceId) {
            this.remoteDeviceId = remoteDeviceId;
            return this;
        }

        public ConnectionStatus build() {
            checkRequiredFields();
            return new ConnectionStatus(this, null);
        }
    }

    public enum Status implements ProtoEnum {
        CONNECTION_PAIRING(1),
        CONNECTION_SEARCH_STARTED(2),
        CONNECTION_SEARCH_FINISHED(3),
        CONNECTION_FOUND(4),
        CONNECTION_LOST(5),
        CONNECTION_PAIRED_DEVICES_CHANGED(6);
        
        private final int value;

        private Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public ConnectionStatus(Status status, String remoteDeviceId) {
        this.status = status;
        this.remoteDeviceId = remoteDeviceId;
    }

    private ConnectionStatus(Builder builder) {
        this(builder.status, builder.remoteDeviceId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ConnectionStatus)) {
            return false;
        }
        ConnectionStatus o = (ConnectionStatus) other;
        if (equals((Object) this.status, (Object) o.status) && equals((Object) this.remoteDeviceId, (Object) o.remoteDeviceId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.status != null) {
            result = this.status.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.remoteDeviceId != null) {
            i = this.remoteDeviceId.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
