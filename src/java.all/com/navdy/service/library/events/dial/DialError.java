package com.navdy.service.library.events.dial;

import com.squareup.wire.ProtoEnum;

public enum DialError implements ProtoEnum {
    DIAL_ERROR(1),
    DIAL_ALREADY_PAIRED(2),
    DIAL_OPERATION_IN_PROGRESS(3),
    DIAL_PAIRED(4),
    DIAL_NOT_FOUND(5),
    DIAL_NOT_PAIRED(6);
    
    private final int value;

    private DialError(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
