package com.navdy.service.library.events.places;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class PlaceTypeSearchRequest extends Message {
    public static final PlaceType DEFAULT_PLACE_TYPE = PlaceType.PLACE_TYPE_UNKNOWN;
    public static final String DEFAULT_REQUEST_ID = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final PlaceType place_type;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String request_id;

    public static final class Builder extends com.squareup.wire.Message.Builder<PlaceTypeSearchRequest> {
        public PlaceType place_type;
        public String request_id;

        public Builder(PlaceTypeSearchRequest message) {
            super(message);
            if (message != null) {
                this.request_id = message.request_id;
                this.place_type = message.place_type;
            }
        }

        public Builder request_id(String request_id) {
            this.request_id = request_id;
            return this;
        }

        public Builder place_type(PlaceType place_type) {
            this.place_type = place_type;
            return this;
        }

        public PlaceTypeSearchRequest build() {
            return new PlaceTypeSearchRequest(this, null);
        }
    }

    public PlaceTypeSearchRequest(String request_id, PlaceType place_type) {
        this.request_id = request_id;
        this.place_type = place_type;
    }

    private PlaceTypeSearchRequest(Builder builder) {
        this(builder.request_id, builder.place_type);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PlaceTypeSearchRequest)) {
            return false;
        }
        PlaceTypeSearchRequest o = (PlaceTypeSearchRequest) other;
        if (equals((Object) this.request_id, (Object) o.request_id) && equals((Object) this.place_type, (Object) o.place_type)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.request_id != null) {
            result = this.request_id.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.place_type != null) {
            i = this.place_type.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
