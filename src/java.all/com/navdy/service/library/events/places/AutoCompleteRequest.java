package com.navdy.service.library.events.places;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class AutoCompleteRequest extends Message {
    public static final Integer DEFAULT_MAXRESULTS = Integer.valueOf(0);
    public static final String DEFAULT_PARTIALSEARCH = "";
    public static final Integer DEFAULT_SEARCHAREA = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer maxResults;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String partialSearch;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer searchArea;

    public static final class Builder extends com.squareup.wire.Message.Builder<AutoCompleteRequest> {
        public Integer maxResults;
        public String partialSearch;
        public Integer searchArea;

        public Builder(AutoCompleteRequest message) {
            super(message);
            if (message != null) {
                this.partialSearch = message.partialSearch;
                this.searchArea = message.searchArea;
                this.maxResults = message.maxResults;
            }
        }

        public Builder partialSearch(String partialSearch) {
            this.partialSearch = partialSearch;
            return this;
        }

        public Builder searchArea(Integer searchArea) {
            this.searchArea = searchArea;
            return this;
        }

        public Builder maxResults(Integer maxResults) {
            this.maxResults = maxResults;
            return this;
        }

        public AutoCompleteRequest build() {
            checkRequiredFields();
            return new AutoCompleteRequest(this);
        }
    }

    public AutoCompleteRequest(String partialSearch, Integer searchArea, Integer maxResults) {
        this.partialSearch = partialSearch;
        this.searchArea = searchArea;
        this.maxResults = maxResults;
    }

    private AutoCompleteRequest(Builder builder) {
        this(builder.partialSearch, builder.searchArea, builder.maxResults);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof AutoCompleteRequest)) {
            return false;
        }
        AutoCompleteRequest o = (AutoCompleteRequest) other;
        if (equals((Object) this.partialSearch, (Object) o.partialSearch) && equals((Object) this.searchArea, (Object) o.searchArea) && equals((Object) this.maxResults, (Object) o.maxResults)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.partialSearch != null ? this.partialSearch.hashCode() : 0) * 37;
        if (this.searchArea != null) {
            hashCode = this.searchArea.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.maxResults != null) {
            i = this.maxResults.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
