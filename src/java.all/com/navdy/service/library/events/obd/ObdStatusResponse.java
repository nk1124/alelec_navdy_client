package com.navdy.service.library.events.obd;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class ObdStatusResponse extends Message {
    public static final Boolean DEFAULT_ISCONNECTED = Boolean.valueOf(false);
    public static final Integer DEFAULT_SPEED = Integer.valueOf(0);
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    public static final String DEFAULT_VIN = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean isConnected;
    @ProtoField(tag = 4, type = Datatype.INT32)
    public final Integer speed;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String vin;

    public static final class Builder extends com.squareup.wire.Message.Builder<ObdStatusResponse> {
        public Boolean isConnected;
        public Integer speed;
        public RequestStatus status;
        public String vin;

        public Builder(ObdStatusResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.isConnected = message.isConnected;
                this.vin = message.vin;
                this.speed = message.speed;
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder isConnected(Boolean isConnected) {
            this.isConnected = isConnected;
            return this;
        }

        public Builder vin(String vin) {
            this.vin = vin;
            return this;
        }

        public Builder speed(Integer speed) {
            this.speed = speed;
            return this;
        }

        public ObdStatusResponse build() {
            checkRequiredFields();
            return new ObdStatusResponse(this);
        }
    }

    public ObdStatusResponse(RequestStatus status, Boolean isConnected, String vin, Integer speed) {
        this.status = status;
        this.isConnected = isConnected;
        this.vin = vin;
        this.speed = speed;
    }

    private ObdStatusResponse(Builder builder) {
        this(builder.status, builder.isConnected, builder.vin, builder.speed);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ObdStatusResponse)) {
            return false;
        }
        ObdStatusResponse o = (ObdStatusResponse) other;
        if (equals((Object) this.status, (Object) o.status) && equals((Object) this.isConnected, (Object) o.isConnected) && equals((Object) this.vin, (Object) o.vin) && equals((Object) this.speed, (Object) o.speed)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.isConnected != null) {
            hashCode = this.isConnected.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.vin != null) {
            hashCode = this.vin.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.speed != null) {
            i = this.speed.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
