package com.navdy.service.library.events;

import com.squareup.wire.Message;

public final class Ping extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<Ping> {
        public Builder(Ping message) {
            super(message);
        }

        public Ping build() {
            return new Ping(this);
        }
    }

    private Ping(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof Ping;
    }

    public int hashCode() {
        return 0;
    }
}
