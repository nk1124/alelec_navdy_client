package com.navdy.service.library.events;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class Frame extends Message {
    public static final Integer DEFAULT_SIZE = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.FIXED32)
    public final Integer size;

    public static final class Builder extends com.squareup.wire.Message.Builder<Frame> {
        public Integer size;

        public Builder(Frame message) {
            super(message);
            if (message != null) {
                this.size = message.size;
            }
        }

        public Builder size(Integer size) {
            this.size = size;
            return this;
        }

        public Frame build() {
            checkRequiredFields();
            return new Frame(this);
        }
    }

    public Frame(Integer size) {
        this.size = size;
    }

    private Frame(Builder builder) {
        this(builder.size);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof Frame) {
            return equals((Object) this.size, (Object) ((Frame) other).size);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.size != null ? this.size.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
