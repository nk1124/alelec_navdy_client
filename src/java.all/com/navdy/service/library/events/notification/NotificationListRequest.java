package com.navdy.service.library.events.notification;

import com.squareup.wire.Message;

public final class NotificationListRequest extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationListRequest> {
        public Builder(NotificationListRequest message) {
            super(message);
        }

        public NotificationListRequest build() {
            return new NotificationListRequest(this);
        }
    }

    private NotificationListRequest(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof NotificationListRequest;
    }

    public int hashCode() {
        return 0;
    }
}
