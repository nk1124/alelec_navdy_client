package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoEnum;

public enum NotificationsError implements ProtoEnum {
    NOTIFICATIONS_ERROR_UNKNOWN(1),
    NOTIFICATIONS_ERROR_AUTH_FAILED(2),
    NOTIFICATIONS_ERROR_BOND_REMOVED(3);
    
    private final int value;

    private NotificationsError(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
