package com.navdy.service.library.events.notification;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NotificationSetting extends Message {
    public static final String DEFAULT_APP = "";
    public static final Boolean DEFAULT_ENABLED = Boolean.valueOf(false);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String app;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.BOOL)
    public final Boolean enabled;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationSetting> {
        public String app;
        public Boolean enabled;

        public Builder(NotificationSetting message) {
            super(message);
            if (message != null) {
                this.app = message.app;
                this.enabled = message.enabled;
            }
        }

        public Builder app(String app) {
            this.app = app;
            return this;
        }

        public Builder enabled(Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public NotificationSetting build() {
            checkRequiredFields();
            return new NotificationSetting(this, null);
        }
    }

    public NotificationSetting(String app, Boolean enabled) {
        this.app = app;
        this.enabled = enabled;
    }

    private NotificationSetting(Builder builder) {
        this(builder.app, builder.enabled);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NotificationSetting)) {
            return false;
        }
        NotificationSetting o = (NotificationSetting) other;
        if (equals((Object) this.app, (Object) o.app) && equals((Object) this.enabled, (Object) o.enabled)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.app != null) {
            result = this.app.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.enabled != null) {
            i = this.enabled.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
