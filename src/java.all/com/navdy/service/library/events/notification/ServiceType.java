package com.navdy.service.library.events.notification;

import com.squareup.wire.ProtoEnum;

public enum ServiceType implements ProtoEnum {
    SERVICE_ANCS(1),
    SERVICE_PANDORA(2);
    
    private final int value;

    private ServiceType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
