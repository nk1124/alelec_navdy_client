package com.navdy.service.library.events.notification;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NotificationAction extends Message {
    public static final Integer DEFAULT_ACTIONID = Integer.valueOf(0);
    public static final String DEFAULT_HANDLER = "";
    public static final Integer DEFAULT_ICONRESOURCE = Integer.valueOf(0);
    public static final Integer DEFAULT_ICONRESOURCEFOCUSED = Integer.valueOf(0);
    public static final Integer DEFAULT_ID = Integer.valueOf(0);
    public static final String DEFAULT_LABEL = "";
    public static final Integer DEFAULT_LABELID = Integer.valueOf(0);
    public static final Integer DEFAULT_NOTIFICATIONID = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.INT32)
    public final Integer actionId;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String handler;
    @ProtoField(tag = 7, type = Datatype.INT32)
    public final Integer iconResource;
    @ProtoField(tag = 8, type = Datatype.INT32)
    public final Integer iconResourceFocused;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.INT32)
    public final Integer id;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String label;
    @ProtoField(label = Label.REQUIRED, tag = 5, type = Datatype.INT32)
    public final Integer labelId;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT32)
    public final Integer notificationId;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationAction> {
        public Integer actionId;
        public String handler;
        public Integer iconResource;
        public Integer iconResourceFocused;
        public Integer id;
        public String label;
        public Integer labelId;
        public Integer notificationId;

        public Builder(NotificationAction message) {
            super(message);
            if (message != null) {
                this.handler = message.handler;
                this.id = message.id;
                this.notificationId = message.notificationId;
                this.actionId = message.actionId;
                this.labelId = message.labelId;
                this.label = message.label;
                this.iconResource = message.iconResource;
                this.iconResourceFocused = message.iconResourceFocused;
            }
        }

        public Builder handler(String handler) {
            this.handler = handler;
            return this;
        }

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder notificationId(Integer notificationId) {
            this.notificationId = notificationId;
            return this;
        }

        public Builder actionId(Integer actionId) {
            this.actionId = actionId;
            return this;
        }

        public Builder labelId(Integer labelId) {
            this.labelId = labelId;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder iconResource(Integer iconResource) {
            this.iconResource = iconResource;
            return this;
        }

        public Builder iconResourceFocused(Integer iconResourceFocused) {
            this.iconResourceFocused = iconResourceFocused;
            return this;
        }

        public NotificationAction build() {
            checkRequiredFields();
            return new NotificationAction(this);
        }
    }

    public NotificationAction(String handler, Integer id, Integer notificationId, Integer actionId, Integer labelId, String label, Integer iconResource, Integer iconResourceFocused) {
        this.handler = handler;
        this.id = id;
        this.notificationId = notificationId;
        this.actionId = actionId;
        this.labelId = labelId;
        this.label = label;
        this.iconResource = iconResource;
        this.iconResourceFocused = iconResourceFocused;
    }

    private NotificationAction(Builder builder) {
        this(builder.handler, builder.id, builder.notificationId, builder.actionId, builder.labelId, builder.label, builder.iconResource, builder.iconResourceFocused);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NotificationAction)) {
            return false;
        }
        NotificationAction o = (NotificationAction) other;
        if (equals((Object) this.handler, (Object) o.handler) && equals((Object) this.id, (Object) o.id) && equals((Object) this.notificationId, (Object) o.notificationId) && equals((Object) this.actionId, (Object) o.actionId) && equals((Object) this.labelId, (Object) o.labelId) && equals((Object) this.label, (Object) o.label) && equals((Object) this.iconResource, (Object) o.iconResource) && equals((Object) this.iconResourceFocused, (Object) o.iconResourceFocused)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.handler != null ? this.handler.hashCode() : 0) * 37;
        if (this.id != null) {
            hashCode = this.id.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.notificationId != null) {
            hashCode = this.notificationId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.actionId != null) {
            hashCode = this.actionId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.labelId != null) {
            hashCode = this.labelId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.label != null) {
            hashCode = this.label.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.iconResource != null) {
            hashCode = this.iconResource.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.iconResourceFocused != null) {
            i = this.iconResourceFocused.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
