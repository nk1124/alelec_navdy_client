package com.navdy.service.library.events.preferences;

import com.squareup.wire.ProtoEnum;

public enum ScrollableSide implements ProtoEnum {
    LEFT(1),
    RIGHT(2);
    
    private final int value;

    private ScrollableSide(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
