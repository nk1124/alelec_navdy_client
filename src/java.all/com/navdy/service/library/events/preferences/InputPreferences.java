package com.navdy.service.library.events.preferences;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class InputPreferences extends Message {
    public static final DialSensitivity DEFAULT_DIAL_SENSITIVITY = DialSensitivity.DIAL_SENSITIVITY_STANDARD;
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    public static final Boolean DEFAULT_USE_GESTURES = Boolean.valueOf(true);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final DialSensitivity dial_sensitivity;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(tag = 2, type = Datatype.BOOL)
    public final Boolean use_gestures;

    public static final class Builder extends com.squareup.wire.Message.Builder<InputPreferences> {
        public DialSensitivity dial_sensitivity;
        public Long serial_number;
        public Boolean use_gestures;

        public Builder(InputPreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.use_gestures = message.use_gestures;
                this.dial_sensitivity = message.dial_sensitivity;
            }
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public Builder use_gestures(Boolean use_gestures) {
            this.use_gestures = use_gestures;
            return this;
        }

        public Builder dial_sensitivity(DialSensitivity dial_sensitivity) {
            this.dial_sensitivity = dial_sensitivity;
            return this;
        }

        public InputPreferences build() {
            checkRequiredFields();
            return new InputPreferences(this);
        }
    }

    public enum DialSensitivity implements ProtoEnum {
        DIAL_SENSITIVITY_STANDARD(0),
        DIAL_SENSITIVITY_LOW(1);
        
        private final int value;

        private DialSensitivity(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public InputPreferences(Long serial_number, Boolean use_gestures, DialSensitivity dial_sensitivity) {
        this.serial_number = serial_number;
        this.use_gestures = use_gestures;
        this.dial_sensitivity = dial_sensitivity;
    }

    private InputPreferences(Builder builder) {
        this(builder.serial_number, builder.use_gestures, builder.dial_sensitivity);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof InputPreferences)) {
            return false;
        }
        InputPreferences o = (InputPreferences) other;
        if (equals((Object) this.serial_number, (Object) o.serial_number) && equals((Object) this.use_gestures, (Object) o.use_gestures) && equals((Object) this.dial_sensitivity, (Object) o.dial_sensitivity)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.serial_number != null ? this.serial_number.hashCode() : 0) * 37;
        if (this.use_gestures != null) {
            hashCode = this.use_gestures.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.dial_sensitivity != null) {
            i = this.dial_sensitivity.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
