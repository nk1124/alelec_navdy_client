package com.navdy.service.library.events.preferences;

import com.navdy.service.library.events.notification.NotificationSetting;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class NotificationPreferences extends Message {
    public static final Boolean DEFAULT_ENABLED = Boolean.valueOf(false);
    public static final Boolean DEFAULT_READALOUD = Boolean.valueOf(true);
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    public static final List<NotificationSetting> DEFAULT_SETTINGS = Collections.emptyList();
    public static final Boolean DEFAULT_SHOWCONTENT = Boolean.valueOf(true);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.BOOL)
    public final Boolean enabled;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean readAloud;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long serial_number;
    @ProtoField(label = Label.REPEATED, messageType = NotificationSetting.class, tag = 2)
    public final List<NotificationSetting> settings;
    @ProtoField(tag = 5, type = Datatype.BOOL)
    public final Boolean showContent;

    public static final class Builder extends com.squareup.wire.Message.Builder<NotificationPreferences> {
        public Boolean enabled;
        public Boolean readAloud;
        public Long serial_number;
        public List<NotificationSetting> settings;
        public Boolean showContent;

        public Builder(NotificationPreferences message) {
            super(message);
            if (message != null) {
                this.serial_number = message.serial_number;
                this.settings = Message.copyOf(message.settings);
                this.enabled = message.enabled;
                this.readAloud = message.readAloud;
                this.showContent = message.showContent;
            }
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public Builder settings(List<NotificationSetting> settings) {
            this.settings = com.squareup.wire.Message.Builder.checkForNulls(settings);
            return this;
        }

        public Builder enabled(Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public Builder readAloud(Boolean readAloud) {
            this.readAloud = readAloud;
            return this;
        }

        public Builder showContent(Boolean showContent) {
            this.showContent = showContent;
            return this;
        }

        public NotificationPreferences build() {
            checkRequiredFields();
            return new NotificationPreferences(this);
        }
    }

    public NotificationPreferences(Long serial_number, List<NotificationSetting> settings, Boolean enabled, Boolean readAloud, Boolean showContent) {
        this.serial_number = serial_number;
        this.settings = Message.immutableCopyOf(settings);
        this.enabled = enabled;
        this.readAloud = readAloud;
        this.showContent = showContent;
    }

    private NotificationPreferences(Builder builder) {
        this(builder.serial_number, builder.settings, builder.enabled, builder.readAloud, builder.showContent);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NotificationPreferences)) {
            return false;
        }
        NotificationPreferences o = (NotificationPreferences) other;
        if (equals((Object) this.serial_number, (Object) o.serial_number) && equals(this.settings, o.settings) && equals((Object) this.enabled, (Object) o.enabled) && equals((Object) this.readAloud, (Object) o.readAloud) && equals((Object) this.showContent, (Object) o.showContent)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (((this.serial_number != null ? this.serial_number.hashCode() : 0) * 37) + (this.settings != null ? this.settings.hashCode() : 1)) * 37;
        if (this.enabled != null) {
            hashCode = this.enabled.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.readAloud != null) {
            hashCode = this.readAloud.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.showContent != null) {
            i = this.showContent.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
