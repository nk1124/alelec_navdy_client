package com.navdy.service.library.events.preferences;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class DashboardPreferences extends Message {
    public static final String DEFAULT_LEFTGAUGEID = "";
    public static final MiddleGauge DEFAULT_MIDDLEGAUGE = MiddleGauge.SPEEDOMETER;
    public static final String DEFAULT_RIGHTGAUGEID = "";
    public static final ScrollableSide DEFAULT_SCROLLABLESIDE = ScrollableSide.LEFT;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String leftGaugeId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MiddleGauge middleGauge;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String rightGaugeId;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final ScrollableSide scrollableSide;

    public static final class Builder extends com.squareup.wire.Message.Builder<DashboardPreferences> {
        public String leftGaugeId;
        public MiddleGauge middleGauge;
        public String rightGaugeId;
        public ScrollableSide scrollableSide;

        public Builder(DashboardPreferences message) {
            super(message);
            if (message != null) {
                this.middleGauge = message.middleGauge;
                this.scrollableSide = message.scrollableSide;
                this.rightGaugeId = message.rightGaugeId;
                this.leftGaugeId = message.leftGaugeId;
            }
        }

        public Builder middleGauge(MiddleGauge middleGauge) {
            this.middleGauge = middleGauge;
            return this;
        }

        public Builder scrollableSide(ScrollableSide scrollableSide) {
            this.scrollableSide = scrollableSide;
            return this;
        }

        public Builder rightGaugeId(String rightGaugeId) {
            this.rightGaugeId = rightGaugeId;
            return this;
        }

        public Builder leftGaugeId(String leftGaugeId) {
            this.leftGaugeId = leftGaugeId;
            return this;
        }

        public DashboardPreferences build() {
            return new DashboardPreferences(this);
        }
    }

    public DashboardPreferences(MiddleGauge middleGauge, ScrollableSide scrollableSide, String rightGaugeId, String leftGaugeId) {
        this.middleGauge = middleGauge;
        this.scrollableSide = scrollableSide;
        this.rightGaugeId = rightGaugeId;
        this.leftGaugeId = leftGaugeId;
    }

    private DashboardPreferences(Builder builder) {
        this(builder.middleGauge, builder.scrollableSide, builder.rightGaugeId, builder.leftGaugeId);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof DashboardPreferences)) {
            return false;
        }
        DashboardPreferences o = (DashboardPreferences) other;
        if (equals((Object) this.middleGauge, (Object) o.middleGauge) && equals((Object) this.scrollableSide, (Object) o.scrollableSide) && equals((Object) this.rightGaugeId, (Object) o.rightGaugeId) && equals((Object) this.leftGaugeId, (Object) o.leftGaugeId)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.middleGauge != null ? this.middleGauge.hashCode() : 0) * 37;
        if (this.scrollableSide != null) {
            hashCode = this.scrollableSide.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.rightGaugeId != null) {
            hashCode = this.rightGaugeId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.leftGaugeId != null) {
            i = this.leftGaugeId.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
