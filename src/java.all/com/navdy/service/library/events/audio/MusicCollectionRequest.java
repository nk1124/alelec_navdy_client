package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;
import java.util.Collections;
import java.util.List;

public final class MusicCollectionRequest extends Message {
    public static final String DEFAULT_COLLECTIONID = "";
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final MusicCollectionType DEFAULT_COLLECTIONTYPE = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final List<MusicCollectionFilter> DEFAULT_FILTERS = Collections.emptyList();
    public static final MusicCollectionType DEFAULT_GROUPBY = MusicCollectionType.COLLECTION_TYPE_UNKNOWN;
    public static final Boolean DEFAULT_INCLUDECHARACTERMAP = Boolean.valueOf(false);
    public static final Integer DEFAULT_LIMIT = Integer.valueOf(0);
    public static final Integer DEFAULT_OFFSET = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4, type = Datatype.STRING)
    public final String collectionId;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final MusicCollectionType collectionType;
    @ProtoField(label = Label.REPEATED, messageType = MusicCollectionFilter.class, tag = 3)
    public final List<MusicCollectionFilter> filters;
    @ProtoField(tag = 8, type = Datatype.ENUM)
    public final MusicCollectionType groupBy;
    @ProtoField(tag = 7, type = Datatype.BOOL)
    public final Boolean includeCharacterMap;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer limit;
    @ProtoField(tag = 6, type = Datatype.INT32)
    public final Integer offset;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicCollectionRequest> {
        public String collectionId;
        public MusicCollectionSource collectionSource;
        public MusicCollectionType collectionType;
        public List<MusicCollectionFilter> filters;
        public MusicCollectionType groupBy;
        public Boolean includeCharacterMap;
        public Integer limit;
        public Integer offset;

        public Builder(MusicCollectionRequest message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.collectionType = message.collectionType;
                this.filters = Message.copyOf(message.filters);
                this.collectionId = message.collectionId;
                this.limit = message.limit;
                this.offset = message.offset;
                this.includeCharacterMap = message.includeCharacterMap;
                this.groupBy = message.groupBy;
            }
        }

        public Builder collectionSource(MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }

        public Builder collectionType(MusicCollectionType collectionType) {
            this.collectionType = collectionType;
            return this;
        }

        public Builder filters(List<MusicCollectionFilter> filters) {
            this.filters = com.squareup.wire.Message.Builder.checkForNulls(filters);
            return this;
        }

        public Builder collectionId(String collectionId) {
            this.collectionId = collectionId;
            return this;
        }

        public Builder limit(Integer limit) {
            this.limit = limit;
            return this;
        }

        public Builder offset(Integer offset) {
            this.offset = offset;
            return this;
        }

        public Builder includeCharacterMap(Boolean includeCharacterMap) {
            this.includeCharacterMap = includeCharacterMap;
            return this;
        }

        public Builder groupBy(MusicCollectionType groupBy) {
            this.groupBy = groupBy;
            return this;
        }

        public MusicCollectionRequest build() {
            return new MusicCollectionRequest(this);
        }
    }

    public MusicCollectionRequest(MusicCollectionSource collectionSource, MusicCollectionType collectionType, List<MusicCollectionFilter> filters, String collectionId, Integer limit, Integer offset, Boolean includeCharacterMap, MusicCollectionType groupBy) {
        this.collectionSource = collectionSource;
        this.collectionType = collectionType;
        this.filters = Message.immutableCopyOf(filters);
        this.collectionId = collectionId;
        this.limit = limit;
        this.offset = offset;
        this.includeCharacterMap = includeCharacterMap;
        this.groupBy = groupBy;
    }

    private MusicCollectionRequest(Builder builder) {
        this(builder.collectionSource, builder.collectionType, builder.filters, builder.collectionId, builder.limit, builder.offset, builder.includeCharacterMap, builder.groupBy);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicCollectionRequest)) {
            return false;
        }
        MusicCollectionRequest o = (MusicCollectionRequest) other;
        if (equals((Object) this.collectionSource, (Object) o.collectionSource) && equals((Object) this.collectionType, (Object) o.collectionType) && equals(this.filters, o.filters) && equals((Object) this.collectionId, (Object) o.collectionId) && equals((Object) this.limit, (Object) o.limit) && equals((Object) this.offset, (Object) o.offset) && equals((Object) this.includeCharacterMap, (Object) o.includeCharacterMap) && equals((Object) this.groupBy, (Object) o.groupBy)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.collectionSource != null ? this.collectionSource.hashCode() : 0) * 37;
        if (this.collectionType != null) {
            hashCode = this.collectionType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (((hashCode2 + hashCode) * 37) + (this.filters != null ? this.filters.hashCode() : 1)) * 37;
        if (this.collectionId != null) {
            hashCode = this.collectionId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.limit != null) {
            hashCode = this.limit.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.offset != null) {
            hashCode = this.offset.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.includeCharacterMap != null) {
            hashCode = this.includeCharacterMap.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.groupBy != null) {
            i = this.groupBy.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
