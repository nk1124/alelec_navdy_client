package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class SpeechRequest extends Message {
    public static final Category DEFAULT_CATEGORY = Category.SPEECH_TURN_BY_TURN;
    public static final String DEFAULT_ID = "";
    public static final String DEFAULT_LOCALE = "";
    public static final Boolean DEFAULT_SENDSTATUS = Boolean.valueOf(false);
    public static final String DEFAULT_WORDS = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2, type = Datatype.ENUM)
    public final Category category;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String id;
    @ProtoField(tag = 5, type = Datatype.STRING)
    public final String locale;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean sendStatus;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String words;

    public static final class Builder extends com.squareup.wire.Message.Builder<SpeechRequest> {
        public Category category;
        public String id;
        public String locale;
        public Boolean sendStatus;
        public String words;

        public Builder(SpeechRequest message) {
            super(message);
            if (message != null) {
                this.words = message.words;
                this.category = message.category;
                this.id = message.id;
                this.sendStatus = message.sendStatus;
                this.locale = message.locale;
            }
        }

        public Builder words(String words) {
            this.words = words;
            return this;
        }

        public Builder category(Category category) {
            this.category = category;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder sendStatus(Boolean sendStatus) {
            this.sendStatus = sendStatus;
            return this;
        }

        public Builder locale(String locale) {
            this.locale = locale;
            return this;
        }

        public SpeechRequest build() {
            checkRequiredFields();
            return new SpeechRequest(this);
        }
    }

    public enum Category implements ProtoEnum {
        SPEECH_TURN_BY_TURN(1),
        SPEECH_REROUTE(2),
        SPEECH_SPEED_WARNING(3),
        SPEECH_GPS_CONNECTIVITY(4),
        SPEECH_MESSAGE_READ_OUT(5),
        SPEECH_WELCOME_MESSAGE(6),
        SPEECH_NOTIFICATION(7),
        SPEECH_CAMERA_WARNING(8);
        
        private final int value;

        private Category(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public SpeechRequest(String words, Category category, String id, Boolean sendStatus, String locale) {
        this.words = words;
        this.category = category;
        this.id = id;
        this.sendStatus = sendStatus;
        this.locale = locale;
    }

    private SpeechRequest(Builder builder) {
        this(builder.words, builder.category, builder.id, builder.sendStatus, builder.locale);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof SpeechRequest)) {
            return false;
        }
        SpeechRequest o = (SpeechRequest) other;
        if (equals((Object) this.words, (Object) o.words) && equals((Object) this.category, (Object) o.category) && equals((Object) this.id, (Object) o.id) && equals((Object) this.sendStatus, (Object) o.sendStatus) && equals((Object) this.locale, (Object) o.locale)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.words != null ? this.words.hashCode() : 0) * 37;
        if (this.category != null) {
            hashCode = this.category.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.id != null) {
            hashCode = this.id.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.sendStatus != null) {
            hashCode = this.sendStatus.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.locale != null) {
            i = this.locale.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
