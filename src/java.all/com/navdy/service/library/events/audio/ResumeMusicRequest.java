package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;

public final class ResumeMusicRequest extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<ResumeMusicRequest> {
        public Builder(ResumeMusicRequest message) {
            super(message);
        }

        public ResumeMusicRequest build() {
            return new ResumeMusicRequest(this);
        }
    }

    private ResumeMusicRequest(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof ResumeMusicRequest;
    }

    public int hashCode() {
        return 0;
    }
}
