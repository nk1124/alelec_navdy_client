package com.navdy.service.library.events.audio;

import com.squareup.wire.ProtoEnum;

public enum MusicCollectionType implements ProtoEnum {
    COLLECTION_TYPE_UNKNOWN(0),
    COLLECTION_TYPE_PLAYLISTS(1),
    COLLECTION_TYPE_ARTISTS(2),
    COLLECTION_TYPE_ALBUMS(3),
    COLLECTION_TYPE_PODCASTS(4),
    COLLECTION_TYPE_AUDIOBOOKS(5);
    
    private final int value;

    private MusicCollectionType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
