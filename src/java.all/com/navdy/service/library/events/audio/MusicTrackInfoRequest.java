package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;

public final class MusicTrackInfoRequest extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicTrackInfoRequest> {
        public Builder(MusicTrackInfoRequest message) {
            super(message);
        }

        public MusicTrackInfoRequest build() {
            return new MusicTrackInfoRequest(this);
        }
    }

    private MusicTrackInfoRequest(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof MusicTrackInfoRequest;
    }

    public int hashCode() {
        return 0;
    }
}
