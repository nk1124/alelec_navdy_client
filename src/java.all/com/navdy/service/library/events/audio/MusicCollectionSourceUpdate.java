package com.navdy.service.library.events.audio;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class MusicCollectionSourceUpdate extends Message {
    public static final MusicCollectionSource DEFAULT_COLLECTIONSOURCE = MusicCollectionSource.COLLECTION_SOURCE_UNKNOWN;
    public static final Long DEFAULT_SERIAL_NUMBER = Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 1, type = Datatype.ENUM)
    public final MusicCollectionSource collectionSource;
    @ProtoField(tag = 2, type = Datatype.INT64)
    public final Long serial_number;

    public static final class Builder extends com.squareup.wire.Message.Builder<MusicCollectionSourceUpdate> {
        public MusicCollectionSource collectionSource;
        public Long serial_number;

        public Builder(MusicCollectionSourceUpdate message) {
            super(message);
            if (message != null) {
                this.collectionSource = message.collectionSource;
                this.serial_number = message.serial_number;
            }
        }

        public Builder collectionSource(MusicCollectionSource collectionSource) {
            this.collectionSource = collectionSource;
            return this;
        }

        public Builder serial_number(Long serial_number) {
            this.serial_number = serial_number;
            return this;
        }

        public MusicCollectionSourceUpdate build() {
            return new MusicCollectionSourceUpdate(this, null);
        }
    }

    public MusicCollectionSourceUpdate(MusicCollectionSource collectionSource, Long serial_number) {
        this.collectionSource = collectionSource;
        this.serial_number = serial_number;
    }

    private MusicCollectionSourceUpdate(Builder builder) {
        this(builder.collectionSource, builder.serial_number);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof MusicCollectionSourceUpdate)) {
            return false;
        }
        MusicCollectionSourceUpdate o = (MusicCollectionSourceUpdate) other;
        if (equals((Object) this.collectionSource, (Object) o.collectionSource) && equals((Object) this.serial_number, (Object) o.serial_number)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.collectionSource != null) {
            result = this.collectionSource.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.serial_number != null) {
            i = this.serial_number.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
