package com.navdy.service.library.events.navigation;

import com.squareup.wire.ProtoEnum;

public enum DistanceUnit implements ProtoEnum {
    DISTANCE_MILES(1),
    DISTANCE_KMS(2),
    DISTANCE_FEET(3),
    DISTANCE_METERS(4);
    
    private final int value;

    private DistanceUnit(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
