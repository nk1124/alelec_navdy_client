package com.navdy.service.library.events.navigation;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class NavigationSessionDeferRequest extends Message {
    public static final Integer DEFAULT_DELAY = Integer.valueOf(0);
    public static final Long DEFAULT_EXPIRETIMESTAMP = Long.valueOf(0);
    public static final Boolean DEFAULT_ORIGINDISPLAY = Boolean.valueOf(false);
    public static final String DEFAULT_REQUESTID = "";
    public static final String DEFAULT_ROUTEID = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 3, type = Datatype.INT32)
    public final Integer delay;
    @ProtoField(tag = 5, type = Datatype.INT64)
    public final Long expireTimestamp;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean originDisplay;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.STRING)
    public final String requestId;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String routeId;

    public static final class Builder extends com.squareup.wire.Message.Builder<NavigationSessionDeferRequest> {
        public Integer delay;
        public Long expireTimestamp;
        public Boolean originDisplay;
        public String requestId;
        public String routeId;

        public Builder(NavigationSessionDeferRequest message) {
            super(message);
            if (message != null) {
                this.requestId = message.requestId;
                this.routeId = message.routeId;
                this.delay = message.delay;
                this.originDisplay = message.originDisplay;
                this.expireTimestamp = message.expireTimestamp;
            }
        }

        public Builder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder routeId(String routeId) {
            this.routeId = routeId;
            return this;
        }

        public Builder delay(Integer delay) {
            this.delay = delay;
            return this;
        }

        public Builder originDisplay(Boolean originDisplay) {
            this.originDisplay = originDisplay;
            return this;
        }

        public Builder expireTimestamp(Long expireTimestamp) {
            this.expireTimestamp = expireTimestamp;
            return this;
        }

        public NavigationSessionDeferRequest build() {
            checkRequiredFields();
            return new NavigationSessionDeferRequest(this);
        }
    }

    public NavigationSessionDeferRequest(String requestId, String routeId, Integer delay, Boolean originDisplay, Long expireTimestamp) {
        this.requestId = requestId;
        this.routeId = routeId;
        this.delay = delay;
        this.originDisplay = originDisplay;
        this.expireTimestamp = expireTimestamp;
    }

    private NavigationSessionDeferRequest(Builder builder) {
        this(builder.requestId, builder.routeId, builder.delay, builder.originDisplay, builder.expireTimestamp);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof NavigationSessionDeferRequest)) {
            return false;
        }
        NavigationSessionDeferRequest o = (NavigationSessionDeferRequest) other;
        if (equals((Object) this.requestId, (Object) o.requestId) && equals((Object) this.routeId, (Object) o.routeId) && equals((Object) this.delay, (Object) o.delay) && equals((Object) this.originDisplay, (Object) o.originDisplay) && equals((Object) this.expireTimestamp, (Object) o.expireTimestamp)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.requestId != null ? this.requestId.hashCode() : 0) * 37;
        if (this.routeId != null) {
            hashCode = this.routeId.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.delay != null) {
            hashCode = this.delay.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.originDisplay != null) {
            hashCode = this.originDisplay.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.expireTimestamp != null) {
            i = this.expireTimestamp.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
