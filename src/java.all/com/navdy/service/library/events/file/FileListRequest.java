package com.navdy.service.library.events.file;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class FileListRequest extends Message {
    public static final FileType DEFAULT_FILE_TYPE = FileType.FILE_TYPE_OTA;
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final FileType file_type;

    public static final class Builder extends com.squareup.wire.Message.Builder<FileListRequest> {
        public FileType file_type;

        public Builder(FileListRequest message) {
            super(message);
            if (message != null) {
                this.file_type = message.file_type;
            }
        }

        public Builder file_type(FileType file_type) {
            this.file_type = file_type;
            return this;
        }

        public FileListRequest build() {
            checkRequiredFields();
            return new FileListRequest(this);
        }
    }

    public FileListRequest(FileType file_type) {
        this.file_type = file_type;
    }

    private FileListRequest(Builder builder) {
        this(builder.file_type);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof FileListRequest) {
            return equals((Object) this.file_type, (Object) ((FileListRequest) other).file_type);
        }
        return false;
    }

    public int hashCode() {
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode = this.file_type != null ? this.file_type.hashCode() : 0;
        this.hashCode = hashCode;
        return hashCode;
    }
}
