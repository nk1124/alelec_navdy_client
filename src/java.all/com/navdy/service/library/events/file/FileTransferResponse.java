package com.navdy.service.library.events.file;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class FileTransferResponse extends Message {
    public static final String DEFAULT_CHECKSUM = "";
    public static final String DEFAULT_DESTINATIONFILENAME = "";
    public static final FileTransferError DEFAULT_ERROR = FileTransferError.FILE_TRANSFER_NO_ERROR;
    public static final FileType DEFAULT_FILETYPE = FileType.FILE_TYPE_OTA;
    public static final Integer DEFAULT_MAXCHUNKSIZE = Integer.valueOf(0);
    public static final Long DEFAULT_OFFSET = Long.valueOf(0);
    public static final Boolean DEFAULT_SUCCESS = Boolean.valueOf(false);
    public static final Boolean DEFAULT_SUPPORTSACKS = Boolean.valueOf(false);
    public static final Integer DEFAULT_TRANSFERID = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String checksum;
    @ProtoField(label = Label.REQUIRED, tag = 8, type = Datatype.STRING)
    public final String destinationFileName;
    @ProtoField(tag = 6, type = Datatype.ENUM)
    public final FileTransferError error;
    @ProtoField(label = Label.REQUIRED, tag = 7, type = Datatype.ENUM)
    public final FileType fileType;
    @ProtoField(tag = 5, type = Datatype.INT32)
    public final Integer maxChunkSize;
    @ProtoField(tag = 2, type = Datatype.INT64)
    public final Long offset;
    @ProtoField(label = Label.REQUIRED, tag = 4, type = Datatype.BOOL)
    public final Boolean success;
    @ProtoField(tag = 9, type = Datatype.BOOL)
    public final Boolean supportsAcks;
    @ProtoField(tag = 1, type = Datatype.INT32)
    public final Integer transferId;

    public static final class Builder extends com.squareup.wire.Message.Builder<FileTransferResponse> {
        public String checksum;
        public String destinationFileName;
        public FileTransferError error;
        public FileType fileType;
        public Integer maxChunkSize;
        public Long offset;
        public Boolean success;
        public Boolean supportsAcks;
        public Integer transferId;

        public Builder(FileTransferResponse message) {
            super(message);
            if (message != null) {
                this.transferId = message.transferId;
                this.offset = message.offset;
                this.checksum = message.checksum;
                this.success = message.success;
                this.maxChunkSize = message.maxChunkSize;
                this.error = message.error;
                this.fileType = message.fileType;
                this.destinationFileName = message.destinationFileName;
                this.supportsAcks = message.supportsAcks;
            }
        }

        public Builder transferId(Integer transferId) {
            this.transferId = transferId;
            return this;
        }

        public Builder offset(Long offset) {
            this.offset = offset;
            return this;
        }

        public Builder checksum(String checksum) {
            this.checksum = checksum;
            return this;
        }

        public Builder success(Boolean success) {
            this.success = success;
            return this;
        }

        public Builder maxChunkSize(Integer maxChunkSize) {
            this.maxChunkSize = maxChunkSize;
            return this;
        }

        public Builder error(FileTransferError error) {
            this.error = error;
            return this;
        }

        public Builder fileType(FileType fileType) {
            this.fileType = fileType;
            return this;
        }

        public Builder destinationFileName(String destinationFileName) {
            this.destinationFileName = destinationFileName;
            return this;
        }

        public Builder supportsAcks(Boolean supportsAcks) {
            this.supportsAcks = supportsAcks;
            return this;
        }

        public FileTransferResponse build() {
            checkRequiredFields();
            return new FileTransferResponse(this);
        }
    }

    public FileTransferResponse(Integer transferId, Long offset, String checksum, Boolean success, Integer maxChunkSize, FileTransferError error, FileType fileType, String destinationFileName, Boolean supportsAcks) {
        this.transferId = transferId;
        this.offset = offset;
        this.checksum = checksum;
        this.success = success;
        this.maxChunkSize = maxChunkSize;
        this.error = error;
        this.fileType = fileType;
        this.destinationFileName = destinationFileName;
        this.supportsAcks = supportsAcks;
    }

    private FileTransferResponse(Builder builder) {
        this(builder.transferId, builder.offset, builder.checksum, builder.success, builder.maxChunkSize, builder.error, builder.fileType, builder.destinationFileName, builder.supportsAcks);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof FileTransferResponse)) {
            return false;
        }
        FileTransferResponse o = (FileTransferResponse) other;
        if (equals((Object) this.transferId, (Object) o.transferId) && equals((Object) this.offset, (Object) o.offset) && equals((Object) this.checksum, (Object) o.checksum) && equals((Object) this.success, (Object) o.success) && equals((Object) this.maxChunkSize, (Object) o.maxChunkSize) && equals((Object) this.error, (Object) o.error) && equals((Object) this.fileType, (Object) o.fileType) && equals((Object) this.destinationFileName, (Object) o.destinationFileName) && equals((Object) this.supportsAcks, (Object) o.supportsAcks)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.transferId != null ? this.transferId.hashCode() : 0) * 37;
        if (this.offset != null) {
            hashCode = this.offset.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.checksum != null) {
            hashCode = this.checksum.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.success != null) {
            hashCode = this.success.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.maxChunkSize != null) {
            hashCode = this.maxChunkSize.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.error != null) {
            hashCode = this.error.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.fileType != null) {
            hashCode = this.fileType.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.destinationFileName != null) {
            hashCode = this.destinationFileName.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.supportsAcks != null) {
            i = this.supportsAcks.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
