package com.navdy.service.library.events.settings;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class DateTimeConfiguration extends Message {
    public static final Clock DEFAULT_FORMAT = Clock.CLOCK_24_HOUR;
    public static final Long DEFAULT_TIMESTAMP = Long.valueOf(0);
    public static final String DEFAULT_TIMEZONE = "";
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.ENUM)
    public final Clock format;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.INT64)
    public final Long timestamp;
    @ProtoField(label = Label.REQUIRED, tag = 2, type = Datatype.STRING)
    public final String timezone;

    public static final class Builder extends com.squareup.wire.Message.Builder<DateTimeConfiguration> {
        public Clock format;
        public Long timestamp;
        public String timezone;

        public Builder(DateTimeConfiguration message) {
            super(message);
            if (message != null) {
                this.timestamp = message.timestamp;
                this.timezone = message.timezone;
                this.format = message.format;
            }
        }

        public Builder timestamp(Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder timezone(String timezone) {
            this.timezone = timezone;
            return this;
        }

        public Builder format(Clock format) {
            this.format = format;
            return this;
        }

        public DateTimeConfiguration build() {
            checkRequiredFields();
            return new DateTimeConfiguration(this);
        }
    }

    public enum Clock implements ProtoEnum {
        CLOCK_24_HOUR(1),
        CLOCK_12_HOUR(2);
        
        private final int value;

        private Clock(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public DateTimeConfiguration(Long timestamp, String timezone, Clock format) {
        this.timestamp = timestamp;
        this.timezone = timezone;
        this.format = format;
    }

    private DateTimeConfiguration(Builder builder) {
        this(builder.timestamp, builder.timezone, builder.format);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof DateTimeConfiguration)) {
            return false;
        }
        DateTimeConfiguration o = (DateTimeConfiguration) other;
        if (equals((Object) this.timestamp, (Object) o.timestamp) && equals((Object) this.timezone, (Object) o.timezone) && equals((Object) this.format, (Object) o.format)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.timestamp != null ? this.timestamp.hashCode() : 0) * 37;
        if (this.timezone != null) {
            hashCode = this.timezone.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.format != null) {
            i = this.format.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
