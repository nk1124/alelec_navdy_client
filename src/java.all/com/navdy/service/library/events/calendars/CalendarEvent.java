package com.navdy.service.library.events.calendars;

import com.navdy.service.library.events.destination.Destination;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.ProtoField;

public final class CalendarEvent extends Message {
    public static final Boolean DEFAULT_ALL_DAY = Boolean.valueOf(false);
    public static final String DEFAULT_CALENDAR_NAME = "";
    public static final Integer DEFAULT_COLOR = Integer.valueOf(0);
    public static final String DEFAULT_DISPLAY_NAME = "";
    public static final Long DEFAULT_END_TIME = Long.valueOf(0);
    public static final String DEFAULT_LOCATION = "";
    public static final Long DEFAULT_START_TIME = Long.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 4, type = Datatype.BOOL)
    public final Boolean all_day;
    @ProtoField(tag = 7, type = Datatype.STRING)
    public final String calendar_name;
    @ProtoField(tag = 5, type = Datatype.FIXED32)
    public final Integer color;
    @ProtoField(tag = 8)
    public final Destination destination;
    @ProtoField(tag = 1, type = Datatype.STRING)
    public final String display_name;
    @ProtoField(tag = 3, type = Datatype.INT64)
    public final Long end_time;
    @ProtoField(tag = 6, type = Datatype.STRING)
    public final String location;
    @ProtoField(tag = 2, type = Datatype.INT64)
    public final Long start_time;

    public static final class Builder extends com.squareup.wire.Message.Builder<CalendarEvent> {
        public Boolean all_day;
        public String calendar_name;
        public Integer color;
        public Destination destination;
        public String display_name;
        public Long end_time;
        public String location;
        public Long start_time;

        public Builder(CalendarEvent message) {
            super(message);
            if (message != null) {
                this.display_name = message.display_name;
                this.start_time = message.start_time;
                this.end_time = message.end_time;
                this.all_day = message.all_day;
                this.color = message.color;
                this.location = message.location;
                this.calendar_name = message.calendar_name;
                this.destination = message.destination;
            }
        }

        public Builder display_name(String display_name) {
            this.display_name = display_name;
            return this;
        }

        public Builder start_time(Long start_time) {
            this.start_time = start_time;
            return this;
        }

        public Builder end_time(Long end_time) {
            this.end_time = end_time;
            return this;
        }

        public Builder all_day(Boolean all_day) {
            this.all_day = all_day;
            return this;
        }

        public Builder color(Integer color) {
            this.color = color;
            return this;
        }

        public Builder location(String location) {
            this.location = location;
            return this;
        }

        public Builder calendar_name(String calendar_name) {
            this.calendar_name = calendar_name;
            return this;
        }

        public Builder destination(Destination destination) {
            this.destination = destination;
            return this;
        }

        public CalendarEvent build() {
            return new CalendarEvent(this);
        }
    }

    public CalendarEvent(String display_name, Long start_time, Long end_time, Boolean all_day, Integer color, String location, String calendar_name, Destination destination) {
        this.display_name = display_name;
        this.start_time = start_time;
        this.end_time = end_time;
        this.all_day = all_day;
        this.color = color;
        this.location = location;
        this.calendar_name = calendar_name;
        this.destination = destination;
    }

    private CalendarEvent(Builder builder) {
        this(builder.display_name, builder.start_time, builder.end_time, builder.all_day, builder.color, builder.location, builder.calendar_name, builder.destination);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof CalendarEvent)) {
            return false;
        }
        CalendarEvent o = (CalendarEvent) other;
        if (equals((Object) this.display_name, (Object) o.display_name) && equals((Object) this.start_time, (Object) o.start_time) && equals((Object) this.end_time, (Object) o.end_time) && equals((Object) this.all_day, (Object) o.all_day) && equals((Object) this.color, (Object) o.color) && equals((Object) this.location, (Object) o.location) && equals((Object) this.calendar_name, (Object) o.calendar_name) && equals((Object) this.destination, (Object) o.destination)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.display_name != null ? this.display_name.hashCode() : 0) * 37;
        if (this.start_time != null) {
            hashCode = this.start_time.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.end_time != null) {
            hashCode = this.end_time.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.all_day != null) {
            hashCode = this.all_day.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.color != null) {
            hashCode = this.color.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.location != null) {
            hashCode = this.location.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode2 = (hashCode2 + hashCode) * 37;
        if (this.calendar_name != null) {
            hashCode = this.calendar_name.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.destination != null) {
            i = this.destination.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
