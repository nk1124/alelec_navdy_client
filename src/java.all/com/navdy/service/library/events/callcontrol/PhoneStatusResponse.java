package com.navdy.service.library.events.callcontrol;

import com.navdy.service.library.events.RequestStatus;
import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class PhoneStatusResponse extends Message {
    public static final RequestStatus DEFAULT_STATUS = RequestStatus.REQUEST_SUCCESS;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 2)
    public final PhoneEvent callStatus;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final RequestStatus status;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhoneStatusResponse> {
        public PhoneEvent callStatus;
        public RequestStatus status;

        public Builder(PhoneStatusResponse message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.callStatus = message.callStatus;
            }
        }

        public Builder status(RequestStatus status) {
            this.status = status;
            return this;
        }

        public Builder callStatus(PhoneEvent callStatus) {
            this.callStatus = callStatus;
            return this;
        }

        public PhoneStatusResponse build() {
            checkRequiredFields();
            return new PhoneStatusResponse(this, null);
        }
    }

    public PhoneStatusResponse(RequestStatus status, PhoneEvent callStatus) {
        this.status = status;
        this.callStatus = callStatus;
    }

    private PhoneStatusResponse(Builder builder) {
        this(builder.status, builder.callStatus);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhoneStatusResponse)) {
            return false;
        }
        PhoneStatusResponse o = (PhoneStatusResponse) other;
        if (equals((Object) this.status, (Object) o.status) && equals((Object) this.callStatus, (Object) o.callStatus)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        if (this.status != null) {
            result = this.status.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 37;
        if (this.callStatus != null) {
            i = this.callStatus.hashCode();
        }
        result = i2 + i;
        this.hashCode = result;
        return result;
    }
}
