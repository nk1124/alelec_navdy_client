package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class TelephonyRequest extends Message {
    public static final CallAction DEFAULT_ACTION = CallAction.CALL_ACCEPT;
    public static final String DEFAULT_CALLUUID = "";
    public static final String DEFAULT_NUMBER = "";
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final CallAction action;
    @ProtoField(tag = 3, type = Datatype.STRING)
    public final String callUUID;
    @ProtoField(tag = 2, type = Datatype.STRING)
    public final String number;

    public static final class Builder extends com.squareup.wire.Message.Builder<TelephonyRequest> {
        public CallAction action;
        public String callUUID;
        public String number;

        public Builder(TelephonyRequest message) {
            super(message);
            if (message != null) {
                this.action = message.action;
                this.number = message.number;
                this.callUUID = message.callUUID;
            }
        }

        public Builder action(CallAction action) {
            this.action = action;
            return this;
        }

        public Builder number(String number) {
            this.number = number;
            return this;
        }

        public Builder callUUID(String callUUID) {
            this.callUUID = callUUID;
            return this;
        }

        public TelephonyRequest build() {
            checkRequiredFields();
            return new TelephonyRequest(this);
        }
    }

    public TelephonyRequest(CallAction action, String number, String callUUID) {
        this.action = action;
        this.number = number;
        this.callUUID = callUUID;
    }

    private TelephonyRequest(Builder builder) {
        this(builder.action, builder.number, builder.callUUID);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof TelephonyRequest)) {
            return false;
        }
        TelephonyRequest o = (TelephonyRequest) other;
        if (equals((Object) this.action, (Object) o.action) && equals((Object) this.number, (Object) o.number) && equals((Object) this.callUUID, (Object) o.callUUID)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.action != null ? this.action.hashCode() : 0) * 37;
        if (this.number != null) {
            hashCode = this.number.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.callUUID != null) {
            i = this.callUUID.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
