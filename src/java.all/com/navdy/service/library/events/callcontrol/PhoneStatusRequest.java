package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.Message;

public final class PhoneStatusRequest extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<PhoneStatusRequest> {
        public Builder(PhoneStatusRequest message) {
            super(message);
        }

        public PhoneStatusRequest build() {
            return new PhoneStatusRequest(this);
        }
    }

    private PhoneStatusRequest(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof PhoneStatusRequest;
    }

    public int hashCode() {
        return 0;
    }
}
