package com.navdy.service.library.events.input;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoField;

public final class GestureEvent extends Message {
    public static final Gesture DEFAULT_GESTURE = Gesture.GESTURE_SWIPE_LEFT;
    public static final Integer DEFAULT_X = Integer.valueOf(0);
    public static final Integer DEFAULT_Y = Integer.valueOf(0);
    private static final long serialVersionUID = 0;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final Gesture gesture;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer x;
    @ProtoField(tag = 3, type = Datatype.INT32)
    public final Integer y;

    public static final class Builder extends com.squareup.wire.Message.Builder<GestureEvent> {
        public Gesture gesture;
        public Integer x;
        public Integer y;

        public Builder(GestureEvent message) {
            super(message);
            if (message != null) {
                this.gesture = message.gesture;
                this.x = message.x;
                this.y = message.y;
            }
        }

        public Builder gesture(Gesture gesture) {
            this.gesture = gesture;
            return this;
        }

        public Builder x(Integer x) {
            this.x = x;
            return this;
        }

        public Builder y(Integer y) {
            this.y = y;
            return this;
        }

        public GestureEvent build() {
            checkRequiredFields();
            return new GestureEvent(this);
        }
    }

    public GestureEvent(Gesture gesture, Integer x, Integer y) {
        this.gesture = gesture;
        this.x = x;
        this.y = y;
    }

    private GestureEvent(Builder builder) {
        this(builder.gesture, builder.x, builder.y);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof GestureEvent)) {
            return false;
        }
        GestureEvent o = (GestureEvent) other;
        if (equals((Object) this.gesture, (Object) o.gesture) && equals((Object) this.x, (Object) o.x) && equals((Object) this.y, (Object) o.y)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.gesture != null ? this.gesture.hashCode() : 0) * 37;
        if (this.x != null) {
            hashCode = this.x.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.y != null) {
            i = this.y.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
